@extends('layouts.new')

@section('title', 'Logbeheer')
@section('page_title', 'Logbeheer')

@push('styles')

@endpush

@push('scripts')

@endpush
@section('content')
    @if(Auth::user()->rol != 99)

        <!-- Start:: row-1 -->
        <div class="row">
        @foreach(App\Models\User::all() as $user)
            @if(count($user->myLast10Logs) > 0)
                <div class="col-xl-6">
                    <div class="card custom-card">
                        <div class="card-header justify-content-between">
                            <div class="card-title">
                                {{$user->name}}
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Aangemaakt Op</th>
                                        <th>Type</th>
                                        <th>Omschrijving</th>
                                        <th>Klant</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($user->myLast10Logs as $log)
                                        <tr>
                                            <td>{{date('d-m-Y H:i', strtotime($log->created_at))}}</td>
                                            <td>{{$log->type}}</td>
                                            <td>{{$log->omschrijving}}</td>
                                            @if($log->klant != null)
                                                <td><a href="/relatie/{{$log->klant->id}}">{{$log->klant->postcode}} {{$log->klant->huisnummer}}</a></td>
                                            @else
                                                <td>NVT</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="4">
                                                <a href="/controle/logbeheer/{{$user->id}}">Bekijk het volledig overzicht van {{$user->name}}</a>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        </div>
        <!-- End:: row-1 -->
    @endif
@endsection
