@extends('layouts.master')

@section('title', 'Flexavi - Logbeheer')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)


<h4 class="heading_a uk-margin-bottom">Logbeheer</h4>
<div class="uk-grid uk-grid-width-large-1-1 uk-grid-width-medium-1-1 uk-grid-medium ">
    <div class="md-card">
        <div class="md-card-content">
            {{$user->name}}
            <div class="uk-overflow-container">
                <table id="dt_individual_search" class="uk-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Aangemaakt Op</th>
                            <th>Type</th>
                            <th>Omschrijving</th>
                            <th>Klant</th>
                        </tr>
                    </thead>
                    @foreach($logs as $log)
                        <tr>
                            <td>{{date('d-m-Y H:i', strtotime($log->created_at))}}</td>
                            <td>{{$log->type}}</td>
                            <td>{{$log->omschrijving}}</td>
                            @if($log->klant != null)
                            <td><a href="/relatie/{{$log->klant->id}}">{{$log->klant->postcode}} {{$log->klant->huisnummer}} </a></td>
                            @else
                            <td>NVT</td>
                            @endif
                        </tr>    
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>


<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-3">
        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">
            <button class="md-btn">Bekijk logboek van een andere gebruiker <i class="material-icons">&#xE315;</i></button>
            <div class="uk-dropdown">
                <ul class="uk-nav uk-nav-dropdown">
                    @foreach($users as $user)
                    <li><a href="/controle/logbeheer/{{$user->id}}">{{$user->name}}</a></li>
                    @endforeach
                    <li><a href="/controle/logbeheer">Totaal Overzicht</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>


@endif

@endsection