@extends('layouts.master')

@section('title', 'Offerte Verwerken - Extra')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>

@endpush
@section('content')
<h2 class="heading_b uk-margin-bottom">Offerte werkzaamheden verwerken {{$relatie->voornaam." ".$relatie->achternaam}}</h2>
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-2 uk-width-medium-1-1">
        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                {!! Form::open(array('url'=>'/offerte/afspraak/'.$afspraak->id.'/nieuw', 'method' => 'post', 'data-parsley-validate')) !!}
                <div class="uk-grid" data-uk-grid-margin>
             <h3>Afspraak Informatie</h3>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-2-4">
                            <label for="kUI_datetimepicker_basic" class="uk-form-label">Start Datum:</label>
                            <input id="kUI_datetimepicker_basic" name="startdatum" required/>
                        </div>
                        <div class="uk-width-large-2-4">
                            <label for="einddatum" class="uk-form-label">Eind Datum:</label>
                            <input id="kUI_datetimepicker_basic_end" name="einddatum" required/>
                        </div>
                            
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-2-4">
                            <select id="select_demo_5" name="geschreven_door" data-md-selectize data-md-selectize-bottom>
                                @foreach($werknemers as $werknemer)
                                <option value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                @endforeach
                            </select>
                            <span class="uk-form-help-block">Geschreven Door:</span>
                        </div>
                        <div class="uk-width-medium-2-4">
                            <select id="select_demo_5" name="afspraaktype_id" data-md-selectize data-md-selectize-bottom>
                                @foreach($afspraaktypes as $type)
                                <option value="{{$type->id}}">{{$type->omschrijving}}</option>
                                @endforeach
                            </select>
                            <span class="uk-form-help-block">Type Afspraak</span>
                        </div>
                        <input type="hidden" name="verwerkt_door" value="{{Auth::user()->id}}"/>
                        <input type="hidden" name="klant_id" value="{{$relatie->id}}"/>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-1">
                            <label>Omschrijving</label><br>
                            <textarea id='editor' class="md-input label-fixed" id="omschrijving" placeholder="Schrijf hier de nuttige informatie voor de collega's, voorbeelden: Uitvoering werkzaamheden: Klant wilt een half uur van te voren gebeld worden. Garantie: Dakgoot lekt nog steeds. Uitgestelde Betaling: Mevrouw betaalt € 70,-. 
                                Offerte Opstellen: Betalingsherinnering: Let op, klant betaalt binnen twee weken per bank. Terugbetalingsverzoek: Dakgoot is binnen de garantie niet meer te repareren, crediteer € 35,-" name="omschrijving"></textarea>
                            
                        </div>
                    </div>
                </div>
                <br>
                    <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="0" id="radio_demo_inline_1" data-md-icheck />
                        <label for="radio_demo_inline_1" class="inline-label">Voor 12:30</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="1" id="radio_demo_inline_2" data-md-icheck />
                        <label for="radio_demo_inline_2" class="inline-label">Na 12:30</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="2" id="radio_demo_inline_3" data-md-icheck checked />
                        <label for="radio_demo_inline_3" class="inline-label">Specifieke tijd</label>
                    </span>

                     <p>
                        <input type="checkbox" name="voorrijkosten" value="1" id="checkbox_demo_3" data-md-icheck />
                        <label for="checkbox_demo_3"  class="inline-label">Voorrijkosten rekenen? (alleen geldig bij offerte opstellen)</label>
                    </p>
                </div>
            </div> 
        </div>
    </div>
</div>
@if(isset($offerte))
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-2 uk-width-medium-1-1">
        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                <table>
                    <h4>Selecteer de werkzaamheden van de offerte voor de afspraak.</h4>
                    <thead>
                        <th>
                            <td></td>
                            <td>Aantal</td>
                            <td>Werkactiviteit</td>
                            <td>Opmerking</td>
                            <td>Prijs</td>
                        </th>
                    </thead>
                    <tbody>
                        @foreach($ohas as $oha)
                            <tr>
                                <td><input type="checkbox" value="{{$oha->id}}" name="activiteiten[]" id="checkbox_demo_1[]" data-md-icheck/></td>
                                <td>{{$oha->aantal}}</td>
                                <td>{{$oha->activiteit->omschrijving}}</td>
                                <td>{{$oha->opmerking}}</td>
                                <td>€ {{number_format($oha->prijs,2)}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Totaal</td>
                            <td>€ {{number_format($offerte->totaalprijs, 2)}}</td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <h3 class="heading_a">Klusprijs</h3></br>
                <div class="uk-input-group">
                   <span class="uk-input-group-addon"><input type="checkbox" name="klusprijsAan" value="1" data-md-icheck/></span>
                   <label>Klusprijs</label>
                   <input type="number" step="0.01" name="klusprijs" class="md-input" />
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="notNull" value="{{$offerte->id}}" />
@endif

<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-2 uk-width-medium-1-1">
        <div class="uk-form-row">
            <div align="right">
                <div class="md-btn-group">
                    <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder</button>
                </div>
            </div>
        </div>
    </div>
</div>
                {!! Form::close() !!}


@endsection