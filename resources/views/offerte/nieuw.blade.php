@extends('layouts.master')

@section('title', 'Offerte maken')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor1');
        CKEDITOR.replace('editor2')
        CKEDITOR.replace('editor3')
    </script>

@endpush
@section('content')



@if(Auth::user()->rol != 99)

<h4 class="heading_a uk-margin-bottom">Offerte Opstellen</h4>

<div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
        {!! Form::open(array('url'=>'/offerte/nieuw/'.$relatie->id, 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
             <h3>Afspraak Informatie</h3>
            <div class="uk-width-large">
                <div class="uk-form-row">
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-2-4">
                            <input id="kUI_datepicker_a" @if(isset($afspraak)) value="{{date('d-m-Y', strtotime($afspraak->startdatum))}}" @endif name="datum" required/>
                            <span class="uk-form-help-block"> Opgesteld op:</span>
                        </div>
                        <div class="uk-width-large-2-4">
                            <select id="select_demo_5" name="werknemer_id" data-md-selectize data-md-selectize-bottom>
                                @foreach($werknemers as $werknemer)
                                <option value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                @endforeach
                            </select>
                            <span class="uk-form-help-block">Opgesteld Door:</span>
                        </div>                       
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin> 
                        <div class="uk-width-large">
                            <select id="select_demo_5" name="afspraak_id" data-md-selectize data-md-selectize-bottom>
                                @if(null !== $afspraken)
                                    @if(count($afspraken) == 1)
                                        @foreach($afspraken as $afspraak)
                                        <option value="{{$afspraak->id}}">Opvolging van afspraak {{$afspraak->id}} van {{$afspraak->startdatum}} - {{$afspraak->einddatum}}</option>
                                        @endforeach
                                        <option value="0">Hoort bij geen afspraak</option>
                                    @else
                                        <option value="0">Hoort bij geen afspraak</option>
                                        @foreach($afspraken as $afspraak)
                                        <option value="{{$afspraak->id}}">Opvolging van afspraak {{$afspraak->id}} van {{$afspraak->startdatum}} - {{$afspraak->einddatum}}</option>
                                        @endforeach
                                    @endif
                                @else
                                
                                    <option value="0">Hoort bij geen afspraak</option>
                                @endif
                            </select>
                            <span class="uk-form-help-block"> Betreft afspraak:</span>
                        </div>
                        <input type="hidden" name="verwerkt_door" value="{{Auth::user()->id}}"/>
                        <input type="hidden" name="klant_id" value="{{$relatie->id}}"/>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-1">
                            <label>Opmerking</label><br>
                            <textarea class="md-input label-fixed" id='editor1' placeholder="Schrijf hier de opmerking van de monteur" name="omschrijving"></textarea>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-1">
                            <label>Aanbieding</label><br>
                            <textarea class="md-input label-fixed" id='editor2' placeholder="Schrijf hier de aanbieding van de monteur, zoals 'binnen 7 dagen reactie is 5% korting'" name="aanbieding"></textarea>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-1">
                            <label>Garantievoorwaarden</label><br>
                            <textarea class="md-input label-fixed" id='editor3' placeholder="Beschrijf hier de garantie, zoals '10 jaar garantie op de werkzaamheden'" name="opmerking"></textarea>
                        </div>
                    </div>
                </div>
                @if(isset($voldaan))
                    <input type="hidden" name="voldaan" value="{{$voldaan}}">
                @endif
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-2-4">
                            <input id="kUI_datepicker_z" name="vervaldatum" required/>
                            <span class="uk-form-help-block"> Vervaldatum offerte:</span>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div align="right">
                        <div class="md-btn-group">
                            <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                            <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder</button>
                        </div>
                    </div>
                </div>
            </div>
         </div> 
        {!! Form::close() !!}
    </div>
</div>
   

@endif

@endsection

@push('scripts')

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>
@endpush