@extends('layouts.master')

@section('title', 'Offerte')
@push('scripts')

<script src="{{ URL::asset('assets/assets/js/pages/page_invoices.min.js')}}"></script>

@endpush
@section('content')


@if(Auth::user()->rol != 99)


<div class="md-card-toolbar hidden-print">
            <div class="md-card-toolbar-actions hidden-print">
                <i class="md-icon material-icons" id="invoice_print">&#xE8ad;</i>
                <!-- <div class="md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                </div> -->
            </div>
            <h3 class="md-card-toolbar-heading-text large" id="invoice_name">
                Offerte {{$offerte->id}}
            </h3>
        </div>
        <div class="md-card-content invoice_content print_bg invoice_footer_active">
            
                <div class="invoice_header md-bg-blue-A100">
                    <span class="uk-text-muted uk-text-small uk-text-italic">Offerte status per {{date('d-m-Y', strtotime($shf->updated_at))}}: {{$status->omschrijving}}</span>
                    <a class="uk-float-right"><span class="uk-text-muted uk-text-small uk-text-italic">LOGO ONDERNEMING HIER</span></a>
                </div>
            
            <div class="uk-margin-medium-bottom">
                
                <h3 class="heading_a uk-margin-bottom">Offerte {{$offerte->id}} </h3>
                
                <span class="uk-text-muted uk-text-small uk-text-italic">Offerte:</span> {{$offerte->datum}}
                <br/>
                <span class="uk-text-muted uk-text-small uk-text-italic">Vervaldatum:</span> <span class="uk-text-danger uk-text-bold">{{$offerte->vervaldatum}}</span>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-3-5">
                    <div class="uk-margin-bottom">
                        <span class="uk-text-muted uk-text-small uk-text-italic">Van:</span>
                        <address>
                            <p><strong>Bedrijfsnaam</strong></p>
                            <p>Adres Huisnummer</p>
                            <p>Postcode te Plaats</p>
                        </address>
                    </div>
                    <div class="uk-margin-medium-bottom">
                        <span class="uk-text-muted uk-text-small uk-text-italic">Aan:</span>
                        <address>
                            <p><strong>{{$relatie->voornaam}} {{$relatie->achternaam}}</strong></p>
                            <p>{{$relatie->straat}} {{$relatie->huisnummer}} {{$relatie->hn_prefix}}</p>
                            <p>{{$relatie->postcode}} te {{$relatie->woonplaats}}</p>
                        </address>
                    </div>
                </div>
                <div class="uk-width-small-2-5">
                    <span class="uk-text-muted uk-text-small uk-text-italic">Totaal:</span>
                    <!-- If over datum, rode tekst. -->
                    <p class="heading_b uk-text-success">€ {{number_format($offerte->totaalbedrag,2)}}</p>
                    <p class="uk-text-small uk-text-muted uk-margin-top-remove">Incl. BTW -
                        € {{number_format(round($offerte->totaalbedrag/1.21*.21,2),2)}}</p>
                </div>
            </div>
            <div class="uk-grid uk-margin-large-bottom">
                <div class="uk-width-1-1">
                    <table class="uk-table">
                        <thead>
                            <tr class="uk-text-upper">
                                <th>Activiteit</th>
                                <th>Tarief</th>
                                <th class="uk-text-center">Aantal</th>
                                <th class="uk-text-center">(incl. BTW)</th>
                                <th class="uk-text-center">Totaal</th>
                            </tr>
                        </thead>
                        <tbody>
                        <!-- Voor elke activiteit -->
                        <tr class="uk-table-middle">
                        	@for($i = 0; $i < count($offerteregels);$i++)
                                <td>
                                    <span class="uk-text-large">{{$offerteregels[$i]['omschrijving']}}</span><br/>
                                    <span class="uk-text-muted uk-text-small">{{$offerteregels[$i]['eenheid']}}</span>
                                </td>
                                <td>
                                    € {{number_format($offerteregels[$i]['prijs'],2)}}
                                </td>
                                <td class="uk-text-center">
                                    {{$offerteregels[$i]['aantal']}}
                                </td>
                                <td class="uk-text-center">
                                    (€ {{number_format($offerteregels[$i]['btw'],2)}})
                                </td>
                                <td class="uk-text-center">
                                    € {{number_format($offerteregels[$i]['totaalregel'],2)}}
                                </td>
                            </tr>
                            @endfor
                        <!-- eind -->
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <span class="uk-text-muted uk-text-small uk-text-italic">Opmerking</span>
                    <p class="uk-margin-top-remove">
                        {{$offerte->opmerking}}
                    </p>
                </div>
            </div>
           	
            <div class="invoice_footer">
                Bedrijf<span>&middot;</span>adres,  1012 AA te Amsterdam<br>
                </span>06-12345678<span>&middot;</span>Mail            </div>
            
        </div>


@endif


@endsection