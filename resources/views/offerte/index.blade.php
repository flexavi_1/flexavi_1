@extends('layouts.master')



@section('title', 'Offerte overzicht')

@push('scripts')



    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>

    <!-- datatables buttons-->

    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>

    

    <!-- datatables custom integration -->

    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>



    <!--  datatables functions -->

    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>



@endpush

@section('content')







@if(Auth::user()->rol != 99)

<div class="uk-grid" data-uk-grid-margin>

    <div class="uk-width-medium-1-3">

        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">

            <button class="md-btn">Zoek op Status<i class="material-icons">&#xE315;</i></button>

            <div class="uk-dropdown">

                <ul class="uk-nav uk-nav-dropdown">

                    @foreach($offertetypes as $status)

                    <li><a href="/offerte/zoeken/{{$status->id}}">{{$status->omschrijving}}</a></li>

                    @endforeach

                    <li><a href="/offerte">Totaal Overzicht</a></li>

                </ul>

            </div>

        </div>

    </div>

</div> 

<h4 class="heading_a uk-margin-bottom">Offerte Overzicht</h4>

    <div class="md-card uk-margin-medium-bottom">

        <div class="md-card-content">

            <div class="dt_colVis_buttons"></div>

            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">

                <thead>

                    <tr>

                        <th width="10%">Off. Nr.</th>

                        <th width="10%">Klant</th>

                        <th width="10%">Opgesteld Door:</th>

                        <th width="15%">Offerte status</th>

                        <th width="15%">Vervaldatum</th>

                        <th width="10%">Totaalbedrag</th>

                        <th width="30%">Functies</th>

                    </tr>

                </thead>

                <tbody>

        

                    @foreach($offerten as $offerte)

                        <tr>

                            <td width="10%"><a href="/offerte/{{$offerte->offerte_id}}">{{$offerte->offerte_id}}</a></td>

                            <td width="10%"><a href="/relatie/{{$offerte->klant_id}}">{{$offerte->achternaam}}</a></td>

                            <td width="10%"><a href="/relatie/{{$offerte->klant_id}}">{{$offerte->werknemer_id->voornaam." ".$offerte->werknemer_id->achternaam}}</a></td>

                            <td width="20%"><a href="/offerte/{{$offerte->offerte_id}}">{{$offerte->status_id}}: {{$offerte->omschrijving}}</a></td>

                            <td width="15%"><a href="/offerte/{{$offerte->offerte_id}}">{{$offerte->vervaldatum}}</a></td>

                            <td width="15%"><a href="/offerte/{{$offerte->offerte_id}}">€ {{number_format($offerte->totaalbedrag, 2)}}</a></td>

                            <td width="30%">
                            	<?php 
                            		$typeString = (isset($type)) ? '/'.$type : "";
                            	?>
                                <a href="/offerte/{{$offerte->offerte_id}}/honoreren{{$typeString}}" class="md-btn md-btn-success md-btn-wave-light">Gehonoreerd</a>

                                <a href="/offerte/{{$offerte->offerte_id}}/vervallen{{$typeString}}" class="md-btn md-btn-danger md-btn-wave-light">Vervallen</a>

                            </td>

                        </tr>

                    @endforeach

                </tbody>

            </table>

        </div>

    </div>





@endif



@endsection