@extends('layouts.master')

@section('title', 'Afspraak maken')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <script src="{{ URL::asset('assets/bower_components/handlebars/handlebars.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/handlebars_helpers.min.js')}}"></script>

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


@endpush
@section('content')

@if(isset($danger))
   <div class="md-card md-card-collapsed">
        <div class="md-card-toolbar md-bg-red-400 uk-text-contrast">
            <h3 class="md-card-toolbar-heading-text ">
                {{$danger}}
            </h3>
        </div>
    </div>
@endif

@if(Auth::user()->rol != 99)

 <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large">
        <div class="md-card">
            <div class="md-card-content">
            <h3 class="heading_a">Werkzaamheden Wijzigen</h3>
            {{-- {{dd($ohas)}} --}}
                {!! Form::open(array('url'=>'/relatie/'.$offerte->klant_id.'/offerte/'.$relatie->id.'/activiteit/wijzigen', 'method' => 'post')) !!}
                
                <div class="uk-grid uk-grid-medium" data-uk-grid-match>
                @foreach($ohas as $oha)
                    <div class="uk-width-10-10">
                        <div class="uk-grid">
                            <div class="uk-width-2-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Werkzaamheden</span>
                                    <select id="d_form_select_activity" name="activiteiten[]" data-md-selectize required>
                                        @foreach($activiteiten as $activiteit)
                                            @if($activiteit->id == $oha->activiteit_id)
                                                <option value="{{$activiteit->id}}">{{$activiteit->omschrijving}}, {{$activiteit->prijs}} {{$activiteit->eenheid}} | {{$activiteit->beschrijving}}</option>
                                            @endif
                                        @endforeach

                                        @foreach($activiteiten as $activiteit)
                                            @if($activiteit->id != $oha->activiteit_id)
                                                <option value="{{$activiteit->id}}">{{$activiteit->omschrijving}}, {{$activiteit->prijs}} {{$activiteit->eenheid}} | {{$activiteit->beschrijving}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Opmerking</span>
                                    <input type="text" class="md-input label-fixed" id="d_form_amount" name="opmerking[]" value="{{$oha->opmerking}}">
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Aantal</span>
                                    <input type="number" step="0.1" min="0.1" class="md-input label-fixed" name="aantal[]" value="{{$oha->aantal}}" required>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Prijs per eenheid</span>
                                    <input type="number" value="{{$oha->prijs}}" step="0.01" class="md-input label-fixed" id="d_form_amount" name="prijs[]" required>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Korting in Euro</span>
                                    <input type="number" value="{{$oha->kortingsbedrag}}" step="0.01" class="md-input label-fixed" id="d_form_amount" name="korting[]" required>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
                <br />
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-2-4">
                            <input type="checkbox" name="werkzaamheden" value="1" id="checkbox_demo_1 test" data-md-icheck />
                            <label for="checkbox_demo_1 test" name="updateWork" class="inline-label">Meer werkzaamheden toevoegen?</label>
                        </div>
                    </div>
                </div>
                <br />
                 <p>
                    <input type="checkbox" name="inc" value="1" id="checkbox_demo_3" data-md-icheck checked />
                    <label for="inc"  class="inline-label">Bedragen zijn inclusief?</label>
                </p>
                 <p><em>N.B. Bij leeglaten van de prijs, wordt de standaardprijs gehanteerd.</em></p>
                 <p><em>N.B. Indien een activiteit als service uitgevoerd wordt, dient de korting het volledige bedrag te zijn.</em></p>
                 <br>
                    <input type="hidden" name="offerte_id" value="{{$offerte->id}}"/>
                    <br><p></p>
                <div class="uk-grid">
                    <div class="uk-width-1-1" align="right">
                        <button type="reset" href="#" class="md-btn md-btn-warning">Reset</button>
                        <button type="submit" href="#" class="md-btn md-btn-success">Wijzig toegevoegde werkzaamheden</button>
                    </div>
                </div>
                {!! Form::close() !!}
            {{-- <a href="/offerte/{{$offerte->id}}/activiteit/nieuw">Nieuwe activiteit toevoegen</a> --}}
            </div>
        </div>
    </div>
</div>

 <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large">
        <div class="md-card">
            <div class="md-card-content">
                <h4 class="heading_a">Huidige werkzaamheden</h4>
                <div class="uk-overflow-container">
                    <table class="uk-table uk-text-nowrap">
                        <thead>
                            <tr>
                                <th>Aantal</th>
                                <th>Activiteit</th>
                                <th>Prijs per eenheid</th>
                                <th>Totale korting</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ohas as $o)
                            <tr>
                                <td>{{$o->aantal}}</td>
                                <td>{{$o->activiteit->omschrijving}}</td>
                                <td>{{number_format($o->prijs,2)}}</td>
                                <td>{{number_format($o->kortingsbedrag,2)}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endif



@endsection