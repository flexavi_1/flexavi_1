@extends('layouts.master')

@section('title', 'Offerte Verwerken - Extra')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush
@section('content')

<h2 class="heading_b uk-margin-bottom">Offerte werkzaamheden verwerken {{$relatie->voornaam." ".$relatie->achternaam}}</h2>
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-2 uk-width-medium-1-1">
        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                {!! Form::open(array('url'=>'/offerte/afspraak/'.$afspraak->id.'/verwerken', 'method' => 'post', 'data-parsley-validate')) !!}
                <div class="uk-grid" data-uk-grid-margin>
                    @foreach($ohas as $oha)
                    <div class="uk-width-9-10">
                        <div class="uk-grid">
                            <div class="uk-width-2-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Werkzaamheden</span>
                                    <select id="d_form_select_activity" name="activiteiten[]" data-md-selectize required>
                                        @foreach($activiteiten as $activiteit)
                                        @if($activiteit->id == $oha->activiteit_id)
                                        <option value="{{$activiteit->id}}">{{$activiteit->omschrijving}}, {{$activiteit->prijs}} {{$activiteit->eenheid}}</option>
                                        @endif
                                        @endforeach
                                        @foreach($activiteiten as $activiteit)
                                        @if($activiteit->id != $oha->activiteit_id)
                                        <option value="{{$activiteit->id}}">{{$activiteit->omschrijving}}, {{$activiteit->prijs}} {{$activiteit->eenheid}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Opmerking</span>
                                    <input type="text" class="md-input label-fixed" id="d_form_amount" name="opmerking[]" value="{{$oha->opmerking}}" required>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Aantal</span>
                                    <input type="number" step="0.1" value="{{$oha->aantal}}" min="0.1" class="md-input label-fixed" id="d_form_amount" name="aantal[]" required>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Prijs per eenheid</span>
                                    <input type="number" value="{{$oha->prijs}}" value='0.00' step="0.01" class="md-input label-fixed" id="d_form_amount" name="prijs[]" required>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Korting in Euro</span>
                                    <input type="number" value="{{$oha->kortingsbedrag}}" value='0.00' step="0.01" class="md-input label-fixed" id="d_form_amount" name="korting[]" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div data-dynamic-fields="field_template_b"></div>
                    <script id="field_template_b" type="text/x-handlebars-template">
                    
                    <div class="uk-grid uk-grid-medium form_section form_section_separator" id="d_form_row" data-uk-grid-match>
                        <div class="uk-width-9-10">
                            <div class="uk-grid">
                                <div class="uk-width-2-3">
                                    <div class="parsley-row">
                                        <span class="uk-form-help-block">Werkzaamheden</span>
                                        <select id="d_form_select_activity" name="activiteiten[]" data-md-selectize required>
                                            @foreach($activiteiten as $activiteit)
                                            <option value="{{$activiteit->id}}">{{$activiteit->omschrijving}}, {{$activiteit->prijs}} {{$activiteit->eenheid}} | {{$activiteit->beschrijving}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="uk-width-1-3">
                                    <div class="parsley-row">
                                        <span class="uk-form-help-block">Opmerking</span>
                                        <input type="text" class="md-input label-fixed" id="d_form_amount" name="opmerking[]" required>
                                    </div>
                                </div>
                                <div class="uk-width-1-3">
                                    <div class="parsley-row">
                                        <span class="uk-form-help-block">Aantal</span>
                                        <input type="number" step="0.1" min="0.1" class="md-input label-fixed" id="d_form_amount" name="aantal[]" required>
                                    </div>
                                </div>
                                <div class="uk-width-1-3">
                                    <div class="parsley-row">
                                        <span class="uk-form-help-block">Prijs per eenheid</span>
                                        <input type="number" value='0.00' step="0.01" class="md-input label-fixed" id="d_form_amount" name="prijs[]" required>
                                    </div>
                                </div>
                                <div class="uk-width-1-3">
                                    <div class="parsley-row">
                                        <span class="uk-form-help-block">Korting in Euro</span>
                                        <input type="number" value='0.00' step="0.01" class="md-input label-fixed" id="d_form_amount" name="korting[]" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-10 uk-text-center">
                            <div class="uk-vertical-align uk-height-1-1">
                                <div class="uk-vertical-align-middle">
                                    <a href="#" class="btnSectionClone" data-section-clone="#d_form_section"><i class="material-icons md-36">&#xE146;</i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                     </script>

                     <p><em>N.B. Bij leeglaten van de prijs, wordt de standaardprijs gehanteerd.</em></p>
                     <p><em>N.B. Indien een activiteit als service uitgevoerd wordt, dient de korting het volledige bedrag te zijn.</em></p>
                     <br>
                        <input type="hidden" name="offerte_id" value="{{$offerte->id}}"/>
                        <input type="hidden" name="voldaan" value="{{$voldaan}}"/>
                    <br><p></p>
                <div class="uk-grid">
                    <div class="uk-width-1-1" align="right">
                        <button type="reset" href="#" class="md-btn md-btn-warning">Reset</button>
                        <button type="submit" href="#" class="md-btn md-btn-success">Voeg werkzaamheden Toe</button>
                    </div>
                </div>
                {!! Form::close() !!}


@endsection