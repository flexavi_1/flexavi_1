@extends('layouts.master')

@section('title', 'Factuur overzicht')

@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush
@section('content')



@if(Auth::user()->rol != 99)

<h4 class="heading_a uk-margin-bottom">Factuur Overzicht</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Fact. Nr.</th>
                        <th>Klant</th>
                        <th>Factuurstatus</th>
                        <th>Betaaldatum</th>
                        <th>Bedrag</th>
                        <th>Betaald</th>
                        <th>Functies</th>
                    </tr>
                </thead>
                <tbody>

                    @if($facturen != "")
                    @foreach($facturen as $factuur)
                        <tr>
                            <td>{{$factuur->factuur_id}}</td>
                            <td><A HREF="/relatie/{{$factuur->klant_id}}">{{$factuur->voornaam}} {{$factuur->achternaam}}</A></td>
                            <td><A HREF="/relatie/{{$factuur->klant_id}}">{{$factuur->omschrijving}}</A></td>
                            <td><A HREF="/relatie/{{$factuur->klant_id}}">{{$factuur->betaaldatum}}</A></td>
                            <td><a href="/relatie/{{$factuur->klant_id}}">€ {{number_format($factuur->totaalbedrag,2)}}</a></td>
                            <td><A HREF="/relatie/{{$factuur->klant_id}}">
                                @if($factuur->betaaldatum >= date('d-m-Y') && $factuur->voldaan == 0)
                                    <span class="uk-text-danger">Betaaltermijn verstreken!</span>
                                @elseif($factuur->voldaan == 1)
                                    <span class="uk-text-success">Betaald</span>
                                @else
                                    <span class="uk-text-warning">Betaling in afwachting</span>
                                @endif</A>
                            </td>
                            <td>
                                @if($factuur->voldaan == 0)
                                <a href="/factuur/{{$factuur->factuur_id}}/betaald" class="md-btn md-btn-success md-btn-wave-light">Betaald</a>
                                    @if($factuur->email != null)
                                    @for($i = 0; $i < count($dagenopen);$i++)
                                        @if($dagenopen[$i]['factuur_id'] == $factuur->factuur_id)
                                            @if($dagenopen[$i]['dagenopen'] >= 14 && ($factuur->status_id == 8 || $factuur->status_id == 10))
                                            <a href="/factuur/{{$factuur->factuur_id}}/eerste" class="md-btn md-btn-danger md-btn-wave-light">*Mee bezig*Verstuur eerste brief</a>
                                            @elseif($dagenopen[$i]['dagenopen'] >= 28 && $factuur->status_id == 11)
                                            <a href="/factuur/{{$factuur->factuur_id}}/tweede" class="md-btn md-btn-danger md-btn-wave-light">*Mee bezig*Verstuur tweede brief</a>
                                            @elseif($dagenopen[$i]['dagenopen'] >= 42 && $factuur->status_id == 12)
                                            <a href="/factuur/{{$factuur->factuur_id}}/derde" class="md-btn md-btn-danger md-btn-wave-light">*Mee bezig*Verstuur aanmaning</a>
                                            @elseif($dagenopen[$i]['dagenopen'] >= 56 && $factuur->status_id == 13)
                                            <a href="/factuur/{{$factuur->factuur_id}}/vierde" class="md-btn md-btn-danger md-btn-wave-light">*Mee bezig*Verstuur ingebrekestelling</a>
                                            @endif
                                        @endif
                                    @endfor
                                    @else
                                    Zonder Email kan er geen automatische brief verstuurd worden.
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-3">
        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">
            <button class="md-btn">Zoek op Betaald<i class="material-icons">&#xE315;</i></button>
            <div class="uk-dropdown">
                <ul class="uk-nav uk-nav-dropdown">
                    @if(isset($open))
                    <li><a href="/debiteur">Alle facturen</a></li>
                    @else
                    <li><a href="/debiteur/open">Openstaande facturen</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div> 

@endif

@endsection