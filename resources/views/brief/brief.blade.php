@extends('layouts.master')

@section('title', 'Brief inzien')
@push('scripts')

@endpush
@section('content')

@if(Auth::user()->rol < 99 )
<h2 class="heading_b uk-margin-bottom">Inzage Brief</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                {!!$vft->brief!!}
            </div>
        </div>
    </div>
</div>

@endif
@endsection