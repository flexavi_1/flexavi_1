@extends('layouts.master')

@section('title', 'Brieven wijzigen')
@push('scripts')
    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
<?php $count = count($brieven); ?>
<script>
	//declare all editors
	for(var i = 0; i < {{$count}}; i++){

        CKEDITOR.replace('editor'+i);	
	}

</script>


@endpush
@section('content')

@if(Auth::user()->rol < 99 )
<h2 class="heading_b uk-margin-bottom">Standaard brieven wijzigen</h2>
<em><b>Opmerking: </b>Alle woorden die omringd zijn met een # (bijvoorbeeld, #achternaam#) worden automatisch aangevuld door het systeem. Het wijzigen van deze woorden, zorgt ervoor dat het systeem dat woord niet meer kan veranderen. Bij het alsnog wijzigen van deze woorden, dient u contact op te nemen met uw systeembeheerder.</em>
<br><br>
{!! Form::open(array('url'=>'/vijftieneuro/wijzig/brieven', 'method' => 'post', 'data-parsley-validate')) !!}
@for($i = 0; $i < count($brieven); $i++)

	<div class="md-card uk-margin-large-bottom">
	    <div class="md-card-content">
	         <div class="uk-grid" data-uk-grid-margin>
		        <h3>{{$brieven[$i]->naam}}</h3>
	            <div class="uk-width-medium">
	            	<div class="uk-form-row">
	                	<div class="uk-grid" data-uk-grid-margin>
	                		<div class="uk-width-large-1-1">
	                            <label>Brief</label><br>
	                			<textarea class="md-input label-fixed" name="{{$brieven[$i]->naam}}" id="editor{{$i}}">{{$brieven[$i]->brief}}</textarea>
	                		</div>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endfor


<div align="right">
	<div class="md-btn-group">
    	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
        <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Wijzig brieven</button>
	</div>
</div>
{!! Form::close() !!}


@endif
@endsection