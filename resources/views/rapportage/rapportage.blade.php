@extends('layouts.master')

@section('title', 'Rapportage')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>    <!-- kendo UI -->
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol < 6)

<h2 class="heading_b uk-margin-bottom">Rapportage opvragen</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
        {!! Form::open(array('url'=>'/rapportage', 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
             <h3>Overzichten</h3>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-2">
                            <label for="kUI_datepicker_a" class="uk-form-label">*Startdatum:</label>
                            <input id="kUI_datepicker_a" name="startdatum" required/>
                        </div>
                        <div class="uk-width-large-1-2">
                            <label for="kUI_datepicker_z" class="uk-form-label">Einddatum:</label>
                            <input id="kUI_datepicker_z" name="einddatum" />
                        </div>
                    </div>
                </div>
                <div class="uk-form-row" align="left">
                    <div class="uk-grid" align="left"  data-uk-grid-margin>
                        <div class="uk-width-large" align="left">
                                <span class="uk-input-group-addon"><input  type="checkbox" name="startToEnd" value="1" data-md-icheck/> <br><span>Alleen startdatum tonen?</span>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="ük-grid" data-uk-grig-margin>
                        <div class="uk-width-large-1-1">
                                <span class="uk-input-group-addon"><input type="checkbox" name="aggregate" value="1" data-md-icheck/>  <br><span>Cijfers samenvoegen?</span></span>
                        </div>
                    </div>
                </div>
                <br />
                <p>
                    <em>NB: <br>Bij het <b>NIET</b> selecteren van het samenvoegen, wordt er een pagina buiten het systeem om geopend. Dit heeft te maken met de grootte van de (eventuele) rapportage <br>
                    Bij het <b>NIET</b> aanvinken van 'Periode van startdatum tot en met einddatum',  wordt het einddatum veld genegeerd.
                    <br> Eveneens worden de selectievelden genegeerd wanneer alleen een startdatum ingevuld is.</em>
                </p>
                    <div class="uk-form-row">
                        <div align="right">
                            <div class="md-btn-group">
                                <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        {!! Form::close() !!}
    </div>
</div>

@endif

@endsection