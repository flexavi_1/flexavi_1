<html lang="nl">

<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="{{ URL::asset('assets/image/png')}}" href="{{ URL::asset('assets/assets/img/favicon-16x16.png')}}" sizes="16x16">
    <link rel="icon" type="{{ URL::asset('assets/image/png')}}" href="{{ URL::asset('assets/assets/img/favicon-32x32.png')}}" sizes="32x32">

    <link rel="stylesheet" href="{{ URL::asset('assets/assets/css/nlvw_factuur.css')}}" media="all">

    <title>Offerte | Flexavi</title>

    <!-- additional styles for plugins -->
        <!-- weather icons -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/weather-icons/css/weather-icons.min.css')}}" media="all">
    <!-- metrics graphics (charts) -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/metrics-graphics/dist/metricsgraphics.css')}}">
    <!-- chartist -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/chartist/dist/chartist.min.css')}}">
    
    <!-- uikit -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/uikit/css/uikit.almost-flat.min.css')}}" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/icons/flags/flags.min.css')}}" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/css/style_switcher.min.css')}}" media="all">
    
    <!-- altair admin -->{{-- 
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/css/main.min.css')}}" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/css/themes/themes_combined.min.css')}}" media="all"> --}}

    <link rel="stylesheet" href="{{ URL::asset('assets/assets/icons/material-design-icons/material-icons.css')}}" media="all">
    <!-- kendo UI -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/kendo-ui/styles/kendo.common-material.min.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/kendo-ui/styles/kendo.material.min.css')}}" id="kendoCSS"/>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
        <link rel="stylesheet" href="assets/css/ie.css" media="all">
    <![endif]-->


</head>
<body>
	{{-- {{dd($bedrijf)}} --}}

<div id="heading">

	<div class="padding-10">
		<div class="hidden-print">
		    <div class="hidden-print" style="">
		        <i class="md-icon material-icons" onclick="window.print()" id="invoice_print">&#xE8ad;</i>
		        <!-- <div class="md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
		        </div> -->
		    </div>
		    <h3 class="md-card-toolbar-heading-text large" id="invoice_name" style="margin-top: -20px; margin-left: 50px; color: #fff;">
		        Offerte | Status: {{$status->omschrijving}}
		    </h3>
		</div>
		
	</div>

</div>

<div class="md-card-content invoice_content print_bg invoice_footer_active">
	<br />
	<div class="invoice_header">
		<table>
			<tr>
			
				<td width="30%"> 
					@if(isset($bedrijf->logo)) 
						<img src="{{ URL::asset($bedrijf->logo)}}" width="100%" height="100%" /> 
					@else Geen logo bekend @endif </td>
				<td style="text-align: right">
					<strong>Offerte</strong>
					<br />
					<br />
					<span>Datum : {{date('d-m-Y', strtotime($offerte->datum))}}</span>
					<br />
					<span>offerte nr. :{{$offerte->id}}</span>
					<div style="clear: both;"></div>
					<ul class="contact">
						<li>{{$bedrijf->bankrekening}}</li>
						<li>KvK. {{$bedrijf->kvk}}</li>
						<li>{{$bedrijf->plaats}}</li>
						<li>{{$bedrijf->postcode}}</li>
						<li>{{$bedrijf->adres." ".$bedrijf->huisnummer}}</li>
						
						<li>BTW. {{$bedrijf->btwnummer}}</li>
						<li>{{$bedrijf->tel1}}</li>
						<li>{{$bedrijf->tel2}}</li>
						<li>{{$bedrijf->website}}</li>
						<li>{{$bedrijf->email}}</li>
					</ul>

				</td>
				
			</tr>
		
		</table>
	</div>
	<br>
	<br /><br />
	<table width="100%" id="gegevens">
	
		<tr>
			<td width="27%" height="24px;" style="border-bottom: 1px dotted #000;">Naam :</td>
			<td width="73%" height="24px;" style="border-bottom: 1px dotted #000;">{{$relatie->voornaam." ".$relatie->achternaam}}</td>
		</tr>
	
		<tr>
			<td width="27%" height="24px;" style="border-bottom: 1px dotted #000;">Straat + Huis nr. :</td>
			<td width="73%" height="24px;" style="border-bottom: 1px dotted #000;">{{$relatie->straat." ".$relatie->huisnummer.$relatie->hn_prefix}}</td>
		</tr>
	
		<tr>
			<td width="27%" height="24px;" style="border-bottom: 1px dotted #000;">Postcode + Woonplaats :</td>
			<td width="73%" height="24px;" style="border-bottom: 1px dotted #000;">{{$relatie->postcode}} {{$relatie->woonplaats}}</td>
		</tr>
	
		<tr>
			<td width="27%" height="24px;" style="border-bottom: 1px dotted #000;">Telefoon nr. :</td>
			<td width="73%" height="24px;" style="border-bottom: 1px dotted #000;">{{$relatie->telefoonnummer_prive}}</td>
		</tr>
	
		<tr>
			<td width="27%" height="24px;" style="border-bottom: 1px dotted #000;">Email :</td>
			<td width="73%" height="24px;" style="border-bottom: 1px dotted #000;">{{$relatie->email}}</td>
		</tr>
	
	</table>
	
	<div style="clear: both"></div>
	<br />
	<br />
	<table width="100%">
	
		<tr>
		
			<td width="80%">
				<strong>Opmerking Dakdekker ({{$verkoper->voornaam}}):</strong><br />
				<br />
				{{$offerte->omschrijving}}
			</td>
		
			<td width="20%" style="border-left: 1px dotted #000; text-align: center">
				<strong>Betalingswijze:</strong><br />
				{{$payment}}
				<br />
				<br />
				{{$offerte->opmerking}}
			</td>
		
		</tr>
	
	</table>
	
	<br /><br />
	
	<table width="100%" id="offerte">
	
		<tr>
			<td width="5%" style="text-align: center; height: 50px; border-bottom: 1px solid #000;"><strong>AANTAL</strong></td>
			<td width="70%" style="text-align: center; border-bottom: 1px solid #000;"><strong>BESCHRIJVING</strong></td>
			<td width="10%" style="text-align: center; border-bottom: 1px solid #000;"><strong>PRIJS PER EENHEID</strong></td>
			<td width="15%" style="text-align: center; border-bottom: 1px solid #000;"><strong>TOTAAL</strong></td>
		</tr>
		<?php $totaalbtw = 0.00; ?>
		@foreach($offerteregels as $fr)
		<tr>
			<td style="text-align: center; border-bottom: 1px solid #000;">{{$fr->aantal}} 
				{{$fr->eenheid}}</td>
			<td style="border-bottom: 1px solid #000;padding-top: 10px; padding-bottom: 10px">
				<strong>{{$fr->omschrijving}}</strong><br />
				{{$fr->beschrijving}} @if($fr->opmerking != '' && $fr->opmerking != null) ({{$fr->opmerking}}) @endif
			</td>
			<td style="text-align: center;border-bottom: 1px solid #000;">€ {{number_format($fr->prijs - ($fr->kortingsbedrag / $fr->aantal),2)}}</td>
			<?php
				$subUnit = $fr->prijs - ($fr->kortingsbedrag / $fr->aantal);
				$subTotal = $subUnit * $fr->aantal;
			?>
			<td style="text-align: center;border-bottom: 1px solid #000;">€ {{number_format($subTotal, 2)}}</td>
		</tr>
		<?php $totaalbtw += $fr->btw; ?>
		@endforeach
		
		<tr>
			<td></td>
			<td></td>
			<td style="text-align: center;border-bottom: 1px solid #000;padding-top: 10px; padding-bottom: 10px">
				SUBTOTAAL
			</td>
			<td style="text-align: center;border-bottom: 1px solid #000;padding-top: 10px; padding-bottom: 10px">
				
					€ {{number_format($offerte->totaalbedrag - $totaalbtw, 2)}}
			</td>
		</tr>
		
		<tr>
			<td></td>
			<td></td>
			<td style="text-align: center;border-bottom: 1px solid #000;padding-top: 10px; padding-bottom: 10px">
				BTW
			</td>
			<td style="text-align: center;border-bottom: 1px solid #000;padding-top: 10px; padding-bottom: 10px">
				
					€ {{number_format($totaalbtw, 2)}}
				
			</td>
		</tr>
		
		<tr>
			<td> </td>
			<td>{{$offerte->aanbieding}}</td>
			<td style="text-align: center;border-bottom: 1px solid #000;padding-top: 10px; padding-bottom: 10px">
				TOTAAL
			</td>
			<td style="text-align: center;border-bottom: 1px solid #000;padding-top: 10px; padding-bottom: 10px">
				€ {{number_format($offerte->totaalbedrag, 2)}}
			</td>
		</tr>
	
	</table>
	<div class="invoice_footer">
		<table width="100%">

			<tr>
			
				<td style="text-align:center"><br /><br />Wij willen u bedanken voor het vertrouwen in {{$bedrijf->naam}}<br /><br /></td>
			
			</tr>

			<tr>
			
				<td style="text-align:center">
				
				<ul class="contact2" style="margin-left: 50px">
					<li>{{$bedrijf->bankrekening}}</li>
					<li>KvK. {{$bedrijf->kvk}}</li>
					<li>{{$bedrijf->plaats}}</li>
					<li>{{$bedrijf->postcode}}</li>
					<li>{{$bedrijf->adres." ".$bedrijf->huisnummer}}</li>
					
					<li>BTW. {{$bedrijf->btwnummer}}</li>
					<li>{{$bedrijf->tel1}}</li>
					<li>{{$bedrijf->tel2}}</li>
					<li>{{$bedrijf->website}}</li>
					<li>{{$bedrijf->email}}</li>
				</ul>
				
				</td>
			
			</tr>

		</table>

		<br />
	</div>
</div>

<script src="{{ URL::asset('assets/assets/js/pages/page_invoices.min.js')}}"></script>
    <!-- common functions -->
    <script src="{{ URL::asset('assets/assets/js/common.min.js')}}"></script>
    <!-- uikit functions -->
    <script src="{{ URL::asset('assets/assets/js/uikit_custom.min.js')}}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ URL::asset('assets/assets/js/altair_admin_common.min.js')}}"></script>
</body>
</html>