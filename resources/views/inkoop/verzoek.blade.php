@extends('layouts.master')

@section('title', 'Inkoopverzoek plaatsen')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <script src="{{ URL::asset('assets/bower_components/handlebars/handlebars.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/handlebars_helpers.min.js')}}"></script>

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

 <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large">
        <div class="md-card">
            <div class="md-card-content">
            <h3 class="heading_a">Inkoopverzoek plaatsen</h3>
                {!! Form::open(array('url'=>'/voorraad/plaats-verzoek', 'method' => 'post')) !!}

                <div data-dynamic-fields="field_template_b"></div>
                <script id="field_template_b" type="text/x-handlebars-template">
                
                <div class="uk-grid uk-grid-medium form_section form_section_separator" id="d_form_row" data-uk-grid-match>
                    <div class="uk-width-9-10">
                        <div class="uk-grid">
                            <div class="uk-width-2-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Materiaal</span>
                                    <select id="d_form_select_activity" name="materiaal_id[]" data-md-selectize required>
                                        @foreach($materialen as $activiteit)
                                        <option value="{{$activiteit->id}}">{{$activiteit->naam}}, {{$activiteit->omschrijving}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Aantal</span>
                                    <input type="number" step="0.1" min="0.1" data-uk-tooltip="{pos:'top'}" title="Het aantal in dozen/pakketten." class="md-input label-fixed" id="d_form_amount" name="aantal[]" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-10 uk-text-center">
                        <div class="uk-vertical-align uk-height-1-1">
                            <div class="uk-vertical-align-middle">
                                <a href="#" class="btnSectionClone" data-section-clone="#d_form_section"><i class="material-icons md-36">&#xE146;</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                 </script>
                 <hr>
                <div class="uk-grid">
                    <div class="uk-width-1-1" align="right">
                        <button type="reset" href="#" class="md-btn md-btn-warning">Reset</button>
                        <button type="submit" href="#" class="md-btn md-btn-success">Voeg Verzoek Toe</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


@endif

@endsection