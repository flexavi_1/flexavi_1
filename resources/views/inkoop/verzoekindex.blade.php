@extends('layouts.master')

@section('title', 'Inkoop Verzoeken')


@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)

<h4 class="heading_a uk-margin-bottom">Inkoopverzoeken</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <div class="dt_colVis_buttons"></div>
                <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Datum</th>
                        <th>Geplaatst door</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($verzoeken as $verzoek)
                    <tr>
                        <td><a href="/verzoek/{{$verzoek->id}}">{{date('d-m-Y', strtotime($verzoek->datum))}}</td>
                        <td>{{$verzoek->gebruiker_id->name}}</td>
                        <td> {{$verzoek->status_id->omschrijving}} </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-3">
        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">
            <button class="md-btn">Zoek per Status <i class="material-icons">&#xE315;</i></button>
            <div class="uk-dropdown">
                <ul class="uk-nav uk-nav-dropdown">
                    @foreach($statussen as $status)
                    @if($status->id < 27)
                    <li><a href="/voorraad/verzoek/status/{{$status->id}}">{{$status->omschrijving}}</a></li>
                    @endif
                    @endforeach
                    <li><a href="/voorraad/verzoek">Volledig Overzicht</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@endif

@endsection