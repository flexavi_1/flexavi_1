@extends('layouts.master')

@section('title', 'Inkoopfactuur verwerken')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <script src="{{ URL::asset('assets/bower_components/handlebars/handlebars.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/handlebars_helpers.min.js')}}"></script>

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

    <div class="uk-width-large">
        <div class="md-card">
            <div class="md-card-content">
            <h3 class="heading_a">Inkoopfactuur verwerken</h3>
            {!! Form::open(array('url'=>'/inkoop/verwerken/'.$relatie->id, 'method' => 'post')) !!}
            <hr>
            @foreach($prijslijst as $p)
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-3-5">
                        <label>Materiaal</label>
                        <input type="text" class="md-input label-fixed" name="namen[]" value="{{$p->materiaal->naam}} ({{$p->omschrijving}}) à € {{number_format($p->inkoopprijs,2)}} exclusief" disabled />
                        <input type="hidden" name="materiaal[]" value="{{$p->materiaal_id}}"/>
                    </div>
                    <div class="uk-width-medium-1-5">
                        <label>Aantal</label>
                        <input type="text" class="md-input label-fixed" name="aantal[]" value="0" />
                    </div>
                    <div class="uk-width-medium-1-5">
                        <label>Prijs exclusief</label>
                        <input type="text" class="md-input label-fixed" name="prijs[]" value="{{number_format($p->inkoopprijs,2)}}" />
                    </div>
                </div>
            @endforeach
            <br>
            <p>
                <input type="checkbox" name="betaald" id="checkbox_demo_1" data-md-icheck />
                <label for="betaald" class="inline-label">De factuur is betaald.</label>
            </p>
            <div class="uk-form-row">
                <div align="right">
                    <div class="md-btn-group">
                        <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                        <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>



@endif

@endsection
