@extends('layouts.master')

@section('title', 'Inkoopverzoek')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush
@section('content')
@if($inkoopverzoek->status->id == 24)
<br>
<a class="md-btn md-btn-danger md-btn-wave-light" href="/verzoek/{{$inkoopverzoek->id}}/25">Verzoek afkeuren</a>
<a class="md-btn md-btn-success md-btn-wave-light" href="/verzoek/{{$inkoopverzoek->id}}/26">Verzoek goedkeuren</a>
<br>
</br>
@endif
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-2 uk-width-medium-1-1">
        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <h3>Inkoopverzoek</h3>
                    <div class="uk-width-medium">
                        <table class="uk-table uk-text-nowrap uk-table-hover">
                            <thead>
                            </thead>
                            <tbody>
                               <tr>
                                    <td><b>Aangevraagd door:</b></td>
                                    <td>{{$inkoopverzoek->user->name}}</td>
                               </tr>
                               <tr>
                                    <td><b>Aangevraagd op:</b></td>
                                    <td>{{date('d-m-Y', strtotime($inkoopverzoek->datum))}}</td>
                               </tr>
                               <tr>
                                    <td><b>Status: </b></td>
                                    <td>{{$inkoopverzoek->status->omschrijving}}</td>
                               </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <h3>Gevraagde materialen</h3>
                    <div class="uk-width-medium">
                        <table class="uk-table uk-text-nowrap">
                            <thead>
                                <tr>
                                    <th>Type Materiaal</th>
                                    <th>Aantal</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($ihm as $i)
                                <tr>
                                    <td>{{$i->materiaal->naam}}</td>
                                    <td>{{$i->aantal}}</td>
                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-width-large-1-2 uk-width-medium-1-1">
        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <h3>Huidige voorraad </h3>
                    <div class="uk-width-medium">
                        <table class="uk-table uk-text-nowrap">
                            <thead>
                                <tr>
                                    <th>Type Materiaal</th>
                                    <th>Aantal</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($voorraad as $i)
                                <tr>
                                    <td>{{$i->naam}}</td>
                                    <td>{{$i->aantal}}</td>
                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection