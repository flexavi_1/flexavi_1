@extends('layouts.master')

@section('title', 'Zoekresultaat')
@push('scripts')

    <!-- jquery ui -->
    <script src="{{ URL::asset('assets/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <!-- jTable -->
    <script src="{{ URL::asset('assets/bower_components/jtable/lib/jquery.jtable.min.js')}}"></script>

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>
@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h4 class="heading_a uk-margin-bottom">Zoekresultaten voor "{{$a}}"</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
       
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Naam</th>
                		<th>Adres</th>
                		<th>Contactgegevens</th>
                		<th>Type Relatie</th>
                		<th>Verwerkt Door</th>
                        <th>Geschreven Door</th>
                        <th>Geschreven Op</th>
            		</tr>
            	</thead>
			    <tbody>
                    @foreach($klanten as $relatie)
                        <tr>
                            <td><a href="/relatie/{{$relatie->id}}">{{$relatie->voornaam}} {{$relatie->achternaam}}</a></td>
                            <td><a href="/relatie/{{$relatie->id}}">{{$relatie->straat}} {{$relatie->huisnummer}} {{$relatie->hn_prefix}}, <br> {{$relatie->postcode}} {{$relatie->woonplaats}}</a></td>
                            <td><a href="/relatie/{{$relatie->id}}">{{$relatie->telefoonnummer_prive}} @if($relatie->telefoonnummer_zakelijk != "") || {{$relatie->telefoonnummer_zakelijk}} @endif @if($relatie->email != "") <br>{{$relatie->emailString}} @endif</a></td>
                            <td><a href="/relatie/{{$relatie->id}}">{{$relatie->klanttype_id->omschrijving}}</a></td>
                            
                            <td><a href="/relatie/{{$relatie->id}}">@if(!is_numeric($relatie->geschreven_door) && $relatie->geschreven_door != null) {{$relatie->geschreven_door->voornaam." ".$relatie->geschreven_door->achternaam}} @else Niemand heeft deze werknemer geschreven, wijzig dit zsm @endif</a></td>
                            <td><a href="/relatie/{{$relatie->id}}">{{$relatie->verwerkt_door->name}}</a></td>
                            <td><a href="/relatie/{{$relatie->id}}">{{date('d-m-Y', strtotime($relatie->geschreven_op))}}</a></td>
                        </tr>
                    @endforeach
			    </tbody>
			</table>
		</div>
	</div>

@endif

@endsection