@extends('layouts.new')

@section('title', 'Flexavi - Relatie Toevoegen')

@section('page_title', 'Klant Toevoegen')

@push('styles')

    <!-- FlatPickr CSS -->
    <link rel="stylesheet" href="{{ URL::asset('new') }}/assets/libs/flatpickr/flatpickr.min.css">
@endpush

@push('scripts')

<!-- Date & Time Picker JS -->
<script src="{{ URL::asset('new') }}/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="{{ URL::asset('new') }}/assets/js/date&time_pickers.js"></script>
<script src="{{ URL::asset('new') }}/assets/flexavi_js/zipcode.js"></script>
<script>
    function removeSpacesAfterTyping(inputElement) {
        // Get the input value and remove spaces
        const inputValue = inputElement.value.replace(/\s/g, '');

        // Update the input value
        inputElement.value = inputValue;
    }
</script>
@endpush
@section('content')
    <div class="row">
    @if(Auth::user()->rol != 99)
        <div class="col-xl-12">
            <div class="card custom-card">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        Klant toevoegen
                    </div>
                </div>
                <div class="card-body">
                    <div class="row gy-4">
                        <div class="col-xl-12">
                            <form method="POST" action="/relatie/nieuw" enctype="multipart/form-data">
                                {{csrf_field()}}
                            <div class="row gy-4">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <label for="input-voornaam" class="form-label">Voornaam</label>
                                    <input type="text" class="form-control" id="input-voornaam" name="voornaam">
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <label for="input-achternaam" class="form-label">Achternaam / Bedrijfsnaam</label>
                                    <input type="text" class="form-control" id="input-achternaam" required name="achternaam">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                    <label for="input-postcode" class="form-label">Postcode (9999XX)</label>
                                    <input type="text" class="form-control postcode" id="input-postcode" name="postcode" required maxlength="6" />
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                    <label for="input-housenumber" class="form-label">Huisnummer</label>
                                    <input type="number" class="form-control huisnummer" name="huisnummer" id="input-housenumber" required step="1"/>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                    <label for="input-suffix" class="form-label">Toevoeging</label>
                                    <input type="text" class="form-control hn_prefix" maxlength="3" id="input-suffix" name="hn_prefix"/>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <label for="straat" class="form-label">Adres</label>
                                    <input type="text" class="form-control straat" name="straat" id="input-street" readonly required/>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <label for="woonplaats" class="form-label">Woonplaats</label>
                                    <input type="text" class="woonplaats form-control" name="woonplaats" id="input-city" readonly required/>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                    <label for="input-phone" class="form-label">Telefoonnummer:</label>
                                    <input type="tel" maxlength="15" required class="form-control" id="input-phone" name="telefoonnummer_prive"
                                           pattern="^(06-\d{8}|0[1-9]\d-\d{7}|0[1-9]\d{2}-\d{6}|(\+31\d{9}))$" title="Voer een geldig telefoonnummer in. (06-12345678 of +31612345678)" oninput="removeSpacesAfterTyping(this)"/>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                    <label for="input-phone2" class="form-label">Tweede telefoonnummer:</label>
                                    <input type="tel" class="form-control" id="input-phone2" name="telefoonnummer_zakelijk"
                                           pattern="^(06-\d{8}|0[1-9]\d-\d{7}|0[1-9]\d{2}-\d{6}|(\+31\d{9}))$" title="Voer een geldig telefoonnummer in (06-12345678 of +31612345678)" oninput="removeSpacesAfterTyping(this)"/>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                    <label for="input-email" class="form-label">Email</label>
                                    <input type="email" class="form-control" id="input-email" name="email"/>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <p class="fw-semibold mb-2">Relatie type</p>
                                    <select class="form-control" data-trigger name="klanttype_id" id="choices-single-default">
                                        @foreach($klanttypes as $type)
                                            <option @if($type->omschrijving == "Klant") selected @endif value="{{$type->id}}">{{$type->omschrijving}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <p class="fw-semibold mb-2">Geschreven door</p>
                                    <select class="form-control" data-trigger name="geschreven_door" id="choices-single-default">
                                        <option value="" disabled>Select...</option>
                                        @foreach($werknemers as $werknemer)
                                            <option value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                        <p class="fw-semibold mb-2">Geschreven op</p>
                                    <div class="input-group">
                                        <div class="input-group-text text-muted"> <i class="ri-calendar-line"></i> </div>
                                        <input type="text" class="form-control" id="weeknum" required name="geschreven_op" placeholder="Kies datum" value="{{date('Y-m-d')}}">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-check">
                                        <p class="mb-3 px-0"><strong>Initiatief ligt bij klant</strong></p>
                                        <input class="form-check-input ms-2" type="checkbox" value="1" id="flexCheckDefault" name="initiatief">
                                        <input type="hidden" name="verwerkt_door" value="{{Auth::user()->id}}"/>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div class="md-btn-group">
                                        <div align="right">
                                            <button type="reset" class="btn btn-warning">RESET</button>
                                            <button type="submit" class="btn btn-success">Voeg Relatie Toe</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    </div>

@endsection
