@extends('layouts.master')

@section('title', 'Relatie Profiel')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

<div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
    <div class="uk-width-large-7-10">
        <div class="md-card">
            <div class="user_heading">
                <div class="user_heading_menu" data-uk-dropdown="{pos:'left-top'}">
                    <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                    <div class="uk-dropdown uk-dropdown-small">
                        <ul class="uk-nav">
                            <li><a href="/afspraak/maken/{{$relatie->id}}">Afspraak Maken</a></li>
                            <li><a href="/relatie/{{$relatie->id}}/delete">Verwijder Relatie</a></li>
                            <li><a href="/geschiedenis/{{$relatie->id}}/toevoegen">Geschiedenis Toevoegen</a></li>
                        </ul>
                    </div>
                </div>
                <div class="user_heading_content">
                    <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate">{{$relatie->voornaam}} {{$relatie->achternaam}}</span><span class="sub-heading"> --  {{$type->omschrijving}}</span></h2>
                    <ul class="user_stats">
                        <li>
                            <h4 class="heading_a">€ 100,00 <span class="sub-heading">Klantomzet</span></h4>
                        </li>
                        <li>
                            <h4 class="heading_a">@if($afspraak != ""){{date('d-M-Y', strtotime($afspraak->startdatum))}}@else NVT @endif <span class="sub-heading">Volgende afspraak</span></h4>
                        </li>
                        <li>
                            <h4 class="heading_a">Leeg <span class="sub-heading">Doorlooptijd vanaf eerste afspraak</span></h4>
                        </li>
                    </ul>
                </div>
                <a class="md-btn md-btn-success md-btn-wave-light" href="/relatie/{{$relatie->id}}/aanpassen">Wijzig Relatie</a>
                <a class="md-btn md-btn-warning md-btn-wave-light" href="/afspraak/maken/{{$relatie->id}}">Afspraak maken</a>
                <a class="md-btn md-btn-success md-btn-wave-light" href="/geschiedenis/{{$relatie->id}}/toevoegen">Geschiedenisregel toevoegen</a>
            </div>
            <div class="user_content">
                <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                    <li class="uk-active"><a href="#">Over</a></li>
                    <li><a href="#">Afspraken</a></li>
                    <li><a href="#">Geschiedenis</a></li>
                    <li><a href="#">Facturen & Offertes</a></li>
                </ul>
                <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                    <li>
                    	<div class="uk-grid" data-uk-grid-margin>
                			<div class="uk-width-large-1-1">
                				<table class="uk-table uk-table-hover">
                					<thead>
                						<h3><i class="material-icons md-36">person</i>
                						NAW Gegevens</h3>
                					</thead>
                					<tbody>
                						<tr>
                							<td>Naam:</td>
                							<td>{{$relatie->voornaam}} {{$relatie->achternaam}}</td>
                						</tr>
                						<tr>
                							<td>Adres:</td>
                							<td>{{$relatie->straat}} {{$relatie->huisnummer}} {{$relatie->hn_prefix}}, <br> {{$relatie->postcode}} {{$relatie->woonplaats}}</td>
                						</tr>
                						<tr>
                							<td>Contactgegevens:</td>
                							<td>{{$relatie->telefoonnummer_prive}} || {{$relatie->telefoonnummer_zakelijk}}</td>
                						</tr>
                						<tr>
                							<td>Email:</td>
                							<td>{{$relatie->email}}</td>
                						</tr>
                					</tbody>
                				</table>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                			<div class="uk-width-large-1-1">
                				<table class="uk-table uk-table-hover">
                					<thead>
                						<h3><i class="material-icons md-36">info_outline</i>
                						Overige Informatie</h3>
                					</thead>
                					<tbody>
                						<tr>
                							<td>Bankrekeningnummer:</td>
                							<td>{{$relatie->bankrekening}}</td>
                						</tr>
                						<tr>
                							<td>Geschreven door:</td>
                							<td>{{$werknemer->voornaam}} {{$werknemer->achternaam}}</td>
                						</tr>
                						<tr>
                							<td>Geschreven Op</td>
                							<td>{{date('d-m-Y', strtotime($relatie->created_at))}}</td>
                						</tr>
                						<tr>
                							<td>Aangemaakt door:</td>
                							<td>{{$user->name}}</td>
                						</tr>
                					</tbody>
                				</table>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h4 class="heading_c uk-margin-bottom">Afspraken</h4>
                         <div class="timeline">
                             @if(!empty($alleafspraken) && !empty($actperafs))
                                @foreach($alleafspraken as $afs)
                                         <div class="timeline_item">
                                            <div class="timeline_icon timeline_icon_primary"><i class="material-icons">date_range</i></div>
                                            <div class="timeline_date"><span>{{date('d-m-Y H:i', strtotime($afs->afspraak->startdatum))}}</span><br>
                                                                       <span>{{date('d-m-Y H:i', strtotime($afs->afspraak->einddatum))}}</span></div>
                                            <div class="timeline_content"><span class="uk-text-bold">{{$afs->afspraak->afspraaktype}} -- {{$afs->status->omschrijving}}; {{$afs->omschrijving}}</span> <a href="/afspraak/{{$afs['id']}}/verwijderen"><i class="material-icons">delete</i></a><br>
                                                @foreach($actperafs as $afsa)
                                                    @if($afs->afspraak->id == $afsa->afspraak->id)
                                                        {{$afsa->aantal}} {{$afsa->activiteit->omschrijving}} à € {{$afsa->activiteit->prijs}}<br>
                                                    @endif
                                                @endforeach
                                                {{$afs->afspraak->omschrijving}}<br>
                                                <strong>Totaalbedrag: € {{$afs->afspraak->totaalbedrag}}</strong>
                                                @if($afs->status_id == 1)
                                                <div class="right">
                                                    <a href="/afspraak/{{$afs->afspraak_id}}/verwerken" class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Verwerk afspraak</a>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                @endforeach
                            @endif
                         </div>
                    </li>
                    <li>
                        <a class="md-btn md-btn-primary md-btn-wave-light" href="/relatie/{{$relatie->id}}">Timeline view</a>
                        <h1></h1>
                    	<h4 class="heading_c uk-margin-bottom">Geschiedenis</h4>
                    	<!-- Foreach geschiedenisregels as regel -->
                        <div class="dt_colVis_buttons"></div>
                        <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Datum</th>
                                    <th>Omschrijving</th>
                                    <th>Aangemaakt op</th>
                                    <th>Aangemaakt Door</th>
                                    <th>Geüpdate op</th>
                                    <th>Geüpdate Door</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($regels as $regel)
                                    <tr>
                                        <td>{{date('d-M-Y', strtotime($regel->datum))}}</td>
                                        <td>{{$regel->omschrijving}}</td>
                                        <td>{{$regel->created_at}}</td>
                                        <td>{{Auth::user()->name}}</td>
                                        <td>
                                            @if($regel->created_at != $regel->updated_at)
                                                {{$regel->updated_at}}
                                            @else 
                                                Niet geüpdatet 
                                            @endif
                                        </td>
                                        <td>
                                            @if($regel->created_at != $regel->updated_at)
                                                @foreach($users as $user)
                                                    @if($user->id == $regel->bijgewerkt_door) 
                                                        {{$user->name}} 
                                                    @endif 
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table> 
                    </li>
                    <li>
                    	<p>Er zijn nog geen facturen of offertes</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="uk-width-large-3-10">
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-margin-medium-bottom">
                    <h3 class="heading_c uk-margin-bottom">Let op!</h3>
                    <ul class="md-list md-list-addon">
                        <li>
                            <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">Openstaande Factuur</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Betaaldatum is 5 dagen verstreken. Klik <a href="#">hier</a> voor de factuur</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">Occaecati eaque similique.</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Impedit repudiandae est itaque debitis similique provident.</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">Sint et.</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Fuga natus alias odit quis ratione quia.</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endif

@endsection