@extends('layouts.master')

@section('title', 'test')

@section('content')


@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Advanced Wizard</h2>

            <div class="md-card uk-margin-large-bottom">
                <div class="md-card-content">
                    <form class="uk-form-stacked" id="wizard_advanced_form">
                        <div id="wizard_advanced">


                        	{!! Form::open(array('url' => 'klant/toevoegen', 'method' => 'post')) !!}
                            <!-- first section -->
                            <h3>Owner information</h3>
                            <section>
                                <h2 class="heading_a">
                                    Owner Information
                                    <span class="sub-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                                </h2>
                                <hr class="md-hr"/>
                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-1 parsley-row">
                                        <label for="wizard_fullname">Full Name<span class="req">*</span></label>
                                        <input type="text" name="wizard_fullname" id="wizard_fullname" required class="md-input" />
                                    </div>
                                </div>
                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-1 parsley-row">
                                        <label for="wizard_address">Address<span class="req">*</span></label>
                                        <input type="text" name="wizard_address" id="wizard_address" required class="md-input" />
                                    </div>
                                </div>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <label for="wizard_birth">Birth Date<span class="req">*</span></label>
                                        <input type="text" name="wizard_birth" id="wizard_birth" required class="md-input" data-parsley-date data-parsley-date-message="This value should be a valid date" data-uk-datepicker="{format:'MM.DD.YYYY'}" />
                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <select id="wizard_birth_place" name="wizard_birth_place" required>
                                            <option value="">Place of Birth</option>
                                                                                            <option value="city_0">South Scarlettton</option>
                                                                                            <option value="city_1">Port Jessycaton</option>
                                                                                            <option value="city_2">Conradborough</option>
                                                                                            <option value="city_3">South Leda</option>
                                                                                            <option value="city_4">East Dovie</option>
                                                                                            <option value="city_5">Charlenemouth</option>
                                                                                            <option value="city_6">Port Michele</option>
                                                                                            <option value="city_7">Stoltenbergshire</option>
                                                                                            <option value="city_8">East Nicolette</option>
                                                                                            <option value="city_9">Allenstad</option>
                                                                                            <option value="city_10">New Destinee</option>
                                                                                            <option value="city_11">Koepphaven</option>
                                                                                            <option value="city_12">West Caesarborough</option>
                                                                                            <option value="city_13">East Tyriquemouth</option>
                                                                                            <option value="city_14">Virginiaside</option>
                                                                                            <option value="city_15">Alside</option>
                                                                                            <option value="city_16">Stefanfurt</option>
                                                                                            <option value="city_17">East Chaim</option>
                                                                                            <option value="city_18">Hobartborough</option>
                                                                                            <option value="city_19">Schroederburgh</option>
                                                                                    </select>
                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <label class="uk-form-label">Marital Status<span class="req">*</span></label>
                                        <span class="icheck-inline">
                                            <input type="radio" name="wizard_status" id="wizard_status_married" required class="wizard-icheck" value="married" />
                                            <label for="wizard_status_married" class="inline-label">Married</label>
                                        </span>
                                        <span class="icheck-inline">
                                            <input type="radio" name="wizard_status" id="wizard_status_single" class="wizard-icheck" value="single" />
                                            <label for="wizard_status_single" class="inline-label">Single</label>
                                        </span>
                                    </div>
                                </div>
                                <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-4" data-uk-grid-margin>
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons">&#xE0CD;</i>
                                        </span>
                                        <label for="wizard_phone">Phone Number</label>
                                        <input type="text" class="md-input" name="wizard_phone" id="wizard_phone" />
                                    </div>
                                    <div class=" parsley-row">
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon">
                                                <i class="material-icons">&#xE0BE;</i>
                                            </span>
                                            <label for="wizard_email">Email</label>
                                            <input type="text" class="md-input" name="wizard_email" id="wizard_email" required />
                                        </div>
                                    </div>
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="uk-icon-skype"></i>
                                        </span>
                                        <label for="wizard_skype">Skype</label>
                                        <input type="text" class="md-input" name="wizard_skype" id="wizard_skype" />
                                    </div>
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="uk-icon-twitter"></i>
                                        </span>
                                        <label for="wizard_twitter">Twitter</label>
                                        <input type="text" class="md-input" name="wizard_twitter" id="wizard_twitter" />
                                    </div>
                                </div>
                            </section>

                            <!-- second section -->
                            <h3>Vehicle information</h3>
                            <section>
                                <h2 class="heading_a">
                                    Vehicle information
                                    <span class="sub-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                                </h2>
                                <hr class="md-hr"/>
                                <div class="uk-grid uk-grid-width-large-1-2 uk-grid-width-xlarge-1-4" data-uk-grid-margin>
                                    <div class="parsley-row">
                                        <label for="wizard_vehicle_title_number">Title Number<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_title_number" id="wizard_vehicle_title_number" required class="md-input" />
                                    </div>
                                    <div class="parsley-row">
                                        <label for="wizard_vehicle_vin">VIN<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_vin" id="wizard_vehicle_vin" required class="md-input" />
                                    </div>
                                    <div class="parsley-row">
                                        <label for="wizard_vehicle_plate_number">Current Plate Number<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_plate_number" id="wizard_vehicle_plate_number" required class="md-input" />
                                    </div>
                                    <div class="parsley-row">
                                        <label for="wizard_vehicle_expiration">Expiration Date<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_expiration" id="wizard_vehicle_expiration" required class="md-input" data-parsley-americandate data-parsley-americandate-message="This value should be a valid date (MM.DD.YYYY)" data-uk-datepicker="{format:'MM.DD.YYYY'}" />
                                    </div>
                                </div>
                                <div class="uk-grid uk-grid-width-large-1-3 uk-grid-width-xlarge-1-6" data-uk-grid-margin>
                                    <div class="parsley-row">
                                        <label for="wizard_vehicle_year">Registration Year</label>
                                        <input type="text" name="wizard_vehicle_year" id="wizard_vehicle_year" class="md-input" data-uk-datepicker="{format:'MM.YYYY'}" />
                                    </div>
                                    <div class="parsley-row">
                                        <label for="wizard_vehicle_make">Make</label>
                                        <input type="text" name="wizard_vehicle_make" id="wizard_vehicle_make" class="md-input" />
                                    </div>
                                    <div class="parsley-row">
                                        <label for="wizard_vehicle_model">Model<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_model" id="wizard_vehicle_model" required class="md-input" />
                                    </div>
                                    <div class="parsley-row">
                                        <label for="wizard_vehicle_body">Body Type<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_body" id="wizard_vehicle_body" required class="md-input" />
                                    </div>
                                    <div class="parsley-row">
                                        <label for="wizard_vehicle_axles">Axles</label>
                                        <input type="text" name="wizard_vehicle_axles" id="wizard_vehicle_axles" class="md-input" />
                                    </div>
                                    <div class="parsley-row">
                                        <label for="wizard_vehicle_fuel">Fuel<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_fuel" id="wizard_vehicle_fuel" required class="md-input" />
                                    </div>
                                </div>
                            </section>

                            <!-- third section -->
                            <h3>Additional information</h3>
                            <section>
                                <h2 class="heading_a">
                                    Additional information
                                    <span class="sub-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                                </h2>
                                <hr class="md-hr"/>
                                <div class="uk-grid uk-margin-large-bottom" data-uk-grid-margin>
                                    <div class="uk-width-1-1">
                                        <label class="uk-form-label">Location Where Vehicle is Principally Garaged</label>
                                        <div class="uk-grid" data-uk-grid-margin="">
                                            <div class="uk-width-medium-2-10 parsley-row">
                                                <span class="icheck-inline uk-margin-top uk-margin-left">
                                                    <input type="radio" name="wizard_additional_location" id="wizard_status_location_city" class="wizard-icheck" value="City" />
                                                    <label for="wizard_status_location_city" class="inline-label">City</label>
                                                </span>
                                            </div>
                                            <div class="uk-width-medium-2-10 parsley-row">
                                                <span class="icheck-inline uk-margin-top uk-margin-left">
                                                    <input type="radio" name="wizard_additional_location" id="wizard_status_location_county" class="wizard-icheck" value="County" />
                                                    <label for="wizard_status_location_county" class="inline-label">County</label>
                                                </span>
                                            </div>
                                            <div class="uk-width-medium-3-10 parsley-row">
                                                <div class="uk-input-group">
                                                    <span class="uk-input-group-addon">
                                                       <input type="radio" name="wizard_additional_location" class="wizard-icheck" value="town" />
                                                    </span>
                                                    <label for="wizard_location_town">Town of</label>
                                                    <input type="text" class="md-input" name="wizard_location_town" id="wizard_location_town" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="uk-alert uk-alert-info">If you would like your registration renewals sent to an address other than your residence/business address, enter it below.</span>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-6 parsley-row">
                                        <label for="wizard_vehicle_registration_address">Registration Mailing Address</label>
                                        <input type="text" name="wizard_vehicle_registration_address" id="wizard_vehicle_registration_address" required class="md-input" />
                                    </div>
                                    <div class="uk-width-medium-1-6 parsley-row">
                                        <label for="wizard_vehicle_registration_city">City<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_registration_city" id="wizard_vehicle_registration_city" required class="md-input" />
                                    </div>
                                    <div class="uk-width-medium-1-6 parsley-row">
                                        <label for="wizard_vehicle_registration_state">State<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_registration_state" id="wizard_vehicle_registration_state" required class="md-input" />
                                    </div>
                                    <div class="uk-width-medium-1-6 parsley-row">
                                        <label for="wizard_vehicle_registration_zip">ZIP<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_registration_zip" id="wizard_vehicle_registration_zip" required class="md-input" />
                                    </div>
                                    <div class="uk-width-medium-1-6 parsley-row">
                                        <label for="wizard_vehicle_registration_code">Code<span class="req">*</span></label>
                                        <input type="text" name="wizard_vehicle_registration_code" id="wizard_vehicle_registration_code" required class="md-input" />
                                    </div>
                                </div>
                            </section>

                        </div>
                        {!! Form::close() !!}
                    </form>
                </div>
            </div>


@endif

@endsection