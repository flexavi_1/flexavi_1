@extends('layouts.new')

@section('title', 'Flexavi - Relatie Toevoegen')

@section('page_title', 'Klant Toevoegen')

@push('styles')

    <!-- FlatPickr CSS -->
    <link rel="stylesheet" href="{{ URL::asset('new') }}/assets/libs/flatpickr/flatpickr.min.css">
@endpush

@push('scripts')

    <!-- Date & Time Picker JS -->
    <script src="{{ URL::asset('new') }}/assets/libs/flatpickr/flatpickr.min.js"></script>
    <script src="{{ URL::asset('new') }}/assets/js/date&time_pickers.js"></script>
    <script src="{{ URL::asset('new') }}/assets/flexavi_js/zipcode.js"></script>

@endpush
@section('content')
    <div class="row">
        @if(Auth::user()->rol != 99)
            <div class="col-xl-12">
                <div class="card custom-card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            Klant toevoegen
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row gy-4">
                            <div class="col-xl-12">
                                <form method="POST" action="/relatie/{{$klant->id}}/aanpassen" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row gy-4">
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <label for="input-voornaam" class="form-label">Voornaam</label>
                                            <input type="text" class="form-control" id="input-voornaam" name="voornaam" value="{{$klant->voornaam}}">
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <label for="input-achternaam" class="form-label">Achternaam / Bedrijfsnaam</label>
                                            <input type="text" class="form-control" id="input-achternaam" required name="achternaam" value="{{$klant->achternaam}}">
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                            <label for="input-postcode" class="form-label">Postcode (9999XX)</label>
                                            <input type="text" class="form-control postcode" id="input-postcode" name="postcode" required maxlength="6"  value="{{$klant->postcode}}"/>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                            <label for="input-housenumber" class="form-label">Huisnummer</label>
                                            <input type="number" class="form-control huisnummer" name="huisnummer" id="input-housenumber" required step="1" value="{{$klant->huisnummer}}"/>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                            <label for="input-suffix" class="form-label">Toevoeging</label>
                                            <input type="text" class="form-control hn_prefix" maxlength="3" id="input-suffix" name="hn_prefix" value="{{$klant->hn_prefix}}"/>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <label for="straat" class="form-label">Adres</label>
                                            <input type="text" class="form-control straat" name="straat" id="input-street" readonly required value="{{$klant->straat}}"/>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <label for="woonplaats" class="form-label">Woonplaats</label>
                                            <input type="text" class="woonplaats form-control" name="woonplaats" id="input-city" readonly required value="{{$klant->woonplaats}}"/>
                                        </div>
                                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                            <label for="input-phone" class="form-label">Telefoonnummer:</label>
                                            <input type="tel" maxlength="15" required class="form-control" id="input-phone" name="telefoonnummer_prive"  value="{{$klant->telefoonnummer_prive}}"/>
                                        </div>
                                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                            <label for="input-phone2" class="form-label">Tweede telefoonnummer:</label>
                                            <input type="text" class="form-control" id="input-phone2" name="telefoonnummer_zakelijk" value="{{$klant->telefoonnummer_zakelijk}}"/>
                                        </div>
                                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                            <label for="input-email" class="form-label">Email</label>
                                            <input type="email" class="form-control" id="input-email" name="email" value="{{$klant->email}}"/>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <p class="fw-semibold mb-2">Relatie type</p>
                                            <select required class="form-control" data-trigger name="klanttype_id" id="choices-single-default">
                                                @foreach($klanttypes as $type)
                                                    <option @if($klant->klanttype_id == $type->id) selected @endif value="{{$type->id}}">{{$type->omschrijving}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <p class="fw-semibold mb-2">Geschreven door</p>
                                            <select class="form-control" data-trigger name="geschreven_door" id="choices-single-default">
                                                <option value="" disabled>Select...</option>
                                                @foreach($werknemers as $werknemer)
                                                    <option @if($klant->geschreven_door == $werknemer->id) selected @endif value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <p class="fw-semibold mb-2">Geschreven op</p>
                                            <div class="input-group">
                                                <div class="input-group-text text-muted"> <i class="ri-calendar-line"></i> </div>
                                                <input type="text" class="form-control" id="weeknum" required name="geschreven_op" placeholder="Kies datum"  value="{{date('d-m-Y', strtotime($klant->geschreven_op))}}">
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-check">
                                                <p class="mb-3 px-0"><strong>Initiatief ligt bij klant</strong></p>
                                                <input class="form-check-input ms-2" type="checkbox" value="1" id="flexCheckDefault" @if($klant->initiatief == 1) checked @endif name="initiatief">
                                                <input type="hidden" name="verwerkt_door"  value="{{$klant->verwerkt_door}}"/>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                            <div class="md-btn-group">
                                                <div align="right">
                                                    <button type="reset" class="btn btn-warning">RESET</button>
                                                    <button type="submit" class="btn btn-success">Wijzig Klant</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
