@extends('layouts.new')

@section('title', 'Terug bellijst')
@section('page_title', 'Terug bellijst')
@push('styles')
    <!-- FlatPickr CSS -->
    <link rel="stylesheet" href="{{ URL::asset('new') }}/assets/libs/flatpickr/flatpickr.min.css">
    <link rel="stylesheet" href="{{ URL::asset('new') }}/assets/libs/quill/quill.snow.css">
    <link rel="stylesheet" href="{{ URL::asset('new') }}/assets/libs/quill/quill.bubble.css">
@endpush
@push('scripts')

    <!-- Date & Time Picker JS -->
    <script src="{{ URL::asset('new') }}/assets/libs/flatpickr/flatpickr.min.js"></script>
    <script src="{{ URL::asset('new') }}/assets/js/date&time_pickers.js"></script>
    <!-- Quill Editor JS -->
    <script src="{{ URL::asset('new') }}/assets/libs/quill/quill.min.js"></script>
    <!-- Internal Datatables JS -->
    <script src="{{ URL::asset('new') }}/assets/flexavi_js/fetching.js"></script>
    <script defer src="{{ URL::asset('new') }}/assets/flexavi_js/terugbellijst.js"></script>
@endpush
@section('content')

@if(Auth::user()->rol != 99)
    <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
    <div class="row mb-3" style="display:none;" id="alertDiv">
        <div class="col-xxl-12 col-xl-12">
            <div class="alert alert-solid-success alert-dismissible fade show" id="alertContent">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"><i class="bi bi-x"></i></button>
            </div>
        </div>
    </div>
    <!-- Start::row-1 -->
    <div class="row">
        <div class="col-xl-12">
            <div class="card custom-card pb-4">
                <div class="card-header">
                    <div class="card-title">
                        Terug Bel Lijst
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table text-nowrap w-100">
                            <thead>
                            <tr>
                                <th style="width: 10%;">Naam</th>
                                <th style="width: 15%;">Adres</th>
                                <th style="width: 10%;">Contactgegevens</th>
                                <th style="width: 45%;">Reden van Terugbellen</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="terugbellijst-table-body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End::row-1 -->


@endif

@endsection
