@extends('layouts.master')


@section('title', $relatie->achternaam.' - Relatie Profiel')

@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>

    <!-- datatables buttons-->

    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>



    <!-- datatables custom integration -->

    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>



    <!--  datatables functions -->

    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>



    <!--  My own js -->

    <script src="{{ URL::asset('assets/assets/js/js_test.js')}}"></script>

@endpush

@section('content')

    @if(Auth::user()->rol != 99)

        @if($danger != "")

            <div class="md-card md-card-collapsed">

                <div class="md-card-toolbar md-bg-red-400 uk-text-contrast">

                    <h3 class="md-card-toolbar-heading-text ">

                        {{$danger}}

                        <a href="/relatie/{{$relatie->id}}/setActive">Herstel verwijderde relatie</a>

                    </h3>

                </div>

            </div>

        @endif



        @if($relatie->klanttype_id <= 4)
            <!-- Klant -->

            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">

                <div class="uk-width-large">

                    <div class="md-card">

                        <div class="user_heading">

                            <div class="user_heading_menu" data-uk-dropdown="{pos:'left-top'}">

                                <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>

                                <div class="uk-dropdown uk-dropdown-small">

                                    <ul class="uk-nav">

                                        <li><a href="/afspraak/maken/{{$relatie->id}}">Afspraak Maken</a></li>

                                        <li><a href="/relatie/{{$relatie->id}}/delete">Verwijder Relatie</a></li>

                                        <li><a href="/geschiedenis/{{$relatie->id}}/toevoegen">Geschiedenis
                                                Toevoegen</a></li>

                                    </ul>

                                </div>

                            </div>

                            <div class="user_heading_content">

                                <h2 class="heading_b uk-margin-bottom"><span
                                            class="uk-text-truncate">{{$relatie->voornaam}} {{$relatie->achternaam}}</span><span
                                            class="sub-heading"> --  {{$relatie->klanttype->omschrijving}}</span></h2>

                                <ul class="user_stats">

                                    <li>

                                        <h4 class="heading_a">€ {{number_format($relatie->getCustomerRevenue(),2)}}
                                            <span class="sub-heading">Klantomzet</span></h4>

                                    </li>

                                    <li>

                                        <h4 class="heading_a">@if($afspraak != "")
                                                {{date('d-M-Y', strtotime($afspraak->startdatum))}}
                                            @else
                                                NVT
                                            @endif <span class="sub-heading">Volgende afspraak</span></h4>

                                    </li>

                                    <li>

                                        <h4 class="heading_a">Leeg <span class="sub-heading">Doorlooptijd vanaf eerste afspraak</span>
                                        </h4>

                                    </li>


                                    <li>

                                        <h4 class="heading_a">@if($relatie->inTbLijst())
                                                Ja
                                            @else
                                                Nee
                                            @endif <span class="sub-heading">In terugbellijst</span></h4>

                                    </li>


                                </ul>

                            </div>

                            <div class="user_heading_content">

                                @if(Auth::user()->rol < 7)

                                    <a class="md-btn md-btn-success md-btn-wave-light"
                                       href="/relatie/{{$relatie->id}}/aanpassen">Wijzig relatie</a>

                                    <a class="md-btn md-btn-warning md-btn-wave-light"
                                       href="/afspraak/maken/{{$relatie->id}}">Afspraak maken</a>

                                    <a class="md-btn md-btn-success md-btn-wave-light"
                                       href="/geschiedenis/{{$relatie->id}}/toevoegen">Geschiedenisregel toevoegen</a>

                                    <a class="md-btn md-btn-warning md-btn-wave-light"
                                       href="/offerte/nieuw/{{$relatie->id}}">Offerte toevoegen</a>

                                    <a class="md-btn md-btn-success md-btn-wave-light"
                                       href="/relatie/{{$relatie->id}}/mail/create">Mail invoeren</a>

                                    <a class="md-btn md-btn-warning md-btn-wave-light"
                                       href="/relatie/{{$relatie->id}}/add/email">Extra emailadres invoeren</a>

                                    <!-- <a class="md-btn md-btn-success md-btn-wave-light" href="/factuur/nieuw/{{$relatie->id}}/handmatig">Handmatig factuur opstellen</a> -->

                                @endif

                            </div>

                        </div>

                        <div class="user_content">

                            <ul id="user_profile_tabs" class="uk-tab"
                                data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}"
                                data-uk-sticky="{ top: 48, media: 960 }">

                                <li><a href="#">Over</a></li>

                                <li><a href="#">Afspraken</a></li>

                                <li><a href="#">Betaalherinneringen</a></li>

                                <li class="uk-active"><a href="#">Geschiedenis</a></li>

                                <li><a href="#">Facturen</a></li>

                                <li><a href="#">Offertes</a></li>

                                <li><a href="#">Terugbetaal verzoeken</a></li>

                                <li><a href="#">Mails</a></li>

                                <li><a href="#">Herinneringen/Aanmaningen</a></li>

                            </ul>

                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">

                                <li>

                                    <div class="uk-grid" data-uk-grid-margin>

                                        <div class="uk-width-large-1-1">

                                            <table class="uk-table uk-table-hover">

                                                <thead>

                                                <h3><i class="material-icons md-36">person</i>

                                                    NAW Gegevens</h3>

                                                </thead>

                                                <tbody>

                                                <tr>

                                                    <td width="25%">Naam:</td>

                                                    <td width="75%">{{$relatie->voornaam}} {{$relatie->achternaam}}</td>

                                                </tr>

                                                <tr>

                                                    <td width="25%">Adres:</td>

                                                    <td width="75%">{{$relatie->straat}} {{$relatie->huisnummer}} {{$relatie->hn_prefix}}
                                                        , <br> {{$relatie->postcode}} {{$relatie->woonplaats}}</td>

                                                </tr>

                                                <tr>

                                                    <td width="25%">Contactgegevens:</td>

                                                    <td width="75%">{{$relatie->telefoonnummer_prive}}
                                                        || {{$relatie->telefoonnummer_zakelijk}}</td>

                                                </tr>

                                                <tr>

                                                    <td width="25%">Email:</td>

                                                    <td width="75%">


                                                        {{$relatie->emailString}}


                                                    </td>

                                                </tr>

                                                </tbody>

                                            </table>

                                        </div>

                                    </div>

                                    <div class="uk-grid" data-uk-grid-margin>

                                        <div class="uk-width-large-1-1">

                                            <table class="uk-table uk-table-hover">

                                                <thead>

                                                <h3><i class="material-icons md-36">info_outline</i>

                                                    Overige Informatie</h3>

                                                </thead>

                                                <tbody>

                                                <tr>

                                                    <td width="25%">Bankrekeningnummer:</td>

                                                    <td width="75%">{{$relatie->bankrekening}}</td>

                                                </tr>

                                                <tr>

                                                    <td width="25%">Geschreven door:</td>

                                                    <td width="75%">{{$relatie->geschrevenDoor->voornaam}} {{$relatie->geschrevenDoor->achternaam}}</td>

                                                </tr>

                                                <tr>

                                                    <td width="25%">Geschreven Op</td>

                                                    <td width="75%">{{date('d-m-Y', strtotime($relatie->geschreven_op))}}</td>

                                                </tr>

                                                <tr>

                                                    <td width="25%">Aangemaakt door:</td>

                                                    <td width="75%">{{$relatie->verwerktDoor->name}}</td>

                                                </tr>

                                                <tr>

                                                    <td width="25%">Eerste initiatief:</td>

                                                    <td width="75%">@if($relatie->initiatief == 1)
                                                            Aanmelding vanuit klant
                                                        @else
                                                            Aanmelding vanuit ons
                                                        @endif</td>

                                                </tr>

                                                </tbody>

                                            </table>

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <h4 class="heading_c uk-margin-bottom">Afspraken</h4>

                                    <div class="timeline">

                                        @if(!empty($alleafspraken))

                                            @foreach($alleafspraken as $afs)

                                                @if($afs->afspraak->afspraaktype_id != 6 && $afs->afspraak->afspraaktype_id != 5)

                                                    <div class="timeline_item">

                                                        <div class="timeline_icon timeline_icon_primary"><i
                                                                    class="material-icons">date_range</i></div>


                                                        <div class="timeline_content">

                                                <span class="uk-text-bold">

                                                    <h3 class="heading_c">

                                                        <em>{{$afs->afspraak->id}}:</em>

                                                        @if($afs->afspraak->getDateDifference()->d > 0)

                                                            @if($afs->werkdag_id != null)

                                                                {{App\Models\Werkdag::returnDate($afs->werkdag_id)}}

                                                                {{date('H:i', strtotime($afs->afspraak->startdatum))}} -

                                                                {{date('H:i', strtotime($afs->afspraak->einddatum))}}

                                                            @else

                                                                {{date('d-m-Y H:i', strtotime($afs->afspraak->startdatum))}}

                                                                {{date('d-m-Y H:i', strtotime($afs->afspraak->einddatum))}}

                                                            @endif

                                                        @elseif(date('d-m-Y', strtotime($afs->afspraak->startdatum)) == date('d-m-Y', strtotime($afs->afspraak->einddatum)))

                                                            @if(date('H:i', strtotime($afs->afspraak->startdatum)) == date('H:i', strtotime("08:00")) && date('H:i', strtotime($afs->afspraak->einddatum)) == date('H:i', strtotime("12:30")))

                                                                {{date('d-m-Y', strtotime($afs->afspraak->startdatum))}}
                                                                voor de middag

                                                            @elseif(date('H:i', strtotime($afs->afspraak->startdatum)) == "12:30" && date('H:i', strtotime($afs->afspraak->einddatum)) == "17:00")

                                                                {{date('d-m-Y', strtotime($afs->afspraak->startdatum))}}
                                                                na de middag

                                                            @else

                                                                {{date('d-m-Y', strtotime($afs->afspraak->startdatum))}}

                                                                {{date('H:i', strtotime($afs->afspraak->startdatum))}} -

                                                                {{date('H:i', strtotime($afs->afspraak->einddatum))}}

                                                            @endif

                                                        @else

                                                            {{date('d-m-Y H:i', strtotime($afs->afspraak->startdatum))}}

                                                            {{date('d-m-Y H:i', strtotime($afs->afspraak->einddatum))}}

                                                        @endif

                                                    </h3>

                                                    {{ $afs->afspraak->afspraaktype->omschrijving ?? '' }}





                                                    @if($afs->afspraak->afspraaktype_id == 1)

                                                        {{str_replace('voldaan', 'betaald', $afs->status->omschrijving)}}

                                                    @else
                                                        {{$afs->status->omschrijving}}

                                                    @endif ;



                                                    @if($afs->afspraak->reden != null)

                                                        <br>
                                                        <i class="uk-text-danger">{!!strip_tags($afs->afspraak->reden)!!}</i>

                                                    @endif



                                                    <br>



                                                    {{$afs->omschrijving}}

                                                </span>


                                                            @if(Auth::user()->rol < 7)

                                                                <a data-uk-modal="{target:'#modal_header_footer{{$afs->afspraak->id}}'}"><i
                                                                            class="material-icons">delete</i></a><a
                                                                        href="/afspraak/{{$afs->afspraak->id}}/wijzigen"><i
                                                                            class="material-icons">edit</i></a>

                                                            @endif


                                                            <div class="uk-modal"
                                                                 id="modal_header_footer{{$afs->afspraak->id}}">

                                                                <div class="uk-modal-dialog">

                                                                    <div class="uk-modal-header">

                                                                        <h3 class="uk-modal-title">

                                                                            Alert!

                                                                        </h3>

                                                                    </div>

                                                                    <p>Weet u zeker dat u deze afspraak wilt
                                                                        verwijderen?</p>

                                                                    <div class="uk-modal-footer uk-text-right">

                                                                        <a href="/afspraak/{{$afs->afspraak->id}}/verwijderen"
                                                                           class="md-btn md-btn-flat">Ja</a>

                                                                        <button type="button"
                                                                                class="md-btn md-btn-flat uk-modal-close">
                                                                            Nee
                                                                        </button>

                                                                    </div>

                                                                </div>

                                                            </div>


																				  <?php $totaldisc = 0.00; $totaal = 0.00; ?>

                                                            @foreach($afs->afspraak->aha as $afsa)

                                                                {{-- @if($afs->afspraak->id == $afsa->afspraak->id) --}}

                                                                <br>{{$afsa->aantal}}
                                                                @if($afsa->activiteit !== null)
                                                                    {{$afsa->activiteit->eenheid}}
                                                                    {{$afsa->activiteit->omschrijving}}
                                                                @endif
                                                                <i>{{$afsa->opmerking}}</i>
                                                                à € {{number_format($afsa->prijs,2)}} =
                                                                € {{number_format(($afsa->aantal * $afsa->prijs),2)}}



																					  <?php $totaal += ($afsa->prijs * $afsa->aantal - $afsa->kortingsbedrag); ?>



                                                                @if($afsa->kortingsbedrag != 0)

                                                                    <em> Korting:
                                                                        € {{number_format($afsa->kortingsbedrag, 2)}} </em>

                                                                @endif



																					  <?php $totaldisc = $totaldisc + $afsa->kortingsbedrag; ?>



                                                                {{-- @endif --}}

                                                            @endforeach



                                                            @if($totaal < $afs->afspraak->totaalbedrag && $afs->afspraak->klusprijs != 1)

                                                                <br>Geld ophalen
                                                                € {{number_format($afs->afspraak->totaalbedrag - $totaal, 2)}}

                                                            @endif


                                                            <br>{!!$afs->afspraak->omschrijving!!}<br>


                                                            @if($afs->afspraak->afspraak_id != null)

                                                                <i class="uk-text-primary">Verwijzing van afspraak:
                                                                    <b>{{date('d-m-Y', strtotime($afs->afspraak->getReference()->startdatum))}}</b></i>
                                                                <br>

                                                            @endif



                                                            @if($totaldisc != 0 && $afs->afspraak->klusprijs == 0)

                                                                <strong><em>Totale korting:
                                                                        € {{number_format($totaldisc,2)}}</em></strong>

                                                            @endif



                                                            @if($afs->afspraak->klusprijs == 1)

                                                                <strong><em>Totale korting:
                                                                        € {{number_format($afs->afspraak->getRealAppointmentPrice() - $afs->afspraak->totaalbedrag ,2)}}</em></strong>

                                                            @endif


                                                            <strong><br>Totaalbedrag: €

                                                                @if($totaal - $totaldisc != $afs->afspraak->totaalbedrag && $totaal + $totaldisc != $afs->afspraak->totaalbedrag)

                                                                    {{number_format($afs->afspraak->totaalbedrag, 2)}}

                                                                @else

                                                                    {{number_format(($afs->afspraak->totaalbedrag - $totaldisc),2)}}

                                                                @endif</strong>


                                                            @if($afs->afspraak->niet_betaald > 0)

                                                                {{-- if($afs->afspraak->getAmount($afs->afspraak->id) != 0) --}}

                                                                <br><strong class="uk-text-bold md-color-red-800">Openstaand:
                                                                    € {{number_format($afs->afspraak->niet_betaald,2)}} </strong>

                                                                {{-- endif --}}

                                                            @endif

                                                            @if($afs->afspraak->initiatief == 1)

                                                                <br> Initiatief van deze afspraak ligt bij de klant

                                                            @else

                                                                <br> Initiatief van deze afspraak ligt bij ons

                                                            @endif



                                                            @if($afs->status_id == 2 || $afs->status_id == 23 || $afs->status_id == 30)

                                                                <br> Schuld ligt bij
                                                                <em> @if($afs->afspraak->eigenschuld == 1)
                                                                        ons
                                                                    @else
                                                                        klant
                                                                    @endif </em>

                                                            @endif

                                                            <br> Toegevoegde geschiedenis: <br>

                                                            @foreach($afs->afspraak->getHistoryFromThisAppointment() as $h)

                                                                @if($h->geschiedenis != null)

																						  <?php str_replace(", <br>", "", $h->geschiedenis->omschrijving); ?>

                                                                    - {{ str_replace(", <br>", "", $h->geschiedenis->omschrijving) }}
                                                                    ; <br>

                                                                @endif

                                                            @endforeach


                                                            <div class="right">


                                                                @if($afs->status_id != 23 && Auth::user()->rol < 7)

                                                                    @if($afs->status_id == 1 &&  $afs->afspraak->afspraaktype_id != 6)

                                                                        <a href="/afspraak/{{$afs->afspraak->id}}/verwerken"
                                                                           class="md-btn md-btn-success md-btn-small md-btn-wave-light">Verwerk
                                                                            afspraak</a>

                                                                        @if($afs->afspraak->hasAha() && !$afs->afspraak->hasNewDefPayments() && $afs->afspraak->afspraaktype_id == 1)

                                                                            <a href="/afspraak/{{$afs->afspraak->id}}/verwerken/nietgelukt/nieuweafspraak/3"
                                                                               class="md-btn md-btn-warning md-btn-small md-btn-wave-light">Maak
                                                                                betaal afspraak</a>

                                                                        @endif

                                                                    @elseif($afs->status_id==2 && $afs->afspraak->is_verzet == 0 && $afs->afspraak->hasNewAppointment($afs->afspraak->id) != true)

                                                                        <a href="/afspraak/{{$afs->afspraak->id}}/verwerken/nietgelukt/nieuweafspraak/0"
                                                                           class="md-btn md-btn-warning md-btn-small md-btn-wave-light">Verzet
                                                                            afspraak</a>

                                                                        <a class="md-btn md-btn-danger md-btn-small md-btn-wave-light"
                                                                           href="/afspraak/maken/{{$afs->afspraak->id}}/nietnodig/profiel">Geen
                                                                            nieuwe afspraak</a>

                                                                    @elseif($afs->status_id == 3 && $afs->afspraak->niet_betaald > 0 && $afs->afspraak->getAmountOpen($afs->afspraak->id) < 1)

                                                                        <a href="/afspraak/{{$afs->afspraak->id}}/verwerken/nietgelukt/nieuweafspraak/3"
                                                                           class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Maak
                                                                            betaal afspraak</a>

                                                                    @endif

                                                                    @if( ($afs->status_id == 3 || $afs->status_id == 4) && $afs->afspraak->afspraaktype_id != 4)

                                                                        <a href="/afspraak/{{$afs->afspraak->id}}/garantie/afspraak/maken"
                                                                           class="md-btn md-btn-success md-btn-small md-btn-wave-light">Maak
                                                                            Garantie afspraak</a>

                                                                    @endif

                                                                    @if($afs->afspraak->afspraaktype_id == 6)

                                                                        <a href="/afspraak/{{$afs->afspraak->id}}/garantie/afspraak/maken"
                                                                           class="md-btn md-btn-success md-btn-small md-btn-wave-light">Terugbetaald</a>

                                                                    @endif

                                                                    @if($afs->status_id != 1)

                                                                        <a href="/afspraak/{{$afs->afspraak->id}}/reset/verwerking"
                                                                           class="md-btn md-btn-danger md-btn-small md-btn-wave-light">Herstel
                                                                            verwerking</a>

                                                                    @endif





                                                                    {{-- @ if ($afs->status_id < 2) --}}

                                                                    {{-- <a href="/afspraak/{{$afs->afspraak->id}}/nazorg" class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Verstuur Evaluatie</a> --}}

                                                                    {{-- <a href="/afspraak/{{$afs->afspraak->id}}/boom" class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Toon Afspraakboom</a> --}}





                                                                    <a href="/afspraak/{{$afs->afspraak->id}}/offerte"
                                                                       class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Voeg
                                                                        Offerte toe</a>

                                                                    @if($afs->afspraak->factuur_id == null && $afs->status_id != 2 && ($afs->afspraak->afspraaktype_id == 1 || $afs->afspraak->hasAha() ) )

                                                                        @if($afs->afspraak->afspraak_id == null)

                                                                            <a href="/afspraak/{{$afs->afspraak->id}}/factuur"
                                                                               class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Genereer
                                                                                Factuur</a>

                                                                        @endif

                                                                    @endif

                                                                    @if($afs->status_id == 2 || $afs->status_id == 23 || $afs->status_id == 30 || $afs->status_id == 29)

                                                                        <a href="/veranderEigenschuld/{{$afs->afspraak->id}}"
                                                                           class="md-btn md-btn-danger md-btn-small md-btn-wave-light">Wijzig
                                                                            eigenschuld</a>

                                                                    @endif

                                                                    <!-- @ endif -->

                                                                    @if($afs->status_id == 1)

                                                                        @if($afs->afspraak->getDateDifference()->d > 0)

                                                                            <a href="/afspraak/{{$afs->afspraak->id}}/annuleren/{{App\Models\Werkdag::returnDate($afs->werkdag_id)}}"
                                                                               class="md-btn md-btn-danger md-btn-small md-btn-wave-light">Annuleer
                                                                                Afspraak</a>

                                                                        @else

                                                                            <a href="/afspraak/{{$afs->afspraak->id}}/annuleren"
                                                                               class="md-btn md-btn-danger md-btn-small md-btn-wave-light">Annuleer
                                                                                Afspraak</a>

                                                                        @endif

                                                                        <a href="/afspraak/{{$afs->afspraak->id}}/voeg-geschiedenis-toe"
                                                                           class="md-btn md-btn-success md-btn-small md-btn-wave-light">Voeg
                                                                            geschiedenis toe</a>

                                                                    @endif

                                                                    @if($afs->bevestiging == 0 && $relatie->email != '' && $relatie->email != null)

                                                                        <a href="/afspraak/{{$afs->afspraak->id}}/bevestiging"
                                                                           class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Verstuur
                                                                            afspraakbevestiging</a>

                                                                    @endif

                                                                @endif

                                                                @if($afs->status_id == 23  && Auth::user()->rol < 7)

                                                                    <a href="/veranderEigenschuld/{{$afs->afspraak->id}}"
                                                                       class="md-btn md-btn-danger md-btn-small md-btn-wave-light">Wijzig
                                                                        eigenschuld</a>

                                                                @endif

                                                            </div>

                                                        </div>

                                                    </div>

                                                @endif

                                            @endforeach

                                        @endif

                                    </div>

                                </li>

                                <li>

                                    <h4 class="heading_c uk-margin-bottom">Betaalherinneringen</h4>

                                    <div class="timeline">

														<?php $aas = $relatie->getReminders(); ?>

                                        @if(count($aas) > 0)

                                            @foreach($relatie->getReminders() as $afs)

                                                <div class="timeline_item">

                                                    <div class="timeline_icon timeline_icon_primary"><i
                                                                class="material-icons">date_range</i></div>


                                                    <div class="timeline_date">
                                                        <span>{{date('d-m-Y H:i', strtotime($afs->afspraak->startdatum))}}</span><br>

                                                        <span>{{date('d-m-Y H:i', strtotime($afs->afspraak->einddatum))}}</span>
                                                    </div>


                                                    <div class="timeline_content">

                                            <span class="uk-text-bold">

                                                <i class="uk-text-primary">{{$afs->afspraak->id}}</i> : {{$afs->afspraak->afspraaktype}} -- {{$afs->status->omschrijving}};



                                                @if($afs->afspraak->reden != null)

                                                    <br><i class="uk-text-danger">{{$afs->afspraak->reden}}</i>

                                                @endif



                                                <br>



                                                {{$afs->omschrijving}}

                                            </span><a href="/afspraak/{{$afs->afspraak->id}}/verwijderen"><i
                                                                    class="material-icons">delete</i></a><a
                                                                href="/afspraak/{{$afs->afspraak->id}}/wijzigen"><i
                                                                    class="material-icons">edit</i></a>


																			 <?php $totaldisc = 0.00; $totaal = 0.00; ?>

                                                        @foreach($afs->afspraak->aha as $afsa)

                                                            @if($afs->afspraak->id == $afsa->afspraak->id)

                                                                <br>{{$afsa->aantal}}
                                                                x {{$afsa->activiteit->omschrijving}}
                                                                <i>{{$afsa->opmerking}}</i> à
                                                                € {{number_format($afsa->prijs,2)}} =
                                                                € {{number_format(($afsa->aantal * $afsa->prijs),2)}}



																					 <?php $totaal += ($afsa->prijs * $afsa->aantal - $afsa->kortingsbedrag); ?>



                                                                @if($afsa->kortingsbedrag != 0)

                                                                    <em> Korting:
                                                                        € {{number_format($afsa->kortingsbedrag, 2)}} </em>

                                                                @endif



																					 <?php $totaldisc = $totaldisc + $afsa->kortingsbedrag; ?>

                                                            @endif

                                                        @endforeach



                                                        @if($totaal < $afs->afspraak->totaalbedrag)

                                                            <br>Nog te betalen
                                                            € {{number_format($afs->afspraak->totaalbedrag - $totaal, 2)}}

                                                        @endif


                                                        <br>{!!$afs->afspraak->omschrijving!!}<br>


                                                        @if($afs->afspraak->afspraak_id != null)

                                                            <i class="uk-text-primary">Verwijzing van afspraak:
                                                                <b>{{$afs->afspraak->afspraak_id}}</b></i><br>

                                                        @endif



                                                        @if($totaldisc != 0)

                                                            <strong><em>Totale korting:
                                                                    € {{number_format($totaldisc,2)}}</em></strong>

                                                        @endif


                                                        <strong><br>Totaalbedrag: €

                                                            @if($totaal - $totaldisc != $afs->afspraak->totaalbedrag && $totaal + $totaldisc != $afs->afspraak->totaalbedrag)

                                                                {{number_format($afs->afspraak->totaalbedrag, 2)}}

                                                            @else

                                                                {{number_format(($afs->afspraak->totaalbedrag - $totaldisc),2)}}

                                                            @endif</strong>


                                                        @if($afs->afspraak->niet_betaald > 0)

                                                            @if($afs->afspraak->getAmount($afs->afspraak->id) != 0)

                                                                <br><strong class="uk-text-bold md-color-red-800">Openstaand:
                                                                    € {{number_format($afs->afspraak->niet_betaald,2)}} </strong>

                                                            @endif

                                                        @endif

                                                        @if($afs->afspraak->initiatief == 1)

                                                            <br> Initiatief van deze afspraak ligt bij de klant

                                                        @else

                                                            <br> Initiatief van deze afspraak ligt bij ons

                                                        @endif

                                                        <br> Toegevoegde geschiedenis: <br>

                                                        @foreach($afs->afspraak->getHistoryFromThisAppointment() as $h)

                                                            {{$h->geschiedenis->omschrijving}};

                                                        @endforeach

                                                        <div class="right">


                                                            @if($afs->status_id != 23 && Auth::user()->rol < 7)

                                                                @if($afs->status_id == 1 &&  $afs->afspraak->afspraaktype_id != 6)

                                                                    <a href="/afspraak/{{$afs->afspraak->id}}/verwerken"
                                                                       class="md-btn md-btn-success md-btn-small md-btn-wave-light">Verwerk
                                                                        afspraak</a>

                                                                @elseif($afs->status_id==2 && $afs->afspraak->is_verzet == 0 && $afs->afspraak->hasNewAppointment($afs->afspraak->id) != true)

                                                                    <a href="/afspraak/{{$afs->afspraak->id}}/verwerken/nietgelukt/nieuweafspraak/0"
                                                                       class="md-btn md-btn-warning md-btn-small md-btn-wave-light">Verzet
                                                                        afspraak</a>

                                                                    <a class="md-btn md-btn-danger md-btn-small md-btn-wave-light"
                                                                       href="/afspraak/maken/{{$afs->afspraak->id}}/nietnodig/profiel">Geen
                                                                        nieuwe afspraak</a>

                                                                @elseif($afs->status_id == 3 && $afs->afspraak->niet_betaald > 0 && $afs->afspraak->getAmountOpen($afs->afspraak->id) < 1)

                                                                    <a href="/afspraak/{{$afs->afspraak->id}}/verwerken/nietgelukt/nieuweafspraak/3"
                                                                       class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Maak
                                                                        betaal afspraak</a>

                                                                @endif

                                                                @if( ($afs->status_id == 3 || $afs->status_id == 4) && $afs->afspraak->afspraaktype_id == 1)

                                                                    <a href="/afspraak/{{$afs->afspraak->id}}/garantie/afspraak/maken"
                                                                       class="md-btn md-btn-success md-btn-small md-btn-wave-light">Maak
                                                                        Garantie afspraak</a>

                                                                @endif

                                                                @if($afs->afspraak->afspraaktype_id == 6)

                                                                    <a href="/afspraak/{{$afs->afspraak->id}}/garantie/afspraak/maken"
                                                                       class="md-btn md-btn-success md-btn-small md-btn-wave-light">Terug
                                                                        Betaald</a>

                                                                @endif

                                                                {{-- if($afs->status_id < 2) --}}

                                                                <a href="/afspraak/{{$afs->afspraak->id}}/nazorg"
                                                                   class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Verstuur
                                                                    Evaluatie</a>

                                                                {{-- <a href="/afspraak/{{$afs->afspraak->id}}/boom" class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Toon Afspraakboom</a> --}}

                                                                <a href="/afspraak/{{$afs->afspraak->id}}/offerte"
                                                                   class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Voeg
                                                                    Offerte toe</a>

                                                                @if($afs->afspraak->factuur_id == null && $afs->status_id != 2 && $afs->afspraak->afspraaktype_id == 1)

                                                                    @if($afs->afspraak->afspraak_id == null)

                                                                        <a href="/afspraak/{{$afs->afspraak->id}}/factuur"
                                                                           class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Genereer
                                                                            Factuur</a>

                                                                    @endif

                                                                @endif

                                                                @if($afs->status_id == 2 || $afs->status_id == 23 || $afs->status_id == 30 || $afs->status_id == 29)

                                                                    <a href="/veranderEigenschuld/{{$afs->afspraak->id}}"
                                                                       class="md-btn md-btn-danger md-btn-small md-btn-wave-light">Wijzig
                                                                        eigenschuld</a>

                                                                @endif

                                                                <!-- @ endif -->

                                                                @if($afs->status_id == 1)

                                                                    <a href="/afspraak/{{$afs->afspraak->id}}/annuleren"
                                                                       class="md-btn md-btn-danger md-btn-small md-btn-wave-light">Annuleer
                                                                        Afspraak</a>

                                                                    <a href="/afspraak/{{$afs->afspraak->id}}/voeg-geschiedenis-toe"
                                                                       class="md-btn md-btn-success md-btn-small md-btn-wave-light">Voeg
                                                                        geschiedenis toe</a>

                                                                @endif

                                                            @endif

                                                        </div>

                                                    </div>

                                                </div>

                                            @endforeach

                                        @endif

                                    </div>

                                </li>

                                <li>

                                    {{-- <a class="md-btn md-btn-primary md-btn-wave-light" href="/relatie/{{$relatie->id}}/geschiedenis/tabel">*werkt niet* Tabel view</a> --}}

                                    <h1></h1>

                                    <h4 class="heading_c uk-margin-bottom">Geschiedenis</h4>

                                    <!-- Foreach geschiedenisregels as regel -->

                                    <div class="timeline">


                                        @foreach($array as $regel)

                                            <div class="timeline_item">

                                                <div class="timeline_icon timeline_icon_success"><i
                                                            class="material-icons">&#xE85D;</i></div>

                                                <div class="timeline_date">

                                                    <span>{{ date('d-M-Y', strtotime($regel['datum'])) }}</span><br>

                                                </div>

                                                <div class="timeline_content">

                                                    {{-- {!! App\Models\Geschiedenis::getSalesman($regel['afspraak_id']) !!} --}}

                                                    {!! $regel['omschrijving'] !!}



                                                    {{-- {!! $regel->omschrijving !!}. {!! $regel->getSalesman() !!}  --}}

                                                    @if(isset($regel['advies']))

                                                        <br>

                                                        <span class="uk-text-bold">

                                            {!! $regel['advies'] !!}

                                        </span>

                                                        <br>



                                                        @if($regel['afspraak_id'] != null)

                                                            @if(App\Models\Appointment::where('id', $regel['afspraak_id'])->exists())

                                                                @if(App\Models\Appointment::find($regel['afspraak_id'])->getType()->id == 2)
                                                                    {{-- Now we know its an offerte --}}

                                                                    @if(App\Models\Offerte::where('afspraak_id', $regel['afspraak_id'])->exists())

                                                                        <span class="uk-text-bold">Aanbieding op offerte:</span>

                                                                        {!! App\Models\Offerte::where('afspraak_id', $regel['afspraak_id'])->first()->aanbieding !!}

                                                                    @endif

                                                                @endif

                                                            @endif

                                                        @endif

                                                    @endif

                                                    @if(Auth::user()->rol < 7)
                                                        @if($regel['afspraak_id'] == null)

                                                            <a href="/geschiedenis/{{$regel['id']}}/wijzigen">

                                                                <i class="material-icons">edit</i>

                                                            </a>

                                                            <a href="/geschiedenis/{{$regel['id']}}/verwijderen">

                                                                <i class="material-icons">delete</i>

                                                            </a>

                                                        @else

                                                            <a data-uk-modal="{target:'#modal_header_footergeschiedenis{{$regel['afspraak_id']}}'}"><i
                                                                        class="material-icons">delete</i></a>

                                                            <div class="uk-modal"
                                                                 id="modal_header_footergeschiedenis{{$regel['afspraak_id']}}">

                                                                <div class="uk-modal-dialog">

                                                                    <div class="uk-modal-header">

                                                                        <h3 class="uk-modal-title">

                                                                            Alert!

                                                                        </h3>

                                                                    </div>

                                                                    <p>Weet u zeker dat u deze geschiedenis set wilt
                                                                        verwijderen?</p>

                                                                    <div class="uk-modal-footer uk-text-right">

                                                                        <a href="/relatie/{{$relatie->id}}/geschiedenis/{{$regel['afspraak_id']}}/set"
                                                                           class="md-btn md-btn-flat">Ja</a>

                                                                        <button type="button"
                                                                                class="md-btn md-btn-flat uk-modal-close">
                                                                            Nee
                                                                        </button>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        @endif

                                                    @endif

                                                </div>

																  <?php $user_array = explode(',', $regel['gebruiker_id']); ?>

                                                <div class="timeline_content"><small>Geschiedenisregel gemaakt
                                                        door: @foreach($user_array as $user)
                                                            {{ App\Models\User::getName($user).' & ' }}
                                                        @endforeach

                                                        <br>

                                                        @if($regel['updated_at'] != $regel['created_at'])

                                                            {{-- Geüpdatet op: {{date('d-M-Y', strtotime($regel->updated_at))}} door: @foreach($users as  $user)@if($user->id == $regel['bijgewerkt_door']) {{$user->name}} @endif @endforeach --}}

                                                        @endif

                                                    </small></div>

                                            </div>

                                        @endforeach

                                    </div>

                                </li>

                                <li>


                                    <h4 class="heading_c uk-margin-bottom">Facturen</h4>

                                    <!-- Foreach geschiedenisregels as regel -->

                                    <div class="dt_colVis_buttons"></div>

                                    <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">

                                        <thead>

                                        <tr>

                                            <th>Fact.nr.</th>

                                            <th>Datum</th>

                                            <th>Betaaldatum</th>

                                            <th>Totaalbedrag</th>

                                            <th>Aangemaakt Door</th>

                                            <th>Status</th>

                                            <th>Betaald</th>

                                            <th>Functie</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        @foreach($allefacturen as $af)

                                            <tr>

                                                <td width="11%"><a target="_blank"
                                                                   href="/factuur/{{$af->factuur->id}}">{{$af->factuur->id}}</a>
                                                </td>

                                                <td width="11%"><a target="_blank"
                                                                   href="/factuur/{{$af->factuur->id}}">{{$af->factuur->datum}}</a>
                                                </td>

                                                <td width="11%"><a target="_blank"
                                                                   href="/factuur/{{$af->factuur->id}}">{{$af->factuur->betaaldatum}}</a>
                                                </td>

                                                <td width="11%"><a target="_blank" href="/factuur/{{$af->factuur->id}}">€ {{number_format($af->factuur->totaalbedrag,2)}}</a>
                                                </td>

                                                <td width="11%"><a target="_blank"
                                                                   href="/factuur/{{$af->factuur->id}}">@foreach($users as $user)

                                                            @if($user->id == $af->factuur->aangemaakt_door)

                                                                {{$user->name}}

                                                            @endif

                                                        @endforeach

                                                    </a></td>

                                                <td width="11%"><a target="_blank"
                                                                   href="/factuur/{{$af->factuur->id}}">{{$af->status->omschrijving}}</a>
                                                </td>

                                                <td width="11%">

                                                    @if($af->factuur->betaaldatum < date('d-m-Y') && $af->factuur->voldaan == 0)

                                                        <span class="uk-text-danger">Betaaltermijn verstreken!</span>

                                                    @elseif($af->factuur->voldaan == 1)

                                                        <span class="uk-text-success">Betaald</span>

                                                    @else

                                                        <span class="uk-text-warning">Betaling in afwachting</span>

                                                    @endif

                                                </td>

                                                <td width="23%">

                                                    @if($af->factuur->voldaan == 0  && Auth::user()->rol < 7)

                                                        <a href="/factuur/{{$af->factuur->id}}/betaald"
                                                           class="md-btn md-btn-success md-btn-wave-light">Betaald</a>
                                                        &nbsp;

                                                    @elseif($af->factuur->voldaan == 0 && $af->factuur->betaaldatum >= date('d-m-Y')  && Auth::user()->rol < 7)

                                                        <a href="/debiteuren/open#{{$af->factuur->id}}"
                                                           class="md-btn md-btn-success md-btn-wave-light"> Stuur
                                                            reminder</a>&nbsp;

                                                    @endif

                                                    @if(Auth::user()->rol < 7)

                                                        <a href="/relatie/{{$relatie->id}}/factuur/{{$af->factuur->id}}/share"
                                                           class="md-btn md-btn-primary md-btn-small md-btn-wave-light"><i
                                                                    class="material-icons">send</i></a> &nbsp;

                                                        <a href="/factuur/{{$af->factuur->id}}/verwijderen"
                                                           class="md-btn md-btn-danger md-btn-small md-btn-wave-light"><i
                                                                    class="material-icons">delete</i></a> &nbsp;

                                                    @endif

                                                </td>

                                            </tr>

                                        @endforeach

                                        </tbody>

                                    </table>

                                </li>

                                <li>

                                    <h4 class="heading_c uk-margin-bottom">Offertes</h4>

                                    <!-- Foreach geschiedenisregels as regel -->

                                    <div class="dt_colVis_buttons"></div>

                                    <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">

                                        <thead>

                                        <tr>

                                            <th>Offerte.nr.</th>

                                            <th>Datum</th>

                                            <th>Vervaldatum</th>

                                            <th>Opgesteld door</th>

                                            <th>Aangemaakt Door</th>

                                            <th>Status</th>

                                            <th>Totaalbedrag</th>

                                            <th>Omschrijving</th>

                                            <th>Functie</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        @foreach($alleoffertes as $af)

                                            <tr>

                                                <td><a target="_blank"
                                                       href="/offerte/{{$af->offerte->id}}">{{$af->offerte->id}}</a>
                                                </td>

                                                <td><a target="_blank"
                                                       href="/offerte/{{$af->offerte->id}}">{{$af->offerte->datum}}</a>
                                                </td>

                                                <td><a target="_blank"
                                                       href="/offerte/{{$af->offerte->id}}">{{$af->offerte->vervaldatum}}</a>
                                                </td>

                                                <td><a target="_blank"
                                                       href="/offerte/{{$af->offerte->id}}">{{$af->offerte->werknemer->voornaam." ".$af->offerte->werknemer->achternaam}}

                                                    </a></td>

                                                <td><a target="_blank"
                                                       href="/offerte/{{$af->offerte->id}}">{{$af->offerte->user->name}}</a>
                                                </td>

                                                <td><a target="_blank"
                                                       href="/offerte/{{$af->offerte->id}}">{{$af->status->omschrijving}}</a>
                                                </td>

                                                <td><a target="_blank"
                                                       href="/offerte/{{$af->offerte->id}}">€ {{number_format($af->offerte->totaalbedrag,2)}}</a>
                                                </td>

                                                <td><a target="_blank"
                                                       href="/offerte/{{$af->offerte->id}}">{{$af->offerte->opmerking}}</a>
                                                </td>

                                                <td>

                                                    @if(Auth::user()->rol < 7)

                                                        <a href="/relatie/{{$relatie->id}}/offerte/{{$af->offerte->id}}/share"
                                                           class="md-btn md-btn-primary md-btn-small md-btn-wave-light"><i
                                                                    class="material-icons">send</i></a> &nbsp;

                                                        <a href="/relatie/{{$relatie->id}}/offerte/{{$af->offerte->id}}/wijzigen"
                                                           class="md-btn md-btn-warning md-btn-small md-btn-wave-light"><i
                                                                    class="material-icons">edit</i></a> &nbsp;

                                                        @if(Auth::user()->rol < 7)

                                                            <a href="/offerte/{{$af->offerte->id}}/verwijderen"
                                                               class="md-btn md-btn-danger md-btn-small md-btn-wave-light"><i
                                                                        class="material-icons">delete</i></a> &nbsp;

                                                        @endif

                                                        @if(!$af->offerte->getFollowup())

                                                            <a href="/offerte/{{$af->offerte->id}}/honoreren"
                                                               class="md-btn md-btn-success md-btn-wave-light">Gehonoreerd</a>

                                                            <a href="/offerte/{{$af->offerte->id}}/vervallen"
                                                               class="md-btn md-btn-danger md-btn-wave-light">Vervallen</a>

                                                        @endif

                                                    @endif

                                                </td>

                                            </tr>

                                        @endforeach

                                        </tbody>

                                    </table>

                                </li>

                                <li>

                                    @if(Auth::user()->rol < 7)

                                        <a class="md-btn md-btn-primary md-btn-wave-light"
                                           href="/relatie/{{$relatie->id}}/crediteur">Voeg Terugbetaal verzoek toe</a>

                                    @endif

                                    <div class="dt_colVis_buttons"></div>

                                    <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">

                                        <thead>

                                        <tr>

                                            <th>Aangemaakt op</th>

                                            <th>Aangemaakt door</th>

                                            <th>Bedrag</th>

                                            <th>Betaaldatum</th>

                                            <th>Omschrijving</th>

                                            <th>Betalen</th>

                                            <th>Functie</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        @foreach($crediteuren as $crediteur)

                                            <tr>

                                                <td>{{date('d-m-Y', strtotime($crediteur->created_at))}}</td>

                                                <td>{{$crediteur->user->name}} </a></td>

                                                <td>€ {{number_format($crediteur->bedrag, 2)}}</td>

                                                <td>@if($crediteur->betaaldatum > date('d-m-Y')) @else <a
                                                            class="uk-text-danger"> @endif{{$crediteur->betaaldatum}}</a>
                                                </td>

                                                <td>{{$crediteur->omschrijving}}</td>

                                                <td>@if($crediteur->terugbetaald == 1)

                                                        Crediteur terugbetaald!

                                                    @else

                                                        @if(Auth::user()->rol < 7)

                                                            <a href="/crediteur/{{$relatie->id}}/{{$crediteur->bedrag}}/betaald"
                                                               class="md-btn md-btn-success md-btn-wave-light">Betaald</a>

                                                        @endif

                                                    @endif</td>

                                                <td>

                                                    @if(Auth::user()->rol < 7)

                                                        <a href="/crediteur/{{$crediteur->id}}/verwijderen"><i
                                                                    class="material-icons">delete</i></a>

                                                    @endif</td>

                                            </tr>

                                        @endforeach

                                        </tbody>

                                    </table>

                                </li>

                                <li>

                                    <h4 class="heading_c uk-margin-bottom">Verstuurde Emails/brieven</h4>

                                    <table name class="uk-table uk-text-nowrap">

                                        <thead>

                                        <tr>

                                            <th width="10%">Mailtype</th>

                                            <th width="15%">Onderwerp</th>

                                            <th width="25%">Inhoud</th>

                                            <th width="10%">Opsteller</th>

                                            <th width="10%">Verzonden op</th>

                                            <th width="5%">Per post</th>

                                            <th width="25%"></th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        @if(count($mails) > 0)

                                            @foreach($mails as $m)

                                                <tr>


                                                    <td width="10%">{{$m->mailtype()->omschrijving}}</td>

                                                    <td width="15%">{{$m->onderwerp}}</td>

                                                    <td width="25%">


                                                        <a data-uk-modal="{target:'#modal_header_footer_mail_{{$m->id}}'}">Lees
                                                            meer..</a>

                                                    </td>

                                                    @if($m->ontvangen == 2)

                                                        <td width="10%">{{"Fam. ".$relatie->achternaam}}</td>

                                                    @else

                                                        <td width="10%">{{$m->schrijver()->name}}</td>

                                                    @endif

                                                    <td width="10%">{{date('d-m-Y H:i', strtotime($m->created_at))}}</td>

                                                    <td width="5%">
                                                        <div id="post">@if($m->post == 0)
                                                                Nee
                                                            @else
                                                                Ja
                                                            @endif </div>
                                                    </td>

                                                    <td width="25%">

                                                        @if(Auth::user()->rol < 7)

                                                            <div class="uk-button-dropdown"
                                                                 data-uk-dropdown="{mode:'click', pos:'right-center'}">

                                                                <button class="md-btn">Opties<i class="material-icons">&#xE315;</i>
                                                                </button>

                                                                <div class="uk-dropdown">

                                                                    <ul class="uk-nav uk-nav-dropdown">

                                                                        <li>
                                                                            <a href="/relatie/{{$relatie->id}}/post/{{$m->id}}/change/0">Alleen
                                                                                per mail verstuurd</a></li>

                                                                        <li>
                                                                            <a href="/relatie/{{$relatie->id}}/post/{{$m->id}}/change/1">Per
                                                                                post en per mail verstuurd</a></li>

                                                                        <li>
                                                                            <a href="/relatie/{{$relatie->id}}/post/{{$m->id}}/change/2">Alleen
                                                                                per post verstuurd</a></li>

                                                                        {{-- <li><a href="/relatie/{{$relatie->id}}/voorbeeldbrief/{{$m->afspraak_id}}" target="_blank" data-uk-tooltip="{pos:'right'}" title="Opent in nieuw venster">Bekijk email</a></li> --}}

                                                                    </ul>

                                                                </div>

                                                            </div>

                                                        @endif

                                                    </td>

                                                </tr>

                                            @endforeach

                                        @endif

                                        </tbody>

                                    </table>

                                    <br>

                                </li>

                                <li>

                                    <h4 class="heading_c uk-margin-bottom">Mogelijke € 15,- brieven</h4>

                                    <table class="uk-table uk-text-nowrap">

                                        <thead>

                                        <tr>

                                            <th>#</th>

                                            <th>Afspraaktype</th>

                                            <th>Totaalbedrag</th>

                                            <th>Verwerkt door</th>

                                            <th>Huidige status</th>

                                            <th>Reden</th>

                                            <th></th>

                                        </tr>

                                        </thead>

                                        <tbody>

														 <?php $vijftien = $relatie->getVijftien(); ?>

                                        @if($vijftien != "")

                                            @if(count($vijftien) > 0)

                                                @foreach($vijftien as $v)

                                                    {{--  <tr>

                                                         <td width="5%">{{$v->afspraak_id}}</td>

                                                         <td width="16%">{{App\Models\Afspraaktype::getDescription($v->afspraak->afspraaktype_id)}}</td>

                                                         <td width="16%">€ {{number_format($v->totaalbedrag, 2)}}</td>

                                                         <td width="16%">{{App\Models\User::getName($v->afspraak->verwerkt_door)}}</td>

                                                         <td width="16%">{{$v->status->omschrijving}}</td>

                                                         <td width="16%">{{$v->afspraak->reden}}</td>

                                                         <td data-id="{{$v->afspraak_id}}" width="15%">

                                                             <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click', pos:'left-center'}">

                                                                 <button class="md-btn">Opties<i class="material-icons">&#xE315;</i></button>

                                                                 <div class="uk-dropdown">

                                                                     <ul class="uk-nav uk-nav-dropdown">

                                                                         @if(Auth::user()->rol < 7)

                                                                         <li><a data-uk-modal="{target:'#modal_no15_{{$v->afspraak_id}}'}">Geen € 15,- klant</a></li>



                                                                         <li><a data-uk-modal="{target:'#modal_tempFirst_{{$v->afspraak_id}}'}" >Verstuur brief (als mail of brief)</a></li>



                                                                         <li><a href="/relatie/{{$relatie->id}}/voorbeeldbrief/{{$v->afspraak_id}}" target="_blank" data-uk-tooltip="{pos:'right'}" title="Opent in nieuw venster">Bekijk voorbeeld brief

                                                                         @endif

                                                                         </a></li>

                                                                     </ul>

                                                                 </div>

                                                             </div>

                                                         </td>

                                                     </tr>

                                                     <div class="uk-modal" id="modal_no15_{{$v->afspraak_id}}">

                                                         <div class="uk-modal-dialog">

                                                             <p>U markeert de afspraak nu als 'geen €15,- klant', weet u het zeker?</p>

                                                             <a class="md-btn md-btn-success md-btn-wave-light" href="/veranderEigenschuld/{{$v->afspraakID}}">Ja</a>

                                                             <button type="button" class="md-btn md-btn-primary md-btn-wave-light uk-modal-close">Nee</button>

                                                         </div>

                                                     </div>

                                                     <div class="uk-modal" id="modal_tempFirst_{{$v->afspraak_id}}">

                                                         <div class="uk-modal-dialog">

                                                             <p>Wilt u wijzigingen aan de brief doorvoeren?</p>

                                                             <a class="md-btn md-btn-success md-btn-wave-light" data-uk-modal="{target:'#modal_tempFirst_{{$v->afspraak_id}}_edit'}" >Ja</a>

                                                             <a class="md-btn md-btn-primary md-btn-wave-light" href="/relatie/{{$relatie->id}}/verstuur/{{$v->afspraak_id}}/brief/1">Nee</a>

                                                         </div>

                                                     </div>

                                                     <div class="uk-modal" id="modal_tempFirst_{{$v->afspraak_id}}_edit">

                                                         <div class="uk-modal-dialog">

                                                             <button type="button" class="uk-modal-close uk-close"></button>

                                                             <h3 class="heading_a">Gegevens Wijzigen</h3><br>

                                                             {!! Form::open(array('url'=>'/relatie/'.$relatie->id.'/verstuur/'.$v->afspraak_id.'/brief/1/edit', 'method' => 'post', 'name' => 'editFirstReminder')) !!}



                                                             <label>Administratiekosten</label><br>

                                                             <input type="number" step='0.01' name="administratiekosten" value="25.00"><br><br>



                                                             <label>Totaalbedrag</label><br>

                                                             <input type="number" step='0.01' name="factuurbedrag" value="{{number_format($v->totaalbedrag,2)}}">



                                                             <br>

                                                             <div class="uk-grid" data-uk-grid-margin>

                                                                 <div class="uk-width-medium">

                                                                     <div class="uk-form-row">

                                                                         <div align="right">

                                                                             <div class="md-btn-group">

                                                                             <button class="md-btn md-btn-success md-btn-wave-light" type="submit">Verstuur</button>

                                                                             </div>

                                                                         </div>

                                                                     </div>

                                                                 </div>

                                                             </div>

                                                             <input type="hidden" name='afspraak_id' value="{{$v->afspraak_id}}">

                                                             {!! Form::close() !!}

                                                         </div>

                                                     </div> --}}

                                                @endforeach

                                            @endif

                                        @endif

                                        </tbody>

                                    </table>

                                    <br>

                                    <h4 class="heading_c uk-margin-bottom">Verstuurde Brieven</h4>

                                    <table class="uk-table uk-text-nowrap">

                                        <thead>

                                        <tr>

                                            <th>Mailtype</th>

                                            <th>Onderwerp</th>

                                            <th>Opsteller</th>

                                            <th>Verzonden op</th>

                                            <th></th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        @foreach($mails as $m)

                                            @if($m->mailtype < 5)

                                                <tr>

                                                    <td>{{$m->mailtype()->omschrijving}}</td>

                                                    <td>{{$m->onderwerp}}</td>

                                                    <td>{{$m->schrijver()->name}}</td>

                                                    <td>{{date('d-m-Y', strtotime($m->created_at))}}</td>

                                                    <td>
                                                        <a href="/relatie/{{$relatie->id}}/voorbeeldbrief/{{$m->afspraak_id}}"
                                                           target="_blank" data-uk-tooltip="{pos:'right'}"
                                                           title="Opent in nieuw venster">Bekijk Email</a></td>

                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>

                                    </table>

                                    <br>

                                </li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        @endif





        @if($relatie->klanttype_id==3)

            @foreach($mails as $m)

                <div class="uk-modal" id="modal_header_footer_mail_{{$m->id}}">

                    <div class="uk-modal-dialog">

                        <div class="uk-modal-header">

                            <h3 class="uk-modal-title">

                                Mailtype: {{$m->mailtype()->omschrijving}}

                                <br>Verstuurder: @if($m->ontvangen == 2)
                                    Fam. {{$relatie->achternaam}}
                                @else
                                    {{$m->schrijver()->name}}
                                @endif

                                <br>Onderwerp: {!!$m->onderwerp!!} {{-- <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">&#xE8FD;</i> --}}
                            </h3>

                        </div>

                        <p>{!!$m->omschrijving!!}</p>

                        <div class="uk-modal-footer uk-text-right">

                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Sluit bericht</button>

                        </div>

                    </div>

                </div>

            @endforeach

        @endif

    @endif

@endsection
