<div class="row">
    <div class="col-xl-12">
        <div class="table-responsive">
            <table id="clientHistoryDT" class="table table-bordered w-100">
                <thead>
                <tr>
                    <th style="width: 15%;">Datum</th>
                    <th style="width: 15%;">Afspraak</th>
                    <th style="width: 15%;">Door wie</th>
                    <th style="width: 55%;">Geschiedenis</th>
                </tr>
                </thead>
                <tbody>
                @foreach($client->history as $history)
                    <tr>
                        <td style="width: 15%;">
                            {{date('d-m-Y', strtotime($history->datum))}}
                        </td>
                        <td style="width: 15%;">
                            @if($history->afspraak_id !== null and $history->afspraak_id !== "")
                                @if($history->appointment == null)
                                    Verwijderd
                                @else
                                    {{date('d-m-Y', strtotime($history->appointment->startdatum))}}
                                @endif
                            @endif
                        </td>
                        <td style="width: 15%;">
                            @if($history->gebruiker_id == -1)
                                Systeem
                            @else
                                {{$history->createdBy->getFullName()}}
                            @endif
                        </td>
                        <td style="width: 55%;">
                            {!! $history->omschrijving !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
