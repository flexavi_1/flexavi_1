<div class="row">
    <div class="col-xl-12">
        <div class="table-responsive">
            <table id="clientLogsDT" class="table table-bordered w-100">
                <thead>
                <tr>
                    <th style="width: 15%;">Datum</th>
                    <th style="width: 15%;">Wie</th>
                    <th style="width: 55%;">Log</th>
                </tr>
                </thead>
                <tbody>
                @foreach($client->logs as $log)
                    <tr>
                        <td style="width: 15%;">
                            {{date('d-m-Y', strtotime($log->created_at))}}
                        </td>
                        <td style="width: 15%;">
                            @if($log->gebruiker_id == -1)
                                Systeem
                            @else
                                {{$log->createdBy->getFullName()}}
                            @endif
                        </td>
                        <td style="width: 55%;">
                            {!! $log->omschrijving !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
