@if($appointment->afspraaktype_id == \App\Models\AppointmentType::TYPE_DEFERRED || $appointment->afspraaktype_id == \App\Models\AppointmentType::TYPE_REMINDER)
<span class="avatar avatar-sm bg-warning-transparent avatar-rounded profile-timeline-avatar">
    <i class="bi bi-currency-euro" data-bs-toggle="tooltip" data-bs-placement="left" title="Betaalafspraak (contant of bank)"></i>
</span>
@endif

@if($appointment->afspraaktype_id == \App\Models\AppointmentType::TYPE_EXECUTE)
<span class="avatar avatar-sm bg-success-transparent avatar-rounded profile-timeline-avatar">
    <i class="bi bi-tools" data-bs-toggle="tooltip" data-bs-placement="left" title="Uitvoeren werkzaamheden"></i>
</span>
@endif


@if($appointment->afspraaktype_id == \App\Models\AppointmentType::TYPE_WARRANTY)
<span class="avatar avatar-sm bg-danger-transparent avatar-rounded profile-timeline-avatar">
        <i class="bi bi-hammer" data-bs-toggle="tooltip" data-bs-placement="left" title="Garantie"></i>
</span>
@endif


@if($appointment->afspraaktype_id == \App\Models\AppointmentType::TYPE_QUOTE)
<span class="avatar avatar-sm bg-primary-transparent avatar-rounded profile-timeline-avatar">
    <i class="bi bi-file-text" data-bs-toggle="tooltip" data-bs-placement="left" title="Offerte opstellen"></i>
</span>
@endif
