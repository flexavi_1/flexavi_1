<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1" id="actionlist-id">
    <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
        <a class="dropdown-item" href="javascript:void(0);">Verwerk afspraak</a>
    </li>
    @php $currentStatus = $appointment->getLastStatus(); @endphp
{{--    Process / cancel appointment--}}
    @if($currentStatus->id == \App\Models\Status::APT_MADE)
        <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
            <a class="dropdown-item" href="/afspraak/{{$appointment->id}}/verwerken">Verwerk afspraak</a>
        </li>
        <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
        @if($appointment->isMultipleDays())
            <a class="dropdown-item" href="/afspraak/{{$appointment->id}}/annuleren/{{App\Models\Werkdag::returnDate($afs->werkdag_id)}}">Verwerk afspraak</a>
        @else
            <a class="dropdown-item" href="/afspraak/{{$appointment->id}}/annuleren">Verwerk afspraak</a>
        @endif
        </li>
    @endif
{{--    Cancel / reschedule appointment--}}
    @if($currentStatus->id == \App\Models\Status::APT_FAILED and $appointment->hasNewAppointment())

        <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
            <a class="dropdown-item" href="/afspraak/{{$appointment->id}}/verwerken/nietgelukt/nieuweafspraak/0">Verzet afspraak</a>
        </li>
        <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
            <a class="dropdown-item" href="/afspraak/maken/{{$apointment->id}}/nietnodig/profiel">Geen nieuwe afspraak nodig</a>
        </li>
    @endif
{{--    Create Payment appointment - status = 3--}}
    @if($currentStatus->id == \App\Models\Status::APT_DONE_UNPAID and $appointment->hasNewAppointment([\App\Models\AppointmentType::TYPE_REMINDER, \App\Models\AppointmentType::TYPE_DEFERRED]))
        <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
            <a class="dropdown-item" href="/afspraak/{{$appointment->id}}/verwerken/nietgelukt/nieuweafspraak/3">Maak betaal afspraak</a>
        </li>
    @endif
{{--    Create Warranty--}}
    @if($currentStatus->id == \App\Models\Status::APT_DONE_UNPAID || $currentStatus->id == \App\Models\Status::APT_DONE_PAID)
        <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
            <a class="dropdown-item" href="/afspraak/{{$appointment->id}}/garantie/afspraak/maken">Maak garantie afspraak</a>
        </li>
    @endif
{{--    Change Own Fault--}}
    @if(in_array( $currentStatus->id, [\App\Models\Status::APT_FAILED, \App\Models\Status::APT_CANCELLED, \App\Models\Status::APT_FAILED_NEW_APPOINTMENT, \App\Models\Status::APT_FAILED_NO_NEW_APPOINTMENT]))
        <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
            <a class="dropdown-item" href="/veranderEigenschuld/{{$appointment->id}}">Maak garantie afspraak</a>
        </li>
    @endif
{{--   Reset appointment processing --}}
    @if($currentStatus->id !== \App\Models\Status::APT_MADE)
        <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
            <a class="dropdown-item" href="/afspraak/{{$appointment->id}}/reset/verwerking">Herstel Verwerking</a>
        </li>
    @endif
{{--    Default visible--}}
    <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
        <a class="dropdown-item" href="/afspraak/{{$appointment->id}}/voeg-geschiedenis-toe">Voeg geschiedenis toe</a>
    </li>
    <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
        <a class="dropdown-item" href="/afspraak/{{$appointment->id}}/offerte">Voeg offerte toe</a>
    </li>
</ul>
