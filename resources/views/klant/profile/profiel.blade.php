@extends('layouts.new')

@section('title', 'Profiel van '.$client->achternaam)
@section('page_title', 'Profiel van '.$client->achternaam)


@push('styles')
    <!-- Simplebar Css -->
    <link href="{{ URL::asset('new') }}/assets/libs/simplebar/simplebar.min.css" rel="stylesheet" >

    <link rel="stylesheet" href="{{ URL::asset('new') }}/assets/libs/glightbox/css/glightbox.min.css">
    <!-- Prism CSS -->
    <link rel="stylesheet" href="{{ URL::asset('new') }}/assets/libs/prismjs/themes/prism-coy.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.bootstrap5.min.css">
@endpush

@push('scripts')

    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <!-- Gallery JS -->
    <script src="{{ URL::asset('new') }}/assets/libs/glightbox/js/glightbox.min.js"></script>
    <!-- Internal Profile JS -->
    <script src="{{ URL::asset('new') }}/assets/js/profile.js"></script>
    <!-- Prism JS -->
    <script src="{{ URL::asset('new') }}/assets/libs/prismjs/prism.js"></script>
    <script src="{{ URL::asset('new') }}/assets/js/prism-custom.js"></script>
    <!-- Modal JS -->
    <script src="{{ URL::asset('new') }}/assets/js/modal.js"></script>
    <!-- Datatables Cdn -->
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.6/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
    <!-- Internal Datatables JS -->
    <script src="{{ URL::asset('new') }}/assets/js/datatables.js"></script>

@endpush

@section('content')
    <!-- Start::row-1 -->
    <div class="row">
        <div class="col-xxl-4 col-xl-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body p-0">
                    <div class="d-sm-flex align-items-top p-4 border-bottom-0 main-profile-cover">
                        <div class="flex-fill main-profile-info">
                            <div class="d-flex align-items-center justify-content-between">
                                <h6 class="fw-semibold mb-1 text-fixed-white">{{$client->getFullName()}}</h6>
                                <button class="btn btn-light btn-wave dropdown-toggle" type="button"
                                        id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    Menu
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="javascript:void(0);">Bewerk Klant</a></li>
                                    <li><a class="dropdown-item" href="javascript:void(0);">Afspraak maken</a></li>
                                    <li><a class="dropdown-item" href="javascript:void(0);">Emailadres toevoegen</a></li>
                                    <li><a class="dropdown-item" href="javascript:void(0);">Verwijder klant</a></li>
                                </ul>
                            </div>
                            <p class="mb-1 text-muted text-fixed-white op-7">{{$client->straat}} {{$client->huisnummer}} {{$client->hn_prefix}}</p>
                            <p class="fs-12 text-fixed-white mb-4 op-5">
                                <span class="me-3"><i class="ri-building-line me-1 align-middle"></i>{{$client->woonplaats}}</span>
{{--                                <span><i class="ri-map-pin-line me-1 align-middle"></i>Aangemaakt op: {{date('d-m-Y', strtotime($client->created_at))}}</span>--}}
                            </p>
                            <div class="d-flex mb-0">
                                <div class="me-4">
                                    <p class="fw-bold fs-20 text-fixed-white text-shadow mb-0">{{count($client->appointments)}}</p>
                                    <p class="mb-0 fs-11 op-5 text-fixed-white">Afspraken</p>
                                </div>
                                <div class="me-4">
                                    <p class="fw-bold fs-20 text-fixed-white text-shadow mb-0">{{count($client->appointments->where('is_verzet', 1))}}</p>
                                    <p class="mb-0 fs-11 op-5 text-fixed-white">Aantal keer Verzet</p>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="p-4 border-bottom border-block-end-dashed">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="d-flex flex-wrap align-items-center">
                                    <div class="me-2 fw-semibold">
                                        Name :
                                    </div>
                                    <span class="fs-12 text-muted">{{$client->getFullName()}}</span>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="d-flex flex-wrap align-items-center">
                                    <div class="me-2 fw-semibold">
                                        Adres :
                                    </div>
                                    <span class="fs-12 text-muted">{{str_replace('<br>', " ", $client->getFullAddress())}}</span>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="d-flex flex-wrap align-items-center">
                                    <div class="me-2 fw-semibold">
                                        Email :
                                    </div>
                                    <span class="fs-12 text-muted">{{$client->email}}</span>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="d-flex flex-wrap align-items-center">
                                    <div class="me-2 fw-semibold">
                                        Telefoon :
                                    </div>
                                    <span class="fs-12 text-muted">{{$client->telefoonnummer_prive}}</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="p-4 border-bottom border-block-end-dashed">
                        <p class="fs-15 mb-2 me-4 fw-semibold">Overige (contact) informatie:</p>
                        <div class="text-muted">
                            @if($client->telefoonnummer_zakelijk != null)
                            <p class="mb-2">
                                <span class="avatar avatar-sm avatar-rounded me-2 bg-light text-muted">
                                    <i class="ri-phone-line align-middle fs-14"></i>
                                </span>
                                {{$client->telefoonnummer_zakelijk}}
                            </p>
                            @endif
                            @foreach($client->clientemails as $email)
                            <p class="mb-2">
                                <span class="avatar avatar-sm avatar-rounded me-2 bg-light text-muted">
                                    <i class="ri-mail-line align-middle fs-14"></i>
                                </span>
                                {{$email->email}}
                            </p>
                            @endforeach
                            <p class="mb-2">
                                <span class="avatar avatar-sm avatar-rounded me-2 bg-light text-muted">
                                    <i class="ri-map-pin-line align-middle fs-14"></i>
                                </span>
                               Geschreven door: {{$client->writtenBy->getFullName()}} ({{date('d-m-Y', strtotime($client->geschreven_op))}})
                            </p>
                            <p class="mb-2">
                                <span class="avatar avatar-sm avatar-rounded me-2 bg-light text-muted">
                                    <i class="ri-map-pin-line align-middle fs-14"></i>
                                </span>
                                Aangemaakt door: {{$client->processedBy->getFullName()}} ({{date('d-m-Y', strtotime($client->created_at))}})
                            </p>
                            <p class="mb-2">
                                <span class="avatar avatar-sm avatar-rounded me-2 bg-light text-muted">
                                    <i class="ri-map-pin-line align-middle fs-14"></i>
                                </span>
                                Initiatief: @if($client->initiatief != 1) Ligt bij Klant @else Ligt bij ons @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xxl-8 col-xl-12">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card custom-card">
                        <div class="card-body p-0">
                            <div class="p-3 border-bottom border-block-end-dashed d-flex align-items-center justify-content-between">
                                <div>
                                    <ul class="nav nav-tabs mb-0 tab-style-6 justify-content-start" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link active" id="activity-tab" data-bs-toggle="tab"
                                                    data-bs-target="#activity-tab-pane" type="button" role="tab"
                                                    aria-controls="activity-tab-pane" aria-selected="true"><i
                                                    class="ri-calendar-event-line me-1 align-middle d-inline-block"></i>Afspraken</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="posts-tab" data-bs-toggle="tab"
                                                    data-bs-target="#posts-tab-pane" type="button" role="tab"
                                                    aria-controls="posts-tab-pane" aria-selected="false"><i
                                                    class="ri-article-line me-1 align-middle d-inline-block"></i>Geschiedenis</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="logging-tab" data-bs-toggle="tab"
                                                    data-bs-target="#logging-tab-pane" type="button" role="tab"
                                                    aria-controls="logging-tab-pane" aria-selected="false"><i
                                                    class="ri-time-line me-1 align-middle d-inline-block"></i>Logs</button>
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                    <p class="fw-semibold mb-2">Eerst volgende afspraak:
                                        @php $nextApt = $client->getNextAppointment(); @endphp
                                        @if($nextApt != "--")
                                        <a href="/planning/verkoop/{{$nextApt}}" class="text-primary fs-12">
                                        @else
                                        <a href="javascript:void(0);" class="text-primary fs-12">
                                        @endif
                                                {{$nextApt}}
                                        </a>
                                    </p>
                                    <div class="progress progress-xs progress-animate">
                                        <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="p-3">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane show active fade p-0 border-0" id="activity-tab-pane"
                                         role="tabpanel" aria-labelledby="activity-tab" tabindex="0">
                                        @include('klant.profile.appointments')
                                    </div>
                                    <div class="tab-pane fade p-0 border-0" id="posts-tab-pane"
                                         role="tabpanel" aria-labelledby="posts-tab" tabindex="0">
                                        @include('klant.profile.history')
                                    </div>
                                    <div class="tab-pane fade p-0 border-0" id="logging-tab-pane"
                                         role="tabpanel" aria-labelledby="followers-tab" tabindex="0">
                                        @include('klant.profile.log')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End::row-1 -->
@endsection
