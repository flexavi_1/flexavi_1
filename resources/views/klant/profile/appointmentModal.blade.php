<div class="modal fade" id="appointmentModal-{{$appointment->id}}" tabindex="-1" aria-labelledby="appointmentModal-id" data-bs-keyboard="false"  aria-hidden="true">
    <!-- Scrollable modal -->
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="staticBackdropLabel2">Afspraak gegevens</h6>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row mb-5">
                    <div class="col-md-6">
                        {{--                                                                                Header data--}}
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td colspan="2"><h6 class="link-primary link-offset-2">Algemene informatie</h6></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><b>Type</b></td>
                                    <td>
                                        {{$appointment->type->omschrijving}}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Status</b></td>
                                    <td>
                                        {{$appointment->getLastStatus()->status->omschrijving}}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Start- & Einddatum</b></td>
                                    <td>
                                        {{$appointment->getDateString()}}
                                    </td>
                                </tr>
                                @if($appointment->isMultipleDays())
                                    <tr>
                                        <td><b>Subafspraken</b></td>
                                        <td>
                                            {{count($appointment->countSubAppointments())}}
                                        </td>
                                    </tr>
                                @endif
                                @if($appointment->afspraaktype_id == \App\Models\AppointmentType::TYPE_EXECUTE)
                                    <tr>
                                        <td><b>Bedrag</b></td>
                                        <td>
                                            € {{number_format($appointment->totaalbedrag, 2)}}
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td><b>Initiatief</b></td>
                                    <td>@if($appointment->initiatief == 1) Ligt bij klant @else Ligt bij ons @endif</td>
                                </tr>
                                @if($appointment->isPlanned())
                                    <tr>
                                        <td><b>Ingedeeld bij</b></td>
                                        <td>
                                            {{$appointment->getPlannedEmployeesString()}}
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td><b>Omschrijving</b></td>
                                    <td>
                                        {!! $appointment->omschrijving !!}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td colspan="2"><h6 class="link-primary link-offset-2"> Afspraak informatie </h6></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><b>Geschreven door</b></td>
                                    <td>
                                        {{$appointment->writtenBy?->getFullName()}}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Aangemaakt door</b></td>
                                    <td>
                                        {{$appointment->processedBy->getFullName()}}
                                        <small>(Op {{date('d-m-Y H:i', strtotime($appointment->created_at))}})</small></td>
                                </tr>
                                @if($appointment->bijgewerkt_door !== 0)
                                    <tr>
                                        <td><b>Laatst bijgewerkt door</b></td>
                                        <td>
                                            {{$appointment->editedBy->getFullName()}}
                                            <small>(Op  {{date('d-m-Y H:i', strtotime($appointment->updated_at))}})</small>
                                        </td>
                                    </tr>
                                @endif
                                @if($appointment->is_verzet == 1)
                                    <tr>
                                        <td><b>Verzet</b></td>
                                        <td>
                                            Afspraak is al eens verzet: <br>
                                            <span class="text-danger">{!! $appointment->reden !!}</span>
                                        </td>
                                    </tr>
                                @endif
                                @if($appointment->afspraaktype_id == \App\Models\AppointmentType::TYPE_EXECUTE)
                                    <tr>
                                        <td><b>Betaalstatus</b></td>
                                        <td>
                                            @if($appointment->betaald == 1)
                                                Ja
                                            @else
                                                Nee, er staat nog € {{number_format($appointment->niet_betaald, 2)}} open
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Betaalmethode</b></td>
                                        <td>
                                            {{$appointment->getFirstPaymentAppointment()->getPaymentMethodString()}}
                                        </td>
                                    </tr>
                                @endif
                                @if($appointment->verwerkt_door !== 0)
                                    <tr>
                                        <td><b>Advies</b></td>
                                        <td>{{$appointment->advies}}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{--                                                                        Lines--}}
                <div class="row mb-2">
                    @if(!empty($appointment->hasWork))
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <h6 class="link-primary">Uit te voeren werk</h6>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Aantal</th>
                                        <th>Werk</th>
                                        <th>Prijs</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($appointment->hasWork as $work)
                                        <tr>
                                            <td>{{$work->aantal}}</td>
                                            <td>
                                                @if($work->opmerking != "" || !empty($work->opmerking))
                                                    {{$work->opmerking}}
                                                @else
                                                    {{$work->work->omschrijving}}
                                                @endif
                                            </td>
                                            <td>€ {{number_format($work->prijs, 2)}}</td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2"><p class="link-primary link-offset-2" style="text-align: right !important;">Totaal ex btw</p></td>
                                        <td><i>€
                                                {{number_format($appointment->totaalbedrag / 1.21, 2)}}
                                            </i></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><p class="link-primary link-offset-2" style="text-align: right !important;">Totale btw</p></td>
                                        <td><i>€ {{number_format($appointment->totaalbedrag / 1.21 * 0.21)}}</i></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><p class="link-primary link-offset-2" style="text-align: right !important;">Totaal</p></td>
                                        <td><b>€ {{number_format($appointment->totaalbedrag)}}</b></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    @endif
                    {{--                                                                        History--}}
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <h6 class="link-primary">Geschiedenis</h6>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Datum</th>
                                    <th>Wie</th>
                                    <th>Geschiedenis</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($appointment->getLastHistory() as $history)
                                    <tr>
                                        <td>{{$history->created_at->format('d-m-Y H:i')}}</td>
                                        <td>
                                            {{$history->createdBy?->getFullName()}}
                                        </td>
                                        <td>{!! $history->omschrijving !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <a href="/afspraak/{{$appointment->id}}/wijzigen" class="btn btn-primary">Afspraak bewerken</a>
            </div>
        </div>
    </div>
</div>
