<ul class="list-unstyled profile-timeline">
    @foreach($client->appointments as $appointment)
        <li>
            <div>
                @include('klant.profile.appointmentIcon')
            </div>
            <div class="mb-2">
                <p class="h6 text-primary">{{$appointment->getDateString()}} - <em>Status:
                        {{$appointment->getLastStatus()->status->omschrijving}}</em></p>
                @if($appointment->isMultipleDays())
                    <span class="float-end fs-11 text-muted">{{ $appointment->getNthDayString() }}</span>
                @endif
            </div>
            <div class="mb-4">
                <b class="text-primary">Omschrijving</b> <br>
                {!! $appointment->omschrijving !!}
            </div>
            @if(!empty($appointment->advies))
                <div class="mb-4">
                    <b class="text-warning">Advies achtergelaten door {{$appointment->getSalesMan()->getFullname()}}</b>
                    <br>
                    {!! $appointment->advies !!}
                </div>
            @endif
            @if(!empty($appointment->history))
                <div class="mb-4">
                    <b class="text-danger">Laatste 3 geschiedenisregels</b>
                    <ul class="list-group">
                        @foreach($appointment->getLastHistory(3) as $history)
                            <li style="padding-inline-start: inherit !important;  margin-block-end: inherit !important;">
                                <p>{{strip_tags($history->omschrijving)}}</p>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="mb-4">
                <button type="button" class="btn btn-primary m-1" data-bs-toggle="modal"
                        data-bs-target="#appointmentModal-{{$appointment->id}}">Volledige afspraak inzien
                </button>
                @include('klant.profile.appointmentModal')
                <button class="btn btn-light btn-wave dropdown-toggle" type="button"
                        id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    Acties
                </button>
                @include('klant.profile.appointmentMenu')
            </div>
            <hr class="border-primary-subtle border-3 opacity-75 mt-3 mb-3">
        </li>
    @endforeach
</ul>
