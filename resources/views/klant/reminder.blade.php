@extends('layouts.new')

@section('title', 'Terug bellijst')
@section('page_title', 'Terug bellijst')
@push('styles')
    <!-- FlatPickr CSS -->
    <link rel="stylesheet" href="{{ URL::asset('new') }}/assets/libs/flatpickr/flatpickr.min.css">
    <link rel="stylesheet" href="{{ URL::asset('new') }}/assets/libs/quill/quill.snow.css">
    <link rel="stylesheet" href="{{ URL::asset('new') }}/assets/libs/quill/quill.bubble.css">
@endpush
@push('scripts')

    <!-- Date & Time Picker JS -->
    <script src="{{ URL::asset('new') }}/assets/libs/flatpickr/flatpickr.min.js"></script>
    <script src="{{ URL::asset('new') }}/assets/js/date&time_pickers.js"></script>
    <!-- Quill Editor JS -->
    <script src="{{ URL::asset('new') }}/assets/libs/quill/quill.min.js"></script>

    <!-- Internal Quill JS -->
    {{--    <script src="{{ URL::asset('new') }}/assets/js/quill-editor.js"></script>--}}

    <!-- Internal Datatables JS -->
    <script src="{{ URL::asset('new') }}/assets/flexavi_js/fetching.js"></script>
{{--    <script defer src="{{ URL::asset('new') }}/assets/flexavi_js/reminder.js"></script>--}}

    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <!-- Datatables Cdn -->
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.6/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>

    <!-- Internal Datatables JS -->
    <script src="{{ URL::asset('new') }}/assets/js/datatables.js"></script>

@endpush
@section('content')

    @if(Auth::user()->rol != 99)
        <!-- Start::row-1 -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card custom-card">
                    <div class="card-header">
                        <div class="card-title">
                            Reminder
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="reminderDT" class="table table-bordered text-nowrap w-100">
                                <thead>
                                <tr>
                                    <th>Reminder datum</th>
                                    <th>Naam</th>
                                    <th>Adres</th>
                                    <th>Contactgegevens</th>
                                    <th>Dagen tot reminder</th>
                                    <th> </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($relaties as $relatie)
                                    <tr>
                                        <td>{{$relatie->reminderdatum}}</td>
                                        <td><a href="/relatie/{{$relatie->id}}">{{$relatie->voornaam}} {{$relatie->achternaam}}</a></td>
                                        <td><a href="/relatie/{{$relatie->id}}">{{$relatie->straat}} {{$relatie->huisnummer}} {{$relatie->hn_prefix}}, <br> {{$relatie->postcode}} {{$relatie->woonplaats}}</a></td>
                                        <td><a href="/relatie/{{$relatie->id}}">{{$relatie->telefoonnummer_prive}} || {{$relatie->telefoonnummer_zakelijk}} <br>{{$relatie->email}} </a></td>
                                        @php $reminderResult = \App\Models\DateConverter::daysToReminder(365 - $relatie->verschil, $relatie->laatste_update); @endphp
                                        <td data-sort="{{$reminderResult['days']}}">
                                            <a href="/relatie/{{$relatie->id}}">
                                                {{ $reminderResult['string']  }}
                                            </a>
                                        </td>
                                        <td><a href="/reminder/{{$relatie->id}}/behandeld" class="btn btn-success-light btn-wave">Behandeld</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End::row-1 -->

{{--<h4 class="heading_a uk-margin-bottom">Openstaande Reminders</h4>--}}
{{--<div class="md-card uk-margin-medium-bottom">--}}
{{--        <div class="md-card-content">--}}
{{--            <div class="dt_colVis_buttons"></div>--}}
{{--            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">--}}
{{--            	<thead>--}}
{{--            		<tr>--}}
{{--                        <th>Reminder datum</th>--}}
{{--                		<th>Naam</th>--}}
{{--                		<th>Adres</th>--}}
{{--                		<th>Contactgegevens</th>--}}
{{--                		<th id="ttip_input_focus" data-uk-tooltip="{cls:'long-text', pos:'top'}" title="Dit getal geeft weer hoeveel dagen er nog nodig zijn voordat er een jaar verstreken is. Stel er staat 15m dan betekent dit dat er over 15 dagen precies een jaar verstreken is. Stel er staat -20, dat betekent dat 20 dagen geleden precies een jaar verstreken is.">Dagen tot reminder</th>--}}
{{--                        <th> </th>--}}
{{--            		</tr>--}}
{{--            	</thead>--}}
{{--			    <tbody>--}}
{{--                    @foreach($relaties as $relatie)--}}
{{--                        <tr>--}}
{{--                            <td><a href="/relatie/{{$relatie->id}}">{{$relatie->reminderdatum}}</a></td>--}}
{{--                            <td><a href="/relatie/{{$relatie->id}}">{{$relatie->voornaam}} {{$relatie->achternaam}}</a></td>--}}
{{--                            <td><a href="/relatie/{{$relatie->id}}">{{$relatie->straat}} {{$relatie->huisnummer}} {{$relatie->hn_prefix}}, <br> {{$relatie->postcode}} {{$relatie->woonplaats}}</a></td>--}}
{{--                            <td><a href="/relatie/{{$relatie->id}}">{{$relatie->telefoonnummer_prive}} || {{$relatie->telefoonnummer_zakelijk}} <br>{{$relatie->email}} </a></td>--}}
{{--                            <td><a href="/relatie/{{$relatie->id}}">{{ 365 - $relatie->verschil}}</a></td>--}}
{{--                            <td><a href="/reminder/{{$relatie->id}}/behandeld" class="md-btn md-btn-success md-btn-small md-btn-wave-light">Behandeld</a></td>--}}
{{--                        </tr>--}}
{{--                    @endforeach--}}
{{--			    </tbody>--}}
{{--			</table>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--<!-- HIERONDER EEN DATUM ZOEKBLALK? -->--}}

@endif

@endsection
