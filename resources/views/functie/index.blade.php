@extends('layouts.master')

@section('title', 'Flexavi - Functies')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)


<h4 class="heading_a uk-margin-bottom">Functies</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-striped">
                    <thead>
                    <tr>
                        <th>Titel</th>
                        <th>Dag- of Uurloon</th>
                        <th>Provisie % / €</th>
                        <th>Provisiegrens</th>
                        <th>Functies</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($functies as $activiteit)
                    <tr>
                    	<td>{{$activiteit->titel}}</td>
                    	<td> € @if($activiteit->uurloon == 0){{number_format($activiteit->dagloon,2)}} per dag @else {{number_format($activiteit->uurloon,2)}} per uur @endif</td>
                    	<td>@if($activiteit->provisiebedrag == 0) {{number_format($activiteit->provisieperc,2)}} % @else € {{number_format($activiteit->provisiebedrag,2)}} per klant @endif</td>
                    	<td>@if($activiteit->provisiegrens == 0) Geen grens @else {{number_format($activiteit->provisiegrens, 2)}} @endif</td>
                        <td>
                    		<a href="/functie/{{$activiteit->id}}/wijzigen" class="md-btn md-btn-warning md-btn-small md-btn-wave-light"><i class="material-icons">edit</i></a>
                    		<a href="/functie/{{$activiteit->id}}/verwijderen" class="md-btn md-btn-danger md-btn-small md-btn-wave-light"><i class="material-icons">delete</i></a>
                    	</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endif

@endsection