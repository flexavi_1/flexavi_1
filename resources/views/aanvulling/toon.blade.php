@extends('layouts.master')

@section('title', 'Aanvulling')

@push('scripts')

 <!--  chat functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/page_chat.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99 && (Auth::user()->id == $aanvulling->gebruiker_id || $verzondenaan->ontvanger == Auth::user()->id || Auth::user()->rol < 6))

<div class="uk-width-medium-8-10 uk-container-center">
    <div class="uk-grid uk-grid-collapse" data-uk-grid-margin>
        <div class="uk-width-medium-10-10">
            <div class="md-card md-card-single">
                <div class="md-card-toolbar">
                    <div class="md-card-toolbar-actions hidden-print">
                        <!-- <div class="md-card-dropdown"> -->
                            @if(Auth::user()->rol < 7 || Auth::user()->id == $verzender->id)
                                @if($verzondenaan->status_id == 18)
                                <a href="/aanvulling/{{$verzondenaan->id}}/{{$verzondenaan->ontvanger}}/goedgekeurd" class="md-btn md-btn-success">Goedkeuren</a>
                                <a href="/aanvulling/{{$verzondenaan->id}}/afkeuren" class="md-btn md-btn-danger">Afkeuren</a>
                                @endif
                            @endif
                        <!-- <i class="md-icon  material-icons">&#xE5CD;</i> -->
                    </div>
                    <h3 class="md-card-toolbar-heading-text large">
                        <span class="uk-text-muted">{{$aanvulling->id}}: "{{$aanvulling->titel}}" verzonden door</span> <a href="#">{{$verzender->name}}</a>. Status: {{$status->omschrijving}}
                    </h3>
                </div>
                <div class="md-card-content padding-reset">
                    <div class="chat_box_wrapper">
                        <div class="chat_box touchscroll chat_box_colors_a" id="chat">
                            @if($klant != "")
                            <div class="chat_message_wrapper">
                                <div class="chat_user_avatar">
                                    <h4>{{$verzender->name}}</h4>
                                </div>
                                <ul class="chat_message">
                                    <li><p>Het betreft klant: <a href="/relatie/{{$aanvulling->klant_id}}">{{$aanvulling->getKlant()}}</a><span class="chat_message_time">{{date('d-m-Y H:i', strtotime($aanvulling->created_at))}}</span></p></li>
                                </ul>
                            </div>
                            @endif
                            <div class="chat_message_wrapper">
                                <div class="chat_user_avatar">
                                    <h4>{{$verzender->name}}</h4>
                                </div>
                                <ul class="chat_message">
                                    <li><p>{!!$aanvulling->omschrijving!!}<span class="chat_message_time">{{date('d-m-Y H:i', strtotime($aanvulling->created_at))}}</span></p></li>
                                </ul>
                            </div>
                            @foreach($reacties as $reactie)
                                @if($reactie->gebruiker_id == $verzender->id)
                                <div class="chat_message_wrapper">
                                    <div class="chat_user_avatar">
                                        <h4>{{$verzender->name}}</h4>
                                    </div>
                                    <ul class="chat_message">
                                        <li><p>{!!$reactie->omschrijving!!}<span class="chat_message_time">{{date('d-m-Y H:i', strtotime($reactie->created_at))}}</span></p></li>
                                    </ul>
                                </div>
                                @elseif($reactie->gebruiker_id == $ontvanger->id)
                                <div class="chat_message_wrapper chat_message_right">
                                    <div class="chat_user_avatar">
                                        <h4>{{$ontvanger->name}}</h4>
                                    </div>
                                    <ul class="chat_message">
                                        <li><p>{!!$reactie->omschrijving!!}<span class="chat_message_time">{{date('d-m-Y H:i', strtotime($reactie->created_at))}}</span></p></li>
                                    </ul>
                                </div>
                                @else
                                <div class="chat_message_wrapper">
                                    <div class="chat_user_avatar">
                                        <h4>{{App\Models\User::getName($reactie->gebruiker_id)}}</h4>
                                    </div>
                                    <ul class="chat_message">
                                        <li><p>{!!$reactie->omschrijving!!}<span class="chat_message_time">{{date('d-m-Y H:i', strtotime($reactie->created_at))}}</span></p></li>
                                    </ul>
                                </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="chat_submit_box" id="chat_submit_box">
                            {!! Form::open(array('url'=>'/aanvulling/'.$verzondenaan->id.'/reactieplaatsen', 'method' => 'post')) !!}
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-large-8-10">
                                                <input type="text" class="md-input" name="omschrijving" id="submit_message" placeholder="Verstuur Reactie" required>
                                            </div>
                                            <div class="uk-width-large-2-10">
                                                <button type="submit" name="submit" value="return" class="md-btn md-btn-flat md-btn-flat-success"><i class="material-icons md-12">&#xE163;</i> & ga terug</button><br>
                                                <button type="submit" name="submit" value="stay" class="md-btn md-btn-flat md-btn-flat-success"><i class="material-icons md-12">&#xE163;</i> & en blijf</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <div id="sidebar_secondary">
        <div class="sidebar_secondary_wrapper uk-margin-remove"></div>
    </div>
@endif
@endsection
