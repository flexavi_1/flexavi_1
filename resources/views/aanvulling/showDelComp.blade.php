@extends('layouts.master')

@section('title', 'Aanvulling Controle')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)


<h4 class="heading_a uk-margin-bottom">Afgeronde Aanvullingen</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
       
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Verzender</th>
                		<th>Ontvanger</th>
                		<th>Titel</th>
                        <th>Afgerond op</th>
                        <th>Status</th>
            		</tr>
            	</thead>   
			    <tbody>
                    @foreach($aanvullingen as $aanvulling)
                        <tr>
                            <td><a href="/aanvulling/{{$aanvulling->aanvulling->id}}">{{$aanvulling->aanvulling->gebruiker_id->name}}</a></td>
                            <td><a href="/aanvulling/{{$aanvulling->aanvulling->id}}">{{$aanvulling->ontvanger->name}}</a></td>
                            <td><a href="/aanvulling/{{$aanvulling->aanvulling->id}}">{{$aanvulling->aanvulling->titel}} </a></td>
                            <td><a href="/aanvulling/{{$aanvulling->aanvulling->id}}">{{date('d-m-Y H:i',strtotime($aanvulling->updated_at))}}</a></td>
                            <td><a href="/aanvulling/{{$aanvulling->aanvulling->id}}">{{$aanvulling->getStatus()}}</a></td>
                        </tr>
                    @endforeach
			    </tbody>
			</table>
		</div>
	</div>

{!! /*$datatable->table()*/ "" !!}

@endif

@endsection

@push('scripts')

{!! /*$datatable->scripts()*/ "" !!}

@endpush