@extends('layouts.master')


@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush
@section('title', 'Aanvulling')

@section('content')

@if(Auth::user()->rol != 99)

@if(Auth::user()->rol < 6)

<h4 class="md-card-toolbar-heading-text">Selecteer hier uw werknemer</h3>

<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-{{count($werknemers)}}">
    @foreach($werknemers as $w)
        <a class="md-btn md-btn-success md-btn-wave-light" href="/aanvulling/werknemer/{{$w->id}}">{{$w->name}}</a>
    @endforeach
    </div>
</div>

@endif


<div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
    <div class="uk-width-large">
        <div class="md-card">
            <div class="user_heading">
                <div class="user_heading_menu" data-uk-dropdown="{pos:'left-top'}">
                    <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                    <div class="uk-dropdown uk-dropdown-small">
                        <ul class="uk-nav">
                            <li><a href="/aanvulling/toevoegen">Aanvulling toevoegen</a></li>
                            <!-- <li><a href="#">Verwijder Relatie</a></li>
                            <li><a href="#">Geschiedenis Toevoegen</a></li> -->
                        </ul>
                    </div>
                </div>
                <div class="user_heading_content">
                    <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate">Aanvulling van {{Auth::user()->name}}</span><span class="sub-heading"> -- </span></h2>
                    <ul class="user_stats">
                        <li>
                            <h4 class="heading_a">{{$amount_open_sent}} <span class="sub-heading">Aantal openstaande verstuurde</span></h4>
                        </li>
                        <li>
                            <h4 class="heading_a">{{count($ontvangenopen)}} <span class="sub-heading">Aantal openstaande ontvangen</span></h4>
                        </li>
                    </ul>
                </div>
                <a class="md-btn md-btn-success md-btn-wave-light" href="/aanvulling/toevoegen">Aanvulling versturen</a>
            </div>
            <div class="user_content">
                <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                    <li class="uk-active">
                        <a href="#">Openstaande ontvangen aanvullingen</a>
                    </li>
                    <li><a href="#">Openstaande verstuurde aanvullingen</a></li>
                    <li><a href="#">Gesloten ontvangen aanvullingen</a></li>
                    <li><a href="#">Gesloten verstuurde aanvullingen</a></li>
                </ul>
                <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                    <li>
                        <h4 class="heading_c uk-margin-bottom">Openstaande ontvangen aanvullingen</h4>
                    	<div class="uk-grid" data-uk-grid-margin>
                			<div class="uk-width-large-1-1">
                				<div class="dt_colVis_buttons"></div>
                                <table class="uk-table uk-table-hover" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 10%;">Datum geplaatst</th>
                                            <th style="width: 15%;">Titel</th>
                                            <th style="width: 10%;">Originele Plaatser</th>
                                            <th style="width: 15%;">Laatste reactie door:</th>
                                            <th style="width: 10%;">Status</th>
                                            <th style="width: 15%;">Gelezen</th>
                                            <th style="width: 15%;">Functie</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ontvangenopen as $ont)
                                            <tr>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{date('d-m-Y', strtotime($ont->aanvulling->created_at))}}</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{$ont->aanvulling->titel}}</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">@foreach($users as $us) @if($us->id == $ont->aanvulling->gebruiker_id){{$us->name}}@endif @endforeach</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{$ont->laatste_reactie->name}} heeft op <br> {{$ont->laatste_reactie_tijd}} gereageerd</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{$ont->status->omschrijving}}</a></td>
                                                <td>@if($ont->gelezen_op == NULL) 
                                                        <a href="/aanvulling/{{$ont->id}}/markeergelezen" class="md-btn md-btn-success md-btn-wave-light"> Markeer als gelezen  
                                                    @else 
                                                        <a href="/aanvulling/{{$ont->id}}"> Gelezen op {{date('d-m-Y H:i', strtotime($ont->gelezen_op))}}
                                                    @endif 
                                                        </a><br>
                                                    @if($ont->status_id != 18 && $ont->status_id != 20 )
                                                        <a href="/aanvulling/{{$ont->id}}/markeeropgelost" class="md-btn md-btn-success md-btn-wave-light"> Markeer als opgelost</a>
                                                    @endif
                                                </td>

                                                @if(Auth::user()->rol < 6 || Auth::user()->id == $ont->aanvulling->gebruiker_id)
                                                <td>
                                                    <a href="/aanvulling/totaal/verwijderen/{{$ont->aanvulling->id}}" class="md-btn md-btn-danger md-btn-wave-light">Totale aanvulling verwijderen</a><br>
                                                    <a href="/aanvulling/{{$ont->id}}/verwijderen" class="md-btn md-btn-warning md-btn-wave-light">Aanvulling bij deze gebruiker verwijderen</a>
                                                </td>
                                                @endif

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h4 class="heading_c uk-margin-bottom">Openstaande verstuurde aanvullingen</h4>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-1-1">
                                <div class="dt_colVis_buttons"></div>
                                <table class="uk-table uk-table-hover" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 15%;">Datum geplaatst</th>
                                            <th style="width: 15%;">Titel</th>
                                            <th style="width: 15%;">Ontvanger</th>
                                            <th style="width: 20%;">Laatste interactie</th>
                                            <th style="width: 15%;">Status</th>
                                            <th style="width: 20%;">Gelezen</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sentAanv as $ont)
                                            @if($ont->status->id != 18 && $ont->status->id != 20)
                                            <tr>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{date('d-m-Y', strtotime($ont->aanvulling->created_at))}}</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{$ont->aanvulling->titel}}</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">@foreach($users as $us) @if($us->id == $ont->ontvanger){{$us->name}}@endif @endforeach</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{$ont->laatste_reactie->name}} heeft op <br>{{$ont->laatste_reactie_tijd}} gereageerd</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{$ont->status->id.': '.$ont->status->omschrijving}}</a></td>
                                                <td>@if($ont->gelezen_op == NULL) 
                                                        Nog niet gelezen door gebruiker
                                                    @else 
                                                        <a href="/aanvulling/{{$ont->id}}"> Gelezen op {{date('d-m-Y H:i', strtotime($ont->gelezen_op))}}
                                                    @endif 
                                                        </a>
                                                    @if(Auth::user()->rol < 6 || $ont->status_id == 17)
                                                        <a href="/aanvulling/{{$ont->id}}/verwijderen" class="md-btn md-btn-success md-btn-wave-light"><i class="material-icons">delete</i> Verwijder Aanvulling</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h4 class="heading_c uk-margin-bottom">Opgeloste ontvangen aanvullingen</h4>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-1-1">
                                <div class="dt_colVis_buttons"></div>
                                <table class="uk-table uk-table-hover" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 15%;">Datum geplaatst</th>
                                            <th style="width: 15%;">Titel</th>
                                            <th style="width: 15%;">Plaatser</th>
                                            <th style="width: 20%;">Laatste interactie</th>
                                            <th style="width: 15%;">Status</th>
                                            <th style="width: 20%;">Gelezen</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ontvangengesloten as $ont)
                                            @if($ont->status->id < 20)
                                            <tr>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{date('d-m-Y', strtotime($ont->aanvulling->created_at))}}</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{$ont->aanvulling->titel}}</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">@foreach($users as $us) @if($us->id == $ont->aanvulling->gebruiker_id){{$us->name}}@endif @endforeach</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{$ont->laatste_reactie->name}} heeft op {{$ont->laatste_reactie_tijd}} gereageerd</a></td>
                                                <td><a href="/aanvulling/{{$ont->id}}">{{$ont->status->omschrijving}}</a></td>
                                                <td>@if($ont->gelezen_op == NULL) 
                                                        <a href="/aanvulling/{{$ont->id}}/markeergelezen" class="md-btn md-btn-success md-btn-wave-light"> Markeer als gelezen  
                                                    @else 
                                                        <a href="/aanvulling/{{$ont->id}}"> Gelezen op {{date('d-m-Y H:i', strtotime($ont->gelezen_op))}}
                                                    @endif 
                                                        </a>
                                                    @if($ont->status_id == 17)
                                                        <a href="/aanvulling/{{$ont->id}}/markeeropgelost" class="md-btn md-btn-success md-btn-wave-light"> Markeer als opgelost</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h4 class="heading_c uk-margin-bottom">Opgeloste verstuurde aanvullingen</h4>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-1-1">
                                <div class="dt_colVis_buttons"></div>
                                <table class="uk-table uk-table-hover" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 15%;">Datum geplaatst</th>
                                            <th style="width: 15%;">Titel</th>
                                            <th style="width: 15%;">Ontvanger</th>
                                            <th style="width: 20%;">Laatste interactie</th>
                                            <th style="width: 15%;">Status</th>
                                            <th style="width: 20%;">Gelezen</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sentAanv as $ont)
                                            @if($ont->status->id < 20)
                                                @if($ont->status->id == 18)
                                                <tr>
                                                    <td><a href="/aanvulling/{{$ont->id}}">{{date('d-m-Y', strtotime($ont->aanvulling->created_at))}}</a></td>
                                                    <td><a href="/aanvulling/{{$ont->id}}">{{$ont->aanvulling->titel}}</a></td>
                                                    <td><a href="/aanvulling/{{$ont->id}}">@foreach($users as $us) @if($us->id == $ont->ontvanger){{$us->name}}@endif @endforeach</a></td>
                                                    <td><a href="/aanvulling/{{$ont->id}}">{{$ont->laatste_reactie->name}} heeft op {{$ont->laatste_reactie_tijd}} gereageerd</a></td>
                                                    <td><a href="/aanvulling/{{$ont->id}}">{{$ont->status->id.': '.$ont->status->omschrijving}}</a></td>
                                                    <td>@if($ont->gelezen_op == NULL) 
                                                            Nog niet gelezen door gebruiker
                                                        @else 
                                                            <a href="/aanvulling/{{$ont->id}}"> Gelezen op {{date('d-m-Y H:i', strtotime($ont->gelezen_op))}}
                                                        @endif 
                                                            </a>
                                                        @if($ont->status_id == 17)
                                                            <a href="/aanvulling/{{$ont->id}}/verwijderen" class="md-btn md-btn-success md-btn-wave-light"><i class="material-icons">delete</i> Verwijder Aanvulling</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endif
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

@endif

@endsection