@extends('layouts.master')


@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush
@section('title', 'Aanvulling')

@section('content')

@if(Auth::user()->rol != 99)


<h4 class="md-card-toolbar-heading-text">Selecteer hier uw werknemer</h3>

<div class="uk-grid" data-uk-grid-margin>
    @foreach($werknemers as $w)
    <div class="uk-width-medium-1-{{count($werknemers)}}">
        <a class="md-btn md-btn-success md-btn-wave-light" href="/aanvulling/werknemer/{{$w->id}}">{{$w->name}}</a>
    </div>
    @endforeach
</div>



@endif

@endsection