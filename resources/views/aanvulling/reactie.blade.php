@extends('layouts.master')

@section('title', 'Afspraak maken')

@push('scripts')

<script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>
<script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>

<script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
<script>
    // var CKEDITOR = document.querySelector('#editor');
    CKEDITOR.replace('editor');
</script>


@endpush

@section('content')

@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Reactie op afkeuring</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
    	{!! Form::open(array('url'=>'/aanvulling/'.$verzondenaan->id.'/afkeuren', 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-1-1 uk-width-medium-1-1">
                    	<label>Omschrijving</label><br>
                		<textarea class="md-input label-fixed" id='editor' placeholder="Reden afkeuring" name="omschrijving"></textarea>
                	</div>
                </div>
                <div class="uk-form-row">
		            <div align="right">
		            	<div class="md-btn-group">
			            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
			                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder</button>
		            	</div>
		            </div>
	        	</div>
	        </div>
	    </div>
	    {!! Form::close() !!}
	</div>
</div>

@endif

@endsection
