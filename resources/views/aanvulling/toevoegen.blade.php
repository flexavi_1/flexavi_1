@extends('layouts.master')

@section('title', 'Flexavi - Aanvulling Toevoegen')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
    <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>
@endpush
@section('content')

    @if(Auth::user()->rol != 99)
        @if(isset($danger))
            <div class="md-card md-card-collapsed">
                <div class="md-card-toolbar md-bg-red-400 uk-text-contrast">
                    <h3 class="md-card-toolbar-heading-text ">
                        {{$danger}}
                    </h3>
                </div>
            </div>

            <br>
        @endif
        <h2 class="heading_b uk-margin-bottom">Aanvulling toevoegen</h2>

        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                <h3 class="heading_a">Aanvulling toevoegen</h3>
                @if(isset($tmp))
                    {!! Form::open(array('url'=>'/relatie/'.$id.'/zetAanvulling/'.$date.'/'.$werkdag_id, 'method' => 'post')) !!}
                @else
                    {!! Form::open(array('url'=>'/aanvulling/toevoegen', 'method' => 'post')) !!}
                @endif
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium">
                        <p></p>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-2">
                                    <span class="uk-form-help-block">Titel</span>
                                    <input type="text" required class="md-input label-fixed" step="1" name="titel"
                                           @if(isset($titel)) value="{{$titel}}"
                                           @endif placeholder="Geef hier de titel van de aanvulling" required/>
                                </div>
                                <div class="uk-width-large-1-2">
                                    <span class="uk-form-help-block">Verzenden aan</span>
                                    <select id="select_demo_5" name="ontvangers[]" data-md-selectize
                                            data-md-selectize-bottom data-uk-tooltip="{pos:'top'}"
                                            title="Definieer de ontvangers" multiple required>
                                        @foreach($gebruikers as $gebr)
                                            <option value="{{$gebr->id}}">{{$gebr->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @if(isset($tmp))
                            <input type="hidden" name="aanvulling_id" value="{{$tmp->id}}"/>
                        @endif
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large">
                                    <span class="uk-form-help-block">Omschrijving</span><br>
                                    <textarea cols="30" rows="4" class="md-input" name="omschrijving" id='editor'
                                              required>@if(isset($omschrijving))
                                            {{$omschrijving}}
                                        @endif</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-3">
                                    <span class="uk-form-help-block">Klant Postcode</span>
                                    <input type="text" class="md-input label-fixed" name="postcode"
                                           @if(isset($id)) value="{{App\Models\Client::findMe($id)->postcode}}"
                                           @elseif(isset($postcode)) value="{{$postcode}}"
                                           @endif  placeholder="Geef hier de postcode van de klant" maxlength="6"/>
                                </div>
                                <div class="uk-width-large-1-3">
                                    <span class="uk-form-help-block">Klant huisnummer</span>
                                    <input type="number" step="1" class="md-input label-fixed" name="huisnummer"
                                           @if(isset($id)) value="{{App\Models\Client::findMe($id)->huisnummer}}"
                                           @elseif(isset($huisnummer)) value="{{$huisnummer}}"
                                           @endif placeholder="Geef hier het huisnummer van de klant"/>
                                </div>
                                <div class="uk-width-large-1-3">
                                    <span class="uk-form-help-block">Klant huisnummer toevoeging</span>
                                    <input type="text" class="md-input label-fixed" name="toevoeging"
                                           @if(isset($id)) value="{{App\Models\Client::findMe($id)->hn_prefix}}"
                                           @elseif(isset($toevoeging)) value="{{$toevoeging}}"
                                           @endif placeholder="Geef hier de toevoeging van de klant"/>
                                </div>
                            </div>
                        </div>
                        <div class="uk-form-row">

                        </div>
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium">
                        <div class="uk-form-row">
                            <div align="right">
                                <div class="md-btn-group">
                                    <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg
                                        Aanvulling Toe
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}

    @endif

@endsection
