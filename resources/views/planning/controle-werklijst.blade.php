@extends('layouts.master')

@section('title', 'Controle Werklijsten')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)


<h4 class="heading_a uk-margin-bottom">Lopende/voltooide werklijsten</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
       
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Datum</th>
                		<th>Aantal afspraken</th>
                		<th>Aantal niet verwerkte afspraken</th>
                		<th>Dagomzet</th>
                        <th>Status</th>
                        <th>Dagstaat</th>
            		</tr>
            	</thead>
			    <tbody>
                    @if(count($werkdagen) > 0)
                    @foreach($werkdagen as $wd)
                        <tr>
                            <td><a href="/planning/verkoop/{{$wd->datum}}/{{$wd->id}}/controle">{{date('d-m-Y', strtotime($wd->datum))}}</a></td>
                            <td>{{$wd->aantal_klanten}}</td> 
                            <td>{{$wd->niet_verwerkt}}</td>
                            <td>€ {{number_format($wd->omzet,2)}}</td>
                            <td>@if($wd->behandeld == 0) Nog niet volledig afgerond.<br> Er staan nog {{$wd->niet_verwerkt}} @if($wd->niet_verwerkt != 1) afspraken @else afspraak @endif open. @else Werkdag volledig verwerkt, in afwachting van controle. @endif</td>
                            <td>@if($wd->ublCheck == 0) Dagstaat nog niet gecontroleerd. @else Dagstaat gecontroleerd. @endif</td>
                        </tr>
                    @endforeach
                    @endif
			    </tbody>
			</table>
		</div>
	</div>

{!! /*$datatable->table()*/ "" !!}

@endif

@endsection

@push('scripts')

{!! /*$datatable->scripts()*/ "" !!}

@endpush