@extends('layouts.master')

@section('title', 'Flexavi - Planning')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)


<h4 class="heading_a uk-margin-bottom">Telling</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-hover">
                    <thead>
                    <tr>
                        <th>Datum</th>
                        <th>Aantal Ploegen</th>
                        <th>Max aantal klanten</th>
                        <th>Klanten Nodig</th>
                        <th>Klanten ingepland</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($werkdagen as $wd)
                        <tr>
                            <td><a href="/planning/verkoop/{{$wd->datum}}">{{date('D d-m-Y', strtotime($wd->datum))}}</a></td>
                            <td>{{$wd->aantal_lijsten}}</td>
                            <td>{{$wd->aantal_klanten}}</td>
                            <td><span class="uk-text-bold md-color-red-800">@if($wd->aantal_klanten - $wd->aantal > 0){{$wd->aantal_klanten - $wd->aantal}} @else 0 @endif</span></td>
                            <td>{{$wd->aantal}}</td>
                        </tr>
                            @if(date('N', strtotime($wd->datum)) == 5)
                            <tr><td><br></td><td></td><td></td><td></td><td></td></tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <br>
            <div align="right"><a class="md-btn md-btn-success md-btn-wave-light" href="/planning/werving/koppel">Koppel klantenwervers aan data</a></div>
        </div>
    </div>


@endif

@endsection