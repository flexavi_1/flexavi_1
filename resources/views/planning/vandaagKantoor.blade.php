@extends('layouts.master')

@section('title', 'Planning Kantoor')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>


@endpush
@section('content')

@if(isset($na))
	<p>U heeft hier geen toegang. Indien u hier wel toegang hoort te hebben, neem dan contact op met uw leidinggevende.</p>
@else
    @if(isset($danger))
    
    <div class="uk-alert md-bg-red-400 " data-uk-alert>
        <a href="#" class="uk-alert-close uk-close"></a>
        <h3>
            {{$danger}}<br>Druk <a href="/">hier</a> om terug te keren naar het dashboard.
        </h3>
    </div>
    @else
	<h1>Takenlijst voor {{date('d-m-Y', strtotime($date))}}</h1>
	<h4 class="heading_a uk-margin-bottom">Klanten te verwerken</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Afspraakdatum</th>
                		<th>Klantnaam</th>
                		<th>Contactgegevens</th>
                		<th>Totaalbedrag</th>
                        <th>Functies</th>
            		</tr>
            	</thead>
			    <tbody>
                    @foreach($relaties as $relatie)
                        <tr>
                            <td>{{date('d-m-Y', strtotime($relatie->startdatum))}}</td>
                            <td>{{$relatie->klant->voornaam.' '.$relatie->klant->achternaam}}</td>
                            <td>{{$relatie->klant->postcode." ".$relatie->klant->huisnummer." ".$relatie->klant->hn_prefix}}</td>
                            <td>€ {{number_format($relatie->totaalbedrag,2)}}</td>
                            <td><a href="/afspraak/{{$relatie->id}}/verwerken" class="md-btn md-btn-success md-btn-small md-btn-wave-light">Verwerk afspraak</a></td>
                        </tr>
                    @endforeach
			    </tbody>
			</table>
		</div>
	</div>

	<h4 class="heading_a uk-margin-bottom">TB Klanten</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Klantnaam</th>
                		<th>Adres</th>
                		<th>Contactgegevens</th>
                		<th>Reden van Terugbellen</th>
                        <th>Functies</th>
            		</tr>
            	</thead>
			    <tbody>
                    @foreach($tbers as $relatie)
                        <tr>
                            <td>{{$relatie->voornaam.' '.$relatie->achternaam}}</td>
                            <td>{{$relatie->postcode." ".$relatie->huisnummer." ".$relatie->hn_prefix}}</td>
                            <td>{{$relatie->telefoonnummer_prive}} || {{$relatie->telefoonnummer_zakelijk}} <br>{{$relatie->email}} </td>
                            <td>Afspraak {{$relatie->afspraak_id}}: {{$relatie->reden}}</td>
                            <td><a href="/geschiedenis/{{$relatie->klant_id}}/toevoegen/{{$relatie->afspraak_id}}" class="md-btn md-btn-success md-btn-small md-btn-wave-light">Opmerking toevoegen</a>
                            	<a class="md-btn md-btn-warning md-btn-small md-btn-wave-light" href="/afspraak/maken/{{$relatie->klant_id}}">Afspraak maken</a>
                                <a class="md-btn md-btn-danger md-btn-small md-btn-wave-light" href="/afspraak/maken/{{$relatie->afspraak_id}}/nietnodig">Geen nieuwe afspraak</a></td>
                        </tr>
                    @endforeach
			    </tbody>
			</table>
		</div>
	</div>

	<h4 class="heading_a uk-margin-bottom">Reminder Klanten</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Dagen Tot reminder</th>
                		<th>Klantnaam</th>
                		<th>Contactgegevens</th>
                		<th>Totaalbedrag</th>
                        <th>Functies</th>
            		</tr>
            	</thead>
			    <tbody>
                    @foreach($reminders as $relatie)
                        <tr>
                            <td>{{ 365 - $relatie->verschil}}</td>
                            <td>{{$relatie->voornaam}} {{$relatie->achternaam}}</td>
                            <td>{{$relatie->postcode." ".$relatie->huisnummer." ".$relatie->hn_prefix}}</td>
                            <td>{{$relatie->telefoonnummer_prive}} || {{$relatie->telefoonnummer_zakelijk}} <br>{{$relatie->email}} </td>
                            <td><a href="/reminder/{{$relatie->id}}/behandeld" class="md-btn md-btn-success md-btn-small md-btn-wave-light">Behandeld</a>
                            	<a class="md-btn md-btn-warning md-btn-small md-btn-wave-light" href="/afspraak/maken/{{$relatie->id}}">Afspraak maken</a></td>
                        </tr>
                    @endforeach
			    </tbody>
			</table>
		</div>
	</div>
    @endif

@endif
@endsection