@extends('layouts.master')

@section('title', 'Flexavi - Relatie Toevoegen')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)

 <div class="md-card">
    <div class="md-card-content">
        <h3 class="heading_a">Instellingen Werklijst</h3> 
        <?php $url = "/planning/verkoop/".$date."/".$werkdag->id."/aanpassen"?>

        {!! Form::open(array('url'=>$url, 'method' => 'post')) !!}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label>Aantal ploegen</label>
                            <input type="number" class="md-input" step="1" name="aantal_lijsten" value="3" required/>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Max aantal klanten</label>
                            <input type="number" class="md-input" step="1" name="aantal_klanten" value="25" required/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
        <br>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div align="right">
                            <div class="md-btn-group">
                                <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Wijzig instellingen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

                    {!! Form::close() !!}
</div>



@endif
@endsection
