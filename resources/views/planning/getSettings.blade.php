@extends('layouts.master')

@section('title', 'Flexavi - Relatie Toevoegen')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)

 <div class="md-card">
    <div class="md-card-content">
        <h3 class="heading_a">Instellingen Werklijst</h3> 
        <?php $url = "/planning/verkoop/".$date."/".$werklijst->id."/instellingen"?>

        {!! Form::open(array('url'=>$url, 'method' => 'post')) !!}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <select id="select_demo_5" name="verkoper" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Verkoper">
                                <option value="" disabled>Select...</option>
                                @foreach($werknemers as $werknemer)
                                <option value="{{$werknemer->id}}" @if($werknemer->id == $werklijst->verkoper) selected @endif>{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <select id="select_demo_5" name="veger" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Veger">
                                <option value="" disabled>Select...</option>
                                <option value="null">Geen veger aanwezig</option>
                                @foreach($werknemers as $werknemer)
                                <option value="{{$werknemer->id}}" @if($werknemer->id == $werklijst->veger) selected @endif>{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label>Afstand</label>
                            <input type="number" class="md-input" value="{{$werklijst->afstand}}" step="0.1" name="afstand" required/>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Wisselgeld</label>
                            <input type="number" class="md-input" value="{{$werklijst->wisselgeld}}" step="0.01" name="wisselgeld" value="50" required/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label>Aantal facturen</label>
                            <input type="number" class="md-input" step="1" name="aantal_facturen" 
                                @if($werklijst->aantal_facturen != 12) 
                                    value="{{$werklijst->aantal_facturen}}"
                                @else 
                                    value="12" 
                                @endif
                                required/>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Aantal WO's</label>
                            <input type="number" class="md-input" step="1" name="aantal_wo" 
                                @if($werklijst->aantal_facturen != 12) 
                                    value="{{$werklijst->aantal_facturen}}"
                                @else 
                                    value="12" 
                                @endif 
                            required/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label>Opmerking</label>
                            <input type="text" class="md-input" name="opmerking" 
                                @if($werklijst->opmerking != "Bel afwijkende klanten van te voren of ze wel thuis zijn, dit scheelt kilometers en tijd !") 
                                    value="{{$werklijst->opmerking}}"
                                @else 
                                    value="Bel afwijkende klanten van te voren of ze wel thuis zijn, dit scheelt kilometers en tijd !" 
                                @endif/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <select id="select_demo_5" name="auto_id" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Auto">
                                <option value="" disabled>Select...</option>
                                @foreach($autos as $auto)
                                <option value="{{$auto->id}}" @if($werklijst->auto_id == $auto->id) selected @endif>{{$auto->kenteken}}, {{$auto->merk}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div align="right">
                            <div class="md-btn-group">
                                <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Wijzig instellingen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

                    {!! Form::close() !!}
</div>



@endif
@endsection
