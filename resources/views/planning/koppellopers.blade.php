@extends('layouts.master')

@section('title', 'Flexavi - Relatie Toevoegen')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush

@section('content')

<h2 class="heading_b uk-margin-bottom">Koppel klantenwervers aan data</h2>
<div class="uk-grid data-uk-grid-margin">
<div class="uk-width-large-1-1">
    {!! Form::open(array('url'=>'planning/werving/koppel', 'method' => 'post')) !!}
    <div class="uk-width-large-1-1 uk-width-medium-1-1">
    <div class="uk-grid" data-uk-grid-margin>
    @foreach($werknemers as $wn)
        <div class="uk-width-large-1-3">
        <div class="md-card">
            <div class="md-card-toolbar">
                <h3 class="md-card-toolbar-heading-text">
                    Werknemer: {{$wn->voornaam}} {{$wn->achternaam}}
                </h3>
            </div>
            <div class="md-card-content">
                @foreach($werkdagen as $wd)
                <p>
                    <input type="checkbox" name="datum[]" id="checkbox_demo_{{$wd->datum}}{{$wn->werknemer_id}}" value="{{$wn->werknemer_id}}:{{$wd->datum}}" data-md-icheck />
                    <label for="checkbox_demo_{{$wd->datum}}{{$wn->werknemer_id}}" class="inline-label">{{date('d-m-Y',strtotime($wd->datum))}} MAX {{$wd->aantal_klanten - $wd->aantal}}</label>
                @endforeach
                <hr>
                <!-- <label for="loopplaats[]" class="inline-label">Plaats</label> -->
                <input type="text" maxlength="100" minlength="2" class="md-input label-fixed" name="loopplaats[]" placeholder="Vul plaats in."/> 
                <input type="text" maxlength="100" minlength="2" class="md-input label-fixed" name="startstraat[]" placeholder="Vul startstraat in."/> 
                <input type="hidden" name="werknemer[]" value="{{$wn->werknemer_id}}" />
                <br>
                <label class="uk-form-label" for="wanneer[]">Loopdatum:</label>
                <input class="md-input" type="text" id="uk_dp_1" name="wanneer[]" data-uk-datepicker="{format:'DD-MM-YYYY'}">
            </div>
        </div>
    </div>
    @endforeach
</div>
    </div>
    <br>
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium">
            <div class="uk-form-row">
                <div align="right">
                    <div class="md-btn-group">
                        <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Koppel werknemers aan data</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
</div>

@endsection