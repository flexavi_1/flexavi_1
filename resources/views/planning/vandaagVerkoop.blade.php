@extends('layouts.master')

@section('title', 'Planning Verkoop Vandaag')
@push('scripts')
<script src="{{ URL::asset('assets/bower_components/dragula.js/dist/dragula.min.js')}}"></script>
    <!--  nestable component functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/components_nestable.min.js')}}"></script>
    <!-- Frenzo JS -->
    <script src="{{ URL::asset('assets/assets/js/main.js')}}"></script>
@endpush
@section('content')

@if(Auth::user()->rol < 6)  <!-- User is authorised to edit/delete worklists -->

<h2>Werklijst van {{ $datum }}</h2>

<form method="post" action="planning/verkoop/{{ $datum }}/update"
<!-- Button -->
<div align="left">
    <div class="md-btn-group">
       
    </div>
</div>
<!-- Werklijsten -->
<div class="scrum_board_overflow">
    <div id="scrum_board" class="uk-clearfix">
        <div>

            <div class="scrum_column_heading_wrapper">
                <div class="scrum_column_heading"> Nader in te delen/ Verzetten </div>
            </div>

            <div class="scrum_column" id="verzetten">
                <div id="scrum_column_todo" class="dragula dragula-vertical" drop-id="verzetten">

                        @foreach($verzetten as $whav)
                            <div drag-id="{{ $whav->afspraakID }}">
                                <input type="hidden" name="verzetten" value="1">
                                @if($whav->afspraaktype_id->id != 3 && $whav->afspraaktype_id->id != 4)
                                <div class="scrum_task minor">
                                @elseif($whav->afspraaktype_id->id == 3)
                                <div class="scrum_task blocker uk-text-primary" >
                                @elseif($whav->afspraaktype_id->id == 4)
                                <div class="scrum_task critical uk-text-success">
                                @endif

                                    <h3 class="scrum_task_title">
                                        <a href="/relatie/{{$whav->klant_id->id}}">
                                            {{date('d-m-Y H:i', strtotime($whav->startdatum))}} - {{date('H:i', strtotime($whav->einddatum))}}
                                            <br>
                                        </a>
                                    </h3>
                                    <p class="scrum_task_description uk-text-blue">
                                        <b><i>{{$whav->afspraaktype_id->omschrijving}}</i></b> <br>
                                        Fam. {{$whav->klant_id->achternaam}} <br>
                                        {{$whav->klant_id->straat}} {{$whav->klant_id->huisnummer}} {{$whav->klant_id->hn_prefix}}, <br> {{$whav->klant_id->postcode}}  {{$whav->klant_id->woonplaats}} <br> 
                                        <hr>
                                        @if(!$ahas->isEmpty())
                                        @foreach($ahas as $aha)
                                            @if($aha->afspraak_id == $whav->afspraakID)
                                                {{$aha->aantal}} 
                                                {{$aha->activiteit->omschrijving}} à € 
                                                {{number_format($aha->prijs,2)}} <br>
                                            @endif
                                        @endforeach
                                        @endif

                                        <hr>
                                                <b>Totaal bedrag € {{number_format($whav->totaalbedrag,2)}}</b>
                                        <br>{{$whav->klant_id->telefoonnummer_prive}} | {{$whav->klant_id->telefoonnummer_zakelijk}}
                                        <br> <span class="uk-text-bold uk-text-danger">{{$whav->omschrijving}}</span>
                                        <br> 
                                        @foreach($redenen as $reden)
                                            @if($whav->id == $reden->nieuwe_afspraak && $reden->datum_oude_afspraak != null)
                                                <em>{{date('d-m-Y', strtotime($reden->datum_oude_afspraak))}}: {{$reden->reden}}</em>
                                            @endif
                                        @endforeach
                                    </p>

                                    <p class="scrum_task_info"><span class="uk-text-muted">Verwerkt door:</span> <a href="#">{{$whav->verwerkt_door->name}}</a></p>
                                </div>
                            </div>
                        @endforeach
                    <br />
                </div>
            </div>
        </div>
        @foreach($werklijsten as $werklijst)
        <div>
            <input type="hidden" name="werklijst_id[]" value="{{$werklijst->id}}">
            <div class="scrum_column_heading_wrapper">
                <div class="scrum_column_heading"> @if($werklijst->verkoper == null && $werklijst->veger == null) 
                                                        Voeg werknemers toe (max. 2) 
                                                    @else 
                                                        {{$werklijst->verkoper->voornaam}}  
                                                        @if($werklijst->veger != null)
                                                            + {{$werklijst->veger->voornaam}} 
                                                        @endif
                                                    @endif</div>
                <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                    <i class="md-icon material-icons">&#xE5D4;</i>
                    <div class="uk-dropdown uk-dropdown-small">
                        <ul class="uk-nav uk-nav-dropdown">
                            <li><a href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/instellingen">Instellingen werklijst</a></li>
                            <li><a href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/print">Print werklijst</a></li>
                            <li class="uk-nav-divider"></li>
                            <li><a href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/verwijderen">Verwijder Werklijst</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="scrum_column">
                <div id="scrum_column_todo" class="dragula dragula-vertical" drop-id="{{ $werklijst->id }}">
                    @foreach($whas as $wha)

                        @if($wha->werklijst->id == $werklijst->id)
                        <div drag-id="{{ $wha->afspraak_id->id }}">
                            @if($wha->afspraak_id->afspraaktype_id->id != 3 && $wha->afspraak_id->afspraaktype_id->id != 4)
                            <div class="scrum_task minor">
                            @elseif($wha->afspraak_id->afspraaktype_id->id == 3)
                            <div class="scrum_task blocker uk-text-primary" >
                            @elseif($wha->afspraak_id->afspraaktype_id->id == 4)
                            <div class="scrum_task critical uk-text-success">
                            @endif 
                                <input type="hidden" name="volgorde[]" value="{{$wha->afspraak_id->id}}">
                                <h3 class="scrum_task_title">
                                    <a href="/relatie/{{$wha->afspraak_id->klant_id->id}}">
                                        {{date('d-m-Y', strtotime($wha->afspraak_id->startdatum))}} - {{date('d-m-Y', strtotime($wha->afspraak_id->einddatum))}}
                                        <br>
                                    </a>
                                </h3>
                                <p class="scrum_task_description">
                                    <b><i>{{$wha->afspraak_id->afspraaktype_id->omschrijving}}</i></b> <br>
                                    Fam. {{$wha->afspraak_id->klant_id->achternaam}} <br>
                                        {{$wha->afspraak_id->klant_id->straat}} {{$wha->afspraak_id->klant_id->huisnummer}} {{$wha->afspraak_id->klant_id->hn_prefix}}, <br> {{$wha->afspraak_id->klant_id->postcode}}  {{$wha->afspraak_id->klant_id->woonplaats}} <br> 
                                    <hr>
                                    @if(!$ahas->isEmpty())
                                    @foreach($ahas as $aha)
                                        @if($aha->afspraak_id == $wha->afspraak_id->id)
                                            {{$aha->aantal}} 
                                            {{$aha->activiteit->omschrijving}} à € 
                                            {{number_format($aha->prijs,2)}} <br>
                                        @endif
                                    @endforeach
                                    @endif
                                    <hr>
                                            <span>Totaal bedrag € {{number_format($wha->afspraak_id->totaalbedrag,2)}}</span>
                                    
                                    
                                    <br>{{$wha->afspraak_id->klant_id->telefoonnummer_prive}} | {{$wha->afspraak_id->klant_id->telefoonnummer_zakelijk}}
                                    <br> <span class="uk-text-bold uk-text-danger">{{$wha->afspraak_id->omschrijving}}</span>
                                    <br> 
                                    @foreach($redenen as $reden)
                                        @if($wha->afspraak_id->id == $reden->nieuwe_afspraak)
                                            <em>{{date('d-m-Y', strtotime($reden->datum_oude_afspraak))}}: {{$reden->reden}}</em>
                                        @endif
                                    @endforeach
                                </p>

                                <p class="scrum_task_info"><span class="uk-text-muted">Verwerkt door:</span> <a href="#">{{$wha->afspraak_id->verwerkt_door->name}}</a></p>
                            </div>
                        </div>
                        @endif
                    @endforeach
                    <br>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
</form>

@elseif(Auth::user()->rol == 6) <!-- User is authorised to only view worklists -->
    Laat hier per lijst in tabel vorm de klant zien. Geef een knop mee in de tabel: Afspraak verwerken. 
    Als alle klanten zijn verwerkt in de lijst (opzoeken hoe te achterhalen in de database) dan knop tonen met dag afsluiten.
    Uiteindelijk kan via dag afsluiten deels automatisch de ub lijst voor die ploeg aangemaakt worden.
@elseif(Auth::user()->rol == 8) <!-- User is authorised to only view his/her own worklist -->
    Laat zijn werklijst zien met knoppen enzo..
    Als het nog niet die dag is, laten zien dat er niets beschikbaar is.
@endif
@endsection