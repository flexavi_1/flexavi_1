@extends('layouts.master')

@section('title', 'Flexavi - Werving')


@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>
    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)
{{-- {{dd($aantalperwerknemer)}} --}}
<h4 class="heading_a uk-margin-bottom">Planning Werving {{date('d-m-Y', strtotime($date))}}</h4>
    @if($vandaag == "leeg")
        <p>Er zijn nog geen klantenwervers ingedeeld op deze datum. <a href="/planning/werving/koppel">Klik hier</a> om klantenwervers in te delen.</p>
    @else
        @if(date('d-m-Y') >= date('d-m-Y', strtotime($date)))
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-overflow-container">
                    <table class="uk-table uk-table-striped">
                        <thead>
                        <tr>
                            <th>Werknemer</th>
                            <th>Loopplaats</th>
                            <th>Startstraat</th>
                            <th>Loopdata</th>
                            <th>Aangemaakt door</th>
                            <th>Aangemaakt op</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($vandaag as $vd)
                        <tr>
                        	<td>{{$vd->werknemer_id->voornaam}} {{$vd->werknemer_id->achternaam}}</td>
                        	<td>{{$vd->plaats}}</td>
                            <td>{{$vd->startstraat}}</td>
                        	<td><ul>
                                @for($i = 0; $i < count($vd->loopdata); $i++)
                                    <li>{{$vd->loopdata[$i]}}</li>
                                @endfor
                            </ul></td>
                        	<td>{{$vd->user_id->name}}</td>
                            <td>{{date('d-m-Y', strtotime($vd->created_at))}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
        @if(date('d-m-Y') <= date('d-m-Y', strtotime($date)))

<h4 class="heading_a uk-margin-bottom">Resultaat klantenwervers</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-overflow-container">
                    <table class="uk-table uk-table-striped">
                        <thead>
                        <tr>
                            <th>Werknemer</th>
                            <th>Loopplaats</th>
                            <th>Startstraat</th>
                            <th>Loopdata</th>
                            <th># Geschreven</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($vandaag as $vd)
                        <tr>
                            <td>{{$vd->werknemer_id->voornaam}} {{$vd->werknemer_id->achternaam}}</td>
                            <td>{{$vd->plaats}}</td>
                            <td>{{$vd->startstraat}}</td>
                            <td><ul>
                                @for($i = 0; $i < count($vd->loopdata); $i++)
                                    <li>{{$vd->loopdata[$i]}}</li>
                                @endfor
                            </ul></td>
                            <td>@foreach($aantalperwerknemer as $apw) @if($apw->werknemer->id == $vd->werknemer_id->id) {{$apw->aantal}} @endif @endforeach</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
    @endif
        {!! Form::open(array('url'=>'/planning/werving/dag', 'method' => 'post')) !!}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-1-2">
                        <label for="kUI_datepicker_a" class="uk-form-label">Datum:</label>
                        <input id="kUI_datepicker_a" name="datum" required/>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div class="md-btn-group">
                        <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Ga naar datum</button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
@endif



@endsection