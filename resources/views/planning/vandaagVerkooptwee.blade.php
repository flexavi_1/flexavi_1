@extends('layouts.master')


@section('title', 'Planning Verkoop Vandaag')

@push('scripts')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



    <script>

        (function ($) {

            $.fn.serial = function () {

                var array = [];

                var $elem = $(this);

                $elem.each(function (i) {

                    var list_id = $(this).data('list-identifier');

                    $('tr[data-afspraak-id]', this).each(function (e) {

                        array.push(list_id + '[' + e + ']=' + $(this).data('afspraak-id'));

                    });

                });

                return array.join('&');

            }

        })(jQuery);


        $('.sortable > tbody').sortable({

            items: 'tr:has(td)',

            connectWith: 'tbody',

            update: function (event, ui) {

                if (this === ui.item.parent()[0]) {

                    $.post('{{ route('planning.today.update', $datum) }}', $('.sortable > tbody').serial(), function () {

                        location.reload();

                    });

                }

            },

            helper: function (e, tr) {

                var $originals = tr.children();

                var $helper = tr.clone();

                $helper.children().each(function (index) {

                    // Set helper cell sizes to match the original sizes

                    $(this).width($originals.eq(index).width());

                });

                return $helper;

            }

        });

    </script>

@endpush





@section('content')

    @if(Auth::user()->rol < 6 &&  strtotime(date('d-m-Y', strtotime($datum))) >= strtotime(date('d-m-Y')))
        <!-- Gebruiker mag wijzigingen doorvoeren -->



			  <?php

			  setlocale(LC_ALL, 'nl_NL');

			  ?>



        <h2>Werklijst van {{strftime('%A',strtotime($datum))}} {{ $datum }}</h2>

			  <?php $bool = false; ?>

        <div class="md-card uk-margin-medium-bottom">

            <div class="md-card-content">

                <div class="uk-overflow-container">

                    <table class="uk-table sortable">

                        <caption>Verzetten / Nader in te delen</caption>

                        <thead>

                        <tr>

                            <th style="width: 10%;">Datum en Tijd</th>

                            <th style="width: 15%;">Relatie</th>

                            <th style="width: 15%;">Adres</th>

                            <th style="width: 10%;">Contactgegevens</th>

                            <th style="width: 40%;">Werkzaamheden</th>

                            <th style="width: 10%" ;>Totaalbedrag</th>

                        </tr>

                        </thead>

                        <tbody data-list-identifier="verzetten">

									<?php $i = 0; ?>

                        {{-- <tr><td class="disable">Plaats hier de klanten die nog ingedeeld moeten worden.</td></tr> --}}



                        @foreach($verzetten as $whav)

                            @if($whav->status_id->id == 1 && $whav->afspraak->afspraaktype_id->id < 6)

                                <tr data-afspraak-id="{{ $whav->afspraakID }}" id="{{$whav->id}}">

												  <?php $i++; ?>

                                    <td>

                                        {{date('H:i', strtotime($whav->afspraak->startdatum))}}
                                        - {{date('H:i', strtotime($whav->afspraak->einddatum))}}


                                    </td>

                                    <td

                                            @for($i = 0; $i < count($kleurencombi); $i++)

                                                @if($kleurencombi[$i][1] == $whav->klant_id->woonplaats)

                                                    class="{{$kleurencombi[$i][0]}}"

														<?php break; ?>

                                    @elseif($i == count($kleurencombi)-1)



                                            @endif

                                            {{-- if($i == count($kleurencombi))<td>endif --}}

                                            @endfor

                                    >

                                        <a href="/relatie/{{$whav->klant_id->id}}">Fam. {{$whav->klant_id->achternaam}}</a>

                                    </td>


                                    <td

                                            @for($i = 0; $i < count($kleurencombi); $i++)

                                                @if($kleurencombi[$i][1] == $whav->klant_id->straat)

                                                    class="{{$kleurencombi[$i][0]}}"

														<?php break; ?>

                                    @elseif($i == count($kleurencombi)-1) @endif



                                            @endfor

                                    >

                                        {{$whav->klant_id->straat}} {{$whav->klant_id->huisnummer}} {{$whav->klant_id->hn_prefix}}
                                        , <br> {{$whav->klant_id->postcode}}  {{$whav->klant_id->woonplaats}}

                                    </td>

                                    <td

                                            @for($i = 0; $i < count($kleurencombi); $i++)

                                                @if($kleurencombi[$i][1] == $whav->klant_id->postcode)

                                                    class="{{$kleurencombi[$i][0]}}"

														<?php break; ?>

                                    @elseif($i == count($kleurencombi)-1)



                                            @endif

                                            @endfor

                                    >

                                        {{$whav->klant_id->telefoonnummer_prive}}
                                        <br> {{$whav->klant_id->telefoonnummer_zakelijk}}

                                    </td>


                                    <td>

                                        <!-- Kleur bepaling -->

                                        <i

                                                @if($whav->afspraak->afspraaktype_id->id == 3)

                                                    class="md-color-light-blue-A700"

                                                @elseif($whav->afspraak->afspraaktype_id->id == 4)

                                                    class="md-color-green-A700"

                                        @else



                                                @endif

                                        >

                                            @if($whav->afspraak->afspraaktype_id->id == 3)

                                                Geld ophalen, zie factuur voor meer info

                                            @elseif($whav->afspraak->afspraaktype_id->id == 5)

                                                Betaalherinnering

                                            @endif



                                            <!-- Werkzaamheden ophalen -->

															 <?php $ahas = App\Models\Appointment_has_activities::aha($whav->afspraakID, $whav->klant_id->id); ?>

                                            @if(!$ahas->isEmpty())

                                                @foreach($ahas as $aha)

                                                    @if($aha->afspraak_id == $whav->afspraakID)

                                                        {{$aha->aantal}}{{$aha->activiteit->eenheid}}

                                                        {{$aha->activiteit->omschrijving}} <i>{{$aha->opmerking}}</i> à
                                                        €

                                                        {{number_format($aha->prijs,2)}} <br>

                                                    @endif

                                                @endforeach

                                            @endif


                                        </i>

                                        <!-- Afspraak toevoeging ophalen -->

                                        <br> <span
                                                class="uk-text-bold uk-text-danger">{!!$whav->afspraak->omschrijving!!}</span>

                                        <br>

                                        <!-- Redenen waarom de vorige afspraak eventueel verzet was. -->

														<?php /*@foreach($redenen as $reden)

                                        @if($whav->id == $reden->nieuwe_afspraak && $reden->datum_oude_afspraak != null)

                                            <em>{{date('d-m-Y', strtotime($reden->datum_oude_afspraak))}}: {{$reden->reden}}</em>

                                        @endif

                                    @endforeach */ ?>

                                        <em style="font-size:12px;">{!! strip_tags(App\Models\Appointment::getStaticReden($whav->afspraakID), '<br>')!!}</em>


                                        @if($whav->geschiedenis != null)

                                            @foreach($whav->geschiedenis as $history)

                                                <br>

                                                <i class="md-bg-grey-500">{{$history->geschiedenis->datum}} {{$history->geschiedenis->omschrijving}}</i>

                                            @endforeach

                                        @endif

                                    </td>

                                    <td><b>€ {{number_format($whav->afspraak->totaalbedrag,2)}}</b></td>

                                </tr>

                            @endif

                        @endforeach

                        @if($i == 0)

                            <tr class="tmp_tr">
                                <td>Geen afspraken ingedeeld in deze lijst.</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                        @endif

                        </tbody>

                    </table>

                </div>

            </div>

        </div>



        @foreach($werklijsten as $werklijst)

				  <?php $i = 1; $i++; ?>

				  <?php $tmpSet = false; ?>

            <div class="md-card uk-margin-medium-bottom">

                <div class="md-card-content">

                    <div class="uk-overflow-container">

                        <table class="uk-table sortable">

                            <caption>

                                <h3>@if($werklijst->verkoper == null && $werklijst->veger == null)

                                        Voeg werknemers toe (max. 2)

                                    @else

                                        {{$werklijst->verkoper->voornaam}}

                                        @if($werklijst->veger != null)

                                            + {{$werklijst->veger->voornaam}}

                                        @endif

                                    @endif</h3>

                            </caption>

                            <a align="left" href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/instellingen">Instellingen
                                werklijst</a> ||

                            <a align="left" href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/print">Print
                                werklijst</a> ||

                            <a align="left" href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/verwijderen">Verwijder
                                Werklijst</a>

                    </div>

                    <thead>

                    <tr>

                        <th style="width: 10%;">Datum en Tijd</th>

                        <th style="width: 15%;">Relatie</th>

                        <th style="width: 15%;">Adres</th>

                        <th style="width: 10%;">Contactgegevens</th>

                        <th style="width: 40%;">Werkzaamheden</th>

                        <th style="width: 10%" ;>Totaalbedrag</th>

                    </tr>

                    </thead>

                    <tbody data-list-identifier="{{$werklijst->id}}">


                    @if($werklijst->returnAppAmount() > 0)

                        @foreach($whas as $wha)

                            @if($wha->status_id->id == 1)

                                @if($wha->werklijst->id == $werklijst->id /* && $check */ )

                                    <tr data-afspraak-id="{{ $wha->afspraak }}"
                                        id="{{$wha->werklijst->id}}" {{-- afspraak="{{$wha->afspraak->id}}" --}}>

                                        <td>

                                            {{date('H:i', strtotime($wha->afspraak_id->startdatum))}}
                                            - {{date('H:i', strtotime($wha->afspraak_id->einddatum))}}

                                        </td>

                                        <td

                                                @for($i = 0; $i < count($kleurencombi); $i++)

                                                    @if($kleurencombi[$i][1] == $wha->afspraak_id->klant_id->woonplaats)

                                                        class="{{$kleurencombi[$i][0]}}"

															 <?php break; ?>

                                        @elseif($i == count($kleurencombi)-1)



                                                @endif

                                                @endfor

                                        >

                                            <a href="/relatie/{{$wha->afspraak_id->klant_id->id}}">Fam. {{$wha->afspraak_id->klant_id->achternaam}}</a>

                                        </td>

                                        <td

                                                @for($i = 0; $i < count($kleurencombi); $i++)

                                                    @if($kleurencombi[$i][1] == $wha->afspraak_id->klant_id->straat)

                                                        class="{{$kleurencombi[$i][0]}}"

															 <?php break; ?>

                                        @elseif($i == count($kleurencombi)-1)



                                                @endif

                                                @endfor

                                        >

                                            {{$wha->afspraak_id->klant_id->straat}} {{$wha->afspraak_id->klant_id->huisnummer}} {{$wha->afspraak_id->klant_id->hn_prefix}}
                                            ,
                                            <br> {{$wha->afspraak_id->klant_id->postcode}}  {{$wha->afspraak_id->klant_id->woonplaats}}

                                        </td>

                                        <td

                                                @for($i = 0; $i < count($kleurencombi); $i++)

                                                    @if($kleurencombi[$i][1] == $wha->afspraak_id->klant_id->postcode)

                                                        class="{{$kleurencombi[$i][0]}}"

															 <?php break; ?>

                                        @elseif($i == count($kleurencombi)-1)

                                                @endif

                                                @endfor

                                        >

                                            {{$wha->afspraak_id->klant_id->telefoonnummer_prive}}
                                            <br> {{$wha->afspraak_id->klant_id->telefoonnummer_zakelijk}}

                                        </td>


                                        <td>

                                            <!-- Kleur bepaling -->

                                            <i

                                                    @if($wha->afspraak_id->afspraaktype_id->id == 3 || $wha->afspraak_id->afspraaktype_id->id == 5)

                                                        class="md-color-light-blue-A700"

                                                    @elseif($wha->afspraak_id->afspraaktype_id->id == 4)

                                                        class="md-color-green-A700"

                                            @else



                                                    @endif

                                            >

                                                @if($wha->afspraak_id->afspraaktype_id->id == 3)

                                                    Geld ophalen, zie factuur voor meer info

                                                @elseif($wha->afspraak_id->afspraaktype_id->id == 5)

                                                    Betaalherinnering

                                                @endif



                                                <!-- Werkzaamheden ophalen -->



																  <?php $ahas = App\Models\Appointment_has_activities::aha($wha->afspraak_id->id, $wha->afspraak_id->klant_id->id); ?>
                                                @if(!$ahas->isEmpty())

                                                    @foreach($ahas as $aha)

                                                        @if($aha->afspraak_id == $wha->afspraak_id->id)

                                                            {{$aha->aantal}}{{$aha->activiteit->eenheid}}

                                                            {{$aha->activiteit->omschrijving}}
                                                            <i>{{$aha->opmerking}}</i> à €

                                                            {{number_format($aha->prijs,2)}} <br>

                                                        @endif

                                                    @endforeach

                                                @endif </i>

                                            <!-- Afspraak toevoeging ophalen -->

                                            <br> <span
                                                    class="uk-text-bold uk-text-danger">{!!$wha->afspraak_id->omschrijving!!}</span>

                                            <br>

                                            <!-- Redenen waarom de vorige afspraak eventueel verzet was. -->

															 <?php /*@foreach($redenen as $reden)

                                                @if($whav->id == $reden->nieuwe_afspraak && $reden->datum_oude_afspraak != null)

                                                    <em>{{date('d-m-Y', strtotime($reden->datum_oude_afspraak))}}: {{$reden->reden}}</em>

                                                @endif

                                            @endforeach */ ?>

                                            <em style="font-size:12px;">{!! strip_tags(App\Models\Appointment::getStaticReden($wha->afspraak_id->id), '<br>')!!}</em>


                                            @if($wha->geschiedenis != null)

                                                @foreach($wha->geschiedenis as $history)

                                                    <br>

                                                    <i class="md-bg-grey-500">{{$history->geschiedenis->datum}} {{$history->geschiedenis->omschrijving}}</i>

                                                @endforeach

                                            @endif

                                        </td>

                                        <td><b>€ {{number_format($wha->afspraak_id->totaalbedrag,2)}}</b></td>

                                    </tr>

                                @endif

                            @endif

                        @endforeach

                    @else

                        @if(!$tmpSet)

                            <tr class="tmp_tr">
                                <td>Geen afspraken ingedeeld in deze lijst.</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

										 <?php $tmpSet = true; ?>

                        @endif

                    @endif

                    </tbody>

                    </table>

                </div>

            </div>

            </div>

        @endforeach

    @elseif(Auth::user()->rol == 6 || strtotime(date('d-m-Y', strtotime($datum))) < strtotime(date('d-m-Y')) )
        <!-- Gebruiker mag alleen inzien of datum is verstreken-->



        <h2>Werklijst van {{ $datum }}</h2>

        <div class="md-card uk-margin-medium-bottom">

            <div class="md-card-content">

                <div class="uk-overflow-container">

                    <table class="uk-table" id="verzetten">

                        <caption>Verzetten / Nader in te delen</caption>

                        <thead>

                        <tr>

                            <th>Datum en Tijd</th>

                            <th>Relatie</th>

                            <th>Adres</th>

                            <th>Contactgegevens</th>

                            <th>Werkzaamheden</th>

                            <th>Totaalbedrag</th>

                            <th>Status</th>

                            <th></th>

                        </tr>

                        </thead>

                        <tbody>

                        @foreach($verzetten as $whav)

                            <tr id="{{$whav->id}}">

                                <td>

                                    <!-- if($whav === end($verzetten))

                                    if(date('H:i', strtotime($whav->startdatum)) > date('H:i', strtotime(next($verzetten->startdatum))))

                                        <em>

                                    endif

                                    endif -->


                                    {{date('H:i', strtotime($whav->afspraak->startdatum))}}
                                    - {{date('H:i', strtotime($whav->afspraak->einddatum))}}



                                    <!-- if($whav === end($verzetten))

                                    if(date('H:i', strtotime($whav->startdatum)) > date('H:i', strtotime(next($verzetten->startdatum))))

                                        </em>

                                    endif

                                    endif --></td>

                                <td><a href="/relatie/{{$whav->klant_id->id}}">Fam. {{$whav->klant_id->achternaam}}</a>
                                </td>

                                <td>{{$whav->klant_id->straat}} {{$whav->klant_id->huisnummer}} {{$whav->klant_id->hn_prefix}}
                                    , <br> {{$whav->klant_id->postcode}}  {{$whav->klant_id->woonplaats}}</td>

                                <td>{{$whav->klant_id->telefoonnummer_prive}}
                                    | {{$whav->klant_id->telefoonnummer_zakelijk}}</td>

                                <td>

                                    <!-- Kleur bepaling -->

                                    <i

                                            @if($whav->afspraak->afspraaktype_id->id == 3 || $whav->afspraak->afspraaktype_id->id == 5)

                                                class="md-color-light-blue-A700"

                                            @elseif($whav->afspraak->afspraaktype_id->id == 4)

                                                class="md-color-green-A700"

                                    @else



                                            @endif

                                    >

                                        @if($whav->afspraak->afspraaktype_id->id == 3)

                                            Geld ophalen, zie factuur voor meer info

                                        @elseif($whav->afspraak->afspraaktype_id->id == 5)

                                            Betaalherinnering

                                        @endif

                                        <!-- Werkzaamheden ophalen -->

														<?php $ahas = App\Models\Appointment_has_activities::aha($whav->afspraakID, $whav->klant_id->id); ?>

                                        @if(!$ahas->isEmpty())

                                            @foreach($ahas as $aha)

                                                @if($aha->afspraak_id == $whav->afspraakID)

                                                    {{$aha->aantal}}{{$aha->activiteit->eenheid}}

                                                    {{$aha->activiteit->omschrijving}} <i>{{$aha->opmerking}}</i> à €

                                                    {{number_format($aha->prijs,2)}} <br>

                                                @endif

                                            @endforeach

                                        @endif </i>

                                    <!-- Afspraak toevoeging ophalen -->

                                    <br> <span
                                            class="uk-text-bold uk-text-danger">{!!$whav->afspraak->omschrijving!!}</span>

                                    <br>

                                    <!-- Redenen waarom de vorige afspraak eventueel verzet was. -->

												  <?php /*@foreach($redenen as $reden)

                                        @if($whav->id == $reden->nieuwe_afspraak && $reden->datum_oude_afspraak != null)

                                            <em>{{date('d-m-Y', strtotime($reden->datum_oude_afspraak))}}: {{$reden->reden}}</em>

                                        @endif

                                    @endforeach */ ?>

                                    <em style="font-size:12px;">{!!strip_tags(App\Models\Appointment::getStaticReden($whav->afspraakID), '<br>')!!}</em>


                                    @if($whav->geschiedenis != null)

                                        @foreach($whav->geschiedenis as $history)

                                            <br>

                                            <i class="md-bg-grey-500">

                                                @if($history->geschiedenis != null)

                                                    {{$history->geschiedenis->datum}}

                                                    {{$history->geschiedenis->omschrijving}}

                                                @endif

                                            </i>

                                        @endforeach

                                    @endif

                                </td>

                                <td><b>€ {{number_format($whav->afspraak->totaalbedrag,2)}}</b></td>

                                <td>{{$whav->status_id->omschrijving}}</td>

                                <td>@if($whav->status_id->id == 1)
                                        <a target="_blank" href="/afspraak/{{$whav->afspraakID}}/verwerken">Verwerk
                                            Afspraak</a>
                                    @endif</td>

                            </tr>

                        @endforeach

                        </tbody>

                    </table>

                </div>

            </div>

        </div>



        @foreach($werklijsten as $werklijst)

            <div class="md-card uk-margin-medium-bottom">

                <div class="md-card-content">

                    <div class="uk-overflow-container">

                        <table class="uk-table" id="{{$werklijst->id}}">

                            <caption>

                                @if($werklijst->verkoper == null && $werklijst->veger == null)

                                    Voeg werknemers toe (max. 2)

                                @else

                                    {{$werklijst->verkoper->voornaam}}

                                    @if($werklijst->veger != null)

                                        + {{$werklijst->veger->voornaam}}

                                    @endif

                                @endif

                            </caption>

                            <a align="left" href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/instellingen">Instellingen
                                werklijst</a> ||

                            <a align="left" href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/print">Print
                                werklijst</a> ||

                            <a align="left" href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/verwijderen">Verwijder
                                Werklijst</a>

                    </div>

                    <thead>

                    <tr>

                        <th>Datum en Tijd</th>

                        <th>Relatie</th>

                        <th>Adres</th>

                        <th>Contactgegevens</th>

                        <th>Werkzaamheden</th>

                        <th>Totaalbedrag</th>

                        <th>Status</th>

                        <th></th>

                    </tr>

                    </thead>

                    <tbody>

                    @foreach($whas as $wha)

                        @if($wha->werklijst->id == $werklijst->id)

                            <tr id="{{$wha->id}}">

                                <td>

                                    <!-- if($whav === end($verzetten))

                                    if(date('H:i', strtotime($whav->startdatum)) > date('H:i', strtotime(next($verzetten->startdatum))))

                                        <em>

                                    endif

                                    endif -->


                                    {{date('H:i', strtotime($wha->afspraak_id->startdatum))}}
                                    - {{date('H:i', strtotime($wha->afspraak_id->einddatum))}}



                                    <!-- if($whav === end($verzetten))

                                    if(date('H:i', strtotime($whav->startdatum)) > date('H:i', strtotime(next($verzetten->startdatum))))

                                        </em>

                                    endif

                                    endif -->

                                </td>

                                <td>
                                    <a href="/relatie/{{$wha->afspraak_id->klant_id->id}}">Fam. {{$wha->afspraak_id->klant_id->achternaam}}</a>
                                </td>

                                <td>{{$wha->afspraak_id->klant_id->straat}} {{$wha->afspraak_id->klant_id->huisnummer}} {{$wha->afspraak_id->klant_id->hn_prefix}}
                                    ,
                                    <br> {{$wha->afspraak_id->klant_id->postcode}}  {{$wha->afspraak_id->klant_id->woonplaats}}
                                </td>

                                <td>{{$wha->afspraak_id->klant_id->telefoonnummer_prive}}
                                    | {{$wha->afspraak_id->klant_id->telefoonnummer_zakelijk}}</td>

                                <td>

                                    <!-- Kleur bepaling -->


                                    <i

                                            @if($wha->afspraak_id->afspraaktype_id->id == 3 || $wha->afspraak_id->afspraaktype_id->id == 5)

                                                class="md-color-light-blue-A700"

                                            @elseif($wha->afspraak_id->afspraaktype_id->id == 4)

                                                class="md-color-green-A700"

                                    @else



                                            @endif

                                    >

                                        @if($wha->afspraak_id->afspraaktype_id->id == 3)

                                            Geld ophalen, zie factuur voor meer info

                                        @elseif($wha->afspraak_id->afspraaktype_id->id == 5)

                                            Betaalherinnering

                                        @endif

                                        <!-- Werkzaamheden ophalen -->

														<?php $ahas = App\Models\Appointment_has_activities::aha($wha->afspraak_id->id, $wha->afspraak_id->klant_id->id); ?>

                                        @if(!$ahas->isEmpty())

                                            @foreach($ahas as $aha)

                                                @if($aha->afspraak_id == $wha->afspraak_id->id)

                                                    {{$aha->aantal}}{{$aha->activiteit->eenheid.' '}}

                                                    {{$aha->activiteit->omschrijving}} <i>{{$aha->opmerking}}</i> à €

                                                    {{number_format($aha->prijs,2)}} <br>

                                                @endif

                                            @endforeach

                                        @endif </i>

                                    <!-- Afspraak toevoeging ophalen -->

                                    <br> <span
                                            class="uk-text-bold uk-text-danger">{!!$wha->afspraak_id->omschrijving!!}</span>

                                    <br>

                                    <!-- Redenen waarom de vorige afspraak eventueel verzet was. -->

												  <?php /*@foreach($redenen as $reden)

                                        @if($whav->id == $reden->nieuwe_afspraak && $reden->datum_oude_afspraak != null)

                                            <em>{{date('d-m-Y', strtotime($reden->datum_oude_afspraak))}}: {{$reden->reden}}</em>

                                        @endif

                                    @endforeach */ ?>

                                    <em style="font-size:12px;">{!!strip_tags(App\Models\Appointment::getStaticReden($wha->afspraak_id->id), '<br>')!!}</em>


                                    @if($wha->geschiedenis != null)

                                        @foreach($wha->geschiedenis as $history)

                                            <br>

                                            <i class="md-bg-grey-500">{{$history->geschiedenis->datum}} {{$history->geschiedenis->omschrijving}}</i>

                                        @endforeach

                                    @endif

                                </td>

                                <td><b>€ {{number_format($wha->afspraak_id->totaalbedrag,2)}}</b></td>

                                <td>{{$wha->afspraak_id->getStatus()->omschrijving}}</td>

                                <td>@if($wha->afspraak_id->getStatus()->id == 1)
                                        <a target="_blank" class="md-btn md-btn-success md-btn-wave-light"
                                           href="/afspraak/{{$wha->afspraak_id->id}}/verwerken">Verwerk Afspraak</a>
                                    @endif</td>

                            </tr>

                        @endif

                    @endforeach

                    </tbody>

                    </table>

                </div>

            </div>

            </div>

        @endforeach

    @elseif(Auth::user()->rol == 8)
        <!-- Gebruiker mag zijn eigen werklijst inzien -->



        <h2>Werklijst van {{ $datum }}</h2>



        @if($datum == date('d-m-Y') || $datum == $datum)

            @foreach($werklijsten as $werklijst)

                @if(is_object($werklijst->verkoper))

                    @if(is_object($werklijst->veger))

                        @if($werklijst->veger->id == Auth::user()->werknemer_id)

                            <div class="md-card uk-margin-medium-bottom">

                                <div class="md-card-content">

                                    <div class="uk-overflow-container">

                                        <table class="uk-table" id="{{$werklijst->id}}">

                                            <caption>

                                                @if($werklijst->verkoper == null && $werklijst->veger == null)

                                                    Voeg werknemers toe (max. 2)

                                                @else

                                                    {{$werklijst->verkoper->voornaam}}

                                                    @if($werklijst->veger != null)

                                                        + {{$werklijst->veger->voornaam}}

                                                    @endif

                                                @endif

                                            </caption>

                                            <a align="left"
                                               href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/instellingen">Instellingen
                                                werklijst</a> ||

                                            <a align="left"
                                               href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/print">Print
                                                werklijst</a> ||

                                            <a align="left"
                                               href="/planning/verkoop/{{$datum}}/{{$werklijst->id}}/verwijderen">Verwijder
                                                Werklijst</a>

                                    </div>

                                    <thead>

                                    <tr>

                                        <th>Datum en Tijd</th>

                                        <th>Relatie</th>

                                        <th>Adres</th>

                                        <th>Contactgegevens</th>

                                        <th>Werkzaamheden</th>

                                        <th>Totaalbedrag</th>

                                        <th>Functies</th>

                                    </tr>

                                    </thead>

                                    <tbody>

                                    @foreach($whas as $wha)

                                        @if($wha->werklijst->id == $werklijst->id)

                                            <tr id="{{$wha->id}}">

                                                <td>

                                                    {{date('H:i', strtotime($wha->afspraak_id->startdatum))}}
                                                    - {{date('H:i', strtotime($wha->afspraak_id->einddatum))}}

                                                </td>

                                                <td>
                                                    <a href="/relatie/{{$wha->afspraak_id->klant_id->id}}">Fam. {{$wha->afspraak_id->klant_id->achternaam}}</a>
                                                </td>

                                                <td>{{$wha->afspraak_id->klant_id->straat}} {{$wha->afspraak_id->klant_id->huisnummer}} {{$wha->afspraak_id->klant_id->hn_prefix}}
                                                    ,
                                                    <br> {{$wha->afspraak_id->klant_id->postcode}}  {{$wha->afspraak_id->klant_id->woonplaats}}
                                                </td>

                                                <td>{{$wha->afspraak_id->klant_id->telefoonnummer_prive}}
                                                    | {{$wha->afspraak_id->klant_id->telefoonnummer_zakelijk}}</td>

                                                <td>

                                                    <!-- Kleur bepaling -->

                                                    <i

                                                            @if($wha->afspraak_id->afspraaktype_id->id == 3 || $wha->afspraak_id->afspraaktype_id->id == 5)

                                                                class="md-color-light-blue-A700"

                                                            @elseif($wha->afspraak_id->afspraaktype_id->id == 4)

                                                                class="md-color-green-A700"

                                                    @else



                                                            @endif

                                                    >

                                                        @if($wha->afspraak_id->afspraaktype_id->id == 3)

                                                            Geld ophalen, zie factuur voor meer info

                                                        @elseif($wha->afspraak_id->afspraaktype_id->id == 5)

                                                            Betaalherinnering

                                                        @endif

                                                        <!-- Werkzaamheden ophalen -->

																			 <?php $ahas = App\Models\Appointment_has_activities::aha($wha->afspraak_id->id, $wha->afspraak_id->klant_id->id); ?>

                                                        @if(!$ahas->isEmpty())

                                                            @foreach($ahas as $aha)

                                                                @if($aha->afspraak_id == $wha->afspraak_id->id)

                                                                    {{$aha->aantal}}{{$aha->activiteit->eenheid.' '}}

                                                                    {{$aha->activiteit->omschrijving}}
                                                                    <i>{{$aha->opmerking}}</i> à €

                                                                    {{number_format($aha->prijs,2)}} <br>

                                                                @endif

                                                            @endforeach

                                                        @endif </i>


                                                    <!-- Afspraak toevoeging ophalen -->

                                                    <br> <span
                                                            class="uk-text-bold uk-text-danger">{!!$wha->omschrijving!!}</span>

                                                    <br>

                                                    <!-- Redenen waarom de vorige afspraak eventueel verzet was. -->

																		<?php /*@foreach($redenen as $reden)

                                                            @if($whav->id == $reden->nieuwe_afspraak && $reden->datum_oude_afspraak != null)

                                                                <em>{{date('d-m-Y', strtotime($reden->datum_oude_afspraak))}}: {{$reden->reden}}</em>

                                                            @endif

                                                        @endforeach */ ?>

                                                    <em style="font-size:12px;">{!!strip_tags(App\Models\Appointment::getStaticReden($wha->afspraak_id->id), '<br>')!!}</em>


                                                    @if($wha->geschiedenis != null)

                                                        @foreach($wha->geschiedenis as $history)

                                                            <br>

                                                            <i class="md-bg-grey-500">{{$history->geschiedenis->datum}} {{$history->geschiedenis->omschrijving}}</i>

                                                        @endforeach

                                                    @endif

                                                </td>

                                                <td><b>€ {{number_format($wha->totaalbedrag,2)}}</b></td>

                                                <td><a href="/afspraak/">Start Afspraak</a></td>

                                            </tr>

                                        @endif

                                    @endforeach

                                    </tbody>

                                    </table>

                                </div>

                            </div>

                            </div>

                        @endif

                    @endif

                    @if($werklijst->verkoper->id == Auth::user()->werknemer_id)

                        <div class="md-card uk-margin-medium-bottom">

                            <div class="md-card-content">

                                <div class="uk-overflow-container">

                                    <table class="uk-table" id="{{$werklijst->id}}">

                                        <caption>

                                            @if($werklijst->verkoper == null && $werklijst->veger == null)

                                                Voeg werknemers toe (max. 2)

                                            @else

                                                {{$werklijst->verkoper->voornaam}}

                                                @if($werklijst->veger != null)

                                                    + {{$werklijst->veger->voornaam}}

                                                @endif

                                            @endif

                                        </caption>

                                </div>

                                <thead>

                                <tr>

                                    <th>Datum en Tijd</th>

                                    <th>Relatie</th>

                                    <th>Adres</th>

                                    <th>Contactgegevens</th>

                                    <th>Werkzaamheden</th>

                                    <th>Totaalbedrag</th>

                                    <th>Functies</th>

                                </tr>

                                </thead>

                                <tbody>

                                @foreach($whas as $wha)

                                    @if($wha->werklijst->id == $werklijst->id)

                                        <tr id="{{$wha->werklijst_id}}">

                                            <td>

                                                <!-- if($whav === end($verzetten))

                                                if(date('H:i', strtotime($whav->startdatum)) > date('H:i', strtotime(next($verzetten->startdatum))))

                                                    <em>

                                                endif

                                                endif -->


                                                {{date('H:i', strtotime($wha->afspraak_id->startdatum))}}
                                                - {{date('H:i', strtotime($wha->afspraak_id->einddatum))}}



                                                <!-- if($whav === end($verzetten))

                                                        if(date('H:i', strtotime($whav->startdatum)) > date('H:i', strtotime(next($verzetten->startdatum))))

                                                            </em>

                                                        endif

                                                        endif -->

                                            </td>

                                            <td>
                                                <a href="/relatie/{{$wha->afspraak_id->klant_id->id}}">Fam. {{$wha->afspraak_id->klant_id->achternaam}}</a>
                                            </td>

                                            <td>{{$wha->afspraak_id->klant_id->straat}} {{$wha->afspraak_id->klant_id->huisnummer}} {{$wha->afspraak_id->klant_id->hn_prefix}}
                                                ,
                                                <br> {{$wha->afspraak_id->klant_id->postcode}}  {{$wha->afspraak_id->klant_id->woonplaats}}
                                            </td>

                                            <td>{{$wha->afspraak_id->klant_id->telefoonnummer_prive}}
                                                | {{$wha->afspraak_id->klant_id->telefoonnummer_zakelijk}}</td>

                                            <td>

                                                <!-- Kleur bepaling -->

                                                <i

                                                        @if($wha->afspraak_id->afspraaktype_id->id == 3 || $wha->afspraak_id->afspraaktype_id->id == 5)

                                                            class="md-color-light-blue-A700"

                                                        @elseif($wha->afspraak_id->afspraaktype_id->id == 4)

                                                            class="md-color-green-A700"

                                                @else



                                                        @endif

                                                >

                                                    @if($wha->afspraak_id->afspraaktype_id->id == 3)

                                                        Geld ophalen, zie factuur voor meer info

                                                    @elseif($wha->afspraak_id->afspraaktype_id->id == 5)

                                                        Betaalherinnering

                                                    @endif

                                                    <!-- Werkzaamheden ophalen -->

																		<?php $ahas = App\Models\Appointment_has_activities::aha($wha->afspraak_id->id, $wha->afspraak_id->klant_id->id); ?>

                                                    @if(!$ahas->isEmpty())

                                                        @foreach($ahas as $aha)

                                                            @if($aha->afspraak_id == $wha->afspraak_id->id)

                                                                {{$aha->aantal}}

                                                                {{$aha->activiteit->omschrijving}}
                                                                <i>{{$aha->opmerking}}</i> à €

                                                                {{number_format($aha->prijs,2)}} <br>

                                                            @endif

                                                        @endforeach

                                                    @endif </i>

                                                <!-- Afspraak toevoeging ophalen -->

                                                <br> <span
                                                        class="uk-text-bold uk-text-danger">{!!$wha->omschrijving!!}</span>

                                                <br>

                                                <!-- Redenen waarom de vorige afspraak eventueel verzet was. -->

																  <?php /*@foreach($redenen as $reden)

                                                            @if($whav->id == $reden->nieuwe_afspraak && $reden->datum_oude_afspraak != null)

                                                                <em>{{date('d-m-Y', strtotime($reden->datum_oude_afspraak))}}: {{$reden->reden}}</em>

                                                            @endif

                                                        @endforeach */ ?>

                                                <em style="font-size:12px;">{!!strip_tags(App\Models\Appointment::getStaticReden($wha->afspraak_id->id), '<br>')!!}</em>


                                                @if($wha->geschiedenis != null)

                                                    @foreach($wha->geschiedenis as $history)

                                                        <br>

                                                        <i class="md-bg-grey-500">{{$history->geschiedenis->datum}} {{$history->geschiedenis->omschrijving}}</i>

                                                    @endforeach

                                                @endif

                                            </td>

                                            <td><b>€ {{number_format($wha->afspraak_id->totaalbedrag,2)}}</b></td>

                                            <td>
                                                <a href="/planning/verkoop/vandaag/{{$wha->werklijst_id}}/{{$wha->afspraak_id->id}}/start"
                                                   class="md-btn md-btn-success md-btn-wave-light">Start Afspraak</a>
                                            </td>

                                        </tr>

                                    @endif

                                @endforeach

                                </tbody>

                                </table>

                            </div>

                        </div>

                        </div>

                    @endif

                @endif

            @endforeach

        @else

            Er is nog geen informatie beschikbaar. wacht de datum af.

        @endif

    @endif

@endsection
