@extends('layouts.master')

@section('title', 'Flexavi - Planning')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)
<?php 
setlocale(LC_ALL, 'nl_NL');
?>

<h4 class="heading_a uk-margin-bottom">Planning</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-striped">
                    <thead>
                    <tr>
                        <th>Datum</th>
                        <th>Aantal Ploegen</th>
                        <th>Max aantal klanten</th>
                        <th>Klanten ingepland</th>
                        <th>Tekort aan klanten</th>
                        <th>Functie</th>
                    </tr>
                    </thead>
                    <tbody>
                        {{-- {{dd($aantalperdag)}} --}}
                    @foreach($aantalperdag as $ap)
                        @foreach($werkdagen as $wd)
                        @if($ap['datum'] == $wd->datum && $wd->behandeld < 2)
                            <?php 
                                $day = date('N', strtotime($ap['datum'])); 
                                $old_count = (isset($count)) ? $count : 0; 
                                $count = date('N', strtotime($ap['datum'])); 
                            ?>
                            @if(  (date('N', strtotime($ap['datum'])) < $old_count) || $old_count == $count  )
                                <tr><td><br></td><td><td></td><td></td></td><td></td><td></td><td></td></tr>
                                <?php $bool = true; ?>
                            @endif
                            <tr>

                                <td>
                                    <a href="/planning/verkoop/{{date('d-m-Y', strtotime($ap['datum']))}}">{{ strftime('%A %d-%m-%Y',strtotime($ap['datum']))}} </a>  
                                </td>
                                <td>{{$wd->aantal_lijsten}}</td>
                                <td>{{$wd->aantal_klanten}}</td>
                                <td>
                                    ({{ ($ap['total'] - $ap['aantalUB'] - $ap['aantalTBV']) }} + {{$ap['aantalUB']}})
                                </td>
                                <td><span class="uk-text-bold md-color-red-800">@if($wd->aantal_klanten - $ap['total'] > 0) {{$wd->aantal_klanten - $ap['total']}} @else 0 @endif</span></td>
                                <td>
                                    @if(Auth::user()->rol < 7)
                                        @if($wd->behandeld == 0)
                                            <a href="/planning/verkoop/{{date('d-m-Y', strtotime($ap['datum']))}}/{{$wd->id}}/aanpassen" class="md-btn md-btn-warning md-btn-wave-light"><i class="material-icons">build</i>Wijzig Werkdag</a>
                                            @if($wd->ublCheck == 0)
                                                <a href="/planning/verkoop/{{date('d-m-Y', strtotime($ap['datum']))}}/{{$wd->id}}/dagstaat" class="md-btn md-btn-success md-btn-wave-light"><i class="material-icons">euro_symbol</i>Vul UB lijst aan</a>
                                            @endif
                                            <a href="/planning/verkoop/{{date('d-m-Y', strtotime($ap['datum']))}}/{{$wd->id}}/behandeld" class="md-btn md-btn-primary md-btn-wave-light"><i class="material-icons">check</i>Markeer als behandeld</a>
                                        @elseif($wd->behandeld == 1 && Auth::user()->rol < 6)
                                            @if($wd->ublCheck < 2)
                                                <a href="/planning/verkoop/{{date('d-m-Y', strtotime($ap['datum']))}}/{{$wd->id}}/dagstaat/controle" class="md-btn md-btn-success md-btn-wave-light"><i class="material-icons">euro_symbol</i>Controleer UB Lijst</a>
                                            @endif
                                            <a href="/planning/verkoop/{{date('d-m-Y', strtotime($ap['datum']))}}/{{$wd->id}}/controle" class="md-btn md-btn-primary md-btn-wave-light"><i class="material-icons">check</i>*Mee Bezig*Controleer de werklijst</a>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endif

@endsection