@extends('layouts.master')

@section('title', 'Controle Werkdag')
@push('scripts')

    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>
    <!-- C:\xampp\htdocs\flexavi\public\assets\assets\js -->
    <script src="{{ URL::asset('assets/assets/js/autocomplete.js')}}"></script>
@endpush

@section('content')
@if(Auth::user()->rol != 99)
<?php 
setlocale(LC_ALL, 'nl_NL');
?>

<h1>Controle werkdag: {{ strftime('%A %d-%m-%Y',strtotime($date))}} </h1>

    <div class="uk-width-medium-1-1" align="right"> 
        <a class="md-btn md-btn-success md-btn-wave-light" href="/planning/verkoop/{{$date}}/{{$wd->id}}/dagstaat">Vul UB lijst in</a>
        @if($wd->ublCheck == 1)
        <a class="md-btn md-btn-success md-btn-wave-light" href="/planning/verkoop/{{$date}}/{{$wd->id}}/controle/afgerond">Controle afgerond</a>
        @endif
    </div>
@foreach($werklijsten as $werklijst)
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-2">
        <h4 class="heading_a uk-margin-bottom">Werklijst @if($werklijst->verkoper != null) {{$werklijst->verkoper->voornaam}} @endif</h4>
    </div>
</div>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table class="uk-table uk-table-striped">
                <thead>
                    <tr>
                        <th>Relatie</th>
                        <th>Contactgegevens</th>
                        <th>Werkzaamheden</th>
                        <th>Verwerking</th>
                        <th>Verwerkt door</th>
                        <th>Verwerkt op</th>
                        <th>Laatste geschiedenisregels betreft deze afspraak</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($whas as $wha)
                        @if($werklijst->id == $wha->werklijst_id)
                            <tr>
                                <td width="5%"><a href="/relatie/{{$wha->afspraak_id->klant_id->id}}">Fam. {{$wha->afspraak_id->klant_id->achternaam}}</a></td>
                                <td width="7%">{{$wha->afspraak_id->klant_id->telefoonnummer_prive}} @if($wha->afspraak_id->klant_id->telefoonnummer_zakelijk != null) | {{$wha->afspraak_id->klant_id->telefoonnummer_zakelijk}} @endif <br /> {{$wha->afspraak_id->klant_id->email}}</td>
                                <td width="17%">
                                    <u> Type: {{$wha->afspraak_id->afspraaktype_id->omschrijving}} </u><br>
                                    @foreach($ahas as $aha)
                                        @if($aha->afspraak_id == $wha->afspraak_id->id)
                                            {{$aha->aantal}}x
                                            {{$aha->activiteit->omschrijving}} à €
                                            {{number_format($aha->prijs,2)}} <br>
                                        @endif
                                    @endforeach
                                    <b>Totaal bedrag: € {{number_format($wha->afspraak_id->totaalbedrag, 2)}}</b>
                                </td> 
                                <td width="7%">{{$wha->afspraak_id->getStatus()->omschrijving}}</td>
                                <td width="5%">{{$wha->afspraak_id->verwerkt_door->name}}</td>
                                <td width="7%">{{date('d-m-Y H:i', strtotime($wha->afspraak_id->updated_at))}}</td>
                                <td width="32%">
                                    @foreach($wha->afspraak_id->getHistory() as $regel)
                                    <ul>
                                    @if($regel->gebruiker_id == $wha->afspraak_id->verwerkt_door->id)
                                        <li>   {{$regel->datum}}; {{$regel->omschrijving}} </li>
                                    @endif
                                    </ul>
                                    @endforeach
                                </td>
                                <td width="15%">
                                    <a href="/relatie/{{$wha->afspraak_id->klant_id->id}}/zetAanvulling/{{$date}}/{{$wd->id}}">Zet in aanvulling</a>
                                    @if(count($wha->aanvulling($wha->afspraak_id->id)) > 0)
                                        <br>
                                        Deze klant staat al in de aanvulling bij:

                                        @foreach($wha->aanvulling($wha->afspraak_id->id) as $tmp)
                                            <br> <b>→</b> {{$tmp->ontvanger->name}}
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endforeach

@endif
@endsection