@extends('layouts.master')


@section('title', 'Werklijst')

@push('scripts')

    <script src="{{ URL::asset('assets/assets/js/pages/page_invoices.min.js')}}"></script>

@endpush

@section('content')

    <?php

    setlocale(LC_ALL, 'nl_NL');

    ?>

    @if(Auth::user()->rol != 99)

        <div class="md-card-toolbar hidden-print">

            <div class="md-card-toolbar-actions hidden-print">

                <i class="md-icon material-icons" id="invoice_print">&#xE8ad;</i>

            </div>

            <h2 class="md-card-toolbar-heading-text large" id="invoice_name">

                {{ App\Models\Worklist::getCorrectPersonel($werklijst->id) }}

                ({{$aantal_klanten}} + {{$aantal_garanties}}) ||
                Werklijst {{strftime('%A',strtotime($werklijst->datum))}} {{date('d-m-Y', strtotime($werklijst->datum))}}
                || {{number_format($werklijst->afstand,0)}} KM

            </h2>

        </div>

        <div class="md-card-content invoice_content print_bg invoice_footer_active">

            <div class="invoice_header">

                <h1><span class="uk-text-muted uk-text-large uk-text-italic">

        {{ App\Models\Worklist::getCorrectPersonel($werklijst->id) }}

        ({{$aantal_klanten}} + {{$aantal_garanties}}) || Werklijst {{strftime('%A',strtotime($werklijst->datum))}} {{date('d-m-Y', strtotime($werklijst->datum))}} || {{number_format($werklijst->afstand,0)}} KM
                </h1>

                </span>

            </div>


            <div class="uk-grid uk-margin-large-bottom">

                <div class="uk-width-1-1">

                    <table class="uk-table uk-table-hover">

                        <thead>

                        <tr>

                            <th>Tijd</th>

                            <th>Relatie</th>

                            <th>Adres</th>

                            <th>Contactgegevens</th>

                            <th>Werkzaamheden</th>

                            <th>Bedrag</th>

                        </tr>

                        </thead>

                        <tbody>

                            <?php

                            $count = 1;

                            $divider = floor(count($whas) / 2);

                            ?>

                        @foreach($whas as $wha)

                            <tr>

                                <td>

                                    @if($count <= $divider && date('H:i', strtotime($wha->afspraak_id->startdatum)) > date('H:i', strtotime('08:00')))

                                        <span class="uk-text-bold md-color-red-A700">

                                @elseif($count > $divider && date('H:i', strtotime($wha->afspraak_id->startdatum)) < date('H:i', strtotime('12:30')))

                                                <span class="uk-text-bold md-color-red-A700">

                                @else

                                                        <span>

                                @endif

                                                            {{--  @if($count == 1 && count($rood)-1 > 0)  Startpositie

                                                                @if($rood[$count]['afspraak_id']['startdatum'] != $wha->afspraak_id->startdatum && date('H:i', strtotime($wha->afspraak_id->startdatum)) != date('H:i', strtotime('08:00')))

                                                                    <span class="uk-text-bold md-color-red-A700">

                                                                @endif

                                                            @elseif($count-1 >= 0 && count($rood) > 1)  Elk vervolg

                                                                @if($count == count($rood)) {{-- Laatste positie

                                                                    @if($rood[$count-2]['afspraak_id']['startdatum'] != $wha->afspraak_id->startdatum || date('H:i', strtotime($wha->afspraak_id->startdatum)) != date('H:i', strtotime('12:30')))

                                                                        <span class="uk-text-bold md-color-red-A700">

                                                                    @else

                                                                        <span>

                                                                    @endif

                                                                @elseif($count < count($rood)) {{-- Elk andere positie -

                                                                    @if( ($rood[$count-2]['afspraak_id']['startdatum'] != $wha->afspraak_id->startdatum && $rood[$count]['afspraak_id']['startdatum'] != $wha->afspraak_id->startdatum) ||                  date('H:i', strtotime($rood[$count-2]['afspraak_id']['startdatum'])) > date('H:i', strtotime($wha->afspraak_id->startdatum)))

                                                                        <span class="uk-text-bold md-color-red-A700">



                                                                    @elseif( date('H:i', strtotime($rood[$count-2]['afspraak_id']['startdatum'])) == date('H:i', strtotime($wha->afspraak_id->startdatum)) &&                                                  date('H:i', strtotime($rood[$count]['afspraak_id']['startdatum'])) > date('H:i', strtotime($wha->afspraak_id->startdatum)))

                                                                        <span class="uk-text-bold md-color-red-A700">

                                                                    @else

                                                                        <span>

                                                                    @endif

                                                                @endif

                                                            @else

                                                                <span>

                                                            @endif  --}}



                                                                <?php $count++; ?>

                                                            {{date('H:i', strtotime($wha->afspraak_id->startdatum))}} - {{date('H:i', strtotime($wha->afspraak_id->einddatum))}} </span>

                                </td>

                                <td>@if($wha->afspraak_id->afspraaktype_id->id == 3)

                                        <i class="uk-text md-color-light-blue-A700">

                                            @elseif($wha->afspraak_id->afspraaktype_id->id == 4)

                                                <i class="uk-text md-color-green-A700">

                                                    @else <i> @endif

                                                        @if($wha->afspraak_id->klant_id->voornaam == null)

                                                            Fam.

                                                        @else

                                                            {{$wha->afspraak_id->klant_id->voornaam}}

                                                        @endif

                                                        {{$wha->afspraak_id->klant_id->achternaam}}</i>

                                </td>

                                <td>@if($wha->afspraak_id->afspraaktype_id->id == 3)

                                        <i class="uk-text md-color-light-blue-A700">

                                            @elseif($wha->afspraak_id->afspraaktype_id->id == 4)

                                                <i class="uk-text md-color-green-A700">

                                                    @else <i> @endif

                                                        {{$wha->afspraak_id->klant_id->straat}} {{$wha->afspraak_id->klant_id->huisnummer}} {{$wha->afspraak_id->klant_id->hn_prefix}}

                                                        <br> {{$wha->afspraak_id->klant_id->postcode}} {{$wha->afspraak_id->klant_id->woonplaats}}
                                                    </i>

                                </td>

                                <td>@if($wha->afspraak_id->afspraaktype_id->id == 3)

                                        <i class="uk-text md-color-light-blue-A700">

                                            @elseif($wha->afspraak_id->afspraaktype_id->id == 4)

                                                <i class="uk-text md-color-green-A700">

                                                    @else <i> @endif

                                                        {{$wha->afspraak_id->klant_id->telefoonnummer_prive}}
                                                        <br> {{$wha->afspraak_id->klant_id->telefoonnummer_zakelijk}}
                                                    </i>

                                </td>

                                <td>@if($wha->afspraak_id->afspraaktype_id->id == 3)

                                        <i class="uk-text md-color-light-blue-A700"> Geld ophalen, zie factuur en
                                            profiel
                                            voor meer info

                                            @elseif($wha->afspraak_id->afspraaktype_id->id == 4)

                                                <i class="uk-text md-color-green-A700">

                                                    @else <i> @endif

                                                            <?php

                                                            $afspraak_id = $wha->afspraak_id->id;

                                                            $klant_id = $wha->afspraak_id->klant_id->id;

                                                            ?>

                                                        @foreach(App\Models\Appointment_has_activities::aha($afspraak_id, $klant_id) as $aha)

                                                            {{$aha->aantal}}{{$aha->activiteit->eenheid}} {{$aha->activiteit->omschrijving}}
                                                            <i>{{$aha->opmerking}}</i> à
                                                            € {{number_format($aha->prijs,2)}} <br/>

                                                        @endforeach

                                                        <br>

                                                        <i>{!!$wha->afspraak_id->omschrijving!!}</i>

                                                        <br>

                                                        @foreach($ghas as $gha)

                                                            @if($gha->afspraak_id == $wha->afspraak_id->id)

                                                                {!!$gha->omschrijving!!}

                                                            @endif

                                                        @endforeach</i>

                                                    <em style="font-size:12px;">{!!strip_tags(App\Models\Appointment::getStaticReden($wha->afspraak_id->id), '<br>')!!}</em>

                                </td>

                                <td>@if($wha->afspraak_id->afspraaktype_id->id == 3)

                                        <i class="uk-text md-color-light-blue-A700">

                                            @elseif($wha->afspraak_id->afspraaktype_id->id == 4)

                                                <i class="uk-text md-color-green-A700">

                                                    @else <i> @endif

                                                        € {{number_format($wha->afspraak_id->totaalbedrag, 2)}}</i></td>

                            </tr>

                        @endforeach

                        </tbody>

                    </table>

                </div>

            </div>

            <div class="uk-grid">

                <div class="uk-width-1-1">

                    <span class="uk-text-muted uk-text-small uk-text-italic">Extra:</span>

                    <p class="uk-margin-top-remove">

                        Wisselgeld: € {{number_format($werklijst->wisselgeld,2)}} <br>

                        Facturen: {{$werklijst->aantal_facturen}} <br>

                        WO's: {{$werklijst->aantal_wo}}<br>

                    </p>

                </div>

            </div>

            <div class="uk-grid">

                <div class="uk-width-1-1">

                    <span class="uk-text-muted uk-text-small uk-text-italic">Opmerking:</span>

                    <p class="uk-margin-top-remove">

                        @if($werklijst->opmerking == "")

                            <span> Bel afwijkende klanten van te voren of ze wel thuis zijn, dit scheelt kilometers en tijd ! </span>

                        @else

                            <em><b>{{$werklijst->opmerking}}        </b></em>

                        @endif

                    </p>

                </div>

            </div>

            <div class="invoice_footer"></div>

        </div>

    @endif

@endsection
