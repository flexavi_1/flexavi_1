@extends('layouts.master')

@section('title', 'Controle Werklijsten')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h4 class="heading_a uk-margin-bottom">Mee Bezig!</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            {!! Form::open(array('url'=>'relatie/nieuw', 'method' => 'post', 'class' => 'straat' )) !!}
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium">
                    <div class="uk-form-row">
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-3">
                                <label>Naam</label>
                                {!! Form::text('voornaam', null, array('class' => 'md-input')) !!}
                            </div>
                            <div class="uk-width-medium-2-3">
                                <label>Achternaam/Bedrijfsnaam</label>
                                <input type="text" class="md-input" name="achternaam" required/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium">
                    <div class="uk-form-row">
                        <div align="right">
                            <div class="md-btn-group">
                                <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg Relatie Toe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}

       </div>
    </div>


@endif

@endsection
