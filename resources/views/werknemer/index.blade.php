@extends('layouts.master')

@section('title', 'Werknemer Overzicht')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>


@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h4 class="heading_a uk-margin-bottom">Werknemer Overzicht</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Naam</th>
                		<th>Adres</th>
                		<th>Contactgegevens</th>
                		<th>Functie</th>
                        <th>In dienst</th>
                        <th>Functies</th>
            		</tr>
            	</thead>
			    <tbody>
                    @foreach($wns as $werknemer)
                        <tr>
                            <td><a href="/werknemer/{{$werknemer['id']}}">{{$werknemer['voornaam']}} {{$werknemer['achternaam']}}</a></td>
                            <td><a href="/werknemer/{{$werknemer['id']}}">{{$werknemer['straat']}} {{$werknemer['huisnummer']}} {{$werknemer['hn_prefix']}}, <br> {{$werknemer['postcode']}} {{$werknemer['woonplaats']}}</a></td>
                            <td><a href="/werknemer/{{$werknemer['id']}}">{{$werknemer['telefoonnummer_prive']}} || {{$werknemer['telefoonnummer_zakelijk']}} <br>{{$werknemer['email']}} </a></td>
                            <td><a href="/werknemer/{{$werknemer['id']}}">{{$werknemer['functie']}}</a></td>
                            <td><a href="/werknemer/{{$werknemer['id']}}">{{$werknemer['actief']}}</a></td>
                            <td><a href="/werknemer/{{$werknemer['id']}}/wijzigen"><i class="material-icons md-24">edit</i></a><a href="/werknemer/{{$werknemer['id']}}/verwijderen"><i class="material-icons md-24">delete</i></a></td>
                        </tr>
                    @endforeach
			    </tbody>
			</table>
		</div>
	</div>
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-3">
        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">
            <button class="md-btn">Zoek per Functie <i class="material-icons">&#xE315;</i></button>
            <div class="uk-dropdown">
                <ul class="uk-nav uk-nav-dropdown">
                    @foreach($functies as $functie)
                    <li><a href="/werknemer/zoeken/{{$functie->titel}}">{{$functie->titel}}</a></li>
                    @endforeach
                    <li><a href="/werknemer">Totaal Overzicht</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-3">
        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">
            <button class="md-btn">Zoek per Status <i class="material-icons">&#xE315;</i></button>
            <div class="uk-dropdown">
                <ul class="uk-nav uk-nav-dropdown">
                    <li><a href="/werknemer/zoeken/1">In dienst</a></li>
                    <li><a href="/werknemer/zoeken/0">Niet in dienst</a></li>
                    <li><a href="/werknemer">Totaal Overzicht</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@endif

@endsection