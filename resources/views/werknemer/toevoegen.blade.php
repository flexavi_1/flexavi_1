@extends('layouts.master')

@section('title', 'Flexavi - Werknemer Toevoegn')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <script src="{{ URL::asset('assets/assets/js/autocomplete.js')}}"></script>

        <script>
  var pro6pp_auth_key = "gfFIwXhAJUBLBDPk";//"Bj6eZ5jna5uLFpeP";
  $(document).ready(function() {
    $(".straat").applyAutocomplete();
    $("#straat").applyAutocomplete();
  });
</script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

 <div class="md-card">
    <div class="md-card-content">
        <h3 class="heading_a">Werknemer Toevoegen</h3>
        {!! Form::open(array('url'=>'werknemer/nieuw', 'method' => 'post', 'class' => 'straat')) !!}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
                            <label>Naam</label>
                            {!! Form::text('voornaam', null, array('class' => 'md-input')) !!}
                        </div>
                        <div class="uk-width-medium-2-3">
                            <label>Achternaam</label>
                            <input type="text" class="md-input" name="achternaam" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label>Postcode</label>
                            <input type="text" maxlength="6"  class="md-input postcode" name="postcode" required/>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <label>Huisnummer</label>
                            <input type="number" class="huisnummer md-input" step="1" name="huisnummer" id="huisnummer" required/>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <label>Toevoeging</label>
                            <input type="text" class="md-input" maxlength="3" name="hn_prefix"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-2-4">
                            <label>Adres</label>
                            <input type="text" class="straat md-input"  name="straat" id="straat" required/>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Woonplaats</label>
                            <input type="text" class="woonplaats md-input"  name="woonplaats" id="woonplaats" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium">
                                <div class="uk-form-row">
                </div>
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label>Telefoonnummer:</label>
                            <input type="text" maxlength="11" required minlength="11" class="md-input" name="telefoonnummer_prive" />
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Mobiele nummer</label>
                            <input type="text"  maxlength="11" minlength="11" class="md-input" name="telefoonnummer_zakelijk"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label>Email</label>
                            <input type="email" class="md-input" name="email"/>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Bankrekeningnummer</label>
                            <input type="text" class="md-input" name="bankrekeningnummer"/>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="actief" value="1" />
        </div>
        <div class="uk-grid" data-uk-grid-margin>
        	<div class="uk-width-medium">
        		<div class="uk-form-row">
        			<div class="uk-width-medium">
                        <label>Functie</label>
        				<select name="functie_id"  id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Definieer de functie.">
        					@foreach($functies as $f)
        						<option value="{{$f->id}}">{{$f->titel}}</option>
        					@endforeach
        				</select>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                                <label>Dagloon</label>
                                <input type="number" class="md-input" step="0.01" name="dagloon" id="dagloon"/>
                        </div>
                        <div class="uk-width-medium-1-2">
                                <label>Uurloon</label>
                                <input type="number" class="md-input" step="0.01" name="uurloon" id="uurloon"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
                                <label>Provisietarief Percentage</label>
                                <input type="number" class="md-input" step="0.01" name="provisieperc" id="provisieperc"/>
                        </div>
                        <div class="uk-width-medium-1-3">
                                <label>Provisietarief Bedrag</label>
                                <input type="number" class="md-input" step="0.01" name="provisiebedrag" id="provisiebedrag"/>
                        </div>
                        <div class="uk-width-medium-1-3">
                                <label>Provisiegrens</label>
                                <input type="number" class="md-input" step="0.01" name="provisiegrens" id="provisiegrens"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
		            <div align="right">
			            	<div class="md-btn-group">
				            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
				                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg Werknemer Toe</button>
			            	</div>
			            </div>
		        	</div>
		        </div>
		    </div>
		</div>
    </div>

		            {!! Form::close() !!}
</div>

@endif



@endsection