@extends('layouts.master')

@section('title', 'Werknemer Overzicht')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h4 class="heading_a uk-margin-bottom">Gebruikers koppelen aan werknemers</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
        {!! Form::open(array('url'=>'werknemer/gebruiker/koppelen', 'method' => 'post')) !!}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
				        	<select id="select_demo_5" name="user_id" data-md-selectize data-md-selectize-bottom>
				        		@foreach($users as $user)
				        			<option value="{{$user->id}}">{{$user->name}}, {{$user->email}}</option>
				        		@endforeach
				        	</select>
				        </div>
				        <div class="uk-width-medium-1-3">
				        	Koppelen aan
				        </div>
				        <div class="uk-width-medium-1-3">
				        	<select id="select_demo_5" name="werknemer_id" data-md-selectize data-md-selectize-bottom>
				        		@foreach($werknemers as $werknemer)
				        			<option value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
				        		@endforeach
				        	</select>
				        </div>
				    </div>
				</div>
			</div>
			<div class="uk-grid" data-uk-grid-margin>
	            <div class="uk-width-medium">
	            	<div class="uk-form-row" align="right">
			            <div align="right">
				            	<div class="md-btn-group" align="right">
					                <button type="submit"  align="right" class="md-btn md-btn-success md-btn-wave-light">Voeg Koppeling Toe</button>
				            	</div>
				            </div>
			        	</div>
			        </div>
			    </div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>



@else

<?php header('Location: '.'/index'); ?>

@endif

@endsection