@extends('layouts.master')

@section('title', 'Flexavi - Werknemer Wijzigen')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush
@section('content')


@if(Auth::user()->rol != 99)

 <div class="md-card">
    <div class="md-card-content">
        <h3 class="heading_a">Werknemer Toevoegen</h3>
        {!! Form::open(array('url'=>'werknemer/'.$werknemer->id.'/wijzigen', 'method' => 'post')) !!}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
                            <label>Naam</label>
                            <input type="text" class="md-input" name="voornaam" value="{{$werknemer->voornaam}}" required/>
                        </div>
                        <div class="uk-width-medium-2-3">
                            <label>Achternaam</label>
                            <input type="text" class="md-input" name="achternaam" value="{{$werknemer->achternaam}}" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-2-4">
                            <label>Adres</label>
                            <input type="text" class="md-input" name="straat" value="{{$werknemer->straat}}" required/>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <label>Huisnummer</label>
                            <input type="number" class="md-input" step="1" name="huisnummer" value="{{$werknemer->huisnummer}}" required/>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <label>Toevoeging</label>
                            <input type="text" class="md-input" maxlength="3" value="{{$werknemer->hn_prefix}}" name="hn_prefix"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label>Postcode</label>
                            <input type="text" maxlength="6" class="md-input" name="postcode" value="{{$werknemer->postcode}}" required/>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Woonplaats</label>
                            <input type="text" class="md-input" name="woonplaats" value="{{$werknemer->woonplaats}}" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label>Telefoonnummer:</label>
                            <input type="text" maxlength="11" required minlength="11" class="md-input" value="{{$werknemer->telefoonnummer_prive}}" name="telefoonnummer_prive" />
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Mobiele nummer</label>
                            <input type="text" class="md-input" value="{{$werknemer->telefoonnummer_zakelijk}}" name="telefoonnummer_zakelijk"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label>Email</label>
                            <input type="email" class="md-input" value="{{$werknemer->email}}" name="email"/>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Bankrekeningnummer</label>
                            <input type="text" class="md-input" value="{{$werknemer->bankrekeningnummer}}" name="bankrekeningnummer"/>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="actief" value="{{$werknemer->actief}}" />
        </div>
        <div class="uk-grid" data-uk-grid-margin>
        	<div class="uk-width-medium">
        		<div class="uk-form-row">
        			<div class="uk-width-medium">
        				<select name="functie_id"  id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Definieer de functie.">
        				    @foreach($functies as $f)
                                @if($f->id == $fwh)
                                  <option selected value="{{$f->id}}">{{$f->titel}}</option>
                                @endif
                            @endforeach
                            @foreach($functies as $f)
                                @if($f->id != $fwh)
                                  <option value="{{$f->id}}">{{$f->titel}}</option>
                                @endif
                            @endforeach
                        </select>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                                <label>Dagloon</label>
                                <input type="number" class="md-input" step="0.01" name="dagloon" value="{{$werknemer->dagloon}}" id="dagloon"/>
                        </div>
                        <div class="uk-width-medium-1-2">
                                <label>Uurloon</label>
                                <input type="number" class="md-input" step="0.01" name="uurloon"  value="{{$werknemer->uurloon}}" id="uurloon"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
                                <label>Provisietarief Percentage</label>
                                <input type="number" class="md-input" step="0.01"  value="{{$werknemer->provisieperc}}" name="provisieperc" id="provisieperc"/>
                        </div>
                        <div class="uk-width-medium-1-3">
                                <label>Provisietarief Bedrag</label>
                                <input type="number" class="md-input" step="0.01"  value="{{$werknemer->provisiebedrag}}" name="provisiebedrag" id="provisiebedrag"/>
                        </div>
                        <div class="uk-width-medium-1-3">
                                <label>Provisiegrens</label>
                                <input type="number" class="md-input" step="0.01" name="provisiegrens"  value="{{$werknemer->provisiegrens}}" id="provisiegrens"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="werknemer_id" value="{{$werknemer->id}}"/>
        <br>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
		            <div align="right">
			            	<div class="md-btn-group">
				            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
				                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg Werknemer Toe</button>
			            	</div>
			            </div>
		        	</div>
		        </div>
		    </div>
		</div>
    </div>

		            {!! Form::close() !!}
</div>

@endif



@endsection