@extends('layouts.master')

@section('title', 'Relatie Overzicht')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)



<h4 class="heading_a uk-margin-bottom">Gebruikers</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
       
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Gebuikersnaam</th>
                		<th>Werknemer</th>
                		<th>Authenticatie #</th>
                        <th>Aangemaakt op</th>
                        <th>Functie</th>
            		</tr>
            	</thead>
			    <tbody> 
                    @foreach($users as $user)

                        <tr>
                            <td><a href="#">{{$user->name}}</a></td>
                            <td><a href="#">{{$user->werknemer->voornaam}} {{$user->werknemer->achternaam}}</a></td>
                            <td><a href="#">{{$user->rol}}</a></td>
                            <td><a href="#">{{date('d-m-Y', strtotime($user->created_at))}}</a></td>
                            <td>@if($user->actief == 1)<a class="md-btn md-btn-danger md-btn-wave-light" href="/user/{{$user->id}}/non-active">Verwijder Account 
                                @else <a class="md-btn md-btn-success md-btn-wave-light" href="/user/{{$user->id}}/non-active">Herstel Account @endif</a>
                                <!-- <a class="md-btn md-btn-primary md-btn-wave-light" href="#">Wijzig Authenticatie</a> --></td>
                        </tr>
                    @endforeach
			    </tbody>
			</table>
		</div>
	</div>
@endif

@endsection