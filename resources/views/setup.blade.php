<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{ URL::asset('assets/assets/img/favicon-16x16.png')}}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/assets/img/favicon-32x32.png')}}" sizes="32x32">

    <title>Flexavi - Setup</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

    <!-- uikit -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/uikit/css/uikit.almost-flat.min.css')}}"/>

    <!-- altair admin login page -->
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/css/login_page.min.css')}}" />



</head>
<body class="login_page">
    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
                    <h3>Welkom {{Auth::user()->name}}</h3>
                    <h4>Dit systeem wordt momenteel geconfigureerd voor u.</h4>
                    <h4>U kunt onderstaande gegevens invullen ter inrichting van uw systeem.</h4>
                </div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/set-up') }}">
                        {{ csrf_field() }}
                        <div class="uk-form-row">
                            <label for="naam" class="col-md-4 control-label">Bedrijfsnaam</label>
                            <input id="naam" type="text" class="md-input" name="naam" required>
                        </div>
                        <div class="uk-form-row">
                            <label for="adres" class="col-md-4 control-label">Adres</label>
                            <input id="adres" type="text" class="md-input" name="adres" required>
                        </div>

                        <div class="uk-form-row">
                            <label for="huisnummer" class="col-md-4 control-label">Huisnummer</label>
                            <input id="huisnummer" type="number" step="1" class="md-input" name="huisnummer" required>
                        </div>

                        <div class="uk-form-row">
                            <label for="postcode" class="col-md-4 control-label">Postcode</label>
                            <input id="postcode" type="text" class="md-input" name="postcode" required>
                        </div>

                        <div class="uk-form-row">
                            <label for="plaats" class="col-md-4 control-label">Plaats</label>
                            <input id="plaats" type="text" class="md-input" name="plaats" required>
                        </div>

                        <div class="uk-form-row">
                            <label for="tel"1 class="col-md-4 control-label">Telefoonnummer</label>
                            <input id="tel1" type="text" class="md-input" name="tel1" required>
                        </div>

                        <div class="uk-form-row">
                            <label for="tel2" class="col-md-4 control-label">Telefoonnummer</label>
                            <input id="tel2" type="text" class="md-input" name="tel2">
                        </div>

                        <div class="uk-form-row">
                            <label for="tel3" class="col-md-4 control-label">Telefoonnummer</label>
                            <input id="tel3" type="text" class="md-input" name="tel3">
                        </div>

                        <div class="uk-form-row">
                            <label for="email" class="col-md-4 control-label">Email</label>
                            <input id="email" type="email" class="md-input" name="email" required>
                        </div>

                        <div class="uk-form-row">
                            <label for="bankrekening" class="col-md-4 control-label">Bankrekeningnummer</label>
                            <input id="bankrekening" type="text" class="md-input" name="bankrekening">
                        </div>
                        <div class="uk-form-row">
                                <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">
                                    <i class="fa fa-btn fa-sign-in"></i> Verzend
                                </button>                                
                        </div>
                       <!--  <div class="md-card-content large-padding uk-position-relative" id="login_help" style="display: none">
                            <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                            <h2 class="heading_b uk-text-success">Can't log in?</h2>
                            <p>Here’s the info to get you back in to your account as quickly as possible.</p>
                            <p>First, try the easiest thing: if you remember your password but it isn’t working, make sure that Caps Lock is turned off, and that your username is spelled correctly, and then try again.</p>
                            <p>If your password still isn’t working, it’s time to <a href="#" id="password_reset_show">reset your password</a>.</p>
                        </div> -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- common functions -->
    <script src="{{ URL::asset('assets/assets/js/common.min.js')}}"></script>
    <!-- uikit functions -->
    <script src="{{ URL::asset('assets/assets/js/uikit_custom.min.js')}}"></script>
    <!-- altair core functions -->
    <script src="{{ URL::asset('assets/assets/js/altair_admin_common.min.js')}}"></script>

    <!-- altair login page functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/login.min.js')}}"></script>

    <script>
        // check for theme
        if (typeof(Storage) !== "undefined") {
            var root = document.getElementsByTagName( 'html' )[0],
                theme = localStorage.getItem("altair_theme");
            if(theme == 'app_theme_dark' || root.classList.contains('app_theme_dark')) {
                root.className += ' app_theme_dark';
            }
        }
    </script>

    <!--  notifications functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/components_notifications.min.js')}}"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        // ga('create', 'UA-65191727-1', 'auto');
        // ga('send', 'pageview');
    </script>
</body>
</html>
