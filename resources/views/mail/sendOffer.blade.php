<p>
	Geachte heer/mevrouw {{$relatie->achternaam}},
</p>
<p>
Naar aanleiding van ons contact stuur ik u uw offerte toe. <br>
<br>
Via onderstaande link heeft u toegang tot uw offerte.<br>
<a href="https://cwb.mbivdevelopment.nl/share/{{$relatie->id}}_{{$relatie->huisnummer}}_v_{{$offerte->id}}/offerte">Uw offerte</a><br> 
 <br>
Heeft u verder nog vragen over uw offerte, dan horen wij dat graag! <br>
Ik hoop u hiermee voldoende te hebben geïnformeerd.
</p> 
<p> 
Met vriendelijke groet,<br>
 <br>
 <br> 
{{$bedrijf->naam}}<br>
{{$bedrijf->adres}} {{$bedrijf->huisnummer}}<br>
{{$bedrijf->postcode}} te {{$bedrijf->plaats}}<br>
{{$bedrijf->email}}<br>
</p>
