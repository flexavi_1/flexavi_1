<p>
Fam. {{$relatie->achternaam}}<br>
{{$relatie->straat." ".$relatie->huisnummer.$relatie->hn_prefix}}<br>
{{$relatie->postcode>" te ".$relatie->woonplaats}}<br>
</p>
<p>
Amsterdam, {{$date}}
</p>
<p>
Betreft: sommatie en aankondiging incassokosten

Geachte heer/mevrouw *naam,

Ondanks onze verzoeken tot betaling van onderstaande facturen, hebben wij het door u
verschuldigde bedrag helaas nog niet mogen ontvangen.
Het betreft factuur *briefnr/factuurnr(over het algemeen pc+hn).
Wij verzoeken u het verschuldigde bedrag van € *totaalbedrag aanmaning,- alsnog zo spoedig
mogelijk, doch uiterlijk binnen 14 dagen, over te maken op onze bankrekening NL13 INGB 0006 5719
24. Vermeld u bij uw betaling als kenmerk *pc+hn.
Geeft u aan deze sommatie binnen 15 dagen geen gevolg, dan stellen wij u bij deze reeds in gebreke.
De vordering dragen wij ter incasso over aan Swier cs Gerechtsdeurwaarders. De hieraan verbonden
incassokosten bedragen volgens de wet € http://www.swiercs.nl/incassokosten-calculator/, exclusief
de rente en BTW.
In afwachting van uw betaling verblijven wij.
Met vriendelijke groet,

Dakgoed
Raadhuisstraat 24
1016 DE Amsterdam
06-28285458
085-3015039
info@dakgoed.com