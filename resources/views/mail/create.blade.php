@extends('layouts.master')

@section('title', 'Flexavi - Aanvulling Toevoegen')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>
    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>
@endpush
@section('content')


@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Mail invoeren</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
        <h3 class="heading_a">Aanvulling toevoegen</h3>
        {!! Form::open(array('url'=>'relatie/'.$relatie->id.'/mail/create', 'method' => 'post')) !!}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <p></p>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-3">
                            <span class="uk-form-help-block">Titel</span>
                            <input type="text" required class="md-input label-fixed" step="1" name="titel" placeholder="Geef hier de titel van de mail" required/>
                        </div>
                       <div class="uk-width-large-1-3">
                            <span class="uk-form-help-block">Ontvangen op</span>
                            <input id="kUI_datetimepicker_basic" name="ontvangen_op" required/>
                        </div>
                        <div class="uk-width-large-1-3">
                            <span class="uk-form-help-block">E-mail</span>
                            <input type="text" required class="md-input label-fixed" step="1" name="email" placeholder="Email van verstuurder" value="{{$relatie->email}}" required/>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large">
                            <span class="uk-form-help-block">Omschrijving</span><br>
                            <textarea cols="30" rows="4" class="md-input" name="omschrijving" id='editor' required></textarea>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large">
                            <input type="checkbox" name="door_ons" value="1" id="checkbox_demo_3" data-md-icheck />
                            <label for="checkbox_demo_3"  class="inline-label">Mail is door ons verstuurd.</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div align="right">
                            <div class="md-btn-group">
                                <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg mail toe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endif

@endsection