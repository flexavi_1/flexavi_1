<p>
Fam. {{$relatie->achternaam}}<br>
{{$relatie->straat." ".$relatie->huisnummer.$relatie->hn_prefix}}<br>
{{$relatie->postcode>" te ".$relatie->woonplaats}}<br>
</p>
<p>
Amsterdam, {{$date}}
</p>
<p>
Betreft: Aanmaning
</p>
<p>	
Geachte heer/mevrouw {{$relatie->achternaam}},<br>
<br>
Wij hebben naar aanleiding van onze herinneringen nog geen betaling uwerzijds mogen ontvangen.<br>
Dit terwijl de betalingstermijn ruim is overschreden.<br>
<br>
<table>
	<tbody>
	<tr>
		<td>Factuur d.d. </td>
		<td>{{$factuurdatum}}</td>
		<td>€ {{number_format($factuurbedrag,2)}}</td>
	</tr>
	<tr>
		<td>Administratiekosten</td>
		<td>{{$date}}</td>
		<td>€ {{number_format($administratiekosten,2)}}</td>
	</tr>
	<hr>
	<tr>
		<td>Totaalbedrag d.d.</td>
		<td>{{$date}}</td>
		<td>€ {{number_format($factuurbedrag + $administratiekosten,2)}}</td>
	</tr>
	</tbody>
</table>
<br/>
Wij verzoeken u dringend het bovengenoemde bedrag zo spoedig mogelijk, maar uiterlijk binnen 5
dagen, over te maken op onze bankrekening NL13 INGB 0006 5719 24 onder vermelding van *pc+hn.
Wanneer de factuur niet binnen de gestelde termijn van vijf dagen voldaan is, voelen wij ons
genoodzaakt verdere stappen tegen u te ondernemen. Alle hieruit voortkomende kosten zullen voor
uw rekening komen.
In afwachting van uw betaling verblijven wij.
Met vriendelijke groet,

Dakgoed
Raadhuisstraat 22
1016 DE Amsterdam
06-28285458
085-3015039
info@dakgoed.com