<p>Geachte heer/mevrouw {{$a->klant_id->voornaam." ".$a->klant_id->achternaam}} ,</p>
 
<p>Naar aanleiding van ons telefoongesprek stuur ik u deze mail. </p>
 
<p>
Datum: {{date('d-m-Y', strtotime($a->startdatum))}} <br>
Tijd: {{date('H:i', strtotime($a->startdatum))."-".date('H:i', strtotime($a->einddatum))}} <br>
@if(count($a->ahas) != 0 && $a->afspraaktype_id->id == 1)
Werkzaamheden: <br>
	<ul>
		<?php $korting = 0.00; $totaalbedrag = 0.00; ?>
		@foreach($a->ahas as $aha)
		<ul>{{$aha->aantal." x ".$aha->activiteit->omschrijving." à ".$aha->prijs}} => {{number_format($aha->prijs * $aha->aantal,2)}} 
			@if($aha->kortingsbedrag > 0) <i>Korting: € {{number_format($aha->kortingsbedrag,2)}}</i> @endif</ul>
		<?php $korting += $aha->kortingsbedrag; $totaalbedrag += $aha->prijs * $aha->aantal; ?>
		@endforeach
	</ul>
	@if($a->klusprijs == 1)
        Totaalprijs: € {{number_format($a->totaalbedrag, 2)}} <i>(€ {{number_format($a->getRealAppointmentPrice(),2)}} - € {{number_format($a->getRealAppointmentPrice() - $a->totaalbedrag ,2)}})</i>
	@else
		Totaalprijs: € {{number_format($a->totaalbedrag,2)}}  @if($korting > 0) 
															<i>(€ {{number_format($totaalbedrag,2)}} - € {{number_format($korting,2)}})</i>
														  @endif
	@endif
@elseif($a->afspraaktype_id->id == 4)
<br>
	Garantie ingepland betreft werkzaamheden d.d. {{date('d-m-Y', strtotime($a->getPreviousApp()->startdatum))}}.
@endif
</p>
@if($a->omschrijving != "" && $a->afspraaktype_id->id != 4)
<p>
	Opmerking: <br> <em>{{$a->omschrijving}}</em> 
</p>
@endif
<p> 
Hopende u hiermee voldoende te hebben geïnformeerd!
<p>
<p> 
Met vriendelijke groet,
</p> 
<p> 
{{$bedrijf->naam}} <br>
{{$bedrijf->adres." ".$bedrijf->huisnummer.", ".$bedrijf->postcode." te ".$bedrijf->plaats}}<br>
@if($bedrijf->tel1 != ""){{$bedrijf->tel1}} <br>@endif
@if($bedrijf->tel2 != ""){{$bedrijf->tel2}} <br>@endif
@if($bedrijf->tel3 != ""){{$bedrijf->tel3}} <br>@endif
{{$bedrijf->email}}<br>
{{$bedrijf->website}}
</p>
<p><i>
@if($a->afspraaktype_id->id < 3)
	Wilt u annuleren? Gaarne minimaal twee werkdagen van te voren zodat wij dit kunnen opvangen in onze planning. 
	Anders zijn wij genoodzaakt € 
		@if($a->totaalbedrag > 100) 
			<?php echo number_format(0.35 * $a->totaalbedrag,2); ?> materiaalkosten
		@else 
			15,- voorrijkosten 
		@endif
		in rekening te brengen.
	Aanwezigheid van de hoofdbewoner is verplicht! </i>
@endif
<p>