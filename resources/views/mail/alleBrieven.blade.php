<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="nl"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="nl"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="{{ URL::asset('assets/image/png')}}" href="{{ URL::asset('assets/assets/icons/flexavi_favicon.jpeg')}}" sizes="16x16">
    <link rel="icon" type="{{ URL::asset('assets/image/png')}}" href="{{ URL::asset('assets/assets/icons/flexavi_favicon.jpeg')}}" sizes="32x32">
    <!-- material icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<style>
		@media print {
			body * {
			    visibility: hidden;
			}
			#section-to-print * {
			    visibility: visible;
			}
			#section-to-print {
			    position: relative;
			    left: 0;
			    top: 0;
			}
			/*.page-break{
			  page-break-before: always;
			}*/
		}

		/*.page-break{
		  page-break-after: always;
		  page-break-inside: always;
		}*/
	</style>

</head>
<body>
<button onClick="window.print()"><i class="material-icons">print</i></button>
<div id="section-to-print">
	@foreach($vijftien as $v)
	{!! $v->brief !!}
	<div style="page-break-after: always;"></div>
	<div>&nbsp;</div>
	@endforeach
</div>	
</body>
</html>