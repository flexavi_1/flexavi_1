<p>
Fam. {{$relatie->achternaam}}<br>
{{$relatie->straat." ".$relatie->huisnummer.$relatie->hn_prefix}}<br>
{{$relatie->postcode." te ".$relatie->woonplaats}}<br>
</p>
<p>
{{$bedrijf->plaats}}, {{$date}}
</p>
<p>
Betreft: Niet nagekomen overeenkomst
</p>
<p>
Geachte heer/mevrouw {{$relatie->achternaam}},
<br><br>
Uit onze gegevens is gebleken dat u de afspraak van {{$afspraakdatum}} niet hebt 
kunnen nakomen. De voorwaarde van onze overeenkomst houdt in dat u minimaal 2 werkdagen <br>
voor datum van afspraak kosteloos kunt annuleren, anders brengen wij daar € {{number_format($factuurbedrag, 2)}} administratiekosten voor in rekening. 
<br>Deze brief is ook per post verstuurd. Voor het inzien van uw getekende overeenkomst vernemen wij dit graag van u. <br>
<br>
Hierdoor ontvangen wij binnen 10 werkdagen na dagtekening uw betaling van € 15,- op
onderstaande gegevens:
<br><br>
<pre>
Rekening nummer: <b><em>{{$bedrijf->bankrekening}}</em></b> <br>
T.n.v.  <b><em>{{$bedrijf->naam}}</em></b>    O.v.v. <b><em>{{$relatie->postcode.$relatie->huisnummer}}</em></b>
</pre>
<br><br>
Indien dit bedrag niet overgemaakt is binnen het gestelde termijn, zijn we genoodzaakt om € {{number_format($administratiekosten, 2)}}
administratie kosten extra in rekening te brengen.
<br><br>
Ik hoop u hiermee voldoende te hebben geïnformeerd.
</p>
<p>
Met vriendelijke groet,
<br>
<br>
{{$bedrijf->naam}}
<br>
{{$bedrijf->adres." ".$bedrijf->huisnummer}}
<br>
{{$bedrijf->postcode." te ".$bedrijf->plaats}}
<br>
{{$bedrijf->tel1}}
<br>
@if($bedrijf->tel2 != null && $bedrijf->tel2 != "")
{{$bedrijf->tel2}}
<br>
@endif
@if($bedrijf->tel3 != null && $bedrijf->tel3 != "")
{{$bedrijf->tel3}}
<br>
@endif
{{$bedrijf->email}}
</p>

<img src="{{ '#' }}"> LOGO HIER