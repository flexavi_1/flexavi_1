<p>
Fam. {{$relatie->achternaam}}<br>
{{$relatie->straat." ".$relatie->huisnummer.$relatie->hn_prefix}}<br>
{{$relatie->postcode>" te ".$relatie->woonplaats}}<br>
</p>
<p>
Amsterdam, {{$date}}
</p>
<p>
Betreft: eerste herinnering
</p>
<p>
Geachte heer/mevrouw {{$relatie->achternaam}},<br>
<br>
Volgens onze administratie hebben wij nog geen betaling van factuurnummer <b>{{$relatie->postcode.$relatie->huisnummer}}</b> mogen ontvangen. <br>
Wij verzoeken u het verschuldigde bedrag van € {{number_format($factuurbedrag,2)}} binnen 10 werkdagen over te maken op onderstaande gegevens. <br>
Anders zijn we genoodzaakt om alsnog € 25,- administratiekosten in rekening te brengen.
<pre>
Rekening nummer: <b><em>{{$bedrijf->bankrekening}}</em></b> <br>
T.n.v.  <b><em>{{$bedrijf->naam}}</em></b>    O.v.v. <b><em>{{$relatie->postcode.$relatie->huisnummer}}</em></b>
</pre>
<br><br>
Heeft u vragen over de factuur of bent u van mening dat deze ten onrechte is verstuurd, neem dan
contact op met ondergetekende.
Indien het bedrag inmiddels is overgemaakt kunt u deze brief uiteraard als niet verzonden
beschouwen.
<br><br>
Ik hoop u hiermee voldoende te hebben geïnformeerd.
</p>
<p>
Met vriendelijke groet,
<br><br>
Dakgoed
Raadhuisstraat 22
1016 DE Amsterdam
06-28285458
085-3015039
info@dakgoed.com
</p>