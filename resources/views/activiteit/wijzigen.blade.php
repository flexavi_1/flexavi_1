@extends('layouts.master')

@section('title', 'Flexavi - Werkactiviteit Toevoegen')

@section('content')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>
@endpush
@if(Auth::user()->rol != 99)

<div class="md-card">
    <div class="md-card-content">
        <h3 class="heading_a">Werkactiviteit Toevoegen</h3>
        {!! Form::open(array('url'=>'activiteit/'.$activiteit->id.'/wijzigen', 'method' => 'post')) !!}
       	<div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
                        	<div class="uk-input-group">
	                        	<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-wrench"></i></span>
	                            <label>Omschrijving</label>
	                            <input type="text" value="{{$activiteit->omschrijving}}" class="md-input label-fixed" name="omschrijving" required/>
                        	</div>
                        </div>
                        <div class="uk-width-medium-1-3">
                        	<div class="uk-input-group">
	                        	<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-eur"></i></span>
	                            <label>Prijs</label>
	                            <input type="number" step="0.01" min="0.01" class="md-input label-fixed" name="prijs" value="{{$activiteit->prijs}}" required/>
                        	</div>
                        </div>
                        <div class="uk-width-medium-1-3">
                        	<div class="uk-input-group">
	                        	<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-table"></i></span>
	                            <label>Eenheid</label>
	                            <input type="text" value="{{$activiteit->eenheid}}" class="md-input label-fixed" name="eenheid" required/>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
		            <div align="right">
		            	<div class="md-btn-group">
			            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
			                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Wijzig Werkactiviteit</button>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
    </div>
</div>
	{!! Form::close() !!}

@endif

@endsection