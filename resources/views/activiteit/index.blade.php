@extends('layouts.master')

@section('title', 'Flexavi - Werkactiviteit Overzicht')


@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush

@section('content')
@if(Auth::user()->rol != 99)

<h4 class="heading_a uk-margin-bottom">@if(Auth::user()->rol == 8) Prijslijst @else Werkactiviteiten @endif</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="width:10%; overflow:hidden;">Omschrijving</th>
                        <th style="width:15%; overflow:hidden;">Beschrijving</th>
                        {{-- <th style="width:40%; overflow:hidden;">Hoe</th> --}}
                        <th style="width:10%; overflow:hidden;">Prijs</th>
                        <th style="width:10%; overflow:hidden;">Eenheid</th>
                        @if(Auth::user()->rol < 7)
                        <th style="width:15%; overflow:hidden;"></th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($activiteiten as $activiteit)
                    <tr>
                        <td style="width:10%; overflow:hidden;">{{$activiteit->omschrijving}}</td>
                        <td style="width:15%; overflow:hidden;">{{$activiteit->beschrijving}}</td>
                        {{-- <td style="width:40%; overflow:hidden;">{!!$activiteit->hoe!!}</td> --}}
                    	<td style="width:10%; overflow:hidden;">€ {{number_format($activiteit->prijs,2)}}</td>
                    	<td style="width:10%; overflow:hidden;">{{$activiteit->eenheid}}</td>
                        @if(Auth::user()->rol < 7)
                    	<td style="width:15%; overflow:hidden;">
                    		<a href="/activiteit/{{$activiteit->id}}/wijzigen" class="md-btn md-btn-warning md-btn-small md-btn-wave-light"><i class="material-icons">edit</i></a>
                            <a href="/activiteit/{{$activiteit->id}}/verwijderen" class="md-btn md-btn-danger md-btn-small md-btn-wave-light"><i class="material-icons">delete</i></a>
                            @if($activiteit->actief == 1)
                            <a href="/activiteit/{{$activiteit->id}}/materiaal" class="md-btn md-btn-success md-btn-small md-btn-wave-light">Voeg materiaal toe</a>
                            <a href="/activiteit/{{$activiteit->id}}/geenmateriaal" class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Activiteit heeft geen materiaal</a>
                            @endif
                            @if($activiteit->hasMats() && $activiteit->actief == 2)
                            <a href="#" class="md-btn md-btn-primary md-btn-small md-btn-wave-light">Wijzig materiaal</a>
                            <a href="#" class="md-btn md-btn-success md-btn-small md-btn-wave-light">Bekijk materiaal</a>
                            @endif
                    	</td>
                        @endif
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <a href="/activiteit/nieuw">Nieuwe Werkactiviteit aanmaken</a>
@endif



@endsection