@extends('layouts.master')

@section('title', 'Flexavi - Werkactiviteit Toevoegen')

@section('content')



@push('scripts')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <script src="{{ URL::asset('assets/bower_components/handlebars/handlebars.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/handlebars_helpers.min.js')}}"></script>

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

@endpush
@if(Auth::user()->rol != 99)

<div class="md-card">
    <div class="md-card-content">
        <h3 class="heading_a">Werkactiviteit Toevoegen</h3>
        @if(isset($cont))
            @if($cont == 'edit')
                {!! Form::open(array('url'=>'afspraak/'.$afspraak->id.'/bewerken/activiteit/nieuw', 'method' => 'post')) !!}
            @elseif($cont == 'offer')
                {!! Form::open(array('url'=>'/offerte/'.$offerte->id.'/activiteit/nieuw', 'method' => 'post')) !!}
            @else
                {!! Form::open(array('url'=>'afspraak/'.$afspraak->id.'/activiteit/nieuw', 'method' => 'post')) !!}
            @endif
        @elseif(isset($relatie))
            {!! Form::open(array('url'=>'/returntoprocess/'.$afspraak->id, 'method'=>'post')) !!}
        @else
            {!! Form::open(array('url'=>'activiteit/nieuw', 'method' => 'post')) !!}
        @endif
        <div data-dynamic-fields="field_template_b"></div>
            <script id="field_template_b" type="text/x-handlebars-template">
            
            <div class="uk-grid uk-grid-medium form_section form_section_separator" id="d_form_row" data-uk-grid-match>
                <div class="uk-width-9-10">
                    <div class="uk-grid">
                        <div class="uk-width-1-4">
                            <div class="parsley-row">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-wrench"></i></span>
                                <label>Omschrijving</label>
                                {!! Form::text('omschrijving[]', null, array('class' => 'md-input label-fixed', 'required', 'placeholder' => 'Nokvorst reparatie All-in')) !!}
                            </div>
                        </div>
                        <div class="uk-width-1-4">
                            <div class="parsley-row">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-eur"></i></span>
                                <label>Prijs</label>
                                <input type="number" step="0.01" min="0.01" class="md-input label-fixed" name="prijs[]" placeholder="69,99" required/>
                            </div>
                        </div>
                        <div class="uk-width-1-4">
                            <div class="parsley-row">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-table"></i></span>
                                <label>Eenheid</label>
                                <input type="text" placeholder="per meter" class="md-input label-fixed" name="eenheid[]" required/>
                            </div>
                        </div>
                        <div class="uk-width-1-4">
                            <div class="parsley-row">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-eur"></i></span>
                                <label>BTW-tarief</label>
                                <input type="number" step="1" class="md-input label-fixed" name="btw[]" value="21" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-10 uk-text-center">
                    <div class="uk-vertical-align uk-height-1-1">
                        <div class="uk-vertical-align-middle">
                            <a href="#" class="btnSectionClone" data-section-clone="#d_form_section"><i class="material-icons md-36">&#xE146;</i></a>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
             </script>
        {{-- <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
                        	<div class="uk-input-group">
	                        	<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-wrench"></i></span>
	                            <label>Omschrijving</label>
	                            {!! //Form::text('omschrijving', null, array('class' => 'md-input label-fixed', 'required', 'placeholder' => 'Nokvorst reparatie All-in')) !!}
                        	</div>
                        </div>
                        <div class="uk-width-medium-1-3">
                        	<div class="uk-input-group">
	                        	<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-eur"></i></span>
	                            <label>Prijs</label>
	                            <input type="number" step="0.01" class="md-input label-fixed" name="prijs" placeholder="69,99" required/>
                        	</div>
                        </div>
                        <div class="uk-width-medium-1-3">
                        	<div class="uk-input-group">
	                        	<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-table"></i></span>
	                            <label>Eenheid</label>
	                            <input type="text" placeholder="per meter" class="md-input label-fixed" name="eenheid" required/>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
		            <div align="right">
		            	<div class="md-btn-group">
			            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
			                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg Werkactiviteit Toe</button>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
    </div>
</div>
	{!! Form::close() !!}

@endif

@endsection