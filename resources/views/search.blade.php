@extends('layouts.new')

@section('title', 'Flexavi - Zoeken')

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.bootstrap5.min.css">
@endpush

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <!-- Datatables Cdn -->
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.6/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>

    <!-- Internal Datatables JS -->
    <script src="{{ URL::asset('new') }}/assets/js/datatables.js"></script>
@endpush

@section('page_title', 'Zoekresultaten')

@section('content')
    @if(isset($results))
    <div class="row">
        <div class="col-xl-12">
            <div class="card custom-card">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        Zoekresultaten
                    </div>
                </div>
                <div class="card-body">
                    <div class="accordion accordion-solid-primary" id="accordionPrimarySolidExample">
                        @foreach($results as $topic)
                        <div class="accordion-item my-2">
                            <h2 class="accordion-header" id="headingPrimarySolidOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapsePrimarySolidOne" aria-expanded="true"
                                        aria-controls="collapsePrimarySolidOne">
                                    {{$topic['subject']}}
                                </button>
                            </h2>
                            <div id="collapsePrimarySolidOne" class="accordion-collapse collapse @if($loop->first) show @endif"
                                 aria-labelledby="headingPrimarySolidOne"
                                 data-bs-parent="#accordionPrimarySolidExample">
                                <div class="accordion-body">
                                    @if($topic['subject'] == 'Klant')
                                    <table id="search-customer" class="table table-bordered text-nowrap w-100">
                                        <thead>
                                        <tr>
                                            <th style="width:15%;">Naam</th>
                                            <th style="width:20%;">Adres</th>
                                            <th style="width:20%;">Contactgegevens</th>
                                            <th style="width:15%;">Verwerkt door</th>
                                            <th style="width:15%;">Geschreven door</th>
                                            <th style="width:15%;">Geschreven op</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($topic['data'] as $klant)
                                            <tr>
                                                <td>
                                                    <a href="/relatie/{{$klant->id}}">{{$klant->getFullName()}}</a>
                                                </td>
                                                <td>
                                                    {!! $klant->getFullAddress() !!}
                                                </td>
                                                <td>
                                                    {!! $klant->getFullContact() !!}
                                                </td>
                                                <td>
                                                    {{$klant->verwerktDoor->name}}
                                                </td>
                                                <td>
                                                    {{$klant->geschrevenDoor->getFullname()}}
                                                </td>
                                                <td>
                                                    {{date('d-m-Y', strtotime($klant->geschreven_op))}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                    @if($topic['subject'] == 'Afspraak')
                                    <table id="search-appointments" class="table table-bordered w-100">
                                        <thead>
                                        <tr>
                                            <th style="width:10%;">Afspraak Datum</th>
                                            <th style="width:10%;">Klant</th>
                                            <th style="width:20%;">Adres</th>
                                            <th style="width:15%;">Contactgegevens</th>
                                            <th style="width:35%;">Omschrijving</th>
                                            <th style="width:10%;">Verwerkt door</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($topic['data'] as $afspraak)
                                            <tr>
                                                <td>
                                                    <a href="/planning/verkoop/{{date('d-m-Y', strtotime($afspraak->startdatum))}}">{{date('d-m-Y', strtotime($afspraak->startdatum))}}</a>
                                                </td>
                                                <td>
                                                    {{$afspraak->klant->getFullName()}}
                                                </td>
                                                <td>
                                                    {!! $afspraak->klant->getFullAddress() !!}
                                                </td>
                                                <td>
                                                    {!! $afspraak->klant->getFullContact() !!}
                                                </td>
                                                <td>
                                                    {!! $afspraak->omschrijving !!}
                                                </td>
                                                <td>
                                                    @if($afspraak->verwerktDoor)
                                                        {{$afspraak->verwerktDoor->name}}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer d-none border-top-0">
                    <!-- Prism Code -->
                    <pre class="language-html"><code class="language-html">&lt;div class="accordion accordion-primary" id="accordionPrimaryExample"&gt;
    &lt;div class="accordion-item"&gt;
        &lt;h2 class="accordion-header" id="headingPrimaryOne"&gt;
            &lt;button class="accordion-button" type="button" data-bs-toggle="collapse"
                data-bs-target="#collapsePrimaryOne" aria-expanded="true"
                aria-controls="collapsePrimaryOne"&gt;
                Accordion Item #1
            &lt;/button&gt;
        &lt;/h2&gt;
        &lt;div id="collapsePrimaryOne" class="accordion-collapse collapse show"
            aria-labelledby="headingPrimaryOne"
            data-bs-parent="#accordionPrimaryExample"&gt;
            &lt;div class="accordion-body"&gt;
                &lt;strong&gt;This is the first item's accordion body.&lt;/strong&gt; It is shown by
                default, until the collapse plugin adds the appropriate classes that we
                use to style each element. These classes control the overall appearance,
                as well as the showing and hiding via CSS transitions. You can modify
                any of this with custom CSS or overriding our default variables. It's
                also worth noting that just about any HTML can go within the
                &lt;code&gt;.accordion-body&lt;/code&gt;, though the transition does limit overflow.
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class="accordion-item"&gt;
        &lt;h2 class="accordion-header" id="headingPrimaryTwo"&gt;
            &lt;button class="accordion-button collapsed" type="button"
                data-bs-toggle="collapse" data-bs-target="#collapsePrimaryTwo"
                aria-expanded="false" aria-controls="collapsePrimaryTwo"&gt;
                Accordion Item #2
            &lt;/button&gt;
        &lt;/h2&gt;
        &lt;div id="collapsePrimaryTwo" class="accordion-collapse collapse"
            aria-labelledby="headingPrimaryTwo"
            data-bs-parent="#accordionPrimaryExample"&gt;
            &lt;div class="accordion-body"&gt;
                &lt;strong&gt;This is the first item's accordion body.&lt;/strong&gt; It is shown by
                default, until the collapse plugin adds the appropriate classes that we
                use to style each element. These classes control the overall appearance,
                as well as the showing and hiding via CSS transitions. You can modify
                any of this with custom CSS or overriding our default variables. It's
                also worth noting that just about any HTML can go within the
                &lt;code&gt;.accordion-body&lt;/code&gt;, though the transition does limit overflow.
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class="accordion-item"&gt;
        &lt;h2 class="accordion-header" id="headingPrimaryThree"&gt;
            &lt;button class="accordion-button collapsed" type="button"
                data-bs-toggle="collapse" data-bs-target="#collapsePrimaryThree"
                aria-expanded="false" aria-controls="collapsePrimaryThree"&gt;
                Accordion Item #3
            &lt;/button&gt;
        &lt;/h2&gt;
        &lt;div id="collapsePrimaryThree" class="accordion-collapse collapse"
            aria-labelledby="headingPrimaryThree"
            data-bs-parent="#accordionPrimaryExample"&gt;
            &lt;div class="accordion-body"&gt;
                &lt;strong&gt;This is the first item's accordion body.&lt;/strong&gt; It is shown by
                default, until the collapse plugin adds the appropriate classes that we
                use to style each element. These classes control the overall appearance,
                as well as the showing and hiding via CSS transitions. You can modify
                any of this with custom CSS or overriding our default variables. It's
                also worth noting that just about any HTML can go within the
                &lt;code&gt;.accordion-body&lt;/code&gt;, though the transition does limit overflow.
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;</code></pre>
                    <!-- Prism Code -->
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
