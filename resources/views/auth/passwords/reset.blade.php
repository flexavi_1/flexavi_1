<!DOCTYPE html>
<html lang="nl" dir="ltr" data-nav-layout="vertical" data-vertical-style="overlay" data-theme-mode="light" data-header-styles="light" data-menu-styles="light" data-toggled="close">

<head>

    <!-- Meta Data -->
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title> Flexavi </title>
    <meta name=”robots” content=”noindex”>
    <!-- Favicon -->
    <link rel="icon" href="{{ URL::asset('new/assets/images/logo/flexavi_favicon.jpeg')}}" type="image/x-icon">

    <!-- Main Theme Js -->
    <script src="{{ URL::asset('new/assets/js/authentication-main.js')}}"></script>

    <!-- Bootstrap Css -->
    <link id="style" href="{{ URL::asset('new/assets/libs/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" >

    <!-- Style Css -->
    <link href="{{ URL::asset('new/assets/css/styles.min.css')}}" rel="stylesheet" >

    <!-- Icons Css -->
    <link href="{{ URL::asset('new/assets/css/icons.min.css')}}" rel="stylesheet" >


    <link rel="stylesheet" href="{{ URL::asset('new/assets/libs/swiper/swiper-bundle.min.css')}}">

</head>

<body class="bg-white">


<div class="row authentication mx-0">

    <div class="col-xxl-7 col-xl-7 col-lg-12">
        <div class="row justify-content-center align-items-center h-100">
            <div class="col-xxl-6 col-xl-7 col-lg-7 col-md-7 col-sm-8 col-12">
                <div class="p-5">
                    <div class="mb-3">
                        <a href="index.html">
                            <img src="{{ URL::asset('new/assets/images/logo/Logo-Flexavi-Horizontaal.png')}}" alt="" class="authentication-brand desktop-logo">
                            <img src="{{ URL::asset('new/assets/images/logo/Logo-Flexavi-Horizontaal.png')}}" alt="" class="authentication-brand desktop-dark">
                        </a>
                    </div>
                    <p class="h5 fw-semibold mb-2">Wachtwoord Resetten</p>
                    <p class="mb-3 text-muted op-7 fw-normal">Vraag hier een nieuw wachtwoord aan!</p>

                    <div class="text-center my-5 authentication-barrier">
                    </div>
                    @if (session('status'))
                        <div class="mb-3">
                            <span class="alert alert-success"> Als uw email adres voorkomt in onze database, dan zult u over enkele ogenblikken een email ontvangen. </span>
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}
                        <div class="row gy-3">
                            <div class="col-xl-12 mt-0 mb-3">
                                <label for="reset-password" class="form-label text-default">Email Adres</label>
                                <div class="input-group">
                                    <input type="email" required class="form-control form-control-lg" name="email" id="reset-password" placeholder="Email" value="{{ $email or old('email') }}">
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <label for="reset-newpassword" class="form-label text-default">New Password</label>
                                <div class="input-group">
                                    <input type="password" required name="password" class="form-control form-control-lg" id="reset-newpassword" placeholder="new password">
                                    <button class="btn btn-light" onclick="createpassword('reset-newpassword',this)" type="button" id="button-addon21"><i class="ri-eye-off-line align-middle"></i></button>
                                    @if ($errors->has('password'))
                                        <span style="color:red;">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xl-12 mb-3">
                                <label for="reset-confirmpassword" class="form-label text-default">Confirm Password</label>
                                <div class="input-group">
                                    <input type="password" required name="password_confirmation" class="form-control form-control-lg" id="reset-confirmpassword" placeholder="confirm password">
                                    <button class="btn btn-light" onclick="createpassword('reset-confirmpassword',this)" type="button" id="button-addon22"><i class="ri-eye-off-line align-middle"></i></button>
                                    @if ($errors->has('password_confirmation'))
                                        <span style="color:red;">
                                            {{ $errors->first('password_confirmation') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xl-12 d-grid mt-2">
                                <button type="submit" href="sign-in-cover.html" class="btn btn-lg btn-primary">Create</button>
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="fs-12 text-muted mt-4">Weet u uw wachtwoord weer? <a href="/login" class="text-primary">Inloggen</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xxl-5 col-xl-5 col-lg-5 d-xl-block d-none px-0">
        <div class="authentication-cover">
            <div class="aunthentication-cover-content rounded">
                <div class="swiper keyboard-control">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="text-fixed-white text-center p-5 d-flex align-items-center justify-content-center">
                                <div>
                                    <div class="mb-5">
                                        <img src="{{ URL::asset('new/assets/images/logo/Logo-Flexavi-Verticaal.png')}}" class="authentication-image" alt="">
                                    </div>
                                    <h6 class="fw-semibold text-fixed-white">Flexavi</h6>
                                    <p class="fw-normal fs-14 op-7">Klant relatie software voor Dakdekkers!</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="text-fixed-white text-center p-5 d-flex align-items-center justify-content-center">
                                <div>
                                    <div class="mb-5">
                                        <img src="{{ URL::asset('new/assets/images/authentication/2.png')}}" class="authentication-image" alt="">
                                    </div>
                                    <h6 class="fw-semibold text-fixed-white">Flexavi</h6>
                                    <p class="fw-normal fs-14 op-7">Beheer afspraken, facturen, offertes, planning en meer.. </p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="text-fixed-white text-center p-5 d-flex align-items-center justify-content-center">
                                <div>
                                    <div class="mb-5">
                                        <img src="{{ URL::asset('new/assets/images/authentication/3.png')}}" class="authentication-image" alt="">
                                    </div>
                                    <h6 class="fw-semibold text-fixed-white">Flexavi</h6>
                                    <p class="fw-normal fs-14 op-7">Nieuwe release!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Bootstrap JS -->
<script src="{{ URL::asset('new/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Swiper JS -->
<script src="{{ URL::asset('new/assets/libs/swiper/swiper-bundle.min.js') }}"></script>

<!-- Internal Sing-Up JS -->
<script src="{{ URL::asset('new/assets/js/authentication.js') }}"></script>

<!-- Show Password JS -->
<script src="{{ URL::asset('new/assets/js/show-password.js') }}"></script>

</body>

</html>



@php
/*
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-refresh"></i> Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
*/
@endphp
