<!DOCTYPE html>
<html lang="nl" dir="ltr" data-nav-layout="vertical" data-vertical-style="overlay" data-theme-mode="light" data-header-styles="light" data-menu-styles="light" data-toggled="close">

<head>

    <!-- Meta Data -->
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title> Flexavi </title>
    <meta name=”robots” content=”noindex”>
    <!-- Favicon -->
    <link rel="icon" href="{{ URL::asset('new/assets/images/logo/flexavi_favicon.jpeg')}}" type="image/x-icon">

    <!-- Main Theme Js -->
    <script src="{{ URL::asset('new/assets/js/authentication-main.js')}}"></script>

    <!-- Bootstrap Css -->
    <link id="style" href="{{ URL::asset('new/assets/libs/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" >

    <!-- Style Css -->
    <link href="{{ URL::asset('new/assets/css/styles.min.css')}}" rel="stylesheet" >

    <!-- Icons Css -->
    <link href="{{ URL::asset('new/assets/css/icons.min.css')}}" rel="stylesheet" >


    <link rel="stylesheet" href="{{ URL::asset('new/assets/libs/swiper/swiper-bundle.min.css')}}">

</head>

<body class="bg-white">


<div class="row authentication mx-0">

    <div class="col-xxl-7 col-xl-7 col-lg-12">
        <div class="row justify-content-center align-items-center h-100">
            <div class="col-xxl-6 col-xl-7 col-lg-7 col-md-7 col-sm-8 col-12">
                <div class="p-5">
                    <div class="mb-3">
                        <a href="index.html">
                        <img src="{{ URL::asset('new/assets/images/logo/Logo-Flexavi-Horizontaal.png')}}" alt="" class="authentication-brand desktop-logo">
                        <img src="{{ URL::asset('new/assets/images/logo/Logo-Flexavi-Horizontaal.png')}}" alt="" class="authentication-brand desktop-dark">
                        </a>
                    </div>
                    <p class="h5 fw-semibold mb-2">Wachtwoord Resetten</p>
                    <p class="mb-3 text-muted op-7 fw-normal">Vraag hier een nieuw wachtwoord aan!</p>

                    <div class="text-center my-5 authentication-barrier">
                    </div>
                    @if (session('status'))
                        <div class="mb-3">
                            <span class="alert alert-success"> Als uw email adres voorkomt in onze database, dan zult u over enkele ogenblikken een email ontvangen. </span>
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}
                        <div class="row gy-3">
                            <div class="col-xl-12 mt-0 mb-3">
                                <label for="reset-password" class="form-label text-default">Email Adres</label>
                                <div class="input-group">
                                    <input type="email" class="form-control form-control-lg" name="email" id="reset-password" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-xl-12 d-grid mt-2">
                                <button type="submit" href="sign-in-cover.html" class="btn btn-lg btn-primary">Create</button>
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="fs-12 text-muted mt-4">Weet u uw wachtwoord weer? <a href="/login" class="text-primary">Inloggen</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xxl-5 col-xl-5 col-lg-5 d-xl-block d-none px-0">
        <div class="authentication-cover">
            <div class="aunthentication-cover-content rounded">
                <div class="swiper keyboard-control">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="text-fixed-white text-center p-5 d-flex align-items-center justify-content-center">
                                <div>
                                    <div class="mb-5">
                                        <img src="{{ URL::asset('new/assets/images/logo/Logo-Flexavi-Verticaal.png')}}" class="authentication-image" alt="">
                                    </div>
                                    <h6 class="fw-semibold text-fixed-white">Flexavi</h6>
                                    <p class="fw-normal fs-14 op-7">Klant relatie software voor Dakdekkers!</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="text-fixed-white text-center p-5 d-flex align-items-center justify-content-center">
                                <div>
                                    <div class="mb-5">
                                        <img src="{{ URL::asset('new/assets/images/authentication/2.png')}}" class="authentication-image" alt="">
                                    </div>
                                    <h6 class="fw-semibold text-fixed-white">Flexavi</h6>
                                    <p class="fw-normal fs-14 op-7">Beheer afspraken, facturen, offertes, planning en meer.. </p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="text-fixed-white text-center p-5 d-flex align-items-center justify-content-center">
                                <div>
                                    <div class="mb-5">
                                        <img src="{{ URL::asset('new/assets/images/authentication/3.png')}}" class="authentication-image" alt="">
                                    </div>
                                    <h6 class="fw-semibold text-fixed-white">Flexavi</h6>
                                    <p class="fw-normal fs-14 op-7">Nieuwe release!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Bootstrap JS -->
<script src="{{ URL::asset('new/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Swiper JS -->
<script src="{{ URL::asset('new/assets/libs/swiper/swiper-bundle.min.js') }}"></script>

<!-- Internal Sing-Up JS -->
<script src="{{ URL::asset('new/assets/js/authentication.js') }}"></script>

<!-- Show Password JS -->
<script src="{{ URL::asset('new/assets/js/show-password.js') }}"></script>

</body>

</html>



@php

/*
<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{ URL::asset('assets/assets/img/favicon-16x16.png')}}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/assets/img/favicon-32x32.png')}}" sizes="32x32">

    <title>Altair Admin v2.8.0 - Login Page</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

    <!-- uikit -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/uikit/css/uikit.almost-flat.min.css')}}"/>

    <!-- altair admin login page -->
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/css/login_page.min.css')}}" />



</head>
<body class="login_page">

    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">

            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
                    <div class="user_avatar"></div>
                </div>
                <h2 class="heading_a uk-margin-large-bottom">Reset password</h2>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="uk-form-row{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="login_username" class="col-md-4 control-label">E-mail</label>
                            <input id="login_username" type="email" class="md-input" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                            <div class="uk-width-medium-1-4">
                                <button class="md-btn" data-message="<a href='#' class='notify-action'>{{ $errors->first('email') }}</a> Danger message" data-status="danger" data-pos="top-center">Danger</button>
                            </div>
                            @endif
                        </div>
                        <div class="uk-margin-medium-top">
                            <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">
                                <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                            </button>
                            <a href="/login">Ik heb al een account</a>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</body>

*/
@endphp
