<!DOCTYPE html>
<html lang="nl" dir="ltr" data-nav-layout="vertical" data-vertical-style="overlay" data-theme-mode="light" data-header-styles="light" data-menu-styles="light" data-toggled="close">

<head>

    <!-- Meta Data -->
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title> Flexavi </title>
    <meta name=”robots” content=”noindex”>
    <!-- Favicon -->
    <link rel="icon" href="{{ URL::asset('new/assets/images/logo/flexavi_favicon.jpeg')}}" type="image/x-icon">

    <!-- Main Theme Js -->
    <script src="{{ URL::asset('new/assets/js/authentication-main.js')}}"></script>

    <!-- Bootstrap Css -->
    <link id="style" href="{{ URL::asset('new/assets/libs/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" >

    <!-- Style Css -->
    <link href="{{ URL::asset('new/assets/css/styles.min.css')}}" rel="stylesheet" >

    <!-- Icons Css -->
    <link href="{{ URL::asset('new/assets/css/icons.min.css')}}" rel="stylesheet" >


    <link rel="stylesheet" href="{{ URL::asset('new/assets/libs/swiper/swiper-bundle.min.css')}}">

</head>

<body class="bg-white">

<div class="row authentication mx-0">

    <div class="col-xxl-7 col-xl-7 col-lg-12">
        <div class="row justify-content-center align-items-center h-100">
            <div class="col-xxl-6 col-xl-7 col-lg-7 col-md-7 col-sm-8 col-12">
                <div class="p-5">
                    <div class="mb-3">
                        <a href="#">
                            <img src="{{ URL::asset('new/assets/images/logo/Logo-Flexavi-Horizontaal.png')}}" alt="" class="authentication-brand desktop-logo">
                            <img src="{{ URL::asset('new/assets/images/logo/Logo-Flexavi-Horizontaal.png')}}" alt="" class="authentication-brand desktop-dark">
                        </a>
                    </div>
                    <p class="h5 fw-semibold mb-2">Registreer</p>
                    <p class="mb-3 text-muted op-7 fw-normal">Maak hier uw account aan!</p>

                    <div class="text-center my-5 authentication-barrier">
                    </div>
                    <form role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
                        <div class="row gy-3">
                            <div class="col-xl-12 mt-0">
                                <label for="signup-firstname" class="form-label text-default">Naam</label>
                                <input required type="text" name="name" class="form-control form-control-lg" id="signup-firstname" placeholder="Naam">
                                @if ($errors->has('name'))
                                    <span style="color:red;">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xl-12">
                                <label for="signup-email" class="form-label text-default">Email</label>
                                <input required type="email" name="email" class="form-control form-control-lg" id="signup-email" placeholder="Email">

                                @if ($errors->has('email'))
                                    <span style="color:red;">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xl-12">
                                <label for="signup-password" class="form-label text-default">Wachtwoord</label>
                                <div class="input-group">
                                    <input required type="password" class="form-control form-control-lg" id="signup-password" name="password" placeholder="password">
                                    <button class="btn btn-light" onclick="createpassword('signup-password',this)" type="button" id="button-addon2"><i class="ri-eye-off-line align-middle"></i></button>

                                    @if ($errors->has('password'))
                                    <span style="color:red;">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xl-12 mb-3">
                                <label for="signup-confirmpassword" class="form-label text-default">Bevestig wachtwoord</label>
                                <div class="input-group">
                                    <input required type="password" class="form-control form-control-lg" id="signup-confirmpassword" name="password_confirmation" placeholder="confirm password">
                                    <button class="btn btn-light" onclick="createpassword('signup-confirmpassword',this)" type="button" id="button-addon21"><i class="ri-eye-off-line align-middle"></i></button>

                                    @if ($errors->has('password_confirmation'))
                                        <span style="color:red;">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-check mt-3">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    <label class="form-check-label text-muted fw-normal" for="defaultCheck1">
                                        By creating a account you agree to our <a href="terms_conditions.html" class="text-success"><u>Terms & Conditions</u></a> and <a class="text-success"><u>Privacy Policy</u></a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xl-12 d-grid mt-2">
                                <button type="submit" class="btn btn-lg btn-primary">Create Account</button>
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="fs-12 text-muted mt-4">Heeft u al een account? <a href="/login" class="text-primary">Inloggen</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xxl-5 col-xl-5 col-lg-5 d-xl-block d-none px-0">
        <div class="authentication-cover">
            <div class="aunthentication-cover-content rounded">
                <div class="swiper keyboard-control">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="text-fixed-white text-center p-5 d-flex align-items-center justify-content-center">
                                <div>
                                    <div class="mb-5">
                                        <img src="{{ URL::asset('new/assets/images/logo/Logo-Flexavi-Verticaal.png')}}" class="authentication-image" alt="">
                                    </div>
                                    <h6 class="fw-semibold text-fixed-white">Flexavi</h6>
                                    <p class="fw-normal fs-14 op-7">Klant relatie software voor Dakdekkers!</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="text-fixed-white text-center p-5 d-flex align-items-center justify-content-center">
                                <div>
                                    <div class="mb-5">
                                        <img src="{{ URL::asset('new/assets/images/authentication/2.png')}}" class="authentication-image" alt="">
                                    </div>
                                    <h6 class="fw-semibold text-fixed-white">Flexavi</h6>
                                    <p class="fw-normal fs-14 op-7">Beheer afspraken, facturen, offertes, planning en meer.. </p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="text-fixed-white text-center p-5 d-flex align-items-center justify-content-center">
                                <div>
                                    <div class="mb-5">
                                        <img src="{{ URL::asset('new/assets/images/authentication/3.png')}}" class="authentication-image" alt="">
                                    </div>
                                    <h6 class="fw-semibold text-fixed-white">Flexavi</h6>
                                    <p class="fw-normal fs-14 op-7">Nieuwe release!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Bootstrap JS -->
<script src="{{ URL::asset('new/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Swiper JS -->
<script src="{{ URL::asset('new/assets/libs/swiper/swiper-bundle.min.js') }}"></script>

<!-- Internal Sing-Up JS -->
<script src="{{ URL::asset('new/assets/js/authentication.js') }}"></script>

<!-- Show Password JS -->
<script src="{{ URL::asset('new/assets/js/show-password.js') }}"></script>

</body>

</html>
