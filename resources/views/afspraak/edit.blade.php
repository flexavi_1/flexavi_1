@extends('layouts.master')

@section('title', 'Afspraak wijzigen')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
    <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>



    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>

@endpush
@section('content')

    @if(Auth::user()->rol != 99)

        <h2 class="heading_b uk-margin-bottom">Afspraak wijzigen
            van {{$afspraak->klant_id->voornaam." ".$afspraak->klant_id->achternaam}}</h2>

        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                {!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/wijzigen', 'method' => 'post', 'data-parsley-validate')) !!}
                <div class="uk-grid" data-uk-grid-margin>
                    <h3>Afspraak Informatie</h3>
                    <div class="uk-width-medium">
                        <div class="uk-form-row">
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-2-4">
                                    <label for="kUI_datetimepicker_basic" class="uk-form-label">Start Datum:</label>
                                    <input id="kUI_datetimepicker_basic" name="startdatum"
                                           value="{{date('d-m-Y H:i', strtotime($afspraak->startdatum))}}" required/>
                                </div>
                                <div class="uk-width-large-2-4">
                                    <label for="einddatum" class="uk-form-label">Eind Datum:</label>
                                    <input id="kUI_datetimepicker_basic_end" name="einddatum"
                                           value="{{date('d-m-Y H:i', strtotime($afspraak->einddatum))}}" required/>
                                </div>
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-2-4">
                                    <select id="select_demo_5" name="geschreven_door" data-md-selectize
                                            data-md-selectize-bottom>
                                        @foreach($werknemers as $werknemer)
                                            @if($werknemer->id == $afspraak->geschreven_door)
                                                <option value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                            @endif
                                        @endforeach
                                        @foreach($werknemers as $werknemer)
                                            @if($werknemer->id != $afspraak->geschreven_door)
                                                <option value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <span class="uk-form-help-block">Geschreven Door:</span>
                                </div>
                                <div class="uk-width-medium-2-4">
                                    <select id="select_demo_5" name="afspraaktype_id" data-md-selectize
                                            data-md-selectize-bottom>
                                        @if($afspraak->afspraaktype_id->id == 3 || $afspraak->afspraaktype_id->id == 5)
                                            <option value="{{$afspraak->afspraaktype_id->id}}">{{$afspraak->afspraaktype_id->omschrijving}}
                                            @if($afspraak->afspraaktype_id->id == 3)
                                                <option value="5">{{App\Models\AppointmentType::getDescription(5)}}
                                            @else
                                                <option value="3">{{App\Models\AppointmentType::getDescription(3)}}
                                                    @endif
                                                </option>
                                                @else
                                                    @foreach($types as $type)
                                                        @if($type->id == $afspraak->afspraaktype_id->id)
                                                            <option value="{{$type->id}}">{{$type->omschrijving}}</option>
                                                        @endif
                                                    @endforeach
                                                    @foreach($types as $type)
                                                        @if($type->id != $afspraak->afspraaktype_id->id)
                                                            <option value="{{$type->id}}">{{$type->omschrijving}}</option>
                                                        @endif
                                                    @endforeach
                                    </select>
                                    @endif
                                    <span class="uk-form-help-block">Type Afspraak</span>
                                </div>
                                <input type="hidden" name="verwerkt_door" value="{{Auth::user()->id}}"/>
                                <input type="hidden" name="klant_id" value="{{$afspraak->klant_id->id}}"/>
                            </div>
                        </div>
                        <br>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-1">
                                    <label>Omschrijving</label><br>
                                    <textarea class="md-input label-fixed" id='editor' placeholder="Schrijf hier de nuttige informatie voor de collega's, voorbeelden: Uitvoering werkzaamheden: Klant wilt een half uur van te voren gebeld worden. Garantie: Dakgoot lekt nog steeds. Uitgestelde Betaling: Mevrouw betaalt € 70,-.
                            Offerte Opstellen: Betalingsherinnering: Let op, klant betaalt binnen twee weken per bank. Terugbetalingsverzoek: Dakgoot is binnen de garantie niet meer te repareren, crediteer € 35,-"
                                              name="omschrijving">{{$afspraak->omschrijving}}</textarea>
                                </div>
                            </div>
                        </div>
                        <br>
                        <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="0" id="radio_demo_inline_1" data-md-icheck/>
                        <label for="radio_demo_inline_1" class="inline-label">Voor 12:30</label>
                    </span>
                        <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="1" id="radio_demo_inline_2" data-md-icheck/>
                        <label for="radio_demo_inline_2" class="inline-label">Na 12:30</label>
                    </span>
                        <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="2" id="radio_demo_inline_3" data-md-icheck
                               checked/>
                        <label for="radio_demo_inline_3" class="inline-label">Specifieke tijd</label>
                    </span>
                        @if($afspraak->afspraaktype_id->id != 3 && $afspraak->afspraaktype_id->id != 5)
                            <p>
                                <input type="checkbox" name="werkzaamheden" value="1" id="checkbox_demo_1 test"
                                       data-md-icheck/>
                                <label for="checkbox_demo_1 test" name="updateWork" class="inline-label">Werkzaamheden
                                    ook wijzigen?</label>
                            </p>
                            <p>
                                <input type="checkbox" name="geschiedenis" value="1" id="checkbox_demo_1"
                                       data-md-icheck/>
                                <label for="checkbox_demo_1" name="updateGeschiedenis" class="inline-label">Toegevoegde
                                    geschiedenis ook wijzigen?</label>
                            </p>
                            @if($afspraak->klant_id->email != null && $afspraak->klant_id->email != "")
                                <p>
                                    <input type="checkbox" name="bevestiging" value="1" id="checkbox_demo_3"
                                           data-md-icheck/>
                                    <label for="checkbox_demo_3" class="inline-label">Afspraakbevestiging
                                        sturen?</label>
                                </p>
                            @endif
                        @endif
                        <div class="uk-form-row">
                            <div align="right">
                                <div class="md-btn-group">
                                    <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder
                                    </button>
                                </div>
                            </div>
                        </div>
                        <p>
                            <span><em>NB: Als er betaalafspraken gemaakt zijn, komen deze te vervallen bij het wijzigen van de werkzaamheden van de afspraak. Hiervoor moet dan een nieuwe afspraak gemaakt worden.</em></span>
                        </p>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <br>

        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                <h4>Huidige afspraak</h4>
                <div class="uk-overflow-container">
                    <table class="uk-table uk-table-striped">
                        <thead>
                        <tr>
                            <th>Startdatum</th>
                            <th>Einddatum</th>
                            <th>Geschreven Door</th>
                            <th>Afspraaktype</th>
                            <th>Omschrijving</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{date('d-m-Y H:i', strtotime($afspraak->startdatum))}}</td>
                            <td>{{date('d-m-Y H:i', strtotime($afspraak->einddatum))}}</td>
                            <td>
                                @if(App\Models\Employee::findMe($afspraak->geschreven_door) != null)
                                    {{App\Models\Employee::findMe($afspraak->geschreven_door)->voornaam}} {{App\Models\Employee::findMe($afspraak->geschreven_door)->achternaam}}
                                @else
                                    Werknemer onbekend, selecteer nu de goede werknemer
                                @endif
                            </td>
                            <td>{{$afspraak->afspraaktype_id->omschrijving}}</td>
                            <td>{{$afspraak->omschrijving}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    @endif
@endsection



@push('scripts')

    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>
    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>


    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>
@endpush
