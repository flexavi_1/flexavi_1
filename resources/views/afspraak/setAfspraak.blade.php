@extends('layouts.master')

@section('title', 'Afspraak maken')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>

@endpush
@section('content')
@if(isset($danger))
<div class="uk-alert md-bg-red-400 " data-uk-alert>
    <a href="#" class="uk-alert-close uk-close"></a>
    <h3>
        {{$danger}}
    </h3>
</div>
@endif
@if(isset($succes))
    <div class="uk-alert md-bg-light-green-400" data-uk-alert>
        <a href="#" class="uk-alert-close uk-close"></a>
        <h3>
            {{$succes}}
        </h3>
    </div>
@endif
@if(Auth::user()->rol != 99)
    <h2 class="heading_b uk-margin-bottom">Afspraak maken met {{$relatie->voornaam." ".$relatie->achternaam}}</h2>
    @if(!isset($temp))
        @if(!isset($ub))
            @if(isset($ubsetAmount))
                <div class="md-card uk-margin-large-bottom">
                    <div class="md-card-content">
                        
                        {!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/uitgestelde-betaling/afspraken', 'method' => 'post', 'data-parsley-validate')) !!}
                         <div class="uk-grid" data-uk-grid-margin>
                             <h3>Afspraak Informatie</h3>
                            <div class="uk-width-medium">
                                <div class="uk-form-row">
                                </div>
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-large-2-4">
                                            <label for="kUI_datetimepicker_basic" class="uk-form-label">Start Datum:</label>
                                            <input id="kUI_datetimepicker_basic" name="startdatum" required/>
                                        </div>
                                        <div class="uk-width-large-2-4">
                                            <label for="kUI_datetimepicker_basic_end" class="uk-form-label">Eind Datum:</label>
                                            <input id="kUI_datetimepicker_basic_end" name="einddatum" required/>
                                        </div>                       
                                    </div>
                                </div>
                                <br />
                                <span class="icheck-inline">
                            <input type="radio" name="radio_demo" value="0" id="radio_demo_inline_1" data-md-icheck />
                            <label for="radio_demo_inline_1" class="inline-label">Voor 12:30</label>
                        </span>
                        <span class="icheck-inline">
                            <input type="radio" name="radio_demo" value="1" id="radio_demo_inline_2" data-md-icheck />
                            <label for="radio_demo_inline_2" class="inline-label">Na 12:30</label>
                        </span>
                        <span class="icheck-inline">
                            <input type="radio" name="radio_demo" value="2" id="radio_demo_inline_3" data-md-icheck checked />
                            <label for="radio_demo_inline_3" class="inline-label">Specifieke tijd</label>
                        </span>
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        
                                        <div class="uk-width-medium-2-4">
                                            <select id="select_demo_5" name="afspraaktype_id" data-md-selectize data-md-selectize-bottom>
                                                @foreach($types as $type)
                                                <option value="{{$type->id}}">{{$type->omschrijving}}</option>
                                                @endforeach
                                            </select>
                                            <span class="uk-form-help-block">Type Afspraak</span>
                                        </div>
                                        <input type="hidden" name="verwerkt_door" value="{{Auth::user()->id}}"/>
                                        <input type="hidden" name="klant_id" value="{{$relatie->id}}"/>
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-large-1-1">
                                            <div class="parsley-row">
                                                <span class="uk-form-help-block">Te betalen bedrag</span>
                                                <input type="number" step="0.01" class="md-input label-fixed" id="d_form_amount" name="totaalbedrag" value="{{$afspraak->totaalbedrag}}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-large-1-1">
                                            <label>Omschrijving</label><br>
                                            <textarea class="md-input label-fixed" id='editor' placeholder="Klant wilt een half uur van te voren gebeld worden." name="omschrijving"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input type="hidden" name="afspraak_id" value="{{$afspraak->id}}"/>
                                    <div class="uk-form-row">
                                        <div align="right">
                                            <div class="md-btn-group">
                                                <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg Afspraak toe</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        {!! Form::close() !!}
                    </div>
                </div>
            @else
                <div class="md-card uk-margin-large-bottom">
                <div class="md-card-content">
                    {!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/verwerken/nietgelukt/nieuweafspraak', 'method' => 'post', 'data-parsley-validate')) !!}
                     <div class="uk-grid" data-uk-grid-margin>
                         <h3>Afspraak Informatie</h3>
                        <div class="uk-width-medium">
                            <div class="uk-form-row">
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-large-2-4">
                                        <label for="kUI_datetimepicker_basic" class="uk-form-label">Start Datum:</label>
                                        <input id="kUI_datetimepicker_basic" name="startdatum" required/>
                                    </div>
                                    <div class="uk-width-large-2-4">
                                        <label for="kUI_datetimepicker_basic_end" class="uk-form-label">Eind Datum:</label>
                                        <input id="kUI_datetimepicker_basic_end" name="einddatum" required/>
                                    </div>                       
                                </div>
                            </div>
                            <br />
                            <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="0" id="radio_demo_inline_1" data-md-icheck />
                        <label for="radio_demo_inline_1" class="inline-label">Voor 12:30</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="1" id="radio_demo_inline_2" data-md-icheck />
                        <label for="radio_demo_inline_2" class="inline-label">Na 12:30</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="2" id="radio_demo_inline_3" data-md-icheck checked />
                        <label for="radio_demo_inline_3" class="inline-label">Specifieke tijd</label>
                    </span>

                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    
                                    <div class="uk-width-medium-2-4">
                                        <select id="select_demo_5" name="afspraaktype_id" data-md-selectize data-md-selectize-bottom>
                                            @foreach($types as $type)
                                            <option value="{{$type->id}}">{{$type->omschrijving}}</option>
                                            @endforeach
                                        </select>
                                        <span class="uk-form-help-block">Type Afspraak</span>
                                    </div>
                                    <input type="hidden" name="verwerkt_door" value="{{Auth::user()->id}}"/>
                                    <input type="hidden" name="klant_id" value="{{$relatie->id}}"/>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-large-1-1">
                                        <label>Omschrijving</label><br>
                                        <textarea class="md-input label-fixed" id='editor' placeholder="Klant wilt een half uur van te voren gebeld worden." name="omschrijving"></textarea>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="afspraak_id" value="{{$afspraak->id}}"/>
                                <div class="uk-form-row">
                                    <div align="right">
                                        <div class="md-btn-group">
                                            <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                            <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg Afspraak toe</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    {!! Form::close() !!}
                </div>
                </div>
            @endif
        @elseif(isset($ub))
            <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-large-1-2 uk-width-medium-1-1">
                <div class="md-card uk-margin-large-bottom">
                    <div class="md-card-content">
                        @if(isset($gelukt))
                        {!! Form::open(array('url' => '/afspraak/'.$afspraak->id.'/verwerken/gelukt/nieuweafspraak', 'method' => 'post', 'data-parsley-validate')) !!}
                        @else
                    	{!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/verwerken/nietgelukt/nieuweafspraak', 'method' => 'post', 'data-parsley-validate')) !!}
                        @endif 
                         <div class="uk-grid" data-uk-grid-margin>
                	         <h3>Afspraak Informatie</h3>
                            <div class="uk-width-medium">
                                <div class="uk-form-row">
                                </div>
                            	<div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-large-2-4">
                                            <label for="kUI_datetimepicker_basic" class="uk-form-label">Start Datum:</label>
                                            <input id="kUI_datetimepicker_basic" name="startdatum" required/>
                                        </div>
                                    	<div class="uk-width-large-2-4">
                                            <label for="kUI_datetimepicker_basic_end" class="uk-form-label">Eind Datum:</label>
                                            <input id="kUI_datetimepicker_basic_end" name="einddatum" required/>
                                        </div>                       
                                    </div>
                                </div>
                            <br />
                                <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="0" id="radio_demo_inline_1" data-md-icheck />
                        <label for="radio_demo_inline_1" class="inline-label">Voor 12:30</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="1" id="radio_demo_inline_2" data-md-icheck />
                        <label for="radio_demo_inline_2" class="inline-label">Na 12:30</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="radio_demo" value="2" id="radio_demo_inline_3" data-md-icheck checked />
                        <label for="radio_demo_inline_3" class="inline-label">Specifieke tijd</label>
                    </span>

                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                    	
                                        <div class="uk-width-medium-2-4">
                                            <select id="select_demo_5" name="afspraaktype_id" data-md-selectize data-md-selectize-bottom>
                                                @foreach($types as $type)
                                                @if($type->id == 3 || $type->id == 5)
                                                <option value="{{$type->id}}">{{$type->omschrijving}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            <span class="uk-form-help-block">Type Afspraak</span>
                                        </div>
                                        <input type="hidden" name="verwerkt_door" value="{{Auth::user()->id}}"/>
                                        <input type="hidden" name="klant_id" value="{{$relatie->id}}"/>
                                    </div>
                                </div> 
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                    @if(isset($tweede) && !isset($test))
                                        <div class="uk-width-large-1-1">
                                            <div class="parsley-row">
                                                <span class="uk-form-help-block">Te betalen bedrag</span>
                                                <input type="number" step="0.01" class="md-input label-fixed" id="d_form_amount" name="tebetalen" required>
                                            </div>
                                        </div>
                                    @else
                                        <div class="uk-width-large-1-2">
                                            <div class="parsley-row">
                                                <span class="uk-form-help-block">Bedrag betaald</span>
                                                <input type="number" step="0.01" class="md-input label-fixed" id="d_form_amount" name="betaald" value="0" required>
                                            </div>
                                        </div>
                                        <div class="uk-width-large-1-2">
                                            <div class="parsley-row">
                                                <span class="uk-form-help-block">Te betalen bedrag</span>
                                                <input type="number" step="0.01" class="md-input label-fixed" id="d_form_amount" name="tebetalen" required>
                                            </div>
                                        </div>
                                    @endif
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-large-1-1">
                                            <label>Omschrijving</label><br>
                                            <textarea class="md-input label-fixed" id='editor' placeholder="Klant wilt een half uur van te voren gebeld worden." name="omschrijving"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                @if(isset($gelukt))
                                    <input type="hidden" name="gelukt" value="true">
                                @endif
                                <input type="hidden" name="meer" value="false"/>
                                <input type="hidden" name="afspraak_id" value="{{$afspraak->id}}"/>
            	            	<div class="uk-form-row">
            			            <div align="right">
            			            	<div class="md-btn-group">
            				            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
            				                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg Afspraak toe</button>
            			            	</div>
            			            </div>
            		        	</div>
            				</div>
                        </div> 
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="uk-width-large-1-2 uk-width-medium-1-1">
                <div class="md-card uk-margin-large-bottom">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                             <h3>Originele afspraak</h3>
                            <div class="uk-width-medium">
                                <table class="uk-table uk-table-hover">
                                    <thead>
                                        <tr>
                                            <td>Datum & tijd</td>
                                            <td>Klant</td>
                                            <td>Werkzaamheden</td>
                                            <td>Totaal bedrag</td>
                                            <td>Openstaand</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Start: {{date('d-m-Y', strtotime($afspraak->startdatum))}}<br> Eind: {{date('d-m-Y', strtotime($afspraak->einddatum))}}</td>
                                            <td>{{$relatie->voornaam}} {{$relatie->achternaam}}</td>
                                            <td>
                                                <ul>
                                                    <?php $amount = 0.00; ?>
                                                    @foreach($actperafs as $apa)
                                                        @if($apa->afspraak_id == $afspraak->id)
                                                            <li>{{$apa->aantal}}x {{$apa->activiteit->omschrijving}} -> € {{number_format($apa->aantal * $apa->prijs - $apa->kortingsbedrag, 2)}}<br>{{$apa->opmerking}}</li>
                                                            <?php $amount += ($apa->aantal * $apa->prijs - $apa->kortingsbedrag); ?>
                                                        @endif
                                                    @endforeach
                                                    @if($amount < $afspraak->totaalbedrag && $afspraak->afspraaktype_id == 3)
                                                    <li>Uitgestelde betaling ophalen -> € {{$afspraak->totaalbedrag - $amount}}</li>
                                                    @endif
                                                </ul></td>
                                            <td>€ {{number_format($afspraak->totaalbedrag, 2)}}</td>
                                            <td>€ {{number_format($afspraak->niet_betaald, 2)}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            @if(isset($nieuweafspraken))
                <div class="md-card uk-margin-large-bottom">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <h3>Ingeplande afspraken betreft oorspronkelijke afspraak</h3>
                            <div class="uk-width-medium">
                                <table class="uk-table uk-table-hover">
                                    <thead>
                                        <tr>
                                            <td>Datum & tijd</td>
                                            <td>Op te halen bedrag</td>
                                            <td>Ingedeeld</td>
                                            <td>Betaald</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($nieuweafspraken as $nas)
                                        <tr>
                                            <td>Start: {{date('d-m-Y', strtotime($nas->startdatum))}}<br> Eind: {{date('d-m-Y', strtotime($nas->einddatum))}}</td>
                                            <td>€ {{number_format($nas->totaalbedrag, 2)}}</td>
                                            <td>@if($nas->ingepland($nas->id)) Ja @else Nee @endif </td>
                                            <td>@if($nas->betaald == 1) Ja @else Nee @endif</td>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            @endif
        @endif
    @else
        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                {!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/garantie/afspraken/nietopgelost/terugbetaalverzoek', 'method' => 'post', 'data-parsley-validate')) !!}
                 <div class="uk-grid" data-uk-grid-margin>
                     <h3>Afspraak Informatie</h3>
                    <div class="uk-width-medium">
                        <div class="uk-form-row">
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-2-4">
                                    <label for="kUI_datetimepicker_basic" class="uk-form-label">Start Datum:</label>
                                    <input id="kUI_datetimepicker_basic" name="startdatum" required/>
                                </div>
                                <div class="uk-width-large-2-4">
                                    <label for="kUI_datetimepicker_basic_end" class="uk-form-label">Eind Datum:</label>
                                    <input id="kUI_datetimepicker_basic_end" name="einddatum" required/>
                                </div>                       
                            </div>
                        </div>
                                <br />
                        <span class="icheck-inline">
                            <input type="radio" name="radio_demo" value="0" id="radio_demo_inline_1" data-md-icheck />
                            <label for="radio_demo_inline_1" class="inline-label">Voor 12:30</label>
                        </span>
                        <span class="icheck-inline">
                            <input type="radio" name="radio_demo" value="1" id="radio_demo_inline_2" data-md-icheck />
                            <label for="radio_demo_inline_2" class="inline-label">Na 12:30</label>
                        </span>
                        <span class="icheck-inline">
                            <input type="radio" name="radio_demo" value="2" id="radio_demo_inline_3" data-md-icheck checked />
                            <label for="radio_demo_inline_3" class="inline-label">Specifieke tijd</label>
                        </span>

                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                
                                <div class="uk-width-medium-2-4">
                                    <select id="select_demo_5" name="afspraaktype_id" data-md-selectize data-md-selectize-bottom>
                                        
                                        <option value="{{$types->id}}">{{$types->omschrijving}}</option>
                                        
                                    </select>
                                    <span class="uk-form-help-block">Type Afspraak</span>
                                </div>
                                <input type="hidden" name="verwerkt_door" value="{{Auth::user()->id}}"/>
                                <input type="hidden" name="klant_id" value="{{$relatie->id}}"/>
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-1">
                                    <label>Bedrag</label>
                                    <input type="number" step="0.01" min="0.1" class="md-input label-fixed" id="d_form_amount" name="totaalbedrag" required>
                                </div>
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-1">
                                    <label>Omschrijving</label><br>
                                    <textarea class="md-input label-fixed" id='editor' placeholder="Klant wilt een half uur van te voren gebeld worden." name="omschrijving"></textarea>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="afspraak_id" value="{{$afspraak->id}}"/>
                            <div class="uk-form-row">
                                <div align="right">
                                    <div class="md-btn-group">
                                        <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                        <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg Terugbetaalverzoek toe</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                {!! Form::close() !!}
            </div>
        </div>
    @endif
    @endsection

@endif

@push('scripts')

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>
@endpush