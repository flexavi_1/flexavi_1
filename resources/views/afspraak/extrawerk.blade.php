@extends('layouts.master')

@section('title', 'Afspraak maken')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <script src="{{ URL::asset('assets/bower_components/handlebars/handlebars.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/handlebars_helpers.min.js')}}"></script>

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>
@endpush
@section('content')

@if($danger != "")
   <div class="md-card md-card-collapsed">
        <div class="md-card-toolbar md-bg-red-400 uk-text-contrast">
            <h3 class="md-card-toolbar-heading-text ">
                {{$danger}}
            </h3>
        </div>
    </div>
@endif

@if(Auth::user()->rol != 99)

 <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large">
        <div class="md-card">
            <div class="md-card-content">
        	    <h3 class="heading_a">Werkzaamheden Toevoegen</h3>
    			{!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/verwerken/gelukt/extrawerk', 'method' => 'post')) !!}
    			<div data-dynamic-fields="field_template_b"></div>
                <script id="field_template_b" type="text/x-handlebars-template">
                
                <div class="uk-grid uk-grid-medium form_section form_section_separator" id="d_form_row" data-uk-grid-match>
                    <div class="uk-width-9-10">
                        <div class="uk-grid">
                            <div class="uk-width-2-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Werkzaamheden</span>
                                    <select id="d_form_select_activity" name="activiteiten[]" data-md-selectize required>
                                        @foreach($activiteiten as $activiteit)
                                        <option value="{{$activiteit->id}}">{{$activiteit->omschrijving}}, {{$activiteit->prijs}} {{$activiteit->eenheid}} | {{$activiteit->beschrijving}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Opmerking</span>
                                    <input type="text" class="md-input label-fixed" id="d_form_amount" name="opmerking[]">
                                </div>
                            </div>>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Aantal</span>
                                    <input type="number" step="0.1" min="0.1" class="md-input label-fixed" id="d_form_amount" name="aantal[]" required>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Prijs per eenheid</span>
                                    <input type="number" value='0.00' step="0.01" class="md-input label-fixed" id="d_form_amount" name="prijs[]" required>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Korting in Euro</span>
                                    <input type="number" value='0.00' step="0.01" class="md-input label-fixed" id="d_form_amount" name="korting[]" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-10 uk-text-center">
                        <div class="uk-vertical-align uk-height-1-1">
                            <div class="uk-vertical-align-middle">
                                <a href="#" class="btnSectionClone" data-section-clone="#d_form_section"><i class="material-icons md-36">&#xE146;</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                </script>
                
                <br>
                <h3 class="heading_a">Al ingevoerde werkzaamheden</h3>
                <br>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-1-1">
                                @foreach($ahas as $aha)
                                <p>
                                    <input type="checkbox" name="service[]" value="{{$aha->activiteit_id}}" class="inline-label" data-md-icheck/>
                                    <label for="service[]" name="name[]" class="inline-label">{{$aha->aantal}} {{$aha->activiteit->omschrijving}} voor €{{number_format($aha->aantal * $aha->prijs, 2)}} || Rekenen als Service?</label>
                                </p>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <h3 class="heading_a">Klusprijs</h3></br>
                <div class="uk-input-group">
                   <span class="uk-input-group-addon"><input type="checkbox" name="klusprijsAan" value="1" data-md-icheck/></span>
                   <label>Klusprijs</label>
                   <input type="number" step="0.01" name="klusprijs" class="md-input" />
                </div>
                 <p><em>N.B. Bij leeglaten van de prijs, wordt de standaardprijs gehanteerd.</em></p>
                 <p><em>N.B. Indien een activiteit als service uitgevoerd wordt, dient de korting het volledige bedrag te zijn.</em></p>
                 <p><em>N.B. Bij het invullen van een klusprijs worden de prijzen per werkactiviteit automatisch berekend en dus derhalve genegeerd.</em></p>
                 <br>
                @if($afspraak->afspraaktype_id == $garantie)
                <p>
                    <input type="checkbox" name="voldaan" id="checkbox_demo_1" value="1" data-md-icheck />
                    <label for="checkbox_demo_1" class="inline-label">Is betaald</label>
                </p>
                @endif
                <div class="uk-grid">
                    <div class="uk-width-1-1" align="right">
                        <button type="reset" href="#" class="md-btn md-btn-warning">Reset</button>
                        <button type="submit" href="#" class="md-btn md-btn-success">Voeg werkzaamheden Toe</button>
                    </div>
                </div>
                    <input type="hidden" name="klant_id" value="{{$relatie->id}}"/>
                    <input type="hidden" name="afspraak_id" value="{{$afspraak->id}}"/>
                    <input type="hidden" name="extra" value="{{$extra}}"/>
                {{-- if($afspraak->afspraaktype_id != $garantie) --}}
                    <input type="hidden" name="voldaan" value="{{$voldaan}}"/> 
                {{-- @ndif --}}
                {{-- {!! Form::close() !!}  --}}
                <a href="/afspraak/{{$afspraak->id}}/verwerken/activiteit/nieuw">Nieuwe activiteit toevoegen</a>
            </div>
        </div>

                {!! Form::close() !!} 
    </div>
</div>

@endif



@endsection