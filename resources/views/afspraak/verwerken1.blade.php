@extends('layouts.master')

@section('title', 'Afspraak maken')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
    <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>


    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>

@endpush
@section('content')

    @if(Auth::user()->rol != 99)

        <h2 class="heading_b uk-margin-bottom">Afspraak verwerken
            van {{$relatie->voornaam." ".$relatie->achternaam}}</h2>

        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                {!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/verwerken/', 'method' => 'post', 'data-parsley-validate')) !!}
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-2">
                                <p>Is de afspraak doorgegaan?</p>
                                <p>
                                    <input type="radio" name="gelukt" value="1" data-md-icheck/>
                                    <label for="radio_demo_1" class="inline-label">Ja</label>
                                </p>
                                <p>
                                    <input type="radio" name="gelukt" value="0" data-md-icheck checked/>
                                    <label for="radio_demo_2" class="inline-label">Nee</label>
                                </p>
                            </div>

                            @if($afspraak->afspraaktype_id != 5)
                                <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-2">
                                    <p>
                                        @if($afspraak->afspraaktype_id == 1)
                                            Is er extra werk uitgevoerd?
                                        @elseif($afspraak->afspraaktype_id == 2)
                                            Is er werk uitgevoerd?
                                        @elseif($afspraak->afspraaktype_id == 3)
                                            Is er extra werk uitgevoerd?
                                        @elseif($afspraak->afspraaktype_id == 4)
                                            Is er extra werk uitgevoerd?
                                        @elseif($afspraak->afspraaktype_id == 5)
                                            Is er extra werk uitgevoerd?
                                        @else
                                            Is er extra werk uitgevoerd?
                                        @endif
                                    </p>
                                    <p>
                                        <input type="radio" name="extra" value="1" data-md-icheck/>
                                        <label for="radio_demo_1" class="inline-label">Ja</label>
                                    </p>
                                    <p>
                                        <input type="radio" name="extra" value="0" data-md-icheck checked/>
                                        <label for="radio_demo_2" class="inline-label">Nee</label>
                                    </p>
                                </div>
                                <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-2">
                                    <p>
                                        @if($afspraak->afspraaktype_id == 1)
                                            Is de afspraak betaald?
                                        @elseif($afspraak->afspraaktype_id == 2)
                                            Is er een nieuwe afspraak uit voortgekomen?
                                        @elseif($afspraak->afspraaktype_id == 3)
                                            Is de afspraak betaald?
                                        @elseif($afspraak->afspraaktype_id == 4)
                                            Is de garantie opgelost?
                                        @elseif($afspraak->afspraaktype_id == 5)
                                            Is de afspraak betaald?
                                        @else
                                            Is de afspraak terugbetaald?
                                        @endif
                                    </p>
                                    <p>
                                        <input type="radio" name="voldaan" value="1" data-md-icheck checked/>
                                        <label for="voldaan" class="inline-label">Ja</label>
                                    </p>
                                    <p>
                                        <input type="radio" name="voldaan" value="0" data-md-icheck/>
                                        <label for="voldaan" class="inline-label">Nee</label>
                                    </p>
                                </div>
                            @endif
                            <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-2">
                                <p>Is de afspraak betaald per bank of per kas?</p>
                                <p>
                                    <input type="radio" name="bank" value="1" data-md-icheck
                                           @if($afspraak->afspraaktype_id != 5) checked @endif />
                                    <label for="bank" class="inline-label">Kas</label>
                                </p>
                                <p>
                                    <input type="radio" name="bank" value="0" data-md-icheck
                                           @if($afspraak->afspraaktype_id == 5) checked @endif/>
                                    <label for="bank" class="inline-label">Bank</label>
                                </p>
                            </div>
                            @if($afspraak->afspraaktype_id != 5)
                                <div class="uk-width-large-1-1 uk-width-medium-1-1">
                                    <label>Advies</label><br>
                                    <textarea class="md-input label-fixed" id="editor"
                                              placeholder="Let op, beschrijf hier alleen het advies, bij een offerte moet er een offerte toegevoegd worden."
                                              name="advies"></textarea>
                                </div>
                            @else
                                <div class="uk-width-large-1-1">
                                    <div class="parsley-row">
                                        <span class="uk-form-help-block">Betalingsherinnering verlengen met X dagen:</span>
                                        <input type="number" step="1" class="md-input label-fixed" id="d_form_amount"
                                               name="betalingsherinnering" value="7">
                                    </div>
                                </div>
                            @endif
                            @if($afspraak->getDateDifference()->d > 0 && !App\Models\Status_has_appointment::getNull($afspraak->id))
                                {{-- if its about multiple appointments where the workday_id is known. --}}
                                <div class="uk-width-medium-2-4">
                                    <select id="select_demo_5" name="sha_id" data-md-selectize data-md-selectize-bottom>
                                        @foreach(App\Models\Status_has_appointment::returnShas($afspraak->id) as $sha)
                                            <option value="{{$sha->id}}">Afspraak
                                                van: {{App\Models\Werkdag::returnDate($sha->werkdag_id)}}</option>
                                        @endforeach
                                    </select>
                                    <span class="uk-form-help-block">Selecteer de datum van de afspraak.</span>
                                </div>
                            @endif
                            <br>
                            <div class="uk-width-large-1-1 uk-width-medium-1-1">
                                <p>Deze antwoorden zijn bepalend voor het verwerkproces, raadpleeg de handleiding voor
                                    een uitgebreide uitleg.<br></p>
                                <p>
                                    <strong>N.B.</strong> <br> - Als de afspraak niet is doorgegaan, worden de andere
                                    knoppen genegeerd.<br>
                                    - Als het een afspraak betreft waarin er een offerte opgesteld en er gewerkt is, dan
                                    telt voldaan alsof deze wel/niet betaald is.<br>
                                    - Als de afspraak deels betaald is, wordt hier 'Nee' aangevinkt.<br>
                                    - Als de afspraak meerdere dagen beslaat en de verwerking zit niet bij de laatste
                                    afspraak. Dan is wel/niet betaald (nog) niet relevant.
                                </p>
                            </div>
                            <div class="uk-width-large-1-1">
                                <div class="btn-group" align="right">
                                    <a href="/relatie/{{$relatie->id}}" class="md-btn md-btn-danger md-btn-wave-light">Cancel</a>
                                    <button class="md-btn md-btn-success md-btn-wave-light">Verder</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    @endif

@endsection
