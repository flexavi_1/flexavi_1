@extends('layouts.master')

@section('title', 'Betalingsherinnering verwerken')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Betalingsherinnering verlengen van {{$relatie->voornaam." ".$relatie->achternaam}}</h2>

<div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
        <h3>Verleng de betalingsherinnering</h3>
        {!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/betalingsherinnering/', 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-grid" data-uk-grid-margin>
                    
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@endif
@endsection