@extends('layouts.master')

@section('title', 'Afspraak maken')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Afspraak aanmaken voor {{$relatie->voornaam." ".$relatie->achternaam}}</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
    	{!! Form::open(array('url'=>'/afspraak/maken/'.$relatie->id.'/geschiedenis', 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-1-1">
                        <p>Selecteer welke geschiedenisregels er toegevoegd moeten worden.</p>
                        @foreach($geschiedenis as $gs)
                        <p>
                            <input type="checkbox" name="geschiedenis[]" value="{{$gs->id}}" id="checkbox_demo_1[] {{$gs->id}}" data-md-icheck />
                            <label for="checkbox_demo_1[] {{$gs->id}}" name="name[]" class="inline-label">{{$gs->omschrijving}}</label>
                        </p>
                        @endforeach
					</div>
					<input type="hidden" name="afspraak_id" value="{{$afspraak->id}}"/>
                    @if(isset($update))
                        <input type="hidden" name="update" value="{{$update}}"/>
                    @endif
                    <div class="uk-width-large-1-1">
                        <div class="btn-group" align="right">
                            <a class="md-btn md-btn-danger" href="/afspraak/{{$afspraak->id}}/verwijderen">Annuleer</a>
                            <a class="md-btn md-btn-warning" href="/afspraak/{{$afspraak->id}}/vorige/2">Vorige</a>
                        	<button class="md-btn md-btn-success md-btn-wave-light">Verder</button>
                        </div>
                	</div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@endif


@endsection
