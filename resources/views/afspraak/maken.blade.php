@extends('layouts.master')

@section('title', 'Afspraak maken')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
    <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>



    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>

@endpush
@section('content')

    @if(Auth::user()->rol != 99)

        <h2 class="heading_b uk-margin-bottom">Afspraak maken met {{$relatie->voornaam." ".$relatie->achternaam}}</h2>

        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                {!! Form::open(array('url'=>'/afspraak/maken/'.$relatie->id, 'method' => 'post', 'data-parsley-validate')) !!}
                <div class="uk-grid" data-uk-grid-margin>
                    <h3>Afspraak Informatie</h3>
                    <div class="uk-width-medium">
                        <div class="uk-form-row">
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-2-4">
                                    <label for="kUI_datetimepicker_basic" class="uk-form-label">Start Datum:</label>
                                    <input id="kUI_datetimepicker_basic" name="startdatum"
                                           @if(isset($previous)) value='{{date('d-m-Y H:i', strtotime($previous->startdatum))}}'
                                           @else value='{{date('d-m-Y H:i')}}' @endif required/>
                                </div>
                                <div class="uk-width-large-2-4">
                                    <label for="einddatum" class="uk-form-label">Eind Datum:</label>
                                    <input id="kUI_datetimepicker_basic_end" name="einddatum"
                                           @if(isset($previous)) value='{{date('d-m-Y H:i', strtotime($previous->einddatum))}}'
                                           @else value='{{date('d-m-Y 17:00')}}' @endif  required/>
                                </div>

                            </div>
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-2-4">
                                    <select id="select_demo_5" name="geschreven_door" data-md-selectize
                                            data-md-selectize-bottom>

                                        @foreach($werknemers as $werknemer)
                                            @if(isset($previous))
                                                @if($werknemer->id == $previous->geschreven_door)
                                                    <option selected
                                                            value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                                @else
                                                    <option value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                                @endif
                                            @else
                                                <option value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <span class="uk-form-help-block">Geschreven Door:</span>
                                </div>
                                <div class="uk-width-medium-2-4">
                                    <select id="select_demo_5" name="afspraaktype_id" data-md-selectize
                                            data-md-selectize-bottom>
                                        @foreach($types as $type)
                                            @if(isset($previous))
                                                @if($type->id == $previous->afspraaktype_id)
                                                    <option selected
                                                            value="{{$type->id}}">{{$type->omschrijving}}</option>
                                                @endif
                                            @elseif(isset($oude_afspraak))
                                                @if($oude_afspraak->afspraaktype_id == $type->id)
                                                    <option selected
                                                            value="{{$type->id}}">{{$type->omschrijving}}</option>
                                                @else
                                                    <option value="{{$type->id}}">{{$type->omschrijving}}</option>
                                                @endif
                                            @else
                                                <option value="{{$type->id}}">{{$type->omschrijving}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <span class="uk-form-help-block">Type Afspraak</span>
                                </div>
                                <input type="hidden" name="verwerkt_door" value="{{Auth::user()->id}}"/>
                                <input type="hidden" name="klant_id" value="{{$relatie->id}}"/>
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-1">
                                    <label>Omschrijving</label><br>
                                    <textarea class="md-input label-fixed" id="editor" placeholder="Schrijf hier de nuttige informatie voor de collega's, voorbeelden: Uitvoering werkzaamheden: Klant wilt een half uur van te voren gebeld worden. Garantie: Dakgoot lekt nog steeds. Uitgestelde Betaling: Mevrouw betaalt € 70,-.
                                Offerte Opstellen: Betalingsherinnering: Let op, klant betaalt binnen twee weken per bank. Terugbetalingsverzoek: Dakgoot is binnen de garantie niet meer te repareren, crediteer € 35,-"
                                              name="omschrijving">@if(isset($previous))
                                            {!!$previous->omschrijving!!}
                                        @endif</textarea>

                                </div>
                            </div>
                        </div>
                        <br>
                        <span class="icheck-inline">
                    <input type="radio" name="radio_demo" value="0" id="radio_demo_inline_1" data-md-icheck
                           @if(isset($knoppen)) @if(isset($knoppen['voor'])) checked @endif  @endif />
                    <label for="radio_demo_inline_1" class="inline-label">Voor 12:30</label>
                </span>
                        <span class="icheck-inline">
                    <input type="radio" name="radio_demo" value="1" id="radio_demo_inline_2" data-md-icheck
                           @if(isset($knoppen)) @if(isset($knoppen['na'])) checked @endif  @endif />
                    <label for="radio_demo_inline_2" class="inline-label">Na 12:30</label>
                </span>
                        <span class="icheck-inline">
                    <input type="radio" name="radio_demo" value="2" id="radio_demo_inline_3" data-md-icheck
                           @if(isset($knoppen)) @if(isset($knoppen['spec'])) checked @endif @else checked @endif />
                    <label for="radio_demo_inline_3" class="inline-label">Specifieke tijd</label>
                </span>

                        <p>
                            <input type="checkbox" name="voorrijkosten" value="1" id="checkbox_demo_3" data-md-icheck/>
                            <label for="voorrijkosten" class="inline-label">Voorrijkosten rekenen? (alleen geldig bij
                                offerte opstellen)</label>
                        </p>
                        <p>
                            <input type="checkbox" name="initiatief" value="1" id="checkbox_demo_3" data-md-icheck
                                   @if(isset($knoppen)) @if(isset($knoppen['initiatief'])) checked @endif @endif />
                            <label for="initiatief" class="inline-label">Initiatief ligt bij klant</label>
                        </p>
                        @if($relatie->email != null && $relatie->email != "")
                            <p>
                                <input type="checkbox" name="bevestiging" value="1" id="checkbox_demo_3"
                                       data-md-icheck/>
                                <label for="bevestiging" class="inline-label">Afspraakbevestiging sturen?</label>
                            </p>
                        @endif
                        @if(isset($oude_afspraak))
                            <p>
                                <input type="checkbox" name="werkzaamheden" value="1" id="checkbox_demo_3"
                                       data-md-icheck/>
                                <label for="werkzaamheden" class="inline-label">Dezelfde werkzaamheden gebruiken? (zie
                                    hieronder)</label>
                            </p>
                        @endif
                        @if(isset($afspraak_id))
                            <input type="hidden" name='afspraak_id' value="{{$afspraak_id}}">
                        @endif
                        <div class="uk-form-row">
                            <div align="right">
                                <div class="md-btn-group">
                                    <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @if(isset($oude_afspraak))
            <div class="md-card uk-margin-large-bottom">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <h3>Vorige afspraak</h3>
                        <div class="uk-width-medium">
                            <table class="uk-table uk-table-striped">
                                <thead>
                                <tr>
                                    <th>Startdatum</th>
                                    <th>Einddatum</th>
                                    <th>Geschreven Door</th>
                                    <th>Afspraaktype</th>
                                    <th>Omschrijving</th>
                                    <th>Oude Werkzaamheden</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{date('d-m-Y H:i', strtotime($oude_afspraak->startdatum))}}</td>
                                    <td>{{date('d-m-Y H:i', strtotime($oude_afspraak->einddatum))}}</td>
                                    <td>
                                        @if(App\Models\Employee::findMe($oude_afspraak->geschreven_door) != null)
                                            {{App\Models\Employee::findMe($oude_afspraak->geschreven_door)->voornaam}} {{App\Models\Employee::findMe($oude_afspraak->geschreven_door)->achternaam}}
                                        @else
                                            Werknemer onbekend, selecteer nu de goede werknemer
                                        @endif
                                    </td>
                                    <td>{{App\Models\AppointmentType::getDescription($oude_afspraak->afspraaktype_id)}}</td>
                                    <td>{!!$oude_afspraak->omschrijving!!}</td>
                                    <td>
                                        @foreach($oude_afspraak->aha as $aha)
                                            {{$aha->aantal}}
                                            x {{App\Models\Work::getDescription($aha->activiteit_id)}}
                                            € {{number_format($aha->prijs, 2)}} <br>
                                        @endforeach
                                        Totaal: <b>€ {{number_format($oude_afspraak->totaalbedrag,2)}}</b>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif
@endsection



@push('scripts')

    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>
    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>


    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>
@endpush
