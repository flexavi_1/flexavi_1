@extends('layouts.master')

@section('title', 'Openstaande Betaalafspraken')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>

    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush
@section('content')

    @if(Auth::user()->rol != 99)

        <h4 class="heading_a uk-margin-bottom">Openstaande Betaalafspraken</h4>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-3">
                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">
                    <button class="md-btn">Zoek op Status<i class="material-icons">&#xE315;</i></button>
                    <div class="uk-dropdown">
                        <ul class="uk-nav uk-nav-dropdown">
                            @foreach($statussen as $status)
                                <li>
                                    <a href="/afspraak/openstaande-betalingen/{{$status->id}}">{{$status->omschrijving}}</a>
                                </li>
                            @endforeach
                            <li><a href="/afspraak/openstaande-betalingen/all">Totaal Overzicht</a></li>
                            <li><a href="/afspraak/openstaande-betalingen/">Open afspraken</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium-1-3">
                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">
                    <button class="md-btn">Zoek op Afspraaktype<i class="material-icons">&#xE315;</i></button>
                    <div class="uk-dropdown">
                        <ul class="uk-nav uk-nav-dropdown">
                            @foreach($afspraaktypes as $status)
                                <li>
                                    <a href="/afspraak/openstaande-betalingen/{{$status->omschrijving}}">{{$status->omschrijving}}</a>
                                </li>
                            @endforeach
                            <li><a href="/afspraak/openstaande-betalingen/">Beide types</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="dt_colVis_buttons"></div>
                <table class="uk-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="width: 10%;">Afspraak Datum:</th>
                        <th style="width: 10%;">Klant</th>
                        <th style="width: 15%;">Adres</th>
                        <th style="width: 10%;">Contactgegevens</th>
                        <th style="width: 10%;">Afspraaktype</th>
                        <th style="width: 15%;">Afspraak omschrijving</th>
                        <th style="width: 10%;">Afspraak redenen</th>
                        <th style="width: 10%;">Openstaand bedrag</th>
                        <th style="width: 10%;">Status</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($afspraken as $afs)
                        <tr>
                            <td style="width: 10%;"><b>{{date('d-m-Y', strtotime($afs->afspraak->startdatum))}}
                                    <br>{{date('H:i', strtotime($afs->afspraak->startdatum))}}
                                    - {{date('H:i', strtotime($afs->afspraak->einddatum))}}</b></td>
                            <td style="width: 10%;"><a
                                        href="/relatie/{{$afs->afspraak->klant_id}}">Fam. {{$afs->afspraak->klant->achternaam}}</a>
                            </td>
                            <td style="width: 15%;">
                                {{$afs->afspraak->klant->straat.' '.$afs->afspraak->klant->huisnummer.' '.$afs->afspraak->klant->toevoeging}}
                                <br>
                                {{$afs->afspraak->klant->postcode.' '.$afs->afspraak->klant->woonplaats}}
                            </td>
                            <td style="width: 10%;">{{$afs->afspraak->klant->telefoonnummer_prive}} {{$afs->afspraak->klant->telefoonnummer_zakelijk}}
                                <br>{{$afs->afspraak->klant->email}}</td>
                            <td style="width: 10%;">{{App\Models\AppointmentType::getDescription($afs->afspraak->afspraaktype_id)}}</td>
                            <td style="width: 15%;">{{$afs->afspraak->omschrijving}}</td>
                            <td style="width: 10%;">{{$afs->afspraak->reden}}</td>
                            <td style="width: 10%;">€ {{number_format($afs->afspraak->totaalbedrag, 2)}}</td>
                            <td style="width: 10%;">{{$afs->status->omschrijving}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @endif

@endsection
