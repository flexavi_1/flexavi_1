@extends('layouts.master')

@section('title', 'Afspraak maken')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>
@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Garantie verwerken van {{$relatie->voornaam." ".$relatie->achternaam}}</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
    	{!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/garantie/afspraken/nietopgelost', 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-1-2 uk-width-medium-1-2">
                    	<p>Wordt er (een deel) gecrediteerd?</p>
                        <p>
                            <input type="radio" name="credit" value="1"  data-md-icheck />
                            <label for="radio_demo_1" class="inline-label">Ja</label>
                        </p>
                        <p>
                            <input type="radio" name="credit" value="0" data-md-icheck checked />
                            <label for="radio_demo_2" class="inline-label">Nee</label>
                        </p>
					</div>
					<div class="uk-width-large-1-2 uk-width-medium-1-2">
						<p>Is er een nieuwe afspraak?</p>
                        <p>
                            <input type="radio" name="app" value="1" data-md-icheck />
                            <label for="radio_demo_1" class="inline-label">Ja</label>
                        </p>
                        <p>
                            <input type="radio" name="app" value="0" data-md-icheck checked />
                            <label for="radio_demo_2" class="inline-label">Nee</label>
                        </p>
					</div>
					<div class="uk-width-large-1-1 uk-width-medium-1-1">
	                    <p><strong>N.B.</strong> <br> - Gecrediteerd leidt tot een terugbetaalverzoek.<br>
                         - Indien beide op ja staan betekent het dat er een afspraak wordt gemaakt om het geld contant terug te betalen.</p>
                	</div>
                    <input type="hidden" name="afspraak_id" value="{{$afspraak->id}}" />
                    <input type="hidden" name="relatie_id" value="{{$relatie->id}}" />
                    <div class="uk-width-large-1-1">
                        <div class="btn-group" align="right">
                        	<button class="md-btn md-btn-success md-btn-wave-light">Verder</button>
                        </div>
                	</div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@endif


@endsection
