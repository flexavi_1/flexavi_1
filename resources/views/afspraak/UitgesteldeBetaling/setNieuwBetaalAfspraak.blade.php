@extends('layouts.master')

@section('title', 'Afspraak maken')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>

@endpush
@section('content')
@if(isset($danger))
<div class="uk-alert md-bg-red-400 " data-uk-alert>
    <a href="#" class="uk-alert-close uk-close"></a>
    <h3>
        {{$danger}}
    </h3>
</div>
@endif
@if(isset($succes))
    <div class="uk-alert md-bg-light-green-400" data-uk-alert>
        <a href="#" class="uk-alert-close uk-close"></a>
        <h3>
            {{$succes}}
        </h3>
    </div>
@endif
@if(Auth::user()->rol != 99)
    <h2 class="heading_b uk-margin-bottom">Afspraak maken met {{$relatie->voornaam." ".$relatie->achternaam}}</h2>
    <div class="md-card uk-margin-large-bottom">
        <div class="md-card-content">
            {!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/uitgestelde-betaling/nieuwe-afspraak', 'method' => 'post', 'data-parsley-validate')) !!}
             <div class="uk-grid" data-uk-grid-margin>
                 <h3>Afspraak Informatie</h3>
                <div class="uk-width-medium">
                    <div class="uk-form-row">
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-2-4">
                                <label for="kUI_datetimepicker_basic" class="uk-form-label">Start Datum:</label>
                                <input id="kUI_datetimepicker_basic" name="startdatum" required/>
                            </div>
                            <div class="uk-width-large-2-4">
                                <label for="kUI_datetimepicker_basic_end" class="uk-form-label">Eind Datum:</label>
                                <input id="kUI_datetimepicker_basic_end" name="einddatum" required/>
                            </div>                       
                        </div>
                    </div>
                    <br />
                    <span class="icheck-inline">
                <input type="radio" name="radio_demo" value="0" id="radio_demo_inline_1" data-md-icheck />
                <label for="radio_demo_inline_1" class="inline-label">Voor 12:30</label>
            </span>
            <span class="icheck-inline">
                <input type="radio" name="radio_demo" value="1" id="radio_demo_inline_2" data-md-icheck />
                <label for="radio_demo_inline_2" class="inline-label">Na 12:30</label>
            </span>
            <span class="icheck-inline">
                <input type="radio" name="radio_demo" value="2" id="radio_demo_inline_3" data-md-icheck checked />
                <label for="radio_demo_inline_3" class="inline-label">Specifieke tijd</label>
            </span>
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            
                            <div class="uk-width-medium-2-4">
                                <select id="select_demo_5" name="afspraaktype_id" data-md-selectize data-md-selectize-bottom>
                                    @foreach($types as $type)
                                    <option value="{{$type->id}}">{{$type->omschrijving}}</option>
                                    @endforeach
                                </select>
                                <span class="uk-form-help-block">Type Afspraak</span>
                            </div>
                            <input type="hidden" name="verwerkt_door" value="{{Auth::user()->id}}"/>
                            <input type="hidden" name="klant_id" value="{{$relatie->id}}"/>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-1-1">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Te betalen bedrag</span>
                                    <input type="number" step="0.01" class="md-input label-fixed" id="d_form_amount" name="tebetalen" value="{{$afspraak->totaalbedrag}}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <input type="hidden" name="betaald" value="0">
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-1-1">
                                <label>Omschrijving</label><br>
                                <textarea class="md-input label-fixed" id='editor' placeholder="Klant wilt een half uur van te voren gebeld worden." name="omschrijving"></textarea>
                            </div>
                        </div>
                    </div>
                    <br>
                    <input type="hidden" name="afspraak_id" value="{{$afspraak->id}}"/>
                        <div class="uk-form-row">
                            <div align="right">
                                <div class="md-btn-group">
                                    <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg Afspraak toe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            {!! Form::close() !!}
        </div>
    </div>
@endif
@endsection

@push('scripts')

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>
@endpush