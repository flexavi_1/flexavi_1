@extends('layouts.master')

@section('title', 'Afspraak maken')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>
@endpush
@section('content')



@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Uitgestelde Betaling afspraak maken met {{$relatie->voornaam." ".$relatie->achternaam}}</h2>

    <!-- Toon overzicht met alle afspraken en werkzaamheden, waaruit geselecteerd kan worden.-->
    <!-- Tevens een veld voor omschrijving waarin extra infomatie toegevoegd kan worden? -->
    <div class="md-card uk-margin-large-bottom">
        <div class="md-card-content">
            {!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/uitgestelde-betaling/afspraken', 'method' => 'post', 'data-parsley-validate')) !!}
             <div class="uk-grid" data-uk-grid-margin>
                <h3>Afspraak Informatie</h3>
                <h3 class="subtitle">Selecteer hier de afspraak waarop de Uitgestelde Betaling betrekking heeft.</h3>
                <div class="uk-width-large">
                    @foreach($alleafspraken as $afs)
                        @if($afs->afspraak->id != $afspraak->id && $afs->status_id == 3)
                        <p>
                            --------------------------------------------------------------------------------------<br>
                            <input type="radio" name="afspraken" id="radio_demo_1" value="{{$afs->afspraak->id}}"  />
                            <label for="radio_demo_1" class="inline-label">{{$afs->afspraak->startdatum}}</label>
                            <div class="timeline_content">
                                <span class="uk-text-bold">{{$afs->afspraak->afspraaktype}} -- {{$afs->status->omschrijving}}; <br>{{$afs->omschrijving}}<br></span>
                                @foreach($actperafs as $afsa)
                                    @if($afs->afspraak->id == $afsa->afspraak->id)
                                        {{$afsa->aantal}} {{$afsa->activiteit->omschrijving}} à € {{$afsa->activiteit->prijs}}<br>
                                    @endif
                                @endforeach
                                {{$afs->afspraak->omschrijving}}<br>
                                <span>Totaalbedrag: € {{$afs->afspraak->totaalbedrag}}</span><br>
                                <span>Betaald: € {{($afs->afspraak->totaalbedrag - $afs->afspraak->niet_betaald)}}</span>
                                <strong>Openstaand: € {{$afs->afspraak->niet_betaald}}</strong>
                            </div>
                            --------------------------------------------------------------------------------------
                        </p>
                        @endif
                    @endforeach
                </div>
                <div class="uk-width-large-1-1">
                    <div class="btn-group" align="right">
                        <a href="/afspraak/{{$afspraak->id}}/verwijderen" class="md-btn md-btn-danger md-btn-wave-light">Cancel</a>
                        <button class="md-btn md-btn-success md-btn-wave-light">Maak Garantie afspraak</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endif
@endsection