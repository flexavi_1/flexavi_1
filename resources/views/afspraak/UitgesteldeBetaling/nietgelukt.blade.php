@extends('layouts.master')

@section('title', 'Afspraak maken')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>
@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Afspraak verwerken van {{$relatie->voornaam." ".$relatie->achternaam}}</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
    	{!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/verwerken/uitgestelde-betaling/nietgelukt', 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-1-1 uk-width-medium-1-1">
                    	<label>Omschrijving</label><br>
                		<textarea class="md-input label-fixed" id='editor' required placeholder="Collega probeerde afspraak te verzetten naar de middag, maar was niet mogelijk voor de klant." name="reden"></textarea>
                	</div>
                    <div class="uk-width-large-1-1 uk-width-medium-1-2">
                        <p>
                            <input type="checkbox" name="eigenschuld" value="1" id="checkbox_demo_2" data-md-icheck />
                            <label for="checkbox_demo_2"  class="inline-label">Afspraak wordt door ons verzet</label>
                        </p>
                    </div>
                </div>
                <br>
                <div class="uk-form-row">
		            <div align="right">
		            	<div class="md-btn-group">
			            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
			                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder</button>
		            	</div>
		            </div>
	        	</div>
	        </div>
	    </div>
	    {!! Form::close() !!}
	</div>
</div>

@endif

@endsection