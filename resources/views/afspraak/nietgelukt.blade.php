@extends('layouts.master')

@section('title', 'Afspraak maken')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
    <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor1');
        CKEDITOR.replace('editor');
    </script>
@endpush
@section('content')

    @if(Auth::user()->rol != 99)

        <h2 class="heading_b uk-margin-bottom">Afspraak verwerken
            van {{$relatie->voornaam." ".$relatie->achternaam}}</h2>

        <div class="md-card uk-margin-large-bottom">
            <div class="md-card-content">
                {!! Form::open(array('url'=>'/afspraak/'.$afspraak->id.'/verwerken/nietgelukt', 'method' => 'post', 'data-parsley-validate')) !!}
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-1-1 uk-width-medium-1-1">
                                <label>Omschrijving Voor TB lijst</label><br>
                                <textarea class="md-input label-fixed" id='editor'
                                          placeholder="Collega probeerde afspraak te verzetten naar de middag, maar was niet mogelijk voor de klant."
                                          name="geschiedenis"></textarea>
                            </div>
                            <div class="uk-width-large-1-1 uk-width-medium-1-1">
                                <label>Omschrijving voor Werklijst</label><br>
                                <textarea class="md-input label-fixed" id='editor1' required
                                          placeholder="Collega probeerde afspraak te verzetten naar de middag, maar was niet mogelijk voor de klant."
                                          name="reden"></textarea>
                            </div>
                            @if(!isset($return))
                                @if($afspraak->getDateDifference()->d > 0 && !App\Models\Status_has_appointment::getNull($afspraak->id))
                                    {{-- if its about multiple appointments where the workday_id is known. --}}
                                    <div class="uk-width-medium-2-4">
                                        <select id="select_demo_5" name="sha_id" data-md-selectize
                                                data-md-selectize-bottom>
                                            @foreach(App\Models\Status_has_appointment::returnShas($afspraak->id) as $sha)
                                                <option value="{{$sha->id}}">Afspraak
                                                    van: {{App\Models\Werkdag::returnDate($sha->werkdag_id)}}</option>
                                            @endforeach
                                        </select>
                                        <span class="uk-form-help-block">Selecteer de datum van de afspraak.</span>
                                    </div>

                                @endif
                                <div class="uk-width-large-1-1 uk-width-medium-1-2">
                                    <p>
                                        <input type="checkbox" name="afspraak" value="nieuwe afspraak"
                                               id="checkbox_demo_1" data-md-icheck/>
                                        <label for="checkbox_demo_1" class="inline-label">Direct nieuwe afspraak
                                            maken</label>
                                    </p>
                                </div>
                                <div class="uk-width-large-1-1 uk-width-medium-1-2">
                                    <p>
                                        <input type="checkbox" name="eigenschuld" value="1" id="checkbox_demo_2"
                                               data-md-icheck/>
                                        <label for="checkbox_demo_2" class="inline-label">Afspraak wordt door ons
                                            verzet</label>
                                    </p>
                                </div>
                        </div>
                        <div class="uk-width-large-1-1 uk-width-medium-1-2">
                            <p>
                                <input type="checkbox" name="notInTb" value="30" id="checkbox_demo_3" data-md-icheck/>
                                <label for="checkbox_demo_3" class="inline-label">Afspraak niet in TB lijst
                                    zetten.</label>
                            </p>
                        </div>
                        @endif
                    </div>
                    <br>
                    <div class="uk-form-row">
                        <div align="right">
                            <div class="md-btn-group">
                                <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        </div>

    @endif

@endsection
