@extends('layouts.master')

@section('title', 'Afspraak maken')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>
@endpush
@section('content')



@if(Auth::user()->rol != 99)
<h2 class="heading_b uk-margin-bottom">Garantie afspraak maken met {{$output[3]->voornaam." ".$output[3]->achternaam}}</h2>

<!-- Toon overzicht met alle afspraken en werkzaamheden, waaruit geselecteerd kan worden.-->
<!-- Tevens een veld voor omschrijving waarin extra infomatie toegevoegd kan worden? -->
<div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
    	{!! Form::open(array('url'=>'/afspraak/'.$output[0]->id.'/garantie/afspraken', 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
	        <h3>Afspraak Informatie</h3>
	        <h3 class="subtitle">Selecteer hier de laatste afspraak waarop de garantie betrekking heeft.</h3>
	        <div class="uk-width-large">
                    <?php $count = 1; ?>
	        	@foreach($output[1] as $afs)
                    {{-- {!! $afs->afspraak->id.", ".$output[0]->id.', '.$afs->status_id.' <hr />' !!} --}}
	        		@if($afs->afspraak->id != $output[0]->id && $afs->status_id != 1 && $afs->status_id != 2)
	        		<p>
                        --------------------------------------------------------------------------------------<br>
                            <input type="radio" name="afspraken[]" id="radio_demo_{{$count}}" value="{{$afs->afspraak->id}}"  />
                            <label for="radio_demo_{{$count}}" class="inline-label">{{$afs->afspraak->startdatum}}</label>
                        <div class="timeline_content">
                        	<span class="uk-text-bold">{{$afs->afspraak->afspraaktype}} -- {{$afs->status->omschrijving}}; <br>{{$afs->omschrijving}}<br></span>
                            @foreach($output[2] as $afsa)
                                @if($afs->afspraak->id == $afsa->afspraak->id)
                                    {{$afsa->aantal}} {{$afsa->activiteit->omschrijving}} à € {{$afsa->prijs}}<br>
                                @endif
                            @endforeach
                            {{$afs->afspraak->omschrijving}}<br>
                            <strong>Totaalbedrag: € {{number_format($afs->afspraak->totaalbedrag,2)}}</strong>
                        </div>
                        <!-- Input amount of money to be paid. -->
                        --------------------------------------------------------------------------------------
                    </p>
                    @endif
                        <?php $count++; ?>
	        	@endforeach
                    {{-- {{dd($output)}} --}}
	        </div>
            <div class="uk-width-large-1-1">
                <div class="btn-group" align="right">
                	<a href="/afspraak/{{$output[0]->id}}/verwijderen" class="md-btn md-btn-danger md-btn-wave-light">Cancel</a>
                    @if(!$output[2]->isEmpty())
                	<button class="md-btn md-btn-success md-btn-wave-light">Maak Garantie afspraak</button>
                    @endif
                </div>
        	</div>
	    </div>
	    {!! Form::close() !!}
	</div>
</div>





@endif
@endsection