@extends('layouts.master')

@section('title', 'Verlof')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>


@endpush
@section('content')

@if(Auth::user()->rol != 99)


<h4 class="heading_a uk-margin-bottom">Verlof Overzicht</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
       
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Aangevraagd op</th>
                		<th>Werknemer</th>
                		<th>Startdatum</th>
                		<th>Einddatum</th>
                		<th>Gekeurd</th>
                        <th></th>
            		</tr>
            	</thead>
			    <tbody>
                    @foreach($aanvragen as $uur)
                        <tr>
                            <td>{{date('d-m-Y', strtotime($uur->created_at))}}</td>
                            <td>{{$uur->werknemer->voornaam}} {{$uur->werknemer->achternaam}}</td>
                            <td>{{date('d-m-Y',strtotime($uur->begin))}}</td>
                            <td>{{date('d-m-Y',strtotime($uur->eind))}}</td>
                            <td>@if($uur->goedgekeurd == 0)Nog in behandeling @elseif($uur->goedgekeurd == 1) Afgekeurd @else Goedgekeurd @endif</td>
                            <td>
                                @if(Auth::user()->rol < 7) <a class="md-btn md-btn-warning md-btn-wave-light" href="/verlof/{{$uur->id}}/setstatus/1">Keur af</a><a href="/verlof/{{$uur->id}}/setstatus/2" class="md-btn md-btn-success md-btn-wave-light">Keur goed</a> @endif
                                @if(Auth::user()->werknemer_id == $uur->werknemer_id) <a class="md-btn md-btn-danger md-btn-wave-light" href="/verlof/{{$uur->id}}/verwijder">Verwijder verlof</a>@endif
                            </td>    
                        </tr>
                    @endforeach
			    </tbody>
			</table>
		</div>
	</div>

<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-3">
        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">
            <button class="md-btn">Zoek per verlof status<i class="material-icons">&#xE315;</i></button>
            <div class="uk-dropdown">
                <ul class="uk-nav uk-nav-dropdown">
                    <li><a href="/verlof/status/0">Openstaande verlofaanvragen</a></li>
                    <li><a href="/verlof/status/1">Afgekeurde verlofaanvragen</a></li>
                    <li><a href="/verlof/status/2">Goedgekeurde verlofaanvragen</a></li>
                    <li><a href="/verlof">Totaal overzicht</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@endif

@endsection
