@extends('layouts.master')

@section('title', 'Uren declaratie Overzicht')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>


@endpush
@section('content')

@if(Auth::user()->rol != 99)


<h4 class="heading_a uk-margin-bottom">Uren Overzicht</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
       
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Datum</th>
                		<th>Werknemer</th>
                		<th>Klant</th>
                		<th>Aantal uur</th>
                		<th>Gekeurd</th>
                        <th>Functie</th>
            		</tr>
            	</thead>
			    <tbody>
                    @foreach($urenpp as $uur)
                        <tr>
                            <td>{{$uur->datum}}</td>
                            <td>{{$uur->werknemer->voornaam}} {{$uur->werknemer->achternaam}}</td>
                            <td><a href="/relatie/{{$uur->klant->id}}">{{$uur->klant->achternaam}}, <br>{{$uur->klant->postcode}} {{$uur->klant->huisnummer}}</a></td>
                            <td>{{floor($uur->aantal_minuten / 60)}} uur {{$uur->aantal_minuten - (floor($uur->aantal_minuten / 60)*60)}} minuten</td>
                            <td>@if($uur->approved == 0)Nog in behandeling @else Behandeld. @endif</td>
                            <td>@if(Auth::user()->rol < 7 || Auth::user()->werknemer_id == $uur->werknemer_id) <a href="/uren/{{$uur->id}}/verwijderen"><i class="material-icons">delete</i></a>@endif</td>    
                        </tr>
                    @endforeach
			    </tbody>
			</table>
		</div>
	</div>

<!-- Zoeken per werknemer --> <!-- Zoeken per datum -->

@endif

@endsection
