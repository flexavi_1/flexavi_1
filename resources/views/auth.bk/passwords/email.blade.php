<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{ URL::asset('assets/assets/img/favicon-16x16.png')}}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/assets/img/favicon-32x32.png')}}" sizes="32x32">

    <title>Altair Admin v2.8.0 - Login Page</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

    <!-- uikit -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/uikit/css/uikit.almost-flat.min.css')}}"/>

    <!-- altair admin login page -->
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/css/login_page.min.css')}}" />



</head>
<body class="login_page">

    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">

            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
                    <div class="user_avatar"></div>
                </div>
                <h2 class="heading_a uk-margin-large-bottom">Reset password</h2>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="uk-form-row{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="login_username" class="col-md-4 control-label">E-mail</label>
                            <input id="login_username" type="email" class="md-input" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                            <div class="uk-width-medium-1-4">
                                <button class="md-btn" data-message="<a href='#' class='notify-action'>{{ $errors->first('email') }}</a> Danger message" data-status="danger" data-pos="top-center">Danger</button>
                            </div>
                            @endif
                        </div>
                        <div class="uk-margin-medium-top">
                            <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">
                                <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                            </button>
                            <a href="/login">Ik heb al een account</a>
                        </div>
                    </form> 
            </div>
        </div>
    </div>
</body>
