@extends('layouts.master')

@section('title', 'Flexavi - Werkactiviteit Toevoegen')

@section('content')

@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>
@endpush


@if(Auth::user()->rol != 99)

<div class="md-card">
    <div class="md-card-content">
        <h3 class="heading_a">Voertuig Toevoegen</h3>
        {!! Form::open(array('url'=>'wagenpark/nieuw', 'method' => 'post')) !!}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
                        	<div class="uk-input-group">
	                        	<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-wrench"></i></span>
	                            <label>Kenteken</label>
	                            {!! Form::text('kenteken', null, array('class' => 'md-input label-fixed', 'required', 'maxlength' => '8', 'minlength' => '8', 'placeholder' => 'AB-12-CD')) !!}
                        	</div>
                        </div>
                        <div class="uk-width-medium-1-3">
                        	<div class="uk-input-group">
	                        	<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-car"></i></span>
	                            <label>Merk</label>
	                            <input type="text" class="md-input label-fixed" name="merk" placeholder="Fiat" required/>
                        	</div>
                        </div>
                        <div class="uk-width-medium-1-3">
                        	<div class="uk-input-group">
	                        	<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-table"></i></span>
	                            <label>Omschrijving</label>
	                            <input type="text" placeholder="Punto Evo 2010" class="md-input label-fixed" name="omschrijving"/>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
		            <div align="right">
		            	<div class="md-btn-group">
			            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
			                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Voeg Voertuig Toe</button>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
    </div>
</div>
	{!! Form::close() !!}

@endif

@endsection