@extends('layouts.master')

@section('title', 'Flexavi - Wagenpark')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)


<h4 class="heading_a uk-margin-bottom">Wagenpark</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-striped">
                    <thead>
                    <tr>
                        <th>Kenteken</th>
                        <th>Merk</th>
                        <th>Omschrijving</th>
                        <th>Verzekerd?</th>
                        <th>Functie</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($autos as $activiteit)
                    <tr>
                    	<td>{{$activiteit->kenteken}}</td>
                    	<td>{{$activiteit->merk}}</td>
                    	<td>{{$activiteit->omschrijving}}</td>
                        <td>{{$activiteit->verzekerd}}</td>
                    	<td>
                    		<a href="/wagenpark/{{$activiteit->id}}/wijzigen" class="md-btn md-btn-warning md-btn-small md-btn-wave-light"><i class="material-icons">edit</i></a>
                    		<a href="/wagenpark/{{$activiteit->id}}/verwijderen" class="md-btn md-btn-danger md-btn-small md-btn-wave-light"><i class="material-icons">delete</i></a>
                    	</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <a href="/wagenpark/nieuw">Nieuw Voertuig toevoegen</a>


@endif

@endsection