@extends('layouts.master')

@section('title', 'Flexavi - Autolijst bijwerken')

@section('content')

@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>    <!-- kendo UI -->
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>

@endpush


@if(Auth::user()->rol != 99)

<div class="md-card">
    <div class="md-card-content">
        <h3 class="heading_a">Voertuig Toevoegen</h3>
        {!! Form::open(array('url'=>'/wagenpark/voeg-geschiedenis-toe', 'method' => 'post')) !!}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-wrench"></i></span>
                                <label>Auto</label>
                                <select id="select_demo_1" class="md-input" name="auto_id">
                                    @foreach($autos as $auto)
                                        <option value="{{$auto->id}}">{{$auto->kenteken}}, {{$auto->merk}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-3">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="material-icons md-36">&#xE87C;</i></span>
                                <label>Chauffeur</label>
                                <select id="select_demo_1" class="md-input" name="chauffeur">
                                    @foreach($werknemers as $werknemer)
                                        <option value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-3">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="material-icons md-36">&#xE87C;</i></span>
                                <label>Bijrijder (optioneel)</label>
                                <select id="select_demo_1" class="md-input" name="bijrijder">
                                        <option value="0">-- Geen Bijrijder --</option>
                                    @foreach($werknemers as $werknemer)
                                        <option value="{{$werknemer->id}}">{{$werknemer->voornaam}} {{$werknemer->achternaam}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-2-4">
                            <label for="kUI_datetimepicker_basic" class="uk-form-label">Start Datum:</label>
                            <input id="kUI_datetimepicker_basic" name="van" required/>
                        </div>
                        <div class="uk-width-large-2-4">
                            <label for="einddatum" class="uk-form-label">Eind Datum:</label>
                            <input id="kUI_datetimepicker_basic_end" name="tot" required/>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-1">
                            <label>Omschrijving</label><br>
                            <textarea class="md-input label-fixed" id='editor' placeholder="Beschrijf hier eventuele opmerkingen." name="omschrijving"></textarea>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="{{Auth::user()->id}}" name="users_id">
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
		            <div align="right">
		            	<div class="md-btn-group">
			            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
			                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Werk autolijst bij</button>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
    </div>
</div>
	{!! Form::close() !!}

@endif

@endsection