@extends('layouts.master')

@section('title', 'Flexavi - Autolijst')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)


<h4 class="heading_a uk-margin-bottom">Wagenpark</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Van</th>
                        <th>Tot</th>
                        <th>Kenteken</th>
                        <th>Chauffeur</th>
                        <th>Bijrijder</th>
                        <th>Omschrijving</th>
                        <th>Verwerkt Door</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($whas as $activiteit)
                    <tr>
                    	<td>{{$activiteit->van}}</td>
                        <td>{{$activiteit->tot}}</td>
                        <td>{{$activiteit->auto_id->kenteken}}</td>
                        <td>{{$activiteit->chauffeur->voornaam}} {{$activiteit->chauffeur->achternaam}}</td>
                        <td>@if($activiteit->bijrijder != null){{$activiteit->bijrijder->voornaam}} {{$activiteit->bijrijder->achternaam}}@endif</td>
                        <td>{{$activiteit->omschrijving}}</td>
                        <td>{{$activiteit->users_id->name}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endif

@endsection