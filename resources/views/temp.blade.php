@extends('layouts.master')

@section('title', 'Flexavi - Index')

@push('scripts')
    <script src="{{ URL::asset('assets/assets/js/pages/dashboard.min.js')}}"></script>    <!--  dashbord functions -->
    <script src="{{ URL::asset('assets/bower_components/chartist/dist/chartist.min.js')}}"></script>    <!-- chartist (charts) -->
    <script src="{{ URL::asset('assets/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>    <!-- easy-pie-chart (circular statistics) -->
    <script src="{{ URL::asset('assets/bower_components/metrics-graphics/dist/metricsgraphics.min.js')}}"></script>    <!-- metrics graphics (charts) -->
    <script src="{{ URL::asset('assets/bower_components/peity/jquery.peity.min.js')}}"></script>    <!-- peity (small charts) -->
    <script src="{{ URL::asset('assets/bower_components/clndr/clndr.min.js')}}"></script>    <!-- CLNDR -->
    <script src="{{ URL::asset('assets/bower_components/countUp.js/dist/countUp.min.js')}}"></script>    <!-- countUp -->
    <script src="{{ URL::asset('assets/bower_components/d3/d3.min.js')}}"></script>    <!-- d3 -->
    <script src="{{ URL::asset('assets/bower_components/maplace-js/dist/maplace.min.js')}}"></script>    <!-- maplace (google maps) -->
    <script src="https://maps.google.com/maps/api/js"></script>    <!-- maplace (google maps) -->
    <script src="{{ URL::asset('assets/assets/js/custom/handlebars_helpers.min.js')}}"></script>    <!-- handlebars.js -->
    <script src="{{ URL::asset('assets/bower_components/handlebars/handlebars.min.js')}}"></script>    <!-- handlebars.js -->
    <script src="{{ URL::asset('assets/assets/js/pages/page_sticky_notes.min.js')}}"></script> <!--  sticky notes functions -->
    <!--  charts functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_charts.min.js')}}"></script>

    {{-- <script src="https://cdnjs.com/libraries/Chart.js"></script> <!-- maplace (google maps) --> --}}


    <script src="{{ URL::asset('assets/assets/js/chartjs/Chart.bundle.js')}}"></script>
    {{-- <script src="{{ URL::asset('assets/assets/js/chartjs/Chart.js')}}"></script> --}}
    <script src="{{ URL::asset('assets/assets/js/chartjs/samples/utils.js')}}"></script>

    <script scr="{{ URL::asset('assets/assets/js/echarts/echarts.min.js')}}"></script>

@endpush


@section('content')
    @if(!$empty)
        <script>
            // var chartjs = require('chart.js')
            // var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var colors = {
                red: 'rgb(255, 99, 132)',
                orange: 'rgb(255, 159, 64)',
                yellow: 'rgb(255, 205, 86)',
                green: 'rgb(75, 192, 192)',
                blue: 'rgb(54, 162, 235)',
                purple: 'rgb(153, 102, 255)',
                grey: 'rgb(201, 203, 207)'
            };
            //total
            var configRevenueChart1 = {
                type: 'line',
                data: {
                    labels: <?php echo json_encode($totalRevenuePerPeriod_months); ?>,
                    datasets: [{
                        label: 'Omzetverloop',
                        backgroundColor: this.colors.red,
                        borderColor: this.colors.red,
                        data: <?php echo json_encode($totalRevenuePerPeriod_data); ?>
                        ,
                        fill: false,
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Totale omzetverloop'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Maand'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Omzet'
                            }
                        }]
                    }
                }
            };
            //per customer
            var configRevenueChart2 = {
                type: 'line',
                data: {
                    labels: <?php echo json_encode($totalRevenuePerPeriod_months); ?>,
                    datasets: [{
                        label: 'Omzet per klant per maand',
                        backgroundColor: this.colors.blue,
                        borderColor: this.colors.blue,
                        data: <?php echo json_encode($totalRevenuePerCustomer_data); ?>
                        ,
                        fill: false,
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Omzet per klant per maand'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Maand'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Omzet'
                            }
                        }]
                    }
                }
            };
            //per employee
            var employees = <?php echo json_encode($totalRevenuePerEmployee_names); ?>;
            var configRevenueChart3 = {
                labels: employees,
                datasets: [{
                    label: '',
                    backgroundColor: colors.green,
                    borderColor: colors.green,
                    borderWidth: 1,
                    data: <?php echo json_encode($totalRevenuePerEmployee_data); ?>
                }]
            };

            window.onload = function () {
                var ctx = document.getElementById('lineChartRevenue').getContext('2d');
                window.myLine = new Chart(ctx, configRevenueChart1);
                var rpc = document.getElementById('lineChartRevenueCustomer').getContext('2d');
                window.myLine = new Chart(rpc, configRevenueChart2);
                var rpe = document.getElementById('lineChartRevenueEmployee').getContext('2d');
                // window.myLine = new Chart(rpe, configRevenueChart3);
                window.myBar = new Chart(rpe, {
                    type: 'bar',
                    data: configRevenueChart3,
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Omzet per werknemer - Totaal'
                        }
                    }
                });
            };

            // var colorNames = Object.keys(window.chartColors);
        </script>
    @endif
    {{-- Postits --}}
    @if(!isset($postits) || count($postits) < 8)
        <div class="uk-grid">
            <div class="uk-width-medium-3-5 uk-width-large-2-5 uk-container-center">
                <div class="md-card">
                    <div class="md-card-content">
                        <span class="note_form_text">Voeg geheugensteun toe</span>
                        <div id="note_form"></div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-4 uk-container-center uk-margin-large-top"
         data-uk-grid="{gutter: 20, controls: '#notes_grid_filter'}" id="notes_grid">
        @if(isset($postits))
            @foreach($postits as $pi)
                <div class="uk-grid-width-1-1">
                    <div class="md-card md-bg-amber-100">
                        <div class="uk-position-absolute uk-position-top-right uk-margin-small-right uk-margin-small-top">
                            <a href="/verwijderpostit/{{$pi->id}}" class="note_action_remove"><i
                                        class="md-icon material-icons">&#xE5CD;</i></a>
                        </div>
                        <div class="md-card-content">
                            <h2 class="heading_b uk-margin-large-right">{{ $pi->title }}</h2>
                            <p>{{ $pi->content }}</p>
                            <span class="uk-margin-top uk-text-italic uk-text-muted uk-display-block uk-text-small">{{ date('Y-m-d', strtotime($pi->created_at)) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>

    <script id="note_form_template" type="text/x-handlebars-template">
        {!! Form::open(array('url'=>'/slaPostItOp', 'method' => 'post')) !!}
        <div class="uk-form-row">
            <label>Title</label>
            <input type="text" name="title" class="md-input" required id="note_f_title"/>
        </div>
        <div class="uk-form-row">
            <label>Note content</label>
            <textarea type="text" name="content" class="md-input" required placeholder=""
                      id="note_f_content"></textarea>
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}"/>
        </div>
        <div class="uk-form-row uk-clearfix">
            <div class="uk-float-right">
                <button type="submit" id="note-add" class="md-btn md-btn-success md-btn-wave-light">Sla op</button>
            </div>
        </div>
        </form>
    </script>
    <script id="note_form_checkbox_template" type="text/x-handlebars-template">
        <li class="uk-margin-small-top uk-clearfix">
            <a href="#" class="uk-float-right remove_checklist_item"><i class="material-icons">&#xE5CD;</i></a>
            <div class="uk-nbfc">
                <input type="checkbox" id="checkbox_@{{ id }}" name="checkboxes" data-md-icheck
                       data-title="@{{ title }}"/>
                <label for="checkbox_@{{ id }}" class="inline-label">@{{ title }}</label>
            </div>
        </li>
    </script>

    <hr>
    {{-- Quick numbers --}}
    @if(Auth::user()->rol < 5 && !$empty)

        <div class="uk-grid data-uk-grid-margin">
            <div class="uk-width-large-1-1 uk-grid-width-medium-1-1 uk-grid-width-small-1-1">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-1-5 uk-width-medium-1-2 uk-width-small-1-1">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span
                                            class="peity_visitors peity_data">5,3,9,6,5,9,1</span></div>
                                <span class="uk-text-muted uk-text-small">Aantal Klanten</span>
                                <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $amountOfCustomers }}</noscript></span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-large-1-5 uk-width-medium-1-2 uk-width-small-1-1">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span
                                            class="peity_visitors peity_data">2,3,5,8,13,21,34</span></div>
                                <span class="uk-text-muted uk-text-small">Totale Omzet</span>
                                {{-- <h2 class="uk-margin-remove">€ <span class="countUpMe">0<noscript>{{ App\Models\Klant::getTotalRevenue() }}</noscript></span></h2> --}}
                                <h2 class="uk-margin-remove">€ <span class="countUpMe">0<noscript>{{ $totalRevenue2[0]->totaal }}</noscript></span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-large-1-5 uk-width-medium-1-2 uk-width-small-1-1">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span
                                            class="peity_sale peity_data">5,3,9,6,5,9,7,3,5,2</span></div>
                                <span class="uk-text-muted uk-text-small">Totaal Openstaande aanvullingen</span>
                                <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ App\Models\Verzonden_aan::getTotalOpen() }}</noscript></span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-large-1-5 uk-width-medium-1-2 uk-width-small-1-1">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span
                                            class="peity_orders peity_data">7/8</span></div>
                                <span class="uk-text-muted uk-text-small">Totaal openstaande betaalafspraken</span>
                                <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ App\Models\Appointment::getAmountOfDefPayments() }}</noscript></span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-large-1-5 uk-width-medium-1-2 uk-width-small-1-1">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span
                                            class="peity_visitors peity_data">5,3,9,6,5,9,1</span></div>
                                <span class="uk-text-muted uk-text-small">Totaal openstaande posten</span>
                                <h2 class="uk-margin-remove">€ <span class="countUpMe">0<noscript>{{ $totalOpen }}</noscript></span>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="heading_a uk-margin-bottom uk-margin-large-top">Omzet</h4>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-large-1-3">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_c uk-margin-bottom">Omzetverloop</h4>
                        <canvas id="lineChartRevenue" class="chartist"></canvas>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-1-3">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_c uk-margin-bottom">Omzet per klant</h4>
                        <canvas id="lineChartRevenueCustomer" class="chartist"></canvas>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-1-3">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_c uk-margin-bottom">Omzet per werknemer</h4>
                        <canvas id="lineChartRevenueEmployee" class="chartist"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="heading_a uk-margin-bottom uk-margin-large-top">Verkoop</h4>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-large-1-6">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_c uk-margin-bottom">Behaalde omzet per provincie</h4>
                        <table class="uk-table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Provincie</th>
                                <th>Omzet</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($totalRevenuePerProvince as $trep)
                                <tr>
                                    <td>{{$trep->province}}</td>
                                    <td>€ {{number_format($trep->omzet, 2)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-2-6">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_c uk-margin-bottom">Hoogst behaalde omzet in provincie door werknemer (Top
                            10)</h4>
                        <table class="uk-table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Werknemer</th>
                                <th>Provincie</th>
                                <th>Omzet</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($totalRevenuePerEmployeePerProvince as $trep)
                                <tr>
                                    <td>{{$trep->name}}</td>
                                    <td>{{$trep->province}}</td>
                                    <td>€ {{number_format($trep->omzet, 2)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-3-6">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_c uk-margin-bottom">Meest verkochte dienst à gemiddeld tarief en aantal (Top
                            10)</h4>
                        <table class="uk-table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Activiteit</th>
                                <th>Totaal</th>
                                <th>Gemiddeld Tarief</th>
                                <th>Gemiddeld Aantal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bestSoldActivityWithAverages as $trep)
                                <tr>
                                    <td>{{$trep->name}}</td>
                                    <td>{{$trep->total}}</td>
                                    <td>€ {{number_format($trep->avgRate, 2)}}</td>
                                    <td>{{number_format($trep->avgAmount, 2)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    @endif

@endsection
