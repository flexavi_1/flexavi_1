@extends('layouts.master')

@section('title', 'Flexavi - Geschiedenis wijzigen')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Geschiedenis regel wijzigen voor {{$relatie->voornaam." ".$relatie->achternaam}}</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
        <h3 class="heading_a">Geschiedenis toevoegen</h3>
        {!! Form::open(array('url'=>'/geschiedenis/'.$regel->id.'/wijzigen', 'method' => 'post')) !!}
        <div class="uk-grid" data-uk-grid-margin>
        	<div class="uk-width-medium">
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
                            <label for="kUI_datetimepicker_basic" class="uk-form-label">Datum</label>
                            <input id="kUI_datetimepicker_basic" name="datum" value="{{$regel->datum}}" required/>
                        </div>
                        <div class="uk-width-medium">
                            <label>Omschrijving</label>
                            <input type="text" required class="md-input label-fixed" step="1" name="omschrijving" value="{{$regel->omschrijving}}" required/>
                        </div>
                        <input type="hidden" name="klant_id" value="{{$relatie->id}}"/>
                        <input type="hidden" name="bijgewerkt_door" value="{{Auth::user()->id}}" />
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
            	<div class="uk-form-row">
		            <div align="right">
			            	<div class="md-btn-group">
				            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
				                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Wijzig Geschiedenisregel</button>
			            	</div>
			            </div>
		        	</div>
		        </div>
		    </div>
		</div>
    </div>
	{!! Form::close() !!}

@endif

@endsection

@push('scripts')
    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>
@endpush