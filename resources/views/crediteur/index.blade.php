@extends('layouts.master')

@section('title', 'Relatie Overzicht')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@section('content')


@if(Auth::user()->rol != 99)

<h4 class="heading_a uk-margin-bottom">Relatie Overzicht</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
            	<thead>
            		<tr>
                		<th>Naam</th>
                		<th>Adres</th>
                		<th>Contactgegevens</th>
                        <th>Bankrekening</th>
                		<th>Bedrag</th>
                		<th>Betaaldatum</th>
                		<th>Aangemaakt door</th>
                        <th>Omschrijving</th>
                        <th>Betalen</th>
            		</tr>
            	</thead>
			    <tbody>
                    @foreach($relaties as $relatie)
                        <tr>
                            <td><a href="/relatie/{{$relatie->klant->id}}">{{$relatie->klant->voornaam}} {{$relatie->klant->achternaam}}</a></td>
                            <td><a href="/relatie/{{$relatie->klant->id}}">{{$relatie->klant->straat}} {{$relatie->klant->huisnummer}} {{$relatie->klant->hn_prefix}}, <br> 
                                {{$relatie->klant->postcode}} {{$relatie->klant->woonplaats}}</a></td>
                            <td><a href="/relatie/{{$relatie->klant->id}}">{{$relatie->klant->telefoonnummer_prive}} || {{$relatie->klant->telefoonnummer_zakelijk}} <br>{{$relatie->klant->email}} </a></td>
                            <td>{{$relatie->klant->bankrekening}}</td>
                            <td><a href="/relatie/{{$relatie->klant->id}}">€ {{number_format($relatie->bedrag,2)}}</a></td>
                            <td>@if($relatie->betaaldatum > date('d-m-Y')) <a href="/relatie/{{$relatie->klant->id}}"> @else <a href="/relatie/{{$relatie->id}}" class="uk-text-danger"> @endif{{$relatie->betaaldatum}}</a></td>
                            <td><a href="/relatie/{{$relatie->klant->id}}">@foreach($users as $user) @if($user->id == $relatie->users_id) {{$user->name}} @endif @endforeach</a></td>    
                            <td><a href="/relatie/{{$relatie->klant->id}}">{{$relatie->omschrijving}}</a></td>
                            <td>@if($relatie->terugbetaald == 0)<a href="/crediteur/{{$relatie->klant->id}}/{{$relatie->bedrag}}/betaald" class="md-btn md-btn-success md-btn-wave-light">Betaald</a>@else Relatie al betaald.@endif</td>
                        </tr>
                    @endforeach
			    </tbody>
			</table>
		</div>
	</div>
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-3">
        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">
            <button class="md-btn">Zoek op status <i class="material-icons">&#xE315;</i></button>
            <div class="uk-dropdown">
                <ul class="uk-nav uk-nav-dropdown">
                    <li><a href="/crediteur/niet-betaald">Niet Betaald</a></li>
                    <li><a href="/crediteur/betaald">Betaald</a></li>
                    <li><a href="/crediteur">Totaal overzicht</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@endif
@endsection