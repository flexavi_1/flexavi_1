@if(session()->has('primary'))
    <div class="row mb-3">
        <div class="col-xxl-12 col-xl-12">
            <div class="alert alert-solid-primary alert-dismissible fade show">
                {{session('primary')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"><i class="bi bi-x"></i></button>
            </div>
        </div>
    </div>
@endif
@if(session()->has('secondary'))
    <div class="row mb-3">
        <div class="col-xxl-12 col-xl-12">
            <div class="alert alert-solid-secondary alert-dismissible fade show">
                {{session('secondary')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"><i class="bi bi-x"></i></button>
            </div>
        </div>
    </div>
@endif
@if(session()->has('info'))
    <div class="row mb-3">
        <div class="col-xxl-12 col-xl-12">
            <div class="alert alert-solid-info alert-dismissible fade show">
                {{session('info')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"><i class="bi bi-x"></i></button>
            </div>
        </div>
    </div>
@endif
@if(session()->has('warning'))
    <div class="row mb-3">
        <div class="col-xxl-12 col-xl-12">
            <div class="alert alert-solid-warning alert-dismissible fade show">
                {{session('warning')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"><i class="bi bi-x"></i></button>
            </div>
        </div>
    </div>
@endif
@if(session()->has('success'))
    <div class="row mb-3">
        <div class="col-xxl-12 col-xl-12">
            <div class="alert alert-solid-success alert-dismissible fade show">
                {{session('success')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"><i class="bi bi-x"></i></button>
            </div>
        </div>
    </div>
@endif
@if(session()->has('danger'))
    <div class="row mb-3">
        <div class="col-xxl-12 col-xl-12">
            <div class="alert alert-solid-danger alert-dismissible fade show">
                {{session('danger')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"><i class="bi bi-x"></i></button>
            </div>
        </div>
    </div>
@endif
@if(session()->has('light'))
    <div class="row mb-3">
        <div class="col-xxl-12 col-xl-12">
            <div class="alert alert-solid-light alert-dismissible fade show">
                {{session('light')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"><i class="bi bi-x"></i></button>
            </div>
        </div>
    </div>
@endif
@if(session()->has('dark'))
    <div class="row mb-3">
        <div class="col-xxl-12 col-xl-12">
            <div class="alert alert-solid-dark alert-dismissible fade show">
                {{session('dark')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"><i class="bi bi-x"></i></button>
            </div>
        </div>
    </div>
@endif
