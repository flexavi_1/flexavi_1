<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="nl"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="nl"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="{{ URL::asset('assets/image/png') }}"
        href="{{ URL::asset('assets/assets/icons/flexavi_favicon.jpeg') }}" sizes="16x16">
    <link rel="icon" type="{{ URL::asset('assets/image/png') }}"
        href="{{ URL::asset('assets/assets/icons/flexavi_favicon.jpeg') }}" sizes="32x32">

    <title>@yield('title') | Flexavi</title>

    <!-- additional styles for plugins -->
    <!-- weather icons -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/weather-icons/css/weather-icons.min.css') }}"
        media="all">
    <!-- metrics graphics (charts) -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/metrics-graphics/dist/metricsgraphics.css') }}">
    <!-- chartist -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/chartist/dist/chartist.min.css') }}">

    <!-- uikit -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/uikit/css/uikit.almost-flat.min.css') }}"
        media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/icons/flags/flags.min.css') }}" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/css/style_switcher.min.css') }}" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/css/main.min.css') }}" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="{{ URL::asset('assets/assets/css/themes/themes_combined.min.css') }}" media="all">


    <!-- kendo UI -->
    <link rel="stylesheet"
        href="{{ URL::asset('assets/bower_components/kendo-ui/styles/kendo.common-material.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/bower_components/kendo-ui/styles/kendo.material.min.css') }}"
        id="kendoCSS" />

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
        <link rel="stylesheet" href="assets/css/ie.css" media="all">
    <![endif]-->

</head>
<?php
$bedrijf = App\Models\Bedrijf::find(1);
?>

<body class=" sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <header id="header_main" style="background-color:grey;">
        <div class="header_main_content">
            <nav class="uk-navbar" style="background-color:grey;">

                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left" style"background-color:grey;">
                    <span class="sSwitchIcon"></span>
                </a>
                {{--
                <!-- secondary sidebar switch -->
                <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>

                    <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                        <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                            <a href="#" class="top_menu_toggle"><i class="material-icons md-24">&#xE8F0;</i></a>
                            <div class="uk-dropdown uk-dropdown-width-3">
                                <div class="uk-grid uk-dropdown-grid">
                                    <div class="uk-width-2-3">
                                        <div class="uk-grid uk-grid-width-medium-1-3 uk-margin-bottom uk-text-center">
                                            <a href="page_mailbox.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-light-green-600">&#xE158;</i>
                                                <span class="uk-text-muted uk-display-block">Mailbox</span>
                                            </a>
                                            <a href="page_invoices.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-purple-600">&#xE53E;</i>
                                                <span class="uk-text-muted uk-display-block">Invoices</span>
                                            </a>
                                            <a href="page_chat.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-cyan-600">&#xE0B9;</i>
                                                <span class="uk-text-muted uk-display-block">Chat</span>
                                            </a>
                                            <a href="page_scrum_board.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-red-600">&#xE85C;</i>
                                                <span class="uk-text-muted uk-display-block">Scrum Board</span>
                                            </a>
                                            <a href="page_snippets.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-blue-600">&#xE86F;</i>
                                                <span class="uk-text-muted uk-display-block">Snippets</span>
                                            </a>
                                            <a href="page_user_profile.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-orange-600">&#xE87C;</i>
                                                <span class="uk-text-muted uk-display-block">User profile</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="uk-width-1-3">
                                        <ul class="uk-nav uk-nav-dropdown uk-panel">
                                            <li class="uk-nav-header">Components</li>
                                            <li><a href="components_accordion.html">Accordions</a></li>
                                            <li><a href="components_buttons.html">Buttons</a></li>
                                            <li><a href="components_notifications.html">Notifications</a></li>
                                            <li><a href="components_sortable.html">Sortable</a></li>
                                            <li><a href="components_tabs.html">Tabs</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 --}}
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i
                                    class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                        <li><a href="#" id="main_search_btn" class="user_action_icon"><i
                                    class="material-icons md-24 md-light">&#xE8B6;</i></a></li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_icon"><i
                                    class="material-icons md-24 md-light">&#xE7F4;</i><!-- <span class="uk-badge">16</span> --></a>
                            <div class="uk-dropdown uk-dropdown-xlarge">
                                <div class="md-card-content">
                                    <ul class="uk-tab uk-tab-grid"
                                        data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                        <li class="uk-width-1-2 uk-active"><a href="#"
                                                class="js-uk-prevent uk-text-small">Aanvullingen</a></li>
                                        <li class="uk-width-1-2"><a href="#"
                                                class="js-uk-prevent uk-text-small">Targets</a></li>
                                    </ul>
                                    <ul id="header_alerts" class="uk-switcher uk-margin">
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                {{-- <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-cyan">jj</span>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="pages_mailbox.html">Nostrum explicabo.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Enim voluptatibus iste illum doloremque rerum iure asperiores quasi ut.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_07_tn.png" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="pages_mailbox.html">Voluptatum dolore.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Fuga rerum autem nihil atque ea dignissimos blanditiis debitis sed est.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-light-green">wy</span>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="pages_mailbox.html">Velit nulla.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Non consequatur ut possimus voluptatum minus doloremque excepturi ex.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_02_tn.png" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="pages_mailbox.html">Eligendi nihil.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Porro illum labore sint vel corrupti modi quidem.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_09_tn.png" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="pages_mailbox.html">Sunt est occaecati.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Culpa non debitis saepe in eaque voluptas molestiae voluptatibus sint.</span>
                                                    </div>
                                                </li> --}}
                                            </ul>
                                            <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="#"
                                                    class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Toon
                                                    alles</a>
                                            </div>
                                        </li>
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                {{--  <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Nihil fuga earum.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Amet rerum voluptatem incidunt cum voluptas occaecati.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Occaecati a.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Quod rem deserunt porro et rerum fugiat possimus.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Distinctio suscipit neque.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Deserunt ratione qui soluta recusandae neque ratione.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Natus expedita.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Nobis tempora sed vitae illum repellat.</span>
                                                    </div>
                                                </li> --}}
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image"><img class="md-user-image"
                                    src="{{ URL::asset('assets/assets/img/avatars/avatar_11_tn.png') }}"
                                    alt="" /></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                    {{-- <li><a href="#">Mijn Profiel</a></li>
                                    <li><a href="#">Instellingen</a></li> --}}
                                    <li>
                                        <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                        class="dropdown-item">
                                        <span class="align-middle">Uitloggen</span>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            @csrf
                                        </form>
                                    </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            {!! Form::open(['url' => '/zoeken', 'method' => 'post']) !!}
            <input type="text" name="zoeken" class="header_main_search_input" />
            <button class="header_main_search_btn uk-button-link"><i
                    class="md-icon material-icons">&#xE8B6;</i></button>
            {!! Form::close() !!}
        </div>
    </header><!-- main header end -->
    <!-- main sidebar -->
    <aside id="sidebar_main" style="background-color:grey;">

        <div class="sidebar_main_header" style="background-color:grey;">
            <div class="">
                <a href="/index" class="sSidebar_hide sidebar_logo_large">
                    <img class="" src="{{ URL::asset('assets/assets/icons/Logo-Flexavi-Horizontaal.png') }}"
                        alt="" style="margin-top: 4px;" /> {{-- style="margin-top: -10px; margin-left: 9px;" --}}
                </a>
                <a href="/index" class="sSidebar_show sidebar_logo_small">
                    <img class="" src="{{ URL::asset('assets/assets/icons/Logo-Flexavi-Horizontaal.png') }}"
                        alt="" />
                </a>
            </div>
        </div>

        <div class="menu_section" style="background-color:grey;">
            @if (Auth::user()->rol != 99)
                @if (Auth::user()->rol < 7)

                    <ul>
                        <li @if ($page == 'dashboard') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                            title="Dashboard">
                            <a href="/index"
                                @if ($page == 'dashboard') style="color:lightblue;" @else style="color:white;" @endif>
                                <span class="menu_icon" style="color:white;"><i class="material-icons"
                                        style="color:white;">&#xE871;</i></span>
                                <span class="menu_title">Dashboard</span>
                            </a>
                        </li>
                        <li @if ($page == 'relatie') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                            title="Relatie">
                            <a href=""
                                @if ($page == 'relatie') style="color:lightblue;" @else style="color:white;" @endif>
                                <span class="menu_icon" style="color:white;"><i class="material-icons"
                                        style="color:white;">person</i></span>
                                <span class="menu_title">Relatie</span>
                            </a>
                            <ul>
                                <li @if ($sub == 'relatoev') class="act_item" @endif><a
                                        href="/relatie/nieuw">Relatie Toevoegen</a></li>
                                <li @if ($sub == 'relaover') class="act_item" @endif><a
                                        href="/relatie">Relatie Overzicht</a></li>
                            </ul>
                        </li>

                        <li @if ($page == 'financieel') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                            title="Financieel">
                            <a href="">
                                <span class="menu_icon" style="color:white;"><i class="material-icons"
                                        style="color:white;">euro_symbol</i></span>
                                <span class="menu_title">Financieel</span>
                            </a>
                            <ul>
                                <li>
                                    <a href="" style="color:white;">Debiteur</a>
                                    <ul>
                                        <li @if ($sub == 'debioverzicht') class="act_item" @endif
                                            style="color:white;"><a href="/debiteur">Overzicht</a></li>
                                        <li @if ($sub == 'open') class="act_item" @endif
                                            style="color:white;"><a href="/debiteur/open">Openstaande Debiteuren</a>
                                        </li>
                                        <li @if ($sub == 'vijftien') class="act_item" @endif
                                            style="color:white;"><a href="/vijftieneuro">€15,- lijst</a></li>
                                        <!-- <li><a href="/debiteur/factuur/nieuw">Factuur maken</a></li> -->
                                    </ul>
                                </li>
                                @if ($bedrijf->naam != 'Huis Beheer Nederland B.V.')
                                    <li @if ($sub == 'crediteur') class="act_item" @endif
                                        style="color:white;">
                                        <a href="/crediteur">Crediteur</a>
                                    </li>
                                @endif
                                <li @if ($sub == 'offerte') class="act_item" @endif style="color:white;">
                                    <a href="/offerte">Offerte</a>
                                    <!-- <ul> -->
                                    <!-- <li><a href="/offerte">Overzicht</a></li> -->
                                    <!-- <li><a href="/offerte/nieuw">Offerte maken</a></li> -->
                                    <!-- <li><a href="/offerte/verval/{{ date('Y-m-d') }}">Offertes vandaag verlopen</a></li> -->
                                    <!-- </ul> -->
                                </li>
                                <li @if ($sub == 'openstaande_afspraken') class="act_item" @endif style="color:white;">
                                    <a href="/afspraak/openstaande-betalingen">Openstaande Betaalafspraken</a>
                                </li>
                                @if ($bedrijf->naam != 'Huis Beheer Nederland B.V.')
                                    <li @if ($sub == 'ublijst') class="act_item" @endif><a
                                            href="/ublijst">Uitbetaallijst</a></li>
                                    <li @if ($sub == 'rapportage') class="act_item" @endif><a
                                            href="/opvragen-rapportage">Opvragen rapportage</a></li>
                                    {{-- <li @if ($sub == 'rapportage') class="act_item" @endif><a href="/financiele-rapportage">*Nog in behandeling* Opvragen financiële rapportage</a></li> --}}
                                @endif
                            </ul>
                        </li>
                        <li @if ($page == 'werving') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                            title="Telefonische Werving">
                            <a href="">
                                <span class="menu_icon" style="color:white;"><i class="material-icons"
                                        style="color:white;">library_add</i></span>
                                <span class="menu_title">Werving</span>
                            </a>
                            <ul>
                                <li @if ($sub == 'reminder') class="act_item" @endif style="color:white;"><a
                                        href="/reminder">Reminder</a></li> <!-- Reminder                     -->
                                <!-- <li><a href="/lead">Lead</a></li>     <!-- Vrijblijvende afspraken              -->
                                <li @if ($sub == 'tblijst') class="act_item" @endif style="color:white;">
                                    <a href="/terugbellijst">Terugbellijst</a>
                                    <!-- <ul> -->
                                    <!-- <li><a href="/klant/offerte/lopend">Lopende Offertes</a></li> -->
                                    <!-- <li><a href="/terugbellijst">Terug bellijst</a></li> -->
                                    <!-- </ul> -->
                                </li> <!-- Lopende Offertes, terug bellijst (mislukte afspraken)-->
                            </ul>
                        </li>

                        <li @if ($page == 'activiteit') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                            title="activiteit">
                            <a href="">
                                <span class="menu_icon" style="color:white;"><i class="material-icons"
                                        style="color:white;">build</i></span>
                                @if ($bedrijf->naam != 'Huis Beheer Nederland B.V.')
                                    <span class="menu_title">Materialen</span>
                                @else
                                    <span class="menu_title">Werkzaamheden</span>
                                @endif
                            </a>
                            <ul>
                                <li @if ($sub == 'acttoev') class="act_item" @endif style="color:white;"><a
                                        href="/activiteit/nieuw">Werkactiviteit Toevoegen</a></li>
                                <li @if ($sub == 'actover') class="act_item" @endif style="color:white;"><a
                                        href="/activiteit">Overzicht Werkactiviteiten</a></li>
                                @if ($bedrijf->naam != 'Huis Beheer Nederland B.V.')
                                    <li @if ($sub == 'mattoev') class="act_item" @endif
                                        style="color:white;"><a href="/materiaal/nieuw">Materiaal Toevoegen</a></li>
                                    <li @if ($sub == 'matover') class="act_item" @endif
                                        style="color:white;"><a href="/materiaal">Materiaal Overzicht</a></li>
                                @endif
                            </ul>
                        </li>
                        @if (Auth::user()->rol < 6)
                            <li @if ($page == 'controle') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                                title="controle">
                                <a href="">
                                    <span class="menu_icon" style="color:white;"><i class="material-icons"
                                            style="color:white;">spellcheck</i></span>
                                    <span class="menu_title">Controle</span>
                                </a>
                                <ul>
                                    <li @if ($sub == 'afwl') class="act_item" @endif
                                        style="color:white;"><a href="/controle/werklijst">Afgeronde werklijsten</a>
                                    </li>
                                    <li @if ($sub == 'log') class="act_item" @endif
                                        style="color:white;"><a href="/controle/logbeheer">Logbeheer</a></li>
                                    <li @if ($sub == 'gebr') class="act_item" @endif
                                        style="color:white;"><a href="/controle/gebruikers">Gebruikers</a></li>
                                    <li @if ($sub == 'aanv') class="act_item" @endif
                                        style="color:white;"><a href="/controle/aanvulling">Aanvulling</a></li>
                                    <li @if ($sub == 'verwij') class="act_item" @endif
                                        style="color:white;"><a href="/controle/verwijderde-relaties">Verwijderde
                                            relaties</a></li>
                                </ul>
                            </li>
                        @endif
                        <li @if ($page == 'planning') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                            title="planning">
                            <a href="">
                                <span class="menu_icon" style="color:white;"><i class="material-icons"
                                        style="color:white;">event</i></span>
                                <span class="menu_title">Planning</span>
                            </a>
                            <ul>
                                <li>
                                    <a href="" style="color:white;">Verkoop</a>
                                    <ul>
                                        <li @if ($sub == 'planverk') class="act_item" @endif
                                            style="color:white;"><a href="/planning/verkoop">Planning Verkoop</a></li>
                                        <li @if ($sub == 'verkvand') class="act_item" @endif
                                            style="color:white;"><a
                                                href="/planning/verkoop/{{ date('d-m-Y') }}">Verkoop Vandaag</a></li>
                                    </ul>
                                </li>
                                @if ($bedrijf->naam != 'Huis Beheer Nederland B.V.')
                                    <li>
                                        <a href="" style="color:white;">Werving</a>
                                        <ul>
                                            <li @if ($sub == 'telling') class="act_item" @endif
                                                style="color:white;"><a href="/planning/werving/telling">Telling</a>
                                            </li>
                                            <li @if ($sub == 'wervvand') class="act_item" @endif
                                                style="color:white;"><a
                                                    href="/planning/werving/dag/{{ date('d-m-Y') }}">Werving
                                                    Vandaag</a></li>
                                            <li @if ($sub == 'indelen') class="act_item" @endif
                                                style="color:white;"><a href="/planning/werving/koppel">Klantenwervers
                                                    indelen</a></li>
                                            {{-- <li @if ($sub == 'betaling') class="act_item" @endif style="color:white;"><a style="color:white;" href="/planning/werving/ub">Uitbetaallijst Werving</a></li> --}}
                                        </ul>
                                    </li>
                                    <li @if ($sub == 'kantvand') class="act_item" @endif
                                        style="color:white;"><a href="/planning/kantoor/{{ date('d-m-Y') }}">Kantoor
                                            Vandaag</a></li>
                                @endif
                                {{-- <li @if ($sub == 'klantprog') class="act_item" @endif style="color:white;"><a href="/planning/prognose">*Mee bezig*Prognose</a></li> --}}
                                <!-- <li><a href="/planning/werkdag/aanmaken">Werkdag Aanmaken</a></li> -->
                            </ul>
                        </li>
                        <li @if ($page == 'aanvulling') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                            title="Aanvulling">
                            <a href="">
                                <span class="menu_icon" style="color:white;"><i class="material-icons"
                                        style="color:white;">chat</i></span>
                                <span class="menu_title">Aanvulling</span>
                            </a>
                            <ul>
                                <li @if ($sub == 'aanvtoev') class="act_item" @endif style="color:white;"><a
                                        href="/aanvulling/toevoegen">Maak Aanvulling</a></li>
                                <li @if ($sub == 'aanvover') class="act_item" @endif style="color:white;"><a
                                        href="/aanvulling">Aanvulling Overzicht</a></li>
                                @if (Auth::user()->rol < 6)
                                    <li @if ($sub == 'aanvwerk') class="act_item" @endif
                                        style="color:white;"><a href="/aanvulling/werknemer/overzicht">Aanvulling
                                            andere werknemer</a></li>
                                @endif
                            </ul>
                        </li>
                        <li @if ($page == 'werknemers') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                            title="Werknemers">
                            <a href="">
                                <span class="menu_icon" style="color:white;"><i class="material-icons"
                                        style="color:white;">supervisor_account</i></span>
                                <span class="menu_title">Werknemers</span>
                            </a>
                            <ul>
                                <li @if ($sub == 'werktoev') class="act_item" @endif style="color:white;"><a
                                        href="/werknemer/nieuw">Werknemer toevoegen</a></li>
                                <li @if ($sub == 'werkover') class="act_item" @endif style="color:white;"><a
                                        href="/werknemer">Werknemers</a></li>
                                @if (Auth::user()->rol < 7)
                                    <li @if ($sub == 'werkkoppel') class="act_item" @endif
                                        style="color:white;"><a href="/werknemer/gebruiker/koppelen">Werknemer
                                            koppelen</a></li>
                                @endif
                            </ul>
                        </li>
                        @if ($bedrijf->naam != 'Huis Beheer Nederland B.V.')
                            <li @if ($page == 'voorraad') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                                title="Voorraad">
                                <a href="">
                                    <span class="menu_icon" style="color:white;"><i class="material-icons"
                                            style="color:white;">account_balance_wallet</i></span>
                                    <span class="menu_title">Inkoop</span>
                                </a>
                                <ul>
                                    <li @if ($sub == 'voorraad') class="act_item" @endif
                                        style="color:white;"><a href="/voorraad/">Huidige Voorraad</a></li>
                                    <li @if ($sub == 'inkverz') class="act_item" @endif
                                        style="color:white;"><a href="/voorraad/plaats-verzoek">Plaats
                                            inkoopverzoek</a></li>
                                    <li @if ($sub == 'verzover') class="act_item" @endif
                                        style="color:white;"><a href="/voorraad/verzoek">Inkoopverzoeken</a></li>
                                    <li @if ($sub == 'leveran') class="act_item" @endif
                                        style="color:white;"><a href="/leveranciers">Leveranciers</a></li>
                                </ul>
                            </li>
                        @endif
                        @if ($bedrijf->naam != 'Huis Beheer Nederland B.V.')
                            <li @if ($page == 'functie') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                                title="Functies">
                                <a href="/functie">
                                    <span class="menu_icon" style="color:white;"><i class="material-icons"
                                            style="color:white;">content_copy</i></span>
                                    <span class="menu_title">Functies</span>
                                </a>
                            </li>
                        @endif
                        <li @if ($page == 'wagenpark') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                            title="Wagenpark">
                            <a href="">
                                <span class="menu_icon" style="color:white;"><i class="material-icons"
                                        style="color:white;">directions_car</i></span>
                                <span class="menu_title">Wagenpark</span>
                            </a>
                            <ul>
                                <li @if ($sub == 'wagtoev') class="act_item" @endif style="color:white;"><a
                                        href="/wagenpark/nieuw">Voertuig toevoegen</a></li>
                                <li @if ($sub == 'wagover') class="act_item" @endif style="color:white;"><a
                                        href="/wagenpark">Wagenpark</a></li>
                                <li @if ($sub == 'waggesch') class="act_item" @endif style="color:white;"><a
                                        href="/wagenpark/voeg-geschiedenis-toe">Voeg geschiedenis toe</a></li>
                                <li @if ($sub == 'autolijst') class="act_item" @endif style="color:white;"><a
                                        href="/wagenpark/autolijst">Autolijst</a></li>
                            </ul>
                        </li>
                        @if ($bedrijf->naam != 'Huis Beheer Nederland B.V.')
                            <li @if ($page == 'urenregistratie') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                                title="urenregistratie">
                                <a href="">
                                    <span class="menu_icon" style="color:white;"><i class="material-icons"
                                            style="color:white;">hourglass_empty</i></span>
                                    <span class="menu_title">Uren Registratie</span>
                                </a>
                                <ul>
                                    <li @if ($sub == 'urentoev') class="act_item" @endif
                                        style="color:white;"><a href="/uren/nieuw">Uren registreren</a></li>
                                    <li @if ($sub == 'urenover') class="act_item" @endif
                                        style="color:white;"><a href="/uren">Geregistreerde Uren inzien</a></li>
                                    <li @if ($sub == 'verloftoev') class="act_item" @endif
                                        style="color:white;"><a href="/verlof/nieuw">Verlof aanvragen</a></li>
                                    <li @if ($sub == 'verlofinz') class="act_item" @endif
                                        style="color:white;"><a href="/verlof">Verlof inzien</a></li>
                                </ul>
                            </li>
                        @endif
                        <li @if ($page == 'tickets') class="current_section" style="color:lightblue;" @else style="color:white;" @endif
                            title="Tickets">
                            <a href="">
                                <span class="menu_icon" style="color:white;"><i style="color:white;"
                                        class="material-icons">&#xE158;</i></span>
                                <span class="menu_title">Tickets</span>
                            </a>
                            <ul>
                                <li @if ($sub == 'ticktoev') class="act_item" @endif style="color:white;"><a
                                        href="/ticket/nieuw">Ticket Aanmaken</a></li>
                                <li @if ($sub == 'tickover') class="act_item" @endif style="color:white;"><a
                                        href="/ticket">Ticket Overzicht</a></li>
                            </ul>
                        </li>
                    </ul>
                @elseif(Auth::user()->rol == 8)
                    <ul>
                        <li @if ($page == 'dashboard') class="current_section" @endif title="Dashboard">
                            <a href="/index">
                                <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                                <span class="menu_title">Dashboard</span>
                            </a>
                        </li>
                        {{-- <li @if ($page == 'relatie') class="current_section" @endif title="Relatie">
                        <a href="">
                            <span class="menu_icon"><i class="material-icons">person</i></span>
                            <span class="menu_title">Relatie</span>
                        </a>
                        <ul>
                            <li @if ($sub == 'relatoev') class="act_item" @endif><a href="/relatie/nieuw">Relatie Toevoegen</a></li>
                            <li @if ($sub == 'relaover') class="act_item" @endif><a href="/relatie">Relatie Overzicht</a></li>
                        </ul>
                    </li> --}}
                        <li @if ($page == 'planning') class="current_section" @endif title="Planning">
                            <a href="#">
                                <span class="menu_icon"><i class="material-icons">event</i></span>
                                <span class="menu_title">Planning</span>
                            </a>
                            <ul>
                                <li @if ($sub == 'planverk') class="act_item" @endif><a
                                        href="/planning/verkoop">Vorige werklijsten</a></li>
                                <li @if ($sub == 'verkvand') class="act_item" @endif><a
                                        href="/planning/verkoop/{{ date('d-m-Y') }}/{{ Auth::user()->id }}">Werklijst
                                        vandaag</a></li>
                            </ul>
                        </li>
                        @if ($bedrijf->naam != 'Huis Beheer Nederland B.V.')
                            <li @if ($page == 'voorraad') class="current_section" @endif title="Voorraad">
                                <a href="">
                                    <span class="menu_icon"><i
                                            class="material-icons">account_balance_wallet</i></span>
                                    <span class="menu_title">Inkoop</span>
                                </a>
                                <ul>
                                    <li @if ($sub == 'inkverz') class="act_item" @endif><a
                                            href="/voorraad/plaats-verzoek">Plaats inkoopverzoek</a></li>
                                    <li @if ($sub == 'verzover') class="act_item" @endif><a
                                            href="/voorraad/verzoek">Inkoopverzoeken</a></li>
                                    <li @if ($sub == 'leveran') class="act_item" @endif><a
                                            href="/leveranciers">Leveranciers</a></li>
                                </ul>
                            </li>
                        @endif
                        <li @if ($page == 'activiteit') class="current_section" @endif title="activiteit">
                            <a href="/activiteit">
                                <span class="menu_icon"><i class="material-icons">build</i></span>
                                <span class="menu_title">Prijslijst</span>
                            </a>
                        </li>
                        @if ($bedrijf->naam != 'Huis Beheer Nederland B.V.')
                            <li @if ($page == 'urenregistratie') class="current_section" @endif
                                title="urenregistratie">
                                <a href="">
                                    <span class="menu_icon"><i class="material-icons">hourglass_empty</i></span>
                                    <span class="menu_title">Uren Registratie</span>
                                </a>
                                <ul>
                                    <li @if ($sub == 'urentoev') class="act_item" @endif><a
                                            href="/uren/nieuw">Uren registreren</a></li>
                                    <li @if ($sub == 'urenover') class="act_item" @endif><a
                                            href="/uren">Geregistreerde Uren inzien</a></li>
                                    <li @if ($sub == 'verloftoev') class="act_item" @endif><a
                                            href="/verlof/nieuw">Verlof aanvragen</a></li>
                                    <li @if ($sub == 'verlofinz') class="act_item" @endif><a
                                            href="/verlof">Verlof inzien</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                @elseif(Auth::user()->rol == 7)

                @elseif(Auth::user()->rol == 10)
                @endif
            @else
                <ul>
                    <li @if ($sub == 'index') class="act_item" @endif><a href="/index">Index</a></li>
                </ul>
            @endif
        </div>
    </aside><!-- main sidebar end -->

    <div id="page_content" class="uk-height-1-1">
        <div id="page_content_inner">
            @if (session('succes'))
                <div class="uk-alert md-bg-light-green-400" data-uk-alert>
                    <a href="#" class="uk-alert-close uk-close"></a>
                    <h3>
                        {!! session('succes') !!}
                    </h3>
                </div>
            @endif
            @if (session('danger'))
                <div class="uk-alert md-bg-red-400 " data-uk-alert>
                    <a href="#" class="uk-alert-close uk-close"></a>
                    <h3>
                        {!! session('danger') !!}
                    </h3>
                </div>
            @endif

            @yield('content')

        </div>
    </div>



    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- common functions -->
    <script src="{{ URL::asset('assets/assets/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ URL::asset('assets/assets/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ URL::asset('assets/assets/js/altair_admin_common.min.js') }}"></script>

    @stack('scripts')

    <script>
        $(function() {
            if (isHighDensity()) {
                $.getScript("assets/bower_components/dense/src/dense.js", function() {
                    // enable hires images
                    altair_helpers.retina_images();
                })
            }
            if (Modernizr.touch) {
                // fastClick (touch devices)
                FastClick.attach(document.body);
            }
        });
        $window.load(function() {
            // ie fixes
            altair_helpers.ie_fix();
        });
    </script>

    <script>
        //     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        //             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        //         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        //     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        //     ga('create', 'UA-65191727-1', 'auto');
        //     ga('send', 'pageview');
        //
    </script>
    {{--
    <div id="style_switcher">
        <div id="style_switcher_toggle"><i class="material-icons">&#xE8B8;</i></div>
        <div class="uk-margin-medium-bottom">
            <h4 class="heading_c uk-margin-bottom">Colors</h4>
            <ul class="switcher_app_themes" id="theme_switcher">
                <li class="app_style_default active_theme" data-app-theme="">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_a" data-app-theme="app_theme_a">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_b" data-app-theme="app_theme_b">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_c" data-app-theme="app_theme_c">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_d" data-app-theme="app_theme_d">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_e" data-app-theme="app_theme_e">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_f" data-app-theme="app_theme_f">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_g" data-app-theme="app_theme_g">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_h" data-app-theme="app_theme_h">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_i" data-app-theme="app_theme_i">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_dark" data-app-theme="app_theme_dark">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
            </ul>
        </div>
        <div class="uk-visible-large uk-margin-medium-bottom">
            <h4 class="heading_c">Sidebar</h4>
            <p>
                <input type="checkbox" name="style_sidebar_mini" id="style_sidebar_mini" data-md-icheck />
                <label for="style_sidebar_mini" class="inline-label">Mini Sidebar</label>
            </p>
            <p>
                <input type="checkbox" name="style_sidebar_slim" id="style_sidebar_slim" data-md-icheck />
                <label for="style_sidebar_slim" class="inline-label">Slim Sidebar</label>
            </p>
        </div>
        <div class="uk-visible-large uk-margin-medium-bottom">
            <h4 class="heading_c">Layout</h4>
            <p>
                <input type="checkbox" name="style_layout_boxed" id="style_layout_boxed" data-md-icheck />
                <label for="style_layout_boxed" class="inline-label">Boxed layout</label>
            </p>
        </div>
        <div class="uk-visible-large">
            <h4 class="heading_c">Main menu accordion</h4>
            <p>
                <input type="checkbox" name="accordion_mode_main_menu" id="accordion_mode_main_menu" data-md-icheck />
                <label for="accordion_mode_main_menu" class="inline-label">Accordion mode</label>
            </p>
        </div>
    </div>
 --}}
    <script>
        $(function() {
            var $switcher = $('#style_switcher'),
                $switcher_toggle = $('#style_switcher_toggle'),
                $theme_switcher = $('#theme_switcher'),
                $mini_sidebar_toggle = $('#style_sidebar_mini'),
                $slim_sidebar_toggle = $('#style_sidebar_slim'),
                $boxed_layout_toggle = $('#style_layout_boxed'),
                $accordion_mode_toggle = $('#accordion_mode_main_menu'),
                $html = $('html'),
                $body = $('body');


            $switcher_toggle.click(function(e) {
                e.preventDefault();
                $switcher.toggleClass('switcher_active');
            });

            $theme_switcher.children('li').click(function(e) {
                e.preventDefault();
                var $this = $(this),
                    this_theme = $this.attr('data-app-theme');

                $theme_switcher.children('li').removeClass('active_theme');
                $(this).addClass('active_theme');
                $html
                    .removeClass(
                        'app_theme_a app_theme_b app_theme_c app_theme_d app_theme_e app_theme_f app_theme_g app_theme_h app_theme_i app_theme_dark'
                        )
                    .addClass(this_theme);

                if (this_theme == '') {
                    localStorage.removeItem('altair_theme');
                    $('#kendoCSS').attr('href',
                        "{{ URL::asset('assets/bower_components/kendo-ui/styles/kendo.material.min.css') }}"
                        );
                } else {
                    localStorage.setItem("altair_theme", this_theme);
                    if (this_theme == 'app_theme_dark') {
                        $('#kendoCSS').attr('href',
                            "{{ URL::asset('assets/bower_components/kendo-ui/styles/kendo.materialblack.min.css') }}"
                            )
                    } else {
                        $('#kendoCSS').attr('href',
                            "{{ URL::asset('assets/bower_components/kendo-ui/styles/kendo.material.min.css') }}"
                            );
                    }
                }

            });

            // hide style switcher
            $document.on('click keyup', function(e) {
                if ($switcher.hasClass('switcher_active')) {
                    if (
                        (!$(e.target).closest($switcher).length) ||
                        (e.keyCode == 27)
                    ) {
                        $switcher.removeClass('switcher_active');
                    }
                }
            });

            // get theme from local storage
            if (localStorage.getItem("altair_theme") !== null) {
                $theme_switcher.children('li[data-app-theme=' + localStorage.getItem("altair_theme") + ']').click();
            }


            // toggle mini sidebar

            // change input's state to checked if mini sidebar is active
            if ((localStorage.getItem("altair_sidebar_mini") !== null && localStorage.getItem(
                    "altair_sidebar_mini") == '1') || $body.hasClass('sidebar_mini')) {
                $mini_sidebar_toggle.iCheck('check');
            }

            $mini_sidebar_toggle
                .on('ifChecked', function(event) {
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_mini", '1');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event) {
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                });

            // toggle slim sidebar

            // change input's state to checked if mini sidebar is active
            if ((localStorage.getItem("altair_sidebar_slim") !== null && localStorage.getItem(
                    "altair_sidebar_slim") == '1') || $body.hasClass('sidebar_slim')) {
                $slim_sidebar_toggle.iCheck('check');
            }

            $slim_sidebar_toggle
                .on('ifChecked', function(event) {
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_slim", '1');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event) {
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                });

            // toggle boxed layout

            if ((localStorage.getItem("altair_layout") !== null && localStorage.getItem("altair_layout") ==
                'boxed') || $body.hasClass('boxed_layout')) {
                $boxed_layout_toggle.iCheck('check');
                $body.addClass('boxed_layout');
                $(window).resize();
            }

            $boxed_layout_toggle
                .on('ifChecked', function(event) {
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_layout", 'boxed');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event) {
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_layout');
                    location.reload(true);
                });

            // main menu accordion mode
            if ($sidebar_main.hasClass('accordion_mode')) {
                $accordion_mode_toggle.iCheck('check');
            }

            $accordion_mode_toggle
                .on('ifChecked', function() {
                    $sidebar_main.addClass('accordion_mode');
                })
                .on('ifUnchecked', function() {
                    $sidebar_main.removeClass('accordion_mode');
                });


        });
    </script>
</body>

</html>
