@extends('layouts.master')

@section('title', 'Flexavi - Wagenpark')
@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)


<h4 class="heading_a uk-margin-bottom">Tickets</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Onderwerp</th>
                        <th>Prioriteit</th>
                        <th>Plaats</th>
                        <th>Aangemaakt door</th>
                        <th>Toegevoegd op</th>
                        <th>Gewijzigd door</th>
                        <th>Gewijzigd op</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($tickets) > 0)
                    @foreach($tickets as $t)
                    <tr>
                        <td>{{$t->id}}</td>
                        <td>{{$t->onderwerp}}</td>
                        <td>{{$t->prioriteit}}</td>
                        <td>{{$t->waar}}</td>
                        <td>{{$t->toegevoegd()}}</td>
                        <td>{{date( 'd-m-Y' , strtotime( $t->created_at ) )}}</td>
                        <td>{{$t->bijgewerkt()}}</td>
                        <td>{{date( 'd-m-Y' , strtotime( $t->updated_at ) )}}</td>
                        <td>{{$t->getStatus()}}</td>
                        <td>{{-- <a href="/ticket/{{$t->id}}/wijzigen">Wijzigen</a> || --}} <a href="/ticket/{{$t->id}}/verwijderen">Verwijderen</a></td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endif

@endsection