@extends('layouts.master')

@section('title', 'Ticket Toevoegen')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
    <!-- inputmask-->
    <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <!--  forms advanced functions -->
    <script src="assets/js/pages/forms_advanced.min.js"></script>    <!-- kendo UI -->
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)

<div class="md-card">
    <div class="md-card-content">
        <h3 class="heading_a">Ticket Toevoegen</h3>
        {!! Form::open(array('url'=>'/ticket/nieuw', 'method' => 'post')) !!}
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3">
                           <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-bug"></i></span>
                                <label>Onderwerp</label>
                                <input type="text" class="md-input label-fixed" name="onderwerp" placeholder="Fout klant aanmaken" required/>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-3">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-bars"></i></span>
                                <label>Prioriteit</label>
                                <select id="select_demo_1" class="md-input" name="prioriteit">
                                   <option value="Laag">Laag</option>
                                   <option selected value="Middel">Middel</option>
                                   <option value="Hoog">Hoog</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-3">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-map-marker"></i></span>
                                <label>Plaats</label>
                                <input type="text" class="md-input label-fixed" name="waar" placeholder="Relatie Toevoegen" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-1">
                            <label>Omschrijving</label><br>
                            <textarea class="md-input label-fixed" id='editor' placeholder="Beschrijf hier gedetaileerd uw bevinding of vraag." name="bericht"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                    <div align="right">
                        <div class="md-btn-group">
                            <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                            <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Maak ticket aan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    {!! Form::close() !!}

@endif

@endsection