@extends('layouts.master')

@section('title', 'Vijftien euro lijst')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
    <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>


    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush
@section('content')

    @if(Auth::user()->rol != 99)

        <h1 class="heading_b uk-margin-bottom">Vijftien euro lijst</h1>

        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-3" align='center'>
                <a class="md-btn md-btn-primary md-btn-wave-light" target="_blank"
                   href="/vijftieneuro/toon/alle-brieven">Toon alle te versturen brieven</a>
            </div>
            <div class="uk-width-medium-1-3" align='center'>
                <a class="md-btn md-btn-primary md-btn-wave-light" href="/vijftieneuro/wijzig/brieven">Wijzig standaard
                    brieven</a>
            </div>
            <div class="uk-width-medium-1-3" align='center'>
                <a class="md-btn md-btn-primary md-btn-wave-light" href="#">What to put here?</a>
            </div>
        </div>
        <br><br>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="user_content">
                    <ul id="user_profile_tabs" class="uk-tab"
                        data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}"
                        data-uk-sticky="{ top: 48, media: 960 }">
                        <li class="uk-active">
                            <a href="#">Vijftien euro klanten</a>
                        </li>
                        <li><a href="#">Potentiële vijftien euro klanten</a></li>
                    </ul>
                    <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                        <li>
                            {{-- <div style="align:right;">
                                <a href="/vijftien/alle-lopende-brieven">Overzicht alle lopende brieven</a>
                            </div> --}}
                            {!! Form::open(array('url'=>'/vijftien/multiselect/verstuurd', 'method' => 'post', 'data-parsley-validate')) !!}
                            <div class="uk-overflow-container">
                                <div class="dt_colVis_buttons"></div>
                                <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Klant</th>
                                        <th>Afspraaktype</th>
                                        <th>Totaalbedrag</th>
                                        <th>Verwerkt door</th>
                                        <th>Huidige status</th>
                                        <th>Reden</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($vijftien as $v)
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="vft[]" value="{{$v->id}}" data-md-icheck/>
                                                <label name="name[]" class="inline-label"> </label>
                                                <input type="hidden" name="input[]" value="{{$v->id}}"/>
                                            </td>
                                            <td>
                                                <a href="/relatie/{{$v->afspraak->klant_id}}">Fam. {{App\Models\Client::findMe($v->afspraak->klant_id)->achternaam}}</a>
                                            </td>
                                            <td>{!! App\Models\AppointmentType::getDescription($v->afspraak->afspraaktype_id) !!}</td>
                                            <td>€ {{ number_format($v->afspraak->totaalbedrag,2) }}</td>
                                            <td>{{App\Models\User::getName($v->afspraak->verwerkt_door)}}</td>
                                            <td>{{ $v->status->omschrijving }}</td>
                                            <td>{{ $v->afspraak->reden }}</td>
                                            <td>
                                                <a href="/vijftieneuro/{{$v->id}}/show"
                                                   class="md-btn md-btn-primary md-btn-wave-light">Toon brief</a>
                                                @if($v->status_id == 35)
                                                    <a data-uk-modal="{target:'#modal_tempFirst_{{$v->afspraak_id}}_edit'}"
                                                       class="md-btn md-btn-warning md-btn-wave-light">Wijzig brief</a>
                                                    <br>
                                                @endif
                                                <a href="/vijftieneuro/{{$v->id}}/single/send"
                                                   class="md-btn md-btn-success md-btn-wave-light">Verstuur brief</a>
                                                <a href="/veranderEigenschuld/{{$v->afspraak->id}}/vijftieneurolijst"
                                                   class="md-btn md-btn-danger md-btn-wave-light">Geen vijftieneuro</a>
                                            </td>
                                        </tr>
                                        <div class="uk-modal" id="modal_tempFirst_{{$v->afspraak_id}}_edit">
                                            <div class="uk-modal-dialog">
                                                <button type="button" class="uk-modal-close uk-close"></button>
                                                <h3 class="heading_a">Gegevens Wijzigen</h3><br>
                                                {!! Form::open(array('url'=>'/vijftieneuro/'.$v->id.'/edit', 'method' => 'post', 'name' => 'edit')) !!}

                                                <label>Administratiekosten</label><br>
                                                <input type="number" step='0.01' name="administratiekosten"
                                                       value="25.00"><br><br>

                                                <label>Totaalbedrag</label><br>
                                                <input type="number" step='0.01' name="factuurbedrag"
                                                       value="{{number_format($v->totaalbedrag,2)}}">

                                                <br>
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium">
                                                        <div class="uk-form-row">
                                                            <div align="right">
                                                                <div class="md-btn-group">
                                                                    <button class="md-btn md-btn-success md-btn-wave-light"
                                                                            type="submit">Verstuur
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    @endforeach
                                    </tbody>
                                    <button class="md-btn md-btn-primary md-btn-wave-light" type="submit">Markeer
                                        geselecteerde als verstuurd
                                    </button>

                                </table>
                            </div>
                            {!! Form::close() !!}
                        </li>
                        <li>
                            <div class="uk-overflow-container">
                                <div class="dt_colVis_buttons"></div>
                                <table id="dt_tableExport" class="uk-table responsive" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Klant</th>
                                        <th>Afspraaktype</th>
                                        <th>Totaalbedrag</th>
                                        <th>Verwerkt door</th>
                                        <th>Huidige afspraak status</th>
                                        <th>Reden</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($potentiele as $v)
                                        @if($v->afspraak->eigenschuld == 0)
                                            <tr>
                                                <td>
                                                    <a href="/relatie/{{$v->afspraak->klant_id}}">Fam. {{App\Models\Client::findMe($v->afspraak->klant_id)->achternaam}}</a>
                                                </td>
                                                <td>{{App\Models\AppointmentType::getDescription($v->afspraak->afspraaktype_id)}}</td>
                                                <td>€ {{number_format($v->totaalbedrag, 2)}}</td>
                                                <td>{{App\Models\User::getName($v->afspraak->verwerkt_door)}}</td>
                                                <td>{{$v->status->omschrijving}}</td>
                                                <td>{!!str_replace(';', '<br>', $v->afspraak->reden)!!}</td>
                                                <td>
                                                    <a href="/vijftien/{{$v->afspraak->id}}/nieuw"
                                                       class="md-btn md-btn-primary md-btn-wave-light">Markeer als
                                                        vijftieneuro</a><br>
                                                    <a href="/veranderEigenschuld/{{$v->afspraak->id}}/vijftieneurolijst"
                                                       class="md-btn md-btn-danger md-btn-wave-light">Geen
                                                        vijftieneuro</a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                </div>
                </li>


            </div>

    @endif

@endsection
