@extends('layouts.master')

@section('title', 'Uitbetaallijst')


@section('content')

<h2>Uitbetaallijst {{$datumstring}} </h2>
@if($arr->starttoend)
	@if(isset($arr->omzet) || isset($arr->kosten) || isset($arr->winst))
	<h4 class="heading_a uk-margin-bottom">Resultaat</h4>
	<div class="md-card uk-margin-medium-bottom">
		<div class="md-card-content">
			<div class="uk-overflow-container">
				<table class="uk-table uk-text-nowrap">
					<thead>
					<tr>
						<th width="33%"><b>Omzet</b></th>
						<th width="33%"><span class="uk-text-bold md-color-red-A400"><b>Kosten</b></span></th>
						<th width="34%"><span class="uk-text-bold md-color-green-A700"><b>Resultaat</b></span></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td width="33%">
							@if(isset($arr->omzet) && isset($arr->nb)) 
								<b>	€ {{number_format($arr->omzet, 2)}} ( € {{number_format($arr->nb, 2)}} NB ) </b>
							@endif
						</td>
						<td width="33%">
							@if(isset($arr->kosten))
								<span class="uk-text-bold md-color-red-A400">
								<b>€ {{number_format($arr->kosten->totaal, 2)}}</b> 
								</span>
							@endif
						</td>
						<td width="33%">
							@if(isset($arr->winst))
								@if($arr->winst < 0)
									<span class="uk-text-bold md-color-red-A700">
								@else
									<span class="uk-text-bold md-color-green-A700">
								@endif
								<b>€ {{number_format($arr->winst, 2)}}</b> 
								</span>
							@endif
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif

	@if($arr->agg)
	<h4 class="heading_a uk-margin-bottom">Kosten per afdeling geaggregeerd</h4>
	<div class="md-card uk-margin-medium-bottom">
		<div class="md-card-content">
			<div class="uk-overflow-container">
				<table class="uk-table uk-text-nowrap">
					<thead>
					<tr>
						@if(isset($arr->salesAgg))
						<th>Verkoop</th>
						@endif
						@if(isset($arr->acquisitionAgg))
						<th>Werving</th>
						@endif
						@if(isset($arr->adminAgg))
						<th>Kantoor</th>
						@endif
						@if(isset($arr->purchaseAgg))
						<th>Inkoop</th>
						@endif
						@if(isset($arr->remainderAgg))
						<th>Overig</th>
						@endif
						<th><span class="uk-text-bold md-color-red-A700">Totaal </span></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<?php $kostentotaal = 0.00; ?>
						@if(isset($arr->salesAgg))
						<td>€ {{number_format($arr->salesAgg, 2)}}</td>
						<?php $kostentotaal += $arr->salesAgg; ?>
						@endif
						@if(isset($arr->acquisitionAgg))
						<td>€ {{number_format($arr->acquisitionAgg->wervingskosten, 2)}}</td>
						<?php $kostentotaal += $arr->acquisitionAgg->wervingskosten; ?>
						@endif
						@if(isset($arr->adminAgg))
						<td>€ {{number_format($arr->adminAgg->kantoorkosten, 2)}}</td>
						<?php $kostentotaal += $arr->adminAgg->kantoorkosten; ?>
						@endif
						@if(isset($arr->purchaseAgg))
						<td>€ {{number_format($arr->purchaseAgg, 2)}}</td>
						<?php $kostentotaal += $arr->purchaseAgg; ?>
						@endif
						@if(isset($arr->remainderAgg))
						<td>€ {{number_format($arr->remainderAgg, 2)}}</td>
						<?php $kostentotaal += $arr->remainderAgg; ?>
						@endif
						<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($kostentotaal, 2)}}</span></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif
	
	
	{{-- Normale ub lijst...? --}}
	@if($arr->employee)
	<h4 class="heading_a uk-margin-bottom">Resultaten op werknemerniveau</h4>
	<div class="md-card uk-margin-medium-bottom">
		<div class="md-card-content">
			<div class="uk-overflow-container">
				<table class="uk-table uk-text-nowrap">
					<thead>
					<tr>
						<th><b>Wie/Wat</b></th>
						<th><b>UB</b></th>
						<th><b>NB</b></th>
						<th><b>Totaal/GH</b></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						@if(isset($arr->salesTrans))
							<tr><td><b>Verkoop</b></td><td></td><td></td><td></td></tr>
							@foreach($arr->salesTrans as $s)
								@foreach($werknemers as $w)
									@if($w->id == $s->verkoper)
										<tr>
											<td>{{$w->voornaam}}</td>
											<td>€ {{number_format($s->totaalloonVerk - $s->nbloonVerk,2)}}</td>
											<td><span class="uk-text md-color-red-A700">€ {{number_format($s->nbloonVerk,2)}}</span></td>
											<td>€ {{number_format($s->totaalloonVerk,2)}}</td>
										</tr>
									@endif
									@if( $s->veger == $w->id )
										<tr>
											<td>{{$w->voornaam}}</td>
											<td>€ {{number_format($s->totaalloonVeg - $s->nbloonVeg,2)}}</td>
											<td><span class="uk-text md-color-red-A700">€ {{number_format($s->nbloonVeg,2)}}</span></td>
											<td>€ {{number_format($s->totaalloonVeg,2)}}</td>
										</tr>
									@endif
								@endforeach
								<tr>
									<td><i><u>Omzet</u></i></td>
									<td><span class="uk-text-bold md-color-green-A700">€ {{number_format($s->omzet,2)}}</span></td>
									<td><span class="uk-text md-color-red-A700">€ {{number_format($s->nb,2)}}</span></td>
									<td>€ {{number_format($s->gh, 2)}}</td>
								</tr>
								<tr>
									<td>Benzine</td>
									<td>€ {{number_format($s->benzine,2)}}</td>
									<td>€ {{number_format(0,2)}}</td>
									<td>€ {{number_format($s->benzine,2)}}</td>							
								</tr>
								<tr>
									<td>Eten</td>
									<td>€ {{number_format($s->eten,2)}}</td>
									<td>€ {{number_format(0,2)}}</td>
									<td>€ {{number_format($s->eten,2)}}</td>
								</tr>
								<tr>
									<td>Telefoonkosten</td>
									<td>€ {{number_format($s->telefoonkosten,2)}}</td>
									<td>€ {{number_format(0,2)}}</td>
									<td>€ {{number_format($s->telefoonkosten,2)}}</td>
								</tr>
								<tr>
									<td>Inkoop</td>
									<td>€ {{number_format($s->inkoop,2)}}</td>
									<td>€ {{number_format(0,2)}}</td>
									<td>€ {{number_format($s->inkoop,2)}}</td>
								</tr>
								<tr>
									<td>Extra</td>
									<td>€ {{number_format($s->extra,2)}}</td>
									<td>€ {{number_format(0,2)}}</td>
									<td>€ {{number_format($s->extra,2)}}</td>
								</tr>
								<tr><td></td><td></td><td></td><td></td></tr>
							@endforeach
						@endif
						@if(isset($arr->acquisitionTrans))
							<tr><td><b>Werving</b></td><td></td><td></td><td></td></tr>
							@foreach($arr->acquisitionTrans as $at)
							<tr>
								<td>{{$at->werknemer_id->voornaam}}</td>
								<td>€ {{number_format($at->ubloon,2)}}</td>
								<td><span class="uk-text md-color-red-A700">€ {{number_format($at->nbloon,2)}}</span></td>
								<td>€ {{number_format($at->totaalloon,2)}}</td>
							</tr>
							<tr>
								<td>Benzine van {{$at->werknemer_id->voornaam}}</td>
								<td>€ {{number_format($at->benzine,2)}}</td>
								<td><span class="uk-text md-color-red-A700">€ {{number_format($at->benzinenb,2)}}</span></td>
								<td>€ {{number_format($at->benzine + $at->benzinenb,2)}}</td>
							</tr>
								<tr><td></td><td></td><td></td><td></td></tr>
							@endforeach
						@endif
						@if(isset($arr->adminTrans))
							<tr><td><b>Kantoor</b></td><td></td><td></td><td></td></tr>
							@foreach($arr->adminTrans as $a)
							<tr>
								<td>{{$a->werknemer_id->voornaam}}</td>
								<td>€ {{number_format($a->betaald,2)}}</td>
								<td><span class="uk-text md-color-red-A700">€ {{number_format($a->nb,2)}}</span></td>
								<td>€ {{number_format($a->betaald + $a->nb,2)}}</td>
							</tr>
							@endforeach
						@endif
						@if(isset($arr->purchaseTrans))
							<tr><td><b>Inkoop</b></td><td></td><td></td><td></td></tr>
							@foreach($arr->purchaseTrans as $p)
							<tr>
								<td>{{$p->klant_id->achternaam}}</td>
								<td>@if($p->voldaan == 1) € {{number_format($p->totaalbedrag,2)}} @else € {{number_format(0,2)}} @endif</td>
								<td><span class="uk-text md-color-red-A700">@if($p->voldaan == 0) € {{number_format($p->totaalbedrag,2)}} @else € {{number_format(0,2)}} @endif</span></td>
								<td>€ {{number_format($p->totaalbedrag,2)}}</td>
							</tr>
							@endforeach
						@endif
						@if(isset($arr->remainderTrans))
							<tr><td><b>Overig</b></td><td></td><td></td><td></td></tr>
							@foreach($arr->remainderTrans as $r)
							<tr>
								<td>{{$r->omschrijving}}</td>
								<td>€ {{number_format($r->kosten,2)}}</td>
								<td>€ {{number_format(0,2)}}</td>
								<td>€ {{number_format($r->kosten,2)}}</td>
							</tr>
							@endforeach
						@endif
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif

	@if($arr->trans)
	<h4 class="heading_a uk-margin-bottom">Resultaten op transactieniveau</h4>
	<div class="md-card uk-margin-medium-bottom">
		<div class="md-card-content">
			<div class="uk-overflow-container">
				<table class="uk-table uk-text-nowrap">
					<thead>
					<tr>
						<th>Wat</th>
						<th>In</th>
						<th>Uiy</th>
						<th>Cumulatief</th>
					</tr>
					<?php $cum = 0.00; ?>
					</thead>
					<tbody>
						@if(isset($arr->salesTrans))
							@foreach($arr->salesTrans as $st)
								<tr>
									<td width="40%"><b>Omzet werklijst van @foreach($werknemers as $w) @if($w->id == $st->verkoper) {{$w->voornaam}} @endif @endforeach </b></td>
									<td width="20%"><span class="uk-text-bold md-color-green-A700">€ {{number_format($st->omzet - $st->nb, 2)}}</span></td> <?php $cum += $st->omzet - $st->nb; ?>
									<td width="20%"><span class="uk-text-bold md-color-red-A700"> - </span></td>
									<td width="20%" style="border-left: 1px solid black;">
										@if($cum >= 0)
									 		<span class="uk-text-bold md-color-green-A700">
									 	@else
											<span class="uk-text-bold md-color-red-A700">
									 	@endif
									 	 € {{number_format($cum, 2)}}</span>
									 </td>
								</tr>
							@endforeach
							@foreach($arr->salesTrans as $st)
								@foreach($werknemers as $w)
									@if($w->id == $st->verkoper)
										<tr>
											<td>Loon {{$w->voornaam}}</td> <?php $voornaam = $w->voornaam; ?>
											<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
											<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($s->totaalloonVerk - $s->nbloonVerk,2)}}</span></td>
											<?php $cum -= ($s->totaalloonVerk - $s->nbloonVerk); ?>
											<td width="20%" style="border-left: 1px solid black;">
												@if($cum >= 0)
											 		<span class="uk-text-bold md-color-green-A700">
											 	@else
													<span class="uk-text-bold md-color-red-A700">
											 	@endif
											 	 € {{number_format($cum, 2)}}</span>
											 </td>
										</tr>
									@endif
									@if( $s->veger == $w->id )
										<tr>
											<td>Loon {{$w->voornaam}}</td>
											<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
											<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($s->totaalloonVeg - $s->nbloonVeg,2)}}</span></td>
											<?php $cum -= ($s->totaalloonVerk - $s->nbloonVerk); ?>
											<td width="20%" style="border-left: 1px solid black;">
												@if($cum >= 0)
											 		<span class="uk-text-bold md-color-green-A700">
											 	@else
													<span class="uk-text-bold md-color-red-A700">
											 	@endif
											 	 € {{number_format($cum, 2)}}</span>
											 </td>
										</tr>
									@endif
								@endforeach
								<tr>
									<td>Benzine</td>
									<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
									<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($s->benzine,2)}}</span></td>							
									<?php $cum -= ($s->benzine); ?>
									<td width="20%" style="border-left: 1px solid black;">
										@if($cum >= 0)
									 		<span class="uk-text-bold md-color-green-A700">
									 	@else
											<span class="uk-text-bold md-color-red-A700">
									 	@endif
									 	 € {{number_format($cum, 2)}}</span>
									 </td>
								</tr>
								<tr>
									<td>Eten</td>
									<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
									<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($s->eten,2)}}</span></td>
									<?php $cum -= ($s->eten); ?>
									<td width="20%" style="border-left: 1px solid black;">
										@if($cum >= 0)
									 		<span class="uk-text-bold md-color-green-A700">
									 	@else
											<span class="uk-text-bold md-color-red-A700">
									 	@endif
									 	 € {{number_format($cum, 2)}}</span>
									 </td>
								</tr>
								<tr>
									<td>Telefoonkosten</td>
									<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
									<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($s->telefoonkosten,2)}}</span></td>
									<?php $cum -= ($s->telefoonkosten); ?>
									<td width="20%" style="border-left: 1px solid black;">
										@if($cum >= 0)
									 		<span class="uk-text-bold md-color-green-A700">
									 	@else
											<span class="uk-text-bold md-color-red-A700">
									 	@endif
									 	 € {{number_format($cum, 2)}}</span>
									 </td>
								</tr>
								<tr>
									<td>Inkoop</td>
									<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
									<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($s->inkoop,2)}}</span></td>
									<?php $cum -= ($s->inkoop); ?>
									<td width="20%" style="border-left: 1px solid black;">
										@if($cum >= 0)
									 		<span class="uk-text-bold md-color-green-A700">
									 	@else
											<span class="uk-text-bold md-color-red-A700">
									 	@endif
									 	 € {{number_format($cum, 2)}}</span>
									 </td>
								</tr>
								<tr>
									<td>Extra kosten Werklijst {{$voornaam}}</td>
									<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
									<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($s->extra,2)}}</span></td>
									<?php $cum -= ($s->extra); ?>
									<td width="20%" style="border-left: 1px solid black;">
										@if($cum >= 0)
									 		<span class="uk-text-bold md-color-green-A700">
									 	@else
											<span class="uk-text-bold md-color-red-A700">
									 	@endif
									 	 € {{number_format($cum, 2)}}</span>
									 </td>
								</tr>
							@endforeach
						@endif
						@if(isset($arr->acquisitionTrans))
							@foreach($arr->acquisitionTrans as $at)
								<tr>
									<td>{{$at->werknemer_id->voornaam}}  
										@if($at->uurloon == null || $at->uurloon <= 0) 
											(daggeld: {{$at->dagloon}} + ({{$at->provisiebedrag}} * {{$at->aantal}} klanten)
										@endif
									</td>
									<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
									<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($at->ubloon,2)}}</span></td>
									<?php $cum -= $at->ubloon; ?>
									<td width="20%" style="border-left: 1px solid black;">
										@if($cum >= 0)
									 		<span class="uk-text-bold md-color-green-A700">
									 	@else
											<span class="uk-text-bold md-color-red-A700">
									 	@endif
									 	 € {{number_format($cum, 2)}}</span>
									 </td>
								</tr>
								<tr>
									<td>Benzine</td>
									<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
									<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($at->benzine - $at->benzinenb,2)}}</span></td>
									<?php $cum -= $at->benzine; ?>
									<td width="20%" style="border-left: 1px solid black;">
										@if($cum >= 0)
									 		<span class="uk-text-bold md-color-green-A700">
									 	@else
											<span class="uk-text-bold md-color-red-A700">
									 	@endif
									 	 € {{number_format($cum, 2)}}</span>
									 </td>
								</tr>
							@endforeach
						@endif
						@if(isset($arr->adminTrans))
							@foreach($arr->adminTrans as $at)
								<tr>
									<td>{{$at->werknemer_id->voornaam}} (uurloon: {{$at->uurtarief}} * aantal uur: {{$at->aantal_uur}})</td>
									<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
									<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($at->betaald,2)}}</span></td>
									<?php $cum -= $at->betaald; ?>
									<td width="20%" style="border-left: 1px solid black;">
										@if($cum >= 0)
									 		<span class="uk-text-bold md-color-green-A700">
									 	@else
											<span class="uk-text-bold md-color-red-A700">
									 	@endif
									 	 € {{number_format($cum, 2)}}</span>
									 </td>
								</tr>
							@endforeach
						@endif
						@if(isset($arr->purchaseTrans))
							@foreach($arr->purchaseTrans as $pt)
							<tr>
								<td>{{$pt->klant_id->achternaam}}</td>
								<td><span class="uk-text-bold md-color-green-A700"> - </span></td>
								<td><span class="uk-text-bold md-color-red-A700">€ @if($pt->voldaan == 1) {{number_format($pt->totaalbedrag,2)}} @else - @endif</span></td>
								<?php ($pt->voldaan == 1) ? $cum -= $pt->totaalbedrag : $cum -= 0; ?>
								<td width="20%" style="border-left: 1px solid black;">
									@if($cum >= 0)
								 		<span class="uk-text-bold md-color-green-A700">
								 	@else
										<span class="uk-text-bold md-color-red-A700">
								 	@endif
								 	 € {{number_format($cum, 2)}}</span>
								 </td>
							</tr>
							@endforeach
						@endif
						@if(isset($arr->remainderTrans))
							@foreach($arr->remainderTrans as $rt)
								<tr>
									<td>{{$rt->omschrijving}}</td>
									<td><span class="uk-text-bold md-color-green-A700"> 
										@if($rt->kosten < 0)
											€ {{number_format($rt->kosten,2)}} 
										@else 
											- 
										@endif
									</span></td>
									<td><span class="uk-text-bold md-color-red-A700"> @if($rt->kosten > 0) € {{number_format($rt->kosten,2)}} @else - @endif</span></td>
									<?php $cum -= $rt->kosten;?>
									<td width="20%" style="border-left: 1px solid black;">
									@if($cum >= 0)
								 		<span class="uk-text-bold md-color-green-A700">
								 	@else
										<span class="uk-text-bold md-color-red-A700">
								 	@endif
								 	 € {{number_format($cum, 2)}}</span>
								 	</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif

@else
	@if(isset($arr->omzet) || isset($arr->kosten) || isset($arr->winst))
		<h4 class="heading_a uk-margin-bottom">Resultaat</h4>
		<div class="md-card uk-margin-medium-bottom">
			<div class="md-card-content">
				<div class="uk-overflow-container">
					<table class="uk-table uk-text-nowrap">
						<thead>
						<tr>
							<th width="33%"><b>Omzet</b></th>
							<th width="33%"><span class="uk-text-bold md-color-red-A400"><b>Kosten</b></span></th>
							<th width="34%"><span class="uk-text-bold md-color-green-A700"><b>Resultaat</b></span></th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td width="33%">
								@if(isset($arr->omzet) && isset($arr->nb)) 
									<b>	€ {{number_format($arr->omzet, 2)}} ( € {{number_format($arr->nb, 2)}} NB ) </b>
								@endif
							</td>
							<td width="33%">
								@if(isset($arr->kosten))
									<span class="uk-text-bold md-color-red-A400">
									<b>€ {{number_format($arr->kosten->totaal, 2)}}</b> 
									</span>
								@endif
							</td>
							<td width="33%">
								@if(isset($arr->winst))
									@if($arr->winst < 0)
										<span class="uk-text-bold md-color-red-A700">
									@else
										<span class="uk-text-bold md-color-green-A700">
									@endif
									<b>€ {{number_format($arr->winst, 2)}}</b> 
									</span>
								@endif
							</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@endif
	@if($arr->agg)
		<h4 class="heading_a uk-margin-bottom">Kosten per afdeling geaggregeerd</h4>
		<div class="md-card uk-margin-medium-bottom">
			<div class="md-card-content">
				<div class="uk-overflow-container">
					<table class="uk-table uk-text-nowrap">
						<thead>
						<tr>
							@if(isset($arr->salesAgg))
							<th>Verkoop</th>
							@endif
							@if(isset($arr->acquisitionAgg))
							<th>Werving</th>
							@endif
							@if(isset($arr->adminAgg))
							<th>Kantoor</th>
							@endif
							@if(isset($arr->purchaseAgg))
							<th>Inkoop</th>
							@endif
							@if(isset($arr->remainderAgg))
							<th>Overig</th>
							@endif
							<th><span class="uk-text-bold md-color-red-A700">Totaal </span></th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<?php $kostentotaal = 0.00; ?>
							@if(isset($arr->salesAgg))
							<td>€ {{number_format($arr->salesAgg, 2)}}</td>
							<?php $kostentotaal += $arr->salesAgg; ?>
							@endif
							@if(isset($arr->acquisitionAgg))
							<td>€ {{number_format($arr->acquisitionAgg->wervingskosten, 2)}}</td>
							<?php $kostentotaal += $arr->acquisitionAgg->wervingskosten; ?>
							@endif
							@if(isset($arr->adminAgg))
							<td>€ {{number_format($arr->adminAgg->kantoorkosten, 2)}}</td>
							<?php $kostentotaal += $arr->adminAgg->kantoorkosten; ?>
							@endif
							@if(isset($arr->purchaseAgg))
							<td>€ {{number_format($arr->purchaseAgg, 2)}}</td>
							<?php $kostentotaal += $arr->purchaseAgg; ?>
							@endif
							@if(isset($arr->remainderAgg))
							<td>€ {{number_format($arr->remainderAgg, 2)}}</td>
							<?php $kostentotaal += $arr->remainderAgg; ?>
							@endif
							<td><span class="uk-text-bold md-color-red-A700">€ {{number_format($kostentotaal, 2)}}</span></td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@endif
	@if($arr->employee)
	<h4 class="heading_a uk-margin-bottom">Resultaten op werknemerniveau</h4>
	<div class="md-card uk-margin-medium-bottom">
		<div class="md-card-content">
			<div class="uk-overflow-container">
				<table class="uk-table uk-text-nowrap">
					<thead>
					<tr>
						<th></th>
						@foreach($date_object as $do)
						<th>{{$do}}</th>
						<th></th>
						@endforeach
						<th><b>Totaal</b></th>
					</tr>
					<tr>
						<th><b>Wie/Wat</b></th>
						@foreach($date_object as $do)
						<th>UB</th>
						<th>NB</th>
						@endforeach
						<th>Totaal</th>
					</tr>
					</thead>
					<tbody>
					@foreach($date_object as $do)
						@foreach($arr->salesAgg as $sa)
							als datum klopt
							 print werklijst
						@endforeach
						voor elke kosten in kantoor
							als datum klopt
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif
	@if($arr->trans)
	<h4 class="heading_a uk-margin-bottom">Resultaten op transactieniveau</h4>
	<div class="md-card uk-margin-medium-bottom">
		<div class="md-card-content">
			<div class="uk-overflow-container">
				<table class="uk-table uk-text-nowrap">
					<thead>
					<tr>
						<th>Wat</th>
						<th>In</th>
						<th>Uit</th>
						<th>Cumulatief</th>
					</tr>
					<?php $cum = 0.00; ?>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif
@endif
@endsection