@extends('layouts.master')

@section('title', 'Dagstaat')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol < 99)

<h2>Uitbetaallijst voor {{date('d-m-Y', strtotime($werkdag->datum))}}</h2>
{{-- {{dd($klanten)}} --}}
{{ Form::open(array('url'=>'/planning/verkoop/'.$werkdag->datum.'/'.$werkdag->id.'/dagstaat/controle', 'method' => 'post')) }}
{{-- Verkoop --}}
<?php $omzet = 0.00; $kostenver = 0.00; $gh = 0.00; ?>
@foreach($werklijsten as $wl)
<h4>Werklijst van @if($wl->verkoper != null){{$wl->verkoper->voornaam}} @endif @if($wl->veger != null) & {{$wl->veger->voornaam}} @endif </h4>
<table class="uk-table uk-table-hover">
    <thead>
        <tr>
            <th width="25%"><span class="uk-text-bold">Wat</span></th>
            <th width="25%"><span class="md-color-green-900">UB</span></th>
            <th width="25%"><span class="md-color-red-A700">NB</span></th>
            <th width="25%">Totaal</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="25%"> @if($wl->verkoper != null){{$wl->verkoper->voornaam}} @endif</td>
            <input type="hidden" value="{{$wl->id}}" name="werklijst_id[]"/>
            <td width="25%"><input type="number" name="ubloonVerk[]" class="md-input" step="0.01" value="{{number_format($wl->totaalloonVerk - $wl->nbloonVerk,2)}}" /> </td>
            <td width="25%"><input type="number" name="nbloonVerk[]" class="md-input" step="0.01" value="{{number_format($wl->nbloonVerk,2)}}" /> </td>
            <td width="25%"><input type="number" name="totaalloonVerk[]" class="md-input" step="0.01" value="{{number_format($wl->totaalloonVerk,2)}}" /> </td>
        </tr>
        @if($wl->veger != null)
        <tr>
            <td width="25%">{{$wl->veger->voornaam}}</td>
            <td width="25%"><input type="number" name="ubloonVeg[]" class="md-input" step="0.01" value="{{number_format($wl->totaalloonVeg - $wl->nbloonVeg,2)}}" /> </td>
            <td width="25%"><input type="number" name="nbloonVeg[]" class="md-input" step="0.01" value="{{number_format($wl->nbloonVeg,2)}}" /> </td>
            <td width="25%"><input type="number" name="totaalloonVeg[]" class="md-input" step="0.01" value="{{number_format($wl->totaalloonVeg,2)}}" /> </td>
        </tr>
        @endif
        <tr>
            <td width="25%">Omzet</td>
            <td width="25%"><input disabled type="number" name="ubloonOmz[]" class="md-input" step="0.01" value="{{number_format($wl->omzet - $wl->nbomzet,2)}}" /> </td>
            <td width="25%"><input disabled type="number" name="nbomzet[]" class="md-input" step="0.01" value="{{number_format($wl->nbomzet,2)}}" /> </td>
            <td width="25%"><input disabled type="number" name="omzet[]" class="md-input" step="0.01" value="{{number_format($wl->omzet,2)}}" /> </td>
        </tr>
        <tr>
            <td width="25%">Geld opgehaald</td>
            <td width="25%"><input type="number" name="gh[]" class="md-input" step="0.01" value="{{number_format($wl->gh,2)}}" /> </td>
            <td width="25%"></td>
            <td width="25%"></td>
        </tr>
        <tr>
            <td width="25%">Benzine</td>
            <td width="25%"><input type="number" name="benzine[]" class="md-input" step="0.01" value="{{number_format($wl->benzine,2)}}" /> </td>
            <td width="25%"></td>
            <td width="25%"></td>
        </tr>
        <tr>
            <td width="25%">Eten</td>
            <td width="25%"><input type="number" name="eten[]" class="md-input" step="0.01" value="{{number_format($wl->eten,2)}}" /> </td>
            <td width="25%"></td>
            <td width="25%"></td>
        </tr>
        <tr>
            <td width="25%">Telefoonkosten</td>
            <td width="25%"><input type="number"  name="telefoonkosten[]" class="md-input" step="0.01" value="{{number_format($wl->telefoonkosten,2)}}" /> </td>
            <td width="25%"></td>
            <td width="25%"></td>
        </tr>
        <tr>
            <td width="25%">Inkoop</td>
            <td width="25%"><input type="number" name="inkoop[]" class="md-input" step="0.01" value="{{number_format($wl->inkoop,2)}}" /> </td>
            <td width="25%"></td>
            <td width="25%"></td>
        </tr>
        <tr>
            <td width="25%">Extra</td>
            <td width="25%"><input type="number" name="extra[]" class="md-input" step="0.01" value="{{number_format($wl->extra,2)}}" /> </td>
            <td width="25%"></td>
            <td width="25%"></td>
        </tr>
    </tbody>
</table>

{{-- Resultaat Werklijst --}}
<?php 
$omzet = $omzet + $wl->omzet; 
$kostenver = $kostenver + ($wl->totaalloonVerk + $wl->totaalloonVeg + $wl->benzine + $wl->eten + $wl->telefoonkosten + $wl->inkoop + $wl->extra);
$gh = $gh + $wl->gh;
?>
<table class="uk-table uk-table-hover">
    <h4>Resultaat Werklijst</h4>
    <thead>
        <tr>
            <th width="25%"><span class="md-color-green-900">Omzet</span></th>
            <th width="25%"><span class="md-color-red-A700">Kosten</span></th>
            <th width="25%"><span class="md-color-blue-A700">Geld opgehaald</span></th>
            <th width="25%"><em>
                @if($wl->omzet - ($wl->totaalloonVerk + $wl->totaalloonVeg + $wl->benzine + $wl->eten + $wl->telefoonkosten + $wl->inkoop + $wl->extra) + $wl->gh > 0)
                <span class="uk-text-bold md-color-green-900">
                @else
                <span class="uk-text-bold md-color-red-A700">
                @endif
                Bruto Winst</span></em></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="25%"><span class="md-color-green-900">€ {{number_format($wl->omzet,2)}}</span></td>
            <td width="25%"><span class="md-color-red-A700">€ {{number_format($wl->totaalloonVerk + $wl->totaalloonVeg + $wl->benzine + $wl->eten + $wl->telefoonkosten + $wl->inkoop + $wl->extra,2)}}</span></td>
            <td width="25%"><span class="md-color-blue-A700">€ {{number_format($wl->gh,2)}}</span></td>
            <td width="25%">
                @if($wl->omzet - ($wl->totaalloonVerk + $wl->totaalloonVeg + $wl->benzine + $wl->eten + $wl->telefoonkosten + $wl->inkoop + $wl->extra) + $wl->gh > 0)
                <span class="uk-text-bold md-color-green-900">
                @else
                <span class="uk-text-bold md-color-red-A700">
                @endif
                € {{number_format($wl->omzet - ($wl->totaalloonVerk + $wl->totaalloonVeg + $wl->benzine + $wl->eten + $wl->telefoonkosten + $wl->inkoop + $wl->extra) + $wl->gh,2)}}
                </span>
            </td>
        </tr>  
    </tbody>
</table>
<hr />
@endforeach

<br>

{{-- Werving --}}
<?php $kostenwer = 0.00; ?>
<h4>Klantenwervers</h4>
<table class="uk-table uk-table-hover">
    <thead>
        <tr>
            <th width="20%"><span class="uk-text-bold">Wie</span></th>
            <th width="16%"><span class="md-color-green-900">UB</span></th>
            <th width="16%"><span class="md-color-red-A700">NB</span></th>
            <th width="16%">Totaal</th>
            <th width="16%">Benzinekosten UB</th>
            <th width="16%">Benzinekosten NB</th>
        </tr>
    </thead>
    <tbody>
        @foreach($wervers as $w)
        @foreach($klanten as $k)
        @if($w->werknemer_id == $k->werknemer->id)
        <tr>
            <input type="hidden" value="{{$k->werknemer->id}}" name="werver[]"/>
            <td width="20%"><span class="uk-text-bold">{{$k->werknemer->voornaam}}</span></td>
            <td width="16%"><input type="number" name="ubWerver[]" class="md-input" step="0.01" value="{{number_format($w->ubloon, 2)}}"/></td>
            <td width="16%"><input type="number" name="nbWerver[]"  class="md-input" step="0.01" value="{{number_format($w->nbloon,2)}}"/></td>
            <td width="16%"><input type="number"  name="totaalloonWerver[]" class="md-input" step="0.01" value="{{number_format($w->totaalloon, 2)}}"/></span></td>
            <td width="16%"><input type="number"  name="benzineUB[]" class="md-input" step="0.01" value="{{$w->benzine}}"/></span></td>
            <td width="16%"><input type="number"  name="benzineNB[]" class="md-input" step="0.01" value="{{$w->benzinenb}}"/></span></td>
            <?php $kostenwer = $kostenwer + $w->totaalloon + $w->benzine + $w->benzinenb;?>
        </tr>   
        @endif
        @endforeach
        @endforeach
    </tbody>
</table>

<br>
{{-- Kantoor --}}
<?php $kostenkan = 0.00; ?>
<h4>Kantoor</h4>
<table class="uk-table uk-table-hover">
    <thead>
        <tr>
            <th width="25%"><span class="uk-text-bold">Wie</span></th>
            <th width="25%"><span class="md-color-green-900">UB</span></th>
            <th width="25%"><span class="md-color-red-A700">NB</span></th>
            <th width="25%">Totaal</th>
        </tr>
    </thead>
    <tbody>
    @foreach($kantoor as $k)
        <tr>
            <input type="hidden" value="{{$k->werknemer_id->id}}" name="werknemer_id[]"/>
            <td width="25%"><span class="uk-text-bold">{{$k->werknemer_id->voornaam}}</span></td>
            <td width="25%"><input type="number" name="ubloonKan[]" class="md-input" step="0.01" value="{{number_format($k->bedrag_betaald, 2)}}"/></td>
            <td width="25%"><input type="number" name="nbloonKan[]" class="md-input" step="0.01" value="{{number_format($k->aantal_uur * $k->uurtarief - $k->bedrag_betaald, 2)}}"/></td>
            <td width="25%"><input type="number" name="totaalloonKan[]" class="md-input" step="0.01" value="{{number_format($k->aantal_uur * $k->uurtarief, 2)}}"/></td>
            <?php $kostenkan = $kostenkan + $k->aantal_uur * $k->uurtarief; ?>
        </tr>
    @endforeach
    </tbody>
</table>

<br>
{{-- Overig --}}
<?php $kostenove = 0.00; ?>
<h4>Overige kosten</h4>
<table class="uk-table uk-table-hover">
    <thead>
        <tr>
            <th width="75%"><span class="uk-text-bold">Wat</span></th>
            <th width="25%"><span>Hoeveel</span></th>
        </tr>
    </thead>
    <tbody>
    @foreach($overig as $k)
        <tr>
            <input type="hidden" value="{{$k->id}}" name="overig[]"/>
            <td width="75%"><span class="uk-text-bold">{{$k->omschrijving}}</span></td>
            <td width="25%"><input type="number" name="totaalloonOve[]" class="md-input" step="0.01" value="{{number_format($k->kosten, 2)}}"/></td>
            <?php $kostenove = $kostenove + $k->kosten; ?>
        </tr>
    @endforeach
    </tbody>
</table>

<br>

<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium">
        <div class="uk-form-row">
            <div align="right">
                <div class="md-btn-group">
                    <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Sla UB Lijst op</button>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}

<br>
{{-- Resultaat --}}
<h3>Dag Resultaat</h3>
<table class="uk-table uk-table-hover">
    <thead>
        <tr>
            <th width="75%"><span class="uk-text-bold">Wat</span></th>
            <th width="25%"><span>Hoeveel</span></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="75%"><span class="uk-text-bold">Omzet</span></td>
            <td width="25%"><span class="md-color-green-900">€ {{number_format($omzet, 2)}}</span></td>
        </tr>
        <tr>
            <td width="75%"><span class="uk-text-bold">Geld Opgehaald</span></td>
            <td width="25%"><span class="md-color-green-900">€ {{number_format($gh, 2)}}</span></td>
        </tr>
        <tr class="md-bg-grey-300" style="border: 1px solid black ;">
            <td width="75%"><span class="uk-text-bold"></span></td>
            <td width="25%"><span @if($omzet + $gh < 0) class="md-color-red-A700" @else class="md-color-green-900" @endif>€ {{number_format($omzet + $gh, 2)}}</span></td>
        </tr>
        <tr>
            <td width="75%"><span class="uk-text-bold">Kosten Verkoop</span></td>
            <td width="25%"><span class="md-color-red-A700">€ {{number_format($kostenver, 2)}}</span></td>
        </tr>
        <tr class="md-bg-grey-300" style="border: 1px solid black ;">
            <td width="75%"><span class="uk-text-bold"></span></td>
            <td width="25%"><span @if($omzet + $gh - $kostenver < 0) class="md-color-red-A700" @else class="md-color-green-900" @endif>€ {{number_format($omzet + $gh - $kostenver, 2)}}</span></td>
        </tr>
        <tr>
            <td width="75%"><span class="uk-text-bold">Kosten Werving</span></td>
            <td width="25%"><span class="md-color-red-A700">€ {{number_format($kostenwer, 2)}}</span></td>
        </tr>
        <tr class="md-bg-grey-300" style="border: 1px solid black ;">
            <td width="75%"><span class="uk-text-bold"></span></td>
            <td width="25%"><span @if($omzet + $gh - $kostenver - $kostenwer < 0) class="md-color-red-A700" @else class="md-color-green-900" @endif>€ {{number_format($omzet + $gh - $kostenver + $kostenwer, 2)}}</span></td>
        </tr>
        <tr>
            <td width="75%"><span class="uk-text-bold">Kosten Kantoor</span></td>
            <td width="25%"><span class="md-color-red-A700">€ {{number_format($kostenkan, 2)}}</span></td>
        </tr>
        <tr class="md-bg-grey-300" style="border: 1px solid black ;">
            <td width="75%"><span class="uk-text-bold"></span></td>
            <td width="25%"><span @if($omzet + $gh - $kostenver - $kostenwer - $kostenkan < 0) class="md-color-red-A700" @else class="md-color-green-900" @endif>
                € {{number_format($omzet + $gh - $kostenver - $kostenwer - $kostenkan, 2)}}</span></td>
        </tr>
        <tr>
            <td width="75%"><span class="uk-text-bold">Overige Kosten</span></td>
            <td width="25%"><span class="md-color-red-A700">€ {{number_format($kostenove, 2)}}</span></td>
        </tr>
        <tr class="md-bg-grey-400" style="border: 1px solid black ;">
            <td width="75%"><span class="uk-text-bold">Resultaat</span></td>
            <td width="25%"><span @if($omzet + $gh - $kostenver - $kostenwer - $kostenkan - $kostenove < 0) class="uk-text-bold md-color-red-A700" @else class="uk-text-bold md-color-green-900" @endif>
                € {{number_format($omzet + $gh - $kostenver - $kostenwer - $kostenkan - $kostenove, 2)}}</span></td>
        </tr>
    </tbody>
</table>
@endif

@endsection