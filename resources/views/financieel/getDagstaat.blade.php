@extends('layouts.master')

@section('title', 'Opvragen Financiële Rapportage')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Weekstaat Opvragen</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
    	{!! Form::open(array('url'=>'/financiele-rapportage', 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
	         <h3>Weekstaat</h3>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-2">
                            <label for="kUI_datepicker_a" class="uk-form-label">*Startdatum:</label>
                            <input id="kUI_datepicker_a" value="{{date("d-m-Y",strtotime('monday this week'))}}" name="startdatum" required/>
                        </div>
                        <div class="uk-width-large-1-2">
                            <label for="kUI_datepicker_z" class="uk-form-label">Einddatum:</label>
                            <input id="kUI_datepicker_z" value="{{date("d-m-Y",strtotime('friday this week'))}}" name="einddatum" />
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" align="center"  data-uk-grid-margin>
                        <div class="uk-width-large" align="center">
                            <h4> Welke hoofdonderwerpen moeten getoond worden? </h4>
                            <span class="icheck-inline">
                                <input type="checkbox" name="omzet" id="checkbox_demo_inline_1" data-md-icheck checked />
                                <label for="checkbox_demo_inline_1" class="inline-label">Omzet</label>
                            </span>
                            <span class="icheck-inline">
                                <input type="checkbox" name="kosten" id="checkbox_demo_inline_2" data-md-icheck checked />
                                <label for="checkbox_demo_inline_2" class="inline-label">Kosten</label>
                            </span>
                            <span class="icheck-inline">
                                <input type="checkbox" name="winst" id="checkbox_demo_inline_3" data-md-icheck checked/>
                                <label for="checkbox_demo_inline_3" class="inline-label">Winst</label>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" align="center"  data-uk-grid-margin>
                        <div class="uk-width-large" align="center">
                            <h4> Welke subonderwerpen moeten getoond worden? </h4>
                            <span class="icheck-inline">
                                <input type="checkbox" name="verkoop" id="checkbox_demo_inline_6" data-md-icheck checked />
                                <label for="checkbox_demo_inline_6" class="inline-label">Verkoop</label>
                            </span>
                            <span class="icheck-inline">
                                <input type="checkbox" name="werving" id="checkbox_demo_inline_5" data-md-icheck checked />
                                <label for="checkbox_demo_inline_5" class="inline-label">Werving</label>
                            </span>
                            <span class="icheck-inline">
                                <input type="checkbox" name="kantoor" id="checkbox_demo_inline_4" data-md-icheck checked />
                                <label for="checkbox_demo_inline_4" class="inline-label">Kantoor</label>
                            </span>
                            <span class="icheck-inline">
                                <input type="checkbox" name="inkoop" id="checkbox_demo_inline_7" data-md-icheck checked />
                                <label for="checkbox_demo_inline_7" class="inline-label">Inkoop</label>
                            </span>
                            <span class="icheck-inline">
                                <input type="checkbox" name="overig" id="checkbox_demo_inline_8" data-md-icheck checked />
                                <label for="checkbox_demo_inline_8" class="inline-label">Overige Kosten</label>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row" align="left">
                    <div class="uk-grid" align="left"  data-uk-grid-margin>
                        <div class="uk-width-large" align="center">
                            <h4> Extra opties </h4>
                            <span class="icheck-inline">
                                <input type="checkbox" name="startToEnd" value="1" id="checkbox_demo_1" data-md-icheck />
                                <label for="checkbox_demo_1" class="inline-label">Alleen startdatum tonen?</label>
                            </span>
                            <span class="icheck-inline">
                                <input type="checkbox" name="aggregate" value="1" id="checkbox_demo_3" data-md-icheck checked />
                                <label for="checkbox_demo_3" class="inline-label">Cijfers samenvoegen?</label> 
                            </span>
                            <span class="icheck-inline">
                                <input type="checkbox" name="employee" value="1" id="checkbox_demo_4" data-md-icheck checked />
                                <label for="checkbox_demo_4" class="inline-label">Cijfers op werknemerniveau tonen?</label>
                            </span>
                            <span class="icheck-inline">
                                <input type="checkbox" name="transaction" value="1" id="checkbox_demo_2" data-md-icheck checked />
                                <label for="checkbox_demo_2" class="inline-label">Cijfers op transactieniveau tonen?</label>
                            </span>
                        </div>
                    </div>
                </div>
                <br />
                <p>
                    <em>
                        NB: <br>
                        - Bij het selecteren van 'Alleen startdatum tonen',  wordt het einddatum veld genegeerd.<br>
                        - Bij het selecteren van 'Omzet', wordt de omzet getoond van die dag(en). <br>
                        - Bij het selecteren van 'Verkoop', worden de kosten van de verkoop meegenomen. <br>
                        - Bij het selecteren van 'Cijfers samenvoegen' wordt de gehele datumselectie geaggregeerd per onderwerp.<br>
                        - Bij het selecteren van een groot bereik (periode van drie maanden bijvoorbeeld), kan de laadtijd en de leesbaarheid van de transactie en werknemer dimensie erg verslechteren. <br>
                        - Bij het <b>niet</b> selecteren van een subonderwerp worden alleen de hoofdonderwerpen, indien geselecteerd, op dimensie getoond. <br>
                        - Bij het <b>niet</b> selecteren van een hoofdonderwerp, worden de geselecteerde subonderwerpen niet samengevoegd naar een hoofdonderwerp. <br>
                        - Bij het <b>niet</b> selecteren van een hoofd- en subonderwerp, wordt er niets getoond. <br>
                        - Er dient te allen tijde minstens een dimensie geselecteerd te worden. 
                    </em>
                </p>
	            	<div class="uk-form-row">
			            <div align="right">
			            	<div class="md-btn-group">
				            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
				                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder</button>
			            	</div>
			            </div>
		        	</div>
				</div>
            </div> 
        {!! Form::close() !!}
    </div>
</div>

@endif
@endsection



@push('scripts')

    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>
    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>
@endpush