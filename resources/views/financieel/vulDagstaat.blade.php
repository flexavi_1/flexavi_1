@extends('layouts.master')

@section('title', 'Dagstaat')


@push('scripts')


    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>

    <script src="{{ URL::asset('assets/bower_components/handlebars/handlebars.min.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/custom/handlebars_helpers.min.js')}}"></script>

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Dagstaat {{$date}}</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
    	{!! Form::open(array('url'=>'/planning/verkoop/'.$date.'/'.$werkdag_id.'/dagstaat', 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
	         <h3>Dagstaat Werklijsten</h3>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
            	<div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-2">
                            <label for="kUI_datepicker_a" class="uk-form-label">Datum:</label>
                            <input id="kUI_datepicker_a" name="startdatum" value="{{date('d-m-Y', strtotime($date))}}" disabled required/>
                        </div>
                    </div>
                </div>
                <br />
                @if(count($werkdagen) == 0)
                    @for($z = 0; $z < count($danger); $z++)
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-1-1">
                                <div class="md-bg-red-500 uk-text-contrast">
                                    <h3 class="md-bg-white"><span class="md-bg-white">{{$danger[$z]['bericht']}}</span></h3>
                                </div>
                            </div>
                        </div>
                    @endfor
                    <?php $check = false; ?>
                @endif

                @for($i = 0; $i < count($werkdagen); $i++)
                    <?php $check = true; // true = alle klanten verwerkt. ?>
                    @for($z = 0; $z < count($danger); $z++)
                        @if($danger[$z] == $werkdagen[$i][9]->id)
                            <?php $check = false; //er zijn dus klanten nog niet verwerkt.?>
                        @endif
                    @endfor
                    @if($check)
                        <h4>{{$werkdagen[$i][7]}}</h4>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-4">
                                    <label class="uk-form-label">{{$werkdagen[$i][9]->verkoper->voornaam}} UB</label>
                                    <input type="number" step="0.01" class="md-input" name="totaalloonVerk[]" value="{{$werkdagen[$i][3] + $werkdagen[$i][5]}}" required/>
                                </div>
                                <div class="uk-width-medium-1-4">
                                    <label class="uk-form-label">{{$werkdagen[$i][9]->verkoper->voornaam}} NB</label>
                                    <input type="number" step="0.01" class="md-input" name="nbloonVerk[]" value="0.00"required/>
                                </div>
                                <input type="hidden" name="werkdag_id[]" value="{{$werkdagen[$i][9]->id}}"/>
                            </div>
                        @if($werkdagen[$i][10] != false)
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-4">
                                    <label class="uk-form-label">{{$werkdagen[$i][9]->veger->voornaam}} UB</label>
                                    <input type="number" step="0.01" class="md-input" name="totaalloonVeg[]" value="{{$werkdagen[$i][4]  + $werkdagen[$i][6]}}" required/>
                                </div>
                                <div class="uk-width-medium-1-4">
                                    <label class="uk-form-label">{{$werkdagen[$i][9]->veger->voornaam}} NB</label>
                                    <input type="number" value="0.00" step="0.01" class="md-input" name="nbloonVeg[]" required/>
                                </div>
                            </div>
                        @endif
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-3">
                                    <label class="uk-form-label">Omzet betaald</label>
                                    <input type="number" step="0.01" class="md-input" name="omzet[]" value="{{$werkdagen[$i][0] - $werkdagen[$i][1]}}" required/>
                                </div>
                                <div class="uk-width-medium-1-3">
                                    <label class="uk-form-label">Omzet Niet betaald</label>
                                    <input type="number" step="0.01" class="md-input" name="nbomzet[]" value="{{$werkdagen[$i][1]}}" required/>
                                </div>
                                <div class="uk-width-medium-1-3">
                                    <label class="uk-form-label">Geld Opgehaald</label>
                                    <input type="number" step="0.01" class="md-input" name="gh[]" value="{{$werkdagen[$i][8]}}" required/>
                                </div>
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-5">
                                    <label class="uk-form-label">Benzine</label>
                                    <input type="number" step="0.01" class="md-input" name="benzine[]" value="{{$werkdagen[$i][9]->benzine}}" required/>
                                </div>
                                <div class="uk-width-medium-1-5">
                                    <label class="uk-form-label">Eetgeld</label>
                                    <input type="number" step="0.01" class="md-input" name="eten[]" value="{{$werkdagen[$i][9]->eten}}" required/>
                                </div>
                                <div class="uk-width-medium-1-5">
                                    <label class="uk-form-label">Telefoonkosten</label>
                                    <input type="number" step="0.01" class="md-input" name="telefoonkosten[]" value="{{$werkdagen[$i][9]->telefoonkosten}}" required/>
                                </div>
                                <div class="uk-width-medium-1-5">
                                    <label class="uk-form-label">Inkoop</label>
                                    <input type="number" step="0.01" class="md-input" name="inkoop[]" value="{{$werkdagen[$i][9]->inkoop}}" required/>
                                </div>
                                <div class="uk-width-medium-1-5">
                                    <label class="uk-form-label">Extra kosten</label>
                                    <input type="number" step="0.01" class="md-input" name="extra[]" value="{{$werkdagen[$i][9]->extra}}" required/>
                                </div>
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2">
                                    <label class="uk-form-label">Aantal klanten</label>
                                    <input type="number" step="1" class="md-input" name="aantalklanten[]" value="{{$werkdagen[$i][2]}}" required/>
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <label class="uk-form-label">Klanten niet gelukt</label>
                                    <input type="number" step="1" class="md-input" name="nietgelukt[]" value="{{$werkdagen[$i][11]}}"required/>
                                </div>
                            </div>
                        </div>
                        <br>

                    @else
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-1-1">
                                <div class="md-bg-red-500 uk-text-contrast">
                                    <h3 class="md-bg-white"><span class="md-bg-white">U kunt de UB lijst nog niet controleren/invullen. De afspraken van deze werklijst zijn nog niet allemaal verwerkt.</span></h3>
                                </div>
                            </div>
                        </div>
                    @endif
                @endfor

                        <h4> Werving vandaag</h4>
                        
                        @foreach($klanten as $k)
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <input type="hidden" name="werver[]" value="{{$k->werknemer->id}}">
                                <div class="uk-width-medium-1-6">
                                    <label class="uk-form-label">Werknemer</label>
                                    <input type="text" class="md-input label-fixed" disabled value="{{$k->werknemer->voornaam." ".$k->werknemer->achternaam}}" />  
                                </div>
                                <div class="uk-width-medium-1-6">
                                    <label class="uk-form-label">Aantal klanten</label>
                                    <input type="number" disabled step="1" class="md-input"  value="{{$k->aantal}}" required/>
                                    <input type="hidden" value="{{$k->aantal}}" name="werveraantal[]" />
                                </div>
                                <div class="uk-width-medium-1-6">
                                    @if($k->functie != null)
                                    <input type="hidden" name="functie[]" value="{{$k->functie->functie_id}}">
                                    @endif
                                    <label class="uk-form-label">UB</label>
                                    @if($k->functie != null)
                                    <input type="number" step="0.01" class="md-input" name="werverUB[]" value="0" required/>
                                    @else
                                    <input type="number" step="0.01" class="md-input" name="werverUB[]" value="0" required/>
                                    @endif
                                </div>
                                <div class="uk-width-medium-1-6">
                                    <label class="uk-form-label">NB</label>
                                    @if($k->functie != null)
                                    <input type="number" step="0.01" class="md-input" name="werverNB[]" value="{{$k->aantal * $k->functie->provisiebedrag + $k->functie->dagloon}}" required/>  
                                    @else
                                    <input type="number" step="0.01" class="md-input" name="werverNB[]" value="0" required/> 
                                    @endif
                                </div>
                                <div class="uk-width-medium-1-6">
                                    <label class="uk-form-label">Benzine UB</label>
                                    <input type="number" step="0.01" class="md-input" name="werverBenzineUB[]" value="0" required/>
                                </div>
                                <div class="uk-width-medium-1-6">
                                    <label class="uk-form-label">Benzine NB</label>
                                    <input type="number" step="0.01" class="md-input" name="werverBenzineNB[]" value="0" required/>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        <br>
                        <h4> Kantoor vandaag </h4>

                        <div data-dynamic-fields="field_template_b"></div>
                        <script id="field_template_b" type="text/x-handlebars-template">
                            <div class="uk-grid uk-grid-medium form_section form_section_separator" id="d_form_row" data-uk-grid-match>
                                <div class="uk-width-9-10">
                                    <div class="uk-grid">
                                        <div class="uk-width-1-4">
                                            <div class="parsley-row">
                                                <span class="uk-form-help-block">Werknemer</span>
                                                <select id="d_form_select_activity" name="werknemer[]" data-md-selectize required>
                                                    @foreach($werknemers as $werknemer)
                                                    <option value="{{$werknemer->werknemer->id}}">{{$werknemer->werknemer->voornaam}} {{$werknemer->werknemer->achternaam}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <div class="parsley-row">
                                                <span class="uk-form-help-block">Aantal uur</span>
                                                <input type="number" step="0.1" min="0.00" value="0.00" class="md-input label-fixed" id="d_form_amount" name="aantal_uur[]" required>
                                            </div>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <div class="parsley-row">
                                                <span class="uk-form-help-block">Uurloon</span>
                                                <input type="number" value='0.00' step="0.01" class="md-input label-fixed" id="d_form_amount" name="uurtarief[]" required>
                                            </div>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <div class="parsley-row">
                                                <span class="uk-form-help-block">Bedrag betaald</span>
                                                <input type="number" value='0.00' step="0.01" class="md-input label-fixed" id="d_form_amount" name="betaald[]" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-1-10 uk-text-center">
                                    <div class="uk-vertical-align uk-height-1-1">
                                        <div class="uk-vertical-align-middle">
                                            <a href="#" class="btnSectionClone" data-section-clone="#d_form_section"><i class="material-icons md-36">&#xE146;</i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </script>
                        <br>
                        <hr/>
                        <h4> Extra kosten </h4>

                        <div data-dynamic-fields="field_template_a"></div>
                        <script id="field_template_a" type="text/x-handlebars-template">
                            <div class="uk-grid uk-grid-medium form_section form_section_separator" id="d_form_row" data-uk-grid-match>
                                <div class="uk-width-9-10">
                                    <div class="uk-grid">
                                        <div class="uk-width-7-10">
                                            <div class="parsley-row">
                                                <span class="uk-form-help-block">Omschrijving</span>
                                                <input type="text" class="md-input label-fixed" placeholder='Eten tijdens het overwerken' value="Boodschappen" id="d_form_amount" name="omschrijving[]" required>
                                            </div>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <div class="parsley-row">
                                                <span class="uk-form-help-block">Kosten</span>
                                                <input type="number" value='0.00' step="0.01" class="md-input label-fixed" id="d_form_amount" name="kosten[]" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-1-10 uk-text-center">
                                    <div class="uk-vertical-align uk-height-1-1">
                                        <div class="uk-vertical-align-middle">
                                            <a href="#" class="btnSectionClone" data-section-clone="#d_form_section"><i class="material-icons md-36">&#xE146;</i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </script>
                        <br />


                @if($check)
	            	<div class="uk-form-row">
			            <div align="right">
			            	<div class="md-btn-group">
				            	<button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
				                <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder</button>
			            	</div>
			            </div>
		        	</div>
                @endif
				</div>
            </div> 
        {!! Form::close() !!}
    </div>
</div>

@endif
@endsection



@push('scripts')

    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>
    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>
@endpush