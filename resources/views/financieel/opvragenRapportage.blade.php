@extends('layouts.master')

@section('title', 'Opvragen Financiële Rapportage')


@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>
    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>


    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

@endpush
@section('content')

@if(Auth::user()->rol != 99)

<h2 class="heading_b uk-margin-bottom">Rapportage Opvragen</h2>

 <div class="md-card uk-margin-large-bottom">
    <div class="md-card-content">
        {!! Form::open(array('url'=>'/opvragen-rapportage', 'method' => 'post', 'data-parsley-validate')) !!}
         <div class="uk-grid" data-uk-grid-margin>
             <h3>Rapportage</h3>
            <div class="uk-width-medium">
                <div class="uk-form-row">
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-2">
                            <label for="kUI_datepicker_a" class="uk-form-label">Startdatum:</label>
                            <input id="kUI_datepicker_a" value="" name="startdate"/>
                        </div>
                        <div class="uk-width-large-1-2">
                            <label for="kUI_datepicker_z" class="uk-form-label">Einddatum:</label>
                            <input id="kUI_datepicker_z" value="" name="enddate" />
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-6">
                            <select id="select_demo_4" name='selectOne' required class="md-input">
                                <option value="" disabled selected hidden>Selectie 1: dimensie*</option>
                                <optgroup label="Rekenkundig">
                                    <option value="gemiddelde">Gemiddelde</option>
                                    <option value="totaal">Totaal</option>
                                    <option value="count">Aantal</option>
                                </optgroup>
                                <optgroup label="Groep">
                                    <option value='omzet'>Omzet</option>
                                </optgroup>
                                <optgroup label="Onderwerp">
                                    <option value="tarief">Tarief</option>
                                    <option value="aantal">Aantal eenheden</option>
                                    <option value="verkoper">Verkoper</option>
                                    <option value="klant">Klant</option>
                                    <option value="activiteit">Activiteit</option>
                                    <option value="plaats">Plaats</option>
                                    <option value="type">Type</option>
                                    <option value="geschreven_door">Geschreven door</option>
                                    <option value="tijdj">Tijd - Jaar</option>
                                    <option value="tijdm">Tijd - Maand</option>
                                    <option value="tijdw">Tijd - Week</option>
                                    <option value="tijdd">Tijd - Dag</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="uk-width-large-1-6">
                            <select id="select_demo_4" name='selectTwo' required class="md-input">
                                <option value="" disabled selected hidden>Selecteer..*</option>
                                <option value='van'>Van</option>
                                <option value='tov'>T.o.v.</option>
                                <option value='en'>En</option>
                            </select>
                        </div>
                        <div class="uk-width-large-1-6">
                            <select id="select_demo_4" name='selectThree' required class="md-input">
                                <option value="" disabled selected hidden>Selectie 2: dimensie*</option>
                                <optgroup label="Groep">
                                    <option value='omzet'>Omzet</option>
                                </optgroup>
                                <optgroup label="Onderwerp">
                                    <option value="tarief">Tarief</option>
                                    <option value="aantal">Aantal eenheden</option>
                                    <option value="verkoper">Verkoper</option>
                                    <option value="klant">Klant</option>
                                    <option value="activiteit">Activiteit</option>
                                    <option value="plaats">Plaats</option>
                                    <option value="type">Type</option>
                                    <option value="geschreven_door">Geschreven door</option>
                                    <option value="tijdj">Tijd - Jaar</option>
                                    <option value="tijdm">Tijd - Maand</option>
                                    <option value="tijdw">Tijd - Week</option>
                                    <option value="tijdd">Tijd - Dag</option>
                                </optgroup>
                                <optgroup label="Status">
                                    @foreach(App\Models\Status::orderBy('omschrijving', 'ASC')->get() as $s)
                                        <option value="{{$s->id}}-status">{{$s->omschrijving}}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                        <div class="uk-width-large-1-6">
                            <select id="select_demo_4" name='selectFour' class="md-input">
                                <option value="" disabled selected hidden>Selecteer..</option>
                                <option value='nvt'>NVT</option>
                                <option value='per'>Per</option>
                            </select>
                        </div>
                        <div class="uk-width-large-1-6">
                            <select id="select_demo_4" name='selectFive' class="md-input">
                                <option value="" disabled selected hidden>Selectie 3: dimensie</option>
                                <option value='nvt'>NVT</option>
                                <optgroup label="Onderwerp">
                                    <option value="tarief">Tarief</option>
                                    <option value="aantal">Aantal eenheden</option>
                                    <option value="verkoper">Verkoper</option>
                                    <option value="klant">Klant</option>
                                    <option value="activiteit">Activiteit</option>
                                    <option value="plaats">Plaats</option>
                                    <option value="type">Type</option>
                                    <option value="geschreven_door">Geschreven door</option>
                                    <option value="tijdj">Tijd - Jaar</option>
                                    <option value="tijdm">Tijd - Maand</option>
                                    <option value="tijdw">Tijd - Week</option>
                                    <option value="tijdd">Tijd - Dag</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="uk-width-large-1-6">
                            <select id="select_demo_4" name='selectSix' class="md-input">
                                <option value="" disabled selected hidden>Sorteren op</option>
                                <option value='nvt'>NVT</option>
                                <option value='1'>Selectie 1 (hoog - laag)</option>
                                <option value='2'>Selectie 1 (laag - hoog)</option>
                                <option value='3'>Selectie 2 (hoog - laag)</option>
                                <option value='4'>Selectie 2 (laag - hoog)</option>
                                <option value='5'>Selectie 3 (hoog - laag)</option>
                                <option value='6'>Selectie 3 (laag - hoog)</option>
                            </select>
                        </div>
                    </div>
                </div><br />
                <p>
                    <span>
                        Voorbeelden: <br>
                        - Gemiddelde Van Tarief per plaats<br>
                        - Activiteit T.o.v. Plaats (NVT - NVT)<br>
                        - Klant En Tarief per verkoper<br>
                    </span>
                </p>
                <p>
                    <em>
                        Uitleg: <br>
                        NVT in de laatste drie selectievelden zijn niet verplicht.<br>
                        Elke combinatie creeert een grafiek die wel/niet handig kan zijn. Gemiddelde van tarief per activiteit kan bijvoorbeeld iets zeggen hoeveel er nu werkelijk gemiddeld per activiteit gevraagd wordt.<br> Let wel op, er zijn ook mogelijkheden die iets opleveren, maar minder handig zijn. Tarief en Tarief per Tarief, zal niets zeggen, net zoals Gemiddelde Van Plaats. Activiteit per 'geschreven door' geeft<br> aan welke werknemer welke activiteit het meeste aanneemt, dat kan handig zijn. Wees creatief!<br>
                        <b></b>
                    </em>
                </p>
                <p>
                    <b><em>NB: <br>- Blijven de tijdsvakken leeg, dan wordt er gerekend vanaf dag één tot nu.
                        <br>- Selectievelden 4 & 5 moeten beide ingevuld zijn, anders worden ze beide genegeerd.
                        <br>- Het invullen van één datum is ook toegestaan. Bijvoorbeeld, vanaf 14-05-2018 tot nu of sinds het begin tot 12-04-2018.
                        <br>- 'Aantal' onder rekenkundig betekent aantal keer dat iets voorkomt.
                        <br>- De groep Onderwerp werkt NIET met 'van', overigen wel.
                    </em></b>
                </p>
                <div class="uk-form-row">
                    <div align="right">
                        <div class="md-btn-group">
                            <button type="reset" class="md-btn md-btn-warning md-btn-wave-light">RESET</button>
                            <button type="submit" class="md-btn md-btn-success md-btn-wave-light">Verder</button>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>

@endif
@endsection



@push('scripts')

    <script src="http://kendo.cdn.telerik.com/2.0/js/cultures/kendo.culture.nl-NL.min.js"></script>
    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>


    <!-- ckeditor -->
    <script src="{{ URL::asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/ckeditor/adapters/jquery.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>

    
    <!-- tinymce -->
    <script src="{{ URL::asset('assets/bower_components/tinymce/tinymce.min.js')}}"></script>

    <!--  wysiwyg editors functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_wysiwyg.min.js')}}"></script>
@endpush