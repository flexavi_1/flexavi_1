@extends('layouts.master')

@section('title', 'Materiaal bewerken')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <script src="{{ URL::asset('assets/bower_components/handlebars/handlebars.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/handlebars_helpers.min.js')}}"></script>

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

    <script src="{{ URL::asset('assets/assets/js/ckeditor/ckeditor.js')}}"></script>
    <script>
        // var CKEDITOR = document.querySelector('#editor');
        CKEDITOR.replace('editor');
    </script>

@endpush
@section('content')


@if(Auth::user()->rol != 99)

 <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large">
        <div class="md-card">
            <div class="md-card-content">
            <h3 class="heading_a">Materiaal Toevoegen</h3>
                {!! Form::open(array('url'=>'/materiaal/'.$materiaal->id.'/wijzigen', 'method' => 'post')) !!}
                <div class="uk-width-1-1">
                    <div class="uk-grid">
                        <div class="uk-width-1-3">
                            <div class="parsley-row">
                                <span class="uk-form-help-block">Naam</span>
                                <input type="text" placeholder="Bitumen dakbedekking" class="md-input label-fixed" id="d_form_amount" name="naam" value="{{$materiaal->naam}}" required>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="parsley-row">
                                <span class="uk-form-help-block">Omschrijving</span><br>
                                <textarea id='editor' placeholder="Bitumen dakbedekking, ook wel bitumineuze genoemd is meestal 4mm dik en is voorzien van polyester inlage wat het rekken en krimpen tegengaat" class="md-input" id="d_form_amount" name="omschrijving" required>{{$materiaal->omschrijving}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="uk-grid">
                    <div class="uk-width-1-1" align="right">
                        <button type="reset" href="#" class="md-btn md-btn-warning">Reset</button>
                        <button type="submit" href="#" class="md-btn md-btn-success">Wijzig materiaal</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endif



@endsection