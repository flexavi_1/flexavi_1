@extends('layouts.master')

@section('title', 'Voorraad')


@push('scripts')

    <script src="{{ URL::asset('assets/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{ URL::asset('assets/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    
    <!-- datatables custom integration -->
    <script src="{{ URL::asset('assets/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/plugins_datatables.min.js')}}"></script>

@endpush

@section('content')

@if(Auth::user()->rol != 99)

<h4 class="heading_a uk-margin-bottom">Voorraad</h4>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-striped">
                    <thead>
                    <tr>
                        @if(isset($mutaties))
                        <th><h3>Datum</h3></th>
                        <th><h3>Klant</h3></th>
                        @endif
                        <th><h3>Materiaal</h3></th>
                        <th><h3>Aantal</h3></th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($voorraad as $activiteit)
                    @if(!isset($mutaties))
                    <tr>
                        <td>@if($activiteit->aantal < 0) <h3 class="uk-text-bold md-color-red-A700">@else <h3 class="uk-text-bold md-color-green-A700">  @endif {{$activiteit->materiaal_id->naam}}</h3></td>
                        <td>@if($activiteit->aantal < 0) <h3 class="uk-text-bold md-color-red-A700">@else <h3 class="uk-text-bold md-color-green-A700">  @endif {{$activiteit->aantal}} </h3></td>
                    </tr>
                    @else
                    <tr>
                        <td>{{date('d-m-Y', strtotime($activiteit->datum))}}</td>
                        <td><a href="/relatie/{{$activiteit->klant_id->id}}">{{$activiteit->klant_id->postcode.$activiteit->klant_id->huisnummer." | ".$activiteit->klant_id->achternaam}}</a></td>
                        <td><h4> {{$activiteit->materiaal_id->naam}}</h4></td>
                        <td>@if($activiteit->mutatie < 0) <h4 class="uk-text-bold md-color-red-A700">@else <h4 class="uk-text-bold md-color-green-A700">  @endif {{$activiteit->mutatie}} </h4></td>
                    </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
 <div class="user_heading_content">
    @if(!isset($mutaties))
    <a class="md-btn md-btn-primary md-btn-wave-light" href="/voorraad/mutatie">Bekijk voorraad op mutatie niveau</a>
    @else
    <a class="md-btn md-btn-primary md-btn-wave-light" href="/voorraad">Bekijk de voorraad geagreggeerd</a>
    @endif
    <div class="uk-button-dropdown" data-uk-dropdown="{pos:'right-top'}">
        <button class="md-btn md-btn-warning">Zoek per Materiaal<i class="material-icons">&#xE315;</i></button>
        <div class="uk-dropdown">
            <ul class="uk-nav uk-nav-dropdown">
                @foreach($materialen as $m)
                    <li><a href="/voorraad/materiaal/{{$m->id}}">{{$m->naam}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
{{--     <a class="md-btn md-btn-primary md-btn-wave-light" href="">Afspraak maken</a>
    <a class="md-btn md-btn-primary md-btn-wave-light" href="">Geschiedenisregel toevoegen</a>
    <a class="md-btn md-btn-primary md-btn-wave-light" href="">Offerte toevoegen</a> --}}
</div> 


@endif





@endsection