@extends('layouts.master')

@section('title', 'Materiaal toevoegen')
@push('scripts')

    <!-- ionrangeslider -->
    <script src="{{ URL::asset('assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- htmleditor (codeMirror) -->
    <script src="{{ URL::asset('assets/assets/js/uikit_htmleditor_custom.min.js')}}"></script>
    <!-- inputmask-->
    <script src="{{ URL::asset('assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
        <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/forms_advanced.min.js')}}"></script>


    <script src="{{ URL::asset('assets/bower_components/handlebars/handlebars.min.js')}}"></script>
    <script src="{{ URL::asset('assets/assets/js/custom/handlebars_helpers.min.js')}}"></script>

    <!-- kendo UI -->
    <script src="{{ URL::asset('assets/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('assets/assets/js/pages/kendoui.min.js')}}"></script>

@endpush
@section('content')


@if(Auth::user()->rol != 99)

 <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large">
        <div class="md-card">
            <div class="md-card-content">
        	<h3 class="heading_a">Materiaal koppelen aan activiteit</h3>
    			{!! Form::open(array('url'=>'/activiteit/'.$activiteit->id.'/materiaal', 'method' => 'post')) !!}
                <div class="uk-width-medium">
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <p>{{$activiteit->omschrijving}} à € {{number_format($activiteit->prijs, 2)}} {{$activiteit->eenheid}}</p>
                        </div>
                    </div>
                </div>
                <div data-dynamic-fields="field_template_b"></div>
                <script id="field_template_b" type="text/x-handlebars-template">
    			
                <div class="uk-grid uk-grid-medium form_section form_section_separator" id="d_form_row" data-uk-grid-match>
                    <div class="uk-width-9-10">
                        <div class="uk-grid">
                            <div class="uk-width-2-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Materiaal</span>
                                    <select id="d_form_select_activity" name="materiaal[]" data-md-selectize required>
                                        @foreach($materialen as $activiteit)
                                        <option value="{{$activiteit->id}}">{{$activiteit->naam}}, {{$activiteit->omschrijving}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="parsley-row">
                                    <span class="uk-form-help-block">Aantal</span>
                                    <input type="number" step="0.01" class="md-input label-fixed" name="aantal[]" placeholder="5" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-10 uk-text-center">
                        <div class="uk-vertical-align uk-height-1-1">
                            <div class="uk-vertical-align-middle">
                                <a href="#" class="btnSectionClone" data-section-clone="#d_form_section"><i class="material-icons md-36">&#xE146;</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                 </script>

                <div class="uk-grid">
                    <div class="uk-width-1-1" align="right">
                        <button type="reset" href="#" class="md-btn md-btn-warning">Reset</button>
                        <button type="submit" href="#" class="md-btn md-btn-success">Voeg Materialen Toe</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endif



@endsection