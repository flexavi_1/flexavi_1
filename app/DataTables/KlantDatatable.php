<?php

namespace App\DataTables;

use App\Models\User;
use App\Models\Client;
// use Yajra\Datatables\Services\DataTable;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\QueryDataTable;


class KlantDatatable extends Datatables
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'klant.index')
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(\Illuminate\Contracts\Database\Query\Builder $builder): QueryDataTable
    {
        $query = Client::query();

        return $this->applyScopes($query);
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->addAction(['width' => '80px'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Naam',
            'Adres',
            'Contactgegevens',
            'Email',
            'Bankrekeningnummer',
            'Type Relatie',
            'Geschreven Door',
            'Verwerkt Door',
            'Aangemaakt op',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'klantdatatables_' . time();
    }
}
