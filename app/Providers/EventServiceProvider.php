<?php

namespace App\Providers;

use App\Models\Appointment;
use App\Models\History;
use App\Models\Client;
use App\Models\Search;
use App\Models\Status_has_appointment;
use App\Observers\AfspraakObserver;
use App\Observers\GeschiedenisObserver;
use App\Observers\KlantObserver;
use App\Observers\SearchObserver;
use App\Observers\StatusAfspraakObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
        Search::observe(SearchObserver::class);
        Client::observe(KlantObserver::class);
        History::observe(GeschiedenisObserver::class);
        Status_has_appointment::observe(StatusAfspraakObserver::class);
        Appointment::observe(AfspraakObserver::class);
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
