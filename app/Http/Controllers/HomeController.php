<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Request;

use App\Models\Postit;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(Auth::check())
            return redirect('/home');
        else
            return redirect('/login');
    }

    public function slaPostItOp()
    {
        $input = Request::all();
        if(Postit::where('user_id', Auth::user()->id)->count() < 8 || !Postit::where('user_id', Auth::user()->id)->exists())
            Postit::create($input)->save();
        else
            return redirect('/home')->with('danger', 'U kunt maximaal 4 Post-Its aanwezig hebben, verwijder eerst een Post-It.');    
        return redirect('/home');
    }

    public function home()
    {
        $postits = collect();
        if(Postit::where('user_id', Auth::user()->id)->exists())
            $postits = Postit::where('user_id', Auth::user()->id)->get();
        
        $page = "dashboard";
        $sub = "";
        return view('welcome', compact('postits', 'page', 'sub'));
    }

    public function removePI($id)
    {
        $pi = Postit::find($id);
        $pi->delete();

        return redirect('/dashboard');
    }
}
