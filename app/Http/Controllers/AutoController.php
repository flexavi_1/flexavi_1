<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Werknemer_has_auto;
use App\Models\Auto;
use App\Models\Log;
use Auth;
use DateTime;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class AutoController extends Controller
{


protected $htmlBuilder;
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $autos = Auto::all();
        foreach ($autos as $auto) {
            $kentekenzonderstreepjes = str_replace('-', '', $auto->kenteken);
            $kentekenhoofdletters = strtoupper($kentekenzonderstreepjes);
            $verzekerd = json_decode(file_get_contents('https://opendata.rdw.nl/resource/m9d7-ebf2.json?$$app_token=XZpvsChhFUPaGXv1pcRB8fqIM&kenteken='.$kentekenhoofdletters), true);
            if(count($verzekerd) == 0){
                $auto->verzekerd = "Ongeldig Kenteken";
            } else {
                $auto->verzekerd = $verzekerd[0]['wam_verzekerd'];
            }
        }
        $page = "wagenpark";
        $sub = "wagover";
        return view('auto.index', compact('autos', 'page', 'sub'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $page = "wagenpark";
        $sub = "wagtoev";
        return view('auto.toevoegen', compact('page', 'sub'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        $input = Request::all();
        foreach($input as $key => $e){
                // echo $key ." - ".$e.'<br>';
                if($key == "q"){
                    unset($input['q']);
                }
            }
        Auto::create($input)->save();

        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft de auto: ".$input['kenteken'].", toegevoegd.";
        $log->gebruiker_id = Auth::user()->id;
        $log->type = "Auto";
        $log->save();

        return redirect('/wagenpark')->with('succes', "Werkauto succesvol toegevoegd.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $auto = Auto::findOrFail($id);

        $page = "wagenpark";
        $sub = "wagover";
        return view('auto.wijzigen', compact('auto', 'page', 'sub'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $auto = Auto::findOrFail($id);
        $input = Request::all();
        foreach($input as $key => $e){
                // echo $key ." - ".$e.'<br>';
                if($key == "q"){
                    unset($input['q']);
                }
            }
        $auto->fill($input)->save();

        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft de auto: ".$input['kenteken'].", gewijzigd.";
        $log->gebruiker_id = Auth::user()->id;
            $log->type = "Auto";
        $log->save();

        return redirect('/wagenpark')->with('succes', 'U heeft de werkauto succesvol gewijzigd.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $auto = Auto::findOrFail($id);
        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft de auto: ".$auto->kenteken.", verwijderd.";
        $log->gebruiker_id = Auth::user()->id;
            $log->type = "Auto";
        $log->save();
        $auto->delete();

        return redirect('/wagenpark')->with('succes', 'U heeft de werkauto succesvol verwijderd.');
    }

    public function addHistory()
    {
        $autos = Auto::all();
        $werknemers = Employee::where('actief', 1)->get();

        $page = "wagenpark";
        $sub = "waggesch";

        return view('auto.geschiedenis', compact('autos', 'werknemers', 'page', 'sub'));
    }


    public function saveHistory()
    {
        $input = Request::all();

        $wha = new Werknemer_has_auto;
        $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['van']);
        $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['tot']);
        $startstamp = $strtotime->getTimestamp();
        $endstamp = $strtotimee->getTimestamp();
        $wha->van = date('Y-m-d H:i:s', $startstamp);
        $wha->tot = date('Y-m-d H:i:s', $endstamp);
        $wha->auto_id = $input['auto_id'];
        $wha->users_id = $input['users_id'];
        $wha->chauffeur = $input['chauffeur'];
        $wha->bijrijder = $input['bijrijder'];
        $wha->omschrijving = $input['omschrijving'];
        $wha->save();

        return redirect('/wagenpark')->with('succes', 'U heeft de autolijst bijgewerkt.');
    }

    public function showHistory()
    {
        $whas = Werknemer_has_auto::all();
        foreach($whas as $wha)
        {
            $wha->chauffeur = Employee::find($wha->chauffeur);
            $wha->bijrijder = Employee::find($wha->bijrijder);
            $wha->users_id = User::find($wha->users_id);
            $wha->auto_id = Auto::find($wha->auto_id);
        }


        // dd($whas);

        $page = "wagenpark";
        $sub = "autolijst";

        return view('auto.autolijst', compact('whas', 'page', 'sub'));
    }
}
