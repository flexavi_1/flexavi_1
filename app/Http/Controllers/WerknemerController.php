<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use Auth;
use DB;
use App\Models\Functie;
use App\Models\Functie_has_Werknemer;

use Yajra\Datatables\Html\Builder;
use App\DataTables\KlantDatatable;

class WerknemerController extends Controller
{

    protected $htmlBuilder;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(KlantDatatable $datatable)
    {
        //

        $werknemers = Employee::where('actief', 1)->get();
        $functies = Functie::all();
        $fhws = Functie_has_Werknemer::all();
        $users = User::all();

        $wns = array();

        foreach($werknemers as $werknemer)
        {
            foreach($functies as $functie)
            {
                foreach($fhws as $fwh)
                {
                    if($fwh->functie_id == $functie->id && $werknemer->id == $fwh->werknemer_id)
                    {
                        if($werknemer->actief != 1)
                        {// er worden nu dubbele records getoond als de werknemer enkele malen van functie wijzigt, dit moet afgevangen worden naar de meest recente functie.
                            array_push($wns, array(
                                'id' => $werknemer->id,
                                'voornaam' => $werknemer->voornaam,
                                'achternaam' => $werknemer->achternaam,
                                'straat' => $werknemer->straat,
                                'huisnummer' => $werknemer->huisnummer,
                                'hn_prefix' => $werknemer->hn_prefix,
                                'postcode' => $werknemer->postcode,
                                'woonplaats' => $werknemer->woonplaats,
                                'telefoonnummer_prive' => $werknemer->telefoonnummer_prive,
                                'telefoonnummer_zakelijk' => $werknemer->telefoonnummer_zakelijk,
                                'email' => $werknemer->email,
                                'actief' => "Niet in dienst",
                                'functie' => $functie->titel
                                ));
                        }
                        else
                        {
                            array_push($wns, array(
                                'id' => $werknemer->id,
                                'voornaam' => $werknemer->voornaam,
                                'achternaam' => $werknemer->achternaam,
                                'straat' => $werknemer->straat,
                                'huisnummer' => $werknemer->huisnummer,
                                'hn_prefix' => $werknemer->hn_prefix,
                                'postcode' => $werknemer->postcode,
                                'woonplaats' => $werknemer->woonplaats,
                                'telefoonnummer_prive' => $werknemer->telefoonnummer_prive,
                                'telefoonnummer_zakelijk' => $werknemer->telefoonnummer_zakelijk,
                                'email' => $werknemer->email,
                                'actief' => "In dienst",
                                'functie' => $functie->titel
                                ));
                        }
                    }
                }
            }
        }
        $page = "werknemer";
        $sub = "werkover";

        return view('werknemer.index', compact('wns', 'functies', 'page', 'sub'));
    }

    public function getType($type)
    {
        if(is_numeric($type)){
            $wns = Employee::where('actief', $type)->get();
            $functies = Functie::all();
        } else {
            $functies = Functie::where('titel', $type)->first();
            $wns = DB::select(DB::raw(" select f.titel as 'functie', w.*
                                        from werknemer as w
                                        join functie_has_werknemer fhw
                                        on fhw.werknemer_id = w.id
                                        join functie as f
                                        on f.id = fhw.functie_id
                                        where f.id = '".$functies->id."'"));
            $wns = collect($wns);
        }
        // $fhws = Functie_has_Werknemer::all();

        foreach ($wns as $w) {
            $w->actief = ($w->actief == 1) ? 'In dienst' : 'Niet in dienst';

            $functie_has_werknemer = Functie_has_Werknemer::where('werknemer_id', $w->id)->first();

            if ($functie_has_werknemer) {
                $functie_id = $functie_has_werknemer->functie_id;
                $functie = Functie::find($functie_id);

                if ($functie) {
                    $w->functie = $functie->titel;
                } else {
                    $w->functie = 'Unknown Functie';
                }
            } else {
                $w->functie = 'No Functie assigned';
            }
        }

        $functies = Functie::all();

        $page = "werknemer";
        $sub = "werkover";

        return view('werknemer.index', compact('wns', 'functies', 'page', 'sub'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $functies = Functie::all();

        $page = "werknemer";
        $sub = "werktoev";

       return view('werknemer.toevoegen', compact('functies', 'page', 'sub'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $input = Request::all();

        $werknemer = new Employee;
        $werknemer->voornaam = $input['voornaam'];
        $werknemer->achternaam = $input['achternaam'];
        $werknemer->straat = $input['straat'];
        $werknemer->huisnummer = $input['huisnummer'];
        $werknemer->hn_prefix = $input['hn_prefix'];
        $werknemer->postcode = $input['postcode'];
        $werknemer->woonplaats = $input['woonplaats'];
        $werknemer->telefoonnummer_prive = $input['telefoonnummer_prive'];
        $werknemer->telefoonnummer_zakelijk = $input['telefoonnummer_zakelijk'];
        $werknemer->email = $input['email'];
        $werknemer->bankrekeningnummer = $input['bankrekeningnummer'];
        $werknemer->actief = $input['actief'];
        $werknemer->save();

        $functie = Functie::find($input['functie_id']);
        $wn = Employee::where('postcode', $input['postcode'])->where('huisnummer', $input['huisnummer'])->where('achternaam', $input['achternaam'])->where('voornaam', $input['voornaam'])->first();
        $fhw = new Functie_has_Werknemer;
        $fhw->functie_id = $input['functie_id'];
        $fhw->werknemer_id = $wn->id;
        $fhw->verwerkt_door = Auth::user()->id;
        $input = (object) $input;
        //Check aanmaken op input
        $fhw->dagloon = $input->dagloon;
        $fhw->uurloon = $input->uurloon;
        $fhw->provisiebedrag = $input->provisiebedrag;
        $fhw->provisieperc = $input->provisieperc;
        $fhw->provisiegrens = $input->provisiegrens;
        $fhw->functie_id = 0;
        $fhw->save();

        //log

        return redirect('/werknemer')->with('succes', 'U heeft de werknemer succesvol toegevoegd.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $wn = Employee::findOrFail($id);
       $fhws = Functie_has_Werknemer::all();
       $functies = Functie::all();

        $page = "werknemer";
        $sub = "werkover";

       return view('werknemer.profiel', compact('wn', 'fhws', 'functies', 'page', 'sub'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $werknemer = Employee::findOrFail($id);
        $fwh = Functie_has_Werknemer::where('werknemer_id', $id)->orderBy('created_at')->first()->functie_id;
        $functies = Functie::all();

        $page = "werknemer";
        $sub = "werkover";

        return view('werknemer.wijzigen', compact('werknemer', 'fwh', 'functies', 'page', 'sub'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = Request::all();
        $werknemer = Employee::findOrFail($id);
        $werknemer->voornaam = $input['voornaam'];
        $werknemer->achternaam = $input['achternaam'];
        $werknemer->straat = $input['straat'];
        $werknemer->huisnummer = $input['huisnummer'];
        $werknemer->hn_prefix = $input['hn_prefix'];
        $werknemer->postcode = $input['postcode'];
        $werknemer->woonplaats = $input['woonplaats'];
        $werknemer->telefoonnummer_prive = $input['telefoonnummer_prive'];
        $werknemer->telefoonnummer_zakelijk = $input['telefoonnummer_zakelijk'];
        $werknemer->email = $input['email'];
        $werknemer->bankrekeningnummer = $input['bankrekeningnummer'];
        $werknemer->actief = $input['actief'];
        $werknemer->save();
        $werknemer = Employee::findOrFail($id);
        if(!Functie_has_Werknemer::where('functie_id', $input['functie_id'])->where('werknemer_id', $input['werknemer_id'])->exists())
        {
            $fhw = new Functie_has_Werknemer;
            $fhw->functie_id = $input['functie_id'];
            $fhw->werknemer_id = $werknemer->id;
            $fhw->verwerkt_door = Auth::user()->id;
            $input = (object) $input;
            $functie = Functie::find($fhw->functie_id);
            $fhw->dagloon = $input->dagloon;
            $fhw->uurloon = $input->uurloon;
            $fhw->provisiebedrag = $input->provisiebedrag;
            $fhw->provisieperc = $input->provisieperc;
            $fhw->provisiegrens = $input->provisiegrens;
            $fhw->save();
        }
        else
        {
            $fhw = Functie_has_Werknemer::where('functie_id', $input['functie_id'])->where('werknemer_id', $input['werknemer_id'])->first();
            $fhw->functie_id = $input['functie_id'];
            $fhw->werknemer_id = $werknemer->id;
            $fhw->verwerkt_door = Auth::user()->id;
            $input = (object) $input;
            $functie = Functie::find($fhw->functie_id);
            $fhw->dagloon = $input->dagloon;
            $fhw->uurloon = $input->uurloon;
            $fhw->provisiebedrag = $input->provisiebedrag;
            $fhw->provisieperc = $input->provisieperc;
            $fhw->provisiegrens = $input->provisiegrens;
            $fhw->save();
        }

        return redirect('/werknemer')->with('succes', 'Medewerker succesvol aangepast.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $relatie = Employee::findOrFail($id);
        if($relatie->actief == 1)
            $relatie->actief = 0;
        else
            $relatie->actief = 1;
        $relatie->save();

        return redirect('/werknemer')->with('succes', 'U heeft de werknemer succesvol op (non-)actief gezet.');
    }

    public function setUser()
    {
        $users = User::where('werknemer_id', 0)->get();
        // dd($users);
        $werknemers = Employee::where('toegewezen', 0)->get();

        $page = "werknemer";
        $sub = "werkkoppel";

        return view('werknemer.koppel', compact('users', 'werknemers', 'page', 'sub'));
    }

    public function storeUser()
    {
        $input = Request::all();
        $user = User::find($input['user_id']);
        $werknemer = Employee::find($input['werknemer_id']);
        $rol = Functie_has_Werknemer::where('werknemer_id', $werknemer->id)->first();
        $user->werknemer_id = $input['werknemer_id'];
        $user->rol = $rol->functie_id;
        $user->save();

        $werknemer = Employee::find($input['werknemer_id']);
        $werknemer->toegewezen = 1;
        $werknemer->save();

        return redirect('/werknemer')->with('succes', 'U heeft de werknemer succesvol gekoppeld aan de gebruiker.');
    }

    public function showUsers()
    {
        $users = User::with('werknemer')->where('werknemer_id', '!=', 0)->get();
        // dd($users);

        $page = "controle";
        $sub = "gebr";
        return view('werknemer.controle', compact('users', 'page', 'sub'));
    }

    public function nonactiveUser($id)
    {
        $user = User::find($id);
        // dd($user);
        if($user->actief == 0)
        {
            $user->actief = 1;
            $user->save();
        }
        else
        {
            $user->actief = 0;
            $user->save();
        }

        return redirect('/controle/gebruikers')->with('succes', 'U heeft de gebruiker succesvol gewijzigd.');

    }
}
