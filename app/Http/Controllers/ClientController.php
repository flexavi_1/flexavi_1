<?php

namespace App\Http\Controllers;

use App\Models\Appointment_has_activities;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\Bedrijf;
use App\Models\Crediteur;
use App\DataTables\KlantDatatable;
use App\Models\History;
use App\Models\Inkoop;
use App\Models\Client;
use App\Models\ClientEmail;
use App\Models\ClientType;
use App\Models\Klant_has_status;
use App\Models\Leveranciersprijslijst;
use App\Models\Log;
use App\Models\Mails;
use App\Models\Offerte_has_status;
use App\Models\Status;
use App\Models\Status_has_appointment;
use App\Models\Status_has_factuur;
use App\Models\User;
use App\Models\Employee;
use Auth;
use Datatables;
use DB;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
//

class ClientController extends Controller
{

    protected $htmlBuilder;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @description Display a listing of the customers.
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function index(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('klant.index');
    }

    /**
     * @description Retrieves (filtered) data for filling the datatables
     * @return JsonResponse
     */
    public function getDatatable(): \Illuminate\Http\JsonResponse
    {
//        dd($_GET);

        $clienten = DB::table('klant')
                ->selectRaw("CONCAT('<a style=\"text-decoration: underline;\" href=\"/relatie/', klant.id, '\">',klant.voornaam,' ', klant.achternaam, '</a>') as 'naam',
                    CONCAT(klant.straat,' ',klant.huisnummer,'', klant.hn_prefix, ',<br>',klant.postcode, ' te ', klant.woonplaats) as adres,
                    CONCAT(klant.telefoonnummer_prive,' - ', klant.telefoonnummer_zakelijk, '<br>', klant.email) as contact,
                    users.name as verwerktdoor,
                    CONCAT(werknemer.voornaam,' ', werknemer.achternaam) as 'geschrevendoor',
                    DATE_FORMAT(klant.geschreven_op, '%d-%m-%Y') as 'geschreven_op'")
                ->join('users', 'users.id', '=', 'klant.verwerkt_door')
                ->join('werknemer', 'werknemer.id', '=', 'klant.geschreven_door')
                ->where('klant.klanttype_id', '=', '3');

        $filter = false;
        if (!empty($_GET['search']['value'])) {
            $filter = true;
            $clienten = $clienten->whereRaw("CONCAT('<a href=\"/relatie/', klant.id, '\">',klant.voornaam,' ', klant.achternaam, '</a>') like '%".$_GET['search']['value']."%'")
                ->orWhereRaw("CONCAT(klant.straat,' ',klant.huisnummer,'', klant.hn_prefix, ',<br>',klant.postcode, ' te ', klant.woonplaats) like '%". $_GET['search']['value']. "%'")
                ->orWhereRaw("CONCAT(klant.telefoonnummer_prive,' - ', klant.telefoonnummer_zakelijk, '<br>', klant.email) like '%". $_GET['search']['value']. "%'")
                ->orWhereRaw('users.name like "%'. $_GET['search']['value']. '%"')
                ->orWhereRaw("CONCAT(werknemer.voornaam,' ', werknemer.achternaam) like '%". $_GET['search']['value']. "%'")
                ->orWhereRaw("DATE_FORMAT(klant.geschreven_op, '%d-%m-%Y') like '%". $_GET['search']['value']. "%'");
        }
        //define sorting
        switch($_GET['order'][0]['column']){
            case 0:
                $clienten = $clienten->orderBy('naam', $_GET['order'][0]['dir']);
                break;
            case 1:
                $clienten = $clienten->orderBy('adres', $_GET['order'][0]['dir']);
                break;
            case 2:
                $clienten = $clienten->orderBy('contact', $_GET['order'][0]['dir']);
                break;
            case 3:
                $clienten = $clienten->orderBy('verwerktdoor', $_GET['order'][0]['dir']);
                break;
            case 4:
                $clienten = $clienten->orderBy('geschrevendoor', $_GET['order'][0]['dir']);
                break;
            case 5:
                $clienten = $clienten->orderBy('klant.geschreven_op', $_GET['order'][0]['dir']);
                break;
            default:
                $clienten = $clienten->orderBy('klant.created_at', 'desc');
                break;
        }
        $recordsFiltered = ($filter) ? $clienten->count(): Client::count();
        //define skips
        $clienten = $clienten->skip($_GET['start'])
                    ->take($_GET['length'])
                    ->get();

        $returnArray = [
            'draw' => $_GET['draw'],
            'recordsTotal' => Client::count(),
            'recordsFiltered' => $recordsFiltered,
            'data' => $clienten->toArray()
        ];
        return response()->json($returnArray);
    }

    /**
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function create(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $clienttypes = ClientType::all();
        $werknemers = Employee::whereActief(1)->get();
        return view('klant.toevoegen', compact('clienttypes', 'werknemers'));
    }

    /**
     * @description Store a newly created customer in database.
     * @param Request $request
     * @return Application|Redirector|RedirectResponse|\Illuminate\Contracts\Foundation\Application
     */
    public function store(Request $request): Application|Redirector|RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $input = $request->all();
        if($input['woonplaats'] == null){
            return redirect('/relatie/nieuw')->with('danger', 'Ongeldige adresgegevens');
        }
        if(Client::wherePostcode($input['postcode'])->whereHuisnummer($input['huisnummer'])->exists()){
            return redirect('/relatie/nieuw')->with('danger', 'Deze klant komt al voor in het systeem, wijzig/verwijder deze klant eerst.');
        }
        $client = new Client;
        $client->fill($input);
        $client->initiatief = isset($input['initiatief'])? 1 : 0;
        $client->geschreven_op = date('Y-m-d', strtotime($input['geschreven_op']));
        if($client->voornaam == null) {
            $client->voornaam = "";
        }
        if($client->hn_prefix == null) {
            $client->hn_prefix = "";
        }
        if($client->telefoonnummer_zakelijk == null){
            $client->telefoonnummer_zakelijk = "";
        }
        if($client->email == null) {
            $client->email = "";
        }
        $client->save();
        if ($client->email != '' || $client->email != null) {
            $ke = new ClientEmail;
            $ke->klant_id = $client->id;
            $ke->email = $client->email;
            $ke->save();
        }
        return redirect('/relatie/' . $client->id)->with('succes', 'Relatie succesvol toegevoegd.');
    }

    public function OLDshow($id)
    {

        //

        $relatie = Client::find($id);

        if ($relatie) {


            // dd($relatie->klanttype);

            switch ($relatie->klanttype_id) {

                case 3: // Klant

                    $emailString = "";

                    $emails = ClientEmail::where('klant_id', $relatie->id)->get();
                    // dd(implode(', ', $relatie->klantemail->toArray()));
                    // dd($relatie->klantemail->toArray());
                    foreach ($relatie->klantemail as $e) {

                        $emailString .= $e->email . "; ";
                    }

                    $relatie->emailString = $emailString;

                    // $regels = Geschiedenis::with('afspraak')->where('klant_id', $id)->orderBy('created_at', 'DESC')->orderBy('afspraak_id', 'DESC')->get();

                    $regels = History::with('afspraak')->where('klant_id', $id)->orderByRaw('datum DESC')->get();

                    // dd($regels);

                    $array = array();

                    $id_new = 0;

                    $count = 0;

                    $sentence = "";

                    //we need to aggregate history rules for visibility in customer profile!

                    $rules = History::where('klant_id', $id)->orderByRaw('datum DESC, created_at DESC')->get();

                    foreach ($rules as $r) {

                        if ($r->afspraak_id != null) {

                            $r->afspraak = Appointment::find($r->afspraak_id);

                            if ($r->afspraak == null) {

                                // $r->afspraak_id = null;

                            }
                        } else {

                            $r->afspraak = null;
                        }
                    }

                    foreach ($rules as $r) {

                        if ($id_new == 0) {

                            $id_new = $r->afspraak_id;
                        }

                        // echo $id_new.'<br>';

                        if ($r->afspraak_id != $id_new) {

                            $id_new = $r->afspraak_id;
                        }

                        if ($r->afspraak_id == $id_new && $r->afspraak_id != null) {

                            //it means this is the history for the same appointment

                            $check = false;

                            $adviesCount = 0;

                            for ($i = 0; $i < count($array); $i++) {

                                if ($array[$i]['afspraak_id'] == $id_new) {

                                    $check = true;

                                    $array[$i]['omschrijving'] .= '<br>' . $r->omschrijving;

                                    $array[$i]['id'] .= ',' . $r->id;

                                    $array[$i]['gebruiker_id'] .= ',' . $r->gebruiker_id;

                                    $array[$i]['bijgewerkt_door'] .= ',' . $r->bijgewerkt_door;
                                }
                            }

                            if (!$check) {

                                if ($r->afspraak == null) {

                                    $date = 'Afspraak is verwijderd';

                                    $advies = 'Geen advies bekend, afspraak is verwijderd.';

                                    $type = "";
                                } else {

                                    $date = date('d-m-Y', strtotime($r->afspraak->startdatum));

                                    $advies = $r->afspraak->advies;

                                    $testAfspraak = Appointment::find($r->afspraak_id);

                                    if (!$testAfspraak) {

                                        $testAfspraak = Appointment::find($r->afspraak->id);
                                    }

                                    $type = $testAfspraak->getType();

                                    $type = $type->omschrijving;
                                }

                                array_push($array, [
                                    'volgorde' => $count, 'afspraak_id' => $id_new, 'omschrijving' => 'Betreft Afspraak: <span class="uk-text-bold"><i class="uk-text-primary">' . $date . '</i> -<i class="uk-text-warning"> ' . $type . '</i></span> - ' . History::getSalesman($id_new) . ':<br> ' . $r->omschrijving,

                                    'created_at' => $r->created_at, 'datum' => $r->datum, 'updated_at' => $r->updated_at, 'gebruiker_id' => $r->gebruiker_id, 'bijgewerkt_door' => $r->bijgewerkt_door, 'id' => $r->id, 'advies' => 'Advies: ' . $advies
                                ]);
                            }
                        }

                        if ($r->afspraak_id == null) {

                            array_push($array, [
                                'volgorde' => $count, 'afspraak_id' => null, 'omschrijving' => $r->omschrijving,

                                'created_at' => $r->created_at, 'datum' => $r->datum, 'updated_at' => $r->updated_at, 'gebruiker_id' => $r->gebruiker_id, 'bijgewerkt_door' => $r->bijgewerkt_door, 'id' => $r->id
                            ]);
                        }

                        $count++;
                    }

                    // dd($array);

                    $types = AppointmentType::all();

                    #get the first upcoming appointment for the customer

                    $afspraak = Appointment::where('klant_id', $id)->where('startdatum', '>', date('Y-m-d'))->orderBy('startdatum', 'asc')->first();

                    if ($afspraak == null) {

                        $afspraak = "";
                    }

                    #get all appointments combined with the workactivities

                    // $actperafs = Activiteit_has_Afspraak::with('afspraak', 'activiteit')->whereHas('afspraak', function ($afs) use ($relatie) {

                    //     $afs->where('klant_id', $relatie->id);
                    // })->get();
                    $actperafs = null;

                    #get all appointments combined with their status

                    $alleafspraken = Status_has_appointment::with('afspraak', 'status')->whereHas('afspraak', function ($afd) use ($relatie) {

                        $afd->where('klant_id', $relatie->id)->groupBy('status_id');
                    })->orderBy('id', 'desc')->orderBy('created_at', 'desc')->get();

                    $totaalbedrag = 0.00;

                    $alleafspraken = $alleafspraken->sortByDesc(function ($afspraak) {

                        return $afspraak->afspraak->startdatum;
                    });

                    // foreach ($alleafspraken as $aa) {

                    //     if ($aa->afspraak_id != null) {

                    //         if ($aa->status_id == 4) {

                    //             $ahas = Activiteit_has_Afspraak::where('afspraak_id', $aa->afspraak->id)->get();

                    //             foreach ($ahas as $aha) {

                    //                 $totaalbedrag += ($aha->aantal * $aha->prijs - $aha->kortingsbedrag);
                    //             }
                    //         }
                    //     }
                    // }

                    $allefacturen = Status_has_factuur::with('factuur', 'status')->whereHas('factuur', function ($fac) use ($relatie) {

                        $fac->where('klant_id', $relatie->id);
                    })->get();

                    $alleoffertes = Offerte_has_status::with('offerte', 'status')->whereHas('offerte', function ($fac) use ($relatie) {

                        $fac->where('klant_id', $relatie->id);
                    })->get();

                    // $werknemers = Werknemer::all();

                    $crediteuren = Crediteur::where('klant_id', $relatie->id)->get();
                    $danger = "";
                    if ($relatie->actief == 0) {
                        $danger = "Let op, deze klant is verwijderd.";
                    }

                    //retrieve all posible 15 euro customers

                    // $vijftien = DB::table('afspraak')

                    //                 ->leftJoin('mail', 'afspraak.id', '=', 'mail.afspraak_id')

                    //                 ->where('afspraak.eigenschuld', '=', '0')

                    //                 ->where('afspraak.klant_id', '=', $relatie->id)

                    //                 ->select('afspraak.*', "afspraak.id as afspraakID")

                    //                 ->get();

                    // $vijftien = "";

                    // // $vijftien = collect($vijftien);

                    // // dd($vijftien);

                    // foreach($vijftien as $v)

                    // {

                    //     $v->afspraaktype_id = Afspraaktype::find($v->afspraaktype_id);

                    //     $v->verwerkt_door = User::find($v->verwerkt_door);

                    //     $v->status = Status_has_afspraak::where('afspraak_id', $v->afspraakID)->first();

                    // }


                    $mails = Mails::where('klant_id', $relatie->id)->get();

                    $page = "relatie";

                    $sub = "relaover";

                    return view('klant.profiel', compact(
                        'relatie',
                        'mails',
                        'afspraak',
                        'regels',
                        'alleafspraken',
                        'totaalbedrag',
                        'allefacturen',
                        'crediteuren',
                        'alleoffertes',
                        'danger',
                        'page',
                        'sub',
                        'array'
                    ));

                    break;

                default:

                    $relatie = Client::findOrFail($id);

                    $clienttype = ClientType::findOrFail($relatie->klanttype_id);

                    $werknemer = Employee::findOrFail($relatie->geschreven_door);

                    $user = User::findOrFail($relatie->verwerkt_door);

                    $users = User::all();

                    $emailString = "";

                    $emails = ClientEmail::where('klant_id', $relatie->id)->get();

                    foreach ($emails as $e) {

                        $emailString .= $e->email . "; ";
                    }

                    $relatie->emailString = $emailString;

                    // $regels = Geschiedenis::with('afspraak')->where('klant_id', $id)->orderBy('created_at', 'DESC')->orderBy('afspraak_id', 'DESC')->get();

                    $regels = History::with('afspraak')->where('klant_id', $id)->orderByRaw('datum DESC')->get();

                    // dd($regels);

                    $array = array();

                    $id_new = 0;

                    $count = 0;

                    $sentence = "";

                    //we need to aggregate history rules for visibility in customer profile!

                    $rules = History::where('klant_id', $id)->orderByRaw('datum DESC, created_at DESC')->get();

                    foreach ($rules as $r) {

                        if ($r->afspraak_id != null) {

                            $r->afspraak = Appointment::find($r->afspraak_id);

                            if ($r->afspraak == null) {

                                // $r->afspraak_id = null;

                            }
                        } else {

                            $r->afspraak = null;
                        }
                    }

                    foreach ($rules as $r) {

                        if ($id_new == 0) {

                            $id_new = $r->afspraak_id;
                        }

                        // echo $id_new.'<br>';

                        if ($r->afspraak_id != $id_new) {

                            $id_new = $r->afspraak_id;
                        }

                        if ($r->afspraak_id == $id_new && $r->afspraak_id != null) {

                            //it means this is the history for the same appointment

                            $check = false;

                            $adviesCount = 0;

                            for ($i = 0; $i < count($array); $i++) {

                                if ($array[$i]['afspraak_id'] == $id_new) {

                                    $check = true;

                                    $array[$i]['omschrijving'] .= '<br>' . $r->omschrijving;

                                    $array[$i]['id'] .= ',' . $r->id;

                                    $array[$i]['gebruiker_id'] .= ',' . $r->gebruiker_id;

                                    $array[$i]['bijgewerkt_door'] .= ',' . $r->bijgewerkt_door;
                                }
                            }

                            if (!$check) {

                                if ($r->afspraak == null) {

                                    $date = 'Afspraak is verwijderd';

                                    $advies = 'Geen advies bekend, afspraak is verwijderd.';

                                    $type = "";
                                } else {

                                    $date = date('d-m-Y', strtotime($r->afspraak->startdatum));

                                    $advies = $r->afspraak->advies;

                                    $testAfspraak = Appointment::find($r->afspraak_id);

                                    if (!$testAfspraak) {

                                        $testAfspraak = Appointment::find($r->afspraak->id);
                                    }

                                    $type = $testAfspraak->getType();

                                    $type = $type->omschrijving;
                                }

                                array_push($array, [
                                    'volgorde' => $count, 'afspraak_id' => $id_new, 'omschrijving' => 'Betreft Afspraak: <span class="uk-text-bold"><i class="uk-text-primary">' . $date . '</i> -<i class="uk-text-warning"> ' . $type . '</i></span> - ' . History::getSalesman($id_new) . ':<br> ' . $r->omschrijving,

                                    'created_at' => $r->created_at, 'datum' => $r->datum, 'updated_at' => $r->updated_at, 'gebruiker_id' => $r->gebruiker_id, 'bijgewerkt_door' => $r->bijgewerkt_door, 'id' => $r->id, 'advies' => 'Advies: ' . $advies
                                ]);
                            }
                        }

                        if ($r->afspraak_id == null) {

                            array_push($array, [
                                'volgorde' => $count, 'afspraak_id' => null, 'omschrijving' => $r->omschrijving,

                                'created_at' => $r->created_at, 'datum' => $r->datum, 'updated_at' => $r->updated_at, 'gebruiker_id' => $r->gebruiker_id, 'bijgewerkt_door' => $r->bijgewerkt_door, 'id' => $r->id
                            ]);
                        }

                        $count++;
                    }

                    // dd($array);

                    $types = AppointmentType::all();

                    #get the first upcoming appointment for the customer

                    $afspraak = Appointment::where('klant_id', $id)->where('startdatum', '>', date('Y-m-d'))->orderBy('startdatum', 'asc')->first();

                    if ($afspraak == null) {

                        $afspraak = "";
                    }

                    #get all appointments combined with the workactivities

                    $actperafs = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function ($afs) use ($relatie) {

                        $afs->where('klant_id', $relatie->id);
                    })->get();

                    #get all appointments combined with their status

                    $alleafspraken = Status_has_appointment::with('afspraak', 'status')->whereHas('afspraak', function ($afd) use ($relatie, $types) {

                        $afd->where('klant_id', $relatie->id)->groupBy('status_id');
                    })->orderBy('id', 'desc')->orderBy('created_at', 'desc')->get();

                    $totaalbedrag = 0.00;

                    foreach ($alleafspraken as $t) {

                        foreach ($types as $type) {

                            if ($t->afspraak->afspraaktype_id == $type->id) {
                                $t->afspraak->afspraaktype = $type->omschrijving;
                            }
                        }
                    }

                    $alleafspraken = $alleafspraken->sortByDesc(function ($afspraak) {

                        return $afspraak->afspraak->startdatum;
                    });

                    foreach ($alleafspraken as $aa) {

                        if ($aa->afspraak_id != null) {

                            if ($aa->status_id == 4) {

                                $ahas = Appointment_has_activities::where('afspraak_id', $aa->afspraak->id)->get();

                                foreach ($ahas as $aha) {

                                    $totaalbedrag += ($aha->aantal * $aha->prijs - $aha->kortingsbedrag);
                                }
                            }
                        }
                    }

                    $allefacturen = Status_has_factuur::with('factuur', 'status')->whereHas('factuur', function ($fac) use ($relatie) {

                        $fac->where('klant_id', $relatie->id);
                    })->get();

                    $alleoffertes = Offerte_has_status::with('offerte', 'status')->whereHas('offerte', function ($fac) use ($relatie) {

                        $fac->where('klant_id', $relatie->id);
                    })->get();

                    $werknemers = Employee::all();

                    $crediteuren = Crediteur::where('klant_id', $relatie->id)->get();

                    if ($relatie->actief == 0) {

                        $danger = "Let op, deze klant is verwijderd.";
                    } else {

                        $danger = "";
                    }

                    //retrieve all posible 15 euro customers

                    // $vijftien = DB::table('afspraak')

                    //                 ->leftJoin('mail', 'afspraak.id', '=', 'mail.afspraak_id')

                    //                 ->where('afspraak.eigenschuld', '=', '0')

                    //                 ->where('afspraak.klant_id', '=', $relatie->id)

                    //                 ->select('afspraak.*', "afspraak.id as afspraakID")

                    //                 ->get();

                    // $vijftien = "";

                    // // $vijftien = collect($vijftien);

                    // // dd($vijftien);

                    // foreach($vijftien as $v)

                    // {

                    //     $v->afspraaktype_id = Afspraaktype::find($v->afspraaktype_id);

                    //     $v->verwerkt_door = User::find($v->verwerkt_door);

                    //     $v->status = Status_has_afspraak::where('afspraak_id', $v->afspraakID)->first();

                    // }


                    $mails = Mails::where('klant_id', $relatie->id)->get();

                    $page = "relatie";

                    $sub = "relaover";

                    return view('klant.profiel', compact(
                        'relatie',
                        'mails',
                        'klanttype',
                        'werknemer',
                        'user',
                        'afspraak',

                        'regels',
                        'users',
                        'alleafspraken',
                        'actperafs',
                        'totaalbedrag',
                        'allefacturen',

                        'crediteuren',
                        'alleoffertes',
                        'werknemers',
                        'danger',
                        'page',
                        'sub',
                        'array'
                    ));

                    break;
            }
        } else {
            dd("Klant niet gevonden");
        }
    }

    public function show(Client $client){
//        dd($client);
        return view('klant.profile.profiel', compact('client'));
    }

    /**
     * @description Displays the edit form for a customer
     * @param Client $client
     * @return Application|Factory|View|Redirector|RedirectResponse|\Illuminate\Contracts\Foundation\Application
     */
    public function edit(Client $client): Application|Factory|View|Redirector|RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        //
        if(!$client){
              return redirect('/relatie')->with('danger', 'Klant niet gevonden');
        }
        $clienttypes = ClientType::all();
        $employees = Employee::whereActief(1)->get();
        return view('klant.wijzigen', compact('client', 'clienttypes', 'employees'));
    }

    /**
     * @description Updates a customer
     * @param Request $request
     * @param $id - Customer ID
     * @return Application|Redirector|RedirectResponse|\Illuminate\Contracts\Foundation\Application
     */
    public function update(Request $request, Client $client): Application|Redirector|RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $input = $request->all();
//        $client = Client::find($id);
        $oldEmail = $client->email;

        $client->fill($input);
        $client->initiatief = isset($input['initiatief'])? 1 : 0;
        $client->geschreven_op = date('Y-m-d', strtotime($input['geschreven_op']));
        if($client->voornaam == null) {
            $client->voornaam = "";
        }
        if($client->hn_prefix == null) {
            $client->hn_prefix = "";
        }
        if($client->email == null) {
            $client->email = "";
        }
        if($client->telefoonnummer_zakelijk == null){
            $client->telefoonnummer_zakelijk = "";
        }
        $client->save();
        if ($client->email != '' || $client->email != null) {
            if ($client->email != $oldEmail) {
                //add new email
                $ke = new ClientEmail;
                $ke->klant_id = $client->id;
                $ke->email = $client->email;
                $ke->save();
                //delete old email if not the same
                $clientEmail = ClientEmail::whereKlantId($client->id)->whereEmail($oldEmail)->first();
                if($clientEmail){
                    $clientEmail->delete();
                }
            }
        }
        return redirect('/relatie/' . $client->id)->with('succes', 'Relatie succesvol toegevoegd.');
    }


    /**
     * @description Soft Deletes a customer
     * @param $id
     * @return Application|Redirector|RedirectResponse|\Illuminate\Contracts\Foundation\Application
     */
    public function destroy($id): Application|Redirector|RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $relatie = Client::find($id);
        if(!$relatie){
            return redirect('/relatie')->with('danger', 'Klant niet gevonden');
        }
        $relatie->actief = 0;
        $relatie->bijgewerkt_door = Auth::user()->id;
        $relatie->save();
        Log::createLog(Auth::user()->id, $relatie->id, 'Relatie', Auth::user()->name . " heeft een relatie verwijderd, Postcode+huisnummer: " . $relatie->postcode . $relatie->huisnummer);
        return redirect('/relatie')->with('succes', 'U heeft de relatie succesvol verwijderd.');
    }

    public function showDeleted()
    {

        $relaties = Client::where('actief', 0)->get();

        foreach ($relaties as $r) {

            $r->bijgewerkt_door = User::find($r->bijgewerkt_door);
        }

        $page = "controle";

        $sub = "verwij";

        return view('klant.controleVerwijderd', compact('relaties',  'page', 'sub'));
    }

    public function setActive($id)
    {

        $client = Client::find($id);

        $client->actief = 1;

        $client->save();

        return redirect('/relatie/' . $id)->with('succes', 'De klant is hersteld.');
    }

    public function hardDelete($id)
    {

        $client = Client::find($id);

        $client->actief = 2;

        $client->save();

        return redirect('/controle/verwijderde-relaties')->with('succes', 'Klant definitief verwijderd');
    }

    public function changePost($id, $mail_id, $var)
    {

        $mail = Mails::find($mail_id);

        $mail->post = $var;

        $mail->save();

        return redirect('/relatie/' . $id)->with('succes', 'Status van de mail/brief gewijzigd.');
    }

    public function addEmail($id)
    {

        $page = '';

        $sub = '';

        return view('klant.email', compact('page', 'sub', 'id'));
    }

    public function storeEmail($id)
    {

        $client = Client::find($id);

        $input = Request::all();

        if (!ClientEmail::where('klant_id', $client->id)->where('email', $input['email'])->exists()) {

            $ke = new ClientEmail;

            $ke->klant_id = $client->id;

            $ke->email = $input['email'];

            // $k->email = null;

            $client->save();

            $ke->save();
        } else {

            return redirect('/relatie/' . $id)->with('danger', 'Dit emailadres bestaat al.');
        }

        return redirect('/relatie/' . $id)->with('succes', 'U heeft een nieuw emailadres toegevoegd.');
    }

    public function refreshAddresses()
    {

        $clienten = Client::where('woonplaats', "")->get();

        $bedrijf = Bedrijf::find(1);

        foreach ($clienten as $k) {

            $pc = $k->postcode;

            $hn = $k->huisnummer;

            $url = "https://api.pro6pp.nl/v1/autocomplete?auth_key=" . $bedrijf->pro66p . "&nl_sixpp=" . urlencode($pc) .

                "&streetnumber=" . $hn;

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 10000);

            $result = curl_exec($ch);

            curl_close($ch);

            if ($result) {

                // Show raw output

                // print("Output: " . $result . "<br/>");

                $output = json_decode($result);

                $status = $output->{"status"};

                if ($status === "ok") {

                    echo "<br>Aangepaste klant: " . $k->id;

                    // print("Found results. The first one:<br/>");

                    // print($output->{"results"}[0]->{"street"} . ",<br/>" .

                    // $output->{"results"}[0]->{"nl_sixpp"} . ",<br/>" .

                    // $output->{"results"}[0]->{"city"});

                    $k->straat = $output->{"results"}[0]->{"street"};

                    $k->woonplaats = $output->{"results"}[0]->{"city"};

                    $k->save();
                } else {

                    // Error.

                    echo "<br> Error bij klant: " . $k->id;

                    $message = $output->{"error"}->{"message"};

                    print("<br>Error message: " . $message);
                }
            } else {

                print("Pro6PP is unavailable at this time");
            }
        }
    }

    public function refreshEmails()
    {

        $clienten = Client::all();

        foreach ($clienten as $k) {

            if ($k->email != null || $k->email != "") {

                if (!ClientEmail::where('klant_id', $k->id)->where('email', $k->email)->exists()) {

                    $ke = new ClientEmail;

                    $ke->klant_id = $k->id;

                    $ke->email = $k->email;

                    $ke->save();
                }
            }
        }

        // dd('done');
    }
}
