<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\Client;
use App\Models\Klant_has_status;
use App\Models\Log;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReminderController extends Controller
{
    //
    /**
     * Only create instance when user is authenticated.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @description Shows the reminder page
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function index(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $relaties = DB::select("
                SELECT
                    khs.status_id,
                    s.omschrijving,
                    DATEDIFF(CURRENT_DATE(), khs.updated_at) AS 'verschil',
                    DATE(DATE_ADD(khs.updated_at, INTERVAL 1 YEAR)) AS 'reminderdatum',
                    DATE(khs.updated_at) AS 'laatste_update',
                    k.*
                FROM
                    klant AS k
                JOIN
                    klant_has_status AS khs ON k.id = khs.klant_id
                JOIN
                    status AS s ON s.id = khs.status_id
                WHERE
                    DATEDIFF(CURRENT_DATE(), khs.updated_at) > 330
                    AND khs.status_id = 21
                ORDER BY
                    DATEDIFF(CURRENT_DATE(), khs.updated_at) DESC
            ");

        foreach ($relaties as $relatie) {
            $relatie->reminderdatum = date('d-m-Y', strtotime($relatie->reminderdatum));
        }

        return view('klant.reminder', compact('relaties'));
    }

    /**
     * @description Marks this client reminder as processed
     * @param Client $id
     * @return Application|Redirector|RedirectResponse|\Illuminate\Contracts\Foundation\Application
     */
    public function processed(Client $id): Application|Redirector|RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {

        $relatie = $id;
        $khs = Klant_has_status::whereKlantId($relatie->id)->first();
        $khs->status_id = 22;
        $khs->verwerkt_door = Auth::user()->id;
        $khs->save();

        return redirect('/relatie/' . $relatie->id)->with('succes', 'Reminder succesvol behandeld, vul de geschiedenis aan voor extra informatie.');
    }
}
