<?php

namespace App\Http\Controllers;
use App\Models\Work;
use App\Models\AppointmentType;
use App\Models\Employee;
use App\Models\Appointment;
use App\Models\Client;
use App\Models\Bedrijf;
use App\Models\Worklist_has_appointments;
use App\Models\Appointment_has_activities;
use App\Models\Status_has_appointment;
use Log;

class DatamartController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
    */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function loadActivity(){
    	$activiteiten = Work::all();
    	$bedrijf = Bedrijf::find(1);
    	$dm_url = $bedrijf->dm_url;
    	foreach($activiteiten as $a){
	    	$url = $dm_url.'/api/loadActivities';
	    	$post = array();
	    	$post = (object) $post;
	    	$post->name = $a->omschrijving;
	    	$post = (array) $post;
	    	$ch = curl_init($url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

	        // response contains channel id
	        $response = curl_exec($ch);

	        // close the connection, release resources used
	        curl_close($ch);

			Log::info($response);
	    }
	}

	public function loadEmployee(){
    	$bedrijf = Bedrijf::find(1);
    	$dm_url = $bedrijf->dm_url;
	    $werknemers = Employee::all();
    	foreach($werknemers as $a){
	    	$url = $dm_url.'/api/loadEmployees';
	    	$post = array();
	    	$post = (object) $post;
	    	$post->name = $a->voornaam;
	    	$post->lastname = $a->achternaam;
	    	$post = (array) $post;
	    	$ch = curl_init($url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

	        // response contains channel id
	        $response = curl_exec($ch);

	        // close the connection, release resources used
	        curl_close($ch);

	        Log::info($response);
	    }
	}

	public function loadTypes(){
    	$bedrijf = Bedrijf::find(1);
    	$dm_url = $bedrijf->dm_url;
	    $afspraaktypes = AppointmentType::all();
    	foreach($afspraaktypes as $a){
	    	$url = $dm_url.'/api/loadTypes';
	    	$post = array();
	    	$post = (object) $post;
	    	$post->type = $a->omschrijving;
	    	$post = (array) $post;
	    	$ch = curl_init($url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

	        // response contains channel id
	        $response = curl_exec($ch);

	        // close the connection, release resources used
	        curl_close($ch);

	        Log::info($response);
	    }
	}

	public function loadTime(){
    	$bedrijf = Bedrijf::find(1);
    	$dm_url = $bedrijf->dm_url;
		$afspraken = Appointment::all();
    	foreach($afspraken as $a){
	    	$url = $dm_url.'/api/loadTime';
	    	$post = array();
	    	$post = (object) $post;
	    	$date = date('Y-m-d', strtotime($a->startdatum));
	    	$post->date = $date;
	    	$post->week = date("W", strtotime($date));
	    	$post->year = date("Y", strtotime($date));
	    	$post->month = date("m", strtotime($date));
	    	$post->weekday = date("N", strtotime($date));
	    	$post = (array) $post;
	    	$ch = curl_init($url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

	        // response contains channel id
	        $response = curl_exec($ch);

	        // close the connection, release resources used
	        curl_close($ch);

	        Log::info($response);
	    }
	}

	    // $delete = file_get_contents($dm_url.'/api/'.Activiteit_has_Afspraak::count().'/clearFacts');
	public function loadPlace(){
    	$bedrijf = Bedrijf::find(1);
    	$dm_url = $bedrijf->dm_url;
		$klanten = Client::all();
	    $url = $dm_url.'/api/loadPlaces';
    	$plaatsen = array();
    	// $check = false;
    	foreach($klanten as $a){
    		$check = false;
    		if(count($plaatsen) > 0){
				for($i = 0; $i < count($plaatsen); $i++){
					if($plaatsen[$i]['city'] == $a->woonplaats){
						$check = true;
					}
				}
			}
			if(!$check){
				array_push($plaatsen, ['city' => $a->woonplaats, 'zipcode' => $a->postcode]);
			}
	    }

	    $plaatsen = json_encode($plaatsen);
	    $ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $plaatsen);
        curl_setopt($ch, CURLOPT_CRLF, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-type: application/json"
            ));

	    $response = curl_exec($ch);
	    echo $response;
	        // close the connection, release resources used
	    curl_close($ch);
	}

	    // dd($afspraken);
	public function loadFacts(){
    	$bedrijf = Bedrijf::find(1);
    	$dm_url = $bedrijf->dm_url;
		$afspraken = Appointment::all();
		$written_by_name = '';
		$written_by_lastname = '';
		$sold_by_name = '';
		$sold_by_lastname = '';
	    foreach($afspraken as $a){
	    	$wn = Employee::find($a->geschreven_door);
	    	if($wn != null){
		    	$written_by_name = ($wn->voornaam != null || $wn->voornaam != "") ? $wn->voornaam : "";
		    	$written_by_lastname = ($wn->achternaam != null || $wn->achternaam != "") ? $wn->achternaam : "";
	    	} else {
	    		$written_by_name = "";
	    		$written_by_lastname = "";
	    	}
	    	if(Worklist_has_appointments::where('afspraak_id', $a->id)->exists()){
	    		echo "afspraak staat in een werklijst";
	    		$wha = Worklist_has_appointments::with('werklijst')->where('afspraak_id', $a->id)->first();
	    		if($wha->werklijst->verkoper != null){
	    			echo "VERKOPER IS NIET NULL";
	    			$wn = Employee::find($wha->werklijst->verkoper);
		    		$sold_by_name = $wn->voornaam;
		    		$sold_by_lastname = $wn->achternaam;
			    	echo '<br>'.$sold_by_name.' '.$sold_by_lastname.'<br>';
	    		} else {
	    			echo "VERKOPER IS NULL";
		    		$sold_by_name = "";
		    		$sold_by_lastname = "";
	    		}
	    	} else {
	    		$sold_by_name = "";
	    		$sold_by_lastname = "";
	    	}
	    	$city = Client::find($a->klant_id)->woonplaats;
	    	$type = AppointmentType::find($a->afspraaktype_id)->omschrijving;
	    	$date = date('Y-m-d', strtotime($a->startdatum));
	    	if(Status_has_appointment::where('afspraak_id', $a->id)->exists()){
		    	if(Status_has_appointment::where('afspraak_id', $a->id)->first()->status_id == 3 || Status_has_appointment::where('afspraak_id', $a->id)->first()->status_id == 4){
			    	// if($a->afspraaktype_id == 1){ // in regular appointments the appointment has activities
		    		if(Status_has_appointment::where('afspraak_id', $a->id)->first()->status_id == 4)
		    			echo '<br.<br> STATUS ID IS 4 BIJ AFSPRAAK ID '.$a->id.' <br><br>';
			    	$ahas = Appointment_has_activities::where('afspraak_id', $a->id)->get();
			    	foreach($ahas as $aha){
			    		$amount = $aha->aantal;
			    		$price = $aha->prijs;
			    		$activity = Work::find($aha->activiteit_id)->omschrijving;

			    		$url = $dm_url.'/api/loadFacts';
			    		$post = ['written_by_lastname' => $written_by_lastname, 'written_by_name' => $written_by_name, 'sold_by_lastname' => $sold_by_lastname, 'sold_by_name' => $sold_by_name, 'city' => $city, 'type' => $type, 'date' => $date, 'amount' => $amount, 'price' => $price, 'activity' => $activity];
			    		print_r($post, true);
			    		$ch = curl_init($url);
					    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

					    // response contains channel id
					    $response = curl_exec($ch);

					        // close the connection, release resources used
					    curl_close($ch);

					    Log::info($response);
			    	}
				}
			}

	    }
			// echo "klaar";

    }

    public function clearFacts(){
    	$bedrijf = Bedrijf::find(1);
    	$dm_url = $bedrijf->dm_url;
    	$url = $dm_url . '/api/clearFacts';
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $output contains the output string
        $response = json_decode(curl_exec($ch));
        // close curl resource to free up system resources
        curl_close($ch);
    }

    public function getProCode(){
    	$bedrijf = Bedrijf::find(1);
    	return $bedrijf->pro66p;
    }

}
