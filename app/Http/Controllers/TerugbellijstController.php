<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\History;
use App\Models\Client;
use App\Models\Status;
use App\Models\Status_has_appointment;
use App\Models\User;
use App\Models\Employee;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TerugbellijstController extends Controller
{
    //
    /**
     * @description Create new controller instance
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @description Retrieve all customers whose appointment has failed and we can retry with
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function index(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('klant.terugbellijst');
    }

    /**
     * @description Retrieve all customers data as json whose appointment has failed and we can retry with
     * @return JsonResponse
     */
    public function getIndexData(): JsonResponse
    {
        try {

            $relaties = DB::select("
                                    SELECT
                                        k.id AS klant_id,
                                        k.voornaam,
                                        k.achternaam,
                                        k.straat,
                                        k.huisnummer,
                                        k.hn_prefix,
                                        k.postcode,
                                        k.woonplaats,
                                        k.email,
                                        k.telefoonnummer_prive,
                                        k.telefoonnummer_zakelijk,
                                        af.startdatum AS startdatum,
                                        af.id AS afspraak_id,
                                        af.reden
                                    FROM
                                        status_has_afspraak AS sha
                                    JOIN
                                        afspraak AS af ON af.id = sha.afspraak_id
                                    LEFT OUTER JOIN
                                        afspraak AS a ON a.id = af.afspraak_id
                                    JOIN
                                        klant AS k ON k.id = af.klant_id
                                    WHERE
                                        sha.status_id = 2
                                        AND af.is_verzet = 0
                                    ORDER BY
                                        af.startdatum ASC
                                ");

            foreach ($relaties as $r) {
                $afspraak = Appointment::find($r->afspraak_id);
                $reden = $afspraak->reden;
                $redenen = History::whereAfspraakId($r->afspraak_id)
                            ->whereType('terugbellijst')->orderBy('created_at', 'ASC')->get()->toArray();
                $r->redenen = array_merge([$reden], $redenen);
            }

            return response()->json([
                'data' => $relaties,
                'status' => 200
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'error' => $th->getMessage(),
                'status' => 500
            ]);
        }
    }

    /**
     * @description Updates the history made via the Terug Bel Lijst and potentially removes it from the list
     * @param Request $request
     * @return JsonResponse
     */
    public function setComment(Request $request): JsonResponse
    {
        //string manipulation for initials
        $nameArray = explode(' ',Auth::user()->name);
        $nameString = strtoupper(str_split($nameArray[0])[0]);
        if(count($nameArray) > 1) {
            $nameString.= strtoupper(str_split($nameArray[1])[0]);
            if(count($nameArray) > 2) {
                $nameString.= strtoupper(str_split($nameArray[2])[0]);
            }
        }
        //create message string
        $text = date('d-m', strtotime($request->input('datetime'))).' '
            .$request->input('history').' ('.$nameString.')';

        //create history
        $geschiedenis = History::setHistory(
            $request->input('klant_id'),
            $request->input('afspraak_id'),
            Auth::user()->id,
            $text,
            'terugbellijst'
        );
        $noNewAppointment = $request->input('noNewAppointment');
        //if customer doesn't want to have a new appointment
        if($noNewAppointment){
            Status_has_appointment::setSha(
                $request->input('afspraak_id'),
                Status::APT_FAILED_NO_NEW_APPOINTMENT,
                Auth::user()->name." heeft de klant verwerkt: Er is geen nieuwe afspraak nodig.",
                date('Y-m-d',
                    strtotime(
                        Appointment::find(
                            $request->input('afspraak_id')
                        )->startdatum
                    )
                )
            );

            Appointment::find($request->input('afspraak_id'))->markAsVerzet();
        }

        return response()->json([
            'status' => 200,
            'data' => $geschiedenis,
            'text' => $text
        ]);
    }

    /**
     * @description Creates an appointment from the Terug Bel Lijst
     * @param Request $request
     * @return JsonResponse
     */
    public function setAppointment(Request $request): JsonResponse
    {
        $afspraak = Appointment::find($request->input('afspraak_id'));
        $klant = Client::find($request->input('klant_id'));
        $user = User::find($request->input('user_id'));
        $newAfspraak = "";
        $oldDescription = "";
        if(empty(strip_tags($request->input('description')))){
            $oldDescription += $afspraak->description;
        }
        $newAfspraak = $afspraak->replicate([
            'startdatum',
            'einddatum',
            'omschrijving',
            'verwerkt_door',
            'afspraak_id',
            'geschreven_door'
        ]);
        $startDate = date('Y-m-d', strtotime($request->input('datetime')));
        if($request->input('beforeTwelveThirty')){
            $newAfspraak->startdatum = date('Y-m-d H:i:s', strtotime($startDate . " 08:00:00"));
            $newAfspraak->einddatum = date('Y-m-d H:i:s', strtotime($startDate . " 12:30:00"));
        } else {
            $newAfspraak->startdatum = date('Y-m-d H:i:s', strtotime($startDate . " 12:30:00"));
            $newAfspraak->einddatum = date('Y-m-d H:i:s', strtotime($startDate . " 17:00:00"));
        }
        $newAfspraak->omschrijving = (empty($oldDescription))? $request->input('description'): $oldDescription;
        $newAfspraak->verwerkt_door = $user->id;
        $newAfspraak->afspraak_id = $afspraak->id;
        $newAfspraak->geschreven_door = $user->id;
        $newAfspraak->save();

        $afspraak->markAsVerzet();

        return response()->json([
            'status' => 200,
            'data' => $request
        ]);
    }

    /**
     * @description To Mark the Terug Bel Lijst item as not answering the call
     * @param Request $request
     * @return JsonResponse
     */
    public function setGGH(Request $request): JsonResponse
    {
        // $afspraak = Afspraak::find($afspraak_id);
        $substr = strtoupper(Employee::find(Auth::user()->werknemer_id)->voornaam[0].Employee::find(Auth::user()->werknemer_id)->achternaam[0]);
        // $afspraak->reden = $afspraak->reden.";".date('d-m H:i')." - GGH (".$substr.")";
        // $afspraak->save();
        $gghString = date('d-m H:i')." - GGH (".$substr.")";
        History::setHistory(
            $request->input('klant_id'),
            $request->input('afspraak_id'),
            Auth::user()->id,
            $gghString,
            'terugbellijst'
        );

        return response()->json([
            'status' => 200,
            'data' => $gghString
        ]);
    }
}
