<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Work;
use App\Models\AppointmentType;
use App\Models\Appointment;
use DateTime;
use Auth;
use App\Models\Log;
use App\Models\Status;
use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;
use App\Models\Vijftien;
use App\Models\Brief;
use App\Models\Bedrijf;
use App\Models\Status_has_appointment;

class VijftienController extends Controller
{
	public function __construct(){
        $this->middleware('auth');
	}

	public function create($afspraak_id){
		if(Appointment::where('id', $afspraak_id)->exists()){
			$vft = new Vijftien();
			$vft->aangemaakt_door = Auth::user()->id;
			$vft->afspraak_id = $afspraak_id;
			$vft->status_id = 35;
			$vft->save();
		} else {
			return redirect('/vijftieneuro')->with('danger', 'Ongeldige afspraak, neem contact op met uw systeembeheerder.');
		}

		return redirect('/vijftieneuro')->with('succes', 'Klant gemarkeerd als €15,- klant');
	}

	public function index(){
		$potentiele = Status_has_appointment::with('afspraak', 'status')->where('status_id', 2)->orWhere('status_id', 23)->orWhere('status_id', 30)->get();
        foreach($potentiele as $vft){
            if(Appointment::where('afspraak_id', $vft->afspraak_id)->exists()){
                unset($vft);
            } else {
            }
        }
        // dd($potentiele);
        $vijftien = Vijftien::getVijftien(null);
        foreach($vijftien as $vft){
        	if($vft->afspraak == null){
        		$vft->delete();
        	}
        }
        // dd($vijftien);

        $page = '';
        $sub = '';

        return view('financieel.vijftieneuro', compact('vijftien', 'potentiele','page', 'sub'));
	}

	public function multiSend(){
		$input = Request::all();
		// dd($input);
		foreach($input['vft'] as $v){
			$vft = Vijftien::find($v);
			// dd($v);
			$vft->upOneStatus();
		}
		return redirect('/vijftieneuro')->with('success', 'Brieven zijn gemarkeerd als verstuurd');
	}

	public function singleSend($id){
		$vft = Vijftien::find($id);
		$vft->upOneStatus();

		return redirect('/vijftieneuro')->with('success', 'Brief succesvol gemarkeerd als verstuurd');
	}

	public function show($id){
		$vft = Vijftien::find($id);
		$vft->brief = Brief::where('status_id', $vft->status_id)->first();
		$vft = $this->returnFilledLetter($vft);

		$sub = 'vijftien';
		$page ='financieel';

		return view('brief.brief', compact('vft', 'page', 'sub'));
		//get correct letter corresponding to status!
		//return view(right letter);
	}

	public function showAllToSend(){
		$vijftien = Vijftien::all();
		foreach ($vijftien as $v) {
			$v = $this->returnFilledLetter($v);
		}

		return view('mail.alleBrieven', compact('vijftien'));
	}

	//internal function (only works with the first letter)
	public function returnFilledLetter($v){
		$bedrijf = Bedrijf::find(1);
		$afspraak = Appointment::find($v->afspraak_id);
		$relatie = Client::find($afspraak->klant_id);
		$v->brief = Brief::where('status_id', $v->status_id)->first(); // get correct 'brief' corresponding to status.. Later split vars according to letter for dry principle and error prevention
		$v->brief = str_replace('#achternaam#', $relatie->achternaam, $v->brief->brief);
		$v->brief = str_replace('#straat#',  $relatie->straat, $v->brief);
		$v->brief = str_replace('#postcode#',  $relatie->postcode, $v->brief);
		$v->brief = str_replace('#adres#',  $relatie->adres, $v->brief);
		$v->brief = str_replace('#bedrijfsplaats#',  $bedrijf->plaats, $v->brief);
		$v->brief = str_replace('#date#',  date('d-m-Y'), $v->brief);
		$v->brief = str_replace('#afspraakdatum#',  date('d-m-Y', strtotime($afspraak->startdatum)), $v->brief);
		$v->brief = str_replace('#totaal#',  number_format($v->totaalbedrag,2), $v->brief);
		$v->brief = str_replace('#bankrekening#',  $bedrijf->bankrekening, $v->brief);
		$v->brief = str_replace('#bedrijfsnaam#',  $bedrijf->naam, $v->brief);
		$v->brief = str_replace('#postcode+huisnummer#',  strtoupper($relatie->postcode.$relatie->huisnummer), $v->brief);
		$v->brief = str_replace('#administratiekosten#',  number_format($v->administratiekosten,2), $v->brief);
		$v->brief = str_replace('#bedrijfsadres#',  $bedrijf->adres, $v->brief);
		$v->brief = str_replace('#bedrijfspostcode#',  $bedrijf->postcode, $v->brief);
		$v->brief = str_replace('#tel1#',  $bedrijf->tel1, $v->brief);
		$v->brief = str_replace('#tel2#',  $bedrijf->tel2, $v->brief);
		$v->brief = str_replace('#tel3#',  $bedrijf->tel3, $v->brief);
		$v->brief = str_replace('#bedrijfsmail#',  $bedrijf->email, $v->brief);
		$v->brief = str_replace('#urlLogo#',  '#Logo bedrijf invoegen', $v->brief);

		return $v;
	}

	public function editAllLetters(){
		$brieven = Brief::all();
		// dd($brieven[0]);
		$page = '';
		$sub = '';
		return view('brief.editAll', compact('page', 'sub', 'brieven'));
	}

	public function saveAllLetters(){
		$input = Request::all();
		// dd($input);
		foreach ($input as $i => $value){
			// echo $i.' - '.$value.'<br>';
			$key = str_replace('_', ' ', $i);
			// echo '<br>KEY: '.$key.'<br>';
			$brief = Brief::where('naam', $key)->first();
			// var_dump($brief);
			if($brief){
				// echo '<br> ik zit in de if';
				$brief->brief = $value;
				$brief->updated_by = Auth::user()->id;
				Log::createLog(Auth::user()->id, null, "Standaard brieven", Auth::user()->name.' heeft de standaard brieven gewijzigd.');
				$brief->save();
			}
		}

		// dd('klaar');

		return redirect('/vijftieneuro')->with('succes', 'U heeft de standaard brieven succesvol gewijzigd.');
	}

	public function editLetter($id){
		$v = Vijftien::find($id);
		if($v){
			$input = Request::all();
			$v->administratiekosten = $input['administratiekosten'];
			$v->totaalbedrag = $input['factuurbedrag'];
			$v->save();
		}
		return redirect('/vijftieneuro')->with('succes', 'U heeft de brief succesvol gewijzigd.');
	}

}
