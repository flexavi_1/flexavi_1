<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\History;
use App\Models\Status;
use App\Models\Log;
use App\Models\Status_has_appointment;
use App\Models\Status_has_factuur;
use App\Models\Appointment_has_activities;
use App\Models\Activiteit_has_offerte;
use App\Models\Work;
use Auth;
use App\Models\Factuur;
use App\Models\Klant_has_status;
use DB;
use App\Models\Crediteur;
use App\Models\Offerte;
use App\Models\Offerte_has_status;
use Datetime;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;
use App\Models\Uur;

class UurController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	if(Auth::user()->rol < 7)
    	{
    		$urenpp = Uur::with('klant', 'werknemer')->where('approved', 0)->orderBy('datum', 'DESC')->get();
    	}
    	else
    	{
    		$urenpp = Uur::with('klant', 'werknemer')->where('werknemer_id', Auth::user()->werknemer_id)->get();
    	}

        $page = "urenregistratie";
        $sub = "urenover";

    	return view('uren.index', compact('urenpp', 'page', 'sub'));
    }

    public function create()
    {

        $page = "urenregistratie";
        $sub = "urentoev";
        return view('uren.create', compact('page', 'sub'));
        // Verschil maken per functie? Uren registreren alleen voor kantoor personeel en buitendienst laten genereren?
    }

    public function store()
    {
        $input = Request::all();

        $uur = new Uur();
        $uur->werknemer_id = $input['werknemer_id'];
        $uur->datum = $input['datum'];

        $relatie = Client::where('postcode', $input['postcode'])
                       ->where('huisnummer', $input['huisnummer'])
                       ->first();

        if ($relatie) {
            $uur->klant_id = $relatie->id;

            $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['starttijd']);
            $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['eindtijd']);
            $startstamp = $strtotime->getTimestamp();
            $endstamp = $strtotimee->getTimestamp();
            $uur->starttijd = date('Y-m-d H:i:s', $startstamp);
            $uur->eindtijd = date('Y-m-d H:i:s', $endstamp);
            $uur->approved = 0;
            $uur->aantal_minuten = $uur->minutes($input['starttijd'], $input['eindtijd']);

            $uur->save();

            return redirect('/uren/nieuw')->with('succes', 'U heeft uw uren succesvol toegevoegd.');
        } else {
            // Handle the case where $relatie is null, for example by redirecting with an error message.
            return redirect('/uren/nieuw')->with('error', 'Klant niet gevonden.');
        }
    }


    public function delete($id)
    {
        $uur = Uur::find($id);
        if($uur->approved == 1)
        {
            return redirect('/uren')->with('danger', 'U kunt deze uren niet verwijderen, daar ze al goedgekeurd zijn.');
        }
        $uur->delete();

        return redirect('/uren')->with('succes', 'U heeft uw uren succesvol verwijderd.');
    }

}
