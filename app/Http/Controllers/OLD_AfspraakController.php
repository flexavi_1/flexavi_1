<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Work;
use App\Models\AppointmentType;
use App\Models\Appointment;
use App\Models\History;
use App\Models\Appointment_has_activities;
use DateTime;
use Auth;
use App\Models\Offerte;
use App\Models\Activiteit_has_offerte;
use App\Models\Log;
use App\Models\Status;
use App\Models\Status_has_appointment;
use App\Models\Crediteur;
use App\Models\Klant_has_status;
use App\Models\Afspraak_has_geschiedenis;
use App\Models\Activiteit_has_materiaal;
use App\Models\Voorraad;
use App\Models\Worklist_has_appointments;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class OLDAfspraakController extends Controller
{



        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
    *   Create view for appointment
    */
    public function create($id)
    {
        $relatie = Client::findOrFail($id);
        $types = AppointmentType::where('omschrijving', '!=', 'Uitgestelde Betaling')->where('omschrijving', '!=', 'Betalingsherinnering')->get();
        $werknemers = Employee::all();
        $activiteiten = Work::where('actief', 1)->get();

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.maken', compact('relatie', 'types', 'werknemers', 'activiteiten', 'page', 'sub'));
    }

    /*
    *   Store appointment, set appointmentcontent and redirect to right view
    */
    public function store()
    {
        $input = Request::all();

        if($input['startdatum'] >= $input['einddatum'] && $input['radio_demo'] == 2)
        {
            return redirect('/afspraak/maken/'.$input['klant_id'])->with('danger', 'Startdatum moet voor de einddatum liggen.');
        }
        else
        {
            $afspraak = new Appointment;
            if($input['radio_demo'] < 2 )
            {
                $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
                $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
                $startstamp = $strtotime->getTimestamp();
                $endstamp = $strtotimee->getTimestamp();
                if($input['radio_demo'] == 1)
                {
                    $afspraak->startdatum = date('Y-m-d', $startstamp)." 12:30";
                    $afspraak->einddatum = date('Y-m-d', $startstamp)." 17:00";
                }
                else
                {
                    $afspraak->startdatum = date('Y-m-d', $startstamp)." 08:00";
                    $afspraak->einddatum = date('Y-m-d', $startstamp)." 12:30";
                }
            }
            else
            {
                $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
                $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
                $startstamp = $strtotime->getTimestamp();
                $endstamp = $strtotimee->getTimestamp();
                $afspraak->startdatum = date('Y-m-d H:i:s', $startstamp);
                $afspraak->einddatum = date('Y-m-d H:i:s', $endstamp);
            }
            $afspraak->klant_id = $input['klant_id'];
            $afspraak->geschreven_door = $input['geschreven_door'];
            $afspraak->verwerkt_door = $input['verwerkt_door'];
            $afspraak->afspraaktype_id = $input['afspraaktype_id'];
            $afspraak->omschrijving = $input['omschrijving'];
            // (!isset($input['bevestiging'])) ? $afspraak->bevestiging = 1: $afspraak->bevestiging = 0;
            (!isset($input['initiatief'])) ? $afspraak->initiatief = 0 : $afspraak->initiatief = 1;
            $afspraak->save();

            $relatie = Client::findOrFail($afspraak->klant_id);
            if($relatie->klanttype_id < 3 /*&& afspraaktype =/= offerte*/)
            {
                $relatie->klanttype_id = 3;
                $relatie->save();
            }

            $sha = new Status_has_appointment;
            $sha->status_id = 1;


            $relatie = Client::findOrFail($afspraak->klant_id);
            if(Klant_has_status::where('klant_id', $relatie->id)->exists()) //set Reminder
            {
                $khs = Klant_has_status::where('klant_id', $relatie->id)->first();
                if($khs->status_id == 22)
                {
                    if($relatie->calcDiff($khs->updated_at, date('d-m-Y')) >= 350)
                    {
                        $khs->status_id = 21;
                        $khs->verwerkt_door = Auth::user()->id;
                        $khs->save();
                    }
                }
                // else
                // {
                //     if($relatie->calcDiff($khs->updated_at, date('d-m-Y')) > 300)
                //     {
                //         $khs->status_id = 22;
                //         $khs->save();
                //         $khs = Klant_has_status::where('klant_id', $relatie->id);
                //         $khs->verwerkt_door = Auth::user()->id;
                //         $khs->save();
                //     }
                // }
            }
            else
            {

                $khs = new Klant_has_status;
                $khs->status_id = 21;
                $khs->klant_id = $relatie->id;
                $khs->verwerkt_door = Auth::user()->id;
                $khs->save();
            }


            $sha->afspraak_id = $afspraak->id;
            $sha->gebruiker_id = Auth::user()->id;
            $sha->omschrijving = Auth::user()->name." heeft deze afspraak Toegevoegd.";
            $sha->save();

            $log = new Log;
            $log->omschrijving = Auth::user()->name." heeft een afspraak op: ".$afspraak->startdatum." -- ".$afspraak->einddatum.", toegevoegd.";
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $input['klant_id'];
            $log->type = "Afspraak";
            $log->save();

            //geschiedenis
            $regel = new History;
            $regel->datum = date('Y-m-d');
            $regel->klant_id = $afspraak->klant->id;
            $regel->afspraak_id = $afspraak->id;
            $user = User::findOrFail(Auth::user()->id);
            $regel->gebruiker_id = $user->id;
            $regel->omschrijving = $user->name." heeft een afspraak gemaakt met klant op ".$afspraak->returnDate($afspraak->startdatum)." --- ".$afspraak->returnDate($afspraak->einddatum);
            $regel->save();

            if(AppointmentType::find($input['afspraaktype_id'])->omschrijving == "Garantie")
            {
                //$afspraken = Afspraak::where('klant_id', $input['klant_id'])->get();
                // dd($afspraken);
                $relatie = Client::find($input['klant_id']);
                $actperafs = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function($afs) use ($relatie)
                {
                    $afs->where('klant_id', $relatie->id);
                })->get();
                $alleafspraken = Status_has_appointment::with('afspraak', 'status')->whereHas('afspraak', function($afd) use ($relatie)
                {
                    $afd->where('klant_id', $relatie->id)->groupBy('status_id');
                })->get();
                //dd($alleafspraken);
                $types = AppointmentType::all();
                foreach($alleafspraken as $t)
                {
                    foreach($types as $type)
                    {
                        if($t->afspraak->afspraaktype_id == $type->id)
                            $t->afspraak->afspraaktype = $type->omschrijving;
                    }
                }
                $alleafspraken = $alleafspraken->sortByDesc(function($afspraak){
                    return $afspraak->afspraak->startdatum;
                });
                //dd($actperafs->IsEmpty());

                $page = "relatie";
                $sub = "relover";
                return view('afspraak.kiesAfspraakGarantie', compact('afspraken', 'relatie', 'actperafs', 'alleafspraken', 'afspraak', 'page', 'sub'));
            }
            elseif(AppointmentType::find($input['afspraaktype_id'])->omschrijving == "Offerte opstellen")
            {
                if(isset($input['voorrijkosten']))
                {
                    $act = Work::where('omschrijving', 'Voorrijkosten')->first();
                    $aha = new Appointment_has_activities;
                    $aha->activiteit_id = $act->id;
                    $aha->afspraak_id = $afspraak->id;
                    $aha->aantal = 1;
                    $aha->prijs = $act->prijs;
                    $afspraak->totaalbedrag = $aha->prijs;
                    $afspraak->save();
                    $aha->save();
                }

                return redirect('/relatie/'.$input['klant_id'])->with('succes', 'Afspraak succesvol toegevoegd.');
            }
            elseif(AppointmentType::find($input['afspraaktype_id'])->omschrijving == "Betalingsherinnering")
            {
                dd("nog bouwen");

            }
            elseif(AppointmentType::find($input['afspraaktype_id'])->omschrijving == "Uitgestelde betaling")
            {
                $relatie = Client::find($input['klant_id']);
                $actperafs = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function($afs) use ($relatie)
                {
                    $afs->where('klant_id', $relatie->id)->where('niet_betaald', '!=', 0);
                })->get();

                $alleafspraken = Status_has_appointment::with('afspraak', 'status')->whereHas('afspraak', function($afd) use ($relatie)
                {
                    $afd->where('klant_id', $relatie->id)->where('niet_betaald', '!=', 0)->groupBy('status_id');
                })->get();
                //dd($alleafspraken);
                $types = AppointmentType::all();
                foreach($alleafspraken as $t)
                {
                    foreach($types as $type)
                    {
                        if($t->afspraak->afspraaktype_id == $type->id)
                            $t->afspraak->afspraaktype = $type->omschrijving;
                    }
                }
                $alleafspraken = $alleafspraken->sortByDesc(function($afspraak){
                    return $afspraak->afspraak->startdatum;
                });
                $ub = " ";

                $page = "relatie";
                $sub = "relover";
                return view('afspraak.kiesAfspraakGarantie', compact('afspraken', 'relatie', 'actperafs', 'alleafspraken', 'afspraak', 'ub', 'page', 'sub'));
            }
            elseif(AppointmentType::find($input['afspraaktype_id'])->omschrijving == "Terugbetaalverzoek")
            {
                // $afspraak = Afspraak::where('startdatum', date('Y-m-d H:i:s', $startstamp))
                                // ->where('klant_id', $input['klant_id'])
                                // ->where('einddatum', date('Y-m-d H:i:s', $endstamp))->first();
                // dd($afspraak);
                $relatie = Client::findOrFail($afspraak->klant_id);

                $log = new Log;
                $log->omschrijving = Auth::user()->name." heeft een terugbetaalverzoek toegevoegd.";
                $log->gebruiker_id = Auth::user()->id;
                $log->klant_id = $relatie->id;
                $log->type = "Afspraak";
                $log->save();

                //geschiedenis
                $regel = new History;
                $regel->datum = date('Y-m-d');
                $regel->klant_id = $afspraak->klant->id;
                $regel->afspraak_id = $afspraak->id;
                $user = User::findOrFail(Auth::user()->id);
                $regel->gebruiker_id = $user->id;
                $regel->omschrijving = $user->name." heeft deze klant toegevoegd aan de terugbetaalverzoeken.";
                $regel->save();

                $page = "fincancieel";
                $sub = "crediteur";

                return view('crediteur.garantiecrediteur', compact('relatie', 'afspraak', 'page', 'sub'));
            }
            else
            {
                return redirect('/afspraak/maken/'.$input['klant_id'].'/'.$afspraak->id.'/werkzaamheden');
            }
        }
    }

    /*
    *   Type=Uitvoering Werkzaamheden, View too add workactivities
    */
    public function voegwerkzaamhedentoe($id, $afspraak_id)
    {
        $relatie = Client::findOrFail($id);
        $afspraak = Appointment::findOrFail($afspraak_id);
        $activiteiten = Work::where('actief', 1)->get();
        $danger = "";

        $page = "relatie";
        $sub = "relover";
        return view('afspraak.werkzaamheden', compact('relatie', 'afspraak', 'activiteiten','danger', 'page', 'sub'));

    }

    /*
    *   Type=Uitvoering Werkzaamheden, store workactivities, redirect to relation profile
    */
    public function afspraakOpslaan($id, $afspraak_id)
    {

        $input = Request::all();
        // dd($input);
        $double = false;
        for($k=0;$k<count($input['activiteiten']);$k++)
        {
            for($l=0;$l<count($input['activiteiten']);$l++)
            {
                if($input['activiteiten'][$l]==$input['activiteiten'][$k] && $k != $l)
                    $double=true;
            }
        }
        if(!$double)
        {
            $i = 1; // algoritme om aantal te vinden
            $rij = array();
            foreach($input['aantal'] as $r)
            {
                // if($i != count($input['aantal']))
                // {
                //     if(isset($r["s_".$i.":i_0"]))
                //         array_push($rij, $r["s_".$i.":i_0"]);
                // }
                // else
                // {
                //     $i--;
                //     array_push($rij, $input['aantal'][$i]);
                // }
                // $i++;


            }

            //rij geeft nu in de juiste volgorde de hoeveelheden aan.

            //totaalbedrag uitrekenen
            $relatie = Client::findOrFail($id);
            $afspraak = Appointment::findOrFail($input['afspraak_id']);
            $totaalbedrag = 0;

            $geschiedenisString = date('d-m-Y', strtotime($afspraak->startdatum)).": Afspraak met de volgende werkzaamheden";

            // dd($geschiedenisString);
            if(isset($input['klusprijsAan']))
            {
                $afspraak->klusprijs = 1;
                $klusprijs = $input['klusprijs'];
                $totaalbedrag = $klusprijs;
            }

            for($i = 0; $i < count($input['activiteiten']);$i++)
            {
                $act = Work::findOrFail($input['activiteiten'][$i]);

                $aha = new Appointment_has_activities;
                $aha->activiteit_id = $act->id;
                $aha->afspraak_id = $afspraak->id;
                $aha->aantal = $input['aantal'][$i];
                if(isset($klusprijs))
                {
                    if($klusprijs > 0)
                    {
                        $aha->prijs = ($input['prijs'][$i] > 0 ? $input['prijs'][$i] : $act->prijs);
                        $bedrag = $aha->prijs * $aha->aantal;
                        if($klusprijs - $bedrag <= 0)
                        {
                            $aha->kortingsbedrag = $bedrag - $klusprijs;
                            $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;
                            $klusprijs = $klusprijs - $bedrag;
                        }
                        elseif($klusprijs - $bedrag > 0)
                        {
                            $aha->kortingsbedrag = $input['korting'][$i];
                            $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;
                            $klusprijs = $klusprijs - $bedrag;
                        }
                    }
                }
                else
                {
                    $aha->prijs = ($input['prijs'][$i] > 0 ? $input['prijs'][$i] : $act->prijs);
                    $aha->kortingsbedrag = $input['korting'][$i];
                    $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;
                    $bedrag = $aha->prijs * $input['aantal'][$i] - $aha->kortingsbedrag;
                    $totaalbedrag = $totaalbedrag + $bedrag;
                }

                $geschiedenisString = $geschiedenisString.", ".$aha->aantal." ".$act->omschrijving." € ".$bedrag;
                // dd($aha);
                $aha->opmerking = $input['opmerking'][$i];
                $aha->save();
            }
            $geschiedenisString = $geschiedenisString.",Totaal: € ".$totaalbedrag;

            $geschiedenis = new History;
            $geschiedenis->afspraak_id = $afspraak->id;
            $geschiedenis->gebruiker_id = Auth::user()->id;
            $geschiedenis->klant_id = $afspraak->klant_id;
            $geschiedenis->datum = date('Y-m-d');
            $geschiedenis->omschrijving = $geschiedenisString;
            $geschiedenis->type = "werkzaamheden";
            $geschiedenis->save();

            $afspraak->totaalbedrag = $totaalbedrag;
            // dd($afspraak);
            //logbeheer


            //status

            $afspraak->save();
            $afspraak = Appointment::findOrFail($input['afspraak_id']);

            if(History::where('klant_id', $relatie->id)->where('type', 'werkzaamheden')->where('afspraak_id', '!=', $afspraak->id)->count() > 0)
            {
                $geschiedenis = History::where('klant_id', $relatie->id)->where('afspraak_id', '!=', $afspraak->id)->get();

                $page = "relatie";
                $sub = "relover";
                return view('afspraak.addHistory', compact('relatie', 'afspraak', 'geschiedenis', 'page', 'sub'));
            }

            return redirect('/relatie/'.$id)->with('succes', 'De afspraak is toegevoegd aan de relatie.');
        }
        else
        {

            $relatie = Client::findOrFail($id);
            $afspraak = Appointment::findOrFail($afspraak_id);
            $activiteiten = Work::where('actief', 1)->get();
            $danger = 'U kunt niet meerdere malen dezelfde werkzaamheden opgeven.';

            $page = "relatie";
            $sub = "relover";
            return view('afspraak.werkzaamheden', compact('relatie', 'afspraak', 'activiteiten', 'danger', 'page', 'sub'));
        }

    }

    /*
    *   Delete appointment and all content from linking tables, als write too log and history
    */
    public function delete($id)
    {
        $afspraak = Appointment::findOrFail($id);
        $relatie = $afspraak->klant_id;

        //geschiedenis
        $regel = new History;
        $regel->datum = date('Y-m-d');
        $regel->klant_id = $relatie;
        $regel->afspraak_id = $afspraak->id;
        $user = User::findOrFail(Auth::user()->id);
        $regel->gebruiker_id = $user->id;
        $regel->omschrijving = $user->name." heeft de afspraak van ".$afspraak->returnDate($afspraak->startdatum)." --- ".$afspraak->returnDate($afspraak->einddatum)." verwijderd.";
        $regel->save();

        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft een afspraak op: ".$afspraak->startdatum." -- ".$afspraak->einddatum.", verwijderd.";
        $log->gebruiker_id = Auth::user()->id;
        $log->klant_id = $relatie;
            $log->type = "Afspraak";
        $log->save();

        $werkzaamheden = Appointment_has_activities::all();
        foreach($werkzaamheden as $act)
        {
            if($act->afspraak_id == $afspraak->id)
            {
                $act->delete();
            }
        }
        $andereafspraken = Appointment::where('afspraak_id', $afspraak->id)->get();
        foreach($andereafspraken as $af)
        {
            $regel = new History;
            $regel->datum = date('Y-m-d');
            $regel->klant_id = $relatie;
            $regel->afspraak_id = $af->id;
            $user = User::findOrFail(Auth::user()->id);
            $regel->gebruiker_id = $user->id;
            $regel->omschrijving = $user->name." heeft de afspraak van ".$af->returnDate($af->startdatum)." --- ".$af->returnDate($af->einddatum)." verwijderd.";
            $regel->save();

            $log = new Log;
            $log->omschrijving = Auth::user()->name." heeft een afspraak op: ".$af->startdatum." -- ".$af->einddatum.", verwijderd.";
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $relatie;
                $log->type = "Afspraak";
            $log->save();

            $werkzaamheden = Appointment_has_activities::all();
            foreach($werkzaamheden as $act)
            {
                if($act->afspraak_id == $af->id)
                {
                    $act->delete();
                }
            }
            $af->delete();
        }

        if(Worklist_has_appointments::where('afspraak_id', $afspraak->id)->exists()){
            $wha = Worklist_has_appointments::where('afspraak_id', $afspraak->id)->first();
            $wha->delete();
        }

        $afspraak->delete();

        return redirect('/relatie/'.$relatie)->with('succes', 'U heeft de afspraak succesvol verwijderd.');
    }

    /*
    *   View too process an appointment
    */
    public function process($id)
    {
        $afspraak = Appointment::findOrFail($id);
        $relatie = Client::findOrFail($afspraak->klant_id);

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.verwerken1', compact('afspraak', 'relatie', 'page', 'sub'));
    }

    public function returntoprocess($id)
    {
        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);

        $input = Request::all();

        for($i = 0;$i < count($input['prijs']);$i++)
        {
            $temp = new Work;
            $temp->omschrijving = $input['omschrijving'][$i];
            $temp->prijs = $input['prijs'][$i];
            $temp->eenheid = $input['eenheid'][$i];
            $temp->actief = 1;
            $temp->save();

            $log = new Log;
            $log->omschrijving = Auth::user()->name." heeft de werkactiviteit: ".$input['omschrijving'][$i].", toegevoegd.";
            $log->gebruiker_id = Auth::user()->id;
            $log->type = "Activiteit";
            $log->save();
        }

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.verwerken1', compact('afspraak', 'relatie', 'page', 'sub'));
    }

    /*
    *   Store proces and redirect to follow-up page depending on appointmenttype
    */
    public function setProcess($id)
    {

        $input = Request::all();
        // $afspraak = Afspraak::find($id);
        // $toevoeging = "Na verwerken: ".$input['toevoeging'];
        //dd($input);
        if($input['gelukt'] == 0 && Appointment::find($id)->afspraaktype_id != 5)
        {
            $afs = Appointment::find($id);
            return redirect('/afspraak/'.$afs->id.'/verwerken/nietgelukt');
        }
        else
        {
            if(!isset($input['extra']) && !isset($input['voldaan']))
            {
                $extra = 0;
                $voldaan = 0;
            }
            else
            {
                $extra = $input["extra"];
                $voldaan = $input['voldaan'];
            }
            $afspraak = Appointment::findOrFail($id);
            if($voldaan == 1 || ($afspraak->afspraaktype_id == 5 && $input['gelukt'] == 1))
                $afspraak->bank = $input['bank'];
            if(isset($input['advies']))
                $afspraak->advies = $input['advies'];
            $afspraak->save();
            $afspraak = Appointment::findOrFail($id);

            if($voldaan == 1){
                $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
                $sha->status_id == 4;
                $sha->omschrijving = Status::find(4)->omschrijving;
            }else{
                $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
                $sha->status_id == 3;
                $sha->omschrijving = Status::find(3)->omschrijving;
            }

            if(AppointmentType::find($afspraak->afspraaktype_id)->omschrijving == "Uitvoeren werkzaamheden")
            {

                // Mutating stock
                $aha = Appointment_has_activities::where('afspraak_id', $afspraak->id)->get();
                foreach($aha as $a)
                {
                    if(Activiteit_has_materiaal::where('activiteit_id', $a->activiteit_id)->exists())
                    {
                        $ahm = Activiteit_has_materiaal::where('activiteit_id', $a->activiteit_id)->get();
                        foreach($ahm as $b)
                        {
                            $stockMutation = new Voorraad;
                            $stockMutation->materiaal_id = $b->materiaal_id;
                            $stockMutation->datum = date('Y-m-d', strtotime($afspraak->startdatum));
                            $stockMutation->mutatie = $b->aantal * $a->aantal * -1;
                            $stockMutation->klant_id = $afspraak->klant_id;
                            $stockMutation->save();
                        }
                    }
                }


                if($extra == 1)
                {
                    $danger = "";
                    $relatie = Client::findOrFail($afspraak->klant_id);
                    $activiteiten = Work::where('actief', 1)->get();
                    $garantie = AppointmentType::where('omschrijving', 'Garantie')->first()->id;

                    $ahas = Appointment_has_activities::with('activiteit')->where('afspraak_id', $afspraak->id)->get();

                    $page = "relatie";
                    $sub = "relover";
                    return view('afspraak.extrawerk', compact('danger','afspraak', 'relatie', 'extra', 'voldaan', 'activiteiten', 'garantie', 'ahas', 'page', 'sub'));
                }
                elseif($extra == 0 && $voldaan == 0)
                {
                    $relatie = Client::findOrFail($afspraak->klant_id);
                    $regel = new History;
                    $regel->datum = date('Y-m-d');
                    $regel->klant_id = $id;
                    $regel->afspraak_id = $afspraak->id;
                    $user = User::findOrFail(Auth::user()->id);
                    $regel->gebruiker_id = $user->id;
                    $regel->omschrijving = $user->name." heeft de afspraak verwerkt, afspraak is niet voldaan";
                    $regel->save();
                    $log = new Log;
                    $log->gebruiker_id = $user->id;
                    $log->klant_id = $relatie->id;
                    $log->omschrijving = $user->name." heeft deze klant, ".$relatie->postcode." - ".$relatie->huisnummer." afspraak verwerkt.";
                    $log->type = "Afspraak";
                    $log->save();
                    $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
                    $sha->status_id = 3;
                    $sha->omschrijving = "Afspraak is niet voldaan, maar wel verwerkt door ".Auth::user()->name;
                    $sha->save();
                    $relatie = Client::findOrFail($afspraak->klant_id);
                    $types = AppointmentType::where('id', 3)->orWhere('id', 5)->get();
                    $ub = " ";

                    $page = "relatie";
                    $sub = "relover";


                    $actperafs = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function($afs) use ($relatie, $afspraak)
                    {
                        $afs->where('klant_id', $relatie->id)->where('id', $afspraak->id);
                    })->get();

                    $nieuweafspraken = Appointment::where('afspraak_id', $afspraak->id)->get();
                    $subtotal = 0.00;
                    $afspraak->niet_betaald = $afspraak->totaalbedrag;
                    $afspraak->save();
                    $afspraak = Appointment::findOrFail($id);

                    if(!$nieuweafspraken->isEmpty())
                    {
                        $subtotal = 0.00;
                        foreach($nieuweafspraken as $nas)
                        {
                            $subtotal = $subtotal + $nas->totaalbedrag;
                        }
                    }

                    return view('afspraak.setAfspraak', compact('relatie', 'afspraak', 'types', 'ub', 'page', 'sub', 'actperafs', 'subtotal'));
                }
                elseif($extra == 0 && $voldaan==1)
                {
                    $relatie = Client::findOrFail($afspraak->klant_id);

                    $afspraak = Appointment::findOrFail($id);
                    $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
                    $sha->status_id = 4;
                    $sha->afspraak_id = $afspraak->id;
                    $sha->gebruiker_id = Auth::user()->id;
                    $sha->omschrijving = "Afspraak is voldaan en verwerkt";
                    $sha->save();

                    $regel = new History;
                    $regel->datum = date('Y-m-d');
                    $regel->klant_id = $relatie->id;
                    $regel->afspraak_id = $afspraak->id;
                    $user = User::findOrFail(Auth::user()->id);
                    $regel->gebruiker_id = $user->id;
                    $regel->omschrijving = $user->name." heeft de afspraak verwerkt, afspraak is voldaan";
                    $regel->save();

                    $log = new Log;
                    $log->gebruiker_id = $user->id;
                    $log->klant_id = $relatie->id;
                    $log->omschrijving = $user->name." heeft deze klant, ".$relatie->postcode." - ".$relatie->huisnummer." afspraak verwerkt.";

                    $log->type = "Afspraak";
                    $log->save();

                    $afspraak->betaald=1;
                    $afspraak->save(); //update met status
                    return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                }
            }
            elseif(AppointmentType::find($afspraak->afspraaktype_id)->omschrijving == "Garantie")
            {
                if($extra == 1)
                {
                    $danger = "";
                    $relatie = Client::findOrFail($afspraak->klant_id);
                    $activiteiten = Work::where('actief', 1)->get();
                    $garantie = AppointmentType::where('omschrijving', 'Garantie')->first()->id;

                    $page = "relatie";
                    $sub = "relover";
                    return view('afspraak.extrawerk', compact('danger','afspraak', 'relatie', 'extra', 'voldaan', 'activiteiten', 'garantie', 'page', 'sub'));
                }
                elseif($extra == 0 && $voldaan == 0)
                {
                    $relatie = Client::findOrFail($afspraak->klant_id);
                    $regel = new History;
                    $regel->datum = date('Y-m-d');
                    $regel->klant_id = $id;
                    $regel->afspraak_id = $afspraak->id;
                    $user = User::findOrFail(Auth::user()->id);
                    $regel->gebruiker_id = $user->id;
                    $regel->omschrijving = $user->name." heeft de garantie verwerkt, maar is niet opgelost";
                    $regel->save();
                    $log = new Log;
                    $log->gebruiker_id = $user->id;
                    $log->klant_id = $relatie->id;
                    $log->omschrijving = $user->name." heeft deze garantie van klant, ".$relatie->postcode." - ".$relatie->huisnummer." verwerkt.";
                    $log->type = "Afspraak";
                    $log->save();


                    $page = "relatie";
                    $sub = "relover";
                    return view('afspraak.garantie2', compact('afspraak', 'relatie', 'page', 'sub'));
                }
                elseif($extra == 0 && $voldaan==1)
                {
                    $relatie = Client::findOrFail($afspraak->klant_id);

                    $afspraak = Appointment::findOrFail($id);
                    $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
                    $sha->status_id = 4;
                    $sha->afspraak_id = $afspraak->id;
                    $sha->gebruiker_id = Auth::user()->id;
                    $sha->omschrijving = "Afspraak is voldaan en verwerkt";
                    $sha->save();

                    $regel = new History;
                    $regel->datum = date('Y-m-d');
                    $regel->klant_id = $relatie->id;
                    $regel->afspraak_id = $afspraak->id;
                    $user = User::findOrFail(Auth::user()->id);
                    $regel->gebruiker_id = $user->id;
                    $regel->omschrijving = $user->name." heeft de afspraak verwerkt, afspraak is voldaan";
                    $regel->save();

                    $geschiedenisString = date('d-m-Y', strtotime($afspraak->startdatum)).": Garantie met de volgende klacht opgelost, ".$afspraak->omschrijving;

                    $geschiedenis = new History;
                    $geschiedenis->afspraak_id = $afspraak->id;
                    $geschiedenis->gebruiker_id = Auth::user()->id;
                    $geschiedenis->klant_id = $afspraak->klant_id;
                    $geschiedenis->datum = date('Y-m-d');
                    $geschiedenis->omschrijving = $geschiedenisString;
                    $geschiedenis->type = "werkzaamheden";
                    $geschiedenis->save();

                    $log = new Log;
                    $log->gebruiker_id = $user->id;
                    $log->klant_id = $relatie->id;
                    $log->omschrijving = $user->name." heeft deze klant, ".$relatie->postcode." - ".$relatie->huisnummer." afspraak verwerkt.";
                    $log->type = "Afspraak";
                    $log->save();

                    $afspraak->betaald=1;
                    $afspraak->save(); //update met status
                    return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                }
            }
            elseif(AppointmentType::find($afspraak->afspraaktype_id)->omschrijving == "Uitgestelde betaling")
            {
                if($extra == 1)
                {
                    $danger = "";
                    $relatie = Client::findOrFail($afspraak->klant_id);
                    $activiteiten = Work::where('actief', 1)->get();
                    $garantie = 9999;

                    $page = "relatie";
                    $sub = "relover";
                    return view('afspraak.extrawerk', compact('danger','afspraak', 'relatie', 'extra', 'voldaan', 'activiteiten', 'garantie', 'page', 'sub'));
                }
                elseif($extra == 0 && $voldaan == 0)
                {
                    // er is geen extra werk en het op te moeten halen bedrag is niet voldaan.
                    // status is mislukt
                    $relatie = Client::findOrFail($afspraak->klant_id);
                    $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
                    $sha->status_id = 3;
                    $sha->afspraak_id = $afspraak->id;
                    $sha->gebruiker_id = Auth::user()->id;
                    $sha->omschrijving = "Afspraak is doorgegaan, maar uitgestelde betaling is niet betaald.";
                    $sha->save();

                    $regel = new History;
                    $regel->datum = date('Y-m-d');
                    $regel->klant_id = $relatie->id;
                    $regel->afspraak_id = $afspraak->id;
                    $user = User::findOrFail(Auth::user()->id);
                    $regel->gebruiker_id = $user->id;
                    $regel->omschrijving = $user->name." heeft de Uitgestelde betaling verwerkt, afspraak is niet voldaan.";
                    $regel->save();

                    $log = new Log;
                    $log->gebruiker_id = $user->id;
                    $log->klant_id = $relatie->id;
                    $log->omschrijving = $user->name." heeft deze klant, ".$relatie->postcode." - ".$relatie->huisnummer." afspraak verwerkt.";
                    $log->type = "Afspraak";
                    $log->save();

                    $return = false;
                    $page = "relatie";
                    $sub = "relover";
                    return view('afspraak.nietgelukt', compact('afspraak', 'relatie', 'page', 'sub', 'return'));
                }
                elseif($extra == 0 && $voldaan==1)
                {
                    $relatie = Client::findOrFail($afspraak->klant_id);

                    $nieuw = Appointment::findOrFail($id);
                    $sha = Status_has_appointment::where('afspraak_id', $nieuw->id)->first();
                    $sha->status_id = 4;
                    $sha->afspraak_id = $nieuw->id;
                    $sha->gebruiker_id = Auth::user()->id;
                    $sha->omschrijving = "Uitgestelde betaling voldaan";
                    $sha->save();

                    $regel = new History;
                    $regel->datum = date('Y-m-d');
                    $regel->klant_id = $relatie->id;
                    $regel->afspraak_id = $nieuw->id;
                    $user = User::findOrFail(Auth::user()->id);
                    $regel->gebruiker_id = $user->id;
                    $regel->omschrijving = $user->name." heeft de Uitgestelde betaling verwerkt, afspraak is voldaan";
                    $regel->save();

                    $log = new Log;
                    $log->gebruiker_id = $user->id;
                    $log->klant_id = $relatie->id;
                    $log->omschrijving = $user->name." heeft deze klant, ".$relatie->postcode." - ".$relatie->huisnummer." afspraak verwerkt.";

                    $log->type = "Afspraak";
                    $log->save();

                    $nieuw->betaald=1;
                    $nieuw->niet_betaald = 0;
                    $oud = Appointment::find($nieuw->afspraak_id);
                    $oud->niet_betaald -= $nieuw->totaalbedrag;
                    if($oud->niet_betaald < 0.01)
                    {
                         //update met status
                        $sha = Status_has_appointment::where('afspraak_id', $oud->id)->first();
                        //dd($sha);
                        $sha->status_id = 4;
                        $count = count(Appointment::where('afspraak_id', $oud->id)->where('afspraaktype_id', 3)->get());
                        $sha->omschrijving = "Na ".$count." is de afspraak volledig betaald.";
                        $sha->save();
                    }
                    $oud->save();
                    $nieuw->save();
                    return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                }
            }
            elseif(AppointmentType::find($afspraak->afspraaktype_id)->omschrijving == "Offerte opstellen")
            {
                if($extra == 1)
                {
                    $relatie = Client::find($afspraak->klant_id);
                    $afspraak->setLog(Auth::user()->name." heeft deze afspraak verwerkt.");
                    $afspraak->setStatus(4,"Vrijblijvende afspraak is verwerkt.");
                    $afspraak->setHistory(Auth::user()->name." heeft de afspraak verwerkt met status:".Status_has_appointment::where('afspraak_id', $afspraak->id)->first()->omschrijving);

                    $offerte = Offerte::where('afspraak_id', $afspraak->id)->first();
                    if($offerte != null)
                    {
                        $ohas = Activiteit_has_offerte::with('activiteit')->where('offerte_id', $offerte->id)->get();
                        $activiteiten = Work::where('actief', 1)->get();
                        return view('offerte.verwerkOfferte', compact('afspraak', 'activiteiten', 'ohas', 'offerte', 'relatie', 'voldaan'));
                    }
                    else
                    {
                        $verkoper = Worklist_has_appointments::where('afspraak_id', $afspraak->id)->first();
                        // dd($verkoper);
                        if($verkoper == null)
                            $werknemers = Employee::where('actief', 1)->get();
                        else
                            $werknemers = Employee::where('id', $verkoper->verkoper)->get();
                        if($werknemers == null)
                            $werknemers = Employee::where('actief', 1)->get();

                        $afspraken = Appointment::where('id', $afspraak->id)->get();
                        $werknemers = Employee::all();
                        $page = "financieel";
                        $sub = "offerte";
                        return view('offerte.nieuw', compact('werknemers', 'afspraak', 'afspraken', 'relatie', 'page', 'sub', 'voldaan', 'werknemers'));
                    }
                }
                elseif($extra == 0 && $voldaan == 0)
                {
                    // dd("Nog bouwen.Offerte 'extra' = 0 voldaan = 0");
                    $relatie = Client::find($afspraak->klant_id);
                    $offerte = Offerte::where('afspraak_id', $afspraak->id)->first();

                    $afspraak->setLog(Auth::user()->name." heeft deze afspraak verwerkt.");
                    $afspraak->setStatus(4,"Vrijblijvende afspraak is verwerkt.");
                    $afspraak->setHistory(Auth::user()->name." heeft de afspraak verwerkt met status:".Status_has_appointment::where('afspraak_id', $afspraak->id)->first()->omschrijving);

                    $afspraak->save();
                    if($offerte != null)
                    {
                        return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de afspraak verwerkt.');
                    }
                    $verkoper = Worklist_has_appointments::where('afspraak_id', $afspraak->id)->first();
                    // dd($verkoper);
                    if($verkoper == null)
                        $werknemers = Employee::where('actief', 1)->get();
                    else
                        $werknemers = Employee::where('id', $verkoper->verkoper)->get();
                    if($werknemers == null)
                        $werknemers = Employee::where('actief', 1)->get();

                    $afspraken = Appointment::where('id', $afspraak->id)->get();

                    $page = "financieel";
                    $sub = "offerte";
                    $werknemers = Employee::all();
                    return view('offerte.nieuw', compact('werknemers', 'afspraak', 'afspraken', 'relatie', 'page', 'sub', 'werknemers'));

                }
                elseif($extra == 0 && $voldaan==1)
                {
                    $relatie = Client::find($afspraak->klant_id);
                    $offerte = Offerte::where('afspraak_id', $afspraak->id)->first();

                    $afspraak->setLog(Auth::user()->name." heeft deze afspraak verwerkt.");
                    $afspraak->setStatus(4,"Vrijblijvende afspraak is verwerkt.");
                    $afspraak->setHistory(Auth::user()->name." heeft de afspraak verwerkt met status:".Status_has_appointment::where('afspraak_id', $afspraak->id)->first()->omschrijving);
                    $afspraak->save();

                    $verkoper = Worklist_has_appointments::where('afspraak_id', $afspraak->id)->first();
                    // dd($verkoper);
                    if($verkoper == null)
                        $werknemers = Employee::where('actief', 1)->get();
                    else
                        $werknemers = Employee::where('id', $verkoper->verkoper)->get();
                    if($werknemers == null)
                        $werknemers = Employee::where('actief', 1)->get();

                    $afspraaktypes = AppointmentType::where('id', 1)->get();

                    if($offerte != null)
                    {
                        $ohas = Activiteit_has_offerte::with('activiteit')->where('offerte_id', $offerte->id)->get();

                        $page = "relatie";
                        $sub = "relover";
                        return view('offerte.addToAppointment', compact('afspraak', 'ohas', 'werknemers', 'afspraaktypes', 'relatie', 'offerte', 'page', 'sub'));
                    }

                    //new offer
                    $verkoper = Worklist_has_appointments::where('afspraak_id', $afspraak->id)->first();
                    // dd($verkoper);
                    if($verkoper == null)
                        $werknemers = Employee::where('actief', 1)->get();
                    else
                        $werknemers = Employee::where('id', $verkoper->verkoper)->get();
                    if($werknemers == null)
                        $werknemers = Employee::where('actief', 1)->get();

                    $afspraken = Appointment::where('id', $afspraak->id)->get();

                    $page = "financieel";
                    $sub = "offerte";
                    return view('offerte.nieuw', compact('werknemers', 'afspraken', 'afspraak', 'relatie', 'page', 'sub', 'voldaan'));

                }
            }
            elseif(AppointmentType::find($afspraak->afspraaktype_id)->omschrijving == "Betalingsherinnering")
            {
                if($extra == 1)
                {
                    dd("Nog bouwen. Betalingsherinnering 'extra' = 1");
                }
                elseif($input['gelukt'] == 0)
                {
                    //Afspraak moet verlengd worden
                    // $relatie = Klant::find($afspraak->klant_id);
                    // return view('afspraak.verleng', compact('page', 'sub', 'afspraak', 'relatie'));
                    $aantaldagen = $input['betalingsherinnering'];
                    $afspraak->startdatum = date('Y-m-d H:i', strtotime($afspraak->startdatum.' + '.$aantaldagen.' days'));
                    $afspraak->einddatum = date('Y-m-d H:i', strtotime($afspraak->einddatum.' + '.$aantaldagen.' days'));
                    $afspraak->save();

                    return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Betalingsherinnering aangepast.');

                    dd("Nog bouwen.Betalingsherinnering 'extra' = 0 voldaan = 0");
                }
                elseif($input['gelukt'] == 1)
                {
                    $oud = Appointment::find($afspraak->afspraak_id);
                    $oud->betaald = 1;
                    $oud->niet_betaald = 0;
                    $oud->bank = $input['bank'];
                    $oud->save();
                    $status_oud = Status_has_appointment::where('afspraak_id', $oud->id)->first();
                    $status_oud->status_id = 4;
                    $status_oud->omschrijving = "Betalingsherinnering nagekomen, afspraak betaald.";
                    $status_oud->save();

                    $status_nieuw = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
                    $status_nieuw->status_id = 4;
                    $status_nieuw->omschrijving = "Betalingsherinnering nagekomen, oude afspraak betaald.";
                    $status_nieuw->save();
                    $afspraak->betaald = 1;
                    $afspraak->save();

                    return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Betalingsherinnering verwerkt.');

                    dd("Nog bouwen. Betalingsherinnering 'extra' = 0 voldaan = 1");
                }
            }
            elseif(AppointmentType::find($afspraak->afspraaktype_id)->omschrijving == "Terugbetaalverzoek")
            {
                if($extra == 1)
                {
                    dd("Nog bouwen. Terugbetaalverzoek 'extra' = 1");
                }
                elseif($extra == 0 && $voldaan == 0)
                {
                    dd("Nog bouwen.Terugbetaalverzoek 'extra' = 0 voldaan = 0");
                }
                elseif($extra == 0 && $voldaan==1)
                {
                    dd("Nog bouwen. Terugbetaalverzoek 'extra' = 0 voldaan = 1");
                }
            }
        }
    }

    /*
    *   If appointment had extra workactivities, they will be added here
    *   Log and history has been updated
    */
    public function setWerk($id)
    {
        $afspraak = Appointment::findOrFail($id); // huidige afspraak die verwerkt wordt.
        $relatie = Client::findOrFail($afspraak->klant_id);

        $input = Request::all();
        $extra = $input["extra"];
        $voldaan = $input['voldaan'];
        // dd($input);

        $double = false;
        for($k=0;$k<count($input['activiteiten']);$k++) //check for double activities
        {
            for($l=0;$l<count($input['activiteiten']);$l++)
            {
                if($input['activiteiten'][$l]==$input['activiteiten'][$k] && $k != $l)
                    $double=true;
            }
        }
        if(!$double) //if there are no doubles, then process activities
        {
            $i = 1;
            $rij = array();
            foreach($input['aantal'] as $r)
            {
                if($i != count($input['aantal']))
                {
                    if(isset($r["s_".$i.":i_0"]))
                        array_push($rij, $r["s_".$i.":i_0"]);
                }
                else
                {
                    $i--;
                    array_push($rij, $input['aantal'][$i]);
                }
                $i++;
            }

            if(isset($input['service'])){
                $service = $input['service'];
                for($i = 0; $i < count($service); $i++){
                    $wha = Appointment_has_activities::where('activiteit_id', $service[$i])->where('afspraak_id', $afspraak->id)->first();
                    $wha->prijs = 0.00;
                    $wha->save();
                }
            }
            //rij geeft nu in de juiste volgorde de hoeveelheden aan.

            //totaalbedrag uitrekenen
            if($afspraak->afspraaktype_id == 3) //uitgestelde betaling
            {

                $totaalbedrag = $afspraak->totaalbedrag;                                                                                // zet totaalbedrag in var
                $oud = Appointment::find($afspraak->afspraak_id);                                                                          // vindt de oude afspraak
                $regel = new History;                                                                                              // geschiedenis aanmaken
                $regel->datum = date('Y-m-d');
                $regel->klant_id = $id;
                $regel->afspraak_id = $afspraak->id;
                $user = User::findOrFail(Auth::user()->id);
                $regel->gebruiker_id = $user->id;
                $regel->omschrijving = $user->name." heeft de afspraak verwerkt, betaling van € ".$oud->totaalbedrag;
                $regel->save();
                $oud->niet_betaald -= $totaalbedrag;                                                                                    // Haal het totaalbedrag van de uitgestelde betaling af van de oude afspraak.
                if($oud->niet_betaald <= 0)
                {
                     //update met status
                    $sha = Status_has_appointment::where('afspraak_id', $oud->id)->first();
                    $sha->status_id = 4;
                    $count = count(Appointment::where('afspraak_id', $oud->id)->where('afspraaktype_id', 3)->get());
                    $sha->omschrijving = "Na ".$count."afspraken is de afspraak volledig betaald.";
                    $sha->save();
                }
            }
            else // Garantie, uitvoeren werkzaamheden etc is op dit moment gelukt, met extra werk, maar niet betaald.
            {
                $totaalbedrag = 0; // create var and declare it 0
            }

            if(isset($input['klusprijsAan']) && $afspraak->afspraaktype_id != 4 && $afspraak->afspraaktype_id != 3)
            {
                $afspraak->klusprijs = 1;
                $klusprijs = $input['klusprijs'];
                $totaalbedrag = $klusprijs;
            }

            for($i = 0; $i < count($input['activiteiten']);$i++) //add activities on one appointment
            {
                $act = Work::findOrFail($input['activiteiten'][$i]);
                // $bedrag = $act->prijs * $rij[$i];
                // $totaalbedrag = $totaalbedrag + $bedrag;
                $aha = new Appointment_has_activities;
                $aha->activiteit_id = $act->id;
                $aha->afspraak_id = $afspraak->id;
                $aha->aantal = $input['aantal'][$i];
                // dd($klusprijs);
                if(isset($klusprijs))
                {
                    if($klusprijs > 0)
                    {
                        $aha->prijs = ($input['prijs'][$i] > 0 ? $input['prijs'][$i] : $act->prijs);
                        $bedrag = $aha->prijs * $aha->aantal;
                        if($klusprijs - $bedrag <= 0)
                        {
                            $aha->kortingsbedrag = $bedrag - $klusprijs;
                            $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;
                            $klusprijs = $klusprijs - $bedrag;

                        }
                        elseif($klusprijs - $bedrag > 0)
                        {
                            $aha->kortingsbedrag = $input['korting'][$i];
                            $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;
                            $klusprijs = $klusprijs - $bedrag;
                        }
                    }
                }
                else
                {
                    $aha->prijs = ($input['prijs'][$i] > 0 ? $input['prijs'][$i] : $act->prijs);
                    $aha->kortingsbedrag = $input['korting'][$i];
                    $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;
                    $bedrag = $aha->prijs * $input['aantal'][$i];
                    //$totaalbedrag = $totaalbedrag + $bedrag;
                }
                $aha->opmerking = $input['opmerking'][$i];
                $aha->save();
            }
            // echo $totaalbedrag."<br>";
            $afspraakwerken = Appointment_has_activities::where('afspraak_id', $afspraak->id)->get();
            foreach($afspraakwerken as $aw)
            {
                // echo $totaalbedrag."<br>";
                if(!isset($klusprijs))
                    $totaalbedrag += $aw->prijs * $aw->aantal;
                // echo "erbij opgeteld-> ".$totaalbedrag."<br>";
            }
            if($afspraak->afspraaktype_id == 1)
            {
                // echo "afspraak->totaalbedrag = ".$afspraak->totaalbedrag.", $totaalbedrag = ".$totaalbedrag."<br>";
                if(!isset($klusprijs))
                {
                    $afspraak->totaalbedrag += $totaalbedrag;
                }
                else
                    $afspraak->totaalbedrag = $totaalbedrag;  // als het een 'normale' afspraak is, wordt het totaalbedrag opgeteld met de extra werkzaamheden.
                // echo "afspraak->totaalbedrag = ".$afspraak->totaalbedrag.", $totaalbedrag = ".$totaalbedrag."<br>";
                if($voldaan == 0)
                    $afspraak->niet_betaald = $afspraak->totaalbedrag;                  // de afspraak was gemarkeerd als niet betaald, dus het (nieuwe) totaalbedrag wordt hier als niet betaald gemarkeerd.
                // echo "afspraak->totaalbedrag = ".$afspraak->totaalbedrag.", $totaalbedrag = ".$totaalbedrag."<br>";
            }
            if($afspraak->afspraaktype_id == 3)
            {
                // echo "afspraak->totaalbedrag = ".$afspraak->totaalbedrag.", $totaalbedrag = ".$totaalbedrag."<br>";
                $afspraak->totaalbedrag = $totaalbedrag;
                // echo "afspraak->totaalbedrag = ".$afspraak->totaalbedrag.", $totaalbedrag = ".$totaalbedrag."<br>";
                if($voldaan == 0)
                    $afspraak->niet_betaald = $afspraak->totaalbedrag;
                // echo "afspraak->totaalbedrag = ".$afspraak->totaalbedrag.", $totaalbedrag = ".$totaalbedrag."<br>";
            }
            // dd("bla");
            $aha = Appointment_has_activities::where('afspraak_id', $afspraak->id)->get();
            foreach($aha as $a)
            {
                if(Activiteit_has_materiaal::where('activiteit_id', $a->activiteit_id)->exists())
                {
                    $ahm = Activiteit_has_materiaal::where('activiteit_id', $a->activiteit_id)->get();
                    foreach($ahm as $b)
                    {
                        if(Voorraad::where('materiaal_id', $b->materiaal_id)->where('datum', date('Y-m-d', strtotime($afspraak->startdatum)))->where('klant_id', $afspraak->klant_id)->exists())
                        {
                            if(Voorraad::where('materiaal_id', $b->materiaal_id)->where('datum', date('Y-m-d', strtotime($afspraak->startdatum)))->where('klant_id', $afspraak->klant_id)->first()->mutatie != $b->aantal * $a->aantal)
                            {
                                $stockMutation = Voorraad::where('materiaal_id', $b->materiaal_id)->where('datum', date('Y-m-d', strtotime($afspraak->startdatum)))->where('klant_id', $afspraak->klant_id)->first();
                                $stockMutation->mutatie = $b->aantal * $a->aantal * -1;
                                $stockMutation->save();
                            }
                        }
                        else
                        {
                            $stockMutation = new Voorraad;
                            $stockMutation->materiaal_id = $b->materiaal_id;
                            $stockMutation->datum = date('Y-m-d', strtotime($afspraak->startdatum));
                            $stockMutation->mutatie = $b->aantal * $a->aantal * -1;
                            $stockMutation->klant_id = $afspraak->klant_id;
                            $stockMutation->save();
                        }
                    }
                }
            }

            $afspraak->save();


            if($voldaan==1)
            {
                if($afspraak->afspraaktype_id == 1 || $afspraak->afspraaktype_id == 3) //normal app or delayed payment
                {

                    $afspraak = Appointment::findOrFail($id);
                    $relatie = Client::findOrFail($afspraak->klant_id);

                    $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
                    $sha->status_id = 4;
                    $sha->afspraak_id = $afspraak->id;
                    $sha->gebruiker_id = Auth::user()->id;
                    $sha->omschrijving = "Afspraak is voldaan en verwerkt";
                    $sha->save();

                    $regel = new History;
                    $regel->datum = date('Y-m-d');
                    $regel->klant_id = $id;
                    $regel->afspraak_id = $afspraak->id;
                    $user = User::findOrFail(Auth::user()->id);
                    $regel->gebruiker_id = $user->id;
                    $regel->omschrijving = $user->name." heeft de afspraak verwerkt, afspraak is voldaan";
                    $regel->save();

                    $log = new Log;
                    $log->gebruiker_id = $user->id;
                    $log->klant_id = $relatie->id;
                    $log->omschrijving = $user->name." heeft deze klant, ".$relatie->postcode." - ".$relatie->huisnummer." afspraak verwerkt.";
                    $log->type = "Afspraak";
                    $log->save();

                    $afspraak->betaald=1;
                    $afspraak->save(); //update met status
                    return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                }
                elseif($afspraak->afspraaktype_id == 4) // warranty
                {
                    $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
                    $sha->status_id = 4;
                    $sha->afspraak_id = $afspraak->id;
                    $sha->gebruiker_id = Auth::user()->id;
                    $sha->omschrijving = "Garantie afspraak is voldaan en verwerkt";
                    $sha->save();

                    $regel = new History;
                    $regel->datum = date('Y-m-d');
                    $regel->klant_id = $id;
                    $regel->afspraak_id = $afspraak->id;
                    $user = User::findOrFail(Auth::user()->id);
                    $regel->gebruiker_id = $user->id;
                    $regel->omschrijving = $user->name." heeft de Garantie afspraak verwerkt, afspraak is voldaan";
                    $regel->save();

                    $log = new Log;
                    $log->gebruiker_id = $user->id;
                    $log->klant_id = $relatie->id;
                    $log->omschrijving = $user->name." heeft deze klant, ".$relatie->postcode." - ".$relatie->huisnummer." Garantie afspraak verwerkt.";
                    $log->type = "Afspraak";
                    $log->save();

                    $afspraak->betaald=1;
                    $afspraak->save(); //update met status
                    return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de Garantie afspraak succesvol verwerkt.');
                }
            }
            else
            {
                $afspraak = Appointment::find($id);
                //status updates
                if(Status_has_appointment::where('afspraak_id', $afspraak->id)->exists() )
                {
                    $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
                    $sha->status_id = 3;
                    $sha->afspraak_id = $afspraak->id;
                    $sha->gebruiker_id = Auth::user()->id;
                    $sha->omschrijving = "Afspraak is niet voldaan, maar wel verwerkt.";
                    $sha->save();
                }
                else
                {
                    $sha = new Status_has_appointment;
                    $sha->status_id = 3;
                    $sha->afspraak_id = $afspraak->id;
                    $sha->gebruiker_id = Auth::user()->id;
                    $sha->omschrijving = "Afspraak is niet voldaan, maar wel verwerkt.";
                    $sha->save();
                }
                //geschiedenis updates
                $regel = new History;
                $regel->datum = date('Y-m-d');
                $regel->klant_id = $id;
                $regel->afspraak_id = $afspraak->id;
                $user = User::findOrFail(Auth::user()->id);
                $regel->gebruiker_id = $user->id;
                $regel->omschrijving = $user->name." heeft de afspraak verwerkt, afspraak is niet voldaan.";
                $regel->save();
                //log updates
                $log = new Log;
                $log->gebruiker_id = $user->id;
                $log->klant_id = $relatie->id;
                $log->omschrijving = $user->name." heeft deze afspraak van klant, ".$relatie->postcode." - ".$relatie->huisnummer." verwerkt.";
                $log->type = "Afspraak";
                $log->save();
                $relatie = Client::findOrFail($afspraak->klant_id);
                $types = AppointmentType::all();
                $ub = "";

                if(isset($input['meer']))
                {
                    return redirect('/afspraak/'.$id.'/verwerken/nietgelukt/nieuweafspraak');
                }

                $page = "relatie";
                $sub = "relover";
                $actperafs = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function($afs) use ($relatie, $afspraak)
                {
                    $afs->where('klant_id', $relatie->id)->where('id', $afspraak->id);
                })->get();

                $nieuweafspraken = Appointment::where('afspraak_id', $afspraak->id)->get();
                $gelukt = true;

                return view('afspraak.setAfspraak', compact('relatie', 'afspraak', 'types', 'ub', 'page', 'sub', 'actperafs', 'gelukt', 'nieuweafspraken'));
            }

        }
        else // return with error
        {
            $afspraak = Appointment::findOrFail($id);
            $relatie = Client::findOrFail($afspraak->klant_id);
            $activiteiten = Work::where('actief', 1)->get();
            $danger = 'U kunt niet meerdere malen dezelfde werkzaamheden opgeven.';

            $page = "relatie";
            $sub = "relover";
            return view('afspraak.extrawerk', compact('relatie', 'afspraak', 'activiteiten', 'danger', 'extra', 'voldaan', 'page', 'sub'));
        }
    }

    /*
    *   If appointment failed, the failed view will open.
    */
    public function nietGelukt($id)
    {

        $afspraak = Appointment::findOrFail($id);
        $relatie = Client::findOrFail($afspraak->klant_id);

        $page = "relatie";
        $sub = "relover";
        return view('afspraak.nietgelukt', compact('afspraak', 'relatie', 'page', 'sub'));
    }

    /*
    *   Store failed appointment, als write too log and history
    *   if there is a new appointment, it redirects to new appointment
    *   else to relation profile
    */
    public function nietgelukteind($id)
    {
        $input = Request::all();
        //dd(isset($input['afspraak']));
        $afspraak = Appointment::findOrFail($id);
        $relatie = Client::findOrFail($afspraak->klant_id);

        $afspraak->reden = $input['reden'];
        if(isset($input['eigenschuld']))
        {
            $afspraak->eigenschuld = $input['eigenschuld'];
        }
        else
        {
            $afspraak->eigenschuld = 0;
        }
        if($afspraak->afspraaktype_id == 3)
        {
            $ubsetAmount = true;
            $types = AppointmentType::where('omschrijving', 'Betalingsherinnering')->orWhere('omschrijving', 'Uitgestelde betaling')->get();
            $afspraak->save();
            $afspraak = Appointment::findOrFail($id);

            $page = "relatie";
            $sub = "relover";
            return view('afspraak.setAfspraak', compact('ubsetAmount', 'relatie', 'afspraak', 'types', 'page', 'sub'));
        }
        $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
        if(isset($input['afspraak']))
            $sha->status_id = 29;
        else
            $sha->status_id = 2;
        $sha->omschrijving = Auth::user()->name." heeft deze afspraak verwerkt.";
        $sha->save();
        $i = $afspraak->id;
        $afspraak->save();

        $regel = new History;
        $regel->datum = date('Y-m-d');
        $regel->klant_id = $relatie->id;
        $regel->afspraak_id = $afspraak->id;
        $user = User::findOrFail(Auth::user()->id);
        $regel->gebruiker_id = $user->id;
        $regel->omschrijving = $user->name." heeft de afspraak van ".$afspraak->returnDate($afspraak->startdatum)." --- ".$afspraak->returnDate($afspraak->einddatum)." verwerkt met de reden:\n ".$input['reden'];
        $regel->save();

        $log = new Log;
        $log->gebruiker_id = $user->id;
        $log->klant_id = $relatie->id;
        $log->omschrijving = $user->name." heeft deze klant verwerkt met afspraak: ".$i;
            $log->type = "Afspraak";
        $log->save();

        if(isset($input['afspraak']))
        {
            return redirect('/afspraak/'.$afspraak->id.'/verwerken/nietgelukt/nieuweafspraak');
        }
        else
        {
            return redirect('relatie/'.$relatie->id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
        }

    }

    /*
    *   Open view for new appointment
    */
    public function setAfspraak($id)
    {
        $afspraak = Appointment::findOrFail($id);
        $relatie = Client::findOrFail($afspraak->klant_id);
        $types = AppointmentType::all();
        if(session('ub'))
        {
            $ub = true;
        }

        $actperafs = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function($afs) use ($relatie, $afspraak)
        {
            $afs->where('klant_id', $relatie->id)->where('id', $afspraak->id);
        })->get();


        if(AppointmentType::find($afspraak->afspraaktype_id)->omschrijving == "Uitgestelde betaling")
        {
            $ubsetAmount = "test";
            $types = AppointmentType::where('omschrijving', 'Uitgestelde Betaling')->get();


            $page = "relatie";
            $sub = "relover";
            if(isset($ub))
            {
                $nieuweafspraken = Appointment::where('afspraak_id', $id)->get();
                return view('afspraak.setAfspraak', compact('relatie', 'afspraak', 'types', 'ubsetAmount', 'page', 'sub', 'actperafs', 'ub', 'nieuweafspraken'));
            }
            return view('afspraak.setAfspraak', compact('relatie', 'afspraak', 'types', 'ubsetAmount', 'page', 'sub', 'actperafs'));
        }

        $page = "relatie";
        $sub = "relover";
        $nieuweafspraken = Appointment::where('afspraak_id', $id)->get();
        if(isset($ub))
        {
            return view('afspraak.setAfspraak', compact('relatie', 'afspraak', 'types', 'page', 'sub', 'actperafs', 'ub', 'nieuweafspraken'));
        }
        return view('afspraak.setAfspraak', compact('relatie', 'afspraak', 'types', 'page', 'sub', 'actperafs', 'nieuweafspraken'));
    }

    /*
    *   Store new appointment
    */
    public function setAfspraakeind($id)
    {
        $oud = Appointment::findOrFail($id);
        $relatie = Client::findOrFail($oud->klant_id);
        $input = Request::all();
        // dd($input);

        $afspraak = new Appointment;

        if($input['radio_demo'] < 2 )
        {
            $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
            $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
            $startstamp = $strtotime->getTimestamp();
            $endstamp = $strtotimee->getTimestamp();
            if($input['radio_demo'] == 1)
            {
                $afspraak->startdatum = date('Y-m-d', $startstamp)." 12:30";
                $afspraak->einddatum = date('Y-m-d', $startstamp)." 17:00";
            }
            else
            {
                $afspraak->startdatum = date('Y-m-d', $startstamp)." 08:00";
                $afspraak->einddatum = date('Y-m-d', $startstamp)." 12:30";
            }
        }
        else
        {
            $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
            $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
            $startstamp = $strtotime->getTimestamp();
            $endstamp = $strtotimee->getTimestamp();
            $afspraak->startdatum = date('Y-m-d H:i:s', $startstamp);
            $afspraak->einddatum = date('Y-m-d H:i:s', $endstamp);
        }
        $afspraak->klant_id = $input['klant_id'];
        $afspraak->geschreven_door = Auth::user()->werknemer_id;
        $afspraak->verwerkt_door = $input['verwerkt_door'];
        $afspraak->afspraaktype_id = $input['afspraaktype_id'];
        $afspraak->omschrijving = $input['omschrijving'];
        $afspraak->afspraak_id = $oud->id;
        $afspraak->offerte_id = $oud->id;
        $afspraak->klusprijs = $oud->klusprijs;
        if(isset($input['tebetalen']))
            $afspraak->totaalbedrag = $input['tebetalen'];

        // dd($oud);
        if(($oud->afspraaktype_id == 1 || $oud->afspraaktype_id == 3) && ($afspraak->afspraaktype_id == 3 || $afspraak->afspraaktype_id == 5)) //Uitvoering Werkzaamheden && uitgestelde betaling || Betalingsherinnering
        {
            if(isset($input['betaald']))
            {
                if($input['betaald'] > $oud->totaalbedrag) // als het betaalde bedrag hoger is dan het factuur bedrag, return met error.
                {
                    if(isset($input['gelukt']))
                    {
                        return redirect('/afspraak/'.$oud->id.'/verwerken/gelukt/nieuweafspraak')->with('danger', 'Het betaalde bedrag is hoger dan het factuurbedrag, dit kan niet.')->with('ub', 'ub')->with('gelukt', true)->with('test', 'test');
                    }
                    return redirect('/afspraak/'.$oud->id.'/verwerken/nietgelukt/nieuweafspraak')->with('danger', 'Het betaalde bedrag is hoger dan het factuurbedrag, dit kan niet.')->with('ub', 'ub')->with('test', 'test');
                }
            }

            $nieuweafspraken = Appointment::where('afspraak_id', $oud->id)->get();
            $subtotal = 0.00;
            foreach($nieuweafspraken as $nas) // optellen van alle bedragen tot nu to
            {
                $subtotal += $nas->totaalbedrag;
            }

            if(isset($input['betaald'])) // ophalen van het al betaalde bedrag en optellen bij alle bedragen tot nu toe. Als dit hoger is, return met error.
            {
                $subtotal += $input['betaald'];
                if($subtotal > $oud->totaalbedrag)
                {
                    return redirect('/afspraak/'.$oud->id.'/verwerken/gelukt/nieuweafspraak')->with('danger', 'Het betaalde bedrag inclusief nieuwe afspraken is hoger dan het factuurbedrag, dit kan niet.')->with('ub', 'ub')->with('gelukt', true)->with('test', 'test');
                }
            }

            if($subtotal + $input['tebetalen'] > $oud->totaalbedrag) //totale bedrag te betalen is hoger dan factuurbedrag, return met error.
            {
                return redirect('/afspraak/'.$oud->id.'/verwerken/gelukt/nieuweafspraak')->with('danger', 'Het betaalde bedrag inclusief nieuwe afspraken is hoger dan het factuurbedrag, dit kan niet.')->with('ub', 'ub')->with('gelukt', true)->with('test', 'test');
            }
            // dd($oud->niet_betaald);

            if($subtotal + $input['tebetalen'] < $oud->niet_betaald)
            {
                $tweede = true;
                $gelukt = true;

                if(isset($input['betaald']))
                    $oud->niet_betaald = $oud->niet_betaald - $input['betaald'];


                $afspraak->save();
                $nieuw = Appointment::where('startdatum',date('Y-m-d H:i:s', $startstamp))->where('klant_id', $input['klant_id'])->first();
                $temp = new Status_has_appointment;
                $temp->status_id = 1;
                $temp->afspraak_id = $afspraak->id;
                $temp->gebruiker_id = Auth::user()->id;
                $sha = Status::find(1);
                // dd($sha);
                $temp->omschrijving = $sha->omschrijving." door ".Auth::user()->name;
                $temp->save();

                $oud->save();
                $afspraak = Appointment::find($id);
                $nieuweafspraken = Appointment::where('afspraak_id', $oud->id)->get();

                $page = "relatie";
                $sub = "relover";
                $actperafs = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function($afs) use ($relatie, $afspraak)
                {
                    $afs->where('klant_id', $relatie->id)->where('id', $afspraak->id);
                })->get();

                $types = AppointmentType::where('id', 3)->orWhere('id', 5)->get();
                $ub = "";
                $succes = "Afspraak Toegevoegd.";
                return view('afspraak.setAfspraak', compact('tweede', 'gelukt', 'afspraak', 'nieuweafspraken', 'page', 'sub', 'actperafs', 'types', 'relatie', 'ub', 'succes'));
            }

            if($subtotal + $input['tebetalen'] == $oud->niet_betaald)
            {
                $tweede = true;
                $gelukt = true;

                if(isset($input['betaald']))
                    $oud->niet_betaald = $oud->niet_betaald - $input['betaald'];

                $afspraak->save();
                $nieuw = $afspraak;
                $temp = new Status_has_appointment;
                $temp->status_id = 1;
                $temp->afspraak_id = $nieuw->id;
                $temp->gebruiker_id = Auth::user()->id;
                $sha = Status::where('id', 1)->first();
                $temp->omschrijving = $sha->omschrijving." door ".Auth::user()->name;
                $temp->save();
                $oud_sha = Status_has_appointment::where('afspraak_id', $oud->id)->first();
                // $oud_sha->status_id = 4;
                // $oud_sha->omschrijving = Status::find(4)->omschrijving;
                $oud_sha->save();
                $oud->save();
                $afspraak = Appointment::find($id);
                $nieuweafspraken = Appointment::where('afspraak_id', $oud->id)->get();

                $page = "relatie";
                $sub = "relover";
                $types = AppointmentType::where('id', 3)->orWhere('id', 5)->get();
                $ub = "";
                $succes = "Afspraak Toegevoegd.";
                return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspra(a)k(en) succesvol toegevoegd.');
            }

        }
        if($oud->afspraaktype_id == 2) // offerte opstellen
        {
            $afspraak->save();
            $nieuw = Appointment::where('startdatum',date('Y-m-d H:i:s', $startstamp))->where('klant_id', $input['klant_id'])->first();
            $temp = new Status_has_appointment;
            $temp->status_id = 1;
            $temp->afspraak_id = $afspraak->id;
            $temp->gebruiker_id = Auth::user()->id;
            $temp->omschrijving = Status::find(1)->omschrijving." door ".Auth::user()->name;
            $temp->save();

            $log = new Log;
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $afspraak->klant_id;
            $log->omschrijving = Auth::user()->name." heeft een nieuwe afspraak toegevoegd.";
            $log->type = "afspraak";
            $log->save();

            return redirect('/relatie/'.$oud->klant_id)->with('succes', 'Afspraak succesvol toegevoegd.');
        }

        if($oud->afspraaktype_id == 1 && $afspraak->afspraaktype_id == 1) // uitvoeren werkzaamheden
        {
            $afspraak->save();
            $nieuw = $afspraak;
            // $nieuw = Afspraak::where('startdatum',date('Y-m-d H:i:s', $startstamp))->where('klant_id', $input['klant_id'])->first();
            // dd($nieuw);
            $temp = new Status_has_appointment;
            $temp->status_id = 1;
            $temp->afspraak_id = $nieuw->id;
            $temp->gebruiker_id = Auth::user()->id;
            $temp->omschrijving = Status::find(1)->omschrijving." door ".Auth::user()->name;
            $temp->save();

            $actperafs = Appointment_has_activities::where('afspraak_id', $oud->id)->get();
            $nieuw->totaalbedrag = 0.00;
            foreach($actperafs as $apa)
            {
                $tmp = new Appointment_has_activities;
                $tmp->activiteit_id = $apa->activiteit_id;
                $tmp->afspraak_id = $nieuw->id;
                $tmp->aantal = $apa->aantal;
                $tmp->kortingsbedrag = $apa->kortingsbedrag;
                $tmp->kortingsperc = $apa->kortingsperc;
                $tmp->prijs = $apa->prijs;
                $nieuw->totaalbedrag += (($tmp->prijs * $tmp->aantal) - $tmp->kortingsbedrag);
                $tmp->save();
            }

            $geschiedenis = Afspraak_has_geschiedenis::where('afspraak_id', $oud->id)->get();
            foreach ($geschiedenis as $g)
            {
                $tmp = new Afspraak_has_geschiedenis;
                $tmp->afspraak_id = $nieuw->id;
                $tmp->geschiedenis_id = $g->geschiedenis_id;
                $tmp->datum = $g->datum;
                $tmp->save();
            }

            $log = new Log;
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $nieuw->klant_id;
            $log->omschrijving = Auth::user()->name." heeft een nieuwe afspraak toegevoegd.";
            $log->type = "afspraak";
            $log->save();
            if($oud->klusprijs ==1)
                $nieuw->totaalbedrag = $oud->totaalprijs;
            $nieuw->save();

            return redirect('/relatie/'.$oud->klant_id)->with('succes', 'Afspraak succesvol toegevoegd.');
        }
            $afspraak->save();
            $nieuw = $afspraak;
            // $nieuw = Afspraak::where('startdatum',date('Y-m-d H:i:s', $startstamp))->where('klant_id', $input['klant_id'])->first();
            // dd($nieuw);
            $temp = new Status_has_appointment;
            $temp->status_id = 1;
            $temp->afspraak_id = $nieuw->id;
            $temp->gebruiker_id = Auth::user()->id;
            $temp->omschrijving = Status::find(1)->omschrijving." door ".Auth::user()->name;
            $temp->save();

            $actperafs = Appointment_has_activities::where('afspraak_id', $oud->id)->get();
            foreach($actperafs as $apa)
            {
                $tmp = new Appointment_has_activities;
                $tmp->activiteit_id = $apa->activiteit_id;
                $tmp->afspraak_id = $nieuw->id;
                $tmp->aantal = $apa->aantal;
                $tmp->kortingsbedrag = $apa->kortingsbedrag;
                $tmp->kortingsperc = $apa->kortingsperc;
                $tmp->prijs = $apa->prijs;
                $tmp->save();
            }

            $geschiedenis = Afspraak_has_geschiedenis::where('afspraak_id', $oud->id)->get();
            foreach ($geschiedenis as $g)
            {
                $tmp = new Afspraak_has_geschiedenis;
                $tmp->afspraak_id = $nieuw->id;
                $tmp->geschiedenis_id = $g->geschiedenis_id;
                $tmp->datum = $g->datum;
                $tmp->save();
            }

            $log = new Log;
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $nieuw->klant_id;
            $log->omschrijving = Auth::user()->name." heeft een nieuwe afspraak toegevoegd.";
            $log->type = "afspraak";
            $log->save();

            return redirect('/relatie/'.$oud->klant_id)->with('succes', 'Afspraak succesvol toegevoegd.');

    }

    /*
    *   Store warranty appointment
    */
    public function setWarranty($id)
    {
        $input = Request::all();
        //dd($input);
        $garantieafspraak = Appointment::find($id);
        $relatie = Client::find($garantieafspraak->klant_id);
        for($i = 0; $i < count($input['afspraken']);$i++)
        {
            $tempafspraak = Appointment_has_activities::where('afspraak_id', $input['afspraken'][$i])->get();
            foreach ($tempafspraak as $tempact)
            {
                $aha = new Appointment_has_activities;
                $aha->activiteit_id = $tempact->activiteit_id;
                $aha->afspraak_id = $id;
                $aha->aantal = $tempact->aantal;
                $aha->prijs = 0;

                $aha->kortingsbedrag = 0;
                $aha->kortingsperc = 0;
                $aha->opmerking = $tempact->opmerking;
                $aha->save();
            }
            $garantieafspraak->afspraak_id = $input['afspraken'][$i];
        }
        $garantieafspraak->totaalbedrag = 0;
        $garantieafspraak->save();

        return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de garantie afspraak succesvol aangemaakt.');
    }

    /*
    *   Method for clicking warranty per appointment
    */
    public function idWarrenty($id)
    {
        $afspraak = Appointment::find($id);
        $relatie = Client::findOrFail($afspraak->klant_id);
        $types = AppointmentType::where('omschrijving', 'Garantie')->get();
        $werknemers = Employee::all();

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.setWarrenty', compact('relatie','types','werknemers', 'afspraak', 'page', 'sub'));
    }

    /*
    *   Post method for specific appointment
    */
    public function setidWarrenty($id)
    {
        $input = Request::all();
        if($input['startdatum'] >= $input['einddatum'])
        {
            return redirect('/afspraak/maken/'.$input['klant_id'])->with('danger', 'Startdatum moet voor de einddatum liggen.');
        }
        else
        {
            $oud = Appointment::find($id);
            $relatie = Client::find($oud->klant_id);
            $afspraak = new Appointment;
            if($input['radio_demo'] < 2 )
            {
                $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
                $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
                $startstamp = $strtotime->getTimestamp();
                $endstamp = $strtotimee->getTimestamp();
                if($input['radio_demo'] == 1)
                {
                    $afspraak->startdatum = date('Y-m-d', $startstamp)." 12:30";
                    $afspraak->einddatum = date('Y-m-d', $startstamp)." 17:00";
                }
                else
                {
                    $afspraak->startdatum = date('Y-m-d', $startstamp)." 08:00";
                    $afspraak->einddatum = date('Y-m-d', $startstamp)." 12:30";
                }
            }
            else
            {
                $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
                $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
                $startstamp = $strtotime->getTimestamp();
                $endstamp = $strtotimee->getTimestamp();
                $afspraak->startdatum = date('Y-m-d H:i:s', $startstamp);
                $afspraak->einddatum = date('Y-m-d H:i:s', $endstamp);
            }
            $afspraak->klant_id = $relatie->id;
            $afspraak->geschreven_door = Auth::user()->id;
            $afspraak->verwerkt_door = Auth::user()->id;
            $afspraak->afspraaktype_id = $input['afspraaktype_id'];
            $afspraak->omschrijving = $input['omschrijving'];
            $afspraak->totaalbedrag = 0;
            $afspraak->afspraak_id = $oud->id;
            $afspraak->save();
            $nieuw = Appointment::where('startdatum',date('Y-m-d H:i:s', $startstamp))->where('klant_id', $relatie->id)->first();

            //status
            $sha = new Status_has_appointment;
            $sha->status_id = 1;
            $sha->afspraak_id = $afspraak->id;
            $sha->gebruiker_id = Auth::user()->id;
            $sha->omschrijving = Auth::user()->name." heeft deze garantie Toegevoegd.";
            $sha->save();

            //log
            $log = new Log;
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $relatie->id;
            $log->omschrijving = Auth::user()->name." heeft een garantie van klant, ".$relatie->postcode." - ".$relatie->huisnummer." aangemaakt voor afspraak: ".$oud->id;
            $log->type = "Afspraak";
            $log->save();

            //geschiedenis
            $regel = new History;
            $regel->datum = date('Y-m-d');
            $regel->klant_id = $afspraak->klant_id;
            $regel->afspraak_id = $afspraak->id;
            $user = User::findOrFail(Auth::user()->id);
            $regel->gebruiker_id = $user->id;
            $regel->omschrijving = $user->name." heeft een garantie gemaakt voor afspraak: ".$oud->id;
            $regel->save();


            $tempafspraak = Appointment_has_activities::where('afspraak_id', $oud->id)->get();
            foreach ($tempafspraak as $tempact)
            {
                $aha = new Appointment_has_activities;
                $aha->activiteit_id = $tempact->activiteit_id;
                $aha->afspraak_id = $afspraak->id;
                $aha->aantal = $tempact->aantal;
                $aha->prijs = 0;
                $aha->kortingsbedrag = 0;
                $aha->kortingsperc = 0;
                $aha->opmerking = $tempact->opmerking;
                $aha->save();
            }
            $afspraak->afspraak_id = $oud->id;

            $afspraak->totaalbedrag = 0;
            $afspraak->save();

            return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de garantie afspraak succesvol aangemaakt.');
        }
    }
    /*
    *   Warranty
    *   Appointment completed, problem not solved :(
    *   input: retrieves if new appointment is needed or if customers gets refund
    *   output: view for appointment or view for refund request.
    */
    public function endWarranty($id)
    {
        $input = Request::all();

        $afspraak = Appointment::find($id);
        $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
        $sha->status_id = 3;
        $sha->afspraak_id = $afspraak->id;
        $sha->gebruiker_id = Auth::user()->id;
        $sha->omschrijving = "Afspraak is gelukt, maar niet opgelost.";
        $sha->save();
        $relatie = Client::find($afspraak->relatie_id);

        $app = $input['app'];
        $credit = $input['credit'];

        if($app == 0 && $credit == 0)
        {
            return redirect('relatie/'.$relatie->id)->with('succes', 'U heeft de garantie verwerkt.');
        }
        elseif($app == 1 && $credit == 1)
        {

            $afspraak = Appointment::findOrFail($id);
            $relatie = Client::findOrFail($afspraak->klant_id);
            $types = AppointmentType::where('omschrijving', 'Terugbetaalverzoek')->first();
            $temp = "Terugbetaalverzoek";

            $page = "relatie";
            $sub = "relover";
            return view('afspraak.setAfspraak', compact('afspraak', 'relatie', 'types', 'temp', 'page', 'sub'));
        }
        else
        {
            if($app == 1 && $credit == 0)
            {
                $afspraak = Appointment::findOrFail($id);
                $relatie = Client::findOrFail($afspraak->klant_id);
                $types = AppointmentType::all();


                $page = "relatie";
                $sub = "relover";
                return view('afspraak.setAfspraak', compact('afspraak', 'relatie', 'types', 'page', 'sub'));
            }
            elseif($app == 0 && $credit == 1)
            {
                $afspraak = Appointment::findOrFail($id);
                $relatie = Client::findOrFail($afspraak->klant_id);

                $log = new Log;
                $log->omschrijving = Auth::user()->name." heeft een terugbetaalverzoek toegevoegd.";
                $log->gebruiker_id = Auth::user()->id;
                $log->klant_id = $relatie->id;
                $log->type = "Crediteur";
                $log->save();

                //geschiedenis
                $regel = new History;
                $regel->datum = date('Y-m-d');
                $regel->klant_id = $afspraak->klant->id;
                $regel->afspraak_id = $afspraak->id;
                $user = User::findOrFail(Auth::user()->id);
                $regel->gebruiker_id = $user->id;
                $regel->omschrijving = $user->name." heeft deze klant toegevoegd aan de terugbetaalverzoeken.";
                $regel->save();

                $page = "relatie";
                $sub = "relover";

                return view('crediteur.garantiecrediteur', compact('relatie', 'afspraak', 'page', 'sub'));
            }
        }
    }

    /*
    *   Warranty
    *   input: request for cash repaymment
    *   output: new appointment
    */
    public function setCashRepay($id)
    {
        $input = Request::all();
        $old = Appointment::find($id);
        $relatie = Client::find($old->klant_id);

        $afspraak = new Appointment;

        if($input['radio_demo'] < 2 )
        {
            $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
            $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
            $startstamp = $strtotime->getTimestamp();
            $endstamp = $strtotimee->getTimestamp();
            if($input['radio_demo'] == 1)
            {
                $afspraak->startdatum = date('Y-m-d', $startstamp)." 12:30";
                $afspraak->einddatum = date('Y-m-d', $startstamp)." 17:00";
            }
            else
            {
                $afspraak->startdatum = date('Y-m-d', $startstamp)." 08:00";
                $afspraak->einddatum = date('Y-m-d', $startstamp)." 12:30";
            }
        }
        else
        {
            $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
            $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
            $startstamp = $strtotime->getTimestamp();
            $endstamp = $strtotimee->getTimestamp();
            $afspraak->startdatum = date('Y-m-d H:i:s', $startstamp);
            $afspraak->einddatum = date('Y-m-d H:i:s', $endstamp);
        }
        $afspraak->klant_id = $input['klant_id'];
        //$afspraak->geschreven_door = $input['geschreven_door'];
        $afspraak->verwerkt_door = $input['verwerkt_door'];
        $afspraak->afspraaktype_id = $input['afspraaktype_id'];
        $afspraak->omschrijving = $input['omschrijving'];
        $afspraak->totaalbedrag = -$input['totaalbedrag'];
        $afspraak->afspraak_id = $old->id;

        $crediteur = new Crediteur;
        $crediteur->klant_id = $relatie->id;
        $crediteur->betaaldatum = date('Y-m-d', strtotime("+14 days"));
        $crediteur->users_id = Auth::user()->id;;
        $crediteur->bedrag = $input['totaalbedrag'];
        $crediteur->omschrijving = "Naar aanleiding van de ingeplande garantie afspraak van ".date('d-m-Y',$afspraak->startdatum).", is dit als crediteur toegevoegd.";
        $crediteur->save();

        $afspraak->save();

        $afspr = Appointment::where('startdatum', date('Y-m-d H:i:s', $startstamp))
                            ->where('klant_id', $input['klant_id'])
                            ->where('einddatum', date('Y-m-d H:i:s', $endstamp))->first();



        $sha = new Status_has_appointment;
        $sha->status_id = 1;
        $sha->afspraak_id = $afspr->id;
        $sha->gebruiker_id = Auth::user()->id;
        $sha->omschrijving = Auth::user()->name." heeft deze terugbetaalverzoek Toegevoegd.";
        $sha->save();

        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft een terugbetaalverzoek op: ".$afspr->startdatum." -- ".$afspr->einddatum.", toegevoegd.";
        $log->gebruiker_id = Auth::user()->id;
        $log->klant_id = $input['klant_id'];
            $log->type = "Crediteur";
        $log->save();

        //geschiedenis
        $regel = new History;
        $regel->datum = date('Y-m-d');
        $regel->klant_id = $afspr->klant->id;
        $regel->afspraak_id = $afspr->id;
        $user = User::findOrFail(Auth::user()->id);
        $regel->gebruiker_id = $user->id;
        $regel->omschrijving = $user->name." heeft een terugbetaalverzoek gemaakt met klant op ".$afspraak->returnDate($afspraak->startdatum)." --- ".$afspraak->returnDate($afspraak->einddatum);
        $regel->save();

        return redirect('relatie/'.$relatie->id)->with('succes', 'U heeft succesvol een terugbetaalverzoek aangemaakt.');
    }

    /*
    *   setDefPayment
    *   input: new appointment with selected appointment on which this deffered payment belongs.
    *   output: new appointment
    */
    public function setDefPayment($id)
    {
        $input = Request::all();
        if($input['startdatum'] >= $input['einddatum'])
        {
            return redirect('/afspraak/'.$id.'/verwerken/nietgelukt/nieuweafspraak')->with('danger', 'Startdatum moet voor de einddatum liggen.');
        }
        else
        {
            $input = Request::all();
            $oud = Appointment::find($id);
            $relatie = Client::find($oud->klant_id);
            $afspraak = new Appointment;
            if($input['radio_demo'] < 2 )
            {
                $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
                $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
                $startstamp = $strtotime->getTimestamp();
                $endstamp = $strtotimee->getTimestamp();
                if($input['radio_demo'] == 1)
                {
                    $afspraak->startdatum = date('Y-m-d', $startstamp)." 12:30";
                    $afspraak->einddatum = date('Y-m-d', $startstamp)." 17:00";
                }
                else
                {
                    $afspraak->startdatum = date('Y-m-d', $startstamp)." 08:00";
                    $afspraak->einddatum = date('Y-m-d', $startstamp)." 12:30";
                }
            }
            else
            {
                $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
                $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
                $startstamp = $strtotime->getTimestamp();
                $endstamp = $strtotimee->getTimestamp();
                $afspraak->startdatum = date('Y-m-d H:i:s', $startstamp);
                $afspraak->einddatum = date('Y-m-d H:i:s', $endstamp);
            }
            $afspraak->klant_id = $input['klant_id'];
            $afspraak->geschreven_door = Auth::user()->id;
            $afspraak->verwerkt_door = Auth::user()->id;
            $afspraak->afspraaktype_id = $input['afspraaktype_id'];
            $afspraak->omschrijving = $input['omschrijving'];
            $afspraak->totaalbedrag = $input['totaalbedrag'];
            $afspraak->afspraak_id = $oud->id;
            $afspraak->save();
            $nieuw = Appointment::where('startdatum',date('Y-m-d H:i:s', $startstamp))->where('klant_id', $input['klant_id'])->first();

            //status
            $sha = new Status_has_appointment;
            $sha->status_id = 1;
            $sha->afspraak_id = $nieuw->id;
            $sha->gebruiker_id = Auth::user()->id;
            $sha->omschrijving = Auth::user()->name." heeft deze afspraak Toegevoegd.";
            $sha->save();

            //log
            $log = new Log;
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $relatie->id;
            $log->omschrijving = Auth::user()->name." heeft een uitgestelde betaling van klant, ".$relatie->postcode." - ".$relatie->huisnummer." aangemaakt voor afspraak: ".$nieuw->id;
            $log->type = "Afspraak";
            $log->save();

            //geschiedenis
            $regel = new History;
            $regel->datum = date('Y-m-d');
            $regel->klant_id = $nieuw->klant_id;
            $regel->afspraak_id = $nieuw->id;
            $user = User::findOrFail(Auth::user()->id);
            $regel->gebruiker_id = $user->id;
            $regel->omschrijving = $user->name." heeft een uitgestelde betaling gemaakt van € ".$nieuw->totaalbedrag." voor afspraak: ".$nieuw->id;
            $regel->save();

            $message = ($nieuw->afspraaktype_id == 3) ? "uitgestelde betaling" : ($nieuw->afspraaktype_id == 5) ? "Betalingsherinnering" : "afspraak";
            return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft succesvol een uitgestelde betaling aangemaakt.');
        }
    }

    public function cancelApp($id)
    {
        $afspraak = Appointment::find($id);

        $log = new Log;
        $log->gebruiker_id = Auth::user()->id;
        $log->klant_id = $afspraak->klant_id;
        $klant = Client::find($afspraak->klant_id);
        $log->omschrijving = Auth::user()->name." heeft de afspraak ".$afspraak->id." van ".$klant->achternaam.", ".$klant->postcode." ".$klant->huisnummer.", "."geannuleerd.";
        $log->type = "Afspraak";
        $log->save();

        $geschiedenis = new History;
        $geschiedenis->afspraak_id = $afspraak->id;
        $geschiedenis->gebruiker_id = Auth::user()->id;
        $geschiedenis->klant_id = $afspraak->klant_id;
        $geschiedenis->omschrijving = Auth::user()->name." heeft afspraak ".$afspraak->id." van ".$klant->achternaam.", ".$klant->postcode." ".$klant->huisnummer.", "."geannuleerd.";
        $geschiedenis->datum = date('Y-m-d');
        $geschiedenis->save();

        $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
        $sha->status_id = 23;
        $sha->gebruiker_id = Auth::user()->id;
        $sha->omschrijving = Auth::user()->name." heeft de afspraak geannuleerd.";
        $sha->save();

        $afspraak->eigenschuld =1;
        $afspraak->save();

        return redirect('/relatie/'.$klant->id)->with('succes', 'U heeft de afspraak succesvol geannuleerd.');
    }

    public function editAppointment($id)
    {
        $afspraak = Appointment::find($id);
        $afspraak->afspraaktype_id = AppointmentType::find($afspraak->afspraaktype_id);
        $afspraak->klant_id = Client::find($afspraak->klant_id);
        $user = User::find(Auth::user()->id);
        $werknemers = Employee::where('actief', 1)->get();
        $types = AppointmentType::where('omschrijving', '!=', 'Uitgestelde Betaling')->where('omschrijving', '!=', 'Betalingsherinnering')->get();

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.edit', compact('afspraak', 'user', 'werknemers', 'types', 'page', 'sub'));
    }

    public function storeAppointment($id)
    {
        $input = Request::all();
        $afspraak = Appointment::find($id);
        if($input['radio_demo'] < 2 )
        {
            $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
            $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
            $startstamp = $strtotime->getTimestamp();
            $endstamp = $strtotimee->getTimestamp();
            if($input['radio_demo'] == 1)
            {
                $afspraak->startdatum = date('Y-m-d', $startstamp)." 12:30";
                $afspraak->einddatum = date('Y-m-d', $startstamp)." 17:00";
            }
            else
            {
                    $afspraak->startdatum = date('Y-m-d', $startstamp)." 08:00";
                    $afspraak->einddatum = date('Y-m-d', $startstamp)." 12:30";
            }
        }
        else
        {
            $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
            $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
            $startstamp = $strtotime->getTimestamp();
            $endstamp = $strtotimee->getTimestamp();
            $afspraak->startdatum = date('Y-m-d H:i:s', $startstamp);
            $afspraak->einddatum = date('Y-m-d H:i:s', $endstamp);
        }
        $input = (object) $input;
        $afspraak->afspraaktype_id = $input->afspraaktype_id;

        // (!isset($input->bevestiging)) ? $afspraak->bevestiging = 1: $afspraak->bevestiging = 0;
        $afspraak->bijgewerkt_door = $input->verwerkt_door;
        $afspraak->geschreven_door = $input->geschreven_door;
        $afspraak->omschrijving = $input->omschrijving;
        $afspraak->save();

        if(isset($input->geschiedenis))
        {
            $geschiedenis = true;
        }
        else
        {
            $geschiedenis = false;
        }

        if(isset($input->werkzaamheden) && $input->afspraaktype_id == 1)
        {
            $afspraak = Appointment::find($id);
            $relatie = Client::find($afspraak->klant_id);
            $activiteiten = Work::where('actief', 1)->get();
            $danger = "";

            $page = "relatie";
            $sub = "relover";
            return view('afspraak.editWork', compact('afspraak', 'relatie', 'activiteiten', 'danger', 'geschiedenis', 'page', 'sub'));
        }
        elseif(isset($input->geschiedenis) && $input->afspraaktype_id == 1)
        {
            $afspraak = Appointment::find($id);
            $relatie = Client::find($afspraak->klant_id);

            if(History::where('klant_id', $relatie->id)->where('type', 'werkzaamheden')->where('afspraak_id', '!=', $afspraak->id)->count() > 0)
            {
                $geschiedenis = History::where('klant_id', $relatie->id)->where('afspraak_id', '!=', $afspraak->id)->get();

                $page = "relatie";
                $sub = "relover";
                return view('afspraak.addHistory', compact('relatie', 'afspraak', 'geschiedenis', 'page', 'sub'));
            }
        }
        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol gewijzigd.');
    }

    public function storeWork($id)
    {
        $input = Request::all();
        // dd($input);
        $double = false;
        for($k=0;$k<count($input['activiteiten']);$k++)
        {
            for($l=0;$l<count($input['activiteiten']);$l++)
            {
                if($input['activiteiten'][$l]==$input['activiteiten'][$k] && $k != $l)
                    $double=true;
            }
        }
        if(!$double)
        {
            $i = 1; // algoritme om aantal te vinden
            $rij = array();
            foreach($input['aantal'] as $r)
            {
                // if($i != count($input['aantal']))
                // {
                //     if(isset($r["s_".$i.":i_0"]))
                //         array_push($rij, $r["s_".$i.":i_0"]);
                // }
                // else
                // {
                //     $i--;
                //     array_push($rij, $input['aantal'][$i]);
                // }
                // $i++;


            }

            //rij geeft nu in de juiste volgorde de hoeveelheden aan.

            //totaalbedrag uitrekenen
            $afspraak = Appointment::findOrFail($id);
            $relatie = Client::findOrFail($afspraak->klant_id);
            $totaalbedrag = 0;
            $ahs = Appointment_has_activities::where('afspraak_id', $afspraak->id)->get();

            if(isset($input['klusprijsAan']) && $afspraak->afspraaktype_id != 4 && $afspraak->afspraaktype_id != 3)
            {
                $afspraak->klusprijs = 1;
                $klusprijs = $input['klusprijs'];
                $totaalbedrag = $klusprijs;
            }

            for($i = 0; $i < count($input['activiteiten']);$i++)
            {
                foreach($ahs as $aha)
                {
                    $act = Work::findOrFail($input['activiteiten'][$i]);
                    if($act->id != $aha->activiteit_id)
                    {
                        $aha->activiteit_id = $act->id;
                        $aha->afspraak_id = $afspraak->id;
                        $aha->prijs = ($input['prijs'][$i] > 0 ? $input['prijs'][$i] : $act->prijs);;
                        $aha->kortingsbedrag = $input['korting'][$i];
                        $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;
                    }

                    $aha->aantal = $input['aantal'][$i];
                    if(isset($klusprijs))
                    {
                        if($klusprijs > 0)
                        {
                            $aha->prijs = ($input['prijs'][$i] > 0 ? $input['prijs'][$i] : $act->prijs);
                            $bedrag = $aha->prijs * $aha->aantal;
                            if($klusprijs - $bedrag <= 0)
                            {
                                $aha->kortingsbedrag = $bedrag - $klusprijs;
                                $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;
                                $klusprijs = $klusprijs - $bedrag;

                            }
                            elseif($klusprijs - $bedrag > 0)
                            {
                                $aha->kortingsbedrag = $input['korting'][$i];
                                $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;
                                $klusprijs = $klusprijs - $bedrag;
                            }
                        }
                    }
                    else
                    {
                        $aha->prijs = ($input['prijs'][$i] > 0 ? $input['prijs'][$i] : $act->prijs);
                        $aha->kortingsbedrag = $input['korting'][$i];
                        $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;
                        $bedrag = $aha->prijs * $input['aantal'][$i];
                        $totaalbedrag = $totaalbedrag + $bedrag;
                    }
                    $aha->opmerking = $input['opmerking'][$i];
                    $aha->save();

                }
            }

            $afspraak->totaalbedrag = $totaalbedrag;

            //logbeheer


            //status

            $afspraak->save();

            if($input['geschiedenis'])
            {
                $geschiedenis = $input['geschiedenis'];
                $afspraak = Appointment::find($id);
                $relatie = Client::find($afspraak->klant_id);

                if(History::where('klant_id', $relatie->id)->where('type', 'werkzaamheden')->where('afspraak_id', '!=', $afspraak->id)->count() > 0)
                {
                    $geschiedenis = History::where('klant_id', $relatie->id)->where('afspraak_id', '!=', $afspraak->id)->get();
                    $update = true;

                    $page = "relatie";
                    $sub = "relover";
                    return view('afspraak.addHistory', compact('relatie', 'afspraak', 'geschiedenis', 'update', 'page', 'sub'));
                }
            }

            return redirect('/relatie/'.$relatie->id)->with('succes', 'De afspraak is gewijzigd.');
        }
        else
        {
            $afspraak = Appointment::find($id);
            $relatie = Client::find($afspraak->klant_id);
            $activiteiten = Work::where('actief', 1)->get();
            $danger = 'U kunt niet meerdere malen dezelfde werkzaamheden opgeven.';

            $page = "relatie";
            $sub = "relover";
            return view('afspraak.editWork', compact('afspraak', 'relatie', 'activiteiten', 'danger', 'page', 'sub'));
        }
    }

    public function saveHistoryToAppointment($id)
    {
        $relatie = Client::find($id);
        $input = Request::all();
        $afspraak = Appointment::find($input['afspraak_id']);
        $activiteiten = Appointment_has_activities::where('afspraak_id', $afspraak->id)->get();

        if(!isset($input['update']))
        {
            if(isset($input['geschiedenis']))
            {
                for($i = 0; $i < count($input['geschiedenis']); $i++)
                {
                    $ahg = new Afspraak_has_geschiedenis;
                    $ahg->afspraak_id = $afspraak->id;
                    $ahg->geschiedenis_id = $input['geschiedenis'][$i];
                    $ahg->datum = date('Y-m-d', strtotime($afspraak->startdatum));
                    $ahg->save();
                }
            }
        }
        else
        {
            if(isset($input['geschiedenis']))
            {
                $ghas = Afspraak_has_geschiedenis::where('afspraak_id', $afspraak->id)->get();
                foreach($ghas as $gha)
                {
                    $gha->delete();
                }

                for($i = 0; $i < count($input['geschiedenis']); $i++)
                {
                    $ahg = new Afspraak_has_geschiedenis;
                    $ahg->afspraak_id = $afspraak->id;
                    $ahg->geschiedenis_id = $input['geschiedenis'][$i];
                    $ahg->save();
                }
            }
            else
            {
                $ghas = Afspraak_has_geschiedenis::where('afspraak_id', $afspraak->id)->get();
                foreach($ghas as $gha)
                {
                    $gha->delete();
                }
            }
        }

        return redirect('/relatie/'.$id)->with('succes', 'De afspraak is toegevoegd aan de relatie.');
    }

    public function addNewWork($id)
    {
        $cont = "new";
        $afspraak = Appointment::find($id);

        $page = "relatie";
        $sub = "relover";
        return view('activiteit.toevoegen', compact('page', 'afspraak', 'cont', 'sub'));
    }

    public function addNewWorkToExisting($id)
    {
        $cont = "edit";
        $afspraak = Appointment::find($id);

        $page = "relatie";
        $sub = "relover";
        return view('activiteit.toevoegen', compact('page', 'afspraak', 'cont', 'sub'));
    }

    public function saveWorkToExisting($id)
    {

        $input = Request::all();

        for($i = 0;$i < count($input['prijs']);$i++)
        {
            $temp = new Work;
            $temp->omschrijving = $input['omschrijving'][$i];
            $temp->prijs = $input['prijs'][$i];
            $temp->eenheid = $input['eenheid'][$i];
            $temp->actief = 1;
            $temp->save();

            $log = new Log;
            $log->omschrijving = Auth::user()->name." heeft de werkactiviteit: ".$input['omschrijving'][$i].", toegevoegd.";
            $log->gebruiker_id = Auth::user()->id;
                $log->type = "Activiteit";
            $log->save();
        }


        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);
        $activiteiten = Work::where('actief', 1)->get();
        $danger = "";

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.editWork', compact('afspraak', 'relatie', 'activiteiten', 'danger', 'geschiedenis', 'page', 'sub'));
    }

    public function saveWork($id)
    {

        $input = Request::all();

        for($i = 0;$i < count($input['prijs']);$i++)
        {
            $temp = new Work;
            $temp->omschrijving = $input['omschrijving'][$i];
            $temp->prijs = $input['prijs'][$i];
            $temp->eenheid = $input['eenheid'][$i];
            $temp->actief = 1;
            $temp->save();

            $log = new Log;
            $log->omschrijving = Auth::user()->name." heeft de werkactiviteit: ".$input['omschrijving'][$i].", toegevoegd.";
            $log->gebruiker_id = Auth::user()->id;
                $log->type = "Activiteit";
            $log->save();
        }


        $afspraak = Appointment::find($id);
        $relatie = Client::findOrFail($afspraak->klant_id);
        $activiteiten = Work::where('actief', 1)->get();
        $danger = "";

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.werkzaamheden', compact('relatie', 'afspraak', 'activiteiten','danger', 'page', 'sub'));
    }

    public function veranderEigenschuld($afspraak_id)
    {
        $afspraak = Appointment::find($afspraak_id);

        $afspraak->eigenschuld = ($afspraak->eigenschuld == 1) ? 0 : 1;
        $afspraak->save();

        $log = new Log;
        $log->gebruiker_id = Auth::user()->id;
        $log->klant_id = $afspraak->klant_id;
        $log->omschrijving = Auth::user()->name." heeft afspraak ".$afspraak->id." gemarkeerd als 'geen €15,- klant'.";
        $log->type = "€15,- klant";
        $log->save();

        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Status succesvol geüpdatet!');

    }

    # Vegers gedeelte

    public function start($werklijst_id, $afspraak_id)
    {
        $afspraak = Appointment::find($afspraak_id);
        $relatie = Client::find($afspraak->klant_id);
        $wha = Worklist_has_appointments::where('werklijst_id', $werklijst_id)->where('afspraak_id', $afspraak_id)->first();
        if($wha->start != null)
        {
            $wha->start = date('Y-m-d H:i');
            $wha->save();
        }

        $page = "planning";
        $sub = "verkvand";

        return view('afspraak.verwerken1', compact('afspraak', 'relatie', 'page', 'sub'));
    }

    #end Vegers gedeelte

    public function notNeeded($id)
    {
        $afspraak = Appointment::find($id);
        $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
        $sha->status_id = 30;
        $sha->save();

        return redirect('/terugbellijst')->with('succes', 'Wijziging opgeslagen, relatie uit terugbellijst gehaald.');
    }

    public function notNeededProfile($id)
    {
        $afspraak = Appointment::find($id);
        $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
        $sha->status_id = 30;
        $sha->save();

        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Wijziging opgeslagen en verwerkt.');
    }

    public function setOfferApp($id)
    {
        $input = Request::all();

        $afspraak = new Appointment;
        $afspraak->afspraak_id = $id;

        if($input['radio_demo'] < 2 )
        {
            $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
            $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
            $startstamp = $strtotime->getTimestamp();
            $endstamp = $strtotimee->getTimestamp();
            if($input['radio_demo'] == 1)
            {
                $afspraak->startdatum = date('Y-m-d', $startstamp)." 12:30";
                $afspraak->einddatum = date('Y-m-d', $startstamp)." 17:00";
            }
            else
            {
                $afspraak->startdatum = date('Y-m-d', $startstamp)." 08:00";
                $afspraak->einddatum = date('Y-m-d', $startstamp)." 12:30";
            }
        }
        else
        {
            $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
            $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
            $startstamp = $strtotime->getTimestamp();
            $endstamp = $strtotimee->getTimestamp();
            $afspraak->startdatum = date('Y-m-d H:i:s', $startstamp);
            $afspraak->einddatum = date('Y-m-d H:i:s', $endstamp);
        }

        $afspraak->afspraaktype_id = $input['afspraaktype_id'];
        $afspraak->geschreven_door = $input['geschreven_door'];
        $afspraak->verwerkt_door = $input['verwerkt_door'];
        $afspraak->klant_id = $input['klant_id'];
        $afspraak->omschrijving = $input['omschrijving'];
        $afspraak->save();

        $afspraak->setLog(Auth::user()->name." heeft een afspraak op: ".$afspraak->startdatum." -- ".$afspraak->einddatum.", toegevoegd.");
        $afspraak->setStatus(1, Auth::user()->name." heeft deze afspraak toegevoegd.");
        $afspraak->setHistory(
                        Auth::user()->name." heeft een afspraak gemaakt met klant op ".$afspraak->returnDate($afspraak->startdatum)." --- ".$afspraak->returnDate($afspraak->einddatum)
                        );

        if(isset($input['notNull'])) // Als er een offerte is, kan er hier automatisch de werkzaamheden toegevoegd worden.
        {
            $offerte = Offerte::find($id);
            $ohas = Activiteit_has_offerte::where('offerte_id', $offerte->id)->get();
            $amount = 0;
            for($i = 0; $i < count($input['activiteiten']); $i++)
            {
                foreach($ohas as $oha)
                {
                    if($input['activiteiten'][$i] == $oha->activiteit_id)
                    {
                        $temp = new Appointment_has_activities;
                        $temp->activiteit_id = $input['activiteiten'][$i];
                        $temp->afspraak_id = $afspraak->id;
                        $temp->aantal = $oha->aantal;
                        $temp->prijs = $oha->prijs;
                        $amount += $oha->prijs;
                        $temp->save();
                    }
                }
            }
            $afspraak->totaalbedrag = $amount;
            $afspraak->save();

            return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
        }

        return redirect('/afspraak/maken/'.$input['klant_id'].'/'.$afspraak->id.'/werkzaamheden');
    }

}
