<?php

namespace App\Http\Controllers;

use Request;
use App\Models\User;
use App\Models\Ticket;
use App\Models\Ticketregel;
use App\Models\Bedrijf;
use Auth;
use Mail;

class TicketController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tickets = Ticket::all();
        $page = "tickets";
        $sub = "tickover";
        return view('ticket.index', compact('tickets', 'page', 'sub'));
    }

    public function create()
    {
        $page = "tickets";
        $sub = "ticktoev";
        return view('ticket.nieuw', compact('page', 'sub'));
    }

    public function store()
    {
        $input = Request::all();
        $input = (object) $input;
        $t = new Ticket;
        $t->onderwerp = $input->onderwerp;
        $t->prioriteit = $input->prioriteit;
        $t->waar = $input->waar;
        $t->toegevoegd_door = Auth::user()->id;
        $t->status_id = 31;
        $t->bijgewerkt_door = 0;
        $t->save();

        $t = Ticket::where('onderwerp', $input->onderwerp)->where('waar', $input->waar)->first();

        $tr = new Ticketregel;
        $tr->geplaatst_door = $t->toegevoegd_door;
        $tr->ticket_id = $t->id;
        $tr->bericht = $input->bericht;
        $tr->save();

        $mailObject = (object) $input;

        $text = str_replace("\r\n.", "\n..", $input->bericht);
        $bedrijf = Bedrijf::where('id', 1)->first();
        $data = array('bedrijf' => $bedrijf->naam, 'email' => $bedrijf->email, 'mobnr' => $bedrijf->tel1 ,'bericht' => $input->bericht, 'type' => "Er is een Ticket aangemaakt.", 'prio' => $input->prioriteit);
        $user = User::where('rol', 1)->first();
        if ($user) {
            $data = array('bedrijf' => $bedrijf->naam, 'email' => $bedrijf->email, 'mobnr' => $bedrijf->tel1 ,'bericht' => $input->bericht, 'type' => "Er is een Ticket aangemaakt.", 'prio' => $input->prioriteit);
            Mail::send('mail.ticket', $data, function($message) use ($mailObject, $input, $user, $bedrijf) {
                $message->to('flexavi@itsucceeds.nl', $user->name)->subject(strtoupper($mailObject->prioriteit).' - '.$mailObject->onderwerp);
                $message->from($bedrijf->email, $bedrijf->naam);
            });
        } else {
            // Handle the case where $user is null, for example, by logging an error or redirecting with a message.
            return redirect('/ticket')->with('error', 'Error creating ticket: User not found.');
        }

        return redirect('/ticket')->with('success', 'Ticket aangemaakt en doorgestuurd naar de administrator');
    }

    public function delete($id){
        Ticket::find($id)->delete();
        return redirect('/ticket')->with('succes', 'Ticket verwijderd');
    }

    
}