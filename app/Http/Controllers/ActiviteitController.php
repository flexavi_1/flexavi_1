<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Work;
use App\Models\Log;
use Auth;
use App\Models\Appointment;
use App\Models\Materiaal;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class ActiviteitController extends Controller
{


protected $htmlBuilder;
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activiteiten = Work::where('actief','>=', 1)->get();

        $page = "activiteit";
        $sub = "actover";
        return view('activiteit.index', compact('activiteiten', 'page', 'sub'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $materialen = Materiaal::all();
        $page = "activiteit";
        $sub = "acttoev";
        return view('activiteit.toevoegen', compact('page', 'sub', 'materialen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        $input = Request::all();

        for($i = 0;$i < count($input['prijs']);$i++)
        {
            $temp = new Work;
            $temp->omschrijving = $input['omschrijving'][$i];
            $temp->prijs = $input['prijs'][$i];
            $temp->eenheid = $input['eenheid'][$i];
            $temp->actief = 1;
            $temp->btw = $input['btw'][$i];
            $temp->save();

            $log = new Log;
            $log->omschrijving = Auth::user()->name." heeft de werkactiviteit: ".$input['omschrijving'][$i].", toegevoegd.";
            $log->gebruiker_id = Auth::user()->id;
                $log->type = "Activiteit";
            $log->save();
        }

        return redirect('/activiteit')->with('succes', "Werkactiviteit succesvol toegevoegd.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activiteit = Work::findOrFail($id);
        $page = "activiteit";
        $sub = "actover";
        return view('activiteit.wijzigen', compact('activiteit', 'page', 'sub'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activiteit = Work::findOrFail($id);
        $input = Request::all();

        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft de werkactiviteit: ".$input['omschrijving'].", geüpdatet.";
        $log->gebruiker_id = Auth::user()->id;

            $log->type = "Activiteit";
        $log->save();

        $activiteit->fill($input)->save();

        return redirect('/activiteit')->with('succes', 'U heeft de werkactiviteit succesvol gewijzigd.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activiteit = Work::findOrFail($id);


        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft de werkactiviteit: ".$activiteit->omschrijving.", toegevoegd.";
        $log->gebruiker_id = Auth::user()->id;
            $log->type = "Activiteit";
        $log->save();

        $activiteit->actief = 0;
        $activiteit->save();

        return redirect('/activiteit')->with('succes', 'U heeft de werkactiviteit succesvol verwijderd.');
    }

    public function addNewWork($id)
    {
        // dd("test");
        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);


        $page = "activiteit";
        $sub = "acttoev";

        return view('activiteit.toevoegen', compact('afspraak', 'relatie', 'page', 'sub'));

    }
}
