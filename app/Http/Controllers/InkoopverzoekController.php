<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\History;
use App\Models\Status;
use App\Models\Log;
use App\Models\Status_has_appointment;
use App\Models\Status_has_factuur;
use App\Models\Appointment_has_activities;
use App\Models\Work;
use Auth;
use App\Models\Factuur;
use App\Models\Klant_has_status;
use DB;
use App\Models\Crediteur;
use App\Models\Offerte;
use App\Models\Offerte_has_status;
use App\Models\Leveranciersprijslijst;
use App\Models\Inkoop;
use App\Models\Materiaal;
use App\Models\Voorraad;
use App\Models\Inkoopverzoek;
use App\Models\Inkoopverzoek_has_materiaal;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class InkoopverzoekController extends Controller
{
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$verzoeken = Inkoopverzoek::all();
    	foreach($verzoeken as $verzoek)
    	{
    		$verzoek->gebruiker_id = User::find($verzoek->gebruiker_id);
    		$verzoek->status_id = Status::find($verzoek->status_id);
    	}
    	$statussen = Status::where('id', '>', '23')->get();
        $page = "voorraad";
        $sub = "verzover";

        return view('inkoop.verzoekindex', compact('statussen', 'verzoeken', 'page', 'sub'));

    }

    public function statusIndex($id)
    {
    	$verzoeken = Inkoopverzoek::where('status_id', $id)->get();
    	foreach($verzoeken as $verzoek)
    	{
    		$verzoek->gebruiker_id = User::find($verzoek->gebruiker_id);
    		$verzoek->status_id = Status::find($verzoek->status_id);
    	}
    	$statussen = Status::where('id', '>', '23')->get();
        $page = "voorraad";
        $sub = "verzover";

        return view('inkoop.verzoekindex', compact('statussen', 'verzoeken', 'page', 'sub'));
    }

    public function setRequest()
    {
        $materialen = Materiaal::all();
        $page = "voorraad";
        $sub = "inkverz";

        return view('inkoop.verzoek', compact('materialen', 'page', 'sub'));
    }

    public function saveRequest()
    {
        $input = Request::all();
        // dd($input);
        $inkoop = new Inkoopverzoek;
        $inkoop->gebruiker_id = Auth::user()->id;
        $inkoop->datum = date('Y-m-d');
        $inkoop->status_id = 24;
        $inkoop->save();

        $inkoop = Inkoopverzoek::where('gebruiker_id', Auth::user()->id)->where('datum', date('Y-m-d'))->where('status_id', 24)->first();

        for($i = 0; $i < count($input['materiaal_id']); $i++)
        {
            $temp = new Inkoopverzoek_has_materiaal;
            $temp->materiaal_id = $input['materiaal_id'][$i];
            $temp->inkoopverzoek_id = $inkoop->id;
            $temp->aantal = $input['aantal'][$i];
            $temp->save();
        }

        $log = new Log;
        $log->gebruiker_id = Auth::user()->id;
        $log->omschrijving = Auth::user()->name." heeft een inkoopverzoek geplaatst.";
        $log->type = "Inkoop";
        $log->save();

        return redirect('/voorraad/verzoek')->with('succes', 'U heeft succesvol een inkoopverzoek geplaatst.');
    }

    public function show($id)
    {
    	$inkoopverzoek = Inkoopverzoek::find($id);
    	$inkoopverzoek->user = User::find($inkoopverzoek->gebruiker_id);
    	$inkoopverzoek->status = Status::find($inkoopverzoek->status_id);
    	$ihm = Inkoopverzoek_has_materiaal::with('materiaal')->where('inkoopverzoek_id', $id)->get();

    	$voorraad = DB::select( DB::raw("select v.materiaal_id as 'id', v.datum as 'datum', sum(v.mutatie) as 'aantal', m.naam as 'naam', m.omschrijving as 'omschrijving' from voorraad as v
    									join materiaal as m
    									on m.id = v.materiaal_id
    									group by materiaal_id"));

    	$page = 'voorraad';
    	$sub = 'verzover';

    	return view('inkoop.verzoekshow', compact('inkoopverzoek', 'ihm', 'voorraad', 'page', 'sub'));
    }

    public function setStatus($id, $status_id)
    {
        $inkoopverzoek = Inkoopverzoek::find($id);
        $inkoopverzoek->status_id = $status_id;
        if($status_id == 25)
        {
            $inkoopverzoek->reden_afgekeurd = Auth::user()->name." heeft het verzoek afgekeurd.";
        }
        $inkoopverzoek->save();

        return redirect('/verzoek/'.$id)->with('succes', 'Inkoopverzoek gewijzigd.');
    }
}
