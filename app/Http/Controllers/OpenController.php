<?php

namespace App\Http\Controllers;

use Request;
use Auth;
use DB;
use Redirect;
use Datetime;
use App\Models\Client;
use App\Models\Factuur;
use App\Models\Appointment;
use App\Models\Appointment_has_activities;
use App\Models\Work;
use App\Models\Status;
use App\Models\Status_has_factuur;
use App\Models\Bedrijf;
use App\Models\Employee;
use App\Models\Worklist;
use App\Models\Worklist_has_appointments;
use App\Models\Offerte;
use App\Models\Activiteit_has_offerte;
use App\Models\Offerte_has_status;
use App\Models\Mails;
use Mail;


class OpenController extends Controller
{

	public function sendInvoice($relatie_id, $factuur_id)
	{
		if(Auth::check())
		{
			$relatie = Client::find($relatie_id);
			$factuur = Factuur::find($factuur_id);
			$bedrijf = Bedrijf::find(1);

			//$data = array('relatie' => $relatie, 'bedrijf' => $bedrijf, 'afspraakdatum' => $afspraakdatum, 'date' => $date, 'administratiekosten' => $administratiekosten, 'factuurbedrag' => $factuurbedrag);
			$data = array('relatie' => $relatie, 'bedrijf' => $bedrijf, 'factuur' => $factuur);

			// return view('mail.sendInvoice', compact('relatie', 'factuur', 'bedrijf'));

			if($relatie->email != null && $relatie->email != "")
			{
				// Mail::send('mail.sendInvoice', $data, function($message) use ($relatie, $bedrijf, $factuur)
				// {
				// 	$message->to($relatie->email, $relatie->voornaam." ".$relatie->achternaam)->subject('Factuur '.$factuur->id);
				// 	$message->bcc($bedrijf->email, Auth::user()->name);
				// 	$message->from($bedrijf->email, $bedrijf->naam);
				// });

				$mail = new Mails;
				$mail->schrijver = Auth::user()->id;
				$mail->klant_id = $relatie_id;
				$mail->mailtype = 8;
				$mail->afspraak_id = null;
				$mail->factuur_id = $factuur->id;
				$mail->offerte_id = null;
				$mail->onderwerp = 'Factuur '.$factuur->id;
				$mail->omschrijving = "Verstuurde offerte.";
				$mail->email = $relatie->email;
				$mail->post = 0;
				$mail->save();

				return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de factuur succesvol verzonden!');
			}
			else
			{
				return redirect('/relatie/'.$relatie_id)->with('danger', 'Er is geen email bekend van de relatie, vul deze eerst aan om dit te versturen.');
			}
		}
		else
		{
			return redirect('/login');
		}
	}

	public function sendOffer($relatie_id, $offerte_id)
	{
		if(Auth::check())
		{
			$relatie = Client::find($relatie_id);
			$offerte = Offerte::find($offerte_id);
			$bedrijf = Bedrijf::find(1);

			$data = array('relatie' => $relatie, 'bedrijf' => $bedrijf, 'offerte' => $offerte);

			if($relatie->email != null && $relatie->email != "")
			{
				// Mail::send('mail.sendOffer', $data, function($message) use ($relatie, $bedrijf, $offerte)
				// {
				// 	$message->to($relatie->email, $relatie->voornaam." ".$relatie->achternaam)->subject($bedrijf->naam." | Uw offerte betreft dak werkzaamheden!");
				// 	$message->bcc($bedrijf->email, Auth::user()->name);
				// 	$message->from($bedrijf->email, $bedrijf->naam);
				// });

				$mail = new Mails;
				$mail->schrijver = Auth::user()->id;
				$mail->klant_id = $relatie_id;
				$mail->mailtype = 8;
				$mail->afspraak_id = null;
				$mail->factuur_id = null;
				$mail->offerte_id = $offerte->id;
				$mail->onderwerp = 'Offerte '.$offerte->id;
				$mail->omschrijving = "Verstuurde offerte.";
				$mail->email = $relatie->email;
				$mail->post = 0;
				$mail->save();

				return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de offerte succesvol verzonden!');
			}
			else
			{
				return redirect('/relatie/'.$relatie_id)->with('danger', 'Er is geen email bekend van de relatie, vul deze eerst aan om dit te versturen.');
			}
		}
		else
		{
			return redirect('/login');
		}

	}

	public function shareInvoice($relatie_id, $huisnummer, $factuur_id)
	{
		$relatie = Client::find($relatie_id);
		$factuur = Factuur::find($factuur_id);
		// dd($factuur);
		if($factuur != null && $relatie != null)
		{
			if($factuur->klant_id == $relatie_id && $relatie->huisnummer == $huisnummer)
			{
				$afspraak = Appointment::where('factuur_id', $factuur_id)->first();
		        $aha = Appointment_has_activities::where('afspraak_id', $afspraak->id)->get();
		        $act = Work::all();

		        $factuurregels = array();
		        foreach($act as $ac)
		        {
		            foreach($aha as $ah)
		            {
		                if($ah->activiteit_id == $ac->id)
		                {
		                    $temp = array();
		                    $temp['afspraak_id'] = $afspraak->id;
		                    $temp['startdatum'] = $afspraak->startdatum;
		                    $temp['activiteit_id'] = $ac->id;
		                    $temp['aantal'] = $ah->aantal;
		                    $temp['prijs'] = $ah->prijs;
		                    $temp['btw'] = round($ac->prijs / (1 + ($ac->btw / 100) ) * ($ac->btw / 100) * $ah->aantal, 2);
		                    $temp['totaalregel'] = $ah->aantal * $ac->prijs;
		                    $temp['omschrijving']= $ac->omschrijving;
		                    $temp['eenheid'] = $ac->eenheid;
		                    $temp['beschrijving'] = $ac->beschrijving;
		                    $temp = (object) $temp;
		                    array_push($factuurregels, $temp);
		                }
		            }
		        }
		        // $count = count(Factuur::where('klant_id', $relatie->id)->get());
		        // dd(date('ymd').$relatie->id.'.'.$count);
		        $shf = Status_has_factuur::where('factuur_id', $factuur_id)->first();
		        $status = Status::find($shf->status_id);

		        $page = "";
		        $sub = "";
		        $bedrijf = Bedrijf::find(1);
		        // dd($bedrijf);
		        $verkoper = Employee::find( Worklist::find( Worklist_has_appointments::where('afspraak_id', $afspraak->id)->first()->werklijst_id )->verkoper );
		        // dd($verkoper);
		        $factuurregels = (object) $factuurregels;
		        // dd($factuurregels);
		        return view('share.shareInvoice', compact('factuur', 'afspraak', 'verkoper', 'relatie', 'bedrijf', 'factuurregels', 'status', 'shf', 'page', 'sub'));
			}
		}
		$url = Bedrijf::find(1)->website;
		return Redirect::to('http://'.$url);
	}

	public function shareOffer($relatie_id, $huisnummer, $offerte_id)
	{

		$offerte = Offerte::findOrFail($offerte_id);
        $relatie = Client::findOrFail($offerte->klant_id);
        if($relatie->huisnummer == $huisnummer && $offerte->klant_id == $relatie->id)
        {
	        // $afspraak = Afspraak::where('factuur_id', $id)->first();
	        $aha = Activiteit_has_offerte::where('offerte_id', $offerte->id)->get();
	        $act = Work::all();

	        $offerteregels = array();
	        foreach($act as $ac)
	        {
	            foreach($aha as $ah)
	            {
	                if($ah->activiteit_id == $ac->id)
	                {
	                    $temp = array();
	                    $temp['offerte_id'] = $offerte->id;
	                    $temp['startdatum'] = $offerte->datum;
	                    $temp['activiteit_id'] = $ac->activiteit_id;
	                    $temp['aantal'] = $ah->aantal;
	                    $temp['prijs'] = round($ah->prijs);
	                    $temp['kortingsbedrag'] = $ah->kortingsbedrag;
	                    $temp['kortingsperc'] = $ah->kortingsperc;
	                    $temp['btw'] = round($ac->prijs / (1 + ($ac->btw / 100) ) * ($ac->btw / 100) * $ah->aantal, 2);
	                    // $temp['btw'] = $ac->prijs;
	                    // $temp['btw'] = $ac->prijs / 1.21;
	                    $temp['totaalregel'] = $ah->aantal * $ac->prijs;
	                    $temp['omschrijving']= $ac->omschrijving;
	                    $temp['eenheid'] = $ac->eenheid;
	                    $temp['beschrijving'] = $ac->beschrijving;
	                    $temp = (object) $temp;
	                    array_push($offerteregels, $temp);
	                }
	            }
	        }

	        if($offerte->afspraak_id == null)
	        {
	        	$payment = "Nader te bepalen";
	        }
	        elseif(Appointment::find($offerte->afspraak_id)->bank == 1)
	        {
	        	$payment = "Bank";
	        }
	        else
	        {
	        	$payment = "Contant";
	        }
	        // $count = count(Factuur::where('klant_id', $relatie->id)->get());
	        // dd(date('ymd').$relatie->id.'.'.$count);
	        $shf = Offerte_has_status::where('offerte_id', $offerte_id)->first();
	        $status = Status::find($shf->status_id);

	        $page = "";
	        $sub = "";
	        $bedrijf = Bedrijf::find(1);
	        // dd($bedrijf);
	        $verkoper = Employee::find( $offerte->werknemer_id );
	        // dd($verkoper);
	        $offerteregels = (object) $offerteregels;
	        // dd($offerteregels);

        	return view('share.shareOffer', compact('offerte', 'payment', 'bedrijf', 'afspraak', 'verkoper', 'relatie', 'offerteregels', 'status', 'shf', 'page', 'sub'));
	    }

		$url = Bedrijf::find(1)->website;
		return Redirect::to('http://'.$url);
	}

}
