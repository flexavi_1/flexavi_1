<?php



namespace App\Http\Controllers;



use Request;

use App\Models\Client;

use App\Models\ClientType;

use App\Models\Employee;

use Datatables;

use App\Models\User;

use App\Models\Appointment;

use App\Models\AppointmentType;

use App\Models\History;

use App\Models\Status;

use App\Models\Log;

use App\Models\Status_has_appointment;

use App\Models\Status_has_factuur;

use App\Models\Appointment_has_activities;

use App\Models\Activiteit_has_offerte;

use App\Models\Work;

use Auth;

use App\Models\Factuur;

use App\Models\Klant_has_status;

use DB;

use DateTime;

use App\Models\Crediteur;

use App\Models\Offerte;

use App\Models\Offerte_has_status;

use App\Models\Bedrijf;

use Yajra\Datatables\Html\Builder; //

use App\DataTables\KlantDatatable;



class OfferteController extends Controller

{





    protected $htmlBuilder;

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');
    }



    public function index()

    {

        $offerten = DB::table('klant')

            ->join('offerte', 'klant.id', '=', 'offerte.klant_id')

            ->join('offerte_has_status', 'offerte.id', '=', 'offerte_has_status.offerte_id')

            ->join('status', 'offerte_has_status.status_id', '=', 'status.id')

            ->select('klant.*', 'offerte.*', 'offerte_has_status.*', 'status.*')

            ->get();



        foreach ($offerten as $o) {

            $o->werknemer_id = Employee::find($o->werknemer_id);
        }

        $offertetypes = Status::where('id', '<', 8)->where('id', '>', 4)->get();



        $page = "financieel";

        $sub = "offerte";

        return view('offerte.index', compact('offerten', 'page', 'sub', 'offertetypes'));
    }

    public function indexPerType($type_id)
    {
        $offerten = DB::table('klant')

            ->join('offerte', 'klant.id', '=', 'offerte.klant_id')

            ->join('offerte_has_status', 'offerte.id', '=', 'offerte_has_status.offerte_id')

            ->join('status', 'offerte_has_status.status_id', '=', 'status.id')

            ->select('klant.*', 'offerte.*', 'offerte_has_status.*', 'status.*')

            ->where('status.id', '=', $type_id)

            ->get();



        foreach ($offerten as $o) {

            $o->werknemer_id = Employee::find($o->werknemer_id);
        }

        $offertetypes = Status::where('id', '<', 8)->where('id', '>', 4)->get();

        $type = $type_id;

        $page = "financieel";

        $sub = "offerte";

        return view('offerte.index', compact('offerten', 'page', 'sub', 'offertetypes', 'type'));
    }



    public function create($id)

    {

        $relatie = Client::find($id);

        $afspraken = Appointment::where('klant_id', $relatie->id)->get();

        $werknemers = Employee::where('actief', 1)->get();



        $page = "financieel";

        $sub = "offerte";



        return view('offerte.nieuw', compact('relatie', 'afspraken', 'werknemers', 'page', 'sub'));
    }



    public function store($id)

    {

        $input = Request::all();





        $gebruiker = User::find($input['verwerkt_door']);

        $relatie = Client::find($id);

        $werknemer = Employee::find($input['werknemer_id']);

        $datum = date('Y-m-d', strtotime($input['datum']));

        $vervaldatum = date('Y-m-d', strtotime($input['vervaldatum']));



        $offerte = new Offerte;

        $offerte->gebruiker_id = $gebruiker->id;

        $offerte->klant_id = $relatie->id;

        $offerte->vervaldatum = $vervaldatum;

        $offerte->werknemer_id = $werknemer->id;

        $offerte->datum = $datum;

        if ($input['afspraak_id'] == 0) {

            $offerte->afspraak_id = null;
        } else {

            $afspraak = Appointment::find($input['afspraak_id']);

            $offerte->afspraak_id = $afspraak->id;
        }

        $offerte->opmerking = $input['opmerking'];

        $offerte->aanbieding = $input['aanbieding'];

        $offerte->omschrijving = $input['omschrijving'];

        $offerte->save();



        $offerte = Offerte::where('gebruiker_id', $gebruiker->id)->where('klant_id', $relatie->id)->where('datum', $datum)->first();



        $activiteiten = Work::where('actief', 1)->get();



        $page = "financieel";

        $sub = "offerte";



        if (isset($input['voldaan'])) {

            $voldaan = $input['voldaan'];

            return view('offerte.addWork', compact('voldaan', 'activiteiten', 'offerte', 'page', 'sub'));
        }

        return view('offerte.addWork', compact('activiteiten', 'offerte', 'page', 'sub'));
    }



    public function storeWork($id)

    {

        $relatie = Client::find($id);

        $input = Request::all();

        $offerte = Offerte::find($input['offerte_id']);

        // dd($input);



        $i = 1; // algoritme om aantal te vinden

        $rij = array();

        foreach ($input['aantal'] as $r) {

            if ($i != count($input['aantal'])) {

                if (isset($r["s_" . $i . ":i_0"]))

                    array_push($rij, $r["s_" . $i . ":i_0"]);
            } else {

                $i--;

                array_push($rij, $input['aantal'][$i]);
            }

            $i++;
        }

        //rij geeft nu in de juiste volgorde de hoeveelheden aan.



        //totaalbedrag uitrekenen

        $relatie = Client::find($id);

        $offerte = Offerte::find($input['offerte_id']);

        $offerte->inc =  (isset($input['inc'])) ? 1 : 0;

        $totaalbedrag = 0;

        $werknemer = Employee::find($offerte->werknemer_id);

        if ($offerte->afspraak_id == null) {

            $geschiedenisString = date('Y-m-d') . ": Offerte opgesteld door " . $werknemer->voornaam . " " . $werknemer->achternaam . " met de volgende werkzaamheden: <br>";
        } else {

            $afspraak = Appointment::find($offerte->afspraak_id);

            $geschiedenisString = date('Y-m-d', strtotime($afspraak->startdatum)) . ": Offerte opgesteld door " . $werknemer->voornaam . " " . $werknemer->achternaam . " met de volgende werkzaamheden: <br>";
        }



        for ($i = 0; $i < count($input['activiteiten']); $i++) {

            $act = Work::findOrFail($input['activiteiten'][$i]);

            // dd($rij);

            $aha = new Activiteit_has_offerte;

            $aha->activiteit_id = $act->id;

            $aha->offerte_id = $offerte->id;

            $aha->aantal = $input['aantal'][$i];



            if ($input['prijs'][$i] != 0)

                $aha->prijs = $input['prijs'][$i];

            else

                $aha->prijs = $act->prijs;



            $aha->kortingsbedrag = $input['korting'][$i];

            $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;



            $bedrag = $aha->prijs * $aha->aantal - $aha->kortingsbedrag;

            if ($offerte->inc == 0) {

                $totaalbedrag = $totaalbedrag + ($bedrag / 100 * $act->btw) + $bedrag;
            } else {

                $totaalbedrag = $totaalbedrag + $bedrag;
            }



            $aha->opmerking = $input['opmerking'][$i];

            $geschiedenisString = $geschiedenisString . ", " . $aha->aantal . $aha->eenheid . " " . $act->omschrijving . " (" . $aha->opmerking . ") " . " à € " . $aha->prijs . " => € " . number_format($bedrag, 2) . '<br>';

            $aha->save();
        }

        $geschiedenisString = $geschiedenisString . "<br>Totaal: € " . $totaalbedrag;



        $geschiedenis = new History;

        if ($offerte->afspraak_id != null) {

            $geschiedenis->afspraak_id = $afspraak->id;
        } else {

            $geschiedenis->afspraak_id = null;
        }

        $geschiedenis->gebruiker_id = Auth::user()->id;

        $geschiedenis->klant_id = $offerte->klant_id;

        $geschiedenis->datum = date('Y-m-d');

        $geschiedenis->omschrijving = $geschiedenisString;

        $geschiedenis->type = "werkzaamheden";

        $geschiedenis->save();



        //geschiedenis

        $regel = new History;

        $regel->datum = date('Y-m-d');

        $regel->klant_id = $relatie->id;

        $regel->afspraak_id = $offerte->afspraak_id;

        $user = User::findOrFail(Auth::user()->id);

        $regel->gebruiker_id = $user->id;

        $regel->omschrijving = $user->name . " heeft een opmerking aan de offerte toegevoegd: " . $offerte->omschrijving;

        $regel->save();



        $offerte->totaalbedrag = $totaalbedrag;



        $log = new Log;

        $log->omschrijving = Auth::user()->name . " heeft een offerte aangemaakt voor relatie: " . $relatie->postcode . " - " . $relatie->huisnummer;

        $log->gebruiker_id = Auth::user()->id;

        $log->klant_id = $relatie->id;

        $log->type = "Offerte";

        $log->save();



        //geschiedenis

        $regel = new History;

        $regel->datum = date('Y-m-d');

        $regel->klant_id = $relatie->id;

        $regel->afspraak_id = $offerte->afspraak_id;

        $user = User::findOrFail(Auth::user()->id);

        $regel->gebruiker_id = $user->id;

        $regel->omschrijving = $user->name . " heeft een offerte aangemaakt voor relatie: " . $relatie->postcode . " - " . $relatie->huisnummer;

        $regel->save();



        //status

        $ohs = new Offerte_has_status;

        $ohs->offerte_id = $offerte->id;

        $ohs->status_id = 5;

        $ohs->verwerkt_door = Auth::user()->id;

        $ohs->save();





        $offerte->save();

        if (isset($input['voldaan'])) {

            $voldaan = $input['voldaan'];

            if ($voldaan == 1)

                return redirect('/afspraak/maken/' . $id)->with('succes', 'U heeft de offerte verwerkt en kunt nu de afspraak aanmaken');

            else

                return redirect('/relatie/' . $id)->with('succes', 'De afspraak is succesvol verwerkt.');
        }

        return redirect('/relatie/' . $id)->with('succes', 'De offerte is toegevoegd aan de relatie.');
    }



    public function destroy($id)

    {

        $offerte = Offerte::find($id);



        $aho = Activiteit_has_Offerte::where('offerte_id', $id)->get();

        foreach ($aho as $a) {

            $a->delete();
        }



        $sho = Offerte_has_status::where('offerte_id', $id)->get();

        foreach ($sho as $s) {

            $s->delete();
        }



        $relatie = Client::find($offerte->klant_id);

        $offerte->delete();



        return redirect('/relatie/' . $relatie->id)->with('succes', 'Offerte succesvol verwijderd.');
    }



    public function show($id)

    {

        $offerte_id = $id;

        $offerte = Offerte::findOrFail($offerte_id);

        $relatie = Client::findOrFail($offerte->klant_id);

        $huisnummer = $relatie->huisnummer;

        if ($relatie->huisnummer == $huisnummer && $offerte->klant_id == $relatie->id) {

            // $afspraak = Afspraak::where('factuur_id', $id)->first();

            $aha = Activiteit_has_offerte::where('offerte_id', $offerte->id)->get();

            $act = Work::where('actief', 1)->get();



            $offerteregels = array();

            foreach ($act as $ac) {

                foreach ($aha as $ah) {

                    if ($ah->activiteit_id == $ac->id) {

                        $temp = array();

                        $temp['offerte_id'] = $offerte->id;

                        $temp['startdatum'] = $offerte->datum;

                        $temp['activiteit_id'] = $ac->activiteit_id;

                        $temp['aantal'] = $ah->aantal;

                        // $temp['opmerking'] = $ah->opmerking;

                        $temp['prijs'] = round($ah->prijs);

                        $temp['kortingsbedrag'] = $ah->kortingsbedrag;

                        $temp['kortingsperc'] = $ah->kortingsperc;

                        // $temp['btw'] = round($ac->prijs / (1 + ($ac->btw / 100) ) * ($ac->btw / 100) * $ah->aantal, 2);

                        if ($offerte->inc == 1) {

                            $temp['btw'] = number_format($ah->prijs / (100 + $ac->btw) * $ac->btw, 2); // aanname: prijs is inclusief btw

                        } else {

                            $temp['btw'] = number_format($ah->prijs / 100 * $ac->btw, 2);
                        }

                        // $temp['btw'] = $ac->prijs;

                        // $temp['btw'] = $ac->prijs / 1.21;

                        $temp['opmerking'] = $ah->opmerking;

                        $temp['totaalregel'] = $ah->aantal * $ac->prijs;

                        $temp['omschrijving'] = $ac->omschrijving;

                        $temp['eenheid'] = $ac->eenheid;

                        $temp['beschrijving'] = $ac->beschrijving;

                        $temp = (object) $temp;

                        array_push($offerteregels, $temp);
                    }
                }
            }



            if ($offerte->afspraak_id == null) {

                $payment = "Nader te bepalen";
            } elseif (isset($offerte->afspraak_id)) {

                $relatedAppointment = Appointment::find($offerte->afspraak_id);

                if ($relatedAppointment) {

                    if ($relatedAppointment->bank == 1) {

                        $payment = "Bank";
                    } else {

                        $payment = "Contant";
                    }
                } else {

                    $payment = "Contant";
                }
            } else {

                $payment = "Contant";
            }

            // $count = count(Factuur::where('klant_id', $relatie->id)->get());

            // dd(date('ymd').$relatie->id.'.'.$count);

            $shf = Offerte_has_status::where('offerte_id', $offerte_id)->first();

            $status = Status::find($shf->status_id);



            $page = "";

            $sub = "";

            $bedrijf = Bedrijf::find(1);

            // dd($bedrijf);

            $verkoper = Employee::find($offerte->werknemer_id);

            // dd($verkoper);

            $offerteregels = (object) $offerteregels;

            // dd($offerteregels);



            return view('share.shareOffer', compact('offerte', 'payment', 'bedrijf', 'afspraak', 'verkoper', 'relatie', 'offerteregels', 'status', 'shf', 'page', 'sub'));
        }



        $url = Bedrijf::find(1)->website;

        return Redirect::to('http://' . $url);
    }



    public function openAfspraak($id)

    {



        $afspraken = Appointment::where('id', $id)->get();

        $relatie = Client::find(Appointment::find($id)->klant_id);

        $werknemers = Employee::where('actief', 1)->get();



        $page = "financieel";

        $sub = "offerte";



        return view('offerte.nieuw', compact('relatie', 'afspraken', 'werknemers', 'page', 'sub'));
    }



    public function addNewWork($id)

    {

        $cont = 'offer';

        $offerte = Offerte::find($id);



        $page = "financieel";

        $sub = "offerte";



        return view('activiteit.toevoegen', compact('page', 'offerte', 'cont', 'sub'));
    }



    public function saveNewWork($id)

    {



        $input = Request::all();



        for ($i = 0; $i < count($input['prijs']); $i++) {

            $temp = new Work;

            $temp->omschrijving = $input['omschrijving'][$i];

            $temp->prijs = $input['prijs'][$i];

            $temp->eenheid = $input['eenheid'][$i];

            $temp->actief = 1;

            $temp->save();



            $log = new Log;

            $log->omschrijving = Auth::user()->name . " heeft de werkactiviteit: " . $input['omschrijving'][$i] . ", toegevoegd.";

            $log->gebruiker_id = Auth::user()->id;

            $log->type = "Activiteit";

            $log->save();
        }





        $offerte = Offerte::find($id);



        $activiteiten = Work::where('actief', 1)->get();



        $page = "financieel";

        $sub = "offerte";



        return view('offerte.addWork', compact('activiteiten', 'offerte', 'page', 'sub'));
    }



    public function honoreren($id)

    {

        $offerte = Offerte::find($id);

        $relatie = Client::find($offerte->klant_id);

        $ohas = Activiteit_has_offerte::where('offerte_id', $offerte->id)->get();

        foreach ($ohas as $oha) {

            $oha->activiteit_id = Work::find($oha->activiteit_id);
        }

        $ohs = Offerte_has_status::where('offerte_id', $offerte->id)->first();

        $ohs->status_id = 7;

        $ohs->save();

        // dd($ohas);

        $geschiedenis = new History;

        $geschiedenis->gebruiker_id = Auth::user()->id;

        $geschiedenis->klant_id = $offerte->klant_id;

        $geschiedenis->omschrijving = Auth::user()->name . " heeft de offerte gehonoreerd.";

        $geschiedenis->datum = date('Y-m-d');

        $geschiedenis->type = "Offerte";

        $geschiedenis->save();



        $log = new Log;

        $log->gebruiker_id = Auth::user()->id;

        $log->klant_id = $offerte->klant_id;

        $log->omschrijving = Auth::user()->name . " heeft de offerte gehonoreerd.";

        $log->type = "Offerte";

        $log->save();



        $werknemers = Employee::where('actief', 1)->get();

        $types = AppointmentType::where('id', 1)->get();



        $page = "financieel";

        $sub = "offerte";



        return view('offerte.setAfspraak', compact('offerte', 'werknemers', 'relatie', 'types', 'ohas', 'page', 'sub'));
    }

    public function honorerenWithType($id, $type)

    {

        $offerte = Offerte::find($id);

        $relatie = Client::find($offerte->klant_id);

        $ohas = Activiteit_has_offerte::where('offerte_id', $offerte->id)->get();

        foreach ($ohas as $oha) {

            $oha->activiteit_id = Work::find($oha->activiteit_id);
        }

        $ohs = Offerte_has_status::where('offerte_id', $offerte->id)->first();

        $ohs->status_id = 7;

        $ohs->save();

        // dd($ohas);

        $geschiedenis = new History;

        $geschiedenis->gebruiker_id = Auth::user()->id;

        $geschiedenis->klant_id = $offerte->klant_id;

        $geschiedenis->omschrijving = Auth::user()->name . " heeft de offerte gehonoreerd.";

        $geschiedenis->datum = date('Y-m-d');

        $geschiedenis->type = "Offerte";

        $geschiedenis->save();



        $log = new Log;

        $log->gebruiker_id = Auth::user()->id;

        $log->klant_id = $offerte->klant_id;

        $log->omschrijving = Auth::user()->name . " heeft de offerte gehonoreerd.";

        $log->type = "Offerte";

        $log->save();



        $werknemers = Employee::where('actief', 1)->get();

        $types = AppointmentType::where('id', 1)->get();



        $page = "financieel";

        $sub = "offerte";



        return view('offerte.setAfspraak', compact('offerte', 'werknemers', 'relatie', 'types', 'ohas', 'page', 'sub'));
    }



    public function opslaan($id)

    {

        $input = Request::all();



        if ($input['startdatum'] >= $input['einddatum'] && $input['radio_demo'] == 2) {

            return redirect('/afspraak/maken/' . $input['klant_id'])->with('danger', 'Startdatum moet voor de einddatum liggen.');
        } else {

            $afspraak = new Appointment;


            if ($input['radio_demo'] < 2) {

                $startdatum = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
                $einddatum = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);

                $afspraak->startdatum = $startdatum->format('Y-m-d') . " " . ($input['radio_demo'] == 1 ? "12:30" : "08:00");
                $afspraak->einddatum = $einddatum->format('Y-m-d') . " " . ($input['radio_demo'] == 1 ? "17:00" : "12:30");
            } else {

                $startdatum = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
                $einddatum = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);


                $afspraak->startdatum = $startdatum->format('Y-m-d H:i');
                $afspraak->einddatum = $einddatum->format('Y-m-d H:i');
            }

            $afspraak->klant_id = $input['klant_id'];

            $afspraak->geschreven_door = $input['geschreven_door'];

            $afspraak->verwerkt_door = $input['verwerkt_door'];

            $afspraak->afspraaktype_id = $input['afspraaktype_id'];

            $afspraak->omschrijving = $input['omschrijving'];

            $afspraak->bijgewerkt_door = 0;

            $afspraak->totaalbedrag = 0.00;

            $afspraak->bevestiging = 0;

            $afspraak->save();

            // dd($afspraak);



            // $afspr = Afspraak::where('startdatum', date('Y-m-d H:i:s', $startstamp))

            //                     ->where('klant_id', $input['klant_id'])

            //                     ->where('einddatum', date('Y-m-d H:i:s', $endstamp))->first();



            // dd($afspr);

            $relatie = Client::findOrFail($afspraak->klant_id);

            if ($relatie->klanttype_id < 3 /*&& afspraaktype =/= offerte*/) {

                $relatie->klanttype_id = 3;

                $relatie->save();
            }



            $sha = new Status_has_appointment;

            $sha->status_id = 1;





            $relatie = Client::findOrFail($afspraak->klant_id);

            if (Klant_has_status::where('klant_id', $relatie->id)->exists()) //set Reminder

            {

                $khs = Klant_has_status::where('klant_id', $relatie->id)->first();

                if ($khs->status_id == 22) {

                    if ($relatie->calcDiff($khs->updated_at, date('Y-m-d')) >= 350) {

                        $khs->status_id = 21;

                        $khs->verwerkt_door = Auth::user()->id;

                        $khs->save();
                    }
                }
            } else {



                $khs = new Klant_has_status;

                $khs->status_id = 21;

                $khs->klant_id = $relatie->id;

                $khs->verwerkt_door = Auth::user()->id;

                $khs->save();
            }





            $sha->afspraak_id = $afspraak->id;

            $sha->gebruiker_id = Auth::user()->id;

            $sha->omschrijving = Auth::user()->name . " heeft deze afspraak Toegevoegd.";

            $sha->save();



            $log = new Log;

            $log->omschrijving = Auth::user()->name . " heeft een afspraak op: " . $afspraak->startdatum . " -- " . $afspraak->einddatum . ", toegevoegd.";

            $log->gebruiker_id = Auth::user()->id;

            $log->klant_id = $input['klant_id'];

            $log->type = "Afspraak";

            $log->save();



            //geschiedenis

            $regel = new History;

            $regel->datum = date('Y-m-d');

            $regel->klant_id = $afspraak->klant->id;

            $regel->afspraak_id = $afspraak->id;

            $user = User::findOrFail(Auth::user()->id);

            $regel->gebruiker_id = $user->id;

            $regel->omschrijving = $user->name . " heeft een afspraak gemaakt met klant op " . $afspraak->returnDate($afspraak->startdatum) . " --- " . $afspraak->returnDate($afspraak->einddatum);

            $regel->save();



            $offerte = Offerte::find($id);

            $ohas = Activiteit_has_offerte::where('offerte_id', $id)->get();

            $totaalbedrag = 0.00;

            if (isset($input['klusprijsAan'])) {

                $afspraak->klusprijs = 1;

                $klusprijs = $input['klusprijs'];

                $totaalbedrag = $klusprijs;
            }



            foreach ($ohas as $oha) {

                $tmp = new Appointment_has_activities; //temporary aha

                $tmp->activiteit_id = $oha->activiteit_id; //set a.id into appointment_has_activity

                $tmp->afspraak_id = $afspraak->id; // set appointment_id

                $tmp->aantal = $oha->aantal; // set amount

                $tmp->opmerking = $oha->opmerking;



                if (isset($klusprijs)) {

                    if ($klusprijs > 0) {

                        $tmp->prijs = $oha->prijs;

                        $bedrag = $tmp->prijs * $tmp->aantal;

                        if ($klusprijs - $bedrag <= 0) {

                            $tmp->kortingsbedrag = $bedrag - $klusprijs;

                            $tmp->kortingsperc = $tmp->kortingsbedrag / ($tmp->prijs * $tmp->aantal) * 100;

                            $klusprijs = $klusprijs - $bedrag;
                        } else {

                            $tmp->kortingsbedrag = $oha->kortingsbedrag;

                            $tmp->kortingsperc = $tmp->kortingsbedrag / ($tmp->prijs * $tmp->aantal) * 100;

                            $klusprijs = $klusprijs - $bedrag;
                        }
                    }
                } else {



                    $tmp->kortingsbedrag = $oha->kortingsbedrag;

                    $tmp->kortingsperc = $oha->kortingsperc;

                    $tmp->prijs = $oha->prijs;

                    $totaalbedrag = $totaalbedrag + ($tmp->aantal * $tmp->prijs);
                }

                $tmp->save();
            }

            $afspraak->offerte_id = $id;

            $afspraak->totaalbedrag = $totaalbedrag;

            $afspraak->save();



            return redirect('/relatie/' . $relatie->id)->with('succes', 'U heeft de offerte succesvol genhonoreerd.');
        }
    }



    public function vervallen($id)

    {

        $offerte = Offerte::find($id);

        $oha = Offerte_has_status::where('offerte_id', $offerte->id)->first();

        $oha->status_id = 6;

        $oha->save();



        $geschiedenis = new History;

        $geschiedenis->gebruiker_id = Auth::user()->id;

        $geschiedenis->klant_id = $offerte->klant_id;

        $geschiedenis->omschrijving = Auth::user()->name . " heeft de offerte geannuleerd.";

        $geschiedenis->datum = date('Y-m-d');

        $geschiedenis->type = "Offerte";

        $geschiedenis->save();



        $log = new Log;

        $log->gebruiker_id = Auth::user()->id;

        $log->klant_id = $offerte->klant_id;

        $log->omschrijving = Auth::user()->name . " heeft de offerte geannuleerd.";

        $log->type = "Offerte";

        $log->save();



        return redirect('/offerte')->with('succes', 'Offerte succesvol geannuleerd.');
    }

    public function vervallenWithType($id, $type)

    {

        $offerte = Offerte::find($id);

        $oha = Offerte_has_status::where('offerte_id', $offerte->id)->first();

        $oha->status_id = 6;

        $oha->save();



        $geschiedenis = new History;

        $geschiedenis->gebruiker_id = Auth::user()->id;

        $geschiedenis->klant_id = $offerte->klant_id;

        $geschiedenis->omschrijving = Auth::user()->name . " heeft de offerte geannuleerd.";

        $geschiedenis->datum = date('Y-m-d');

        $geschiedenis->type = "Offerte";

        $geschiedenis->save();



        $log = new Log;

        $log->gebruiker_id = Auth::user()->id;

        $log->klant_id = $offerte->klant_id;

        $log->omschrijving = Auth::user()->name . " heeft de offerte geannuleerd.";

        $log->type = "Offerte";

        $log->save();



        return redirect('/offerte/zoeken/' . $type)->with('succes', 'Offerte succesvol geannuleerd.');
    }



    public function edit($relatie_id, $id)

    {

        $offerte = Offerte::find($id);

        $relatie = Client::find($offerte->klant_id);

        $afspraken = Appointment::where('klant_id', $relatie->id)->get();

        $werknemers = Employee::where('actief', 1)->get();



        $page = "financieel";

        $sub = "offerte";



        return view('offerte.edit', compact('relatie', 'offerte', 'afspraken', 'werknemers', 'page', 'sub', 'offertetypes'));
    }



    public function update($relatie_id, $id)

    {

        $offerte = Offerte::find($id);

        $input = Request::all();

        // dd($input['datum']);

        // dd($offerte);

        $offerte->datum = date('Y-m-d', strtotime($input['datum']));

        $offerte->werknemer_id = $input['werknemer_id'];

        $offerte->afspraak_id = $input['afspraak_id'];

        $offerte->gebruiker_id = $input['verwerkt_door'];

        $offerte->klant_id = $input['klant_id'];

        $offerte->omschrijving = $input['omschrijving'];

        $offerte->aanbieding = $input['aanbieding'];

        $offerte->opmerking = $input['opmerking'];

        $offerte->vervaldatum = date('Y-m-d', strtotime($input['vervaldatum']));

        $offerte->save();



        $activiteiten = Work::where('actief', 1)->get();



        $ohas = Activiteit_has_offerte::with('activiteit')->where('offerte_id', $offerte->id)->get();

        $relatie = Client::find($relatie_id);



        $page = "financieel";

        $sub = "offerte";



        if (isset($input['werkzaamheden']))

            return view('offerte.editWork', compact('relatie', 'activiteiten', 'ohas', 'offerte', 'page', 'sub'));





        return redirect('/relatie/' . $offerte->klant_id)->with('succes', 'Offerte gewijzigd.');
    }



    public function editWork($relatie_id, $id)

    {

        $relatie = Client::find($id);

        $input = Request::all();

        $offerte = Offerte::find($input['offerte_id']);

        $offerte->inc = (isset($input['inc'])) ? 1 : 0;



        // dd($input);

        $double = false;

        for ($k = 0; $k < count($input['activiteiten']); $k++) {

            for ($l = 0; $l < count($input['activiteiten']); $l++) {

                if ($input['activiteiten'][$l] == $input['activiteiten'][$k] && $k != $l)

                    $double = true;
            }
        }



        if (!$double) {

            $i = 1; // algoritme om aantal te vinden

            $rij = array();

            foreach ($input['aantal'] as $r) {

                if ($i != count($input['aantal'])) {

                    if (isset($r["s_" . $i . ":i_0"]))

                        array_push($rij, $r["s_" . $i . ":i_0"]);
                } else {

                    $i--;

                    array_push($rij, $input['aantal'][$i]);
                }

                $i++;
            }

            //rij geeft nu in de juiste volgorde de hoeveelheden aan.



            //totaalbedrag uitrekenen

            $relatie = Client::find($id);

            $offerte = Offerte::find($input['offerte_id']);

            $totaalbedrag = 0;

            $werknemer = Employee::find($offerte->werknemer_id);

            if ($offerte->afspraak_id == null) {

                $geschiedenisString = date('Y-m-d') . ": Offerte opgesteld door " . $werknemer->voornaam . " " . $werknemer->achternaam . " met de volgende werkzaamheden: <br>";
            } else {

                $afspraak = Appointment::find($offerte->afspraak_id);

                $date = ($afspraak) ? date('Y-m-d', strtotime($afspraak->startdatum)) : date('Y-m-d');

                $employeeName = ($werknemer == null) ? "GEEN WERKNEMER GEKOPPELD AAN OFFERTE" : $werknemer->voornaam . ' ' . $werknemer->achternaam;

                $geschiedenisString = $date . ": Offerte opgesteld door " . $employeeName . " met de volgende werkzaamheden: <br>";
            }



            $ahos = Activiteit_has_offerte::where('offerte_id', $offerte->id)->get();

            foreach ($ahos as $o) {

                $o->delete();
            }



            for ($i = 0; $i < count($input['activiteiten']); $i++) {

                $act = Work::find($input['activiteiten'][$i]);

                // dd($rij);

                // foreach ($ahos as $oha)

                // {

                // if($oha->activiteit_id == $act->id)

                // {

                $oha = new Activiteit_has_offerte;

                $oha->activiteit_id = $act->id;

                $oha->offerte_id = $offerte->id;

                $oha->aantal = $input['aantal'][$i];

                if ($input['prijs'][$i] != 0)

                    $oha->prijs = $input['prijs'][$i];

                else

                    $oha->prijs = $act->prijs;





                $oha->kortingsbedrag = $input['korting'][$i];

                $oha->kortingsperc = $oha->kortingsbedrag / ($oha->prijs * $oha->aantal) * 100;



                $bedrag = $oha->prijs * $oha->aantal - $oha->kortingsbedrag;



                if ($offerte->inc == 0) {

                    $totaalbedrag = $totaalbedrag + ($bedrag / 100 * $act->btw) + $bedrag;
                } else {

                    $totaalbedrag = $totaalbedrag + $bedrag;
                }





                $oha->opmerking = $input['opmerking'][$i];

                $geschiedenisString = $geschiedenisString . ", " . $oha->aantal . $oha->eenheid . " " . $act->omschrijving . " (" . $oha->opmerking . ") " . " à € " . $oha->prijs . " € " . number_format($bedrag, 2);

                $oha->save();

                // }

                // }

            }



            $geschiedenisString = $geschiedenisString . "<br>Totaal: € " . $totaalbedrag;



            $geschiedenis = new History;

            if ($offerte->afspraak_id != null) {

                $geschiedenis->afspraak_id = $offerte->afspraak_id;
            } else {

                $geschiedenis->afspraak_id = null;
            }

            $geschiedenis->gebruiker_id = Auth::user()->id;

            $geschiedenis->klant_id = $offerte->klant_id;

            $geschiedenis->datum = date('Y-m-d');

            $geschiedenis->omschrijving = $geschiedenisString;

            $geschiedenis->type = "werkzaamheden";

            $geschiedenis->save();



            $offerte->totaalbedrag = $totaalbedrag;



            $log = new Log;

            $log->omschrijving = Auth::user()->name . " heeft een offerte aangemaakt voor relatie: " . $relatie->postcode . " - " . $relatie->huisnummer;

            $log->gebruiker_id = Auth::user()->id;

            $log->klant_id = $relatie->id;

            $log->type = "Offerte";

            $log->save();



            //geschiedenis

            $regel = new History;

            $regel->datum = date('Y-m-d');

            $regel->klant_id = $relatie->id;

            $regel->afspraak_id = $offerte->afspraak_id;

            $user = User::findOrFail(Auth::user()->id);

            $regel->gebruiker_id = $user->id;

            $regel->omschrijving = $user->name . " heeft een offerte aangemaakt voor relatie: " . $relatie->postcode . " - " . $relatie->huisnummer;

            $regel->save();



            //status

            $ohs = Offerte_has_status::where('offerte_id', $offerte->id)->first();

            $ohs->offerte_id = $offerte->id;

            $ohs->status_id = 5;

            $ohs->verwerkt_door = Auth::user()->id;

            $ohs->save();





            $offerte->save();



            if (isset($input['werkzaamheden'])) {

                $activiteiten = Work::where('actief', 1)->get();

                // $danger = 'U kunt niet meerdere malen dezelfde werkzaamheden opgeven.';

                $edit = true;

                $page = "financieel";

                $sub = "offerte";

                $ohas = Activiteit_has_offerte::with('activiteit')->where('offerte_id', $offerte->id)->get();

                return view('offerte.addWork', compact('edit', 'ohas', 'activiteiten', 'offerte', 'danger', 'page', 'sub'));
            }



            return redirect('/relatie/' . $id)->with('succes', 'De offerte is gewijzigd.');
        } else {

            $activiteiten = Work::where('actief', 1)->get();



            $ohas = Activiteit_has_offerte::with('activiteit')->where('offerte_id', $offerte->id)->get();

            $danger = 'U kunt niet meerdere malen dezelfde werkzaamheden opgeven.';



            $page = "financieel";

            $sub = "offerte";

            return view('offerte.editWork', compact('activiteiten', 'ohas', 'offerte', 'page', 'sub'));
        }
    }



    public function addWorktoEdit($relatie_id, $id)

    {



        $relatie = Client::find($relatie_id);

        $input = Request::all();

        $offerte = Offerte::find($input['offerte_id']);

        // dd($input);

        $double = false;

        for ($k = 0; $k < count($input['activiteiten']); $k++) {

            for ($l = 0; $l < count($input['activiteiten']); $l++) {

                if ($input['activiteiten'][$l] == $input['activiteiten'][$k] && $k != $l)

                    $double = true;
            }
        }



        if (!$double) {

            $i = 1; // algoritme om aantal te vinden

            $rij = array();

            foreach ($input['aantal'] as $r) {

                if ($i != count($input['aantal'])) {

                    if (isset($r["s_" . $i . ":i_0"]))

                        array_push($rij, $r["s_" . $i . ":i_0"]);
                } else {

                    $i--;

                    array_push($rij, $input['aantal'][$i]);
                }

                $i++;
            }

            //rij geeft nu in de juiste volgorde de hoeveelheden aan.



            //totaalbedrag uitrekenen

            $relatie = Client::find($relatie_id);

            $offerte = Offerte::find($input['offerte_id']);

            $totaalbedrag = $offerte->totaalbedrag;

            $werknemer = Employee::find($offerte->werknemer_id);

            if ($offerte->afspraak_id == null) {

                $geschiedenisString = date('Y-m-d') . ": Offerte opgesteld door " . $werknemer->voornaam . " " . $werknemer->achternaam . " met de volgende werkzaamheden:<br>";
            } else {

                $afspraak = Appointment::find($offerte->afspraak_id);

                $geschiedenisString = date('Y-m-d', strtotime($afspraak->startdatum)) . ": Offerte opgesteld door " . $werknemer->voornaam . " " . $werknemer->achternaam . " met de volgende werkzaamheden:<br>";
            }



            for ($i = 0; $i < count($input['activiteiten']); $i++) {

                $act = Work::findOrFail($input['activiteiten'][$i]);

                // dd($rij);



                $aha = new Activiteit_has_offerte;

                $aha->activiteit_id = $act->id;

                $aha->offerte_id = $offerte->id;

                $aha->aantal = $input['aantal'][$i];

                if ($input['prijs'][$i] != 0)

                    $aha->prijs = $input['prijs'][$i];

                else

                    $aha->prijs = $act->prijs;





                $aha->kortingsbedrag = $input['korting'][$i];

                $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;



                $bedrag = $aha->prijs * $aha->aantal - $aha->kortingsbedrag;

                $totaalbedrag = $totaalbedrag + $bedrag;



                $aha->opmerking = $input['opmerking'][$i];

                $geschiedenisString = $geschiedenisString . ", " . $aha->aantal . $aha->eenheid . " " . $act->omschrijving . " (" . $aha->opmerking . ") " . " à € " . $aha->prijs . " € " . number_format($bedrag, 2);

                $aha->save();
            }



            $geschiedenisString = $geschiedenisString . "<br>Totaal: € " . $totaalbedrag;



            $geschiedenis = new History;

            if ($offerte->afspraak_id != null) {

                $geschiedenis->afspraak_id = $afspraak->id;
            } else {

                $geschiedenis->afspraak_id = null;
            }

            $geschiedenis->gebruiker_id = Auth::user()->id;

            $geschiedenis->klant_id = $offerte->klant_id;

            $geschiedenis->datum = date('Y-m-d');

            $geschiedenis->omschrijving = $geschiedenisString;

            $geschiedenis->type = "werkzaamheden";

            $geschiedenis->save();



            $offerte->totaalbedrag = $totaalbedrag;



            $log = new Log;

            $log->omschrijving = Auth::user()->name . " heeft een offerte aangemaakt voor relatie: " . $relatie->postcode . " - " . $relatie->huisnummer;

            $log->gebruiker_id = Auth::user()->id;

            $log->klant_id = $relatie->id;

            $log->type = "Offerte";

            $log->save();



            //geschiedenis

            $regel = new History;

            $regel->datum = date('Y-m-d');

            $regel->klant_id = $relatie->id;

            $regel->afspraak_id = $offerte->afspraak_id;

            $user = User::findOrFail(Auth::user()->id);

            $regel->gebruiker_id = $user->id;

            $regel->omschrijving = $user->name . " heeft een offerte aangemaakt voor relatie: " . $relatie->postcode . " - " . $relatie->huisnummer;

            $regel->save();



            //status

            $ohs = Offerte_has_status::where('offerte_id', $offerte->id)->first();

            $ohs->offerte_id = $offerte->id;

            $ohs->status_id = 5;

            $ohs->verwerkt_door = Auth::user()->id;

            $ohs->save();





            $offerte->save();



            return redirect('/relatie/' . $relatie_id)->with('succes', 'De offerte is toegevoegd aan de relatie.');
        } else {

            $activiteiten = Work::where('actief', 1)->get();

            $danger = 'U kunt niet meerdere malen dezelfde werkzaamheden opgeven.';

            $edit = true;

            $page = "financieel";

            $sub = "offerte";

            return view('offerte.addWork', compact('edit', 'activiteiten', 'offerte', 'danger', 'page', 'sub'));
        }
    }
}
