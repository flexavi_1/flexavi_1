<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\History;
use App\Models\Log;
use Auth;
use App\Models\Klant_has_status;
use DB;
use App\Models\Offerte_has_status;
use App\Models\Leveranciersprijslijst;
use App\Models\Materiaal;
use App\Models\Inkoop;
use App\Models\Voorraad;
use App\Models\Inkoop_has_materiaal;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class InkoopController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function processInvoice($id)
    {
    	$relatie = Client::find($id);

    	if($relatie->klanttype_id != 5)
    	{
    		return redirect('/relatie/'.$id)->with('danger', 'Er gaat iets verkeerd, neem contact op met de administrator.');
    	}

    	$prijslijst = Leveranciersprijslijst::with('materiaal')->where('klant_id', $relatie->id)->get();
    	if(count($prijslijst) == 0)
    	{
    		return redirect('/relatie/'.$id)->with('danger', 'De leverancier heeft geen prijslijst, maak deze eerst aan.');
    	}

    	$page = "voorraad";
    	$sub = "leveran";

    	return view('inkoop.inkoop', compact('prijslijst', 'relatie', 'page', 'sub'));
    }

    public function storeInvoice($id)
    {
    	$relatie = Client::find($id);
    	$input = Request::all();

    	// dd($input);
    	$check = false;
    	for($i = 0; $i < count($input['prijs']); $i++)
    	{
    		if($input['aantal'][$i] != 0)
    		{
    			$check = true;
    		}
    	}

    	if(!$check)
    	{
    		return redirect('/inkoop/verwerken/'.$id)->with('danger', 'U heeft geen materialen ingevuld, probeer opnieuw.');
    	}

    	$tempinkoop = new Inkoop;
    	$tempinkoop->klant_id = $id;
    	$tempinkoop->datum = date('Y-m-d');
    	if(isset($input['betaald']))
    	{
    		$tempinkoop->voldaan = 1;
    	}
    	else
    	{
    		$tempinkoop->voldaan = 0;
    	}
    	$tempinkoop->aangemaakt_door = Auth::user()->id;
    	$totaalbedrag = 0.00;
    	$korting = 0.00;
    	$tempinkoop->save();

    	$tempinkoop = Inkoop::where('klant_id', $id)->where('datum', date('Y-m-d'))->where('aangemaakt_door', Auth::user()->id)->first();

    	for($i = 0; $i < count($input['prijs']); $i++)
    	{
    		if($input['aantal'][$i] > 0)
    		{
    			$temp = new Inkoop_has_materiaal;
    			$temp->inkoop_id = $tempinkoop->id;
    			$temp->materiaal_id = $input['materiaal'][$i];
    			$temp->aantal = $input['aantal'][$i];
    			$temp->inkoopprijs = $input['prijs'][$i];
    			if($input['prijs'][$i] != Leveranciersprijslijst::where('materiaal_id', $input['materiaal'][$i])->first()->inkoopprijs  )
    			{
    				$korting += (Leveranciersprijslijst::where('materiaal_id', $input['materiaal'][$i])->first()->inkoopprijs - $input['prijs'][$i]);
    			}
    			$totaalbedrag += $input['aantal'][$i] * $input['prijs'][$i];
    			$temp->save();

    			$stock = new Voorraad;
    			$stock->datum = date('Y-m-d');
    			$stock->klant_id = $id;
    			$stock->materiaal_id = $input['materiaal'][$i];
    			$stock->mutatie = $input['aantal'][$i] * Leveranciersprijslijst::where('materiaal_id', $input['materiaal'][$i])->first()->aantal;
    			$stock->save();
    		}
    	}

    	$tempinkoop->totaalbedrag = $totaalbedrag;
    	$tempinkoop->korting = $korting;
    	$tempinkoop->save();

    	$l = new Log;
    	$l->gebruiker_id = Auth::user()->id;
    	$l->klant_id = $id;
    	$l->omschrijving = Auth::user()->name." heeft deze inkoopfactuur verwerkt.";
    	$l->type = "Inkoop";
    	$l->save();
    	return redirect('/relatie/'.$id)->with('succes', 'U heeft succesvol de inkoopfactuur van € '.number_format($totaalbedrag,2)." verwerkt.");


    }

}
