<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Work;
use App\Models\AppointmentType;
use App\Models\Appointment;
use App\Models\History;
use App\Models\Appointment_has_activities;
use DateTime;
use Auth;
use App\Models\Offerte;
use App\Models\Activiteit_has_offerte;
use App\Models\Log;
use App\Models\Status;
use App\Models\Status_has_appointment;
use App\Models\Crediteur;
use App\Models\Klant_has_status;
use App\Models\Afspraak_has_geschiedenis;
use App\Models\Activiteit_has_materiaal;
use App\Models\Voorraad;
use App\Models\Worklist_has_appointments;
use App\Models\Werkdag;
use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;


class AfspraakController extends Controller
{

    public function refresh(){
        // $klanten = Klant::all();

        // $shas = Status_has_afspraak::all();
        // foreach ($shas as $sha) {
        //     if($sha->werkdag_id == 93){
        //         $sha->delete();
        //     }
        // }

        // foreach($klanten as $relatie){
        //     $alleafspraken = Status_has_afspraak::with('afspraak', 'status')->whereHas('afspraak', function($afd) use ($relatie)
        //     {
        //             $afd->where('klant_id', $relatie->id)->groupBy('status_id');
        //     })->orderBy('id', 'desc')->orderBy('created_at', 'desc')->get();
        //     foreach($alleafspraken as $aa){
        //         $date = date('Y-m-d', strtotime($aa->afspraak->startdatum));
        //         if($date == '1970-01-01'){
        //             echo $aa->afspraak->startdatum.'<br>';
        //             echo $date.'<br>';
        //             dd($aa);
        //         }
        //         if($aa->werkdag_id == null){
        //             $aa->werkdag_id = Werkdag::createNew($date);
        //             $aa->save();
        //         }
        //     }
        // }
        foreach(Appointment::where('startdatum', '>', date('Y-m-d', strtotime('2018-05-13 00:00')) )->get() as $a){
            if(!Status_has_appointment::where('afspraak_id', $a->id)->exists()){
                Status_has_appointment::setSha($a->id, 1, "Automatisch gegenereerd en derhalve niet door een gebruiker ondertekend", date('Y-m-d', strtotime($a->startdatum)));
            }
        }
        echo 'DONE';
        // return redirect('/')->with('danger', 'Ik weet niet hoe u aan deze link komt, maar deze geheime link is niet beschikbaar voor u!!');
    }




        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
    *   Create view for appointment
    */
    public function create($id)
    {
        $relatie = Client::findOrFail($id);
        $types = AppointmentType::where('omschrijving', '!=', 'Uitgestelde Betaling')->where('omschrijving', '!=', 'Betalingsherinnering')->get();
        $werknemers = Employee::where('actief', 1)->get();
        $activiteiten = Work::where('actief', 1)->get();

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.maken', compact('relatie', 'types', 'werknemers', 'activiteiten', 'page', 'sub'));
    }

    /*
    *   Create view for appointment from terugbellijst
    */
    public function create2($id, $afspraak_id)
    {
        $relatie = Client::findOrFail($id);
        $types = AppointmentType::where('omschrijving', '!=', 'Uitgestelde Betaling')->where('omschrijving', '!=', 'Betalingsherinnering')->get();
        $werknemers = Employee::where('actief', 1)->get();
        $activiteiten = Work::where('actief', 1)->get();
        $oude_afspraak = Appointment::find($afspraak_id);
        $page = "relatie";
        $sub = "relover";

        return view('afspraak.maken', compact('relatie', 'types', 'werknemers', 'activiteiten', 'page', 'sub', 'afspraak_id', 'oude_afspraak'));
    }

    /*
    *   Store appointment, set appointmentcontent and redirect to right view
    */
    public function store(){
    	$input = Request::all();
        $interval = Appointment::getDateDifferenceWoApp($input['startdatum'], $input['einddatum']);
    	if( $interval->invert == 1 && $input['radio_demo'] == 2)
        {
            return redirect('/afspraak/maken/'.$input['klant_id'])->with('danger', 'Startdatum moet voor de einddatum liggen.');
        }
    	$afspraak = new Appointment;
    	//save part one
    	$afspraak = $afspraak->saveAppoinmentPartOne($input);
    	//if customer is a lead/prospect change to customer
    	Client::changeType($afspraak->klant_id);
    	// set status for appointment
        $interval = $afspraak->getDateDifference();
        // dd($interval);
        if($interval->d > 0 && $afspraak->afspraaktype_id == 1){
            for ($i=0; $i <= $interval->d; $i++) {
                // echo $afspraak->startdatum.' startdatum <br>';
                $date = $afspraak->returnAddedDate($i);
                // echo $date.'nieuwe datum<br>';
                if(date('N', strtotime($date)) < 6){
                    Status_has_appointment::setSha($afspraak->id, 1, 'heeft deze afspraak toegevoegd', date('Y-m-d', strtotime($date)));
                    // echo 'status_toegevoegd voor datum: '.$date;
                    // echo '<br><em> voor debugging N = '.date('N', strtotime($date)).'</em><br>';
                }
            }
            // dd('Klaar');
        } else{
    	   Status_has_appointment::setSha($afspraak->id, 1, 'heeft deze afspraak toegevoegd', date('Y-m-d', strtotime($afspraak->startdatum)));
    	}
        // create log
    	Log::createLog(Auth::user()->id, $afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft een afspraak op: ".$afspraak->startdatum." -- ".$afspraak->einddatum.", toegevoegd.");
    	// set customer status
    	Klant_has_status::changeCustomerStatus($afspraak->klant_id);
    	//create history rule
        $werknemerTest = Employee::find($input['geschreven_door']);
        if(!$werknemerTest){
            $werknemerTest = Employee::find(Auth::user()->werknemer_id);
        }
    	History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, Auth::user()->name." heeft een afspraak gemaakt met klant op ".$afspraak->returnDate($afspraak->startdatum)." --- ".$afspraak->returnDate($afspraak->einddatum)." Deze afspraak is geschreven door: ".$werknemerTest->voornaam, "Afspraak");
    	//defining Next Step
    	$output = $afspraak->defineNextCreateStep($input);
        switch ($afspraak->afspraaktype_id) {
            case '1':
                if(isset($input['werkzaamheden'])){
                    $bool = $afspraak->sameActivities($afspraak->afspraak_id);
                    return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Afspraak succesvol verwerkt.');
                }
                return redirect('/afspraak/maken/'.$afspraak->klant_id.'/'.$afspraak->id.'/werkzaamheden');
                break;

            case '2':
                return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Vrijblijvende afspraak is ingepland.');
                break;

            case '3':
                if(isset($input['werkzaamheden'])){
                    // $bool = $afspraak->sameActivities($afspraak->afspraak_id);
                    return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Afspraak succesvol verwerkt.');
                }
                return view('afspraak.UitgesteldeBetaling.selectAfspraak', compact($output));
                break;

            case '4':
                $page = $output[4];
                $sub = $output[5];
                return view('afspraak.Garantie.selectAfspraak', compact('output', 'page', 'sub'));
                break;

            case '5':
                return view('crediteur.garantiecrediteur', compact($output));
                break;

            case '6':
                # code... // nog bouwen
                break;

            default:
                return redirect('/relatie/'.$afspraak->klant_id)->with('danger', 'Er gaat iets fout, neem contact op met de systeembeheerder.');
                break;
        }
    }

    /*
    *   Type=Uitvoering Werkzaamheden, View too add workactivities
    */
    public function voegwerkzaamhedentoe($id, $afspraak_id){
        $relatie = Client::findOrFail($id);
        $afspraak = Appointment::findOrFail($afspraak_id);
        $activiteiten = Work::where('actief', 1)->get();
        $danger = "";

        $page = "relatie";
        $sub = "relover";
        return view('afspraak.werkzaamheden', compact('relatie', 'afspraak', 'activiteiten','danger', 'page', 'sub'));
    }

    /*
    *   Type=Uitvoering Werkzaamheden, store workactivities, redirect to relation profile
    */
    public function afspraakOpslaan($id, $afspraak_id){
        $input = Request::all();
        foreach($input['aantal'] as $aantal){
            if($aantal <= 0 || $aantal == ""){
                $this->voegwerkzaamhedentoe($id, $afspraak_id);
            }
        }
        $afspraak = Appointment::find($afspraak_id);
        $relatie = Client::find($afspraak->klant_id);
        $output = $afspraak->defineLastCreateStep($input);
        switch ($afspraak->afspraaktype_id) {
            case 1:
                if(is_array($output)){
                    $geschiedenis = $output[0];
                    $page = $output[1];
                    $sub = $output[2];
                    return view('afspraak.addHistory', compact('relatie', 'afspraak', 'geschiedenis', 'page', 'sub'));
                }
                return redirect('/relatie/'.$klant->id)->with('succes', 'De afspraak is toegevoegd aan de relatie');
                break;

            default:
                # code...
                break;
        }

    }

    // delete appointment
    public function delete($id){
        $afspraak = Appointment::find($id);
        $message = '';
        if($afspraak){
            $afspraak->deleteSelf();
            $message = "U heeft de afspraak succesvol verwijderd";
        } else {
            $message = "Afspraak is verwijderd, maar er gaat iets mis. Error:AFSNULL_DELETE_254";
        }
        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', $message);
    }

    //open view to process appointment
    public function process($id){
        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);
        $page = 'relatie';
        $sub = 'relover';
        return view('afspraak.verwerken1', compact('afspraak', 'relatie', 'page', 'sub'));
    }

    //return to start after adding new work
    public function returntoprocess($id){
        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);

        $input = Request::all();
        Work::createSelfFromArray($input);

        $page = "relatie";
        $sub = "relover";
        return view('afspraak.verwerken1', compact('afspraak', 'relatie', 'page', 'sub'));
    }

    // process first view and determine next step in process.
    public function setProcess($id){
        $input = Request::all();
        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);
        if($input['gelukt'] == 0 && $afspraak->afspraaktype_id != 5){
            return redirect('/afspraak/'.$id.'/verwerken/nietgelukt');
        }

        if(Status_has_appointment::where('afspraak_id', $afspraak->id)->count() == 1){ // if multiple days add the first?
            $input['sha_id'] = Status_has_appointment::where('afspraak_id', $afspraak->id)->first();
        }

        $output = $afspraak->setProcessOne($input);
        switch ($afspraak->afspraaktype_id) {
            case 1: // Uitvoering Werkzaamheden
                if(count($output) > 1){ // gelukt, geen extra, wel/niet betaald
                    if(!Status_has_appointment::returnNextStep($input['sha_id'], $afspraak->id)){ // if its about a chain of appointments
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt');
                    }
                    if($output[0] == 1){ //wel betaald
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    //niet betaald
                    $page = 'relatie';
                    $sub = 'relover';
                    $actperafs = Appointment_has_activities::aha($afspraak->id, $afspraak->klant_id);
                    $nieuweafspraken = $afspraak->getAllReferences();
                    $subtotal = 0.00;
                    $types = AppointmentType::all();
                    if(!$nieuweafspraken->isEmpty()){
                        foreach($nieuweafspraken as $nas){
                            $subtotal += $nas->totaalbedrag;
                        }
                    } else {
                        $nieuweafspraken = "leeg";
                    }
                    return view('afspraak.uitvoerenWerkzaamheden.setBetaalAfspraak', compact('relatie', 'afspraak', 'types', 'page', 'sub', 'nieuweafspraken', 'actperafs', 'subtotal'));
                }
                // extra werk
                $activiteiten = Work::where('actief', 1)->get();
                $ahas = Appointment_has_activities::with('activiteit')->where('afspraak_id', $afspraak->id)->get();
                $page = 'relatie';
                $sub = 'relover';
                $danger = '';
                $extra = $input['extra'];
                $voldaan = $input['voldaan'];
                $sha_id = isset($input['sha_id']) ? $input['sha_id'] : -1;
                // dd($voldaan);
                return view('afspraak.uitvoerenWerkzaamheden.extraWerk', compact('danger','afspraak', 'relatie', 'extra', 'voldaan', 'activiteiten', 'ahas', 'page', 'sub', 'sha_id'));
                break;

            case 2: // Offerte Opstellen
                if(count($output) > 1){ // geen extra werk
                    if(!Status_has_appointment::returnNextStep($input['sha_id'], $afspraak->id)){
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    if($output[0] == 1){ // er is een nieuwe afspraak uitgekomen, er hoeft geen offerte op gesteld te worden, maar direct een afspraak met de werkzaamheden (misschien deze automatisch koppelen aan een offerte)

                        $types = AppointmentType::where('omschrijving', '!=', 'Uitgestelde Betaling')->where('omschrijving', '!=', 'Betalingsherinnering')->get();
                        $werknemers = Employee::where('actief', 1)->get();
                        $page = "relatie";
                        $sub = "relover";
                        return view('afspraak.maken', compact('relatie', 'types', 'werknemers', 'page', 'sub'));

                    } else { // offerte opstellen
                        $afspraken = Appointment::where('id', $afspraak->id)->get();
                        $werknemers = Employee::where('actief', 1)->get();
                        $page = 'financieel';
                        $sub = 'offerte';
                        return view('offerte.nieuw', compact('relatie', 'afspraken', 'afspraak', 'page', 'sub', 'werknemers'));
                    }
                } else{ // er is extra werk, offerte is niet meer relevant. Werkzaamheden toevoegen en voldaan verwerken
                    $activiteiten = Work::where('actief', 1)->get();
                    $extra = $input['extra'];
                    $page = '';
                    $sub = '';
                    return view('afspraak.OfferteOpstellen.extrawerk', compact('afspraak', 'activiteiten', 'extra', 'page', 'sub'));
                }
                break;
            case 3: // Uitgestelde betaling
                if(count($output) > 1){
                    if(!Status_has_appointment::returnNextStep($input['sha_id'], $afspraak->id)){
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    if($output[0] == 1){ // geen extra, wel betaald
                        return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    //geen extra niet betaald
                    $page = "relatie";
                    $sub = "relover";
                    return view('afspraak.UitgesteldeBetaling.nietgelukt', compact('afspraak', 'relatie', 'page', 'sub'));
                }
                // extra werk
                $activiteiten = Work::where('actief', 1)->get();
                $page = 'relatie';
                $sub = 'relover';
                $danger = '';
                $extra = $input['extra'];
                $voldaan = $input['voldaan'];
                return view('afspraak.UitgesteldeBetaling.extraWerk', compact('danger','afspraak', 'relatie', 'extra', 'voldaan', 'activiteiten', 'page', 'sub'));
                break;

            case 4: // Garantie
                if(count($output) > 1){// gelukt, geen extra, wel/niet voldaan
                    if(!Status_has_appointment::returnNextStep($input['sha_id'], $afspraak->id)){
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    if($output[0] == 1){ //garantie is opgelost
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    //niet opgelost
                    $afspraak->setProcessThree(true);
                    $page = "relatie";
                    $sub = "relover";
                    return view('afspraak.Garantie.garantie2', compact('afspraak', 'relatie', 'page', 'sub'));
                }
                //extra werk
                $activiteiten = Work::where('actief', 1)->get();
                $ahas = Appointment_has_activities::with('activiteit')->where('afspraak_id', $afspraak->id)->get();
                $page = 'relatie';
                $sub = 'relover';
                $danger = '';
                $extra = $input['extra'];
                $voldaan = $input['voldaan'];
                return view('afspraak.uitvoerenWerkzaamheden.extraWerk', compact('danger','afspraak', 'relatie', 'extra', 'voldaan', 'activiteiten', 'ahas', 'page', 'sub'));
                break;

            case 5: // Betalingsherinnering
                if(count($output) > 1){
                    if($output[0] == 0){
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Betalingsherinnering aangepast.');
                    }
                    $bool = $afspraak->setProcessTwo($input, $input['bank']);
                    return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Betalingsherinnering nagekomen, oude afspraak betaald.');
                } else {
                    //nog bouwen
                }
                break;

            case 6: // Terugbetaalverzoek
                //nog bouwen
                break;

            default:
                # code...
                break;
        }

    }

    // add history to appointment
    public function saveHistoryToAppointment($id){
        $input = Request::all();
        $afspraak = Appointment::find($input['afspraak_id']);
        Afspraak_has_geschiedenis::createConnection($input, $afspraak);

        return redirect('/relatie/'.$id)->with('succes', 'De afspraak is toegevoegd aan de relatie.');
    }

    public function setNewDefPayment($id){
        $input = Request::all();
        $nieuw = new Appointment;
        $nieuw->afspraaktype_id = $input['afspraaktype_id'];
        $nieuw->saveAppoinmentPartOne($input);
        // if($nieuw->afspraaktype_id == 3){ // ddef payment
            $afspraak = Appointment::find($id); //get old appointment
            $relatie = Client::find($afspraak->klant_id);
            $output = $afspraak->setProcessTwo($input, $nieuw);
            if(is_array($output)){ // output is array, there is a need to add more appointments
                if(count($output) == 1){
                    $page = 'relatie';
                    $sub = 'relover';
                    $actperafs = Appointment_has_activities::aha($afspraak->id, $afspraak->klant_id);
                    $nieuweafspraken = $afspraak->getAllReferences();
                    $subtotal = 0.00;
                    $types = AppointmentType::all();
                    if(!$nieuweafspraken->isEmpty()){
                        foreach($nieuweafspraken as $nas){
                            if($nas->afspraaktype_id == 3)
                                $subtotal += $nas->totaalbedrag;
                        }
                    } else {
                        $nieuweafspraken = "leeg";
                    }
                    $tweede = true;
                    return view('afspraak.uitvoerenWerkzaamheden.setBetaalAfspraak', compact('relatie', 'afspraak', 'types', 'page', 'sub', 'nieuweafspraken', 'actperafs', 'subtotal', 'tweede'));
                }
            } elseif(!$output){ // output is false, error in amount usages.
                $page = 'relatie';
                $sub = 'relover';
                $actperafs = Appointment_has_activities::aha($afspraak->id, $afspraak->klant_id);
                $nieuweafspraken = $afspraak->getAllReferences();
                $danger = 'Het betaalde bedrag is hoger of gelijk aan het factuurbedrag, dit kan niet.';
                $subtotal = 0.00;
                $types = AppointmentType::all();
                if(!$nieuweafspraken->isEmpty()){
                    foreach($nieuweafspraken as $nas){
                        $subtotal += $nas->totaalbedrag;
                    }
                } else {
                    $nieuweafspraken = "leeg";
                }
                return view('afspraak.uitvoerenWerkzaamheden.setBetaalAfspraak', compact('danger', 'relatie', 'afspraak', 'types', 'page', 'sub', 'nieuweafspraken', 'actperafs', 'subtotal'));

            } else { // output is true. Paid and to be paid amount are equal to the original amount. There is no need to add extra appointments.
                return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspra(a)k(en) succesvol toegevoegd.');
            }
        // }
    }

    public function setExtraWork($id){
        $input = Request::all();
        // dd($input);
        $afspraak = Appointment::find($id);
        $output = $afspraak->setProcessTwo($input, null);
        // Log::info(print_r($output, true));
        $relatie = Client::find($afspraak->klant_id);
        switch ($afspraak->afspraaktype_id) {
            case 1:
                if(!is_array($output)){
                    if(!Status_has_appointment::returnNextStep($input['sha_id'], $afspraak->id)){
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    if($output){ // if true, appointment has been paid
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    //niet betaald
                    $page = 'relatie';
                    $sub = 'relover';
                    $actperafs = Appointment_has_activities::aha($afspraak->id, $afspraak->klant_id);
                    $nieuweafspraken = $afspraak->getAllReferences();
                    $subtotal = 0.00;
                    $types = AppointmentType::all();
                    if(!$nieuweafspraken->isEmpty()){
                        foreach($nieuweafspraken as $nas){
                            $subtotal += $nas->totaalbedrag;
                        }
                    } else {
                        $nieuweafspraken = "leeg";
                    }
                    return view('afspraak.uitvoerenWerkzaamheden.setBetaalAfspraak', compact('relatie', 'afspraak', 'types', 'page', 'sub', 'nieuweafspraken', 'actperafs', 'subtotal'));
                }
                break;
            case 2:
                if(!isset($input['sha_id']))                {
                    $input['sha_id'] = Status_has_appointment::where('afspraak_id' ,$afspraak->id)->first();
                }
                if(!Status_has_appointment::returnNextStep($input['sha_id'], $afspraak->id)){
                    return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                }
                if($output){ // appointment is paid.
                    return redirect('/relatie/'.$afspraak->klant_id)->with('success', 'U heeft de afspraak succesvol verwerkt.');
                } else { // new def payment
                    $page = 'relatie';
                    $sub = 'relover';
                    $actperafs = Appointment_has_activities::aha($afspraak->id, $afspraak->klant_id);
                    $nieuweafspraken = $afspraak->getAllReferences();
                    $subtotal = 0.00;
                    $types = AppointmentType::all();
                    if(!$nieuweafspraken->isEmpty()){
                        foreach($nieuweafspraken as $nas){
                            $subtotal += $nas->totaalbedrag;
                        }
                    } else {
                        $nieuweafspraken = "leeg";
                    }
                    return view('afspraak.uitvoerenWerkzaamheden.setBetaalAfspraak', compact('relatie', 'afspraak', 'types', 'page', 'sub', 'nieuweafspraken', 'actperafs', 'subtotal'));
                }
                break;
            case 3:
                if(!is_array($output)){
                    if(!Status_has_appointment::returnNextStep($input['sha_id'], $afspraak->id)){
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    if($output){ //extra werk betaald
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    //niet betaald
                    $page = 'relatie';
                    $sub = 'relover';
                    $actperafs = Appointment_has_activities::aha($afspraak->id, $afspraak->klant_id);
                    $nieuweafspraken = $afspraak->getAllReferences();
                    $subtotal = 0.00;
                    $types = AppointmentType::all();
                    if(!$nieuweafspraken->isEmpty()){
                        foreach($nieuweafspraken as $nas){
                            $subtotal += $nas->totaalbedrag;
                        }
                    } else {
                        $nieuweafspraken = "leeg";
                    }
                    return view('afspraak.uitvoerenWerkzaamheden.setBetaalAfspraak', compact('relatie', 'afspraak', 'types', 'page', 'sub', 'nieuweafspraken', 'actperafs', 'subtotal'));
                }
                break;

            case 4:
                if(!is_array($output)){
                    if(!Status_has_appointment::returnNextStep($input['sha_id'], $afspraak->id)){
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    if($output){ // if true, appointment has been paid
                        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
                    }
                    //niet betaald
                    $page = 'relatie';
                    $sub = 'relover';
                    $actperafs = Appointment_has_activities::aha($afspraak->id, $afspraak->klant_id);
                    $nieuweafspraken = $afspraak->getAllReferences();
                    $subtotal = 0.00;
                    $types = AppointmentType::all();
                    if(!$nieuweafspraken->isEmpty()){
                        foreach($nieuweafspraken as $nas){
                            $subtotal += $nas->totaalbedrag;
                        }
                    } else {
                        $nieuweafspraken = "leeg";
                    }
                    return view('afspraak.uitvoerenWerkzaamheden.setBetaalAfspraak', compact('relatie', 'afspraak', 'types', 'page', 'sub', 'nieuweafspraken', 'actperafs', 'subtotal'));
                }
                break;

            case 5:
                # code...
                break;

            case 6:
                # code...
                break;

            default:
                # code...
                break;
        }
    }

    public function endWarranty($id){
        $input = Request::all();
        $afspraak = Appointment::find($id);
        Status_has_appointment::setSha($id, 3, "Afspraak is gelukt, maar niet opgelost.", $afspraak->startdatum);
        $app = $input['app'];
        $credit = $input['credit'];
        $relatie = Client::find($afspraak->klant_id);


        if($app == 0 && $credit == 0){ // no credit and no new appointment
            return redirect('relatie/'.$relatie->id)->with('succes', 'U heeft de garantie verwerkt.');
        }

        $output = $afspraak->setProcessFour($input);

        if(is_array($output)){ // both are true, appointment to pay cash money back
            $types = AppointmentType::where('omschrijving', 'Terugbetaalverzoek')->get();
            $page = 'relatie';
            $sub = 'relover';
            return view('afspraak.Garantie.setTerugbetaalAfspraak', compact('afspraak', 'relatie', 'types', 'page', 'sub'));
        }
        if($output){ // if true, new warranty appointment
            $types = AppointmentType::where('omschrijving', 'Garantie')->get();
            $afspraak_id = $afspraak->id;
            $page = "relatie";
            $sub = "relover";
            return view('afspraak.maken', compact('afspraak_id', 'relatie', 'types', 'page', 'sub'));
        } else {
            //else new credit appointment
            $page = "relatie";
            $sub = "relover";

            return view('crediteur.garantiecrediteur', compact('relatie', 'afspraak', 'page', 'sub'));
        }
    }

    public function setCashRepay($id){
        $input = Request::all();
        $old_appointment = Appointment::find($id);
        $relatie = Client::find($old_appointment->klant_id);

        $afspraak = new Appointment;
        //save part one
        $afspraak = $afspraak->saveAppoinmentPartOne($input);
        // set status for appointment
        Status_has_appointment::setSha($afspraak->id, 1, 'heeft deze Terugbetaalverzoek toegevoegd', $afspraak->startdatum);
        // create log
        Log::createLog(Auth::user()->id, $afspraak->klant_id, 'Crediteur', Auth::user()->name." heeft een terugbetaalverzoek op: ".$afspraak->startdatum." -- ".$afspraak->einddatum.", toegevoegd.");
        //create history rule
        History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, Auth::user()->name." heeft een terugbetaalverzoek gemaakt met klant op ".$afspraak->returnDate($afspraak->startdatum)." --- ".$afspraak->returnDate($afspraak->einddatum), "Afspraak");

        return redirect('relatie/'.$relatie->id)->with('succes', 'U heeft succesvol een terugbetaalverzoek aangemaakt.');

    }

    public function defPaymentFailed($id){
        $input = Request::all();
        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);
        $afspraak->eigenschuld = (isset($input['eigenschuld'])) ? 1 : 0;
        $afspraak->reden = $input['reden'];
        $afspraak->save();
        $types = AppointmentType::where('omschrijving', 'Betalingsherinnering')->orWhere('omschrijving', 'Uitgestelde betaling')->get();

        $page = 'relatie';
        $sub = 'relover';
        return view('afspraak.UitgesteldeBetaling.setNieuwBetaalAfspraak', compact('relatie', 'afspraak', 'types', 'page', 'sub'));
    }

    public function cancelApp($id)
    {
        $afspraak = Appointment::find($id);
        $klant = Client::find($afspraak->klant_id);
        Log::createLog(Auth::user()->id, $afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft de afspraak ".$afspraak->id." van ".$klant->achternaam.", ".$klant->postcode." ".$klant->huisnummer.", "."geannuleerd.");
        History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, Auth::user()->name." heeft afspraak ".$afspraak->id." van ".$klant->achternaam.", ".$klant->postcode." ".$klant->huisnummer.", "."geannuleerd.", 'Afspraak');
        Status_has_appointment::setSha($afspraak->id, 23, Auth::user()->name." heeft de afspraak geannuleerd.", $afspraak->startdatum);
        Worklist_has_appointments::deleteAppFromWorklist($afspraak->id);

        $afspraak->eigenschuld = 1;
        $afspraak->save();

        return redirect('/relatie/'.$klant->id)->with('succes', 'U heeft de afspraak succesvol geannuleerd.');
    }

    public function cancelAppFromStatus($id, $date){
        $afspraak = Appointment::find($id);
        $klant = Client::find($afspraak->klant_id);
        Log::createLog(Auth::user()->id, $afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft de afspraak ".$afspraak->id." van ".$klant->achternaam.", ".$klant->postcode." ".$klant->huisnummer.", "."geannuleerd.");
        History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, Auth::user()->name." heeft afspraak ".$afspraak->id." van ".$klant->achternaam.", ".$klant->postcode." ".$klant->huisnummer.", "."geannuleerd.", 'Afspraak');
        $wd = Werkdag::where('datum', date('Y-m-d', strtotime($date)))->first();
        // dd($wd);
        Status_has_appointment::setSha($afspraak->id, 23, Auth::user()->name." heeft de afspraak geannuleerd.", $wd->datum);
        // Werklijst_has_afspraak::deleteAppFromWorklist($afspraak->id); Deze nog even bekijken.

        $afspraak->eigenschuld = 1;
        $afspraak->save();

        return redirect('/relatie/'.$klant->id)->with('succes', 'U heeft de afspraak succesvol geannuleerd.');
    }

    public function editAppointment($id)
    {
        $afspraak = Appointment::find($id);
        $shas = Status_has_appointment::returnShas($id);
        if(count($shas) > 0){
            foreach ($shas as $sha) {
                if($sha->status_id > 1){
                    return redirect('/relatie/'.$afspraak->klant_id)->with('danger', 'De afspraak kan niet aangepast worden, daar (een van) deze afspra(a)k(en) al verwerkt is/zijn.');
                }
            }
        }
        $afspraak->afspraaktype_id = AppointmentType::find($afspraak->afspraaktype_id);
        $afspraak->klant_id = Client::find($afspraak->klant_id);
        $user = User::find(Auth::user()->id);
        $werknemers = Employee::where('actief', 1)->get();
        $types = AppointmentType::where('omschrijving', '!=', 'Uitgestelde Betaling')->where('omschrijving', '!=', 'Betalingsherinnering')->get();

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.edit', compact('afspraak', 'user', 'werknemers', 'types', 'page', 'sub'));
    }

    public function storeAppointment($id) // from edit
    {
        $input = Request::all();
        $afspraak = Appointment::find($id);
        $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
        $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
        if($strtotime->getTimestamp() >= $strtotimee->getTimestamp() && $input['radio_demo'] == 2)
        {
            return redirect('/afspraak/'.$id.'/wijzigen')->with('danger', 'Startdatum moet voor de einddatum liggen.');
        }

        if(Status_has_appointment::where('afspraak_id', $afspraak->id)->first()->status_id > 1){
           return redirect('/relatie/'.$afspraak->klant_id)->with('danger', 'Deze afspraak is al verwerkt en kan derhalve niet meer aangepast worden.');
       }
        Worklist_has_appointments::deleteAppFromWorklist($afspraak->id);

        $old_date = date('Y-m-d', strtotime($afspraak->startdatum));
        // echo $old_date; echo '<br>';
        if($old_date != date('Y-m-d', strtotime($strtotime->getTimestamp()))){

            $wd = Werkdag::where('datum', date('Y-m-d', strtotime($strtotime->getTimestamp())))->first();

            $wd_old = Werkdag::where('datum', date('Y-m-d', strtotime($old_date)))->first();
            // echo $wd_old; echo '<br>';
            if(!$wd_old){
                // echo  'WD_OLD<br>';
                $wd_old = Werkdag::createNew($old_date);
                $wd_old = Werkdag::where('datum', date('Y-m-d', strtotime($old_date)))->first();
            }
            if(!$wd){
                // echo  date('Y-m-d', $strtotime->getTimestamp()).' WD_NEW<br>';

                $wd = Werkdag::createNew(date('Y-m-d', $strtotime->getTimestamp()));
                $wd = Werkdag::where('datum', date('Y-m-d', $strtotime->getTimestamp()))->first();
            }
            // dd($afspraak);

                //if interval > 0 we need to edit multiple dates
            if(date_diff($strtotime, $strtotimee)->d >= 1 && $input['radio_demo'] == 2){
                $shas = Status_has_appointment::where('afspraak_id', $afspraak->id)->get();
                $firstdate = date('Y-m-d', $strtotime->getTimestamp());
                foreach($shas as $sha){
                 $sha->delete();
                }
                $tempDate = $strtotime;
                // dd($strtotime->modify("+2 days"));
                // dd(date_diff($strtotime, $strtotimee)->d + 1);
                $amountOfTimes = date_diff($strtotime, $strtotimee)->d + 1;
                //+1 because including start day itself, so 18 till 22 -> 18, 19, 20, 21, 22. ->d = 4 but ++ -> 5
                for($i = 0; $i < $amountOfTimes; $i++){
                    $sha = new Status_has_appointment;
                    $sha->status_id = 1;
                    $sha->afspraak_id = $afspraak->id;
                    $sha->omschrijving = Auth::user()->name.' heeft de afspraak aangepast';
                    $tempDate = ($i==0)? $strtotime: $strtotime->modify("+1 days");
                    // var_dump($strtotime);
                    // echo "<br> I: ".$i."To use date: ".$tempDate->date."<br>";
                    $sha->werkdag_id = Werkdag::createNew(date('Y-m-d', $tempDate->getTimestamp()));
                    $sha->save();
                }
                // dd('end>');
            } else{
                $sha = Status_has_appointment::where('afspraak_id', $afspraak->id)->where('werkdag_id', $wd_old->id)->first();
                if($sha){
                    $sha->werkdag_id = $wd->id;
                    $sha->save();
                } else {
                    // echo 'sha is leeg';
                }
            }
        }

        $afspraak = $afspraak->saveAppoinmentPartOne($input);
        $afspraak->bijgewerkt_door = Auth::user()->id;
        $afspraak->save();

        History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, Auth::user()->name." heeft de afspraak gewijzigd.", 'Afspraak');
        Log::createLog(Auth::user()->id, $afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft de afspraak gewijzigd van klant: ".$afspraak->klant->postcode.$afspraak->klant->huisnummer);

        if(isset($input['geschiedenis']))
        {
            $geschiedenis = true;
        }
        else
        {
            $geschiedenis = false;
        }

        if(isset($input['werkzaamheden']))
        {
            $afspraak = Appointment::find($id);
            $prevWorks = Appointment_has_activities::with('activiteit')->where('afspraak_id', $id)->get();
            $relatie = Client::find($afspraak->klant_id);
            $activiteiten = Work::where('actief', 1)->get();
            $danger = "";

            $page = "relatie";
            $sub = "relover";
            return view('afspraak.editWork', compact('afspraak', 'relatie', 'activiteiten', 'danger', 'geschiedenis', 'prevWorks', 'page', 'sub'));
        }
        elseif(isset($input['geschiedenis']) && $input['afspraaktype_id'] == 1)
        {
            $afspraak = Appointment::find($id);
            $relatie = Client::find($afspraak->klant_id);
            if(History::where('klant_id', $relatie->id)->where('type', 'werkzaamheden')->exists())
            {
                $geschiedenis = History::where('klant_id', $relatie->id)->where('afspraak_id', '!=', $afspraak->id)->get();
                foreach($geschiedenis as $g){
                    if($g->afspraak_id == $afspraak->id){
                        unset($g);
                    }
                }
                $page = "relatie";
                $sub = "relover";
                return view('afspraak.addHistory', compact('relatie', 'afspraak', 'geschiedenis', 'page', 'sub'));
            }
        }
        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol gewijzigd.');
    }

    public function storeWork($id)
    {
        $input = Request::all();
        $afspraak = Appointment::find($id);
        $klusprijs;
        if(isset($input['klusprijsAan'])){
            $klusprijs = true;
            $afspraak->setKlusprijs();
            $afspraak->setTotal($input['klusprijs']);
        } else {
            $klusprijs = false;
        }

        $ahas = Appointment_has_activities::where('afspraak_id', $id)->get();
        foreach ($ahas as $aha) {
            $aha->delete();
        }
        Appointment_has_activities::addActivityToAppointment($input, $id, $klusprijs);


        $afspraak->save();

        if($afspraak->hasNewDefPayments()){ //if someone already set new defered payments, we need to delete them.
            $afspraak->deleteNewDefPayments();
        }

        if($input['geschiedenis'])
        {
            $geschiedenis = $input['geschiedenis'];
            $afspraak = Appointment::find($id);
            $relatie = Client::find($afspraak->klant_id);

            if(History::where('klant_id', $relatie->id)->where('afspraak_id', '!=', $afspraak->id)->count() > 0)
            {
                $geschiedenis = History::where('klant_id', $relatie->id)->where('afspraak_id', '!=', $afspraak->id)->get();
                $update = true;

                $page = "relatie";
                $sub = "relover";
                return view('afspraak.addHistory', compact('relatie', 'afspraak', 'geschiedenis', 'update', 'page', 'sub'));
            }
        }

        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'De afspraak is gewijzigd.');

    }


    public function addNewWork($id)
    {
        $cont = "new";
        $afspraak = Appointment::find($id);

        $page = "relatie";
        $sub = "relover";
        return view('activiteit.toevoegen', compact('page', 'afspraak', 'cont', 'sub'));
    }

    public function addNewWorkToExisting($id)
    {
        $cont = "edit";
        $afspraak = Appointment::find($id);

        $page = "relatie";
        $sub = "relover";
        return view('activiteit.toevoegen', compact('page', 'afspraak', 'cont', 'sub'));
    }

    public function saveWorkToExisting($id)
    {

        $input = Request::all();


        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);
        $activiteiten = Work::where('actief', 1)->get();
        $danger = "";

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.editWork', compact('afspraak', 'relatie', 'activiteiten', 'danger', 'geschiedenis', 'page', 'sub'));
    }

    public function saveWork($id)
    {

        $input = Request::all();
        Work::createSelfFromArray($input);

        $afspraak = Appointment::find($id);
        $relatie = Client::findOrFail($afspraak->klant_id);
        $activiteiten = Work::where('actief', 1)->get();
        $danger = "";

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.werkzaamheden', compact('relatie', 'afspraak', 'activiteiten','danger', 'page', 'sub'));
    }

    public function veranderEigenschuld($afspraak_id)
    {
        $afspraak = Appointment::find($afspraak_id);
        $afspraak->changeFault();
        Log::createLog(Auth::user()->id, Appointment::find($afspraak_id)->klant_id, '€15,- klant', Auth::user()->name." heeft afspraak ".$afspraak->id." gemarkeerd als 'geen €15,- klant'.");

        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Status succesvol geüpdatet!');
    }

    //set Fault to own fault so it wont appear in the fifteen euro list anymore.
    public function veranderEigenschuldvijftien($afspraak_id)
    {
        $afspraak = Appointment::find($afspraak_id);
        // print_r($afspraak);
        $check = $afspraak->changeSetFault(1);
        // dd($afspraak);
        Log::createLog(Auth::user()->id, Appointment::find($afspraak_id)->klant_id, '€15,- klant', Auth::user()->name." heeft afspraak ".$afspraak->id." gemarkeerd als 'geen €15,- klant'.");

        return redirect('/vijftieneuro')->with('succes', 'Status succesvol geüpdatet!');
    }

    # Vegers gedeelte

    public function start($werklijst_id, $afspraak_id)
    {
        $afspraak = Appointment::find($afspraak_id);
        $relatie = Client::find($afspraak->klant_id);
        $wha = Worklist_has_appointments::where('werklijst_id', $werklijst_id)->where('afspraak_id', $afspraak_id)->first();
        if($wha->start != null)
        {
            $wha->start = date('Y-m-d H:i');
            $wha->save();
        }

        $page = "planning";
        $sub = "verkvand";

        return view('afspraak.verwerken1', compact('afspraak', 'relatie', 'page', 'sub'));
    }

    #end Vegers gedeelte

    public function notNeeded($id)
    {
        $afspraak = Appointment::find($id);
        Status_has_appointment::setSha($afspraak->id, 30, Auth::user()->name." heeft de klant verwerkt: Er is geen nieuwe afspraak nodig.", date('Y-m-d', strtotime($afspraak->startdatum)));
        History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, Auth::user()->name.": Er is geen nieuwe afspraak nodig.", 'Terugbellijst');

        return redirect('/terugbellijst')->with('succes', 'Wijziging opgeslagen, relatie uit terugbellijst gehaald.');
    }

    public function notNeededProfile($id)
    {
        $afspraak = Appointment::find($id);
        Status_has_appointment::setSha($afspraak->id, 30, Auth::user()->name." heeft de klant verwerkt: Er is geen nieuwe afspraak meer nodig.", date('Y-m-d', strtotime($afspraak->startdatum)));
        History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, Auth::user()->name.": Er is geen nieuwe afspraak nodig.", 'Afspraak');

        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'Wijziging opgeslagen en verwerkt.');
    }

    public function setOfferApp($id)
    {
        $input = Request::all();

        $afspraak = new Appointment;
        $afspraak->afspraak_id = $id;
        $afspraak = $afspraak->saveAppoinmentPartOne($input);


        Log::createLog(Auth::user()->id, $afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft een afspraak op: ".$afspraak->startdatum." -- ".$afspraak->einddatum.", toegevoegd.");
        Status_has_appointment::setSha($afspraak->id, 1, Auth::user()->name." heeft deze afspraak toegevoegd.", $afspraak->startdatum);
        History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id,
            Auth::user()->name." heeft een afspraak gemaakt met klant op ".$afspraak->returnDate($afspraak->startdatum)." --- ".$afspraak->returnDate($afspraak->einddatum), 'Afspraak');

        if(isset($input['notNull'])) // Als er een offerte is, kan er hier automatisch de werkzaamheden toegevoegd worden.
        {
            if(isset($input['klusprijsAan'])){
                $klusprijs = true;
                $afspraak->setKlusprijs();
                $afspraak->setTotal($input['klusprijs']);
            } else {
                $klusprijs = false;
            }

            Activiteit_has_offerte::setToAppointment($id, $afspraak->id, $input);

            return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
        }

        return redirect('/afspraak/maken/'.$input['klant_id'].'/'.$afspraak->id.'/werkzaamheden');
    }

    /*
    *   Store warranty appointment
    */
    public function setWarranty($id)
    {
        $input = Request::all();
        //dd($input);
        $afspraak = Appointment::find($id);
        if(!isset($input['afspraken'])){
            $klant_id = $afspraak->klant_id;
            $afspraak->delete();
            return redirect('/relatie/'.$klant_id)->with('danger', 'Er zijn geen afspraken om een garantieafspraak mee te kunnen maken. Er gaat iets fout');
        } else {
            $afspraak->defineLastCreateStep($input);
        }

        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'U heeft de garantie afspraak succesvol aangemaakt.');
    }

    /*
    *   Method for clicking warranty per appointment  CHECK IF STILL NEEDED -----------------------------------------------------------------------------------------------------------!!!!!!!!!
    */
    public function idWarrenty($id)
    {
        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);
        $types = AppointmentType::where('omschrijving', 'Garantie')->get();
        $werknemers = Employee::where('actief', 1)->get();

        $page = "relatie";
        $sub = "relover";

        return view('afspraak.setWarrenty', compact('relatie','types','werknemers', 'afspraak', 'page', 'sub'));
    }

    /*
    *   If appointment failed, the failed view will open.
    */
    public function nietGelukt($id)
    {

        $afspraak = Appointment::findOrFail($id);
        $relatie = Client::findOrFail($afspraak->klant_id);

        $page = "relatie";
        $sub = "relover";
        return view('afspraak.nietgelukt', compact('afspraak', 'relatie', 'page', 'sub'));
    }

    /*
    *   Store failed appointment, als write too log and history
    *   if there is a new appointment, it redirects to new appointment
    *   else to relation profile
    */
    public function nietgelukteind($id)
    {
        $input = Request::all();
        //dd(isset($input['afspraak']));
        $afspraak = Appointment::findOrFail($id);
        $relatie = Client::findOrFail($afspraak->klant_id);
        if(isset($input['eigenschuld']))
        {
            $afspraak->eigenschuld = $input['eigenschuld'];
        }
        else
        {
            $afspraak->eigenschuld = 0;
        }
        if($afspraak->afspraaktype_id == 3 || $afspraak->afspraaktype_id == 5)
        {
            $ubsetAmount = true;
            $types = AppointmentType::where('omschrijving', 'Betalingsherinnering')->orWhere('omschrijving', 'Uitgestelde betaling')->get();
            $afspraak->save();
            $afspraak = Appointment::findOrFail($id);
            if($afspraak->getDateDifference()->d > 0){
                if(isset($input['sha_id'])){
                    Status_has_appointment::editSelfWithID($input['sha_id'], (isset($input['afspraak']) ? 29 : 2), Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt en het bedrag is niet opgehaald.');
                    $sha = Status_has_appointment::find($input['sha_id'])->werkdag_id;
                    if($sha == null){
                        $afspraak->reden = $afspraak->reden.' '.$input['reden'];
                    } else {
                        $afspraak->reden = $afspraak->reden.' '.Werkdag::returndate($sha).': '.$input['reden'];
                    }
                } else {
                    Status_has_appointment::editSelf($afspraak->id, (isset($input['afspraak']) ? 29 : 2), Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt en het bedrag is niet opgehaald.');

                    $afspraak->reden = $afspraak->reden.' '.$input['reden'];
                }

            } else {
                Status_has_appointment::setSha($afspraak->id, (isset($input['afspraak']) ? 29 : 2), Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt en het bedrag is niet opgehaald.', $afspraak->startdatum);
                $afspraak->reden = $input['reden'];
            }
            $afspraak->save();
            Log::createLog(Auth::user()->id, $afspraak->klant_id, 'Afspraak', Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt en het bedrag is niet opgehaald met reden: '.$afspraak->reden);
            History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt en het bedrag is niet opgehaald met reden: '.$afspraak->reden, 'Afspraak');
            $page = "relatie";
            $sub = "relover";
            if(isset($input['afspraak']))
               return view('afspraak.UitgesteldeBetaling.setNieuwBetaalAfspraak', compact('ubsetAmount', 'relatie', 'afspraak', 'types', 'page', 'sub'));
            return redirect('relatie/'.$relatie->id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
        }

        if($afspraak->getDateDifference()->d > 0){
            if(isset($input['sha_id'])){
                Status_has_appointment::editSelfWithID($input['sha_id'], (isset($input['afspraak']) ? 29 : 2), Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt.');
                $sha = Status_has_appointment::find($input['sha_id'])->werkdag_id;
                if($sha == null){
                    $afspraak->reden = $afspraak->reden.' '.$input['reden'];
                } else {
                    $afspraak->reden = $afspraak->reden.' '.Werkdag::returndate($sha).': '.$input['reden'];
                }
            } else {
                $statuss = 2;
                if(isset($input['notInTb'])){ $statuss = 30; }
                elseif(isset($input['afspraak'])){ $statuss = 29; }
                Status_has_appointment::editSelf($afspraak->id, $statuss, Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt.');

                $afspraak->reden = $afspraak->reden.', '.$input['reden'];
            }

        } else {
            $tempStatus = 2;
            if(isset($input['notInTb'])){
                $tempStatus = 30;
            } elseif(isset($input['afspraak'])){
                $tempStatus = 29;
            }


            Status_has_appointment::setSha($afspraak->id, $tempStatus, Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt.', $afspraak->startdatum);
            $afspraak->reden = $input['reden'];
        }
        // Status_has_afspraak::setSha($afspraak->id, (isset($input['afspraak']) ? 29 : 2), Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt.', $afspraak->startdatum);
        if(strlen($input['geschiedenis']) > 0){
            History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, $input['geschiedenis'], 'terugbellijst');
        }
        Log::createLog(Auth::user()->id, $afspraak->klant_id, 'Afspraak', Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt met reden: '.$afspraak->reden);
        History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, Auth::user()->name.' heeft de afspraak verwerkt. Afspraak is niet gelukt met reden: '.$afspraak->reden, 'Afspraak');

        $afspraak->save();
        if(isset($input['afspraak']))
        {
            $types = AppointmentType::where('id', $afspraak->afspraaktype_id)->get();
            $werknemers = Employee::where('actief', 1)->get();
            $afspraak_id = $afspraak->id;
            $page = "relatie";
            $sub = "relover";
            $oude_afspraak = $afspraak;
            return view('afspraak.maken', compact('afspraak_id', 'werknemers', 'relatie', 'types', 'page', 'sub', 'oude_afspraak'));
        }
        else
        {
            return redirect('relatie/'.$relatie->id)->with('succes', 'U heeft de afspraak succesvol verwerkt.');
        }

    }

    public function setAfspraak($id, $proces_id){
        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);
        $afspraak_id = $afspraak->id;
        $page = 'relatie';
        $sub = 'relover';
        if($proces_id == 3){
            $types = AppointmentType::where('omschrijving', 'Betalingsherinnering')->orWhere('omschrijving', 'Uitgestelde betaling')->get();
            return view('afspraak.UitgesteldeBetaling.setNieuwBetaalAfspraak', compact('relatie', 'afspraak', 'types', 'page', 'sub'));
        } else {
            $werknemers = Employee::where('actief', 1)->get();
            if($afspraak->afspraaktype_id == 3 || $afspraak->afspraaktype_id == 5){
                $types = AppointmentType::where('omschrijving', 'Betalingsherinnering')->orWhere('omschrijving', 'Uitgestelde betaling')->get();
            } else {
                // $types = Afspraaktype::where('id', $afspraak->afspraaktype_id)->get();
                $types = AppointmentType::where('omschrijving', '!=', 'Uitgestelde Betaling')->where('omschrijving', '!=', 'Betalingsherinnering')->get();
            }
            $oude_afspraak = $afspraak;
            return view('afspraak.maken', compact('afspraak_id', 'relatie','werknemers', 'page', 'sub', 'types', 'oude_afspraak'));
        }
    }


    public function getOpenDefPayments(){
        $afspraken = Status_has_appointment::with('afspraak', 'status')->where('status_id', 1)->whereHas('afspraak', function($afs){
            $afs->where('afspraaktype_id', 3)->orWhere('afspraaktype_id', 5);
        })->get();

        $statussen = Status::where('id', '<', 5)->orWhere('id', 23)->orWhere('id', 29)->orWhere('id', 30)->get();
        $afspraaktypes = AppointmentType::where('id', 3)->orWhere('id', 5)->get();
        $page = 'financieel';
        $sub = 'openstaande_afspraken';
        return view('afspraak.openstaandeAfspraken', compact('afspraken', 'page', 'sub', 'statussen', 'afspraaktypes'));
    }

    public function getOpenDefPaymentsPerStatus($status){
        if(!is_numeric($status)){
            if($status == 'all'){

                $afspraken = Status_has_appointment::with('afspraak', 'status')->whereHas('afspraak', function($afs) {
                    $afs->where('afspraaktype_id', 3)->orWhere('afspraaktype_id', 5);
                })->get();

            } else {

                $afspraken = Status_has_appointment::with('afspraak', 'status')->where('status_id', 1)->whereHas('afspraak', function($afs) use ($status) {
                    $afs->where('afspraaktype_id', AppointmentType::where('omschrijving', $status)->first()->id);
                })->get();

            }
        } else {

            $afspraken = Status_has_appointment::with('afspraak', 'status')->where('status_id', $status)->whereHas('afspraak', function($afs){
                $afs->where('afspraaktype_id', 3)->orWhere('afspraaktype_id', 5);
            })->get();

        }

        $statussen = Status::where('id', '<', 5)->orWhere('id', 23)->orWhere('id', 29)->orWhere('id', 30)->get();
        $afspraaktypes = AppointmentType::where('id', 3)->orWhere('id', 5)->get();
        $page = 'financieel';
        $sub = 'openstaande_afspraken';
        return view('afspraak.openstaandeAfspraken', compact('afspraken', 'page', 'sub', 'statussen', 'afspraaktypes'));
    }

    public function addHistory($id){
        $afspraak = Appointment::find($id);
        $relatie = Client::find($afspraak->klant_id);
        $geschiedenis = History::where('klant_id', $relatie->id)->where('afspraak_id', '!=', $afspraak->id)->get();

        $page = "relatie";
        $sub = "relover";
        return view('afspraak.addHistory', compact('relatie', 'afspraak', 'geschiedenis', 'page', 'sub'));
    }

    public function resetProcess($id){
        $afspraak = Appointment::find($id);
        $afspraak->verwerkt_door = null;
        $afspraak->reden = null;
        $afspraak->is_verzet = 0;
        $afspraak->niet_betaald = null;
        $afspraak->betaald = 0;
        // $afspraak->initiatief = 0;
        $afspraak->bevestiging = 0;
        $afspraak->factuur_id = null;
        $afspraak->eigenschuld = null;
        $afspraak->bank = null;
        $afspraak->advies = null;
        $afspraak->verwerkt_door = 1;
        $gs = History::where('afspraak_id', $afspraak->id)->orderBy('created_at', 'ASC')->get();
        $count = 0;
        foreach($gs as $g){
            if($count > 0){
                $g->delete();
            }
            $count++;
        }
        $afspraak->save();

        Status_has_appointment::setSha($afspraak->id, 1, "De verwerking van deze afspraak is hersteld door: ".Auth::user()->name, date('Y-m-d', strtotime($afspraak->startdatum)));

        return redirect('/relatie/'.$afspraak->klant_id)->with('succes', 'De afspraak is hersteld!');
    }

    public function Previous($id, $proces_id){
        $afspraak = Appointment::find($id);
        // $previous = Afspraak::find($id);
        if($proces_id <= 2){ // afspraak aanmaken/werkzaamheden toeveogen
            $this->resetProcess($id);
            $interval = $afspraak->getDateDifference();
            $previous = $afspraak;
            $knoppen = array();
            if($afspraak->aha){
                foreach($afspraak->aha as $a){
                    $a->delete();
                }
            }
            if($proces_id == 1){ // aanmaken
                if($interval->d > 0){
                    $knoppen['spec'] = true;
                    // array_push($knoppen, ['spec' => true]);
                } else {
                    if(date('H:i', strtotime($afspraak->einddatum)) == '12:30'){
                    $knoppen['voor'] = true;
                        // array_push($knoppen, ['voor' => true]);
                    } else {
                    $knoppen['na'] = true;
                        // array_push($knoppen, ['na' => true]);
                    }
                }
                if($afspraak->initiatief == 1){
                    $knoppen['initiatief'] = true;
                    // array_push($knoppen, ['initiatief' => true]);
                }

                foreach (Afspraak_has_geschiedenis::where('afspraak_id', $afspraak->id)->get() as $g) {
                    $g_id = $g->geschiedenis_id;
                    $g->delete();
                    History::find($g_id)->delete();
                    foreach(History::where('afspraak_id', $afspraak->id)->get() as $gs){
                        $gs->delete();
                    }
                }
                if(Status_has_appointment::where('afspraak_id', $afspraak->id)->exists()){
                    foreach (Status_has_appointment::where('afspraak_id', $afspraak->id)->get() as $sha) {
                        $sha->delete();
                    }
                }
                $afspraak->delete();
            }
            // dd($previous);

            $page = "relatie";
            $sub = "relover";
            $relatie = Client::findOrFail($afspraak->klant_id);
            $werknemers = Employee::where('actief', 1)->get();
            $activiteiten = Work::where('actief', 1)->get();
            if($proces_id == 1){
                $types = AppointmentType::where('omschrijving', '!=', 'Uitgestelde Betaling')->where('omschrijving', '!=', 'Betalingsherinnering')->get();
                return view('afspraak.maken', compact('page', 'sub', 'previous', 'relatie', 'types', 'werknemers', 'activiteiten', 'knoppen'));
            }
            $danger = '';
            return view('afspraak.werkzaamheden', compact('relatie', 'afspraak', 'activiteiten','danger', 'page', 'sub'));
        } else {

        }
    }

}
