<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Bedrijf;
use Auth;
use DB;

use Yajra\Datatables\Html\Builder; //
use App\Models\DataTables\KlantDatatable;

class BedrijfController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setup()
    {
    	return view('setup');
    }

    public function savesetup()
    {
    	$input = Request::all();
    	Bedrijf::create($input)->save();
    	return redirect('/index')->with('success', 'Bedankt voor het invullen van uw gegevens! <br> Welkom bij Flexavi!');
    }
}