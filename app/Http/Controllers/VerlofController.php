<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Werknemer_has_auto;
use App\Models\Auto;
use App\Models\Log;
use Auth;
use App\Models\Verlof;
use DateTime;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class VerlofController extends Controller
{


protected $htmlBuilder;
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->rol < 6)
        {
            $aanvragen = Verlof::with('werknemer')->get();

            $page = "urenregistratie";
            $sub = "verlofinz";

            return view('uren.verlof', compact('page', 'sub', 'aanvragen'));
        }
        else
        {
            $aanvragen = Verlof::with('werknemer')->where('werknemer_id', Auth::user()->werknemer_id)->get();

            $page = "urenregistratie";
            $sub = "verlofinz";

            return view('uren.verlof', compact('page', 'sub', 'aanvragen'));
        }
    }

    public function showStatus($id)
    {
        if(Auth::user()->rol < 6)
        {
            $aanvragen = Verlof::with('werknemer')->where('goedgekeurd', $id)->get();

            $page = "urenregistratie";
            $sub = "verlofinz";

            return view('uren.verlof', compact('page', 'sub', 'aanvragen'));
        }
        else
        {
            $aanvragen = Verlof::with('werknemer')->where('werknemer_id', Auth::user()->werknemer_id)->where('goedgekeurd', $id)->get();

            $page = "urenregistratie";
            $sub = "verlofinz";

            return view('uren.verlof', compact('page', 'sub', 'aanvragen'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $page = "urenregistratie";
      $sub = "verloftoev";

      return view('uren.verlofCreate', compact('page', 'sub'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        $input = Request::all();
        $input['begin'] = date('Y-m-d', strtotime($input['begin']));
        $input['eind'] = date('Y-m-d', strtotime($input['eind']));
        Verlof::create($input)->save();

        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft een verlofaanvraag gedaan.";
        $log->gebruiker_id = Auth::user()->id;
        $log->type = "Verlof";
        $log->save();

       return redirect('/verlof')->with('succes', 'U heeft succesvol een verlofaanvraag gedaan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function setStatus($id, $sid)
    {
        //
        $verlof = Verlof::find($id);
        $verlof->goedgekeurd = $sid;
        $verlof->save();

        return redirect('/verlof')->with('succes', 'Status aangepast.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $verlof = Verlof::find($id);
        $verlof->delete();

        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft een verlofaanvraag verwijderd.";
        $log->gebruiker_id = Auth::user()->id;
        $log->type = "Verlof";
        $log->save();

       return redirect('/verlof')->with('succes', 'U heeft succesvol een verlofaanvraag verwijderd.');
    }

    public function addHistory()
    {

    }


    public function saveHistory()
    {

    }

    public function showHistory()
    {

    }
}
