<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Work;
use App\Models\Log;
use Auth;
use App\Models\Materiaal;
use App\Models\Activiteit_has_materiaal;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class Activiteit_has_materiaalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function create($id)
    {
    	$materialen = Materiaal::all();
    	$activiteit = Work::find($id);
    	if($activiteit->actief == 2)
    	{
    		return redirect('/activiteit')->with('danger', 'Dit scherm is niet meer beschikbaar. Indien dit beschikbaar hoort te zijn, neem dan contact op met de administrator.');
    	}
    	$page = "";
    	$sub = "";

    	return view('materiaal.setMat', compact('materialen', 'activiteit', 'page', 'sub'));
    }

    public function store($id)
    {
    	$input = Request::all();

    	for($i = 0; $i < count($input['aantal']); $i++)
    	{
    		$temp = new Activiteit_has_materiaal;
    		$temp->activiteit_id = $id;
    		$temp->materiaal_id = $input['materiaal'][$i];
    		$temp->aantal = $input['aantal'][$i];
    		$temp->save();
    	}
    	$activiteit = Work::find($id);
    	$activiteit->actief = 2;
    	$activiteit->save();

    	return redirect('/activiteit')->with('succes', 'U heeft de materialen succesvol toegevoegd aan de werkzaamheden.');
    }

    public function noMats($id)
    {

    	$activiteit = Work::find($id);
    	$activiteit->actief = 2;
    	$activiteit->save();
    	return redirect('/activiteit')->with('succes', 'U heeft de activiteit succesvol verwerkt.');
    }
}
