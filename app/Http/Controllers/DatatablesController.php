<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;

class DatatablesController extends Controller
{

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function relatieData()
    {
        return Datatables::of(Client::query())->make(true);
    }

    public function relatie()
    {
        return view('klant.index');
    }
}
