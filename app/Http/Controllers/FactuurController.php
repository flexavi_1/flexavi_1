<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\History;
use App\Models\Status;
use App\Models\Log;
use App\Models\Status_has_appointment;
use App\Models\Appointment_has_activities;
use App\Models\Work;
use Auth;
use App\Models\Factuur;
use App\Models\Status_has_factuur;
use DateTime;
use DB;
use App\Models\Bedrijf;
use App\Models\Worklist;
use App\Models\Worklist_has_appointments;
use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class FactuurController extends Controller
{


    protected $htmlBuilder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(KlantDatatable $datatable)
    {
        $dagenopen = []; // Initialize $dagenopen as an empty array

        if (count(Factuur::all()) == 0) {
            $facturen = "";
        } else {
            $facturen = DB::table('klant')
                ->join('factuur', 'klant.id', '=', 'factuur.klant_id')
                ->join('status_has_factuur', 'factuur.id', '=', 'status_has_factuur.factuur_id')
                ->join('status', 'status_has_factuur.status_id', '=', 'status.id')
                ->select('klant.*', 'factuur.*', 'status_has_factuur.*', 'status.*')
                ->get();

            foreach ($facturen as $factuur) {
                if ($factuur->voldaan == 0) {
                    $temp = [];
                    $temp['factuur_id'] = $factuur->factuur_id;
                    $temp['dagenopen'] = (new Factuur)->calcDiff($factuur->betaaldatum, date('d-m-Y'));

                    array_push($dagenopen, $temp);
                }
            }
            // dd($facturen);
        }

        $page = "financieel";
        $sub = "debioverzicht";

        return view('factuur.index', compact('facturen', 'dagenopen', 'page', 'sub'));
    }


    public function open()
    {
        $dagenopen = array();

        if (count(Factuur::all()) == 0) {
            $facturen = "";
        } else {
            $facturen = DB::table('klant')
                ->join('factuur', 'klant.id', '=', 'factuur.klant_id')
                ->join('status_has_factuur', 'factuur.id', '=', 'status_has_factuur.factuur_id')
                ->join('status', 'status_has_factuur.status_id', '=', 'status.id')
                ->select('klant.*', 'factuur.*', 'status_has_factuur.*', 'status.*')
                ->where('factuur.voldaan', '=', 0)
                ->get();

            // $dagenopen = array();
            $test = new Factuur;
            foreach ($facturen as $factuur) {
                if ($factuur->voldaan == 0) {
                    $temp = array();
                    $temp['factuur_id'] = $factuur->factuur_id;
                    $temp['dagenopen'] = $test->calcDiff($factuur->betaaldatum, date('d-m-Y'));

                    array_push($dagenopen, $temp);
                }
            }
        }
        // dd($relaties);

        $page = "financieel";
        $sub = "open";




        $open = "blabla";

        return view('factuur.index', compact('facturen', 'open', 'dagenopen', 'page', 'sub'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($klant_id)
    {
        $relatie = Client::findOrFail($klant_id);

        $page = "financieel";
        $sub = "debioverzicht";

        return view('factuur.klantfactuur', compact('relatie', 'page', 'sub'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function nieuw()
    {

        $page = "financieel";
        $sub = "debioverzicht";
        return view('factuur.maken', 'page', 'sub');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //apply business rules
        $input = Request::all();
        $factuur = new Factuur;
        $input['betaaldatum'] = $factuur->setDueDate($input['betaaldatum']);

        Factuur::create($input)->save();

        //set invoicerules

        return redirect('/relatie/' . $input['klant_id'])->with('succes', 'Factuur succesvol toegevoegd.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $factuur_id = $id;
        $factuur = Factuur::find($factuur_id);
        $relatie_id = $factuur->klant_id;
        $relatie = Client::find($relatie_id);
        $huisnummer = $relatie->huisnummer;
        // dd($factuur);
        if ($factuur != null && $relatie != null) {
            if ($factuur->klant_id == $relatie_id && $relatie->huisnummer == $huisnummer) {
                $afspraak = Appointment::where('factuur_id', $factuur_id)->first();
                $aha = Appointment_has_activities::where('afspraak_id', $afspraak->id)->get();
                $act = Work::where('actief', 1)->get();

                $factuurregels = array();
                foreach ($act as $ac) {
                    foreach ($aha as $ah) {
                        if ($ah->activiteit_id == $ac->id) {
                            $temp = array();
                            $temp['afspraak_id'] = $afspraak->id;
                            $temp['startdatum'] = $afspraak->startdatum;
                            $temp['activiteit_id'] = $ac->id;
                            $temp['aantal'] = $ah->aantal;
                            $temp['prijs'] = $ah->prijs;
                            $temp['btw'] = round($ac->prijs / (1 + ($ac->btw / 100)) * ($ac->btw / 100) * $ah->aantal, 2);
                            $temp['totaalregel'] = $ah->aantal * $ah->prijs;
                            $temp['opmerking'] = $ah->opmerking;
                            $temp['omschrijving'] = $ac->omschrijving;
                            $temp['eenheid'] = $ac->eenheid;
                            $temp['beschrijving'] = $ac->beschrijving;
                            $temp['hoe'] = $ac->hoe;
                            $temp['kortingsbedrag'] = $ah->kortingsbedrag;
                            $temp = (object) $temp;
                            array_push($factuurregels, $temp);
                        }
                    }
                }
                // $count = count(Factuur::where('klant_id', $relatie->id)->get());
                // dd(date('ymd').$relatie->id.'.'.$count);
                $shf = Status_has_factuur::where('factuur_id', $factuur_id)->first();
                $status = Status::find($shf->status_id);

                $page = "";
                $sub = "";
                $bedrijf = Bedrijf::find(1);
                // dd($bedrijf);
                if (Worklist_has_appointments::where('afspraak_id', $afspraak->id)->exists()) {
                    if (Worklist::find(Worklist_has_appointments::where('afspraak_id', $afspraak->id)->first()->werklijst_id)->exists()) {
                        if (Employee::find(Worklist::find(Worklist_has_appointments::where('afspraak_id', $afspraak->id)->first()->werklijst_id)->verkoper)->exists()) {
                            $verkoper = Employee::find(Worklist::find(Worklist_has_appointments::where('afspraak_id', $afspraak->id)->first()->werklijst_id)->verkoper);
                        } else {
                            $verkoper = "Niet ingedeeld";
                        }
                    } else {
                        $verkoper = "Niet ingedeeld";
                    }
                } else {
                    $verkoper = "Niet ingedeeld";
                }
                // $verkoper = Werknemer::find( Werklijst::find( Werklijst_has_afspraak::where('afspraak_id', $afspraak->id)->first()->werklijst_id )->verkoper );
                // dd($verkoper);
                $factuurregels = (object) $factuurregels;
                // dd($factuurregels);
                return view('share.shareInvoice', compact('factuur', 'afspraak', 'verkoper', 'relatie', 'bedrijf', 'factuurregels', 'status', 'shf', 'page', 'sub'));
            } else {
                if (Auth::check())
                    return redirect('/dashboard');
                return Redirect::to('http://' . $url);
            }
        }
        $url = Bedrijf::find(1)->website;
        return Redirect::to('http://' . $url);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $factuur = Factuur::findOrFail($id);
        $relatie = Client::find($factuur->klant_id);
        $afspraak = Appointment::where('factuur_id', $factuur->id)->first();
        $afspraak->factuur_id = null;
        $afspraak->save();

        $shf = Status_has_factuur::where('factuur_id', $factuur->id)->first();
        $shf->delete();

        $factuur->delete();

        return redirect('/relatie/' . $relatie->id)->with('succes', 'Factuur succesvol verwijderd.');
        //vergeet status niet
        //afspraak->factuur_id moet weer NULL worden.
    }

    public function genereerFactuur($afspraak_id)
    {
        $afspraak = Appointment::find($afspraak_id);
        $relatie = Client::find($afspraak->klant_id);

        $factuur  = new Factuur;
        // $strtotime = DateTime::createFromFormat("d-m-Y H:i", $afspraak->startdatum);
        // $startstamp = $strtotime->getTimestamp();
        $count = count(Factuur::where('klant_id', $relatie->id)->get()) + 1;
        $factuur->id = date('ymd') . $relatie->id . $count;
        $datum = date('Y-m-d', strtotime($afspraak->startdatum));
        $factuur->datum = $datum;
        $factuur->betaaldatum = date('Y-m-d', strtotime($factuur->datum . ' + 14 days'));
        $factuur->klant_id = $relatie->id;
        $factuur->aangemaakt_door = Auth::user()->id;

        if ($afspraak->klusprijs == 1) {
            $factuur->totaalbedrag = $afspraak->totaalbedrag;
            $factuur->klusprijs_korting = $afspraak->getRealAppointmentPrice() - $afspraak->totaalbedrag;
        } else {
            $totaalbedrag = 0.00;
            $aha = Appointment_has_activities::where('afspraak_id', $afspraak->id)->get();
            foreach ($aha as $a) {
                $price = $a->prijs * $a->aantal;
                $totaalbedrag += $price;
            }
            $factuur->totaalbedrag = $totaalbedrag;
        }
        $factuur->save();

        $fact = Factuur::where('klant_id', $relatie->id)->where('datum', date('Y-m-d', strtotime($afspraak->startdatum)))->first();
        //dd($fact);
        $shf = new Status_has_factuur;
        $shf->status_id = 8;
        $shf->factuur_id = $fact->id;
        $shf->verwerkt_door = Auth::user()->id;
        $shf->save();

        $log = new Log;
        $log->klant_id = $relatie->id;
        $log->omschrijving = Auth::user()->name . " heeft factuur aangemaakt voor klant wonend op: " . $relatie->postcode . " " . $relatie->huisnummer . ".";
        $log->gebruiker_id = Auth::user()->id;
        $log->type = "Factuur";
        $log->save();

        //geschiedenis
        $regel = new History;
        $regel->datum = date('Y-m-d');
        $regel->klant_id = $relatie->id;
        $regel->afspraak_id = $afspraak->id;
        $user = User::findOrFail(Auth::user()->id);
        $regel->gebruiker_id = $user->id;
        $regel->omschrijving = $user->name . " heeft een factuur aangemaakt voor afspraak: " . $afspraak_id;
        $regel->save();
        //dd($fact);
        $afspraak->factuur_id = $fact->id;
        $afspraak->save();
        return redirect('/relatie/' . $relatie->id)->with('succes', 'Factuur succesvol gegenereerd, zie factuur pagina.');
    }

    public function betaald($id)
    {
        $factuur = Factuur::find($id);
        $relatie = Client::find($factuur->klant_id);
        $factuur->voldaan = 1;

        $shf = Status_has_factuur::where('factuur_id', $id)->first();
        $shf->status_id = 9;
        $shf->save();

        $regel = new History;
        $regel->datum = date('Y-m-d');
        $regel->klant_id = $relatie->id;
        $user = User::findOrFail(Auth::user()->id);
        $regel->gebruiker_id = $user->id;
        $regel->omschrijving = $user->name . " heeft factuur " . $id . " gemarkeerd als betaald.";
        $regel->save();

        $log = new Log;
        $log->klant_id = $relatie->id;
        $log->omschrijving = $user->name . " heeft factuur " . $id . " gemarkeerd als betaald.";
        $log->gebruiker_id = Auth::user()->id;
        $log->type = "Factuur";
        $log->save();

        $factuur->save();

        return redirect('/relatie/' . $relatie->id)->with('succes', 'Fijn dat debiteur betaald heeft, factuur is als betaald gemarkeerd.');
    }

    public function eerste($id)
    {
        $factuur = Factuur::find($id);
        $shf = Status_has_factuur::where('factuur_id', $id)->first();
        if ($shf->status_id == 8 || $shf->status_id == 10) {
            //verstuur brief

            $shf->status_id == 11;
            $shf->save();

            $log = new Log;
            $log->omschrijving = Auth::user()->name . " heeft klant de eerste brief verzonden, vanwege niet betalen factuur.";
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $factuur->klant_id;
            $log->type = "Factuur";
            $log->save();

            //geschiedenis
            $regel = new History;
            $regel->datum = date('Y-m-d');
            $regel->klant_id = $factuur->klant_id;
            $user = User::findOrFail(Auth::user()->id);
            $regel->gebruiker_id = $user->id;
            $regel->omschrijving = $user->name . " heeft de eerste brief verzonden, vanwege niet betalen factuur.";
            $regel->save();

            return redirect('/debiteur/')->with('succes', 'Eerste brief succesvol per mail verstuurd.');
        } else {
            return redirect('/debiteur/')->with('danger', 'U kunt deze brief nog niet versturen');
        }
    }
    public function tweede($id)
    {
        $factuur = Factuur::find($id);
        $shf = Status_has_factuur::where('factuur_id', $id)->first();
        if ($shf->status_id == 11) {
            //verstuur brief

            $shf->status_id == 12;
            $shf->save();

            $log = new Log;
            $log->omschrijving = Auth::user()->name . " heeft klant de Tweede brief verzonden, vanwege niet betalen factuur.";
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $factuur->klant_id;

            $log->type = "Factuur";
            $log->save();

            //geschiedenis
            $regel = new History;
            $regel->datum = date('Y-m-d');
            $regel->klant_id = $factuur->klant_id;
            $user = User::findOrFail(Auth::user()->id);
            $regel->gebruiker_id = $user->id;
            $regel->omschrijving = $user->name . " heeft de Tweede brief verzonden, vanwege niet betalen factuur.";
            $regel->save();

            return redirect('/debiteur/')->with('succes', 'Tweede brief succesvol per mail verstuurd.');
        } else {
            return redirect('/debiteur/')->with('danger', 'U kunt deze brief nog niet versturen');
        }
    }
    public function derde($id)
    {
        $factuur = Factuur::find($id);
        $shf = Status_has_factuur::where('factuur_id', $id)->first();
        if ($shf->status_id == 12) {
            //verstuur brief

            $shf->status_id == 13;
            $shf->save();

            $log = new Log;
            $log->omschrijving = Auth::user()->name . " heeft klant de Aanmaning verzonden, vanwege niet betalen factuur.";
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $factuur->klant_id;
            $log->type = "Factuur";
            $log->save();

            //geschiedenis
            $regel = new History;
            $regel->datum = date('Y-m-d');
            $regel->klant_id = $factuur->klant_id;
            $user = User::findOrFail(Auth::user()->id);
            $regel->gebruiker_id = $user->id;
            $regel->omschrijving = $user->name . " heeft de Aanmaning verzonden, vanwege niet betalen factuur.";
            $regel->save();

            return redirect('/debiteur/')->with('succes', 'Aanmaning succesvol per mail verstuurd.');
        } else {
            return redirect('/debiteur/')->with('danger', 'U kunt deze brief nog niet versturen');
        }
    }
    public function vierde($id)
    {
        $factuur = Factuur::find($id);
        $shf = Status_has_factuur::where('factuur_id', $id)->first();
        if ($shf->status_id == 13) {
            //verstuur brief

            $shf->status_id == 14;
            $shf->save();

            $log = new Log;
            $log->omschrijving = Auth::user()->name . " heeft klant de ingebrekestelling verzonden, vanwege niet betalen factuur.";
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $factuur->klant_id;
            $log->type = "Factuur";
            $log->save();

            //geschiedenis
            $regel = new History;
            $regel->datum = date('Y-m-d');
            $regel->klant_id = $factuur->klant_id;
            $user = User::findOrFail(Auth::user()->id);
            $regel->gebruiker_id = $user->id;
            $regel->omschrijving = $user->name . " heeft de ingebrekestelling verzonden, vanwege niet betalen factuur.";
            $regel->save();

            return redirect('/debiteur/')->with('succes', 'ingebrekestelling succesvol per mail verstuurd.');
        } else {
            return redirect('/debiteur/')->with('danger', 'U kunt deze brief nog niet versturen');
        }
    }
}
