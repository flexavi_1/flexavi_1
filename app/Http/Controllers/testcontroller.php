public function opslaan($id)

    {

        $input = Request::all();



        if ($input['startdatum'] >= $input['einddatum'] && $input['radio_demo'] == 2) {

            return redirect('/afspraak/maken/' . $input['klant_id'])->with('danger', 'Startdatum moet voor de einddatum liggen.');
        } else {

            $afspraak = new Afspraak;

            if ($input['radio_demo'] < 2) {

                $strtotime = DateTime::createFromFormat("Y-m-d H:i", $input['startdatum']);

                $strtotimee = DateTime::createFromFormat("Y-m-d H:i", $input['einddatum']);

                

                $startstamp = $strtotime->getTimestamp();

                $endstamp = $strtotimee->getTimestamp();

                if ($input['radio_demo'] == 1) {

                    $afspraak->startdatum = date('Y-m-d', $startstamp) . " 12:30";

                    $afspraak->einddatum = date('Y-m-d', $startstamp) . " 17:00";
                } else {

                    $afspraak->startdatum = date('Y-m-d', $startstamp) . " 08:00";

                    $afspraak->einddatum = date('Y-m-d', $startstamp) . " 12:30";
                }
            } else {

                $strtotime = DateTime::createFromFormat("Y-m-d H:i:s", $input['startdatum']);

                $strtotimee = DateTime::createFromFormat("Y-m-d H:i:s", $input['einddatum']);

               

                $startstamp = $strtotime->getTimestamp();

                $endstamp = $strtotimee->getTimestamp();

                $afspraak->startdatum = date('Y-m-d H:i:s', $startstamp);

                $afspraak->einddatum = date('Y-m-d H:i:s', $endstamp);
            }

            $afspraak->klant_id = $input['klant_id'];

            $afspraak->geschreven_door = $input['geschreven_door'];

            $afspraak->verwerkt_door = $input['verwerkt_door'];

            $afspraak->afspraaktype_id = $input['afspraaktype_id'];

            $afspraak->omschrijving = $input['omschrijving'];

            $afspraak->save();

            // dd($afspraak);



            // $afspr = Afspraak::where('startdatum', date('Y-m-d H:i:s', $startstamp))

            //                     ->where('klant_id', $input['klant_id'])

            //                     ->where('einddatum', date('Y-m-d H:i:s', $endstamp))->first();



            // dd($afspr);

            $relatie = Klant::findOrFail($afspraak->klant_id);

            if ($relatie->klanttype_id < 3 /*&& afspraaktype =/= offerte*/) {

                $relatie->klanttype_id = 3;

                $relatie->save();
            }



            $sha = new Status_has_afspraak;

            $sha->status_id = 1;





            $relatie = Klant::findOrFail($afspraak->klant_id);

            if (Klant_has_status::where('klant_id', $relatie->id)->exists()) //set Reminder

            {

                $khs = Klant_has_status::where('klant_id', $relatie->id)->first();

                if ($khs->status_id == 22) {

                    if ($relatie->calcDiff($khs->updated_at, date('Y-m-d')) >= 350) {

                        $khs->status_id = 21;

                        $khs->verwerkt_door = Auth::user()->id;

                        $khs->save();
                    }
                }
            } else {



                $khs = new Klant_has_status;

                $khs->status_id = 21;

                $khs->klant_id = $relatie->id;

                $khs->verwerkt_door = Auth::user()->id;

                $khs->save();
            }





            $sha->afspraak_id = $afspraak->id;

            $sha->gebruiker_id = Auth::user()->id;

            $sha->omschrijving = Auth::user()->name . " heeft deze afspraak Toegevoegd.";

            $sha->save();



            $log = new Log;

            $log->omschrijving = Auth::user()->name . " heeft een afspraak op: " . $afspraak->startdatum . " -- " . $afspraak->einddatum . ", toegevoegd.";

            $log->gebruiker_id = Auth::user()->id;

            $log->klant_id = $input['klant_id'];

            $log->type = "Afspraak";

            $log->save();



            //geschiedenis

            $regel = new Geschiedenis;

            $regel->datum = date('Y-m-d');

            $regel->klant_id = $afspraak->klant->id;

            $regel->afspraak_id = $afspraak->id;

            $user = User::findOrFail(Auth::user()->id);

            $regel->gebruiker_id = $user->id;

            $regel->omschrijving = $user->name . " heeft een afspraak gemaakt met klant op " . $afspraak->returnDate($afspraak->startdatum) . " --- " . $afspraak->returnDate($afspraak->einddatum);

            $regel->save();



            $offerte = Offerte::find($id);

            $ohas = Activiteit_has_offerte::where('offerte_id', $id)->get();

            $totaalbedrag = 0.00;

            if (isset($input['klusprijsAan'])) {

                $afspraak->klusprijs = 1;

                $klusprijs = $input['klusprijs'];

                $totaalbedrag = $klusprijs;
            }



            foreach ($ohas as $oha) {

                $tmp = new Activiteit_has_Afspraak; //temporary aha

                $tmp->activiteit_id = $oha->activiteit_id; //set a.id into appointment_has_activity

                $tmp->afspraak_id = $afspraak->id; // set appointment_id

                $tmp->aantal = $oha->aantal; // set amount

                $tmp->opmerking = $oha->opmerking;



                if (isset($klusprijs)) {

                    if ($klusprijs > 0) {

                        $tmp->prijs = $oha->prijs;

                        $bedrag = $tmp->prijs * $tmp->aantal;

                        if ($klusprijs - $bedrag <= 0) {

                            $tmp->kortingsbedrag = $bedrag - $klusprijs;

                            $tmp->kortingsperc = $tmp->kortingsbedrag / ($tmp->prijs * $tmp->aantal) * 100;

                            $klusprijs = $klusprijs - $bedrag;
                        } else {

                            $tmp->kortingsbedrag = $oha->kortingsbedrag;

                            $tmp->kortingsperc = $tmp->kortingsbedrag / ($tmp->prijs * $tmp->aantal) * 100;

                            $klusprijs = $klusprijs - $bedrag;
                        }
                    }
                } else {



                    $tmp->kortingsbedrag = $oha->kortingsbedrag;

                    $tmp->kortingsperc = $oha->kortingsperc;

                    $tmp->prijs = $oha->prijs;

                    $totaalbedrag = $totaalbedrag + ($tmp->aantal * $tmp->prijs);
                }

                $tmp->save();
            }

            $afspraak->offerte_id = $id;

            $afspraak->totaalbedrag = $totaalbedrag;

            $afspraak->save();



            return redirect('/relatie/' . $relatie->id)->with('succes', 'U heeft de offerte succesvol genhonoreerd.');
        }
    }
