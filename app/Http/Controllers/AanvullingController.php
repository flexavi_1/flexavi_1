<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\History;
use App\Models\Status;
use App\Models\Log;
use App\Models\Status_has_appointment;
use App\Models\Status_has_factuur;
use App\Models\Appointment_has_activities;
use App\Models\Activiteit_has_offerte;
use App\Models\Work;
use Auth;
use App\Models\Factuur;
use App\Models\Klant_has_status;
use DB;
use App\Models\Crediteur;
use App\Models\Offerte;
use App\Models\Offerte_has_status;
use Datetime;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;
use App\Models\Uur;
use App\Models\Aanvulling;
use App\Models\Verzonden_aan;
use App\Models\Reactie;

class AanvullingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showEmployees()
    {
        $werknemers = User::where('actief', 1)->get();
        $page = "aanvulling";
        $sub = "aanvwerk";
        return view('aanvulling.all', compact('werknemers', 'page', 'sub'));
    }

    public function getEmployee($id)
    {
        if ($id == Auth::user()->id) {
            return redirect('/aanvulling');
        } else {
            $werknemers = User::where('actief', 1)->get();

            $ontvangenopen = Verzonden_aan::with('aanvulling', 'status')->where('ontvanger', $id)->where('status_id', '!=', 18)->where('status_id', '!=', 20)->get(); // fetch all open recieved comments
            $ontvangengesloten = Verzonden_aan::with('aanvulling', 'status')->where('ontvanger', $id)->where('status_id', 18)->orWhere('status_id', 20)->get(); // fetch all closed recieved comments
            //$aanvullingen = Aanvulling::with('verzonden_aan')->where('gebruiker_id', Auth::user()->id)->get(); // fetch all send comments, this can be done more efficient..

            $sentAanv = Verzonden_aan::with('aanvulling', 'status')->whereHas('aanvulling', function ($aan) use ($id) { // fetch all send comments, this can be done more efficient..
                $aan->where('gebruiker_id', $id);
            })->get();

            $amount_new = Verzonden_aan::with('aanvulling', 'status')->where('ontvanger', $id)->where('status_id', 17)->count(); // fetch all open recieved comments

            $amount_open_sent = Verzonden_aan::with('aanvulling', 'status')
                ->whereHas('aanvulling', function ($aan) use ($id) {
                    $aan->where('gebruiker_id', $id);
                })
                ->whereHas('status', function ($sta) {
                    $sta->where('id', '<>', 18)->where('id', '<>', 20);
                })
                ->count();


            $users = User::all();

            $gebruiker = User::find($id);
            $page = "aanvulling";
            $sub = "aanvwerk";
        }
        return view('aanvulling.gebruiker', compact('gebruiker', 'ontvangenopen', 'ontvangengesloten', 'users', 'amount_new', 'sentAanv', 'amount_open_sent', 'werknemers', 'page', 'sub'));
    }

    public function index()
    {
        //update all Users
        $users = User::all();
        foreach ($users as $user) {
            if ($user->werknemer_id != 0 && $user->werknemer_id != null) {
                $wn = Employee::find($user->werknemer_id);
                if ($wn->actief == 0) {
                    $user->actief = 0;
                    $user->save();
                } else {
                    $user->actief = 1;
                    $user->save();
                }
            }
        }

        $werknemers = User::where('actief', 1)->get();

        $ontvangenopen = Verzonden_aan::with('aanvulling', 'status')->where('ontvanger', Auth::user()->id)->where('status_id', '!=', 18)->where('status_id', '!=', 20)->get(); // fetch all open recieved comments
        foreach ($ontvangenopen as $oo) {
            $reactie_id = DB::table('reactie')->where('verzonden_aan_id', $oo->id)->max('id');
            if ($reactie_id == null) {
                $oo->laatste_reactie = User::find($oo->aanvulling->gebruiker_id);
                $oo->laatste_reactie_tijd = date('d-m-Y H:i', strtotime($oo->aanvulling->created_at));
            } else {
                $oo->laatste_reactie = User::find(Reactie::find($reactie_id)->gebruiker_id);
                $oo->laatste_reactie_tijd = date('d-m-Y H:i', strtotime(Reactie::find($reactie_id)->created_at));
            }
        }
        $ontvangengesloten = Verzonden_aan::with('aanvulling', 'status')->where('ontvanger', Auth::user()->id)->where('status_id', 18)->orWhere('status_id', 20)->get(); // fetch all closed recieved comments
        foreach ($ontvangengesloten as $oo) {
            $reactie_id = DB::table('reactie')->where('verzonden_aan_id', $oo->id)->max('id');
            if ($reactie_id == null) {
                $oo->laatste_reactie = User::find($oo->aanvulling->gebruiker_id);
                $oo->laatste_reactie_tijd = date('d-m-Y H:i', strtotime($oo->aanvulling->created_at));
            } else {
                $oo->laatste_reactie = User::find(Reactie::find($reactie_id)->gebruiker_id);
                $oo->laatste_reactie_tijd = date('d-m-Y H:i', strtotime(Reactie::find($reactie_id)->created_at));
            }
        }

        //$aanvullingen = Aanvulling::with('verzonden_aan')->where('gebruiker_id', Auth::user()->id)->get(); // fetch all send comments, this can be done more efficient..

        $sentAanv = Verzonden_aan::with('aanvulling', 'status')->whereHas('aanvulling', function ($aan) { // fetch all send comments, this can be done more efficient..
            $aan->where('gebruiker_id', Auth::user()->id);
        })->get();
        foreach ($sentAanv as $oo) {
            $reactie_id = DB::table('reactie')->where('verzonden_aan_id', $oo->id)->max('id');
            if ($reactie_id == null) {
                $oo->laatste_reactie = User::find($oo->aanvulling->gebruiker_id);
                $oo->laatste_reactie_tijd = date('d-m-Y H:i', strtotime($oo->aanvulling->created_at));
            } else {
                $oo->laatste_reactie = User::find(Reactie::find($reactie_id)->gebruiker_id);
                $oo->laatste_reactie_tijd = date('d-m-Y H:i', strtotime(Reactie::find($reactie_id)->created_at));
            }
        }

        $amount_new = Verzonden_aan::with('aanvulling', 'status')->where('ontvanger', Auth::user()->id)->where('status_id', 17)->count(); // fetch all open recieved comments

        $amount_open_sent = Verzonden_aan::with('aanvulling', 'status')
            ->whereHas('aanvulling', function ($aan) {
                $aan->where('gebruiker_id', Auth::user()->id);
            })
            ->whereHas('status', function ($sta) {
                $sta->where('id', '<>', 18)->where('id', '<>', 20);
            })
            ->count();


        $users = User::all();
        // dd($amount_open_sent);
        $page = "aanvulling";
        $sub = "aanvover";
        return view('aanvulling.index', compact('ontvangenopen', 'ontvangengesloten',  'users', 'amount_new', 'sentAanv', 'werknemers', 'amount_open_sent', 'page', 'sub'));
    }

    public function create()
    {
        $gebruikers = User::all();

        $page = "aanvulling";
        $sub = "aanvtoev";
        return view('aanvulling.toevoegen', compact('gebruikers', 'page', 'sub'));
    }

    public function store()
    {
        $input = Request::all();
        $aanvulling = new Aanvulling();
        $aanvulling->gebruiker_id = Auth::user()->id;
        $aanvulling->titel = $input['titel'];
        $aanvulling->omschrijving = $input['omschrijving'];
        if (isset($input['postcode']) && $input['postcode'] != "") {
            if ($input['toevoeging'] != "") {
                $klant = Client::where('postcode', $input['postcode'])->where('huisnummer', $input['huisnummer'])->where('hn_prefix', $input['toevoeging'])->first();
            } else {
                $klant = Client::where('postcode', $input['postcode'])->where('huisnummer', $input['huisnummer'])->first();
            }

            if ($klant == null) {
                $gebruikers = User::all();
                $danger = 'Ongeldige combinatie van postcode, huisnummer (en toevoeging), probeer opnieuw.';
                $titel = $aanvulling->titel;
                $postcode = $input['postcode'];
                $huisnummer = $input['huisnummer'];
                $omschrijving = $input['omschrijving'];
                $toevoeging = $input['toevoeging'];
                $page = "aanvulling";
                $sub = "aanvtoev";
                return view('aanvulling.toevoegen', compact('gebruikers', 'page', 'sub', 'danger', 'titel', 'omschrijving', 'postcode', 'huisnummer', 'toevoeging'));
            } else {
                $klant = $klant->id;
            }
        } else {
            $klant = null;
        }
        $aanvulling->klant_id = $klant;
        $aanvulling->save();

        $aanv = Aanvulling::where('gebruiker_id', Auth::user()->id)->where('omschrijving', $input['omschrijving'])->first();
        for ($i = 0; $i < count($input['ontvangers']); $i++) {
            $temp = new Verzonden_aan();
            $temp->aanvulling_id = $aanv->id;
            $temp->status_id = 16;
            $temp->ontvanger = $input['ontvangers'][$i];
            $temp->save();
        }

        //log?

        return redirect('/aanvulling')->with('succes', 'U heeft de aanvulling succesvol geplaatst.');
    }

    public function delete($id)
    {
        $aanvulling = Aanvulling::find($id);
        $verzondenaan = Verzonden_aan::where('aanvulling_id', $aanvulling->id)->get();
        foreach ($verzondenaan as $va) {
            $reacties = Reactie::where('verzonden_aan_id', $va->id)->get();
            foreach ($reacties as $reactie) {
                $reactie->delete();
            }
            $va->delete();
        }
        $aanvulling->delete();

        return redirect('/aanvulling')->with('succes', 'Aanvulling is verwijderd.');
    }

    public function showDelComp()
    {
        $aanvullingen = Verzonden_aan::with('aanvulling', 'status')->where('status_id', 18)->get();
        foreach ($aanvullingen as $a) {
            $a->ontvanger = User::find($a->ontvanger);
            if (!is_Object($a->aanvulling->gebruiker_id))
                $a->aanvulling->gebruiker_id = User::find($a->aanvulling->gebruiker_id);
        }

        $page = "controle";
        $sub = "aanv";
        return view('aanvulling.showDelComp', compact('aanvullingen', 'page', 'sub'));
    }

    public function approved($id, $ontvanger)
    {
        if (Auth::user()->rol < 6) {
            $verzondenaan = Verzonden_aan::where('aanvulling_id', $id)->where('ontvanger', $ontvanger)->first();
            // $reacties = Reactie::where('verzonden_aan_id', $verzonden_aan->id)->get();
            // foreach($reacties as $r)
            // {
            //     $r->delete();
            // }
            // dd($verzondenaan);
            $verzondenaan->status_id = 20;
            $verzondenaan->save();

            return redirect('/controle/aanvulling')->with('succes', 'U heeft de aanvulling goedgekeurd.');
        } else {
            return redirect('/controle/aanvulling')->with('danger', 'U heeft hier niet de juiste rechten voor.');
        }
    }

    public function setCustomer($id, $date, $werkdag_id)
    {
        $gebruikers = User::all();
        $tmp = new Aanvulling;
        $tmp->klant_id = $id;
        $tmp->save();
        $page = "aanvulling";
        $sub = "aanvtoev";
        return view('aanvulling.toevoegen', compact('gebruikers', 'page', 'sub', 'tmp', 'date', 'werkdag_id', 'id'));
    }

    public function saveToCustomer($id, $date, $werkdag_id)
    {
        $input = Request::all();
        $aanvulling = Aanvulling::find($input['aanvulling_id']);
        $aanvulling->gebruiker_id = Auth::user()->id;
        $aanvulling->titel = $input['titel'];
        $aanvulling->omschrijving = $input['omschrijving'];
        $aanvulling->klant_id = $id;
        $aanvulling->save();

        $aanvulling = Aanvulling::where('gebruiker_id', Auth::user()->id)->where('omschrijving', $input['omschrijving'])->first();
        for ($i = 0; $i < count($input['ontvangers']); $i++) {
            $temp = new Verzonden_aan();
            $temp->aanvulling_id = $aanvulling->id;
            $temp->status_id = 16;
            $temp->ontvanger = $input['ontvangers'][$i];
            $temp->save();
        }

        // http://localhost:8000/planning/verkoop/30-10-2017/132/controle

        return redirect('/planning/verkoop/' . date('d-m-Y', strtotime($date)) . '/' . $werkdag_id . '/controle')->with('succes', 'U heeft de aanvulling succesvol geplaatst.');
    }
}
