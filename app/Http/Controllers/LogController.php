<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Work;
use App\Models\AppointmentType;
use App\Models\Appointment;
use App\Models\Appointment_has_activities;
use DateTime;
use App\Models\History;
use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;
use App\Models\Log;

class LogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @description Shows listing of all last 10 logs per active user
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function showControle(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
    	return view('log.controle');
    }

    /**
     * @description Shows Logs of this user
     * @param User $user
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function showUser(User $user): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
    	return view('log.user', compact('user'));
    }
}
