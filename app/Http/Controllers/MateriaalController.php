<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\History;
use App\Models\Log;
use Auth;
use App\Models\Klant_has_status;
use DB;
use App\Models\Offerte_has_status;
use App\Models\Leveranciersprijslijst;
use App\Models\Materiaal;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class MateriaalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$materialen = Materiaal::all();
    	$page = "activiteit";
    	$sub = "matover";

    	return view('materiaal.index', compact('materialen', 'page', 'sub'));
    }

    public function create()
    {
    	$page = "activiteit";
    	$sub = "mattoev";
    	return view('materiaal.nieuw', compact('page', 'sub'));
    }

    public function store()
    {
    	$input = Request::all();
    	$materiaalString = Auth::user()->name." heeft de volgende materialen toegevoegd: ";

    	for($i = 0;$i < count($input['naam']);$i++)
    	{
    		$materiaal = new Materiaal;
    		$materiaal->naam = $input['naam'][$i];
    		$materiaal->omschrijving = $input['omschrijving'][$i];
    		$materiaalString = $materiaalString.$input['naam'][$i]."; ";
    		$materiaal->save();
    	}

    	$log = new Log;
    	$log->gebruiker_id = Auth::user()->id;
    	$log->omschrijving = $materiaalString;
    	$log->type = "materiaal";
    	$log->save();

    	return redirect('/materiaal')->with('succes', 'Materia(a)l(en) succesvol toegevoegd.');
    }

    public function edit($id)
    {
        $materiaal = Materiaal::find($id);

        $page = 'activiteit';
        $sub = 'mattoev';
        return view('materiaal.edit', compact('materiaal', 'page', 'sub'));
    }

    public function update($id)
    {
        $input = Request::all();
        $materiaal = Materiaal::find($id);
        $materiaal->fill($input)->save();

        return redirect('/materiaal')->with('succes', 'U heeft het materiaal succesvol gewijzigd.');
    }

    public function delete($id)
    {
        $materiaal = Materiaal::find($id);
        if($materiaal->checkExistence())
            return redirect('/materiaal')->with('danger', 'U kunt het materiaal niet verwijderen, omdat dit gekoppeld is aan bepaalde werkzaamheden.');
        $materiaal->delete();
        return redirect('/materiaal')->with('succes', 'U heeft het materiaal succesvol verwijderd.');
    }

}
