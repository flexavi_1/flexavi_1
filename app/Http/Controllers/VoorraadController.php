<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\History;
use App\Models\Status;
use App\Models\Log;
use App\Models\Status_has_appointment;
use App\Models\Status_has_factuur;
use App\Models\Appointment_has_activities;
use App\Models\Work;
use Auth;
use App\Models\Factuur;
use App\Models\Klant_has_status;
use DB;
use App\Models\Crediteur;
use App\Models\Offerte;
use App\Models\Offerte_has_status;
use App\Models\Leveranciersprijslijst;
use App\Models\Inkoop;
use App\Models\Materiaal;
use App\Models\Voorraad;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class VoorraadController extends Controller
{
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$voorraad = DB::select("
    select materiaal_id, max(datum) as datum, sum(mutatie) as aantal
    from voorraad
    group by materiaal_id
");


        foreach($voorraad as $r)
        {
            $r->materiaal_id = Materiaal::find($r->materiaal_id);
        }
        $materialen = Materiaal::all();

    	$page = "voorraad";
    	$sub = "voorraad";

    	return view('materiaal.voorraad', compact('voorraad', 'materialen', 'page', 'sub'));
    }

    public function getAllMutations()
    {
        $voorraad = Voorraad::all();
        foreach($voorraad as $r)
        {
            $r->materiaal_id = Materiaal::find($r->materiaal_id);
            $r->klant_id = Client::find($r->klant_id);
        }
        $materialen = Materiaal::all();
        $mutaties = true;
        $page = "voorraad";
        $sub = "voorraad";

        return view('materiaal.voorraad', compact('voorraad', 'materialen', 'page', 'sub', 'mutaties'));
    }

    public function stockPerMat($id)
    {
        $voorraad = Voorraad::where('materiaal_id', $id)->get();
        foreach($voorraad as $r)
        {
            $r->materiaal_id = Materiaal::find($r->materiaal_id);
            $r->klant_id = Client::find($r->klant_id);
        }
        $materialen = Materiaal::all();
        $mutaties = true;
        $page = "voorraad";
        $sub = "voorraad";

        return view('materiaal.voorraad', compact('voorraad', 'materialen', 'page', 'sub', 'mutaties'));
    }


}
