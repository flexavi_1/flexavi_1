<?php

namespace App\Http\Controllers;

use Request;
use App\Klant;
use App\Klanttype;
use App\Werknemer;
use Datatables;
use App\User;
use App\Geschiedenis;
use App\Log;
use Auth;
use App\Klant_has_status;
use DB;
use App\Offerte_has_status;
use App\Leveranciersprijslijst;
use App\Materiaal;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class LeverancierprijslijstController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function setNewPricelist($id)
    {
    	$input = Request::all();
    	// dd($input);

    	for($i = 0; $i < count($input['materiaal']); $i++)
    	{
    		$pl = new Leveranciersprijslijst;
    		$pl->klant_id = $input['klant_id'];
    		$pl->materiaal_id = $input['materiaal'][$i];
    		$pl->inkoopprijs = $input['prijs'][$i];
    		$pl->btwtarief = $input['btw'][$i];
    		$pl->omschrijving = $input['omschrijving'][$i];
    		$pl->aantal = $input['aantal'][$i];
    		$pl->save();
    	}

    	return redirect('/relatie/'.$input['klant_id'])->with('succes', 'Prijslijst succesvol aangevuld.');

    }

}