<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Work;
use App\Models\AppointmentType;
use App\Models\Appointment;
use App\Models\History;
use App\Models\Appointment_has_activities;
use DateTime;
use Auth;
use App\Models\Log;
use App\Models\Status;
use App\Models\Status_has_appointment;
use App\Models\Crediteur;
use App\Models\Bedrijf;
use App\Models\Mails;
use App\Models\Mailtype;
use Mail;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class MailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function voorbeeld($id, $afspraak_id)
	{
		$relatie = Client::find($id); // find the customer
		$afspraak = Appointment::find($afspraak_id); // find the appointment
		$bedrijf = Bedrijf::find(1); // find companyrelated information
		$date = date('d-m-Y'); // get current date for example letter
		$afspraakdatum = date('d-m-Y', strtotime($afspraak->startdatum)); //converting startdate to var afspraakdatum
		$administratiekosten = 25.00; // setting administration costs
		$factuurbedrag = $afspraak->totaalbedrag; // retrieving total

	    return view('mail.eerstebrief', compact('relatie', 'afspraakdatum', 'factuurbedrag', 'administratiekosten', 'bedrijf', 'date')); //return plain view with letter as example
	}

	public function sendFirst($id, $afspraak_id, $letter_id)
	{
		$relatie = Client::find($id);
		$afspraak = Appointment::find($afspraak_id);
		$bedrijf = Bedrijf::find(1);
		$date = date('d-m-Y');
		$afspraakdatum = date('d-m-Y', strtotime($afspraak->startdatum));
		$administratiekosten = 25.00;
		$factuurbedrag = $afspraak->totaalbedrag;

		$data = array('relatie' => $relatie, 'bedrijf' => $bedrijf, 'afspraakdatum' => $afspraakdatum, 'date' => $date, 'administratiekosten' => $administratiekosten, 'factuurbedrag' => $factuurbedrag);

		if($relatie->email != null || $relatie->email != "")
		{

			// Mail::send('mail.eerstebrief', $data, function($message) use ($relatie, $bedrijf){
			// 	$message->to($relatie->email, $relatie->voornaam." ".$relatie->achternaam)->subject('Niet nagekomen overeenkomst');
			//	$message->bcc($bedrijf->email, Auth::user()->name);
			// 	$message->from($bedrijf->email, $bedrijf->naam) // later veranderen naar gebruikersemail
			// });
		}



		$mail = new Mails;
		$mail->schrijver = Auth::user()->id;
		$mail->klant_id = $relatie->id;
		$mail->mailtype = 1;
		$mail->afspraak_id = $afspraak->id;
		$mail->onderwerp = Mailtype::find(1)->onderwerp;
		$mail->email = $relatie->email;
		$mail->ontvangen = 1;
		$mail->save();

	    return redirect('/relatie/'.$relatie->id)->with('succes', 'Mail succesvol verzonden');
	}

	public function sendEditedFirst($id, $afspraak_id, $letter_id)
	{
		$input = Request::all();
		// dd($input);
		$relatie = Client::find($id);
		$afspraak = Appointment::find($afspraak_id);
		$bedrijf = Bedrijf::find(1);
		$date = date('d-m-Y');
		$afspraakdatum = date('d-m-Y', strtotime($afspraak->startdatum));
		$administratiekosten = $input['administratiekosten'];
		$factuurbedrag = $input['factuurbedrag'];

		$data = array('relatie' => $relatie, 'bedrijf' => $bedrijf, 'afspraakdatum' => $afspraakdatum, 'date' => $date, 'administratiekosten' => $administratiekosten, 'factuurbedrag' => $factuurbedrag);

		if($relatie->email != null || $relatie->email != "")
		{
			// Mail::send('mail.eerstebrief', $data, function($message) use ($relatie, $bedrijf){
			// 	$message->to($relatie->email, $relatie->voornaam." ".$relatie->achternaam)->subject('Niet nagekomen overeenkomst');
			//	$message->bcc($bedrijf->email, Auth::user()->name);
			// 	$message->from($bedrijf->email, $bedrijf->naam) // later veranderen naar gebruikersemail
			// });
		}


		$mail = new Mails;
		$mail->schrijver = Auth::user()->id;
		$mail->klant_id = $relatie->id;
		$mail->mailtype = 1;
		$mail->afspraak_id = $afspraak->id;
		$mail->onderwerp = Mailtype::find(1)->onderwerp;
		$mail->email = $relatie->email;
		$mail->ontvangen = 1;
		$mail->save();

	    return redirect('/relatie/'.$relatie->id)->with('succes', 'Mail succesvol verzonden');
	}

	public function create($id)
	{
		$relatie = Client::find($id);


        $page = "relatie";
        $sub = "relover";
		return view('mail.create', compact('relatie', 'page', 'sub'));
	}

	public function store($id)
	{
		$relatie = Client::find($id);

		$input = Request::all();
		$mail = new Mails;
		$mail->schrijver = Auth::user()->id;
		$mail->klant_id = $id;
		$mail->onderwerp = $input['titel'];
		$mail->omschrijving = $input['omschrijving'];
		$mail->email = $input['email'];
		if(!isset($input['door_ons'])){
			$mail->mailtype = 9;
			$mail->ontvangen = 2;
		}else{
			$mail->mailtype = 10;
			$mail->ontvangen = 1;
		}
		$strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['ontvangen_op']);
        $startstamp = $strtotime->getTimestamp();
		$mail->ontvangen_op = date('Y-m-d H:i:s', $startstamp);
		$mail->save();
		History::setHistory($id, null, Auth::user()->id, "Mail ingevoerd door: ".Auth::user()->name, "Mail");
		$mail->setLog("De mail is verwerkt door ".Auth::user()->name);



		return redirect('/relatie/'.$id)->with('succes', 'De mail is succesvol ingevoerd.');
	}

	public function sendConfirmation($id){
		$a = Appointment::find($id);
		$relatie = Client::find($a->klant_id);
    	$bedrijf = Bedrijf::find(1);
    	$a->afspraaktype_id = AppointmentType::find($a->afspraaktype_id);
		$a->klant_id = Client::find($a->klant_id);
		$a->changeBevestiging($a->id);
		$a->setLog(Auth::user()->name." heeft de afspraakbevestiging verstuurd naar fam. ".$relatie->achternaam.", ".$relatie->postcode.$relatie->huisnummer.$relatie->hn_prefix.".");
		$a->setHistory(Auth::user()->name." heeft de afspraakbevestiging verstuurd naar fam. ".$relatie->achternaam.", ".$relatie->postcode.$relatie->huisnummer.$relatie->hn_prefix.".");
		$a->ahas = $a->aha;
		// dd($a->ahas);
		// return view('mail.afspraakbevestiging', compact('a', 'bedrijf'));
		$data = array('relatie' => $relatie, 'bedrijf' => $bedrijf, 'a' => $a);

		Mail::send('mail.afspraakbevestiging', $data, function($message) use ($relatie, $bedrijf){
			$message->to($relatie->email, $relatie->voornaam." ".$relatie->achternaam)->subject('Afspraakbevestiging');
			$message->bcc($bedrijf->email, Auth::user()->name);
			$message->from($bedrijf->email, $bedrijf->naam); // later veranderen naar gebruikersemail
		});

		$mail = new Mails;
		$mail->schrijver = Auth::user()->id;
		$mail->klant_id = $relatie->id;
		$mail->mailtype = 5;
		$mail->afspraak_id = $a->id;
		$mail->onderwerp = Mailtype::find(5)->onderwerp;
		$mail->email = $relatie->email;
		$mail->ontvangen = 1;
		$mail->save();

		return redirect('/relatie/'.$relatie->id)->with('succes', 'Bevestiging verstuurd.');

	}
}
