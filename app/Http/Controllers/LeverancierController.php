<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\History;
use App\Models\Log;
use Auth;
use App\Models\Klant_has_status;
use DB;
use App\Models\Offerte_has_status;
use App\Models\Leveranciersprijslijst;
use App\Models\Materiaal;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class LeverancierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getNewPricelist($id)
    {
    	$relatie = Client::find($id);
    	$materialen = Materiaal::all();


    	$page = "voorraad";
    	$sub = "leveran";
    	return view('leverancier.nieuw', compact("relatie","materialen", 'page', 'sub'));
    }

    public function setNewPricelist($id)
    {

    }

}
