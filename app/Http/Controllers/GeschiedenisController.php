<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Work;
use App\Models\AppointmentType;
use App\Models\Appointment;
use App\Models\Appointment_has_activities;
use App\Models\Status_has_appointment;
use DateTime;
use App\Models\History;
use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;
use App\Models\Log;
use Auth;

class GeschiedenisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id)
    {
        $relatie = Client::findOrFail($id);

        $page = "relatie";
        $sub = "relaover";

        return view('geschiedenis.toevoegen', compact('relatie', 'page', 'sub'));
    }

    public function store($id)
    {
        $input = Request::all();

        $regel = new History;

        //dd(strtotime($input['datum']));
        $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['datum']);
        $startstamp = $strtotime->getTimestamp();

        $input['datum'] = date('Y-m-d', $startstamp);

        //dd($input);
        $relatie = Client::findOrFail($input['klant_id']);
        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft geschiedenis aangevuld bij: ".$relatie->achternaam.", Postcode+huisnummer: ".$relatie->postcode.$relatie->huisnummer;
        $log->gebruiker_id = Auth::user()->id;
        $log->klant_id = $relatie->id;
        $log->type = "Geschiedenis";
        $log->save();

        if(isset($input['tb']))
        {
            $afspraak = Appointment::find($input['afspraak_id']);

            $werknemer = Employee::find(Auth::user()->werknemer_id);
            $substr = '';
            if ($werknemer && !empty($werknemer->voornaam) && !empty($werknemer->achternaam)) {
                $substr = strtoupper($werknemer->voornaam[0] . $werknemer->achternaam[0]);
            }


            // $afspraak->reden = $afspraak->reden.";".date('d-m', strtotime($input['datum'])).": ".$input['omschrijving']." (".$substr.")";
            // $afspraak->save();

            History::setHistory($input['klant_id'], $afspraak->id, Auth::user()->id, date('d-m', strtotime($input['datum'])).": ".$input['omschrijving']." (".$substr.")", 'terugbellijst');
            if(Request::get('submit') == 'no'){ // no new appointment
                Status_has_appointment::setSha($afspraak->id, 30, Auth::user()->name." heeft de klant verwerkt: Er is geen nieuwe afspraak nodig.", date('Y-m-d', strtotime($afspraak->startdatum)));

                return redirect('/terugbellijst')->with('succes', 'Wijziging opgeslagen, relatie uit terugbellijst gehaald.');
            }

            return redirect('/terugbellijst')->with('succes', 'Opmerking ingevoegd.');
        }
        else
        {
            // $regel->fill($input)->save();
            History::setHistory($input['klant_id'], null, Auth::user()->id, $input['omschrijving'], '');
        }

        return redirect('/relatie/'.$input['klant_id'])->with('succes', 'U heeft succesvol een geschiedenis regel toegevoegd.');

    }

    public function edit($id)
    {

        $regel = History::findOrFail($id);
        //dd($regel);
        $relatie = Client::findOrFail($regel->klant_id);

        $page = "relatie";
        $sub = "relaover";
        return view('geschiedenis.wijzigen', compact('regel','relatie', 'page', 'sub'));
    }

    public function update($id)
    {
        $input = Request::all();
        $regel = History::findOrFail($id);
        $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['datum']);
        $startstamp = $strtotime->getTimestamp();

        $input['datum'] = date('Y-m-d', $startstamp);
        $regel->fill($input)->save();

        $relatie = Client::findOrFail($input['klant_id']);
        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft geschiedenis gewijzigd bij: ".$relatie->achternaam.", Postcode+huisnummer: ".$relatie->postcode.$relatie->huisnummer;
        $log->gebruiker_id = Auth::user()->id;
        $log->klant_id = $relatie->id;
            $log->type = "Geschiedenis";
        $log->save();

        return redirect('/relatie/'.$input['klant_id'])->with('succes', 'U heeft de geschiedenisregel succesvol aangepast.');
    }

    public function delete($id)
    {
        $regel = History::findOrFail($id);
        $relatie = Client::findOrFail($regel->klant_id);
        $log = new Log;
        $log->omschrijving = Auth::user()->name." heeft geschiedenis verwijderd bij: ".$relatie->achternaam.", Postcode+huisnummer: ".$relatie->postcode.$relatie->huisnummer;
        $log->gebruiker_id = Auth::user()->id;
        $log->klant_id = $relatie->id;
        $log->save();
        $regel->delete();

        return redirect('/relatie/'.$relatie->id)->with('succes', 'U heeft de geschiedenisregel succesvol verwijderd.');
    }

    public function setGeschiedenis($klant_id, $afspraak_id)
    {
        $relatie = Client::findOrFail($klant_id);
        $afspraak_id = $afspraak_id;

        $page = "relatie";
        $sub = "relaover";
        return view('geschiedenis.toevoegen', compact('relatie', 'afspraak_id', 'page', 'sub'));
    }

    public function setGeschiedenisTb($klant_id, $afspraak_id)
    {
        $relatie = Client::findOrFail($klant_id);
        $afspraak_id = $afspraak_id;
        $check = true;

        $page = "relatie";
        $sub = "relaover";
        return view('geschiedenis.toevoegen', compact('relatie', 'afspraak_id', 'page', 'sub', 'check'));
    }

    public function setGeschiedenisggh($klant_id, $afspraak_id)
    {
        // $afspraak = Afspraak::find($afspraak_id);
        $substr = strtoupper(Employee::find(Auth::user()->werknemer_id)->voornaam[0].Employee::find(Auth::user()->werknemer_id)->achternaam[0]);
        // $afspraak->reden = $afspraak->reden.";".date('d-m H:i')." - GGH (".$substr.")";
        // $afspraak->save();

        History::setHistory($klant_id, $afspraak_id, Auth::user()->id, date('d-m H:i')." - GGH (".$substr.")", 'terugbellijst');

        return redirect('/terugbellijst')->with('succes', 'Opmerking ingevoegd.');
    }

    public function deleteSet($klant_id, $afspraak_id){
        if(!Appointment::where('id', $afspraak_id)->exists()){
            $geschiedenis = History::where('afspraak_id', $afspraak_id)->get();
            foreach($geschiedenis as $g){
                $g->delete();
            }
            $relatie = Client::find($klant_id);
            $log = new Log;
            $log->omschrijving = Auth::user()->name." heeft een geschiedenisset verwijderd bij: ".$relatie->achternaam.", Postcode+huisnummer: ".$relatie->postcode.$relatie->huisnummer;
            $log->gebruiker_id = Auth::user()->id;
            $log->klant_id = $relatie->id;
            $log->save();

            return redirect('/relatie/'.$klant_id)->with('succes', 'U heeft de geschiedenis set succesvol verwijderd.');
        } else {
            return redirect('/relatie/'.$klant_id)->with('danger', 'Zolang de afspraak nog bestaat, kan de set niet verwijderd worden. Verwijder eerst de afspraak.');
        }
    }
}
