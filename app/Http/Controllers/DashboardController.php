<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\History;
use App\Models\Status;
use App\Models\Log;
use App\Models\Status_has_appointment;
use App\Models\Status_has_factuur;
use App\Models\Appointment_has_activities;
use App\Models\Activiteit_has_offerte;
use App\Models\Work;
use Auth;
use App\Models\Factuur;
use App\Models\Klant_has_status;
use DB;
use App\Models\Crediteur;
use App\Models\Offerte;
use App\Models\Offerte_has_status;
use Datetime;
use App\Models\Postit;
use App\Models\Bedrijf;

use Yajra\Datatables\Html\Builder; //
use App\Models\DataTables\KlantDatatable;
use App\Models\Uur;
use App\Models\Aanvulling;
use App\Models\Verzonden_aan;
use App\Models\Reactie;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function getDashboardData($url){
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $output contains the output string
        $response = json_decode(curl_exec($ch));
        // close curl resource to free up system resources
        curl_close($ch);
        return $response;
    }

    public function test()
    {
    	//retrieve personal postits
        if(Postit::where('user_id', Auth::user()->id)->exists()){
            $postits = Postit::where('user_id', Auth::user()->id)->get();
        }
        else{
            $postits = collect();
        }

        $page = "dashboard";
        $sub = "";
        return view('welcome', compact('postits', 'page', 'sub'));
    }

    public function getMonth($var){
        switch ($var) {
            case 1:
                return 'jan';
                break;

            case 2:
                return 'feb';
                break;

            case 3:
                return 'mrt';
                break;

            case 4:
                return 'apr';
                break;

            case 5:
                return 'mei';
                break;

            case 6:
                return 'jun';
                break;

            case 7:
                return 'jul';
                break;

            case 8:
                return 'aug';
                break;

            case 9:
                return 'sept';
                break;

            case 10:
                return 'okt';
                break;

            case 11:
                return 'nov';
                break;

            case 12:
                return 'dec';
                break;

            default:
                return 'error';
                break;
        }
    }

    public function index(){
        //clean up sha

        return redirect('/home');

    }

}
