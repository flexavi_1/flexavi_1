<?php



namespace App\Http\Controllers;



use Request;

use App\Models\Client;

use App\Models\ClientType;

use App\Models\Employee;

use Datatables;

use App\Models\User;

use App\Models\Appointment;

use App\Models\AppointmentType;

use App\Models\History;

use App\Models\Status;

use Illuminate\Support\Facades\Log;

use App\Models\Status_has_appointment;

use App\Models\Status_has_factuur;

use App\Models\Appointment_has_activities;

use App\Models\Activiteit_has_offerte;

use App\Models\Work;

use Auth;

use App\Models\Factuur;

use App\Models\Klant_has_status;

use DB;

use App\Models\Crediteur;

use App\Models\Offerte;

use App\Models\Offerte_has_status;

use Datetime;

use App\Models\Werkdag;

use Yajra\Datatables\Html\Builder; //

use App\DataTables\KlantDatatable;

use App\Models\Uur;

use App\Models\Aanvulling;

use App\Models\Verzonden_aan;

use App\Models\Reactie;

use App\Models\Worklist;

use App\Models\Worklist_has_appointments;

use App\Models\Auto;

use App\Models\Afspraak_has_geschiedenis;

use App\Models\Functie_has_Werknemer;

use App\Models\Functie;

use App\Models\Loopdata;

use App\Models\Bedrijf;



class PlanningController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');
    }

    public function refreshCalender()
    {
        //get alle afspraken
        $afspraken = Appointment::where('startdatum', '>', '2020-09-30 00:00:00')->get();
        // dd($afspraken);
        //voor elke afspraak
        foreach ($afspraken as $a) {
            //bepaal datum bereik van afspraak
            $dateArray = $a->getDateDifferenceDateArray();
            //alleen uitvoeren als we meerdere data hebben.
            if (count($dateArray) > 1) {
                // dd($dateArray);
                //controleer voor elke datum of er een werkdag bestaat
                foreach ($dateArray as $date) {
                    //if exists
                    if (!Werkdag::where('datum', date('Y-m-d', strtotime($date)))->exists()) {
                        //alleen iets mee doen wanneer die niet bestaat.
                        $wd_id = Werkdag::createNew($date);
                        $wd = Werkdag::find($wd_id);
                        for ($i = 0; $i < $wd->aantal_lijsten; $i++) {
                            Worklist::createNew($date->datum, $wd->id);
                        }
                    }
                }
            }
        }
    }


    public function todaySale($date)
    {



        $datum = $date;

        $date = date('Y-m-d', strtotime($date));



        // dd($ahas);



        $currentWorkdayID = Werkdag::createNew($date);



        $verzettend = DB::select("
    select a.*, a.id As afspraakID, wha.*, sha.status_id, k.woonplaats, k.straat, k.postcode
    from afspraak as a
    left join werklijst_has_afspraak as wha on wha.afspraak_id = a.id
    join status_has_afspraak as sha on sha.afspraak_id = a.id
    join klant as k on k.id = a.klant_id
    where sha.werkdag_id = '" . $currentWorkdayID . "'
    AND wha.afspraak_id IS NULL AND sha.status_id != 23
    order by a.startdatum ASC, k.woonplaats ASC, k.postcode ASC, k.straat ASC
");


        $ids = array();

        $werklijstenTest = Worklist::where('werkdag_id', $currentWorkdayID)->get();

        foreach ($werklijstenTest as $wl) {

            foreach (Worklist_has_appointments::where('werklijst_id', $wl->id)->get() as $wha) {

                array_push($ids, $wha->afspraak_id);
            }
        }

        // dd($ids);

        $verzettenTest = DB::table('status_has_afspraak')->where('werkdag_id', $currentWorkdayID)

            ->where('status_id', '!=', 23)->whereNotIn('afspraak_id', $ids)->get();

        // dd($verzettenTest);



        // $afspraken = Afspraak::with('klant')->where('startdatum', '>=', $date)->where('einddatum', '>=', date('Y-m-d', strtotime($date)))->where('afspraaktype_id', '!=', 5)->get();

        $afspraken = collect();

        $afsps = collect(DB::select("
    select a.id as 'aid', a.*
    from afspraak as a
    left join werklijst_has_afspraak as wha on wha.afspraak_id = a.id
    join status_has_afspraak as sha on sha.afspraak_id = a.id
    join klant as k on k.id = a.klant_id
    where sha.werkdag_id = '" . $currentWorkdayID . "'
    AND sha.status_id != 23
    order by a.startdatum ASC, k.woonplaats ASC, k.postcode ASC, k.straat ASC
"));


        foreach ($afsps as $afs) {

            $afs->klant = Client::find($afs->klant_id);

            $afspraken->push(new Appointment((array)$afs));
        }



        $verzetten = (object) $verzettenTest;



        foreach ($verzetten as $verzet) {

            $verzet->afspraak = Appointment::find($verzet->afspraak_id);

            $verzet->afspraakID = $verzet->afspraak_id;

            $verzet->klant_id = Client::find($verzet->afspraak->klant_id);

            $verzet->afspraak->verwerkt_door = User::find($verzet->afspraak->verwerkt_door);

            // $verzet->afspraak_id = Afspraak::find($verzet->afspraak_id);

            if ($verzet->afspraak->bijgewerkt_door != 0) {
            }

            $verzet->afspraak->afspraaktype_id = AppointmentType::find($verzet->afspraak->afspraaktype_id);

            $verzet->geschiedenis = Afspraak_has_geschiedenis::with('geschiedenis')->where('afspraak_id', $verzet->afspraak->id)->get();

            $verzet->status_id = Status::find($verzet->status_id);
        }



        //plaats->straat->postcode

        // $verzetten = $verzetten->sortBy($verzetten->klant_id->woonplaats);



        $redenen = DB::select("
        select af.id as 'nieuwe_afspraak',
        date(af.startdatum) as 'huidige_datum',
        af.reden as 'lege_reden',
        af.afspraak_id as 'afgeleid_van',
        a.id as 'oude_afspraak',
        a.reden as 'reden',
        date(a.startdatum) as 'datum_oude_afspraak'
        from afspraak as a
        inner join afspraak as af on a.id = af.afspraak_id
        order by af.id DESC
    ");

        $temp = 0;

        $tempnieuw = 0;

        foreach ($redenen as $reden) {

            if ($temp != $reden->nieuwe_afspraak) {

                $temp = $reden->oude_afspraak;

                $tempnieuw = $reden->nieuwe_afspraak;
            } else {

                $reden->lege_reden = null;

                $reden->huidige_datum = null;

                $reden->nieuwe_afspraak = $tempnieuw;
            }
        }



        $werklijsten = Worklist::where('datum', $date)->get();

        $werkdag = Werkdag::where('datum', date('Y-m-d', strtotime($date)))->first();

        if ($werkdag == null) {

            return redirect('/planning/verkoop')->with('danger', 'Er staan nog geen klanten op ' . date('d-m-Y', strtotime($date)) . ' en kan derhalve niet gegenereerd worden.');
        }

        if ($werklijsten->count() < $werkdag->aantal_lijsten) {

            $amount = $werkdag->aantal_lijsten - $werklijsten->count();



            for ($i = 0; $i < $amount; $i++) {

                $templist = new Worklist;

                $templist->datum = $werkdag->datum;

                $templist->werkdag_id = $werkdag->id;

                $templist->opmerking = "Bel afwijkende klanten van te voren of ze wel thuis zijn, dit scheelt kilometers en tijd !";



                $templist->save();
            }
        }



        $werklijsten = Worklist::where('datum', $date)->get();



        foreach ($werklijsten as $wl) {

            if ($wl->verkoper != null) {

                $wl->verkoper = Employee::find($wl->verkoper);
            }

            if ($wl->veger != null) {

                $wl->veger = Employee::find($wl->veger);
            }

            if ($wl->auto_id != null) {

                $wl->auto_id = Auto::find($wl->auto_id);
            }
        }







        $whas = Worklist_has_appointments::with('werklijst')->whereHas('werklijst', function ($wl) use ($date) {

            $wl->where('datum', date('Y-m-d', strtotime($date)));
        })->orderBy('volgorde', 'ASC')->get();



        // dd($verzetten);

        if ($whas->isEmpty() && count((array)$verzetten) == 0) {

            $check = false;

            return redirect('/planning/verkoop')->with('danger', 'Er staan geen klanten op deze dag, dag kan niet gegenereerd worden.');
        }

        if (!$whas->isEmpty()) {

            foreach ($whas as $wha) {

                // echo $wha->afspraak_id.' -- AFSPRAAK <br>';

                $wha->afspraak = $wha->afspraak_id;

                if (!Appointment::find($wha->afspraak_id) and !Appointment::find($wha->afspraak)) {
                    $wha->delete();
                    unset($wha);
                } else {


                    $wha->afspraak_id = Appointment::find($wha->afspraak_id);
                    if (!isset($wha->afspraak_id->id) and $wha->afspraak !== null) {
                        $wha->afspraak_id = Appointment::find($wha->afspraak);
                    }

                    // dd($wha);

                    if (!$wha->afspraak_id == null) {

                        $wha->afspraak_id->bijgewerkt_door = User::find($wha->afspraak_id->bijgewerkt_door);

                        $wha->afspraak_id->geschreven_door = Employee::find($wha->afspraak_id->geschreven_door);

                        $wha->afspraak_id->klant = $wha->afspraak_id->klant_id;

                        $wha->afspraak_id->klant_id = Client::find($wha->afspraak_id->klant_id);

                        $wha->afspraak_id->verwerkt_door = User::find($wha->afspraak_id->verwerkt_door);

                        $wha->afspraak_id->afspraaktype_id = AppointmentType::find($wha->afspraak_id->afspraaktype_id);

                        $wha->afspraak_id->geschiedenis = Afspraak_has_geschiedenis::where('afspraak_id', $wha->afspraak_id->id)->get();
                    }

                    // echo $date.'-- Datum<br>';

                    $werkdag_id = Werkdag::createNew($date);

                    // echo $werkdag_id.' -- WERKDAG_ID<br>';

                    // echo $wha->afspraak_id->id.'-- AFSPRAAK ID<br>';

                    $sha = Status_has_appointment::where('afspraak_id', $wha->afspraak_id->id)->where('werkdag_id', $werkdag_id)->first();

                    // if($wha->afspraak_id->id == 661){

                    //     dd($sha);

                    // }

                    $wha->status_id = Status::find($sha->status_id);
                    if (!isset($wha->status_id->id)) {
                        dd($wha);
                    }
                }
            }
        }



        // foreach($verzetten as $verzet)

        // {

        //     foreach($whas as $wha)

        //     {



        //         if($verzet->id == $wha->afspraak_id->id)

        //         {

        //             unset($verzet);

        //         }

        //     }

        // }



        $kleuren = array(
            "md-bg-amber-700",    "md-bg-lime-400",        "md-bg-pink-200",       "md-bg-cyan-100",

            "md-bg-yellow-A400",   "md-bg-orange-A700",     "md-bg-brown-200",      "md-bg-blue-grey-300",

            "md-bg-yellow-700",    "md-bg-light-green-300", "md-bg-red-100",  "md-bg-purple-300",

            "md-bg-indigo-300",    "md-bg-deep-orange-A100", "md-bg-cyan-300", "md-bg-light-green-300",

            "md-bg-purple-200",    "md-bg-brown-300",       "md-bg-purple-400"
        );

        $kleurencombi = array();



        $plaatsen = array();

        $adres = array();

        $postcode = array();



        $i = 0;

        $frequencyCounter = 1;

        $appExistsInArray = false;

        $colorInArray = false;

        // echo "Start<br>";

        // dd($afspraken);

        foreach ($afspraken as $afspraak) {

            // echo "<br><br>i = ".$i;

            // echo "<br>frequencyCounter = ".$frequencyCounter;

            // echo "<br>appExistsInArray = ".$appExistsInArray;

            // echo "<br>colorInArray = ".$colorInArray;

            if ($i == count($kleuren) - 1) {
                $i = 0;
            } // reset



            for ($z = 0; $z < count($plaatsen); $z++) {

                if ($plaatsen[$z][0] == $afspraak->klant->woonplaats)

                    $appExistsInArray = true;
            }

            // echo "<br><br>woonplaats = ".$afspraak->klant->woonplaats;

            // echo "<br>appExistsInArray = ".$appExistsInArray;



            if (!$appExistsInArray) // woonplaats

            {

                array_push($plaatsen, [$afspraak->klant->woonplaats, $frequencyCounter]);

                // echo "<br><br>appExistsInArray == false<br> Array plaatsen aangevuld met nieuwe plaats";

            } else {

                for ($counter = 0; $counter < count($plaatsen); $counter++) {

                    if ($plaatsen[$counter][0] == $afspraak->klant->woonplaats) {

                        $plaatsen[$counter][1]++;

                        // echo '<br><br>Plaatsencounter :'.$plaatsen[$counter][1];

                        // echo '<br>Plaats :'.$plaatsen[$counter][0];

                        if ($plaatsen[$counter][1] > 1) {

                            for ($z = 0; $z < count($kleurencombi); $z++) {

                                // echo '<br>FOR: Kleurencombi[1]'.$kleurencombi[$z][1];

                                if ($kleurencombi[$z][1] == $plaatsen[$counter][0]) {

                                    $colorInArray = true;

                                    // echo '<br> colorInArray = true, deze bestaat al';

                                }
                            }

                            if (!$colorInArray) {

                                array_push($kleurencombi, [$kleuren[$i], $afspraak->klant->woonplaats]);

                                // echo '<br> Plaats aan kleurenarray toegevoegd';

                                $i++;
                            }

                            $colorInArray = false;
                        }
                    }
                }

                $appExistsInArray = false;
            }
        }

        // for debugging

        // foreach($plaatsen[0] as $child){

        //     echo $child."\n";

        // }

        // echo '<br>';



        // foreach($kleurencombi[0] as $child){

        //     echo $child."\n";

        // }

        // echo "<br>EINDE 1<br>";

        // dd($plaatsen);

        $i = 0;

        $frequencyCounter = 1;

        $appExistsInArray = false;

        $colorInArray = false;

        // echo "Start<br>";

        foreach ($afspraken as $afspraak) {

            // print_r($adres);

            if ($i == count($kleuren) - 1) {
                $i = 0;
            } // reset

            // print_r($afspraak->klant->adres);

            for ($z = 0; $z < count($adres); $z++) {

                if ($adres[$z][0] == $afspraak->klant->straat)

                    $appExistsInArray = true;
            }



            if (!$appExistsInArray) // woonplaats

            {

                array_push($adres, [$afspraak->klant->straat, $frequencyCounter]);
            } else {

                for ($counter = 0; $counter < count($adres); $counter++) {

                    if ($adres[$counter][0] == $afspraak->klant->straat) {

                        $adres[$counter][1]++;

                        if ($adres[$counter][1] > 1) {

                            for ($z = 0; $z < count($kleurencombi); $z++) {

                                if ($kleurencombi[$z][1] == $adres[$counter][0]) {

                                    $colorInArray = true;
                                }
                            }

                            if (!$colorInArray) {

                                array_push($kleurencombi, [$kleuren[$i], $afspraak->klant->straat]);

                                $i++;
                            }

                            $colorInArray = false;
                        }
                    }
                }

                $appExistsInArray = false;
            }
        }

        // for debugging

        // foreach($adres[0] as $child){

        //     echo $child."\n";

        // }

        // echo '<br>';



        // foreach($kleurencombi[0] as $child){

        //     echo $child."\n";

        // }

        // echo "<br>EINDE 2<br>";

        // dd($afspraken);



        $i = 0;

        $frequencyCounter = 1;

        $appExistsInArray = false;

        $colorInArray = false;

        foreach ($afspraken as $afspraak) {

            if ($i == count($kleuren) - 1) {
                $i = 0;
            } // reset



            for ($z = 0; $z < count($postcode); $z++) {

                if ($postcode[$z][0] == $afspraak->klant->postcode)

                    $appExistsInArray = true;
            }



            if (!$appExistsInArray) // woonplaats

            {

                array_push($postcode, [$afspraak->klant->postcode, $frequencyCounter]);
            } else {

                for ($counter = 0; $counter < count($postcode); $counter++) {

                    if ($postcode[$counter][0] == $afspraak->klant->postcode) {

                        $postcode[$counter][1]++;

                        if ($postcode[$counter][1] > 1) {



                            for ($z = 0; $z < count($kleurencombi); $z++) {

                                if ($kleurencombi[$z][1] == $postcode[$counter][0]) {

                                    $colorInArray = true;
                                }
                            }

                            if (!$colorInArray) {

                                array_push($kleurencombi, [$kleuren[$i], $afspraak->klant->postcode]);

                                $i++;
                            }

                            $colorInArray = false;
                        }
                    }
                }

                $appExistsInArray = false;
            }
        }

        // for debugging

        // foreach($postcode[0] as $child){

        //     echo $child."\n";

        // }

        // echo '<br>';



        // foreach($kleurencombi[0] as $child){

        //     echo $child."\n";

        // }

        // echo "<br>EINDE 3<br>";





        // dd($verzettend);





        // dd($kleurencombi);

        $page = "planning";

        $sub = "verkvand";

        return view('planning.vandaagVerkooptwee', compact('date', 'verzetten',  'redenen', 'werklijsten', 'whas', 'datum', 'kleurencombi',  'page', 'sub'));
    }



    /**

     * Update today schedule

     *

     * @param Request $request

     * @return void

     */

    public function updateSchedule($datum)
    {
        $input = Request::all();
        // Log::info($input);
        // loop over each of the werklijst identifiers and get the afspraak ids
        foreach ($input as $werklijst_id => $afspraak_id_arr) {

            // loop over each of the afspraak ids

            foreach ($afspraak_id_arr as $order_id => $afspraak_id) {

                // make sure we get status_has_afspraak if we need one for a werklijst
                $processed = false;
                $afspraak = Appointment::find($afspraak_id);
                //delete appointment from list
                $werkdag = (object) [];
                foreach ($input as $wl_id => $afspraak_arr) {
                    // Log::info('inside array');
                    // Log::info($wl_id);
                    if (is_numeric($wl_id)) {
                        $werkdag = Werkdag::find(Worklist::find($wl_id)->werkdag_id);
                        // Log::info($werkdag);
                        break;
                    }
                }
                //remove appointment from worklists
                $whas = Worklist::with('wha')->where('werkdag_id', $werkdag->id)->get();
                foreach ($whas as $wha) {
                    foreach ($wha->wha as $werklijst_has_afspraken) {
                        // Log::info('Printing WHA');
                        // Log::info($werklijst_has_afspraken);
                        // Log::info('Printing WHA -> WHA');
                        // Log::info($wha->wha);
                        if ($werklijst_has_afspraken->afspraak_id == $afspraak->id) {
                            $werklijst_has_afspraken->delete();
                        }
                    }
                }
                if (is_numeric($werklijst_id)) {
                    //if numeric we need to create a new record
                    $werklijst_has_afspraak = new Worklist_has_appointments;
                    $werklijst_has_afspraak->werklijst_id = $werklijst_id;
                    $werklijst_has_afspraak->afspraak_id = $afspraak_id;
                    $werklijst_has_afspraak->start = $afspraak->startdatum;
                    $werklijst_has_afspraak->eind = $afspraak->einddatum;
                    $werklijst_has_afspraak->volgorde = $order_id + 1;
                    $werklijst_has_afspraak->save();
                }




                // $werklijst_has_afspraak = new Werklijst_has_afspraak;
                // if(is_numeric($werklijst_id)){
                //     if($afspraak->getDateDifference()->d > 1){

                //         if($afspraak_id == 6690){
                //             Log::info('herkend als afspraak over meerdere dagen');
                //         }
                //         //als target is werklijst
                //         if(is_numeric($werklijst_id)){
                //             //haal alle werklijsten op waarin afspraak id is ingedeeld
                //             $whas = Werklijst_has_afspraak::where('afspraak_id', $afspraak_id)->get();
                //             $existsInTable = false;
                //             //voor elke werklijst
                //             foreach ($whas as $wha) {
                //                 //check of combi werklijst afspraak_id bestaat
                //                 if(date('Y-m-d', strtotime($wha->start)) == date('Y-m-d', strtotime($datum))){
                //                     //this is the record
                //                     $existsInTable = true;
                //                     $werklijst_has_afspraak = $wha;
                //                 }
                //             }
                //             //als exists in table false is, dat betekent dat de afspraak wellicht voorkomt in een andere dag, dus dat we voor deze werklijst een nieuw record moeten aanmaken
                //             if(!$existsInTable) {
                //                 $werklijst_has_afspraak = new Werklijst_has_afspraak;
                //             }

                //             $werklijst_has_afspraak->werklijst_id = $werklijst_id;
                //             $werklijst_has_afspraak->afspraak_id = $afspraak_id;
                //             $werklijst_has_afspraak->start = $afspraak->startdatum;
                //             $werklijst_has_afspraak->eind = $afspraak->einddatum;

                //         }
                //     }
                //     else {

                //         $werklijst_has_afspraak = "";
                //         if(Werklijst_has_afspraak::where('afspraak_id', $afspraak->id)->exists()){
                //             $werklijst_has_afspraak = Werklijst_has_afspraak::where('afspraak_id', $afspraak->id)->first();
                //         } else {
                //             $werklijst_has_afspraak = new Werklijst_has_afspraak;
                //         }

                //         if($afspraak_id == 7495){
                //             Log::info('herkend als afspraak over 1 dag');
                //             Log::info('WerklijstID: '.$werklijst_id);
                //             Log::info('Afspraak_id: '.$afspraak_id);
                //             Log::info($werklijst_has_afspraak);
                //             Log::info(Werklijst_has_afspraak::where('afspraak_id', $afspraak->id)->first());
                //         }
                //         $werklijst_has_afspraak->werklijst_id = $werklijst_id;
                //         $werklijst_has_afspraak->afspraak_id = $afspraak_id;
                //         $werklijst_has_afspraak->start = $afspraak->startdatum;
                //         $werklijst_has_afspraak->eind = $afspraak->einddatum;
                //     }

                //     $werklijst_has_afspraak->volgorde = $order_id+1;

                //     $werklijst_has_afspraak->werklijst_id = $werklijst_id;

                //     $werklijst_has_afspraak->save();
                // } else{
                //     if(Werklijst_has_afspraak::where('afspraak_id', $afspraak->id)->exists()){
                //         $werklijst_has_afspraak = Werklijst_has_afspraak::where('afspraak_id', $afspraak->id)->first();
                //     }

                //     $werklijst_has_afspraak->delete();
                // }


                // // if we are dealing with a numeric list (werklijst), then we save it

                // if (is_numeric($werklijst_id)) {

                //     $werklijst_has_afspraak->volgorde = $order_id+1;

                //     $werklijst_has_afspraak->werklijst_id = $werklijst_id;

                //     $werklijst_has_afspraak->save();

                // } else {

                //     // since we are not dealing with an actual list, we are dealing with the "verzetten" lijst.

                //     // if we have a werklijst_has_afspraak record, let's therefore delete it

                //     if ($werklijst_has_afspraak) {

                //         $werklijst_has_afspraak->delete();

                //     }

                // }
                // }

            }
        }
    }



    public function todayAdmin($date)

    {

        $date = date('Y-m-d', strtotime($date));



        if (date('N', strtotime($date)) > 5) {

            $danger = "Op deze dag staat niets ingepland voor kantoor.";



            $page = "planning";

            $sub = "kantvand";

            return view('planning.vandaagKantoor', compact('danger', 'page', 'sub'));
        }



        $role = Auth::user()->rol;





        $relaties = Appointment::with('klant', 'sha')->where('einddatum', '<=', date('Y-m-d H:i'))->whereHas('sha', function ($sha) {

            $sha->where('status_id', 1);
        })->get();



        $reminders = DB::select("
        select *, datediff(CURRENT_DATE(), khs.updated_at) as verschil, date(Date_add(khs.updated_at, INTERVAL 1 YEAR)) as reminderdatum
        from klant as k
        join klant_has_status as khs on k.id = khs.klant_id
        join status as s on s.id = khs.status_id
        where khs.status_id = 21 AND datediff(date(Date_add(khs.updated_at, INTERVAL 1 YEAR)), khs.updated_at) < 30
        order by datediff(CURRENT_DATE(), khs.updated_at) desc
        limit 15
    ");







    $tbers = DB::select("
    select k.id as 'klant_id',
        k.voornaam,
        k.achternaam,
        k.straat,
        k.huisnummer,
        k.hn_prefix,
        k.postcode,
        k.woonplaats,
        k.email,
        k.telefoonnummer_prive,
        k.telefoonnummer_zakelijk,
        af.id as 'afspraak_id',
        af.reden
    from status_has_afspraak as sha
    join afspraak as af on af.id = sha.afspraak_id
    left outer join afspraak as a on a.id = af.afspraak_id
    join klant as k on k.id = af.klant_id
    where sha.status_id = 2 AND af.is_verzet = 0
");


        // dd($reminders);



        foreach ($reminders as $relatie) {

            $relatie->reminderdatum = date('d-m-Y', strtotime($relatie->reminderdatum));
        }



        $page = "planning";

        $sub = "kantvand";





        return view('planning.vandaagKantoor', compact('relaties', 'reminders', 'tbers', 'page', 'sub', 'date'));
    }



    public function test()

    {

        $allDates = DB::select(DB::raw("SELECT date(a.startdatum) as 'datum'

                                            FROM afspraak as a

                                            JOIN status_has_afspraak as sha

                                            on sha.afspraak_id = a.id

                                            where sha.status_id = 1

                                            group by date(a.startdatum)

                                            order by a.startdatum asc")); // get all dates from appointments where status = 1 <-- This doesnt fully work yet with appointments who are planned on multiple days.

        // dd($allDates);

        $countArray = array();

        // $allAppointments = null;

        foreach ($allDates as $date) {

            if (!Werkdag::where('datum', date('Y-m-d', strtotime($date->datum)))->exists()) { //if there isnt a Werkdag record, we have to create the workday.

                Werkdag::createNew($date->datum);

                if (!Worklist::where('datum', date('Y-m-d', strtotime($date->datum)))->exists()) { //if there are no worklists for that date, we need to create three.

                    $wd = Werkdag::where('datum', date('Y-m-d', strtotime($date->datum)))->first();

                    for ($i = 0; $i < $wd->aantal_lijsten; $i++) {

                        Worklist::createNew($date->datum, $wd->id);
                    }
                }
            }

            if (!Werkdag::where('datum', date('Y-m-d', strtotime($date->datum)))->where('behandeld', 2)->exists()) { //if workday has not been processed

                $allAppointments = DB::select(DB::raw("select date(a.startdatum) as 'date', count(a.afspraaktype_id) as 'aantal', a.afspraaktype_id, at.omschrijving, (select count(id) from afspraak where date(startdatum) = '" . date('Y-m-d', strtotime($date->datum)) . "' OR (date(startdatum) < '" . date('Y-m-d', strtotime($date->datum)) . "' AND '" . date('Y-m-d', strtotime($date->datum)) . "' < date(einddatum))) as 'totaal2', avg(sha.status_id) as 'totaal'

                                                        from afspraak a

                                                        join afspraaktype as at

                                                        on at.id = a.afspraaktype_id

                                                        join status_has_afspraak as sha

                                                        on sha.afspraak_id = a.id

                                                        where sha.status_id = 1 AND date(a.startdatum) = '" . date('Y-m-d', strtotime($date->datum)) . "' OR (date(a.startdatum) < '" . date('Y-m-d', strtotime($date->datum)) . "' AND '" . date('Y-m-d', strtotime($date->datum)) . "' < date(a.einddatum))

                                                        group by a.afspraaktype_id")); // get all appointments on this day

                // dd($allAppointments);

                foreach ($allAppointments as $aa) {

                    // if($aa->date == '2018-06-15'){ dd($aa); }

                    array_push($countArray, ['date' => $aa->date, 'aantal' => $aa->aantal, 'afspraaktype_id' => $aa->afspraaktype_id, 'afspraaktype' => $aa->omschrijving, 'totaal' => $aa->totaal]);
                }
            }
        }

        // dd($countArray);

        $aantalperdag = array();

        $date = null;

        $garantie = 0;

        $ub = 0;

        $uitvoeren = 0;

        $offerte = 0;

        $tbv = 0;

        $total = 0;

        $count = 1;

        foreach ($countArray as $ca) {

            if ($date == null) {

                $date = $ca['date'];
            }

            // echo 'datum: '.$ca['date']." Aantal in ca ".$ca['aantal'].' afspraaktype '.$ca['afspraaktype'].' TOTAAL voor 2e if = '.$total.'-- total is nog de oude total<br>';

            if (($ca['date'] != $date && $total > 0) || count($countArray) == 1) { // its another workday. We need to fill the array now before resetting.

                array_push($aantalperdag, ['datum' => $date, 'total' => $total, 'aantalUW' => $uitvoeren, 'aantalUB' => $ub, 'aantalGA' => $garantie, 'aantalOF' => $offerte, 'aantalTBV' => $tbv]);

                $date = $ca['date'];

                $garantie = 0;

                $ub = 0;

                $uitvoeren = 0;

                $offerte = 0;

                $tbv = 0;

                $total = 0;
            }

            // echo 'datum: '.$ca['date']." Aantal in ca ".$ca['aantal'].' afspraaktype '.$ca['afspraaktype'].' TOTAAL voor switch '.$total.'-- total is hier gereset<br>';

            if ($date != null) {

                // $total += $ca['aantal'];

                switch ($ca['afspraaktype_id']) { //depending on afspraaktype_id, we need to fill vars.

                    case 1:

                        $uitvoeren += $ca['aantal'];

                        $total += $ca['aantal'];

                        break;

                    case 2:

                        $offerte += $ca['aantal'];

                        $total += $ca['aantal'];

                        break;

                    case 3:

                        $ub += $ca['aantal'];

                        $total += $ca['aantal'];

                        break;

                    case 4:

                        $garantie += $ca['aantal'];

                        $total += $ca['aantal'];

                        break;

                    case 6:

                        $tbv += $ca['aantal'];

                        $total += $ca['aantal'];

                        break;



                    default:

                        # code...

                        break;
                }

                // $total = $uitvoeren + $ub + $offerte + $garantie + $tbv;

                // $total = $uitvoeren + $offerte + $ub + $garantie + $tbv;

            }

            // echo 'datum: '.$ca['date']." Aantal in ca ".$ca['aantal'].' afspraaktype '.$ca['afspraaktype'].' TOTAAL na de switch '.$total.'-- nieuwe total<br>';

        }

        // dd($aantalperdag);

        $werkdagen = Werkdag::where('behandeld', '<', 2)->get();

        // $aantalperdag = DB::select( DB::raw("Select date(afspraak.startdatum) as 'datum', count(afspraak.id) as 'aantal', sha.status_id

        //                                     From afspraak

        //                                     join status_has_afspraak as sha

        //                                     on sha.afspraak_id = afspraak.id

        //                                     where sha.status_id = 1 AND afspraak.afspraaktype_id != 3 AND afspraak.afspraaktype_id < 5

        //                                     Group by date(afspraak.startdatum)"));



        // $aantalubperdag = DB::select( DB::raw("Select date(afspraak.startdatum) as 'datum', count(afspraak.id) as 'aantal', sha.status_id

        //                                         from afspraak

        //                                         join status_has_afspraak as sha

        //                                         on sha.afspraak_id = afspraak.id

        //                                         where sha.status_id = 1 AND afspraak.afspraaktype_id = 3

        //                                         group by date(afspraak.startdatum)"));





        // foreach($aantalperdag as $ap)

        // {

        //     if(Werkdag::where('datum', $ap->datum)->count() == 0 || Werklijst::where('datum', $ap->datum)->count() == 0)

        //     {

        //         if(Werkdag::where('datum', $ap->datum)->count() == 0)

        //         {

        //             $tempwd = new Werkdag;

        //             $tempwd->datum = $ap->datum;

        //             $tempwd->save();

        //         }



        //         if(Werklijst::where('datum', $ap->datum)->count() == 0)

        //         {

        //             $werkdag = Werkdag::where('datum', $ap->datum)->first();

        //             for($i = 0; $i < $werkdag->aantal_lijsten; $i++)

        //             {

        //                 $temp = new Werklijst;

        //                 $temp->datum = $werkdag->datum;

        //                 $temp->werkdag_id = $werkdag->id;

        //                 $temp->save();

        //             }

        //         }

        //     }

        // }



        $page = "planning";

        $sub = "planverk";



        return view('planning.verkoopTeste', compact('werkdagen', 'aantalperdag', 'page', 'sub'));
    }



    public function Salesplanning()
    {

        $result = Werkdag::refreshCalender();
        // dd($result);
        // dd('test');
        $allDates = DB::table('afspraak as a')
            ->join('status_has_afspraak as sha', 'sha.afspraak_id', '=', 'a.id')
            ->select(DB::raw('DATE(a.startdatum) as datum'))
            ->where('sha.status_id', 1)
            ->groupBy(DB::raw('DATE(a.startdatum)'))
            ->orderBy('a.startdatum', 'asc')
            ->get(); // get all dates from appointments where status = 1 <-- This doesnt fully work yet with appointments who are planned on multiple days.

        // dd($allDates);

        $countArray = array();

        // $allAppointments = null;

        foreach ($allDates as $date) {

            if (!Werkdag::where('datum', date('Y-m-d', strtotime($date->datum)))->exists()) { //if there isnt a Werkdag record, we have to create the workday.

                Werkdag::createNew($date->datum);

                if (!Worklist::where('datum', date('Y-m-d', strtotime($date->datum)))->exists()) { //if there are no worklists for that date, we need to create three.

                    $wd = Werkdag::where('datum', date('Y-m-d', strtotime($date->datum)))->first();

                    for ($i = 0; $i < $wd->aantal_lijsten; $i++) {

                        Worklist::createNew($date->datum, $wd->id);
                    }
                }
            }
        }

        $werkdagen = Werkdag::where('behandeld', 0)->where('datum', '>', date('Y-m-d', strtotime('01-10-2020')))->orderBy('datum', 'ASC')->get();
        // dd($werkdagen);
        foreach ($werkdagen as $key => $wd) {

            $check = false;

            foreach ($allDates as $ad) {

                if (date('d-m-Y', strtotime($ad->datum)) == date('d-m-Y', strtotime($wd->datum))) {
                    //afspraken die al verwerkt zijn (en dus hele werkdagen), komen niet voor in de alldate array. daarom moet deze later weggehaald worden.
                    //als werkdag in de toekomst is, moet dit niet!

                    //hij komt voor in de all date array, check is true
                    $check = true;
                }

                // echo date('d-m-Y', strtotime($wd->datum)).' wordt vergeleken met '.date('d-m-Y').'<br>';
                // echo (date('Y-m-d', strtotime($wd->datum)) > date('Y-m-d')) ? 'check true' : 'check niet true';
                // echo '<br>';
                if (date('Y-m-d', strtotime($wd->datum)) > date('Y-m-d')) {
                    $check = true;
                }
            }

            if (!$check) {
                //als check false is wordt die niet getoond in het overzicht.
                // unset($key);

                // echo $wd->datum.' wordt verwijderd'.'<br>';

                $werkdagen->forget($key);
            } else {
                if ($wd->getAantalKlanten() < 1) {
                    $werkdagen->forget($key);
                }
            }
        }




        $page = "planning";

        $sub = "planverk";



        return view('planning.verkoop', compact('werkdagen', 'page', 'sub'));
    }



    public function setCheckWorkday($date, $werkdag_id)

    {

        $werkdag = Werkdag::find($werkdag_id);

        $naam = Bedrijf::find(1)->naam;

        if ($naam != 'Centraal Woning Beheer B.V.' && $naam != 'Dakdekkerservice' && $naam != 'Huis Beheer Nederland B.V.') {

            $werkdag = Werkdag::find($werkdag_id);

            // dd($werkdag);

            if ($werkdag->ublCheck == 0)

                return redirect('/planning/verkoop')->with('danger', 'De uitbetaallijst is nog niet verwerkt, voer deze stap eerst uit.');





            $werkdag->behandeld = 1;

            $werkdag->save();



            return redirect('/planning/verkoop')->with('succes', 'U heeft de werkdag behandeld, de controle kan nu plaatsvinden door uw leidinggevende.');
        }





        $verkoperString = 'U kunt de werkdag nog niet afsluiten. Er staan nog afspraken open in: <br>';

        $verkoperArray = array();

        if ($werkdag->getAantalKlanten() == 0) {

            return redirect('/planning/verkoop')->with('danger', 'Op deze dag staan geen klanten en kan derhalve nog niet afgesloten worden.');
        }

        //get all appointments with status 1 (not processed)

        $afspraken = Status_has_appointment::where('werkdag_id', $werkdag_id)->where('status_id', 1)->get();

        if (count($afspraken) > 0) { // if null, all appointments are processed

            foreach ($afspraken as $afspraak) {

                $werklijst = Worklist::findWorklistFromAppointment($afspraak->id, $werkdag->id); //if no worklist is returned, its in the 'verzetten' list

                if ($werklijst) { //its in this worklist

                    $verkoper = Employee::find($werklijst->verkoper);

                    if (!in_array('in de lijst van ' . $verkoper->voornaam, $verkoperArray)) {

                        array_push($verkoperArray, 'in de lijst van ' . $verkoper->voornaam);
                    }
                } else {

                    if (!in_array('verzetlijst', $verkoperArray)) {

                        array_push($verkoperArray, 'Verzetlijst');
                    }
                }
            }

            foreach ($verkoperArray as $va) {

                $verkoperString .= $va . '<br>';
            }

            return redirect('/planning/verkoop')->with('danger', $verkoperString);
        }



        $werkdag->behandeld = 1;

        $werkdag->save();



        return redirect('/planning/verkoop')->with('succes', 'U heeft de werkdag afgesloten.');
    }



    public function editWorkday($date, $werkdag_id)

    {

        $werkdag = Werkdag::find($werkdag_id);



        $page = "planning";

        $sub = "plankant";



        return view('planning.wijzigwerkdag', compact('werkdag', 'date', 'page', 'sub'));
    }



    public function saveWorkday($date, $werkdag_id)

    {

        $input = Request::all();



        $werkdag = Werkdag::find($werkdag_id);

        $werkdag->aantal_lijsten = $input['aantal_lijsten'];

        $werkdag->aantal_klanten = $input['aantal_klanten'];

        $werkdag->save();



        return redirect('/planning/verkoop')->with('succes', 'U heeft de werkdag succesvol gewijzigd.');
    }



    public function getSettings($date, $werklijst_id)

    {

        $werklijst = Worklist::find($werklijst_id);

        $werkdag = Werkdag::find($werklijst->werkdag_id);

        $werknemers = Employee::where('actief', 1)->get();

        $autos = Auto::all();



        $page = "planning";

        $sub = "plankant";



        return view('planning.getSettings', compact('werklijst', 'werkdag', 'date', 'werknemers', 'autos', 'page', 'sub'));
    }



    public function setSettings($date, $werklijst_id)

    {

        $input = Request::all();



        $werklijst = Worklist::find($werklijst_id);

        $werklijst->verkoper = $input['verkoper'];

        $werklijst->veger = $input['veger'];

        $werklijst->afstand = $input['afstand'];

        $werklijst->wisselgeld = $input['wisselgeld'];

        $werklijst->aantal_facturen = $input['aantal_facturen'];

        $werklijst->aantal_wo = $input['aantal_wo'];

        $werklijst->opmerking = $input['opmerking'];

        $werklijst->auto_id = $input['auto_id'];



        $werklijst->save();



        return redirect('/planning/verkoop/' . $date)->with('succes', 'U hebt de werklijst succesvol aangepast');
    }



    public function returnWerklijst($date, $werklijst_id)

    {

        $werklijst = Worklist::find($werklijst_id);

        $werklijst->verkoper = Employee::find($werklijst->verkoper);

        $werklijst->veger = Employee::find($werklijst->veger);

        $werklijst->auto_id = Auto::find($werklijst->auto_id);

        // $whas = Werklijst_has_afspraak::where('werklijst_id', $werklijst_id)->orderBy('volgorde', 'ASC')->get();

        // foreach($whas as $wha){

        //     if(Status_has_afspraak::where('afspraak_id', $wha->afspraak_id)->first()->status_id > 1){

        //         $wha->delete();

        //     }

        // }

        $whas = Worklist_has_appointments::where('werklijst_id', $werklijst_id)->orderBy('volgorde', 'ASC')->get();

        foreach ($whas as $key => $wha) {

            $werklijst = Worklist::find($wha->werklijst_id);

            // echo $wha->werklijst_id.'<br>';

            $wd = Werkdag::createNew($werklijst->datum);

            // echo $wd;

            if (Status_has_appointment::where('afspraak_id', $wha->afspraak_id)->where('werkdag_id', $wd)->first()->status_id != 1) {

                // unset($wha);

                $whas->forget($key);
            }
        }

        $rood = Worklist_has_appointments::where('werklijst_id', $werklijst_id)->orderBy('volgorde', 'ASC')->get();

        foreach ($rood as $key => $wha) {

            $werklijst = Worklist::find($wha->werklijst_id);

            // echo $wha->werklijst_id.'<br>';

            $wd = Werkdag::createNew($werklijst->datum);

            // echo $wd;

            if (Status_has_appointment::where('afspraak_id', $wha->afspraak_id)->where('werkdag_id', $wd)->first()->status_id != 1) {

                // unset($wha);

                $whas->forget($key);
            }
        }

        $aantal_garanties = 0;

        $aantal_klanten = 0;



        foreach ($whas as $wha) {

            $wha->afspraak_id = Appointment::find($wha->afspraak_id);

            $wha->afspraak_id->klant_id = Client::find($wha->afspraak_id->klant_id);

            $wha->afspraak_id->afspraaktype_id = AppointmentType::find($wha->afspraak_id->afspraaktype_id);

            if ($wha->afspraak_id->afspraaktype_id->omschrijving == "Uitgestelde Betaling") {

                $aantal_garanties++;
            } else {

                $aantal_klanten++;
            }
        }

        foreach ($rood as $wha) {

            $wha->afspraak_id = Appointment::find($wha->afspraak_id);

            $wha->afspraak_id->klant_id = Client::find($wha->afspraak_id->klant_id);

            $wha->afspraak_id->afspraaktype_id = AppointmentType::find($wha->afspraak_id->afspraaktype_id);
        }



        $ahas = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function ($afs) use ($werklijst) {

            $afs->where('startdatum', '>=', $werklijst->datum)->where('startdatum', '<', date('Y-m-d', strtotime("+1 day", strtotime($werklijst->datum))));
        })->get();



        // dd($ahas);



        $ghas = DB::select(DB::raw("select a.id as 'afspraak_id', g.id as 'geschiedenis_id', wha.werklijst_id as 'werklijst_id', g.omschrijving as 'omschrijving', w.datum as 'datum'

                    FROM geschiedenis as g

                    join afspraak_has_geschiedenis as ahg

                    on g.id = ahg.geschiedenis_id

                    join afspraak as a

                    on a.id = ahg.afspraak_id

                    join werklijst_has_afspraak as wha

                    on wha.afspraak_id = a.id

                    join werklijst as w

                    on w.id = wha.werklijst_id

                    where w.datum = '" . date('Y-m-d', strtotime($werklijst->datum)) . "'"));



        $redenen = DB::select(DB::raw("select af.id as 'nieuwe_afspraak',date(af.startdatum) 'huidige_datum', af.reden as 'lege_reden', af.afspraak_id as 'afgeleid_van', a.id as 'oude_afspraak', a.reden as 'reden', date(a.startdatum) as 'datum_oude_afspraak'

                                         FROM afspraak as a

                                         inner join afspraak as af on a.id = af.afspraak_id

                                         order by af.id DESC"));

        $temp = 0;

        $tempnieuw = 0;

        foreach ($redenen as $reden) {

            if ($temp != $reden->nieuwe_afspraak) {

                $temp = $reden->oude_afspraak;

                $tempnieuw = $reden->nieuwe_afspraak;
            } else {

                $reden->lege_reden = null;

                $reden->huidige_datum = null;

                $reden->nieuwe_afspraak = $tempnieuw;
            }
        }



        foreach ($redenen as $r) {

            $r->reden = str_replace('<p>', " ", $r->reden);

            $r->reden = str_replace('</p>', " ", $r->reden);

            $r->reden = str_replace(';', "<br>", $r->reden);
        }



        $page = "planning";

        $sub = "planverk";



        return view('planning.werklijst', compact('whas', 'aantal_garanties', 'aantal_klanten', 'werklijst', 'rood', 'ahas', 'ghas', 'redenen', 'page', 'sub'));
    }



    public function deleteWorklist($date, $werklijst_id)

    {

        $werklijst = Worklist::find($werklijst_id);

        $whas = Worklist_has_appointments::where('werklijst_id', $werklijst_id)->get();



        if (!$whas->isEmpty()) {

            foreach ($whas as $wha) {

                $wha->delete();
            }
        }



        $werkdag = Werkdag::find($werklijst->werkdag_id);

        $werkdag->aantal_lijsten--;

        $werkdag->save();



        $werklijst->delete();







        return redirect('/planning/verkoop/' . $date)->with('succes', 'Werklijst is succesvol verwijderd.');
    }



    public function showTelling()

    {

        $aantalperdag = DB::select("
        SELECT date(afspraak.startdatum) AS 'datum', count(afspraak.id) AS 'aantal', sha.status_id
FROM afspraak
JOIN status_has_afspraak AS sha ON sha.afspraak_id = afspraak.id
WHERE sha.status_id != 23 AND date(afspraak.startdatum) > now()
GROUP BY date(afspraak.startdatum), sha.status_id  -- Add sha.status_id here
    ");


        $aantalperdag = collect($aantalperdag);



        foreach ($aantalperdag as $ap) {

            $i = 0;

            $date = new DateTime($ap->datum);

            $dnw = date('N', strtotime($date->format('Y-m-d')));

            $dnw--;

            $date->modify('-' . $dnw . 'day');



            while ($i < 5) {

                // echo '<br>'.$date->format('Y-m-d');

                // echo '. Dagnummer: '.date('N', strtotime($date->format('Y-m-d')));

                if (date('N', strtotime($date->format('Y-m-d'))) != 6 and date('N', strtotime($date->format('Y-m-d'))) != 7) // zaterdag == 6, zondag == 7

                {

                    // echo " -- Deze komt de eerste if door, kijk naar dag nummer";

                    if (!Werkdag::where('datum', $date->format('Y-m-d'))->exists()) {

                        // echo " -- Deze wordt aangemaakt! Valt onder tweede if.";

                        $tempwd = new Werkdag;

                        $tempwd->datum = $date->format('Y-m-d');

                        // echo "-- Deze datum wordt toegevoegd: ".$date->format('Y-m-d')." <br>";

                        $tempwd->save();
                    }



                    $i++; //datum is succes dus doortellen

                } else {

                    // echo " -- datum is niet door de eerste IF gekomen.. <br>";

                }

                $date->modify('+1 day');
            }



            if (Werkdag::where('datum', $ap->datum)->count() == 0 || Worklist::where('datum', $ap->datum)->count() == 0) //maakt werkdagen en werklijsten aan als ze nog niet bestaan.

            {

                if (Werkdag::where('datum', $ap->datum)->count() == 0 && date('N', strtotime($date->format('Y-m-d'))) < 6) {

                    $tempwd = new Werkdag;

                    $tempwd->datum = $ap->datum;

                    $tempwd->save();

                    // echo "<br> Datum toegevoegd: ".$ap->datum." Klopt dit?";

                }



                if (Worklist::where('datum', $ap->datum)->count() == 0 && date('N', strtotime($date->format('Y-m-d'))) < 6) {

                    $werkdag = Werkdag::where('datum', $ap->datum)->first();

                    for ($i = 0; $i < $werkdag->aantal_lijsten; $i++) {

                        $temp = new Worklist;

                        $temp->datum = $werkdag->datum;

                        $temp->werkdag_id = $werkdag->id;

                        $temp->save();

                        // echo "<br> Datum toegevoegd aan werklijst: ".$ap->datum." Klopt dit?";

                    }
                }
            }
        }





        $werkdagen = Werkdag::where('behandeld', '<', 2)->where('datum', '>', date('Y-m-d'))->orderBy('datum', 'ASC')->get();

        // dd($werkdagen);

        foreach ($werkdagen as $wd) {

            foreach ($aantalperdag as $ap) {

                if ($wd->datum == $ap->datum) {

                    $wd->aantal = $ap->aantal;
                }

                if ($ap = $aantalperdag->last() && !isset($wd->aantal)) {

                    $wd->aantal = 0;
                }
            }
        }



        // dd("Klaar");



        $page = "planning";

        $sub = "telling";



        return view('planning.getTelling', compact('werkdagen', 'aantalperdag', 'page', 'sub'));
    }



    public function conceptWorklist()

    {



        $werkdagen = Werkdag::with('werklijst')->where('behandeld', '<', 2)->OrderBy('datum', 'ASC')->get();



        // dd($werkdagen);

        if (count($werkdagen) > 0) {

            foreach ($werkdagen as $wd) {

                $omzet = 0;

                foreach ($wd->werklijst as $wl) {

                    $omzet = $omzet +  $wl->omzet + $wl->nbmzet;
                }

                $wd->omzet = $omzet;



                $date = date('Y-m-d', strtotime($wd->datum));

                $afspraken = Appointment::with('klant')->where('startdatum', '>=', $date)->where('startdatum', '<', date('Y-m-d', strtotime("+1 day", strtotime($date))))->get();

                $niet_verwerkt = 0;



                foreach ($afspraken as $a) {

                    $status = Status_has_appointment::where('afspraak_id', $a->id)->first();

                    // dd($afspraken);

                    // echo $a."<hr />";

                    // echo $status."<hr />";

                    if (empty($status)) {

                        $temp = new Status_has_appointment;

                        $temp->afspraak_id = $a->id;

                        $temp->status_id = 1;

                        $temp->omschrijving = Status::find(1)->omschrijving;

                        $temp->gebruiker_id = Auth::user()->id;

                        $temp->save();



                        $status = Status_has_appointment::where('afspraak_id', $a->id)->first();
                    }



                    if ($status->status_id == 1) // Er stond $status->status_id > 1

                    {

                        $niet_verwerkt++;
                    }
                }

                $wd->niet_verwerkt = $niet_verwerkt;

                $wd->aantal_klanten = count($afspraken);
            }
        }

        // dd("ben benieuwd.");



        $page = "controle";

        $sub = "afwl";



        return view('planning.controle-werklijst', compact('werkdagen', 'page', 'sub'));
    }



    public function worklistCheck($date) // to do

    {

        // $date = date('Y-m-d', strtotime($date));

        $werkdag = Werkdag::where('datum', $date)->first();



        $page = "controle";

        $sub = "afwl";



        return view('planning.controle-eenwerklijst', compact('werkdag', 'page', 'sub'));
    }



    public function showStats($date) // to do

    {
    }



    public function getKoppel() // werkt niet optimaal, wordt nu max gebruikt en dit werkt tijdelijk. Tot dat een loper een veger wordt, of een veger->loper->veger is.

    {

        $werknemers = DB::table('functie_has_werknemer as fhw')
            ->select('w.id as werknemer_id', 'w.voornaam as voornaam', 'w.achternaam as achternaam', DB::raw('max(fhw.functie_id) as functie_id'))
            ->join('werknemer as w', 'w.id', '=', 'fhw.werknemer_id')
            ->groupBy('w.id', 'werknemer_id', 'w.voornaam', 'w.achternaam')
            ->havingRaw('max(fhw.functie_id) = 7 or max(fhw.functie_id) = 8')
            ->orderByDesc('fhw.created_at')
            ->get();


        // HAVING max(fhw.functie_id) = 4 or max(fhw.functie_id) = 7 or max(fhw.functie_id) = 8 // MOET TUSSEN GROUP BY EN ORDER BY



        $werknemers = collect($werknemers);

        $functies = Functie::all();

        foreach ($werknemers as $wn) {

            foreach ($functies as $fn) {

                if ($fn->id == $wn->functie_id)

                    $wn->titel = $fn->titel;
            }
        }



        $aantalperdag = DB::table('afspraak')
            ->select(DB::raw('date(afspraak.startdatum) as datum'), DB::raw('count(afspraak.id) as aantal'), 'sha.status_id')
            ->join('status_has_afspraak as sha', 'sha.afspraak_id', '=', 'afspraak.id')
            ->where('sha.status_id', '!=', 23)
            ->whereDate('afspraak.startdatum', '>', now())
            ->groupBy(DB::raw('date(afspraak.startdatum)'), 'sha.status_id')
            ->get();




        $aantalperdag = collect($aantalperdag);



        $werkdagen = Werkdag::where('behandeld', '<', 2)->where('datum', '>', date('Y-m-d'))->orderBy('datum', 'ASC')->limit(10)->get();

        // dd($werkdagen);

        foreach ($werkdagen as $wd) {

            foreach ($aantalperdag as $ap) {

                if ($wd->datum == $ap->datum) {

                    $wd->aantal = $ap->aantal;
                }

                if ($ap = $aantalperdag->last() && !isset($wd->aantal)) {

                    $wd->aantal = 0;
                }
            }
        }



        $page = "planning";

        $sub = "indelen";



        return view('planning.koppellopers', compact('werknemers', 'werkdagen', 'page', 'sub'));
    }



    public function setKoppel()

    {

        $input = Request::all();



        // dd($input);



        $array = array();


        if(isset($input['werknemer']))
        {
            for ($i = 0; $i < count($input['werknemer']); $i++) {

                // dd($input);

                if ($input['loopplaats'][$i] == "" && $input['wanneer'][$i] != "") {

                    return redirect('/planning/werving/koppel')->with('danger', 'U moet wel een plaats invullen.');
                } elseif ($input['loopplaats'][$i] != "" && $input['wanneer'][$i] == "") {

                    return redirect('/planning/werving/koppel')->with('danger', 'U moet wel een datum invullen.');
                }



                $werknemer = $input['werknemer'][$i];

                $loopplaats = $input['loopplaats'][$i];

                $wanneer = $input['wanneer'][$i];

                $startstraat = $input['startstraat'][$i];



                if ($wanneer < date('d-m-Y') && $wanneer != "") {

                    return redirect('/planning/werving/koppel')->with('danger', 'De uitvoerdatum kan niet in het verleden liggen. Als u van mening bent dat dit wel kan, neem dan contact op met de administrator.');
                }



                // $pos = strpos($haystack, $needle);

                // chunk_split($str, 4)



                $datumString = "";

                if (isset($input['datum']) && $loopplaats != "" && $wanneer != "") {

                    $check = false;

                    for ($l = 0; $l < count($input['datum']); $l++) {



                        $pos = strpos($input['datum'][$l], ':');

                        // dd($pos);

                        $id = substr($input['datum'][$l], 0, $pos);

                        // $datum = substr($input['datum'][$l], $pos+1);

                        if ($id == $werknemer) {

                            $check = true;

                            $datum = substr($input['datum'][$l], $pos + 1);

                            if ($datumString == "") {

                                $datumString = $datumString . $datum;
                            } else {

                                $datumString = $datumString . ',' . $datum;
                            }
                        }
                    }

                    if (!$check) {

                        return redirect('/planning/werving/koppel')->with('danger', 'U heeft niet alle velden correct ingevuld.');
                    }



                    if (!Loopdata::where('werknemer_id', $werknemer)->where('wanneer', date('Y-m-d', strtotime($wanneer)))->exists()) {



                        array_push($array, [$werknemer, $wanneer, $loopplaats, $startstraat, $datumString, Auth::user()->id]);
                    } else {

                        $werknemer = Employee::find($werknemer);

                        return redirect('/planning/werving/koppel')->with('danger', $werknemer->voornaam . ' is al ingedeeld op deze datum, wijzig deze in het juiste venster.');
                    }
                } elseif (!isset($input['datum']) && $loopplaats != "") {

                    return redirect('/planning/werving/koppel')->with('danger', 'U moet wel data selecteren voor de klantenwerver.');
                }
            }
        }




        for ($i = 0; $i < count($array); $i++) {

            $loopdata = new Loopdata;

            $loopdata->werknemer_id = $array[$i][0];

            $loopdata->wanneer = date('Y-m-d', strtotime($array[$i][1]));

            $loopdata->plaats = $array[$i][2];

            $loopdata->startstraat = $array[$i][3];

            $loopdata->loopdata = $array[$i][4];

            $loopdata->user_id = $array[$i][5];

            $loopdata->save();



            $werknemer = Employee::find($array[$i][0]);

            $log = new \App\Models\Log;

            $log->gebruiker_id = $array[$i][4];

            $log->omschrijving = Auth::user()->name . " heeft " . $werknemer->voornaam . " ingedeeld op: " . $array[$i][1] . " in " . $array[$i][2];

            $log->type = "Telling";

            $log->save();
        }



        return redirect('/planning/werving/koppel')->with('succes', 'Klantenwervers zijn ingedeeld.');
    }



    public function todayAcquisition($date)

    {

        $vandaag = Loopdata::where('wanneer', date('Y-m-d', strtotime($date)))->get();

        if (!$vandaag->isEmpty()) {

            foreach ($vandaag as $vd) {

                $vd->user_id = User::find($vd->user_id);

                $vd->werknemer_id = Employee::find($vd->werknemer_id);

                $vd->loopdata = explode(',', $vd->loopdata);
            }
        } else {

            $vandaag = "leeg";
        }



        $aantalperwerknemer = DB::table('klant as k')
            ->select('k.id as klant', 'w.id as werknemer', 'k.geschreven_op as geschreven_op', DB::raw('count(k.id) as aantal'))
            ->join('werknemer as w', 'w.id', '=', 'k.geschreven_door')
            ->where('k.geschreven_op', '=', date('Y-m-d', strtotime($date)))
            ->groupBy('k.id', 'w.id', 'k.geschreven_op')
            ->get();


        foreach ($aantalperwerknemer as $apw) {

            $apw->klant = Client::find($apw->klant);

            $apw->werknemer = Client::find($apw->werknemer);
        }



        $page = "planning";

        $sub = "wervvand";



        return view('planning.vandaagWerving', compact('date', 'vandaag', 'page', 'sub', 'aantalperwerknemer'));
    }



    public function getAcquisition()

    {

        $input = Request::all();



        return redirect('/planning/werving/dag/' . $input['datum']);
    }



    public function showWorklist($date, $id)

    {

        $date = date('Y-m-d', strtotime($date));

        $user = User::find($id);



        $werklijst = Worklist::where('datum', $date)->where(function ($query) use ($id) {

            $query->where('verkoper', $id);

            $query->orWhere('veger', $id);
        })->get();



        if ($werklijst->isEmpty())

            return redirect('/planning/verkoop')->with('danger', 'Er staan geen klanten op deze dag, dag kan niet gegenereerd worden.');



        dd($werklijst);

        $whas = Worklist_has_appointments::where('werklijst_id', $werklijst->id)->get();



        if ($whas->isEmpty() && $afspraken->isEmpty()) {

            $check = false;

            return redirect('/planning/verkoop')->with('danger', 'Er staan geen klanten op deze dag, dag kan niet gegenereerd worden.');
        }

        if (!$whas->isEmpty()) {

            foreach ($whas as $wha) {

                $wha->afspraak = $wha->afspraak_id;

                $wha->afspraak_id = Appointment::with('klant')->where('id', $wha->afspraak_id)->first();

                // dd($wha);

                if (!$wha->afspraak_id == null) {

                    $wha->afspraak_id->bijgewerkt_door = User::find($wha->afspraak_id->bijgewerkt_door);

                    $wha->afspraak_id->geschreven_door = Employee::find($wha->afspraak_id->geschreven_door);

                    $wha->afspraak_id->klant = $wha->afspraak_id->klant_id;

                    $wha->afspraak_id->klant_id = Client::find($wha->afspraak_id->klant_id);

                    $wha->afspraak_id->verwerkt_door = User::find($wha->afspraak_id->verwerkt_door);

                    $wha->afspraak_id->afspraaktype_id = AppointmentType::find($wha->afspraak_id->afspraaktype_id);

                    $wha->afspraak_id->geschiedenis = Afspraak_has_geschiedenis::where('afspraak_id', $wha->afspraak_id->id)->get();
                }

                $wha->werkzaamheden = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function ($afs) use ($date, $wha) {

                    $afs->where('id', $wha->afspraak_id->id);
                })->get();
            }
        }



        $redenen = DB::select(DB::raw("select af.id as 'nieuwe_afspraak',date(af.startdatum) 'huidige_datum', af.reden as 'lege_reden', af.afspraak_id as 'afgeleid_van', a.id as 'oude_afspraak', a.reden as 'reden', date(a.startdatum) as 'datum_oude_afspraak'

                                         FROM afspraak as a

                                         inner join afspraak as af on a.id = af.afspraak_id

                                         order by af.id DESC"));

        $temp = 0;

        $tempnieuw = 0;

        foreach ($redenen as $reden) {

            if ($temp != $reden->nieuwe_afspraak) {

                $temp = $reden->oude_afspraak;

                $tempnieuw = $reden->nieuwe_afspraak;
            } else {

                $reden->lege_reden = null;

                $reden->huidige_datum = null;

                $reden->nieuwe_afspraak = $tempnieuw;
            }
        }



        $werklijsten = Worklist::where('datum', $date)->get();

        $werkdag = Werkdag::where('datum', date('Y-m-d', strtotime($date)))->first();

        if ($werkdag == null) {

            return redirect('/planning/verkoop')->with('danger', 'Er staan nog geen klanten op ' . date('d-m-Y', strtotime($date)) . ' en kan derhalve niet gegenereerd worden.');
        }

        if ($werklijsten->count() < $werkdag->aantal_lijsten) {

            $amount = $werkdag->aantal_lijsten - $werklijsten->count();



            for ($i = 0; $i < $amount; $i++) {

                $templist = new Worklist;

                $templist->datum = $werkdag->datum;

                $templist->werkdag_id = $werkdag->id;

                $templist->opmerking = "Bel afwijkende klanten van te voren of ze wel thuis zijn, dit scheelt kilometers en tijd !";



                $templist->save();
            }
        }



        $werklijsten = Worklist::where('datum', $date)->get();



        foreach ($werklijsten as $wl) {

            if ($wl->verkoper != null) {

                $wl->verkoper = Employee::find($wl->verkoper);
            }

            if ($wl->veger != null) {

                $wl->veger = Employee::find($wl->veger);
            }

            if ($wl->auto_id != null) {

                $wl->auto_id = Auto::find($wl->auto_id);
            }
        }



        $page = "planning";

        $sub = "verkvand";

        return view('planning.vandaagVerkooptwee', compact('date', 'user', 'werklijst', 'whas', 'redenen', 'werklijsten', 'werkdag', 'page', 'sub'));
    }



    public function updateHasAfspraken(Request $request)

    {



        $input = Request::all();



        $order = $input['order'];

        //$order = $request->input('order');

        $parent = $input['parent'];



        if ($parent == "verzetten") {

            foreach ($order as $item) {

                $afspraak = Worklist_has_appointments::where("afspraak_id", '=', $item)->first();



                if ($afspraak != null) {

                    $afspraak->delete();
                }
            }



            $order1 = implode(", ", $order);

            echo $order1;

            $sql = "DELETE FROM werklijst_has_afspraak WHERE afspraak_id IN(" . $order1 . ")";



            \DB::raw($sql);

            return \Response::make('message', 200);
        }



        $i = 0;

        foreach ($order as $item) {

            $afspraak = Worklist_has_appointments::firstOrNew(array('afspraak_id' => $item));

            $afspraak->werklijst_id = $parent;

            $afspraak->afspraak_id = $item;

            $afspraak->volgorde = $i++;

            $afspraak->save();
        }

        return \Response::make('', 200);
    }



    public function checkWorkday($date, $id)

    {

        $datum = $date;

        $date = date('Y-m-d', strtotime($date));

        $afspraken = Appointment::with('klant')->where('startdatum', '>=', $date)->where('startdatum', '<', date('Y-m-d', strtotime("+1 day", strtotime($date))))->get();



        $ahas = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function ($afs) use ($date) {

            $afs->where('startdatum', '>=', $date)->where('startdatum', '<', date('Y-m-d', strtotime("+1 day", strtotime($date))));
        })->get();



        $verzettend = DB::select(DB::raw("select a.*, a.id As afspraakID, wha.*, sha.status_id

                        from afspraak as a

                        left join werklijst_has_afspraak as wha

                        on wha.afspraak_id = a.id

                        join status_has_afspraak as sha

                        on sha.afspraak_id = a.id

                        where date(a.startdatum) = '" . $date . "' AND wha.afspraak_id IS NULL AND sha.status_id != 23"));



        $verzetten = (object) $verzettend;



        foreach ($verzetten as $verzet) {

            $verzet->klant_id = Client::find($verzet->klant_id);

            $verzet->verwerkt_door = User::find($verzet->verwerkt_door);

            // $verzet->afspraak_id = Afspraak::find($verzet->afspraak_id);

            if ($verzet->bijgewerkt_door != 0) {
            }

            $verzet->afspraaktype_id = AppointmentType::find($verzet->afspraaktype_id);

            $verzet->geschiedenis = Afspraak_has_geschiedenis::with('geschiedenis')->where('afspraak_id', $verzet->id)->get();
        }



        $werklijsten = Worklist::where('datum', $date)->get();



        foreach ($werklijsten as $wl) {

            if ($wl->verkoper != null) {

                $wl->verkoper = Employee::find($wl->verkoper);
            }

            if ($wl->veger != null) {

                $wl->veger = Employee::find($wl->veger);
            }

            if ($wl->auto_id != null) {

                $wl->auto_id = Auto::find($wl->auto_id);
            }
        }







        $whas = Worklist_has_appointments::with('werklijst')->whereHas('werklijst', function ($wl) use ($date) {

            $wl->where('datum', date('Y-m-d', strtotime($date)));
        })->orderBy('volgorde', 'ASC')->get();





        if ($whas->isEmpty() && $afspraken->isEmpty()) {

            $check = false;

            return redirect('/planning/verkoop')->with('danger', 'Er staan geen klanten op deze dag, dag kan niet gegenereerd worden.');
        }

        if (!$whas->isEmpty()) {

            // dd($whas);

            foreach ($whas as $wha) {

                // $wha->afspraak = $wha->afspraak_id;

                $wha->afspraak_id = Appointment::find($wha->afspraak_id);

                // dd($wha);

                if (!$wha->afspraak_id == null) {

                    $wha->afspraak_id->bijgewerkt_door = User::find($wha->afspraak_id->bijgewerkt_door);

                    $wha->afspraak_id->geschreven_door = Employee::find($wha->afspraak_id->geschreven_door);

                    $wha->afspraak_id->klant = $wha->afspraak_id->klant_id;

                    $wha->afspraak_id->klant_id = Client::find($wha->afspraak_id->klant_id);

                    $wha->afspraak_id->verwerkt_door = User::find($wha->afspraak_id->verwerkt_door);

                    $wha->afspraak_id->afspraaktype_id = AppointmentType::find($wha->afspraak_id->afspraaktype_id);

                    $wha->afspraak_id->geschiedenis = Afspraak_has_geschiedenis::where('afspraak_id', $wha->afspraak_id->id)->get();
                }
            }
        }



        foreach ($verzetten as $verzet) {

            foreach ($whas as $wha) {



                if ($verzet->id == $wha->afspraak_id->id) {

                    unset($verzet);
                }
            }
        }



        $wd = Werkdag::find($id);



        // dd($kleurencombi);

        $page = "planning";

        $sub = "verkvand";

        return view('planning.controlewerkdag', compact('verzetten', 'ahas', 'redenen', 'werklijsten', 'whas', 'datum', 'planning', 'page', 'sub', 'date', 'wd'));
    }



    public function acquisitionUB()

    {

        $loopdata = Loopdata::where('wanneer', date('Y-m-d'))->get();



        $page = 'planning';

        $sub = 'betaling';

        return view('planning.wervingub', compact('page', 'sub', 'loopdata'));
    }



    public function finishWorkday($date, $id)

    {

        $werkdag = Werkdag::find($id);

        $werkdag->behandeld = 2;

        $werkdag->save();



        return redirect('/planning/verkoop')->with('succes', 'U heeft de controle afgerond.');
    }
}
