<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\History;
use App\Models\Status;
use App\Models\Log;
use App\Models\Status_has_appointment;
use App\Models\Status_has_factuur;
use App\Models\Appointment_has_activities;
use App\Models\Activiteit_has_offerte;
use App\Models\Work;
use Auth;
use App\Models\Factuur;
use App\Models\Klant_has_status;
use DB;
use App\Models\Crediteur;
use App\Models\Offerte;
use App\Models\Offerte_has_status;
use Datetime;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;
use App\Models\Uur;
use App\Models\Aanvulling;
use App\Models\Verzonden_aan;
use App\Models\Reactie;

class ReactieController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

   public function setReaction($id)
   {
        $input = Request::all();
        // dd(Request::get('submit'));
        $reactie = new Reactie();
        $reactie->omschrijving = $input['omschrijving'];
        $reactie->verzonden_aan_id = $id;
        $reactie->gebruiker_id = Auth::user()->id;
        $reactie->save();


        if(Request::get('submit') == 'stay'){
            return redirect('/aanvulling/'.$id)->with('succes', 'Reactie succesvol geplaatst.');
        }
        return redirect('/aanvulling')->with('succes', 'Reactie succesvol geplaatst.');
   }

}
