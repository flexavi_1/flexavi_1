<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Work;
use App\Models\AppointmentType;
use App\Models\Appointment;
use App\Models\History;
use App\Models\Appointment_has_activities;
use DateTime;
use Auth;
use App\Models\Log;
use App\Models\Status;
use App\Models\Status_has_appointment;
use App\Models\Crediteur;


use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;

class CrediteurController extends Controller
{



        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function warrantyStore($id)
    {
        $input = Request::all();

        $relatie = Client::find($id);
        $afspraak = Appointment::find($input['afspraak_id']);

        $crediteur = new Crediteur;
        $crediteur->klant_id = $relatie->id;
        $crediteur->betaaldatum = date('Y-m-d', strtotime("+14 days"));
        $crediteur->users_id = Auth::user()->id;;
        $crediteur->bedrag = $input['totaalbedrag'];
        if($input['omschrijving']=="")
            $crediteur->omschrijving = "Naar aanleiding van de garantie afspraak van ".date('d-m-Y',strtotime($afspraak->startdatum)).", is dit als crediteur toegevoegd.";

        $crediteur->save();

        return redirect('/relatie/'.$relatie->id)->with('succes', 'Terugbetaalverzoek succesvol toegevoegd');
    }

    public function setPayment($id, $bedrag)
    {
        $crediteur = Crediteur::where('klant_id', $id)->where('bedrag', $bedrag)->first();
        $crediteur->terugbetaald = 1;
        $crediteur->save();

        return redirect('/relatie/'.$id)->with('succes', 'Terugbetaalverzoek succesvol betaald.');
    }

    public function index()
    {
        $relaties = Crediteur::with('klant')->get();
        $users = User::all();
        // dd($relaties);

        $page = "financieel";
        $sub = "crediteur";

        return view('crediteur.index', compact('relaties', 'users', 'page', 'sub'));
    }

    public function indexNiet()
    {
        $relaties = Crediteur::with('klant')->where('terugbetaald', 0)->get();
        $users = User::all();
        // dd($relaties);

        $page = "financieel";
        $sub = "crediteur";

        // dd($relaties);

        return view('crediteur.index', compact('relaties', 'users', 'page', 'sub'));
    }

    public function indexBetaald()
    {
        $relaties = Crediteur::with('klant')->where('terugbetaald', 1)->get();
        $users = User::all();
        // dd($relaties);

        $page = "financieel";
        $sub = "crediteur";


        return view('crediteur.index', compact('relaties', 'users', 'page', 'sub'));
    }

    public function create($id)
    {
        $relatie = Client::find($id);
        // dd($relaties);

        $page = "financieel";
        $sub = "crediteur";


        return view('crediteur.create', compact('relatie', 'page', 'sub'));
    }

    public function store($id)
    {

        $input = Request::all();

        $relatie = Client::find($id);
        // $afspraak = Afspraak::find($input['afspraak_id']);

        $crediteur = new Crediteur;
        $crediteur->klant_id = $relatie->id;
        $crediteur->betaaldatum = date('Y-m-d', strtotime("+14 days"));
        $crediteur->users_id = Auth::user()->id;;
        $crediteur->bedrag = $input['totaalbedrag'];
        $crediteur->omschrijving = $input['omschrijving'];
        $crediteur->save();

        return redirect('/relatie/'.$relatie->id)->with('succes', 'Terugbetaalverzoek succesvol toegevoegd');
    }

    public function destroy($id)
    {
        $crediteur = Crediteur::find($id);
        $relatie_id = $crediteur->klant_id;

        $crediteur->delete();

        return redirect('/relatie/'.$relatie_id)->with('succes', 'U heeft het Terugbetaalverzoek succesvol verwijderd.');
    }
}
