<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\History;
use App\Models\Status;
use App\Models\Log;
use App\Models\Status_has_appointment;
use App\Models\Status_has_factuur;
use App\Models\Appointment_has_activities;
use App\Models\Activiteit_has_offerte;
use App\Models\Work;
use Auth;
use App\Models\Factuur;
use App\Models\Klant_has_status;
use DB;
use App\Models\Crediteur;
use App\Models\Offerte;
use App\Models\Offerte_has_status;
use Datetime;

use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;
use App\Models\Uur;
use App\Models\Aanvulling;
use App\Models\Verzonden_aan;
use App\Models\Reactie;

class Verzonden_aanController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setRead($id)
    {
        $va = Verzonden_aan::find($id);
        $va->gelezen_op = date('Y-m-d H:i');
        $va->status_id++;
        $va->save();

        //log?

        return redirect('/aanvulling')->with('succes', 'U heeft de aanvulling succesvol gelezen.');

    }

    public function setSolved($id)
    {
        $va = Verzonden_aan::find($id);
        $va->status_id = 18;
        $va->save();

        //log?

        return redirect('/aanvulling')->with('succes', 'U heeft de aanvulling succesvol gemarkeerd als opgelost.');

    }

    public function setRight($id)
    {
        $va = Verzonden_aan::find($id);
        $va->status_id = 20;
        $va->save();

        //log?

        return redirect('/aanvulling')->with('succes', 'U heeft de aanvulling succesvol goedgekeurd.');

    }

    public function setWrong($id)
    {
        $va = Verzonden_aan::find($id);
        $va->status_id = 19;
        $va->save();

        //log?
        $page = '';
        $sub = '';

        $verzondenaan = Verzonden_aan::find($id);
        return view('aanvulling.reactie', compact('verzondenaan', 'page', 'sub'));

    }

    public function show($id)
    {
        $verzondenaan = Verzonden_aan::find($id);
        $aanvulling = Aanvulling::find($verzondenaan->aanvulling_id);
        $reacties = Reactie::where('verzonden_aan_id', $verzondenaan->id)->orderBy('id', 'ASC')->get();
        if($aanvulling->klant_id == null){
            $klant = "";
        } else {
            $klant = Client::find($aanvulling->klant_id);
        }
        $verzender = User::find($aanvulling->gebruiker_id);
        $ontvanger = User::find($verzondenaan->ontvanger);
        $status = Status::find($verzondenaan->status_id);

        $sub = "aanvover";
        $page = "aanvulling";

        return view('aanvulling.toon', compact('verzondenaan', 'klant','aanvulling', 'reacties', 'verzender', 'ontvanger', 'status', 'page', 'sub'));
    }

    public function delete($id)
    {
        $verzondenaan = Verzonden_aan::find($id);
        $reacties = Reactie::where('verzonden_aan_id', $verzondenaan->id)->get();
        foreach($reacties as $reactie)
        {
            $reactie->delete();
        }
        $verzondenaan->delete();

        return redirect('/aanvulling')->with('succes', 'U heeft de aanvulling bij deze gebruiker verwijderd.');
    }

}
