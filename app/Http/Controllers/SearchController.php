<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Client;
use App\Models\Search;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('search');
    }

    public function search(Request $request){
        $text = $request->input('search');
        $zipcode = $request->input('zipcode');
        $housenumber = $request->input('housenumber');
        if(empty($text) && (empty($zipcode))){
            return redirect('/zoekresultaten')->with('danger', 'Geen zoektekst of postcode & huisnummer ingevuld.');
        }

        if(!$request->has('topics')){
            return redirect('/zoekresultaten')->with('danger', 'Geen onderwerp geselecteerd.');
        }
        $topics = $request->input('topics');
        if(count($topics) == 0){
            return redirect('/zoekresultaten')->with('danger', 'Geen onderwerp geselecteerd.');
        }

        if(!empty($zipcode)){
            if(strlen($zipcode) > 7 || strlen($zipcode) < 6){
                return redirect('/zoekresultaten')->with('danger', 'Ongeldige postcode.');
            }
        }

        $results = [];
        foreach($topics as $topic){
            if($topic == 1){
                //klant data
                $klanten = Client::getFromSearch($text, $zipcode, $housenumber);
                if(count($klanten) == 1){
                    dd('Klant profiel openen van '.$klanten[0]->voornaam.' '.$klanten[0]->achternaam);
                    return redirect('/relatie/'.$klanten[0]->id) ;
                }
                $results[] = [
                    'data' => $klanten,
                    'subject' => 'Klant'
                ];
            } elseif ($topic == 2) {
                //afspraak data
                $results[] = [
                    'data' => Appointment::getFromSearch($text, $zipcode, $housenumber),
                    'subject' => 'Afspraak'
                ];
            } elseif ($topic == 3) {
                //factuur data
            }
        }

        $search = new Search;
        $search->users_id = Auth::user()->id;
        $search->search = $text;
        $search->zipcode = $zipcode;
        $search->housenumber = $housenumber;
        $search->is_zipcode = (!empty($zipcode) && !empty($housenumber) && in_array(1, $topics))? 1 : 0;
        $search->topics = '<small>(onderwerpen: '.implode(',', $topics).")</small>";
        $search->topics = str_replace('1', 'Klant ', $search->topics);
        $search->topics = str_replace('2', 'Afspraak ', $search->topics);
        $search->save();

        return view('search', compact('results'));
    }

}
