<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Employee;
use Datatables;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\History;
use App\Models\Status;
use App\Models\Log;
use App\Models\Status_has_appointment;
use App\Models\Status_has_factuur;
use App\Models\Appointment_has_activities;
use App\Models\Activiteit_has_offerte;
use App\Models\Functie_has_Werknemer;
use App\Models\Work;
use Auth;
use App\Models\Factuur;
use App\Models\Klant_has_status;
use App\Models\Auto;
use DB;
use DatePeriod;
use DateInterval;
use App\Models\Crediteur;
use App\Models\Offerte;
use App\Models\Offerte_has_status;
use Datetime;
use App\Models\Werkdag;
use Yajra\Datatables\Html\Builder; //
use App\DataTables\KlantDatatable;
use App\Models\Uur;
use App\Models\Aanvulling;
use App\Models\Verzonden_aan;
use App\Models\Reactie;
use App\Models\Worklist;
use App\Models\Worklist_has_appointments;
use App\Models\Functie;
use App\Models\Kantoorkosten;
use App\Models\Overigekosten;
use App\Models\Finance;
use App\Models\Wervingskosten;
use App\Models\sqlBuilder;

class FinancieelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function maakDagstaat($date, $werkdag_id)
    {
        $werkdag = Werkdag::find($werkdag_id);

        $werkdagen = array();

        $werklijsten = Worklist::where('werkdag_id', $werkdag_id)->get();

        $danger = array();

        foreach ($werklijsten as $werklijst) {
            $werklijst->verkoper = Employee::find($werklijst->verkoper);
            $werklijst->veger = Employee::find($werklijst->veger);
            $werklijst->auto_id = Auto::find($werklijst->auto_id);
            $whas = Worklist_has_appointments::where('werklijst_id', $werklijst->id)->get();
            $check = true;

            foreach ($whas as $wha) {
                $afspraak = Appointment::find($wha->afspraak_id);
                if ($afspraak->getStatus()->id == 1) {
                    $check = false;
                    array_push($danger, ['werklijst' => $werklijst->id, 'bericht' => "U kunt de UB lijst nog niet invullen/controleren, nog niet alle afspraken zijn verwerkt."]);
                }
            }

            if ($whas->isEmpty()) {
                $check = false;
                array_push($danger, ['werklijst' => $werklijst->id, 'bericht' => "U kunt de UB lijst nog niet controleren/invullen, er staan geen afspraken in."]);
            }

            if ($check) {
                $omzet = 0.00;
                $nb = 0.00;
                $dgVerkoper = 0.00;
                $dgVeger = 0.00;
                $provisieVerkoper = 0.00;
                $provisieVeger = 0.00;
                $gh = 0.00;

                $idsNietgelukt = array();

                if (!$whas->isEmpty()) {
                    foreach ($whas as $wha) {
                        $wha->afspraak_id = Appointment::find($wha->afspraak_id);

                        if (Status_has_appointment::where('afspraak_id', $wha->afspraak_id)->where('status_id', '2')->count() > 0) {
                            array_push($idsNietgelukt, $wha->afspraak_id);
                        } else {
                            if ($wha->afspraak_id->afspraaktype_id == 3) {
                                $gh += $wha->afspraak_id->totaalbedrag;
                            } else {
                                if (Status_has_appointment::where('afspraak_id', $wha->afspraak_id->id)->first()->status_id == 3 || Status_has_appointment::where('afspraak_id', $wha->afspraak_id->id)->first()->status_id == 4)
                                    $omzet += $wha->afspraak_id->totaalbedrag;
                            }
                            $nb += $wha->afspraak_id->niet_betaald;
                        }
                    }
                }
                $aantalklanten = $whas->count();

                // dd($werklijst);
                $check = true;
                if (!$werklijst->verkoper == null || !$werklijst->veger == null) {
                    if ($werklijst->verkoper == null) {
                        $vernaam = "Verkoper";
                        $tempfunctieverk = null;
                        $dgVerkoper = 0.00;
                        $provisieVerkoper = 0.00;
                    } else {
                        $vernaam = $werklijst->verkoper->voornaam;
                        $tempwhfverk = Functie_has_Werknemer::where('werknemer_id', $werklijst->verkoper->id)->first();
                        $tempfunctieverk = Functie::find($tempwhfverk->functie_id);
                        if ($tempfunctieverk != null) {
                            $dgVerkoper = $tempfunctieverk->dagloon;
                        } else {
                            // Handle the case where $tempfunctieverk is null
                            $dgVerkoper = 0.00; // or set it to some default value
                        }

                        // $dgVerkoper = $tempfunctieverk->dagloon;
                        $provisieVerkoper = 0.00;
                    }

                    if ($werklijst->veger == null) {
                        $vegnaam = "";
                        $check = false;
                        $tempfunctieveg = null;
                        $dgVeger = 0.00;
                        $provisieVeger = 0.00;
                    } else {
                        $vegnaam = " en " . $werklijst->verkoper->voornaam;
                        $check = true;
                        $tempwhfveg = Functie_has_Werknemer::where('werknemer_id', $werklijst->veger->id)->first();
                        $tempfunctieveg = Functie::find($tempwhfveg->functie_id);
                        if ($tempfunctieveg != null) {
                            $dgVeger = $tempfunctieveg->dagloon;
                        } else {
                            // Handle the case where $tempfunctieverk is null
                            $dgVeger = 0.00; // or set it to some default value
                        }
                        // $dgVeger = $tempfunctieveg->dagloon;
                        $provisieVeger = 0.00;
                    }

                    if ($omzet > 0) {
                        if ($tempfunctieverk->provisieperc != null && $tempfunctieverk != null) {
                            if ($omzet >= $tempfunctieverk->provisiegrens) {
                                $provisieVerkoper = ($omzet - $tempfunctieverk->provisiegrens) / 100 * $tempfunctieverk->provisieperc;
                            } else {
                                $provisieVerkoper = 0.00;
                            }
                        } else {
                            $provisieVerkoper = $tempfunctieverk->provisiebedrag * $aantalklanten;
                        }

                        if ($tempfunctieveg != null) {
                            if ($tempfunctieveg->provisieperc != null) {
                                if ($omzet >= $tempfunctieverk->provisiegrens) {
                                    $provisieVeger = ($omzet - $tempfunctieveg->provisiegrens) / 100 * $tempfunctieveg->provisieperc;
                                } else {
                                    $provisieVeger = 0.00;
                                }
                            } else
                                $provisieVeger = $tempfunctieveg->provisiebedrag * $aantalklanten;
                        }
                    }
                }
                // dd($dgVerkoper);

                $temparray = array();
                array_push($temparray, $omzet); //0
                array_push($temparray, $nb); // 1
                array_push($temparray, $aantalklanten); //2
                array_push($temparray, $dgVerkoper); //3
                array_push($temparray, $dgVeger); //4
                array_push($temparray, $provisieVerkoper); //5
                array_push($temparray, $provisieVeger); //6
                array_push($temparray, "Werklijst " . $werklijst->id . " van " . $vernaam . $vegnaam); //7
                array_push($temparray, $gh); //8
                array_push($temparray, $werklijst); //9
                array_push($temparray, $check); //10
                array_push($temparray, count($idsNietgelukt)); //11
                // array_push($temparray, $verkoper); //12
                // array_push($temparray, $veger);
                // Array = [0 => omzet], [1 => nb], [2 => aantalklanten] etc....

                array_push($werkdagen, $temparray);
                // Array = [0 => {[0 => omzet], [1 => nb], [2 => aantalklanten] etc..}]
            }
        }
        // dd($werkdagen);

        //dd(date('Y-m-d', strtotime($werkdag->datum)));
        $klanten = DB::select("
         select w.id as 'werknemer', count(k.id) as 'aantal'
         from klant as k
         join werknemer as w on w.id = k.geschreven_door
         join functie_has_werknemer as fhw on fhw.werknemer_id = w.id
         where k.geschreven_op = '" . date('Y-m-d', strtotime($werkdag->datum)) . "'
         AND fhw.functie_id > 6
         group by w.id
     ");
        $klanten = (object) $klanten;

        foreach ($klanten as $k) {
            $k->werknemer = Employee::find($k->werknemer);
            $k->functie = Functie_has_Werknemer::where('functie_id', 7)->where('werknemer_id', $k->werknemer->id)->first();
        }

        // dd($klanten);
        $werknemers = Functie_has_Werknemer::with('werknemer', 'functie')->whereHas('werknemer', function ($wn) {
            $wn->where('actief', 1);
        })->where('functie_id', '<', 7)->get();

        $page = "planning";
        $sub = "planverk";
        return view('financieel.vulDagstaat', compact('date', 'klanten', 'werkdag_id', 'werkdagen', 'danger', 'page', 'sub', 'werknemers'));
    }

    public function updateDagstaat($date, $werkdag_id)
    {
        $input = Request::all();
        // dd($input);

        $werkdag = Werkdag::find($werkdag_id);


        for ($i = 0; $i < $werkdag->aantal_lijsten; $i++) // invullen van de transacties voor elke werklijst
        {
            if (isset($input['werkdag_id'][$i])) {
                $werklijst = Worklist::find($input['werkdag_id'][$i]);
                if ($werklijst->verkoper != null) {
                    $werklijst->nbloonVerk = $input['nbloonVerk'][$i];
                    $werklijst->totaalloonVerk = $input['totaalloonVerk'][$i];
                    if ($werklijst->veger != null) {
                        $werklijst->nbloonVeg = $input['nbloonVeg'][$i];
                        $werklijst->totaalloonVeg = $input['totaalloonVeg'][$i];
                    } else {
                        $werklijst->nbloonVeg = 0;
                        $werklijst->totaalloonVeg = 0;
                    }
                    $werklijst->benzine = $input['benzine'][$i];
                    $werklijst->eten = $input['eten'][$i];
                    $werklijst->inkoop = $input['inkoop'][$i];
                    $werklijst->telefoonkosten = $input['telefoonkosten'][$i];
                    $werklijst->extra = $input['extra'][$i];
                    $werklijst->omzet = $input['omzet'][$i];
                    $werklijst->nbomzet = $input['nbomzet'][$i];
                    $werklijst->gh = $input['gh'][$i];
                    $werklijst->aantal_klanten = $input['aantalklanten'][$i];
                    $werklijst->klantennietgedaan = $input['nietgelukt'][$i];
                    $werklijst->save();
                }
            }
        }

        //werving verwerken
        if (isset($input['werver'])) {
            for ($i = 0; $i < count($input['werver']); $i++) {
                $tmp = new Wervingskosten;
                $tmp->werknemer_id = $input['werver'][$i];      // saving employee id
                $tmp->nbloon = $input['werverNB'][$i];
                $tmp->ubloon = $input['werverUB'][$i];
                $tmp->totaalloon = $tmp->ubloon + $tmp->nbloon;
                $tmp->verwerkt_door = Auth::user()->id;
                $tmp->werkdag_id = $werkdag->id;
                $tmp->datum = date('Y-m-d', strtotime($date));

                $functie = Functie_has_Werknemer::where('functie_id', $input['functie'][$i])->where('werknemer_id', $tmp->werknemer_id)->first();
                $expected_pay = $functie->dagloon + $input['werveraantal'][$i] * $functie->provisiebedrag;

                $tmp->verschil = $expected_pay - $tmp->totaalloon;
                $tmp->benzine = $input['werverBenzineUB'][$i];
                $tmp->benzinenb = $input['werverBenzineNB'][$i];
                $tmp->save();
            }
        }
        //kantoor verwerken
        for ($i = 0; $i < count($input['werknemer']); $i++) {
            if ($input['aantal_uur'][$i] != 0) {
                $tmp = new Kantoorkosten;
                $tmp->werkdag_id = $werkdag_id;
                $tmp->werknemer_id = $input['werknemer'][$i];
                $tmp->aantal_uur = $input['aantal_uur'][$i];
                $tmp->uurtarief = $input['uurtarief'][$i];
                $tmp->betaald = $input['betaald'][$i];
                $tmp->datum = $werkdag->datum;
                $tmp->verschil = $input['betaald'][$i] - ($input['aantal_uur'][$i] * $input['uurtarief'][$i]);
                $tmp->nb = 0.00; // Set nb to null if there is no valid value
                $tmp->save();
            }
        }

        //overige kosten
        for ($i = 0; $i < count($input['omschrijving']); $i++) {
            if ($input['kosten'][$i] != 0) {
                $tmp = new Overigekosten;
                $tmp->werkdag_id = $werkdag_id;
                $tmp->omschrijving = $input['omschrijving'][$i];
                $tmp->kosten = $input['kosten'][$i];
                $tmp->aangemaakt_door = Auth::user()->id;
                $tmp->datum = $werkdag->datum;
                $tmp->save();
            }
        }

        $werkdag->ublCheck = 1;
        // dd($werkdag->ublCheck);
        $werkdag->save();

        return redirect('/planning/verkoop/')->with("succes", 'UB lijst gecontroleerd/aangevuld');
    }

    public function getDagstaat()
    {

        $page = "financieel";
        $sub = "rapportage";
        return view('financieel.getDagstaat', compact('page', 'sub'));
    }

    public function checkDagstaat($date, $werkdag_id)
    {
        $werkdag = Werkdag::find($werkdag_id);
        $werklijsten = Worklist::where('werkdag_id', $werkdag->id)->get();
        foreach ($werklijsten as $w) {
            if ($w->veger != null)
                $w->veger = Employee::find($w->veger);
            if ($w->verkoper != null)
                $w->verkoper = Employee::find($w->verkoper);
        }

        $kantoor = Kantoorkosten::where('werkdag_id', $werkdag->id)->get();
        $overig = Overigekosten::where('werkdag_id', $werkdag->id)->get();

        foreach ($kantoor as $k) {
            $k->werknemer_id = Employee::find($k->werknemer_id);
        }
        // dd($kantoor);
        // $klanten = Klant::with('geschrevenDoor')->where('geschreven_op', $werkdag->datum)->get();
        //dd(date('Y-m-d', strtotime($werkdag->datum)));
        $klanten = DB::select(DB::raw("select w.id as 'werknemer', count(k.id) as 'aantal'
                                         from klant as k
                                         join werknemer as w
                                         on w.id = k.geschreven_door
                                         where k.geschreven_op = '" . date('Y-m-d', strtotime($werkdag->datum)) . "'
                                         group by w.id"));
        $klanten = (object) $klanten;
        foreach ($klanten as $k) {
            $tmp = Functie_has_Werknemer::where('werknemer_id', $k->werknemer)->first();
            $k->functie = $tmp;
            $k->werknemer = Employee::find($k->werknemer);
        }
        $wervers = Wervingskosten::where('datum', date('Y-m-d', strtotime($date)))->get();

        $page = "financieel";
        $sub = "ublijst";

        return view('financieel.ublijstVandaag', compact('werklijsten', 'kantoor', 'overig', 'klanten', 'werkdag', 'page', 'sub', 'wervers'));
    }

    public function setDagstaat($date, $werkdag_id)
    {
        $input = Request::all();
        //ophalen van alle werklisjt gegevens
        for ($i = 0; $i < count($input['werklijst_id']); $i++) {
            $werklijst = Worklist::find($input['werklijst_id'][$i]);
            $werklijst->ubloonVerk = $input['ubloonVerk'][$i];
            $werklijst->nbloonVerk = $input['nbloonVerk'][$i];
            $werklijst->totaalloonVerk = $input['totaalloonVerk'][$i];
            if (isset($input['totaalloonVeg'])) {
                if (isset($input['totaalloonVeg'][$i])) {
                    $werklijst->ubloonVeg = $input['ubloonVeg'][$i];
                    $werklijst->nbloonVeg = $input['nbloonVeg'][$i];
                    $werklijst->totaalloonVeg = $input['totaalloonVeg'][$i];
                }
            }
            $werklijst->gh = $input['gh'][$i];
            $werklijst->benzine = $input['benzine'][$i];
            $werklijst->eten = $input['eten'][$i];
            $werklijst->telefoonkosten = $input['telefoonkosten'][$i];
            $werklijst->inkoop = $input['inkoop'][$i];
            $werklijst->extra = $input['extra'][$i];
        }

        //Ophalen kantoorkosten
        if (isset($input['werknemer_id'])) {
            for ($i = 0; $i < count($input['werknemer_id']); $i++) {
                $werknemer = Employee::find($input['werknemer_id'][$i]);
                if ($input['totaalloonKan'] != 0) {
                    if (Kantoorkosten::where('werknemer_id', $werknemer->id)->where('datum', date('Y-m-d', strtotime($date)))->exists()) {
                        $k = Kantoorkosten::where('werknemer_id', $werknemer->id)->where('datum', date('Y-m-d', strtotime($date)))->first();    // ophalen van kantoorkosten voor deze werknemer
                        $k->werkdag_id = $werkdag_id;                                                                                           //
                        $k->werknemer_id = $werknemer->id;
                        if ($input['ubloonKan'][$i] > ($k->aantal_uur * $k->uurtarief)) {
                            $k->verschil = $input['ubloonKan'][$id] - ($k->aantal_uur * $k->uurtarief);
                        }
                        $k->betaald = $input['ubloonKan'][$i];
                        $k->save();
                    }
                }
            }
        }

        $werkdag = Werkdag::find($werkdag_id);
        $klanten = DB::select(DB::raw("select w.id as 'werknemer', count(k.id) as 'aantal'
                                         from klant as k
                                         join werknemer as w
                                         on w.id = k.geschreven_door
                                         where k.geschreven_op = '" . date('Y-m-d', strtotime($werkdag->datum)) . "'
                                         group by w.id"));

        //ophalen wervingskosten
        if (isset($input['werver'])) {
            for ($i = 0; $i < count($input['werver']); $i++) {
                // dd(count($input['werver']));
                $werver = Employee::find($input['werver'][$i]);
                if (!Wervingskosten::where('werkdag_id', $werkdag_id)->where('werknemer_id', $werver->id)->exists()) {
                    $wk = new Wervingskosten;
                } else {
                    $wk = Wervingskosten::where('werkdag_id', $werkdag_id)->where('werknemer_id', $werver->id)->first();
                }

                $wk->werknemer_id = $werver->id;
                if (!empty($klanten)) {
                    for ($l = 0; $l < count($klanten); $l++) {
                        // dd(count($klanten));
                        // dd($klanten[$l]->werknemer);
                        if ($klanten[$l]->werknemer == $werver->id) {
                            $totaalloon = $klanten[$l]->aantal * Functie_has_Werknemer::where('werknemer_id', $werver->id)->first()->provisiebedrag + Functie_has_Werknemer::where('werknemer_id', $werver->id)->first()->dagloon;
                            $verschil = $input['totaalloonWerver'][$i] - $totaalloon;
                            // dd($totaalloon);
                        }
                    }
                } else {
                    $totaalloon = $input['totaalloonWerver'];
                    $verschil = 0;
                }


                $wk->totaalloon = $input['totaalloonWerver'][$i];
                $wk->verschil = $verschil;
                $wk->ubloon = $input['ubWerver'][$i];
                $wk->nbloon = $input['nbWerver'][$i];
                $wk->verwerkt_door = Auth::user()->id;
                $wk->werkdag_id = $werkdag_id;
                $wk->datum = date('Y-m-d', strtotime($date));
                $wk->save();
            }
        }

        //ophalen overige kosten
        if (isset($input['overig'])) {
            for ($i = 0; $i < count($input['overig']); $i++) {
                $o = Overigekosten::find($input['overig'][$i]);
                $o->kosten = $input['totaalloonOve'][$i];
                $o->save();
            }
        }

        $werkdag->ublCheck++;
        $werkdag->save();

        return redirect('/planning/verkoop')->with('succes', 'Uitbetaallijst is verwerkt.');
    }

    public function showDagstaat()
    {
        $input = Request::all(); // get request
        $input = (object) $input; // collectify
        // dd($input);
        // $startdatum = date('Y-m-d', strtotime($input['startdatum']));
        // if(isset($input['einddatum']))
        //     $einddatum = date('Y-m-d', strtotime($input['einddatum']));
        // echo "Start: ".strtotime($input->startdatum).'<br>';
        // echo "Eind: ".strtotime($input->einddatum).'<br>';
        // echo "if startdatum is groter dan einddatum= ".(strtotime($input->startdatum) < strtotime($input->einddatum))."<br>";
        // echo "if startdatum is kleiner dan einddatum= ".(strtotime($input->startdatum) > strtotime($input->einddatum))."<br>";
        // dd($input);

        if (!isset($input->transaction) && !isset($input->aggregate) && !isset($input->employee))
            return redirect('/ublijst')->with('danger', 'Selecteer minstens één dimensie.');

        if ((strtotime($input->startdatum) == strtotime($input->einddatum) || strtotime($input->startdatum) > strtotime($input->einddatum)) && $input->einddatum != "" && !isset($input->startToEnd)) {
            return redirect('/ublijst')->with('danger', 'Vul een geldige datum in.');
        }

        $startdatum = date('Y-m-d', strtotime($input->startdatum));

        $finance = new Finance;
        $page = "";
        $sub = "";
        $werknemers = Employee::where('actief', 1)->get();
        $arr = array();
        $arr = (object) $arr;

        if ($input->einddatum == "" || isset($input->startToEnd)) {
            if (isset($input->omzet)) // Get revenue for this date
            {
                $getRev =  $finance->getRevenue($startdatum);
                $omzet = $getRev[0];
                $nb = $getRev[1];
                $arr->omzet = $omzet;
                $arr->nb = $nb;
            }

            if (isset($input->kosten)) // Get costs for this date
            {
                $getCosts = $finance->getCosts($startdatum);
                if (isset($getCosts->error))
                    return redirect('/financiele-rapportage')->with('danger', 'Er gaat iets mis, neem contact op met de administrator. Error code: WDNULL01');
                $kantoor = $getCosts->kantoor;
                $creditering = $getCosts->creditering;
                $inkoop = $getCosts->inkoop;
                $werving = $getCosts->werving;
                $overig = $getCosts->overig;
                $kosten = $getCosts->totaal;
                $verkoop = $getCosts->verkoop;
                $arr->kosten = $getCosts;
            }

            if (isset($input->winst)) // get profit for this date
            {
                $winst = $finance->getProfit($startdatum);
                $arr->winst = $winst;
            }

            if (isset($input->verkoop)) // get sales costs for this date
            {
                if (isset($input->aggregate) && isset($input->transaction)) // if aggregate and transaction are true, show table with aggregated(start to end), show table with revenue split in customer.appointments
                {
                    $arr->salesAgg = $finance->getSales($startdatum, true);
                    $arr->salesTrans = $finance->getSales($startdatum, false);
                } elseif (isset($input->aggregate) && !isset($input->transaction)) // if only aggregate is true, aggregate data only start to end.
                {
                    $arr->salesAgg = $finance->getSales($startdatum, true);
                } elseif (!isset($input->aggregate) && isset($input->transaction)) // if only transaction is true, aggregated on transaction level (split revenue to rev.per.customer)
                {
                    $arr->salesTrans = $finance->getSales($startdatum, false);
                } else // both are false only on aggregated on employee level
                {
                    $arr->salesTrans = $finance->getSales($startdatum, false);
                }
            }

            if (isset($input->werving)) // get acquisition costs for this date
            {
                if (isset($input->aggregate) && isset($input->transaction)) // if aggregate and transaction are true, show table with aggregated(start to end), show table with revenue split in customer.appointments
                {
                    $arr->acquisitionAgg = $finance->getAcq($startdatum, true);
                    $arr->acquisitionTrans = $finance->getAcq($startdatum, false);
                } elseif (isset($input->aggregate) && !isset($input->transaction)) // if only aggregate is true, aggregate data only start to end.
                {
                    $arr->acquisitionAgg = $finance->getAcq($startdatum, true);
                } elseif (!isset($input->aggregate) && isset($input->transaction)) // if only transaction is true, aggregated on transaction level
                {
                    $arr->acquisitionTrans = $finance->getAcq($startdatum, false);
                } else // both are false only on aggregated on employee level
                {
                    $arr->acquisitionTrans = $finance->getAcq($startdatum, false);
                }
            }

            if (isset($input->kantoor)) // get administration costs for this date
            {
                if (isset($input->aggregate) && isset($input->transaction)) // if aggregate and transaction are true, show table with aggregated(start to end), show table with revenue split in customer.appointments
                {
                    $arr->adminAgg = $finance->getAdmin($startdatum, true);
                    $arr->adminTrans = $finance->getAdmin($startdatum, false);
                } elseif (isset($input->aggregate) && !isset($input->transaction)) // if only aggregate is true, aggregate data only start to end.
                {
                    $arr->adminAgg = $finance->getAdmin($startdatum, true);
                } elseif (!isset($input->aggregate) && isset($input->transaction)) // if only transaction is true, aggregated on transaction level
                {
                    $arr->adminTrans = $finance->getAdmin($startdatum, false);
                } else // both are false only on aggregated on employee level
                {
                    $arr->adminTrans = $finance->getAdmin($startdatum, false);
                }
            }


            if (isset($input->inkoop)) // get purchase costs for this date
            {
                if (isset($input->aggregate) && isset($input->transaction)) // if aggregate and transaction are true, show table with aggregated(start to end), show table with revenue split in customer.appointments
                {
                    $arr->purchaseAgg = $finance->getPurchase($startdatum, true);
                    $arr->purchaseTrans = $finance->getPurchase($startdatum, false);
                } elseif (isset($input->aggregate) && !isset($input->transaction)) // if only aggregate is true, aggregate data only start to end.
                {
                    $arr->purchaseAgg = $finance->getPurchase($startdatum, true);
                } elseif (!isset($input->aggregate) && isset($input->transaction)) // if only transaction is true, aggregated on transaction level
                {
                    $arr->purchaseTrans = $finance->getPurchase($startdatum, false);
                } else // both are false only on aggregated on employee level
                {
                    $arr->purchaseTrans = $finance->getPurchase($startdatum, false);
                }
            }


            if (isset($input->overig)) // get remaining costs for this date
            {
                if (isset($input->aggregate) && isset($input->transaction)) // if aggregate and transaction are true, show table with aggregated(start to end), show table with revenue split in customer.appointments
                {
                    $arr->remainderAgg = $finance->getRemainder($startdatum, true);
                    $arr->remainderTrans = $finance->getRemainder($startdatum, false);
                } elseif (isset($input->aggregate) && !isset($input->transaction)) // if only aggregate is true, aggregate data only start to end.
                {
                    $arr->remainderAgg = $finance->getRemainder($startdatum, true);
                } elseif (!isset($input->aggregate) && isset($input->transaction)) // if only transaction is true, aggregated on transaction level
                {
                    $arr->remainderTrans = $finance->getRemainder($startdatum, false);
                } else // both are false only on aggregated on employee level
                {
                    $arr->remainderTrans = $finance->getRemainder($startdatum, false);
                }
            }

            $arr->agg = (isset($input->aggregate)) ? true : false;
            $arr->trans = (isset($input->transaction)) ? true : false;
            $arr->starttoend = (isset($input->startToEnd)) ? true : false;
            $arr->employee = (isset($input->employee)) ? true : false;
            $datumstring = date('d-m-Y', strtotime($startdatum));
            return view('financieel.uitbetaallijst', compact('arr', 'page', 'sub', 'werknemers', 'datumstring'));
        } else {
            return redirect('/ublijst')->with('danger', 'Dit is in ontwikkeling');
        }

        $einddatum = date('Y-m-d', strtotime($input->einddatum));

        if (isset($input->omzet)) // Get revenue for this date till enddate
        {
            $getRev =  $finance->getRevenueTwo($startdatum, $einddatum);
            $omzet = $getRev[0];
            $nb = $getRev[1];
            $arr->omzet = $omzet;
            $arr->nb = $nb;
        }
        if (isset($input->kosten)) {
            $getCosts = $finance->getCostsTwo($startdatum, $einddatum);
            if (isset($getCosts->error))
                return redirect('/financiele-rapportage')->with('danger', 'Er gaat iets mis, neem contact op met de administrator. Error code: WDNULL01');
            $kantoor = $getCosts->kantoor;
            $creditering = $getCosts->creditering;
            $inkoop = $getCosts->inkoop;
            $werving = $getCosts->werving;
            $overig = $getCosts->overig;
            $kosten = $getCosts->totaal;
            $verkoop = $getCosts->verkoop;
            $arr->kosten = $getCosts;
        }
        if (isset($input->winst)) {
            $winst = $finance->getProfitTwo($startdatum, $einddatum);
            $arr->winst = $winst;
        }
        if (isset($input->verkoop)) // get sales costs for this date till enddate
        {
            if (isset($input->aggregate) && isset($input->transaction)) // if aggregate and transaction are true, show table with aggregated(start to end), show table with revenue split in customer.appointments
            {
                $arr->salesAgg = $finance->getSalesTwo($startdatum, $einddatum, true);
                $arr->salesTrans = $finance->getSalesTwo($startdatum, $einddatum, false);
            } elseif (isset($input->aggregate) && !isset($input->transaction)) // if only aggregate is true, aggregate data only start to end.
            {
                $arr->salesAgg = $finance->getSalesTwo($startdatum, $einddatum, true);
            } elseif (!isset($input->aggregate) && isset($input->transaction)) // if only transaction is true, aggregated on transaction level (split revenue to rev.per.customer)
            {
                $arr->salesTrans = $finance->getSalesTwo($startdatum, $einddatum, false);
            } else // both are false only on aggregated on employee level
            {
                $arr->salesTrans = $finance->getSalesTwo($startdatum, $einddatum, false);
            }
        }

        if (isset($input->werving)) // get acquisition costs for this date till enddate
        {
            if (isset($input->aggregate) && isset($input->transaction)) // if aggregate and transaction are true, show table with aggregated(start to end), show table with revenue split in customer.appointments
            {
                $arr->acquisitionAgg = $finance->getAcqTwo($startdatum, $einddatum, true);
                $arr->acquisitionTrans = $finance->getAcqTwo($startdatum, $einddatum, false);
            } elseif (isset($input->aggregate) && !isset($input->transaction)) // if only aggregate is true, aggregate data only start to end.
            {
                $arr->acquisitionAgg = $finance->getAcqTwo($startdatum, $einddatum, true);
            } elseif (!isset($input->aggregate) && isset($input->transaction)) // if only transaction is true, aggregated on transaction level
            {
                $arr->acquisitionTrans = $finance->getAcqTwo($startdatum, $einddatum, false);
            } else // both are false only on aggregated on employee level
            {
                $arr->acquisitionTrans = $finance->getAcqTwo($startdatum, $einddatum, false);
            }
        }

        if (isset($input->kantoor)) // get administration costs for this date till enddate
        {
            if (isset($input->aggregate) && isset($input->transaction)) // if aggregate and transaction are true, show table with aggregated(start to end), show table with revenue split in customer.appointments
            {
                $arr->adminAgg = $finance->getAdminTwo($startdatum, $einddatum, true);
                $arr->adminTrans = $finance->getAdminTwo($startdatum, $einddatum, false);
            } elseif (isset($input->aggregate) && !isset($input->transaction)) // if only aggregate is true, aggregate data only start to end.
            {
                $arr->adminAgg = $finance->getAdminTwo($startdatum, $einddatum, true);
            } elseif (!isset($input->aggregate) && isset($input->transaction)) // if only transaction is true, aggregated on transaction level
            {
                $arr->adminTrans = $finance->getAdminTwo($startdatum, $einddatum, false);
            } else // both are false only on aggregated on employee level
            {
                $arr->adminTrans = $finance->getAdminTwo($startdatum, $einddatum, false);
            }
        }

        if (isset($input->inkoop)) // get purchase costs for this date till enddate
        {
            if (isset($input->aggregate) && isset($input->transaction)) // if aggregate and transaction are true, show table with aggregated(start to end), show table with revenue split in customer.appointments
            {
                $arr->purchaseAgg = $finance->getPurchaseTwo($startdatum, $einddatum, true);
                $arr->purchaseTrans = $finance->getPurchaseTwo($startdatum, $einddatum, false);
            } elseif (isset($input->aggregate) && !isset($input->transaction)) // if only aggregate is true, aggregate data only start to end.
            {
                $arr->purchaseAgg = $finance->getPurchaseTwo($startdatum, $einddatum, true);
            } elseif (!isset($input->aggregate) && isset($input->transaction)) // if only transaction is true, aggregated on transaction level
            {
                $arr->purchaseTrans = $finance->getPurchaseTwo($startdatum, $einddatum, false);
            } else // both are false only on aggregated on employee level
            {
                $arr->purchaseTrans = $finance->getPurchaseTwo($startdatum, $einddatum, false);
            }
        }

        if (isset($input->overig)) // get remaining costs for this date till enddate
        {
            if (isset($input->aggregate) && isset($input->transaction)) // if aggregate and transaction are true, show table with aggregated(start to end), show table with revenue split in customer.appointments
            {
                $arr->remainderAgg = $finance->getRemainderTwo($startdatum, $einddatum, true);
                $arr->remainderTrans = $finance->getRemainderTwo($startdatum, $einddatum, false);
            } elseif (isset($input->aggregate) && !isset($input->transaction)) // if only aggregate is true, aggregate data only start to end.
            {
                $arr->remainderAgg = $finance->getRemainderTwo($startdatum, $einddatum, true);
            } elseif (!isset($input->aggregate) && isset($input->transaction)) // if only transaction is true, aggregated on transaction level
            {
                $arr->remainderTrans = $finance->getRemainderTwo($startdatum, $einddatum, false);
            } else // both are false only on aggregated on employee level
            {
                $arr->remainderTrans = $finance->getRemainderTwo($startdatum, $einddatum, false);
            }
        }

        $arr->agg = (isset($input->aggregate)) ? true : false;
        $arr->trans = (isset($input->transaction)) ? true : false;
        $arr->starttoend = (isset($input->startToEnd)) ? true : false;
        $arr->employee = (isset($input->employee)) ? true : false;


        $datumstring = "van " . date('d-m-Y', strtotime($input->startdatum)) . " tot en met " . date('d-m-Y', strtotime($input->einddatum));

        $begin = new DateTime($startdatum);
        $end = new DateTime($einddatum);

        $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
        $date_array = array();
        foreach ($daterange as $date) {
            foreach ($finance->getLists($startdatum, $einddatum) as $gl) {
                if ($date->format('Y-m-d') == date('Y-m-d', strtotime($gl->datum))) {
                    array_push($date_array, $date->format('Y-m-d'));
                    // echo $date->format('Y-m-d')." added in worklists <br>";
                    break;
                } elseif (Wervingskosten::where('datum', $date->format('Y-m-d'))->exists()) {
                    array_push($date_array, $date->format('Y-m-d'));
                    // echo $date->format('Y-m-d')." added in acquisition <br>";
                    break;
                } elseif (Kantoorkosten::where('datum', $date->format('Y-m-d'))->exists()) {
                    array_push($date_array, $date->format('Y-m-d'));
                    // echo $date->format('Y-m-d')." added in administration <br>";
                    break;
                } elseif (Overigekosten::where('datum', $date->format('Y-m-d'))->exists()) {
                    array_push($date_array, $date->format('Y-m-d'));
                    // echo $date->format('Y-m-d')." added in remainder <br>";
                    break;
                }
            }
        }

        // dd($date_array);
        $date_object = (object) $date_array;

        return view('financieel.uitbetaallijst', compact('date_object', 'arr', 'page', 'sub', 'werknemers', 'datumstring'));
    }

    public function getRapportage()
    {
        $page = 'financieel';
        $sub = 'rapportage';
        return view('financieel.opvragenRapportage', compact('page', 'sub'));
    }

    public function postRapportage()
    {
        $input = (object) Request::all();
        $input->enddate = ($input->enddate == "") ? null : date('Y-m-d', strtotime($input->enddate));
        $input->startdate = ($input->startdate == "") ? null : date('Y-m-d', strtotime($input->startdate));
        if (!isset($input->selectFour)) {
            $input->selectFour = null;
            $input->selectFive = null;
        }
        if (!isset($input->selectFive)) {
            $input->selectFour = null;
            $input->selectFive = null;
        }
        // dd($input);
        $sql = $this->sqlBuilder($input);
        dd($sql);
        if ($sql == false) {
            //errooooor
        }
    }

    public function sqlBuilder($input)
    {
        $string = '#select
                    from fact_sales
                    #joins
                    #where
                    #groupby ';
        $select = '';
        $where = '';
        $groupby = '';

        $selectOne = $this->dbNameConverter($input->selectOne);
        $selectTwo = $this->dbNameConverter($input->selectTwo);
        $selectThree = $this->dbNameConverter($input->selectThree);
        $selectFour = $this->dbNameConverter($input->selectFour);
        $selectFive = $this->dbNameConverter($input->selectFive);
        $selectSix = (isset($input->selectSix)) ? $this->dbNameConverter($input->selectSix) : $this->dbNameConverter('nvt');
        if ($selectFour == null || $selectFive == null) {
            $selectFour = null;
            $selectFive = null;
        }
        $select = sqlBuilder::returnSelect($selectOne, $selectTwo, $selectThree, $selectFour, $selectFive, $selectSix);
        if ($select == false) {
            return false;
        }

        $from = ' from fact_sales as fs ';

        $joins = sqlBuilder::returnJoins($selectOne, $selectTwo, $selectThree, $selectFour, $selectFive, $selectSix);

        $where = sqlBuilder::returnWhere($selectOne, $selectTwo, $selectThree, $selectFour, $selectFive, $selectSix, $input->startdate, $input->enddate);

        $groupby = sqlBuilder::returnGroupby($selectOne, $selectTwo, $selectThree, $selectFour, $selectFive, $selectSix);

        $orderby = sqlBuilder::returnOrderby($selectOne, $selectTwo, $selectThree, $selectFour, $selectFive, $selectSix);


        $sql = $select . $from . $joins . $where . $groupby . $orderby;

        return $sql;
    }

    public function dbNameConverter($string)
    {
        switch ($string) {
            case 'verkoper':
                return ['f' => 'fs.dim_employee_id',               'name' =>  'de.name',       'as' => 'as Verkoper', 'id' => 'de.id',  'join' => 'dim_employee as de'];
                break;
            case 'klant':
                return ['f' => 'fs.klant_id',                      'name' => 'distinct(fs.klant_id)',    'as' => 'as Klant'];
                break;
            case 'activiteit':
                return ['f' => 'fs.dim_activity_id',               'name' =>  'da.name',       'as' => 'as Activiteit', 'id' => 'da.id',  'join' => 'dim_activity as da'];
                break;
            case 'tijdj':
                return ['f' => 'fs.dim_time_id',                   'name' =>  'dt.year',       'as' => 'as Tijd', 'id' => 'dt.id',  'join' => 'dim_time as dt'];
                break;
            case 'tijdm':
                return ['f' => 'fs.dim_time_id',                   'name' =>  'dt.month',       'as' => 'as Tijd', 'id' => 'dt.id',  'join' => 'dim_time as dt'];
                break;
            case 'tijdw':
                return ['f' => 'fs.dim_time_id',                   'name' =>  'dt.week',       'as' => 'as Tijd', 'id' => 'dt.id',  'join' => 'dim_time as dt'];
                break;
            case 'tijdd':
                return ['f' => 'fs.dim_time_id',                   'name' =>  'dt.date',       'as' => 'as Tijd', 'id' => 'dt.id',  'join' => 'dim_time as dt'];
                break;
            case 'plaats':
                return ['f' => 'fs.dim_place_id',                  'name' =>  'dp.city',       'as' => 'as Plaats', 'id' => 'dp.id',  'join' => 'dim_place as dp'];
                break;
            case 'type':
                return ['f' => 'fs.dim_type_id',                   'name' =>  'dty.type',      'as' => 'as Type', 'id' => 'dty.id', 'join' => 'dim_type as dty'];
                break;
            case 'geschreven_door':
                return ['f' => 'fs.dim_employee_written_by',    'name' =>  'dew.name',      'as' => 'as Geschreven_door', 'id' => 'dew.id',    'join' => 'dim_employee as dew'];
                break;
            case 'tarief':
                return ['f' => 'fs.tarief',                        'name' =>  'fs.tarief',     'as' => 'as Tarief'];
                break;
            case 'aantal':
                return ['f' => 'fs.aantal',                        'name' =>  'fs.aantal',     'as' => 'as Aantal'];
                break;
            case 'omzet':
                return ['f' => '(fs.aantal * fs.tarief)',            'name' =>  '(fs.aantal * fs.tarief)',     'as' => 'as Omzet'];
                break;

            default:
                if ($string == 'nvt') {
                    $string = null;
                }
                return $string;
                break;
        }
    }
}
