<?php

namespace App\Observers;

use App\Models\Client;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;

class KlantObserver
{
    /**
     * Handle the Klant "created" event.
     */
    public function created(Client $klant): void
    {
        //
        Log::createLog(
            Auth::user()->id,
            $klant->id,
            'Relatie',
            Auth::user()->name . " heeft een relatie toegevoegd, Postcode+huisnummer: " . $klant->postcode .' '. $klant->huisnummer
        );

    }

    /**
     * Handle the Klant "updated" event.
     */
    public function updated(Client $klant): void
    {
        //
        Log::createLog(
            Auth::user()->id,
            $klant->id,
            'Relatie',
            Auth::user()->name . " heeft een relatie gewijzigd, Postcode+huisnummer: " . $klant->postcode .' '. $klant->huisnummer
        );

    }

    /**
     * Handle the Klant "deleted" event.
     */
    public function deleted(Client $klant): void
    {
        //
    }

    /**
     * Handle the Klant "restored" event.
     */
    public function restored(Client $klant): void
    {
        //
    }

    /**
     * Handle the Klant "force deleted" event.
     */
    public function forceDeleted(Client $klant): void
    {
        //
    }
}
