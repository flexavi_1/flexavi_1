<?php

namespace App\Observers;

use App\Models\History;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;

class GeschiedenisObserver
{
    /**
     * Handle the Geschiedenis "created" event.
     */
    public function created(History $geschiedenis): void
    {
         Log::createLog(
            Auth::user()->id,
            $geschiedenis->klant_id,
            'Geschiedenis',
            Auth::user()->name." heeft geschiedenis aangevuld bij: ".
            $geschiedenis->klant->achternaam.", Postcode+huisnummer: ".
            $geschiedenis->klant->postcode.$geschiedenis->klant->huisnummer
        );
    }

    /**
     * Handle the Geschiedenis "updated" event.
     */
    public function updated(History $geschiedenis): void
    {
        //
    }

    /**
     * Handle the Geschiedenis "deleted" event.
     */
    public function deleted(History $geschiedenis): void
    {
        //
    }

    /**
     * Handle the Geschiedenis "restored" event.
     */
    public function restored(History $geschiedenis): void
    {
        //
    }

    /**
     * Handle the Geschiedenis "force deleted" event.
     */
    public function forceDeleted(History $geschiedenis): void
    {
        //
    }
}
