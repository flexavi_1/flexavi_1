<?php

namespace App\Observers;

use App\Models\Search;

class SearchObserver
{
    /**
     * Handle the Search "created" event.
     */
    public function created(Search $search): void
    {
        //
        if(Search::where('users_id', $search->users_id)->count() > 5){
            while(Search::where('users_id', $search->users_id)->count() > 5){
                $oldSearch = Search::where('users_id', $search->users_id)->orderBy('created_at', 'asc')->first();
                $oldSearch->delete();
            }
        }
    }

    /**
     * Handle the Search "updated" event.
     */
    public function updated(Search $search): void
    {
        //
    }

    /**
     * Handle the Search "deleted" event.
     */
    public function deleted(Search $search): void
    {
        //
    }

    /**
     * Handle the Search "restored" event.
     */
    public function restored(Search $search): void
    {
        //
    }

    /**
     * Handle the Search "force deleted" event.
     */
    public function forceDeleted(Search $search): void
    {
        //
    }
}
