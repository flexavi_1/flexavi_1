<?php

namespace App\Observers;

use App\Models\Log;
use App\Models\Status_has_appointment;
use Illuminate\Support\Facades\Auth;

class StatusAfspraakObserver
{
    /**
     * Handle the Status_has_afspraak "created" event.
     */
    public function created(Status_has_appointment $status_has_afspraak): void
    {
        //
        if(count($status_has_afspraak->afspraak->sha) > 1){
            //we know that there is more than 1 status for this appointment, so it's a change
            Log::createLog(
                Auth::user()->id,
                $status_has_afspraak->afspraak->klant->id,
                'Afspraak',
                Auth::user()->name . " heeft een afspraak gewijzigd, Postcode+huisnummer: " .
                    $status_has_afspraak->afspraak->klant->postcode .' '.
                    $status_has_afspraak->afspraak->klant->huisnummer
            );
        }

    }

    /**
     * Handle the Status_has_afspraak "updated" event.
     */
    public function updated(Status_has_appointment $status_has_afspraak): void
    {
        //
    }

    /**
     * Handle the Status_has_afspraak "deleted" event.
     */
    public function deleted(Status_has_appointment $status_has_afspraak): void
    {
        //
    }

    /**
     * Handle the Status_has_afspraak "restored" event.
     */
    public function restored(Status_has_appointment $status_has_afspraak): void
    {
        //
    }

    /**
     * Handle the Status_has_afspraak "force deleted" event.
     */
    public function forceDeleted(Status_has_appointment $status_has_afspraak): void
    {
        //
    }
}
