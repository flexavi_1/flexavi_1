<?php

namespace App\Observers;

use App\Models\Appointment;
use App\Models\History;
use App\Models\Log;
use App\Models\Status;
use App\Models\Status_has_appointment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log as Logger;
class AfspraakObserver
{
    /**
     * Handle the Afspraak "created" event.
     */
    public function created(Appointment $afspraak): void
    {
        //create Log
        Logger::info('Creating log');
        Log::createLog(
            Auth::user()->id,
            $afspraak->klant_id,
            'Afspraak',
            Auth::user()->name." heeft een afspraak gemaakt op: ".
            date('d-m-Y', strtotime($afspraak->startdatum))."\n Voor klant: ".$afspraak->klant->achternaam.' - '.
            $afspraak->klant->postcode.$afspraak->klant->huisnummer
        );

        //create History
        $history = new History;
        $history->klant_id = $afspraak->klant_id;
        $history->afspraak_id = $afspraak->id;
        $history->gebruiker_id = -1;
        $history->omschrijving = Auth::user()->name." heeft een afspraak gemaakt met klant op ".
                                    $afspraak->returnDate($afspraak->startdatum)." --- ".$afspraak->returnDate($afspraak->einddatum).
                                    " Deze afspraak is geschreven door: ".Auth::user()->name;
        $history->datum = date('Y-m-d H:i:s');
        $history->type = "Systeem";
        $history->saveQuietly();

        //create SHA
        Status_has_appointment::setSha(
            $afspraak->id,
            Status::getIntFromText('APT_MADE'),
            "De verwerking van deze afspraak is hersteld door: ".Auth::user()->name,
            date('Y-m-d', strtotime($afspraak->startdatum))
        );
    }

    /**
     * Handle the Afspraak "updated" event.
     */
    public function updated(Appointment $afspraak): void
    {
        //
        if($afspraak->isDirty('is_verzet')){
            //setHistory($klant_id, $afspraak_id, $gebruiker_id, $omschrijving, $type){
            History::setHistory(
                $afspraak->klant_id,
                $afspraak->id,
                -1,
                Auth::user()->name." heeft de afspraak verzet",
                'Systeem'
            );
        }
    }

    /**
     * Handle the Afspraak "deleted" event.
     */
    public function deleted(Appointment $afspraak): void
    {
        //
    }

    /**
     * Handle the Afspraak "restored" event.
     */
    public function restored(Appointment $afspraak): void
    {
        //
    }

    /**
     * Handle the Afspraak "force deleted" event.
     */
    public function forceDeleted(Appointment $afspraak): void
    {
        //
    }
}
