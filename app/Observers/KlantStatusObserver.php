<?php

namespace App\Observers;

use App\Models\History;
use App\Models\Klant_has_status;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;

class KlantStatusObserver
{
    /**
     * Handle the Klant_has_status "created" event.
     */
    public function created(Klant_has_status $klant_has_status): void
    {
        //
    }

    /**
     * Handle the Klant_has_status "updated" event.
     */
    public function updated(Klant_has_status $klant_has_status): void
    {
        //

        $regel = new History;
        $regel->gebruiker_id = -1;
        $regel->klant_id = $klant_has_status->klant->id;
        $regel->datum = date('Y-m-d');
        $regel->omschrijving = Auth::user()->name . " heeft de reminder behandeld.";
        $regel->type = "Systeem";
        $regel->save();

        $log = new Log;
        $log->gebruiker_id = Auth::user()->id;
        $log->klant_id = $klant_has_status->klant->id;
        $log->omschrijving = Auth::user()->name . " heeft de reminder behandeld.";
        $log->type = "Reminder";
        $log->save();
    }

    /**
     * Handle the Klant_has_status "deleted" event.
     */
    public function deleted(Klant_has_status $klant_has_status): void
    {
        //
    }

    /**
     * Handle the Klant_has_status "restored" event.
     */
    public function restored(Klant_has_status $klant_has_status): void
    {
        //
    }

    /**
     * Handle the Klant_has_status "force deleted" event.
     */
    public function forceDeleted(Klant_has_status $klant_has_status): void
    {
        //
    }
}
