<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Materiaal extends Model
{
    //
    protected $fillable = ['naam', 'omschrijving'];
    public $table = "materiaal";
    public $timestamps = false;

    public function ahm()
    {
        return $this->hasMany('App\Models\Activiteit_has_materiaal');
    }

    public function checkExistence()
    {
    	if(Activiteit_has_materiaal::where('materiaal_id', $this->id)->exists())
    		return true;
    	return false;
    }
}
