<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worklist extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "werklijst";

    public function salesman()
    {
        return $this->belongsTo(Employee::class, 'verkoper');
    }

    public function worker()
    {
    	return $this->belongsTo(Employee::class, 'veger');
    }

    public function auto()
    {
        return $this->hasOne('App\Models\Auto');
    }

    public function wha()
    {
        return $this->hasMany('App\Models\Worklist_has_appointments');
    }

    public function returnAppAmount()
    {
        $amount = Worklist_has_appointments::where('werklijst_id', $this->id)->count();
        return Worklist_has_appointments::where('werklijst_id', $this->id)->count();;
    }

    public static function createNew($date, $wd_id){
        $temp = new Worklist;
        $temp->datum = date('Y-m-d', strtotime($date));
        $temp->werkdag_id = $wd_id;
        $temp->opmerking = '';
        $temp->save();
    }

    public static function getCorrectPersonel($id){
        $werklijst = Worklist::find($id);
        if($werklijst){
            $verkoper = Employee::find($werklijst->verkoper);
            $veger = Employee::find($werklijst->veger);
            $teamString = '';
            if($verkoper){
                $teamString .= $verkoper->voornaam;
            } else {
                $teamString .= 'Geen verkoper ingedeeld';
            }
            if($veger){
                $teamString .= ' & '.$veger->voornaam;
            }
            return $teamString;
        } else {
            return "Geen medewerkers ingedeeld";
        }
    }

    public static function findWorklistFromAppointment($afspraak_id, $werkdag_id){
        $werklijsten = Worklist::where('werkdag_id', $werkdag_id)->get();
        foreach ($werklijsten as $w) {
            if(Worklist_has_appointments::where('werklijst_id', $w->id)->where('afspraak_id', $afspraak_id)->exists()){ // this appointment is in this work list.
                return $w;
            }
        }
        return null;
    }

}
