<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Log as LogModel;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'actief',
        'role'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function search(){
        return $this->hasMany('App\Models\Search', 'users_id')->orderBy('created_at', 'desc')->take(5);
    }

    public function verwerkteAfspraken(){
        return $this->hasMany('App\Models\Appointment', 'verwerkt_door');
    }
    public function crediteur()
    {
        return $this->hasMany('App\Models\Crediteur');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'werknemer_id');
    }

    public function wha()
    {
        return $this->hasMany('App\Models\Werknemer_has_auto');
    }

    public function vijftien(){
        return $this->hasMany('App\Models\Vijftien');
    }

    public static function getName($id){
        $user = User::find($id);
        if($user){
            return $user->name;
        }
        return "Onbekende gebruiker";
    }

    /**
     * @description return user's full name
     * @return string
     */
    public function getFullName(){
        if($this->werknemer_id == null || $this->werknemer_id == 0){
            return $this->name;
        }
        return $this->employee->getFullName();
    }

    public function ticket(){
        return $this->hasMany('App\Models\Ticket');
    }

    public function myLogs(){
        return $this->hasMany(LogModel::class, 'gebruiker_id')->orderBy('created_at', 'desc');
    }

    public function myLast10Logs(){
        return $this->hasMany(LogModel::class, 'gebruiker_id')->orderBy('created_at', 'desc')->take(10);
    }
}
