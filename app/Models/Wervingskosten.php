<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wervingskosten extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "wervingskosten";

    public function werkdag_id()
    {
        return $this->belongsTo('App\Models\Werkdag');
    }

    public function werknemer_id()
    {
    	return $this->belongsTo('App\Models\Employee');
    }
}
