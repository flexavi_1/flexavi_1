<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bedrijf extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "bedrijf"; 
    public $timestamps = false;
    
    public static function getPro6pp(){
    	return Bedrijf::find(1)->pro66p;
    }

}
