<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auto extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "auto"; 
    public $timestamps = false;

    public function wha()
    {
        return $this->hasMany('App\Models\Werknemer_has_auto');
    }
}
