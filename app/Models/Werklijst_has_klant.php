<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Werklijst_has_afspraak extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "werklijst_has_afspraak";

    public function werklijst()
    {
        return $this->belongsTo('App\Models\Worklist');
    }

    public function afspraak()
    {
    	return $this->belongsTo('App\Models\Appointment');
    }

}
