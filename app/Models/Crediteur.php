<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Model;



class Crediteur extends Model

{

    //

    protected $guarded = ['id'];

    public $table = "crediteur";



    public function klant()

    {

        return $this->belongsTo('App\Models\Client');

    }



    public function user()

    {

    	return $this->belongsTo('App\Models\User', 'gebruiker_id');

    }

}

