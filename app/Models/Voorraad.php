<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Appointment;

class Voorraad extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "voorraad";

    public function materiaal()
    {
        return $this->belongsTo('App\Models\Materiaal');
    }

    public function status()
    {
    	return $this->belongsTo('App\Models\Status');
    }

    public static function mutatingStock($afspraak_id){
        $afspraak = Appointment::find($afspraak_id);
        $ahas = $afspraak->aha();
        foreach($ahas as $aha){
            foreach($aha->materiaal() as $m){
                if(!Voorraad::where('materiaal_id', $m->materiaal_id)
                            ->where('datum', date('Y-m-d', strtotime($afspraak->startdatum)))
                            ->where('mutatie', $m->aantal * $aha->aantal * -1)
                            ->where('klant_id', $afspraak->klant_id)->exists()){
                    $mutation = new Voorraad;
                    $mutation->materiaal_id = $m->materiaal_id;
                    $mutation->datum = date('Y-m-d', strtotime($afspraak->startdatum));
                    $mutation->mutatie = $m->aantal * $aha->aantal * -1;
                    $mutation->klant_id = $afspraak->klant_id;
                    $mutation->save();
                }
            }
        }
    }

}
