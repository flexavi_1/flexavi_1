<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Client;
use Auth;

class Klant_has_status extends Model
{
    //
    protected $fillable = ['status_id', 'klant_id', 'created_at', 'updated_at', 'verwerk_door'];
    public $table = "klant_has_status";

    public function status()
    {
    	return $this->belongsTo('App\Models\Status');
    }

   	public function klant()
   	{
   		return $this->belongsTo('App\Models\Client');
   	}

    public static function changeCustomerStatus($id){
      //find customer
      $relatie = Client::find($id);
      //if exists in status table
      if(Klant_has_status::where('klant_id', $relatie->id)->exists()){
        //retrieve klant_has_status record
        $khs = Klant_has_status::where('klant_id', $relatie->id)->first();
        //if status is 22 (= reminder treated)
        if($khs->status_id == 22)
        {
            //if difference in current date and last customer updated date.
            if($relatie->calcDiff($khs->updated_at, date('d-m-Y')) >= 350)
            {
                //set status to reminder (it's ready to be stalked again)
                $khs->status_id = 21;
                $khs->verwerkt_door = Auth::user()->id;
                $khs->save();
            }
        }
      } else { //create new one
        $khs = new Klant_has_status;
        // set to treated, because it's a new one.
        $khs->status_id = 21;
        $khs->klant_id = $relatie->id;
        $khs->verwerkt_door = Auth::user()->id;
        $khs->save();
      }
    }
}
