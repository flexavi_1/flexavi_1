<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "status";
    public $timestamps = false;

    public CONST APT_MADE = 1;
    public CONST APT_FAILED = 2;
    public CONST APT_DONE_UNPAID = 3;
    public CONST APT_DONE_PAID = 4;
    public CONST APT_CANCELLED = 23;
    public CONST APT_FAILED_NEW_APPOINTMENT = 29;
    public CONST APT_FAILED_NO_NEW_APPOINTMENT = 30;
    public CONST QUOTE_MADE = 5;
    public CONST QUOTE_FAILED = 6;
    public CONST QUOTE_HONORED = 7;
    public CONST INVOICE_UNPAID = 8;
    public CONST INVOICE_PAID = 9;
    public CONST INVOICE_1ST_REMINDER = 11;
    public CONST INVOICE_2ND_REMINDER = 12;
    public CONST INVOICE_3RD_REMINDER = 13;
    public CONST INVOICE_EXHORT = 14;
    public CONST INVOICE_DEBT_COLLECTOR = 15;
    public CONST ADDITION_RECEIVED = 16;
    public CONST ADDITION_READ = 17;
    public CONST ADDITION_SOLVED = 18;
    public CONST ADDITION_FAILED = 19;
    public CONST ADDITION_APPROVED = 20;
    public CONST REMINDER = 21;
    public CONST REMINDER_PROCESSED = 22;
    public CONST REQUEST_MADE = 24;
    public CONST REQUEST_REJECTED = 25;
    public CONST REQUEST_ACCEPTED = 26;
    public CONST REQUEST_ORDERED = 27;
    public CONST REQUEST_ORDER_RECEIVED = 28;
    public CONST TICKET_MADE = 31;
    public CONST TICKET_IN_PROCESS = 32;
    public CONST TICKET_REPLIED = 33;
    public CONST TICKET_CLOSED = 34;
    public CONST FIFTEEN_MADE = 35;
    public CONST FIFTEEN_1ST_REMINDER = 36;
    public CONST FIFTEEN_2ND_REMINDER = 37;
    public CONST FIFTEEN_3RD_REMINDER = 38;
    public CONST FIFTEEN_EXHORT = 39;
    public CONST FIFTEEN_DEBT_COLLECTOR = 40;
    public CONST FIFTEEN_RESOLVED = 41;


    public function returnDate($date)
    {
    	$format = date('d-M-Y H:i', strtotime($date));

    	return $format;
    }

    public function sha()
    {
    	return $this->hasMany('App\Models\Status_has_appointment');
    }

    public function shf()
    {
        return $this->hasMany('App\Models\Status_has_factuur');
    }

    public function ohs()
    {
        return $this->hasOne('App\Models\Offerte_has_status');
    }

    public function verzonden_aan()
    {
        return $this->hasMany('App\Models\Verzonden_aan');
    }

    public function vijftien(){
        return $this->hasMany('App\Models\Vijftien');
    }

    public function ticket(){
        return $this->hasMany('App\Models\Status');
    }
    public static function getTextToIntegerMapping() {
        return [
            'APT_MADE' => self::APT_MADE,
            'APT_FAILED' => self::APT_FAILED,
            'APT_DONE_UNPAID' => self::APT_DONE_UNPAID,
            'APT_DONE_PAID' => self::APT_DONE_PAID,
            'APT_CANCELLED' => self::APT_CANCELLED,
            'APT_FAILED_NEW_APPOINTMENT' => self::APT_FAILED_NEW_APPOINTMENT,
            'APT_FAILED_NO_NEW_APPOINTMENT' => self::APT_FAILED_NO_NEW_APPOINTMENT,
            'QUOTE_MADE' => self::QUOTE_MADE,
            'QUOTE_FAILED' => self::QUOTE_FAILED,
            'QUOTE_HONORED' => self::QUOTE_HONORED,
            'INVOICE_UNPAID' => self::INVOICE_UNPAID,
            'INVOICE_PAID' => self::INVOICE_PAID,
            'INVOICE_1ST_REMINDER' => self::INVOICE_1ST_REMINDER,
            'INVOICE_2ND_REMINDER' => self::INVOICE_2ND_REMINDER,
            'INVOICE_3RD_REMINDER' => self::INVOICE_3RD_REMINDER,
            'INVOICE_EXHORT' => self::INVOICE_EXHORT,
            'INVOICE_DEBT_COLLECTOR' => self::INVOICE_DEBT_COLLECTOR,
            'ADDITION_RECEIVED' => self::ADDITION_RECEIVED,
            'ADDITION_READ' => self::ADDITION_READ,
            'ADDITION_SOLVED' => self::ADDITION_SOLVED,
            'ADDITION_FAILED' => self::ADDITION_FAILED,
            'ADDITION_APPROVED' => self::ADDITION_APPROVED,
            'REMINDER' => self::REMINDER,
            'REMINDER_PROCESSED' => self::REMINDER_PROCESSED,
            'REQUEST_MADE' => self::REQUEST_MADE,
            'REQUEST_REJECTED' => self::REQUEST_REJECTED,
            'REQUEST_ACCEPTED' => self::REQUEST_ACCEPTED,
            'REQUEST_ORDERED' => self::REQUEST_ORDERED,
            'REQUEST_ORDER_RECEIVED' => self::REQUEST_ORDER_RECEIVED,
            'TICKET_MADE' => self::TICKET_MADE,
            'TICKET_IN_PROCESS' => self::TICKET_IN_PROCESS,
            'TICKET_REPLIED' => self::TICKET_REPLIED,
            'TICKET_CLOSED' => self::TICKET_CLOSED,
            'FIFTEEN_MADE' => self::FIFTEEN_MADE,
            'FIFTEEN_1ST_REMINDER' => self::FIFTEEN_1ST_REMINDER,
            'FIFTEEN_2ND_REMINDER' => self::FIFTEEN_2ND_REMINDER,
            'FIFTEEN_3RD_REMINDER' => self::FIFTEEN_3RD_REMINDER,
            'FIFTEEN_EXHORT' => self::FIFTEEN_EXHORT,
            'FIFTEEN_DEBT_COLLECTOR' => self::FIFTEEN_DEBT_COLLECTOR,
            'FIFTEEN_RESOLVED' => self::FIFTEEN_RESOLVED,
            // Add more mappings as needed
        ];
    }

    public static function getIntFromText($text) {
        $mapping = self::getTextToIntegerMapping();
        return $mapping[$text] ?? null;
    }
}
