<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Ticketregel extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "ticketregel";

    public function geplaatst_door()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function ticket()
    {
        return $this->belongsTo('App\Models\Ticket');
    }

}
