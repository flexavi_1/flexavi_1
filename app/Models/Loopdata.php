<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Loopdata extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "loopdata";

    public function user_id()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function werknemer_id()
    {
    	return $this->belongsTo('App\Models\Employee');
    }

}
