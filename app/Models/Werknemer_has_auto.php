<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Werknemer_has_auto extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "werknemer_has_auto";

    public function auto()
    {
        return $this->belongsTo('App\Models\Auto');
    }

    public function chauffeur()
    {
    	return $this->belongsTo('App\Models\Employee');
    }

    public function bijrijder()
    {
        return $this->belongsTo('App\Models\Employee');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
