<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inkoop extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "inkoop";

   	public function klant()
   	{
   		return $this->belongsTo('App\Models\Client');
   	}

    public function user()
    {
      return $this->belongsTo('App\Models\User');
    }

    public function getUser()
    {
      return User::find($this->aangemaakt_door);
    }

   	// public function inkoop()
   	// {
   	// 	 return $this->belongsTo('App\Models\Inkoop');
   	// }
}
