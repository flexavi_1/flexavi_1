<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Model;

use App\Models\Offerte_has_status;



class Offerte extends Model

{

    //

    protected $guarded = ['id'];

    public $table = "offerte";



    public function klant()

    {

    	return $this->belongsTo('App\Models\Client');

    }



    public function user()

    {

    	return $this->belongsTo('App\Models\User', 'gebruiker_id');

    }



    public function werknemer()

    {

        return $this->belongsTo('App\Models\Employee');

    }



    public function ohs()

    {

        return $this->hasOne('App\Models\Offerte_has_status');

    }



    public function afspraak()

    {

        return $this->belongsTo('App\Models\Appointment');

    }



    public function aho()

    {

        return $this->hasMany('App\Models\Activiteit_has_offerte');

    }



    public function calcDiff($reminderdate, $today)

    {

        $date1 = new DateTime($reminderdate);

        $date2 = new DateTime($today);

        $interval = $date1->diff($date2);



        return $interval->days;

    }



    public function checkPayment()

    {

        if($this->afspraak_id != null)

        {

            return Appointment::find($this->afspraak_id);

        }

        return null;

    }



    public function getFollowup(){ // checks if offer has been honored or cancelled

        if(Offerte_has_status::where('offerte_id', $this->id)->exists()){

            $ohs = Offerte_has_status::where('offerte_id', $this->id)->first();

            // dd($ohs->status_id);

            if($ohs->status_id == 7 || $ohs->status_id == 6)

                return true;

            return false;

        }

    }



}

