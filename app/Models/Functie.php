<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Functie extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "functie"; 
    public $timestamps = false;
   
}
