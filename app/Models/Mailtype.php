<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mailtype extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "mailtype"; 
    public $timestamps = false;

    public function mails()
    {
        return $this->HasMany('App\Models\Mails');
    }

    

}
