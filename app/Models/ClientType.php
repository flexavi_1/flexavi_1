<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientType extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "klanttype";

    // public function klanttype()
    // {
    // 	return $this->hasMany('App\Models\Klanttype');
    // }

    public function klant()
    {
    	return $this->belongsTo('App\Models\Client');
    }

    // public function werknemer()
    // {
    //     return $this->belongsTo('App\Models\Werknemer');
    // }
}
