<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inkoopverzoek_has_materiaal extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "inkoopverzoek_has_materiaal"; 

    public function inkoopverzoek()
    {
        return $this->belongsTo('App\Models\Inkoopverzoek');
    }

    public function materiaal()
    {
    	return $this->belongsTo('App\Models\Materiaal');
    }
}
