<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $users_id
 * @property string $search
 * @property string $zipcode
 * @property int $housenumber
 * @property int $is_zipcode
 * @property mixed|string $topics
 */
class Search extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    public $table = "search";

    public function user(){
        return $this->belongsTo('App\Models\User', 'users_id');
    }

    public function getLink(){
        if($this->is_zipcode == 1){
            return "/relatie/".Client::wherePostcode($this->zipcode)->whereHuisnummer($this->housenumber)->first()->id;
        } else {
            //we need to execute new search?
            return "#";
        }
    }




}
