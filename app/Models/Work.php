<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Activiteit_has_materiaal;
use Auth;

class Work extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "activiteit";
    public $timestamps = false;

    public function hasAppointment()
    {
        return $this->hasMany(Appointment_has_activities::class, 'activiteit_id');
    }
//
//    public function aho()
//    {
//    	return $this->hasMany('App\Models\Activiteit_has_offerte');
//    }
//
//    public function ahm()
//    {
//        return $this->hasMany('App\Models\Activiteit_has_materiaal');
//    }

    public function hasMats()
    {
        if(Activiteit_has_materiaal::where('activiteit_id', $this->id)->exists())
        {
            return true;
        }
        return false;
    }

    public static function getPrice($id){
        return Work::find($id)->prijs;
    }

    public static function getDescription($id){
        return Work::find($id)->omschrijving;
    }

    public static function createSelfFromArray($input){
        for($i = 0;$i < count($input['prijs']);$i++)
        {
            $temp = new Work;
            $temp->omschrijving = $input['omschrijving'][$i];
            $temp->prijs = $input['prijs'][$i];
            $temp->eenheid = $input['eenheid'][$i];
            $temp->actief = 1;
            $temp->save();
            Log::createLog(Auth::user()->id, null, "Activiteit", Auth::user()->name." heeft de werkactiviteit: ".$input['omschrijving'][$i].", toegevoegd.");
        }
    }


}
