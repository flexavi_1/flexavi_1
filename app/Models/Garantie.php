<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Datetime;
use App\Models\Worklist_has_appointments;
use App\Models\Status_has_appointment;
use App\Models\AppointmentType;
use App\Models\Offerte;
use Auth;

class Garantie extends Model
{
	public $afspraak;

	public function __construct($app_object){
		$this->afspraak = $app_object;
	}

	public function nextCreateStep(){
		$afspraak = $this->afspraak;
		$alleafspraken = $afspraak->getAllAppointmentsWithStatus(true);
		$actperafs = $afspraak->getAllAppointmentsWithActivity(true);
		$relatie = $afspraak->klant;
		$page = "relatie";
		$sub = "relover";
		$array = array($afspraak, $alleafspraken, $actperafs, $relatie, $page, $sub);
		return $array;
	}

	public function defineLastCreateStep($input){
        for($i = 0; $i < count($input['afspraken']);$i++)
        {
        	Appointment_has_activities::setWarrantyActivities($input['afspraken'][$i], $this->afspraak->id, $input);
        	$this->afspraak->afspraak_id = $input['afspraken'][$i];
        }
        $this->afspraak->totaalbedrag = 0;
        $this->afspraak->save();
	}

	public function setProcessOne($gelukt, $extra, $voldaan, $bank, $advies){
		$this->afspraak->advies = $advies;
		if($voldaan == 1){
			$this->afspraak->bank = $bank;
			$this->afspraak->betaald = 1;
		} else {
			$this->afspraak->betaald = 0;
			$this->afspraak->niet_betaald = $this->afspraak->totaalbedrag;
		}
		$this->afspraak->save();
		$status = ($voldaan == 1) ? 4 : 3;
		$message = ($voldaan == 1) ? ": Afspraak is voldaan en verwerkt" : ": Afspraak is niet voldaan, maar wel verwerkt.";
		Status_has_appointment::setSha($this->afspraak->id, $status, $message, $this->afspraak->startdatum);
		$message = ($voldaan == 0) ? Auth::user()->name." heeft de garantie verwerkt, maar is niet opgelost" : date('d-m-Y', strtotime($this->afspraak->startdatum)).": Garantie met de volgende klacht opgelost, ".$this->afspraak->omschrijving;
		History::setHistory($this->afspraak->klant_id, $this->afspraak->id, Auth::user()->id, $message, 'Afspraak');
		Log::createlog(Auth::user()->id, $this->afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft deze klant's, ".$this->afspraak->klant->postcode." - ".$this->afspraak->klant->huisnummer." afspraak verwerkt.");

		if($extra == 1){
			return array($extra);
		}
		return array($voldaan, false);

	}

	public function setProcessTwo($input, $bool){
		if($bool){  //if bool is true there is extra work to handle first
			if(isset($input['klusprijsAan'])){
				$klusprijs = true;
				$this->afspraak->setKlusprijs();
				$this->afspraak->setTotal($input['klusprijs']);
			} else {
				$klusprijs = false;
			}
			$betaald = (isset($input['betaald'])) ? 1 : 0;
			$this->afspraak->betaald = $betaald;
			$this->afspraak->save();
			Voorraad::mutatingStock($this->afspraak->id);
			Appointment_has_activities::addActivityToAppointment($input, $input['afspraak_id'], $klusprijs);
			$this->afspraak->setActivityHistory();
			return ($betaald == 1) ? true : false ;
		}
	}

	public function setProcessThree(){ // only accessible if warranty isn't solved.
		History::setHistory($this->afspraak->klant_id, $this->afspraak->id, Auth::user()->id, Auth::user()->name." heeft de garantie verwerkt, maar is niet opgelost", 'Afspraak');
		Log::createLog(Auth::user()->id, $this->afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft deze garantie van klant, ".$this->afspraak->klant->postcode." - ".$this->afspraak->klant->huisnummer." verwerkt.");
		return true;
	}

	public function setProcessFour($input){
		$app = $input['app'];
		$credit = $input['credit'];
		//no credit, only new appointment
		if($app == 1 && $credit == 0){
			return true;
		// no appointment, only credit
		} elseif($app == 0 && $credit == 1){
			Log::createLog(Auth::user()->id, $this->afspraak->klant_id, 'Crediteur', Auth::user()->name." heeft een terugbetaalverzoek toegevoegd.");
			History::setHistory($this->afspraak->klant_id, $this->afspraak->id, Auth::user()->id, Auth::user()->name." heeft deze klant toegevoegd aan de terugbetaalverzoeken.", 'Crediteur');
			return false;
		} else{
			return array('bool' => 'true', 'filler' => 'heftig');
		}

	}

}
