<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Mail;
use Auth;

class Mails extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "mail";

    public function schrijver()
    {
        return User::find($this->schrijver);
        // return $this->belongsTo('App\Models\User');
    }

    public function klant_id()
    {
    	return $this->belongsTo('App\Models\Client');
    }

    public function mailtype()
    {
        return Mailtype::find($this->mailtype);
        // return $this->belongsTo('App\Models\Mailtype');
    }

    public function afspraak_id()
    {
        return $this->belongsTo('App\Models\Appointment');
    }

    public function factuur_id()
    {
        return $this->belongsTo('App\Models\Factuur');
    }

    public function offerte_id()
    {
        return $this->belongsTo('App\Models\Offerte');
    }

    public function sendMail($customer, $appointment, $bedrijf) // refers to (edited) reminders
    {

    }

    public function setLog($omschrijving)
    {
      $log = new Log;
      $log->gebruiker_id = Auth::user()->id;
      $log->klant_id = $this->klant_id;
      $log->omschrijving = $omschrijving;
      $log->type = "Mail";
      $log->save();
    }

}
