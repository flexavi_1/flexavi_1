<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Appointment;
use App\Models\Offerte;
use App\Models\Appointment_has_activities;

class Activiteit_has_offerte extends Model
{
    //
    protected $fillable = ['activiteit_id', 'offerte_id', 'aantal', 'kortingsperc', 'kortingsbedrag', 'prijs'];
    public $table = "activiteit_has_offerte";
    public $timestamps = false;

    public function offerte()
    {
    	return $this->belongsTo('App\Models\Offerte');
    }

    public function activiteit()
    {
    	return $this->belongsTo('App\Models\Work');
    }

    public static function setToAppointment($offerte_id, $afspraak_id, $input){
        $offerte = Offerte::find($id);
        $ohas = Activiteit_has_offerte::where('offerte_id', $offerte->id)->get();
        $amount = 0;
        for($i = 0; $i < count($input['activiteiten']); $i++)
        {
            foreach($ohas as $oha)
            {
                if($input['activiteiten'][$i] == $oha->activiteit_id)
                {
                    $temp = new Appointment_has_activities;
                    $temp->activiteit_id = $input['activiteiten'][$i];
                    $temp->afspraak_id = $afspraak->id;
                    $temp->aantal = $oha->aantal;
                    $temp->prijs = $oha->prijs;
                    $amount += $oha->prijs;
                    $temp->save();
                }
            }
        }
        if(!isset($input['klusprijsAan'])){
            $afspraak->setTotal($amount);
        } else {
            $ahas = Appointment_has_activities::where('afspraak_id'. $afspraak_id)->get();
            $klusprijs = true;
            $totaalbedrag = $afspraak->totaalbedrag;
            foreach($ahas as $aha){
                $bedrag = $aha->prijs * $aha->aantal - $aha->kortingsbedrag;
                if($totaalbedrag - $bedrag > 0){
                    $aha->kortingsbedrag = $aha->kortingsbedrag;
                    $aha->kortingsperc = $aha->kortingsbedrag / $bedrag * 100;
                    $totaalbedrag -= $bedrag;
                } else {
                    if($totaalbedrag > 0){
                        $aha->kortingsbedrag = $aha->kortingsbedrag + ($bedrag - $totaalbedrag);
                        $aha->kortingsperc = $aha->kortingsbedrag / $bedrag * 100;
                        $totaalbedrag -= $bedrag;
                    } else {
                        $aha->prijs = 0.00;
                        $aha->kortingsbedrag = $act->prijs;
                        $aha->kortingsperc = 100;
                    }
                }
                $aha->save();
            }
        }
    }

}
