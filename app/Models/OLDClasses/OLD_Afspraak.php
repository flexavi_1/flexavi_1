<?php

namespace App\Models\OLDClasses;

use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\History;
use App\Models\Log;
use App\Models\Offerte;
use App\Models\Status;
use App\Models\Status_has_appointment;
use App\Models\User;
use App\Models\Worklist_has_appointments;
use Auth;
use Illuminate\Database\Eloquent\Model;

class OLDAfspraak extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "afspraak";

    public function returnDate($date)
    {
    	$format = date('d-M-Y H:i', strtotime($date));

    	return $format;
    }

    public function afspraaktype()
    {
      return $this->belongsTo('App\Models\AppointmentType');
    }

    public function aha()
    {
    	return $this->hasMany('App\Models\Appointment_has_activities');
    }

   	public function klant()
   	{
   		return $this->belongsTo('App\Models\Client');
   	}

   	public function sha()
   	{
   		return $this->hasMany('App\Models\Status_has_appointment');
   	}

   	public function type()
   	{
   		return $this->belongsTo('App\Models\AppointmentType');
   	}

    public function wha()
    {
      return $this->hasMany('App\Models\Worklist_has_appointments');
    }

    public function verwerkt_door()
    {
      return User::find($this->verwerkt_door);
    }

    public function getReference(){
      return Appointment::find($this->afspraak_id);
    }

    public function ingepland($id)
    {
        if(Worklist_has_appointments::where('afspraak_id', $id)->exists())
          return true;
        else
          return false;
    }

    public function getAmountOpen($id) // method for returning if amount has been paid
    {
        $amount = 0.00;
        $nieuweafspraken = Appointment::where('afspraak_id', $id)->where('afspraaktype_id', 3)->get();
        if($nieuweafspraken->isEmpty())
        {
          return -1;
        }
        foreach($nieuweafspraken as $nas)
        {
          $amount += $nas->totaalbedrag;
        }
        if($amount == Appointment::find($id)->niet_betaald)
          return 1;
        else
          return 0;
    }

    public function hasNewAppointment($id)
    {
      if(Appointment::where('afspraak_id', $id)->exists())
        return true;
      else
        return false;
    }

    public function getAmount($id) // method for returning value of current set of appointments
    {
      $amount = 0.00;
      $afspraken = Appointment::where('afspraak_id', $id)->get();
      foreach ($afspraken as $a)
      {
        if(Status_has_appointment::where('afspraak_id', $a->id)->exists()){
          if(Status_has_appointment::where('afspraak_id', $a->id)->first()->status_id == 4)
          {
            $amount += $a->totaalbedrag;
          }
        }
      }
      if($amount == Appointment::find($id)->niet_betaald)
        return 0;
      else
        return $amount;
    }

    public function getStatus()
    {
      $status = Status::find( Status_has_appointment::where('afspraak_id', $this->id)->first()->status_id );
      return $status;
    }

    public function getType()
    {
      $type = AppointmentType::find($this->afspraaktype_id);
      return $type;
    }

    public function getHistory()
    {
      return History::where('afspraak_id', $this->id)->get();
    }

    public function setLog($omschrijving)
    {
      $log = new Log;
      $log->gebruiker_id = Auth::user()->id;
      $log->klant_id = $this->klant_id;
      $log->omschrijving = $omschrijving;
      $log->type = "Afspraak";
      $log->save();
    }

    public function setStatus($id, $omschrijving)
    {
      $sha = Status_has_appointment::where('afspraak_id', $this->id)->first();
      $sha->status_id = $id;
      $sha->afspraak_id = $this->id;
      $sha->gebruiker_id = Auth::user()->id;
      $sha->omschrijving = $omschrijving;
      $sha->save();
    }

    public function setHistory($omschrijving)
    {
      $regel = new History;
      $regel->datum = date('Y-m-d');
      $regel->klant_id = $this->klant_id;
      $regel->afspraak_id = $this->id;
      $user = User::findOrFail(Auth::user()->id);
      $regel->gebruiker_id = $user->id;
      $regel->omschrijving = $omschrijving;
      $regel->save();
    }

    public function getReminders(){
      $alleafspraken = Status_has_appointment::with('afspraak', 'status')->whereHas('afspraak', function($afd)
      {
        $afd->where('klant_id', $this->klant_id)->where('afspraaktype_id', 5)->groupBy('status_id');
      })->get();

      foreach($alleafspraken as $t)
      {
        $t->afspraak->afspraaktype = AppointmentType::find($t->afspraak->afspraaktype_id)->omschrijving;
      }
      $alleafspraken = $alleafspraken->sortByDesc(function($afspraak){
          return $afspraak->afspraak->startdatum;
      });
      return $alleafspraken;
    }

    public function getRealAppointmentPrice(){ // if 'klusprijs' has been set, get the discount
      if($this->offerte_id != null){
        return Offerte::find($this->offerte_id)->totaalbedrag;
      } else {
        $ahas = $this->aha;
        $total = 0.00;
        foreach($ahas as $aha){
          $total += ($aha->aantal * $aha->activiteit->prijs);
        }
        return $total;
      }
    }

    public function getPreviousApp(){
      return Appointment::find($this->afspraak_id);
    }

    public function changeBevestiging($id){
      $a = Appointment::find($id);
      $a->bevestiging = 2;
      $a->save();
    }
}
