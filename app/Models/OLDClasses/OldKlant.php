<?php

namespace App\Models\OLDClasses;

use App\Models\Appointment;
use App\Models\Bedrijf;
use App\Models\Client;
use App\Models\Log as Logger;
use App\Models\Mails;
use App\Models\Status_has_appointment;
use App\Models\User;
use DateTime;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $straat
 * @property integer $huisnummer
 * @property string $hn_prefix
 * @property string $postcode
 * @property string $woonplaats
 * @property int $bijgewerkt_door
 * @property string $voornaam
 * @property string $achternaam
 * @property string $telefoonnummer_prive
 * @property string $telefoonnummer_zakelijk
 * @property string $email
 */
class OLDKlant extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "klant";

	public function klanttype()
    {
        return $this->belongsTo('App\Models\ClientType', 'klanttype_id');
    }

    public function verwerktDoor()
    {
        return $this->belongsTo('App\Models\User', 'verwerkt_door');
    }

    public function geschrevenDoor()
    {
        return $this->belongsTo('App\Models\Employee', 'geschreven_door');
    }

    public function afspraak()
    {
        return $this->hasMany('App\Models\Appointment', 'klant_id');
    }

    public function factuur()
    {
        return $this->hasMany('App\Models\Factuur', 'klant_id');
    }

    public function crediteur()
    {
        return $this->hasOne('App\Models\Crediteur', 'klant_id');
    }

    public function offerte()
    {
        return $this->hasMany('App\Models\Offerte', 'klant_id');
    }

    public function uur()
    {
        return $this->hasMany('App\Models\Uur');
    }

    public function geschiedenis()
    {
        return $this->hasMany('App\Models\History', 'klant_id');
    }

    public function editedBy()
    {
        return $this->hasMany('App\Models\User', 'bijgewerkt_door');
    }

    public function klantemail()
    {
        return $this->hasMany('App\Models\ClientEmail', 'klant_id');
    }

    public function logs(){
        return $this->hasMany(Logger::class, 'klant_id');
    }

    public function bijgewerktdoor()
    {
        $user = User::find($this->bijgewerkt_door)->first();

        if (empty($user)) {
            return "Niet bijgewerkt";
        }

        return $user->name;
    }

    /**
     * @description Returns the customers based on the search query
     * @param string|null $text
     * @param string|null $zipcode
     * @param int|null $housenumber
     * @return array
     */
    public static function getFromSearch(string $text = null, string $zipcode = null, int $housenumber = null)
    {
        if(!empty($housenumber) && !empty($zipcode)){
            return Client::wherePostcode($zipcode)->whereHuisnummer($housenumber)->get();
        }

        if(!empty($zipcode)){
            return Client::wherePostcode($zipcode)->get();
        }

        if(!empty($housenumber)){
            return Client::whereHuisnummer($housenumber)->get();
        }

        if(!empty($text)){
            return Client::
                where('voornaam', 'LIKE', '%'.$text.'%')
                ->orWhere('telefoonnummer_prive', 'LIKE', '%'.$text.'%')
                ->orWhere('telefoonnummer_zakelijk', 'LIKE', '%'.$text.'%')
                ->orWhere('email', 'LIKE', '%'.$text.'%')
                ->orWhere('achternaam', 'LIKE', '%'.$text.'%')
                ->get();
        }
        return [];
    }

    public function getFullAddress(){
        return $this->straat." ".$this->huisnummer.$this->hn_prefix.",<br>".$this->postcode." te ".$this->woonplaats;
    }

    public function getFullName(){
        return $this->voornaam." ".$this->achternaam;
    }

    public function getFullContact(){
        $phoneString = $this->telefoonnummer_prive;
        if($this->telefoonnummer_zakelijk!= null){
            $phoneString .= " - ".$this->telefoonnummer_zakelijk;
        }
        return $phoneString.'<br>'.$this->email;
    }

    public function calcDiff($reminderdate, $today)
    {

        $date1 = new DateTime($reminderdate);

        $date2 = new DateTime($today);

        $interval = $date1->diff($date2);

        return $interval->days;
    }

    public function returnReasonArray()
    {

        $reason = $this->reden;

        $reasons = explode(",", $reason);

        return $reasons;
    }

    public function getMails()
    {

        $mails = Mails::with('schrijver', 'mailtype')->where('klant_id', $this->id)->get();

        return $mails;
    }

    public static function changeType($id)
    {

        $klant = Client::find($id);

        if ($klant->klanttype_id < 3) {

            $klant->klanttype_id == 3;

            $klant->save();
        }
    }

    public function getCustomerRevenue()
    {

        $afspraken = Appointment::where('klant_id', $this->id)->get();

        $total = 0.00;

        foreach ($afspraken as $a) {

            $shas = Status_has_appointment::where('afspraak_id', $a->id)->get();

            foreach ($shas as $sha) {

                if (count($shas) > 1) {

                    $bool = true;

                    if ($sha->status_id == 1 && $sha->id != Status_has_appointment::where('afspraak_id', $a->id)->max('id')) {

                        $bool = false;
                    }

                    if ($bool && $sha->id == Status_has_appointment::where('afspraak_id', $a->id)->max('id')) {

                        if ($a->betaald == 1 && Status_has_appointment::find($sha->id)->status_id == 4) {

                            if ($a->klusprijs == 1) {

                                // echo $total.' - '.$sha->id.'<br>';

                                $total += $a->totaalbedrag;

                                // echo $total.' - '.$sha->id.'<br>';

                                // echo "1 IF MEERDERE AFSPRAKEN<br>";

                            } else {

                                // echo $total.' - '.$sha->id.'<br>';

                                $total += $a->getTotalFromActivity();

                                // echo $total.' - '.$sha->id.'<br>';

                                // echo "2 IF MEERDERE AFSPRAKEN<br>";

                            }

                            // } else {

                            //     if($a->afspraaktype_id == 1 && $sha->id == Status_has_afspraak::where('afspraak_id', $a->id)->max('id')){

                            //         echo $total.' - '.$sha->id.'<br>';

                            //         $total += ($a->totaalbedrag - $a->niet_betaald);

                            //         echo $total.' - '.$sha->id.'<br>';

                            //         echo "3 IF MEERDERE AFSPRAKEN<br>";

                            //     }

                        }
                    }
                } elseif ($sha->status_id > 1 && count($shas) == 1) {

                    if ($a->betaald == 1) {

                        if ($a->klusprijs == 1) {

                            // echo $total.'<br>';

                            $total += $a->totaalbedrag;

                            // echo $total.'<br>';

                            // echo "4 IF enkele AFSPRAKEN<br>";

                        } else {

                            // echo $total.'<br>';

                            $total += $a->getTotalFromActivity();

                            // echo $total.'<br>';

                            // echo "5 IF enkele AFSPRAKEN<br>";

                        }
                    } else {

                        if ($a->afspraaktype_id == 1) {

                            // echo $total.'<br>';

                            $total += ($a->totaalbedrag - $a->niet_betaald);

                            // echo $total.'<br>';

                            // echo "6 IF enkele AFSPRAKEN<br>";

                        }
                    }
                }
            }
        }

        // dd($total);

        return $total;
    }

    public function getFirstUpcomingApp()
    {

        $afspraak = Appointment::where('klant_id', $id)->where('startdatum', '>', date('Y-m-d'))->orderBy('startdatum', 'asc')->first();
    }

    public static function findMe($id)
    {

        return Client::find($id);
    }

    public function getReminders()
    {

        return Appointment::getStaticReminders($this->id);
    }

    public function inTbLijst()
    {
        $tb = DB::table('status_has_afspraak as sha')
            ->join('afspraak as af', 'af.id', '=', 'sha.afspraak_id')
            ->leftJoin('afspraak as a', 'a.id', '=', 'af.afspraak_id')
            ->join('klant as k', 'k.id', '=', 'af.klant_id')
            ->select(
                'k.id as klant_id',
                'k.voornaam',
                'k.achternaam',
                'k.straat',
                'k.huisnummer',
                'k.hn_prefix',
                'k.postcode',
                'k.woonplaats',
                'k.email',
                'k.telefoonnummer_prive',
                'k.telefoonnummer_zakelijk',
                'af.startdatum as startdatum',
                'af.id as afspraak_id',
                'af.reden'
            )
            ->where('sha.status_id', '=', 2)
            ->where('af.is_verzet', '=', 0)
            ->where('k.id', '=', $this->id)
            ->get();

        return $tb;
    }


    public function getVijftien()
    {

        $vijftien = Status_has_appointment::with('afspraak', 'status')->where('status_id', 2)->orWhere('status_id', 23)->orWhere('status_id', 30)->whereHas('afspraak', function ($afs) {

            $afs->where('klant_id', $this->id);
        })->get();

        foreach ($vijftien as $vft) {

            if (Appointment::where('afspraak_id', $vft->afspraak_id)->exists()) {

                unset($vft);
            } else {

                // $vft->afspraak->afspraaktype_id = Afspraaktype::find($vft->afspraak->afspraaktype_id);

                // $vft->afspraak->verwerkt_door = User::find($vft->afspraak->verwerkt_door);

            }
        }

        // dd($vijftien);

        return $vijftien;
    }

    public function retrieveAddress($postcode, $huisnummer)
    {

        // $url = 'https://cwb.mbivdevelopment.nl/getProCode';

        $pro66p = Bedrijf::getPro6pp();

        $new_url = 'https://api.pro6pp.nl/v1/autocomplete?auth_key=' . $pro66p . '&nl_sixpp=' . $postcode . '&streetnumber=' . $huisnummer;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_URL, $new_url);

        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 5000);

        $result = curl_exec($ch);

        curl_close($ch);

        // echo $result.'<br>';

        $result = json_decode($result);

        // Log::info(print_r($result, true));

        if (is_object($result)) {

            $status = $result->{"status"};

            if ($status == 'ok') {

                $this->straat = $result->{"results"}[0]->{"street"};

                $this->woonplaats = $result->{"results"}[0]->{"city"};

                $this->save();
            } else {

                $this->straat = $result->{"error"}->{"message"};

                $this->save();
            }

            return true;
        }

        return false;
    }

    public static function getTotalRevenue()
    {

        //total revenue

        $totalRevenue = 0.00;

        $klanten = Client::where('actief', 1)->get();

        foreach ($klanten as $k) {

            // echo $k->getCustomerRevenue().'<br>';

            $totalRevenue += $k->getCustomerRevenue();

            // echo $totalRevenue.'<br>';

        }

        return $totalRevenue;
    }

    public function openstaand()
    {
        // OLD
        /*
        $shas = Afspraak::with('klant', 'sha')->where('klant_id', $this->id)->where('afspraaktype_id', 1)->get();

        $total = 0.00;

        $processedIds = array();

        foreach ($shas as $s) {

            $temp = 0.00;
            //if appointment has status
            if (!$s->sha->isEmpty()) {
                //if status is smaller than 23 (and not finished) and is not already dealt with
                if ($s->sha[0]->status_id < 23 && !in_array($s->id, $processedIds)) {
                    //push into processed array
                    array_push($processedIds, $s->id);
                    //appointment is a unfinished one, there is still a need for payment
                    if ($s->sha[0]->status_id != 4) {
                        //if appointment is attached to sha (which should be a must)
                        if ($s->afspraak_id == null) {
                            //init checks
                            $check = false;
                            $none = false;

                            while (!$check) {
                                //s = appointment with customer and status, s->id = appointment.id
                                //collect all appointments with this app as reference
                                $afspraken = Afspraak::where('afspraak_id', $s->id)->get();
                                //foreach of those appointments
                                foreach ($afspraken as $a) {
                                    //if amounts aren't a match
                                    if ($a->totaalbedrag > $s->totaalbedrag) {
                                        //fix the total amount
                                        $s->totaalbedrag = $a->niet_betaald;
                                    }

                                    array_push($processedIds, $a->id);
                                    $tempSha = Status_has_afspraak::where('afspraak_id', $a->id)->first();

                                    if ($tempSha) {

                                        if ($tempSha->status_id == 4) {

                                            $check = true;

                                            $none = true;
                                        }

                                        $tempAfspraken = Afspraak::where('afspraak_id', $a->id)->get();

                                        foreach ($tempAfspraken as $t) {

                                            array_push($processedIds, $t->id);

                                            if (Status_has_afspraak::where('afspraak_id', $t->id)->first()->status_id == 4) {

                                                if ($t->totaalbedrag > $s->totaalbedrag) {

                                                    $s->totaalbedrag = $t->totaalbedrag;
                                                }

                                                $check = true;

                                                $none = true;
                                            }
                                        }
                                    }
                                }

                                if (!$check) {

                                    $temp += $s->niet_betaald;

                                    $check = true;
                                }
                            }
                        }
                    }
                }
            } else {
                $temp = 0;
            }

            $total += $temp;
        }

        return $total;
        */
        return 0;
    }
}
