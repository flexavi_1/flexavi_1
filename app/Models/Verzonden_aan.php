<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verzonden_aan extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "verzonden_aan"; 

    public function aanvulling()
    {
        return $this->belongsTo('App\Models\Aanvulling');
    }

    public function user() // ontvanger
    {
    	return $this->hasMany('App\Models\User');
    }

    public function status()
    {
    	return $this->belongsTo('App\Models\Status');
    }

    public function getStatus()
    {
        return Status::find($this->status_id)->omschrijving;
    }

    public static function getTotalOpen(){
        return Verzonden_aan::where('status_id', '<', 18)->distinct('aanvulling_id')->count();
    }

}
