<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activiteit_has_materiaal extends Model
{
    //
    protected $fillable = ['activiteit_id', 'materiaal_id', 'aantal'];
    public $table = "activiteit_has_materiaal";
    public $timestamps = false;

    public function materiaal()
    {
    	return $this->belongsTo('App\Models\Materiaal');
    }

    public function activiteit()
    {
    	return $this->belongsTo('App\Models\Work');
    }

}
