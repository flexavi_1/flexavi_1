<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inkoop_has_materiaal extends Model
{
    //
    protected $fillable = ['inkoop_id', 'materiaal_id', 'created_at', 'updated_at', 'aantal', 'inkoopprijs', 'datum', 'user_id'];
    public $table = "inkoop_has_materiaal";
   
   	public function materiaal()
   	{
   		return $this->belongsTo('App\Models\Materiaal');
   	}

   	// public function inkoop()
   	// {
   	// 	 return $this->belongsTo('App\Models\Inkoop');
   	// }
}
