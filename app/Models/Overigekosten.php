<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Overigekosten extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "overige_kosten"; 

    public function werkdag()
    {
        return $this->belongsTo('App\Models\Werkdag');
    }
}
