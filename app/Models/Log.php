<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property mixed $gebruiker_id
 * @property mixed $klant_id
 * @property integer $type
 * @property string $omschrijving
 */
class Log extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "log";

    public function returnDate($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function createdBy(): BelongsTo
    {
    	return $this->belongsTo(User::class, 'gebruiker_id');
    }

    public function klant(): BelongsTo
    {
    	return $this->belongsTo(Client::class, 'klant_id');
    }

    /**
     * @description: To create the log
     *
     * @param $gebruiker_id
     * @param $klant_id
     * @param $type
     * @param $omschrijving
     * @return void
     */
    public static function createLog($gebruiker_id, $klant_id, $type, $omschrijving): void
    {
        $log = new Log;
        $log->gebruiker_id = $gebruiker_id;
        $log->klant_id = $klant_id;
        $log->type = $type;
        $log->omschrijving = $omschrijving;
        $log->save();
    }
}
