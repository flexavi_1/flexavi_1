<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inkoopverzoek extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "inkoopverzoek"; 

    public function status()
    {
    	return $this->belongsTo('App\Models\Status');
    }

    public function gebruiker()
    {
        return $this->belongsTo('App\Models\User');
    }

}
