<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Postit extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "Postit"; 

    public function gebruiker()
    {
        return $this->belongsTo('App\Models\User');
    }
}
