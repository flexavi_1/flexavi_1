<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Datetime;
use App\Models\Appointment_has_activities;
use App\Models\Appointment;
use Auth;
use App\Models\Client;
use App\Models\History;
use App\Models\Voorraad;
use App\Models\Status_has_appointment;
use App\Models\Work;

class UitvoerenWerkzaamheden extends Model
{

	public $afspraak;

	public function __construct($app_object){
		$this->afspraak = $app_object;
	}

	public function lastCreateStep($input){
		$klusprijs;
		if(isset($input['klusprijsAan'])){
			$klusprijs = true;
			$this->afspraak->setKlusprijs();
			$this->afspraak->setTotal($input['klusprijs']);
		} else {
			$klusprijs = false;
		}
		//add all activities to appointment

		Appointment_has_activities::addActivityToAppointment($input, $this->afspraak->id, $klusprijs);

		//set history
		$this->afspraak->setActivityHistory();
		$klant = Client::find($this->afspraak->klant_id);
		if(count((array) $klant->geschiedenis()) > 0){
			$geschiedenis = History::getHistoryWithoutAppointment($klant->id, $this->afspraak->id);
			$page = 'relatie';
			$sub = 'relover';
			return array($geschiedenis, $page, $sub);
		}
		return 1;
	}

	public function setProcessOne($gelukt, $extra, $voldaan, $bank, $advies, $sha_id){
		// dd($sha_id);
		$this->afspraak->advies = $advies;
		if($voldaan == 1){
			$this->afspraak->bank = $bank;
			$this->afspraak->betaald = 1;
		} else {
			$this->afspraak->betaald = 0;
			$this->afspraak->niet_betaald = $this->afspraak->totaalbedrag;
		}
		$this->afspraak->save();
		if($sha_id != null){
			if(is_numeric($sha_id)){
				$sha_id = Status_has_appointment::find($sha_id);
			}
		}
		Voorraad::mutatingStock($this->afspraak->id);
		$status = ($voldaan == 1) ? 4 : 3;
		$message = ($voldaan == 1) ? ": Afspraak is voldaan en verwerkt" : ": Afspraak is niet voldaan, maar wel verwerkt.";
		$interval = $this->afspraak->getDateDifference();
		// dd($sha_id);
		if($interval->d > 0){
			if($sha_id != null){
				Status_has_appointment::editSelfWithID($sha_id->id, $status, $message);
			} else {
				Status_has_appointment::editSelf($this->afspraak->id, $status, $message);
			}
		} else {
			Status_has_appointment::setSha($this->afspraak->id, $status, $message, $this->afspraak->startdatum);
		}
		$message = ($voldaan == 0) ? Auth::user()->name." heeft de afspraak verwerkt, afspraak is niet voldaan" : Auth::user()->name." heeft de afspraak verwerkt, afspraak is voldaan";
		History::setHistory($this->afspraak->klant_id, $this->afspraak->id, Auth::user()->id, $message, 'Afspraak');

		History::setHistory($this->afspraak->klant_id, $this->afspraak->id, Auth::user()->id, date('d-m-Y')." Advies: ".$this->afspraak->advies, 'Werkzaamheden');

		Log::createlog(Auth::user()->id, $this->afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft deze klant, ".$this->afspraak->klant->postcode." - ".$this->afspraak->klant->huisnummer." afspraak verwerkt.");

		if($extra == 1){
			return array($extra);
		}
		return array($voldaan, false);

	}

	public function setProcessTwo($input, $bool){
		if($bool){  //if bool is true there is extra work to handle first
			if(isset($input['klusprijsAan'])){
				$klusprijs = true;
				$this->afspraak->setKlusprijs();
				$this->afspraak->setTotal($input['klusprijs']);
			} else {
				$klusprijs = false;
			}
			// Log::info($input);

			if(isset($input['service'])){
				Appointment_has_activities::setService($input['afspraak_id'], $input['service']);
			}
			$check = Appointment_has_activities::addActivityToAppointment($input, $input['afspraak_id'], $klusprijs);
			// Log::info('Check: '.$check);
			$this->afspraak->setActivityHistory();
			// dd($input);
			return ($input['voldaan'] == 1) ? true : false ;
		}
	}
}
