<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Functie_has_Werknemer extends Model
{
    //
    protected $fillable = ['functie_id', 'werknemer_id', 'created_at', 'updated_at', 'verwerkt_door'];
    public $table = "functie_has_werknemer";

   	public function werknemer()
   	{
   		return $this->belongsTo('App\Models\Employee');
   	}

   	public function functie()
   	{
   		return $this->belongsTo('App\Models\Functie');
   	}
}
