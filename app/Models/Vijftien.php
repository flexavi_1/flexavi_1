<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vijftien extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "vijftien";

    public function aangemaakt_door(){
    	return $this->belongsTo('App\Models\User');
    }

    public function bijgewerkt_door(){
    	return $this->belongsTo('App\Models\User');
    }

    public function afspraak(){
    	return $this->belongsTo('App\Models\Appointment');
    }

    public function status(){
    	return $this->belongsTo('App\Models\Status');
    }

    public static function getVijftien($status_id){
    	if($status_id == null){
    		$vft = Vijftien::with('status', 'afspraak')->get();
    		return $vft;
    	}
    	$vft = Vijftien::with('status', 'afspraak')->where('status_id', $status_id)->get();
    	return $vft;
    }

    public function upOneStatus(){
        $this->status_id++;
        $this->save();
    }

}
