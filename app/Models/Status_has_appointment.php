<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AppointmentType;
use Illuminate\Support\Facades\Auth;

class Status_has_appointment extends Model
{
    //
    protected $fillable = ['status_id', 'afspraak_id', 'created_at', 'updated_at', 'gebruiker_id', 'omschrijving'];
    public $table = "status_has_afspraak";

    public function returnDate($date)
    {
        $format = date('d-M-Y H:i', strtotime($date));

        return $format;
    }

    public function status()
    {
    	return $this->belongsTo('App\Models\Status');
    }

   	public function afspraak()
   	{
   		return $this->belongsTo(Appointment::class, 'afspraak_id');
   	}

    public static function setSha($afspraak_id, $status_id, $message, $datum){
      $sha = '';
      $wd = Werkdag::createNew($datum);
      if(!Status_has_appointment::where('afspraak_id', $afspraak_id)->where('werkdag_id', $wd)->exists()){
        $sha = new Status_has_appointment;
      } else {
        $sha = Status_has_appointment::where('afspraak_id', $afspraak_id)->where('werkdag_id', $wd)->first();
      }
      $sha->afspraak_id = $afspraak_id;
      $sha->status_id = $status_id;
      $sha->gebruiker_id = Auth::user()->id;
      $sha->omschrijving = Auth::user()->name." ".$message;
      $sha->werkdag_id = $wd;
      $sha->save();
    }

    //bool defines whether we need to return all non paid appointments.
    public static function returnAllAppointments($klant_id, $bool){
      if(!$bool){
        $alleafspraken = Status_has_appointment::with('afspraak', 'status')->whereHas('afspraak', function($afd) use ($klant_id)
        {
            $afd->where('klant_id', $klant_id)->where('niet_betaald', '!=', 0)->groupBy('status_id');
        })->get();
      } else {
        $alleafspraken = Status_has_appointment::with('afspraak', 'status')->whereHas('afspraak', function($afd) use ($klant_id)
        {
            $afd->where('klant_id', $klant_id)->groupBy('status_id');
        })->get();
      }
      foreach($alleafspraken as $a){
        $a->afspraak->afspraaktype = AppointmentType::getDescription($a->afspraak->afspraaktype_id);
      }
      $alleafspraken = $alleafspraken->sortByDesc( function($afspraak){
        return $afspraak->afspraak->startdatum;
      });
      return $alleafspraken;
    }

    //only applicable when appointment covers multiple days
    public static function editSelf($afspraak_id, $status_id, $message){
      $sha = Status_has_appointment::where('afspraak_id', $afspraak_id)->where('status_id', 1)->orderBy('id', 'asc')->first();
      $sha->status_id = $status_id;
      $sha->gebruiker_id = Auth::user()->id;
      $sha->omschrijving = Auth::user()->name." ".$message;
      $sha->save();
    }

    //only applicable when appointment covers multiple days
    public static function editSelfWithID($sha_id, $status_id, $message){
      $sha = Status_has_appointment::find($sha_id);
      $sha->status_id = $status_id;
      $sha->gebruiker_id = Auth::user()->id;
      $sha->omschrijving = Auth::user()->name." ".$message;
      $sha->save();
    }

    public static function returnShas($afspraak_id){
      return Status_has_appointment::where('afspraak_id', $afspraak_id)->where('status_id', 1)->get();
    }

    public static function getNull($afspraak_id){
      if(Status_has_appointment::where('afspraak_id', $afspraak_id)->where('werkdag_id', null)->exists()){
        return true;
      }
      return false;
    }

    public static function returnNextStep($sha_id, $afspraak_id){
      if(Status_has_appointment::where('afspraak_id', $afspraak_id)->count() > 1){ // if its about a chain of appointments
        $currentShaID = $sha_id;
        if($currentShaID != Status_has_appointment::where('afspraak_id', $afspraak_id)->max('id')){ // this isnt the last appointment in the chain
            return false;
        }
      }
      return true;
    }

}
