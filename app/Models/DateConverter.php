<?php

namespace App\Models;

use DateInterval;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DateConverter extends Model
{
    use HasFactory;

    /**
     * @description Calculates the reminder date a year later based on original date
     * @throws Exception
     */
    public static function daysToReminder($days, $baseDate): array
    {
        $dateInterval = new DateInterval('P' . abs($days) . 'D');

        // Create a base date
        $baseDate = new DateTime(date('Y-m-d', strtotime($baseDate)));

        // Clone the base date to avoid modifying it directly
        $targetDate = clone $baseDate;

        // Add or subtract the interval to the target date
        if ($days < 0) {
            $targetDate->sub($dateInterval);
        } else {
            $targetDate->add($dateInterval);
        }

        // Get the difference in years, months, and days
        $diff = $baseDate->diff($targetDate);

        // Format the result in an array
        $result = [
            'years' => $diff->y,
            'months' => $diff->m,
            'days' => $diff->d,
        ];
        $string = "";
        if($result['years'] > 0){
            $string .= $result['years']. " Jaar/Jaren ";
        }
        if($result['months'] > 0){
            $string.= $result['months']. " Maand(en) ";
        }
        if($days < 0){
            if($result['days'] > 0){
                $string.= $result['days']. " dag(en) te laat met de reminder";
            }
        }
        else {
            if($result['days'] > 0){
                $string.= $result['days']. " dag(en) voor de reminder";
            }
        }

        return [
            'string' => $string,
            'days' => $days
            ];
    }

    /**
     * $description - Calculates the difference in days between two dates
     * @param $date1 - Startdate
     * @param $date2 - Enddate
     * @return int
     */
    public static function getDaysDifferenc($date1, $date2): int
    {
        $date1 = strtotime($date1);
        $date2 = strtotime($date2);
        $diff = abs($date1 - $date2);
        return floor($diff / (60 * 60 * 24));
    }

	public static function isInThePast($startdatum, $einddatum)
	{
        $now = new DateTime();
        $start = new DateTime($startdatum);
        $end = new DateTime($einddatum);

        if($now > $start){
            if($now > $end){
                return true;
            } else {
                return false;
            }
        }
        return true;
	}
}
