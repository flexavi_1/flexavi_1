<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Model;

use App\Models\Appointment;

use App\Models\Status_has_appointment;

use App\Models\Worklist;

class Werkdag extends Model

{

    //

    protected $guarded = ['id'];

    public $table = "werkdag";

    public $timestamps = false;



    public function werklijst()

    {

        return $this->hasMany('App\Models\Worklist');
    }



    public function sha()
    {

        return $this->hasMany('App\Models\Status_has_appointment');
    }

    public static function refreshCalender()
    {
        // dd('etst');
        //get alle afspraken
        $afspraken = Appointment::where('startdatum', '>', '2020-09-30 00:00:00')->get();
        // dd($afspraken);
        //voor elke afspraak
        foreach ($afspraken as $a) {
            //bepaal datum bereik van afspraak
            $dateArray = $a->getDateDifferenceDateArray();
            //alleen uitvoeren als we meerdere data hebben.
            if (count($dateArray) > 1) {
                //controleer voor elke datum of er een werkdag bestaat
                foreach ($dateArray as $date) {
                    //if exists
                    // dd($date);
                    if (!Werkdag::where('datum', date('Y-m-d', strtotime($date)))->exists()) {
                        //alleen iets mee doen wanneer die niet bestaat.
                        $wd_id = Werkdag::createNew($date);
                        $wd = Werkdag::find($wd_id);
                        for ($i = 0; $i < $wd->aantal_lijsten; $i++) {
                            $wd_id = Werkdag::createNew($date);
                            $wd = Werkdag::find($wd_id);

                            // Werklijst::createNew($date->datum, $wd->id);
                        }
                    }
                }
            }
        }

        return 'doorlopen';
    }

    public static function createNew($date)
    {

        $date = date('Y-m-d', strtotime($date));

        if ($date == '1970-01-01') {

            dd('Er gaat iets fout. De datum is namelijk 01-01-1970.');
        }

        if (Werkdag::where('datum', $date)->exists()) {

            return Werkdag::where('datum', date('Y-m-d', strtotime($date)))->first()->id;
        }

        $wd = new Werkdag();

        $wd->datum = date('Y-m-d', strtotime($date));

        $wd->save();

        return $wd->id;
    }



    public static function returnDate($id)
    {
        $werkday = Werkdag::find($id);

        if ($werkday) {
            return date('d-m-Y', strtotime($werkday->datum));
        } else {
            // Handle the case where the record with the given $id is not found
            return "Record not found";
        }
    }




    public function getAantalKlanten()
    {

        // $aantal = Status_has_afspraak::where('werkdag_id', $this->id)->where('status_id', 1)->count();

        if ($this->datum >= date('Y-m-d'))

            $aantal = $this->sha()->where('status_id', 1)->count();

        else

            $aantal = $this->sha()->count();

        return $aantal;
    }



    public function getAantalUB()
    {

        $aantal = Status_has_appointment::where('werkdag_id', $this->id)->where('status_id', 1)->whereHas('afspraak', function ($afs) {

            $afs->where('afspraaktype_id', 3);
        })->get();

        return $aantal->count();
    }



    public function getAantalPayReminders()
    {

        $aantal = Status_has_appointment::where('werkdag_id', $this->id)->where('status_id', 1)->whereHas('afspraak', function ($afs) {

            $afs->where('afspraaktype_id', 5);
        })->get();

        // dd($aantal);

        return $aantal->count();
    }
}
