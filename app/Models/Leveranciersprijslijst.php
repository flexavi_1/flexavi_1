<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leveranciersprijslijst extends Model
{
    //
    protected $fillable = ['klant_id', 'materiaal_id', 'created_at', 'updated_at', 'btw-tarief', 'inkoopprijs', 'omschrijving'];
    public $table = "leveranciersprijslijst";

   	public function materiaal()
   	{
   		return $this->belongsTo('App\Models\Materiaal');
   	}

   	public function relatie()
   	{
   		 return $this->belongsTo('App\Models\Client');
   	}
}
