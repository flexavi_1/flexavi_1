<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Datetime;

class Factuur extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "factuur"; 
    public $timestamps = false;

    public function setDuedate($date)
    {
        $time = strtotime($date);
        $final = date("Y-m-d", strtotime("+1 month", $time));

        return $final;
    }

    public function klant()
    {
        return $this->belongsTo('App\Models\Models\Klant');
    }
    
    public function shf()
    {
        return $this->hasMany('App\Models\Models\Status_has_factuur');
    }

    public function calcDiff($duedate, $currdate)
    {
        $date1 = new DateTime($duedate);
        $date2 = new DateTime($currdate);
        $interval = $date1->diff($date2);

        return $interval->days;
    }
}
