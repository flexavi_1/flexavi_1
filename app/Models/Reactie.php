<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reactie extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "reactie"; 

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function verzonden_aan() // ontvanger
    {
    	return $this->hasMany('App\Models\Verzonden_aan');
    }

}
