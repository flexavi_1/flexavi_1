<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Client;

/**
 * @property mixed $klant_id
 * @property mixed $email
 */
class ClientEmail extends Model
{
	protected $fillable = ['klant_id', 'email'];
    public $table = "klant_email";
    public $timestamps = false;
    public $autoincrementing = false;

    public function client(){
    	return $this->belongsTo('App\Models\Client');
    }

    public function refresh(){
        // if(date('H:i') == '00:00'){
    	$klanten = Client::all();
    	foreach ($klanten as $k) {
    		if($k->email != null){
    			if(!ClientEmail::where('klant_id', $k->id)->where('email', $k->email)->exists()){
		    		$ke = new ClientEmail;
		    		$ke->klant_id = $k->id;
		    		$ke->email = $k->email;
		    		// $k->email = null;
		    		$k->save();
		    		$ke->save();
	    		}
	    	}
    	}
        // }
    }

    public static function getAllEmailFromOneCustomer($id){
        // Klantemail::refresh();
    	return ClientEmail::where('klant_id', $id)->get();
    }
}
