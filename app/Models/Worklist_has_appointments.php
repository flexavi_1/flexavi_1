<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worklist_has_appointments extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "werklijst_has_afspraak";

    public function worklist()
    {
        return $this->belongsTo(Worklist::class, 'werklijst_id');
    }

    public function afspraak()
    {
    	return $this->belongsTo('App\Models\Appointment');
    }

    public function returnHistory()
    {
        $history = App\Models\Afspraak_has_geschiedenis::with('geschiedenis')->where('afspraak_id', $this->afspraak_id)->get();
        if(!isEmpty($history))
            return $history;

        return false;
    }

    public function aanvulling($afspraak_id)
    {
        $afspraak = Appointment::find($afspraak_id);
        // dd($afspraak);
        $aanvulling = Aanvulling::where('klant_id', $afspraak->klant_id)->get();

        if(count($aanvulling) > 0)
        {
            foreach($aanvulling as $a)
            {
                $verzonden = Verzonden_aan::where('aanvulling_id', $a->id)->get();
                foreach($verzonden as $v)
                {
                    $v->ontvanger = User::find($v->ontvanger);
                }
            }
            return $verzonden;
        }

        return null;
    }

    public static function deleteAppFromWorklist($afspraak){
        if(Worklist_has_appointments::where('afspraak_id', $afspraak)->exists()){
            $wha = Worklist_has_appointments::where('afspraak_id', $afspraak)->first();
            $wha->delete();
        }
    }
}
