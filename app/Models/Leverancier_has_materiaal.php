<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leverancier_has_materiaal extends Model
{
    //
    protected $fillable = ['klant_id', 'materiaal_id', 'created_at', 'updated_at', 'aantal', 'inkoopprijs', 'datum', 'user_id'];
    public $table = "leveranciers_has_materiaal";

   	public function materiaal()
   	{
   		return $this->belongsTo('App\Models\Materiaal');
   	}

   	public function relatie()
   	{
   		 return $this->belongsTo('App\Models\Client');
   	}
}
