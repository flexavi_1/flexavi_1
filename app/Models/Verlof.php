<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verlof extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "verlof";

    public function werknemer()
    {
        return $this->belongsTo('App\Models\Employee');
    }
}
