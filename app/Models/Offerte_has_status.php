<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offerte_has_status extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "offerte_has_status";

    public function returnDate($date)
    {
        $format = date('d-M-Y H:i', strtotime($date));

        return $format;
    }

    public function status()
    {
    	return $this->belongsTo('App\Models\Status');
    }
   
   	public function offerte()
   	{
   		return $this->belongsTo('App\Models\Offerte');
   	}
}
