<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Datetime;
use App\Models\Worklist_has_appointments;
use App\Models\Status_has_appointment;
use App\Models\AppointmentType;
use App\Models\Offerte;
use Auth;
use App\Models\Log;
use App\Models\History;

class Terugbetaalverzoek extends Model
{
	public $afspraak;

	public function __construct($app_object){
		$this->afspraak = $app_object;
	}

	public function nextCreateStep(){
		$afspraak = $this->afspraak;
		Log::createLog(Auth::user()->id, $afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft een terugbetaalverzoek toegevoegd.");
		History::setHistory($afspraak->klant_id, $afspraak->id, Auth::user()->id, Auth::user()->name." heeft deze klant toegevoegd aan de terugbetaalverzoeken.", 'Terugbetaalverzoek');
		$page = 'financieel';
		$sub = 'crediteur';
		$relatie = $afspraak->klant;
		$array = array($relatie, $afspraak, $page, $sub);
		return $array;
	}

	public function setProcessOne($gelukt, $extra, $voldaan, $bank, $betalingsherinnering){

	}

}
