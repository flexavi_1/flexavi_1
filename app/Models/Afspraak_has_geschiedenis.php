<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Afspraak_has_geschiedenis extends Model
{
    //
    protected $fillable = ['afspraak_id', 'geschiedenis_id'];
    public $table = "afspraak_has_geschiedenis";
    public $timestamps = false;

    public function afspraak()
    {
    	return $this->belongsTo('App\Models\Appointment');
    }

    public function geschiedenis()
    {
    	return $this->belongsTo('App\Models\History');
    }

    public static function copyHistory($oud_id, $nieuw_id){
        $geschiedenis = Afspraak_has_geschiedenis::where('afspraak_id', $oud_id)->get();
        foreach ($geschiedenis as $g) {
            $ahg = new Afspraak_has_geschiedenis;
            $ahg->geschiedenis_id = $g->geschiedenis_id;
            $ahg->afspraak_id = $nieuw_id;
            $ahg->datum = date('Y-m-d');
            $ahg->save();
        }
    }

    public static function createConnection($input, $afspraak){
        if(!isset($input['update']))
        {
            if(isset($input['geschiedenis']))
            {
                for($i = 0; $i < count($input['geschiedenis']); $i++)
                {
                    $ahg = new Afspraak_has_geschiedenis;
                    $ahg->afspraak_id = $afspraak->id;
                    $ahg->geschiedenis_id = $input['geschiedenis'][$i];
                    $ahg->datum = date('Y-m-d', strtotime($afspraak->startdatum));
                    $ahg->save();
                }
            }
        }
        else
        {
            if(isset($input['geschiedenis']))
            {
                $ghas = Afspraak_has_geschiedenis::where('afspraak_id', $afspraak->id)->get();
                foreach($ghas as $gha)
                {
                    $gha->delete();
                }

                for($i = 0; $i < count($input['geschiedenis']); $i++)
                {
                    $ahg = new Afspraak_has_geschiedenis;
                    $ahg->afspraak_id = $afspraak->id;
                    $ahg->geschiedenis_id = $input['geschiedenis'][$i];
                    $ahg->save();
                }
            }
            else
            {
                $ghas = Afspraak_has_geschiedenis::where('afspraak_id', $afspraak->id)->get();
                foreach($ghas as $gha)
                {
                    $gha->delete();
                }
            }
        }
    }

    public static function getAllFromAppointment($id){
        return Afspraak_has_geschiedenis::with('geschiedenis')->where('afspraak_id', $id)->get();
    }

}
