<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status_has_Factuur extends Model
{
    //
    protected $fillable = ['factuur_id', 'status_id', 'created_at', 'updated_at', 'verwerkt_door'];
    public $table = "status_has_factuur";
   
   	public function status()
   	{
   		return $this->belongsTo('App\Models\Status');
   	}

   	public function factuur()
   	{
   		return $this->belongsTo('App\Models\Factuur');
   	}
}
