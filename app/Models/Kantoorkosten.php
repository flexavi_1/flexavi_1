<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kantoorkosten extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "kantoorkosten";

    public function werkdag()
    {
        return $this->belongsTo('App\Models\Werkdag');
    }

    public function werknemer_id()
    {
    	return $this->belongsTo('App\Models\Employee');
    }
}
