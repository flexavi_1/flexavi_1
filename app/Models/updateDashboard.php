<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\DatamartController;
use Log;

class updateDashboard extends Model
{
	public function start(){
		if(date('H:i') == '00:00'){
			$dm = new DatamartController();
			$dm->loadActivity();
	        $dm->loadEmployee();
	        $dm->loadTime();
	        $dm->loadPlace();
	        $dm->loadTypes();
	        $dm->clearFacts();
	        $dm->loadFacts();
	        Log::notice('Datamart bijgewerkt');
		}
	}
}