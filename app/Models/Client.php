<?php

namespace App\Models;

use App\Models\Appointment;
use App\Models\Status_has_appointment;
use DateTime;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Log as Logger;

/**
 * @property string $straat
 * @property integer $huisnummer
 * @property string $hn_prefix
 * @property string $postcode
 * @property string $woonplaats
 * @property int $bijgewerkt_door
 * @property string $voornaam
 * @property string $achternaam
 * @property string $telefoonnummer_prive
 * @property string $telefoonnummer_zakelijk
 * @property string $email
 */
class Client extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "klant";

    public function clienttypes()
    {
        return $this->belongsTo('App\Models\ClientType', 'klanttype_id');
    }

    public function processedBy()
    {
        return $this->belongsTo('App\Models\User', 'verwerkt_door');
    }

    public function writtenBy()
    {
        return $this->belongsTo('App\Models\Employee', 'geschreven_door');
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class , 'klant_id')->orderBy('startdatum', 'desc');
    }

    public function logs(){
        return $this->hasMany(Logger::class, 'klant_id')->orderBy('created_at', 'desc');
    }

    public function factuur()
    {
        return $this->hasMany('App\Models\Factuur', 'klant_id');
    }

    public function crediteur()
    {
        return $this->hasOne('App\Models\Crediteur', 'klant_id');
    }

    public function offerte()
    {
        return $this->hasMany('App\Models\Offerte', 'klant_id');
    }

    public function uur()
    {
        return $this->hasMany('App\Models\Uur');
    }

    public function history()
    {
        return $this->hasMany(History::class, 'klant_id');
    }

    public function editedBy()
    {
        return $this->hasMany('App\Models\User', 'bijgewerkt_door');
    }

    public function clientemails()
    {
        return $this->hasMany('App\Models\ClientEmail', 'klant_id');
    }

    /**
     * @description Returns the customers based on the search query
     * @param string|null $text
     * @param string|null $zipcode
     * @param int|null $housenumber
     * @return array
     */
    public static function getFromSearch(string $text = null, string $zipcode = null, int $housenumber = null)
    {
        if(!empty($housenumber) && !empty($zipcode)){
            return Client::wherePostcode($zipcode)->whereHuisnummer($housenumber)->get();
        }

        if(!empty($zipcode)){
            return Client::wherePostcode($zipcode)->get();
        }

        if(!empty($housenumber)){
            return Client::whereHuisnummer($housenumber)->get();
        }

        if(!empty($text)){
            return Client::
            where('voornaam', 'LIKE', '%'.$text.'%')
                ->orWhere('telefoonnummer_prive', 'LIKE', '%'.$text.'%')
                ->orWhere('telefoonnummer_zakelijk', 'LIKE', '%'.$text.'%')
                ->orWhere('email', 'LIKE', '%'.$text.'%')
                ->orWhere('achternaam', 'LIKE', '%'.$text.'%')
                ->get();
        }
        return [];
    }

    /**
     * @description Returns the next appointment date string or nothing if none
     * @return string
     */
    public function getNextAppointment(): string
    {
        foreach($this->appointments as $appointment){
            if(!$appointment->isMultipleDays()){
                //single day appointment
                if($appointment->sha[0]->status_id == Status::APT_MADE && !DateConverter::isInThePast($appointment->startdatum, $appointment->einddatum)){
                    return date('d-m-Y', strtotime($appointment->startdatum));
                } else {
                    return "--";
                }
            }
            //multiple days appointment
            foreach($appointment->sha as $sha){
                if($sha->status_id == Status::APT_MADE){
                    return date('d-m-Y', strtotime($sha->workday->datum));
                }
            }
        }
        return "--";
    }
    public function getFullAddress(){
        return $this->straat." ".$this->huisnummer.$this->hn_prefix.",<br>".$this->postcode." te ".$this->woonplaats;
    }

    public function getFullName(){
        return $this->voornaam." ".$this->achternaam;
    }

    public function getFullContact(){
        $phoneString = $this->telefoonnummer_prive;
        if($this->telefoonnummer_zakelijk!= null){
            $phoneString .= " - ".$this->telefoonnummer_zakelijk;
        }
        return $phoneString.'<br>'.$this->email;
    }

    public function retrieveAddress($postcode, $huisnummer)
    {

        // $url = 'https://cwb.mbivdevelopment.nl/getProCode';

        $pro66p = Bedrijf::getPro6pp();

        $new_url = 'https://api.pro6pp.nl/v1/autocomplete?auth_key=' . $pro66p . '&nl_sixpp=' . $postcode . '&streetnumber=' . $huisnummer;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_URL, $new_url);

        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 5000);

        $result = curl_exec($ch);

        curl_close($ch);

        // echo $result.'<br>';

        $result = json_decode($result);

        // Log::info(print_r($result, true));

        if (is_object($result)) {

            $status = $result->{"status"};

            if ($status == 'ok') {

                $this->straat = $result->{"results"}[0]->{"street"};

                $this->woonplaats = $result->{"results"}[0]->{"city"};

                $this->save();
            } else {

                $this->straat = $result->{"error"}->{"message"};

                $this->save();
            }

            return true;
        }

        return false;
    }

}
