<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Datetime;
use App\Models\Worklist_has_appointments;
use App\Models\Status_has_appointment;
use App\Models\AppointmentType;
use App\Models\Offerte;
use Auth;
use App\Models\Appointment_has_activities;
use App\Models\Work;

class OfferteOpstellen extends Model
{
	public $afspraak;

	public function __construct($app_object){
		$this->afspraak = $app_object;
	}

	public function nextCreateStep($voorrijkosten){
		if($voorrijkosten == 1){
			Appointment_has_activities::addStandardAha($this->afspraak->id, Work::where('omschrijving', 'Voorrijkosten')->first()->id, 1);
		}
	}

	public function setProcessOne($gelukt, $extra, $voldaan, $bank, $advies){
		$this->afspraak->advies = $advies;
		if($voldaan == 1){
			$this->afspraak->bank = $bank;
			$this->afspraak->betaald = 1;
		} else {
			$this->afspraak->betaald = 0;
			$this->afspraak->niet_betaald = $this->afspraak->totaalbedrag;
		}
		$this->afspraak->save();
		$status = ($voldaan == 1) ? 4 : 3;
		$message = ($voldaan == 1) ? ": Afspraak is voldaan en verwerkt" : ": Afspraak is niet voldaan, maar wel verwerkt.";
		Status_has_appointment::setSha($this->afspraak->id, $status, $message, $this->afspraak->startdatum);
		$message = ($voldaan == 0) ? Auth::user()->name." heeft de afspraak verwerkt, maar is niet voldaan" : Auth::user()->name." heeft de afspraak verwerkt en is voldaan" ;
		History::setHistory($this->afspraak->klant_id, $this->afspraak->id, Auth::user()->id, $message, 'Afspraak');
		Log::createlog(Auth::user()->id, $this->afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft deze klant's, ".$this->afspraak->klant->postcode." - ".$this->afspraak->klant->huisnummer." afspraak verwerkt.");

		if($extra == 1){
			return array($extra);
		}
		return array($voldaan, false);
	}

	public function setProcessTwo($input){
		$klusprijs;
		if(isset($input['klusprijsAan'])){
			$klusprijs = true;
			$this->afspraak->setKlusprijs();
			$this->afspraak->setTotal($input['klusprijs']);
		} else {
			$klusprijs = false;
		}
		Appointment_has_activities::addActivityToAppointment($input, $this->afspraak->id, $klusprijs);
		$this->afspraak->setActivityHistory();
		if(isset($input['voldaan']))
			return true;
		return false;
	}



}
