<?php

namespace App\Models;

use App\Models\AppointmentType;
use App\Models\Betalingsherinnering;
use App\Models\Garantie;
use App\Models\Offerte;
use App\Models\OfferteOpstellen;
use App\Models\Status_has_appointment;
use App\Models\Terugbetaalverzoek;
use App\Models\UitgesteldeBetaling;
use App\Models\UitvoerenWerkzaamheden;
use App\Models\Worklist_has_appointments;
use Auth;
use Datetime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property AppointmentType $afspraaktype_id
 */
class Appointment extends Model
{

  //

  protected $guarded = ['id'];

  public $table = "afspraak";

    public function sha(): HasMany
    {
        return $this->hasMany(Status_has_appointment::class, 'afspraak_id');
    }
    public function type(): BelongsTo
    {
        return $this->belongsTo(AppointmentType::class, 'afspraaktype_id');
    }

    public function history(): HasMany
    {
        return $this->hasMany(History::class, 'afspraak_id')->orderBy('created_at', 'desc');
    }

    public function referalAppointments(){
        return $this->hasMany(Appointment::class,'afspraak_id');
    }

    public function appointment(){
        return $this->belongsTo(Appointment::class, 'afspraak_id');
    }

    public function writtenBy(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'geschreven_door');
    }

    public function processedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'verwerkt_door');
    }

    public function editedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'bijgewerkt_door');
    }

    public function hasWork(){
        return $this->hasMany(Appointment_has_activities::class, 'afspraak_id');
    }

    /**
     * @description 1 client has 0.* Appointments
     * @return BelongsTo
     */
    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'klant_id');
    }

    /**
     * @description searching through the database
     * @param $text
     * @param $zipcode
     * @param $housenumber
     * @return array|Collection
     */
    public static function getFromSearch($text, $zipcode, $housenumber): Collection|array
    {
        if(!empty($housenumber) && !empty($zipcode)){
            $klant = Client::wherePostcode($zipcode)->whereHuisnummer($housenumber)->first();
            return Appointment::whereKlantId($klant->id)->get();
        }

        if(!empty($text)){
            return Appointment::where('omschrijving', 'LIKE', '%'.$text.'%')->get();
        }
        return [];
    }

    /**
     * @desciription Checks if the day difference between start and enddate is higher than 0. If yes -> return true, else return false
     * @return bool
     */
    public function isMultipleDays(): bool
    {
        return (DateConverter::getDaysDifferenc($this->startdatum, $this->einddatum) > 0);
    }

    /**
     * @description Get the correct date string: Afspraak aptNumber van de total number of days
     * @return string
     */
    public function getNthDayString(): string
    {
        $days = DateConverter::getDaysDifferenc($this->startdatum, $this->einddatum);
        $aptNumber = Status_has_appointment::whereAfspraakId($this->id)->where('status_id', '>', Status::APT_MADE)->count();
        return "Afspraak $aptNumber van de $days";
    }

    /**
     * $description get the correct date string, this is depended on whether it's multiple days or not.
     * @return string
     */
    public function getDateString(): string
    {
        if($this->isMultipleDays()){
            return date('d-m-Y', strtotime($this->startdatum)).' t/m '.date('d-m-Y', strtotime($this->einddatum));
        }
        return date('d-m-Y H:i', strtotime($this->startdatum))." ".date('H:i', strtotime($this->einddatum));
    }

    /**
     * @description Get the correct current Status_has_appointment
     * @return Status_has_appointment
     */
    public function getLastStatus(): object
    {
        return Status_has_appointment::whereAfspraakId($this->id)->orderBy('werkdag_id', 'desc')->first();
    }

    /**
     * @description retrieve the salesman related to the appointment
     * @return Employee|bool
     */
    public function getSalesMan():  Employee|bool
    {
        $sha = Status_has_appointment::whereAfspraakId($this->id)->orderBy('werkdag_id', 'asc')->first();
        $worklist_has_appointments = Worklist_has_appointments::whereAfspraakId($sha->afspraak_id)->get();
        // we need to find the match between sha and worklist -> TODO: be mindful about processing time here...
        foreach($worklist_has_appointments as $wha){
            if($wha->worklist->werkdag_id == $sha->werkdag_id){
                if($wha->worklist->salesman !== null){
                    return $wha->worklist->salesman;
                }
            }
        }
        return false;
    }

    /**
     * @description retrieve the worker related to the appointment
     * @return Employee|bool
     */
    public function getWorker(): Employee|bool
    {
        $sha = Status_has_appointment::whereAfspraakId($this->id)->orderBy('werkdag_id', 'asc')->first();
        $worklist_has_appointments = Worklist_has_appointments::whereAfspraakId($sha->afspraak_id)->get();
        // we need to find the match between sha and worklist -> TODO: be mindful about processing time here...
        foreach($worklist_has_appointments as $wha){
            if($wha->worklist->werkdag_id == $sha->werkdag_id){
                if($wha->worklist->worker !== null){
                    return $wha->worklist->worker;
                }
            }
        }
        return false;
    }

    /**
     * @description Get the history for the appointment, use an amount is possible
     * @param int $amount
     * @return Collection
     */
    public function getLastHistory(int $amount = 0): Collection
    {
        if($amount == 0){
            return $this->history;
        } else {
            return History::whereAfspraakId($this->id)->take($amount)->orderBy('created_at', 'desc')->get();
        }
    }

    /**
     * @description Get the number of subappointments made for this appointment
     * @return int
     */
    public function countSubAppointments(): int
    {
        return Status_has_appointment::whereAfspraakId($this->id)->count();
    }

    /**
     * @description check if appointment is planned inside a worklist
     * @return bool
     */
    public function isPlanned(): bool
    {
        return Worklist_has_appointments::whereAfspraakId($this->id)->exists();
    }

    /**
     * @description Get the employee string for full appointment modal
     * @return string
     */
    public function getPlannedEmployeesString(): string
    {
        $salesman = $this->getSalesMan();
        $worker = $this->getWorker();
        $string = $salesman->getFullName();
        if($worker !== false){
            $string.= " & ".$worker->getFullName();
        }
        return $string;
    }

    /**
     * @description Get the first payment appointment related to this appointment
     * @return Appointment|bool
     */
    public function getFirstPaymentAppointment(): Appointment|bool
    {
        $apt = Appointment::whereAfspraakId($this->id)
                    ->whereAfspraaktypeId(AppointmentType::TYPE_DEFERRED)
                    ->orWhere('afspraaktype_id', AppointmentType::TYPE_REMINDER)
                    ->first();
        return  $apt ?? false;
    }

    /**
     * @description get the Payment Method for this appointment
     * @return string
     */
    public function getPaymentMethodString(): string
    {
        if($this->afspraaktype_id == AppointmentType::TYPE_DEFERRED){
            return "Uitgestelde Betaling - Wordt contact opgehaald";
        }
        if($this->afspraaktype_id == AppointmentType::TYPE_REMINDER){
            return "Uitgestelde Betaling - Wordt per bank voldaan";
        }
        return "";
    }

    /**
     * @description Checks if there is a  new appointment with this as reference and returns it if found
     * @param array $appointmentTypes
     * @return Appointment|bool
     */
    public function hasNewAppointment($appointmentTypes = null): Appointment|bool
    {
        if($appointmentTypes == null){
            return Appointment::whereAfspraakId($this->id)
                ->whereAfspraaktypeId($this->afspraaktype_id)
                ->orderBy('created_at', 'asc')->first()
                ?? false;
        }
        foreach($appointmentTypes as $type){
            if(Appointment::whereAfspraakId($this->id)->whereAfspraaktypeId($type)->exists()){
                return Appointment::whereAfspraakId($this->id)->whereAfspraaktypeId($type)->orderBy('created_at', 'asc')->first()?? false;
            }
        }
        return false;
    }

//
//  public function returnDate($date)
//  {
//    return date('d-M-Y H:i', strtotime($date));
//  }
//
//  public function vijftien()
//  {
//    return $this->hasMany('App\Models\Vijftien');
//  }
//
//  public function aha()
//  {
//    return $this->hasMany('App\Models\Activiteit_has_Afspraak', 'afspraak_id');
//  }
//
//  public function klant()
//  {
//    return $this->belongsTo('App\Models\Client');
//  }
//
//
//  public function wha()
//  {
//    return $this->hasMany('App\Models\Werklijst_has_afspraak');
//  }
//
//  public function verwerktDoor(){
//      return $this->belongsTo('App\Models\User', 'verwerkt_door');
//  }
//
//  public function verwerkt_door()
//  {
//    return User::find($this->verwerkt_door);
//  }
//
//  public function markAsVerzet(){
//      $this->is_verzet = 1;
//      $this->save();
//  }
//
//
//  public function hasAha()
//  {
//
//    if ($this->aha) {
//      return true;
//    }
//
//    return false;
//  }
//
//  public function returnAddedDate($increment)
//  {
//
//    $start = new Datetime(date('Y-m-d', strtotime($this->startdatum)));
//
//    $date = date_add($start, date_interval_create_from_date_string($increment . ' days'));
//
//    return date_format($date, 'Y-m-d');
//  }
//
//  //get datedifference when appointment covers multiple days.
//
//  public function getDateDifference()
//  {
//
//    $start = new Datetime(date('Y-m-d', strtotime($this->startdatum)));
//
//    $eind = new Datetime(date('Y-m-d', strtotime($this->einddatum)));
//
//    $interval = date_diff($start, $eind);
//
//    return $interval;
//  }
//
//  public function getDateDifferenceDateArray()
//  {
//
//    $start = new Datetime(date('Y-m-d', strtotime($this->startdatum)));
//
//    $eind = new Datetime(date('Y-m-d', strtotime($this->einddatum)));
//
//    $interval = date_diff($start, $eind);
//    // dd($start);
//    $dateArray = array();
//
//    for ($i = 1; $i <= $interval->d; $i++) {
//      $tempDate = date('Y-m-d', strtotime("+" . $i . " days", strtotime($start->format('Y-m-d'))));
//      array_push($dateArray, $tempDate);
//    }
//
//    return $dateArray;
//  }
//
//  public static function getDateDifferenceWoApp($start, $end)
//  {
//
//    $start = new Datetime(date('Y-m-d', strtotime($start)));
//
//    $eind = new Datetime(date('Y-m-d', strtotime($end)));
//
//    $interval = date_diff($start, $eind);
//
//    return $interval;
//  }
//
//  // return previous appointment
//
//  public function getReference()
//  {
//
//    return Appointment::find($this->afspraak_id);
//  }
//
//  // return all appointments that has this appointment as reference
//
//  public function getAllReferences()
//  {
//
//    return Appointment::where('afspraak_id', $this->id)->get();
//  }
//
//  //check if appointment has been planned
//
//  public function ingepland($id)
//  {
//
//    if (Werklijst_has_afspraak::where('afspraak_id', $id)->exists()) {
//      return true;
//    } else {
//      return false;
//    }
//  }
//
//  public function changeFault()
//  {
//
//    $this->eigenschuld = ($this->eigenschuld == 1) ? 0 : 1;
//
//    $this->save();
//
//    return true;
//  }
//
//  public function changeSetFault($set)
//  {
//
//    $this->eigenschuld = $set;
//
//    $this->save();
//
//    // dd($this);
//
//    return true;
//  }
//
//  //save appointment part one so only times etc.
//
//  public function saveAppoinmentPartOne($input)
//  {
//
//    // dd($input);
//
//    if ($input['radio_demo'] < 2) {
//
//      $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
//
//      $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
//
//      $startstamp = $strtotime->getTimestamp();
//
//      $endstamp = $strtotimee->getTimestamp();
//
//      if ($input['radio_demo'] == 1) {
//
//        $this->startdatum = date('Y-m-d', $startstamp) . " 12:30";
//
//        $this->einddatum = date('Y-m-d', $startstamp) . " 17:00";
//      } else {
//
//        $this->startdatum = date('Y-m-d', $startstamp) . " 08:00";
//
//        $this->einddatum = date('Y-m-d', $startstamp) . " 12:30";
//      }
//    } else {
//
//      $strtotime = DateTime::createFromFormat("d-m-Y H:i", $input['startdatum']);
//
//      $strtotimee = DateTime::createFromFormat("d-m-Y H:i", $input['einddatum']);
//
//      $startstamp = $strtotime->getTimestamp();
//
//      $endstamp = $strtotimee->getTimestamp();
//
//      $this->startdatum = date('Y-m-d H:i:s', $startstamp);
//
//      $this->einddatum = date('Y-m-d H:i:s', $endstamp);
//    }
//
//    $this->klant_id = $input['klant_id'];
//
//    $this->geschreven_door = (isset($input['afspraak_id'])) ? Auth::user()->werknemer_id : $input['geschreven_door'];
//
//    $this->verwerkt_door = $input['verwerkt_door'];
//
//    $this->afspraaktype_id = $input['afspraaktype_id'];
//
//    $this->omschrijving = $input['omschrijving'];
//
//    (!isset($input['bevestiging'])) ? $this->bevestiging = 1: $this->bevestiging = 0;
//
//    (!isset($input['initiatief'])) ? $this->initiatief = 1: $this->initiatief = 0;
//
//    (!isset($input['bijgewerkt_door'])) ? $this->bijgewerkt_door = 1: $this->bijgewerkt_door = 0;
//
//    $this->totaalbedrag = 0.00;
//
//    if (isset($input['afspraak_id'])) {
//
//      $this->afspraak_id = $input['afspraak_id'];
//
//      $oude_afspraak = Appointment::find($input['afspraak_id']);
//
//      $oude_afspraak->is_verzet = 1;
//
//      $oude_afspraak->save();
//    }
//
//    $this->save();
//
//    return $this;
//  }
//
//  //define the next step in the creating process
//
//  public function defineNextCreateStep($input)
//  {
//
//    switch ($this->afspraaktype_id) {
//
//      case 1:
//
//        // $uw = new UitvoerenWerkzaamheden($this);
//
//        return 1;
//
//        break;
//
//      case 2:
//
//        $oo = new OfferteOpstellen($this);
//
//        (isset($input['voorrijkosten'])) ? $voorrijkosten = 1 : $voorrijkosten = 0;
//
//        return $oo->nextCreateStep($voorrijkosten);
//
//        break;
//
//      case 3:
//
//        $ub = new UitgesteldeBetaling($this);
//
//        return $ub->nextCreateStep();
//
//        break;
//
//      case 4:
//
//        $ga = new Garantie($this);
//
//        return $ga->nextCreateStep();
//
//        break;
//
//      case 5:
//
//        $bh = new Betalingsherinnering($this); // nog bouwen.
//
//        break;
//
//      case 6:
//
//        $tbv = new Terugbetaalverzoek($this);
//
//        return $tbv->nextCreateStep();
//
//        break;
//
//      default:
//
//        break;
//    }
//
//    return $this->afspraaktype_id;
//  }
//
//  // defining the last step in the creating process
//
//  public function defineLastCreateStep($input)
//  {
//
//    switch ($this->afspraaktype_id) {
//
//      case 1:
//
//        $uw = new UitvoerenWerkzaamheden($this);
//
//        return $uw->lastCreateStep($input);
//
//        break;
//
//      case 2:
//
//        $oo = new OfferteOpstellen($this);
//
//        break;
//
//      case 3:
//
//        $ub = new UitgesteldeBetaling($this);
//
//        break;
//
//      case 4:
//
//        $ga = new Garantie($this);
//
//        return $ga->defineLastCreateStep($input);
//
//        break;
//
//      case 5:
//
//        $bh = new Betalingsherinnering($this); // nog bouwen.
//
//        break;
//
//      case 6:
//
//        $tbv = new Terugbetaalverzoek($this);
//
//        break;
//
//      default:
//
//        # code...
//
//        break;
//    }
//  }
//
//  // return all appointments from this customer with their status
//
//  public function getAllAppointmentsWithStatus($bool)
//  {
//
//    $alleafspraken = Status_has_appointment::returnAllAppointments($this->klant_id, $bool);
//
//    return $alleafspraken;
//  }
//
//  // return all appointments from this customer with their activities
//
//  public function getAllAppointmentsWithActivity($bool)
//  {
//
//    $actperafs = Activiteit_has_Afspraak::getAllAppointmentsWithActivity($this->klant_id, $bool);
//
//    return $actperafs;
//  }
//
//  // set total for this appointment
//
//  public function setTotal($amount)
//  {
//
//    $this->totaalbedrag = $amount;
//
//    $this->save();
//  }
//
//  // set the var 'klusprijs' from this appointment
//
//  public function setKlusprijs()
//  {
//
//    $this->klusprijs = 1;
//
//    $this->save();
//  }
//
//  // set historyString for appointment
//
//  public function setActivityHistory()
//  {
//
//    $ahas = Activiteit_has_Afspraak::where('afspraak_id', $this->id)->get();
//
//    $geschiedenisString = date('d-m-Y', strtotime($this->startdatum)) . ": Werkzaamheden toegevoegd/gewijzigd op de afspraak, <br>";
//
//    $totaalbedrag = 0.00;
//
//    foreach ($ahas as $aha) {
//
//      $geschiedenisString = $geschiedenisString . ", " . $aha->aantal . " " . Activiteit::find($aha->activiteit_id)->omschrijving . " € " . $aha->prijs;
//
//      $totaalbedrag += $aha->aantal * $aha->prijs - $aha->kortingsbedrag;
//    }
//
//    $klusprijs = '';
//
//    if ($this->klusprijs == 1) {
//
//      $klusprijs = '<b> Klusprijs € ' . number_format($this->totaalbedrag, 2) . '</b>';
//    }
//
//    $geschiedenisString = $geschiedenisString . ",Totaal: € " . number_format($totaalbedrag, 2) . $klusprijs;
//
//    Geschiedenis::setHistory($this->klant_id, $this->id, Auth::user()->id, $geschiedenisString, 'Afspraak');
//  }
//
//  public function setReferenceToNull()
//  {
//
//    $afspraken = Appointment::where('afspraak_id', $this->id)->get();
//
//    foreach ($afspraken as $a) {
//
//      $a->afspraak_id = null;
//
//      $a->save();
//    }
//  }
//
//  public function setProcessOne($input)
//  {
//
//    $gelukt = $input['gelukt'];
//
//    $extra = (isset($input['extra'])) ? $input['extra'] : 0;
//
//    $voldaan = (isset($input['voldaan'])) ? $input['voldaan'] : 0;
//
//    $bank = $input['bank'];
//
//    if (isset($input['betalingsherinnering'])) {
//      $betalingsherinnering = $input['betalingsherinnering'];
//    }
//
//    if (isset($input['advies'])) {
//      $advies = $input['advies'];
//    }
//
//    switch ($this->afspraaktype_id) {
//
//      case 1: // Uitvoeren Werkzaamheden
//
//        $uw = new UitvoerenWerkzaamheden($this);
//
//        $sha_id = (isset($input['sha_id'])) ? $input['sha_id'] : null;
//
//        return $uw->setProcessOne($gelukt, $extra, $voldaan, $bank, $advies, $sha_id);
//
//        break;
//
//      case 2: // Offerte Opstellen
//
//        $oo = new OfferteOpstellen($this);
//
//        return $oo->setProcessOne($gelukt, $extra, $voldaan, $bank, $advies);
//
//        break;
//
//      case 3: //Uitgestelde Betaling
//
//        $ub = new UitgesteldeBetaling($this);
//
//        return $ub->setProcessOne($gelukt, $extra, $voldaan, $bank, $advies);
//
//        break;
//
//      case 4: //Garantie
//
//        $ga = new Garantie($this);
//
//        return $ga->setProcessOne($gelukt, $extra, $voldaan, $bank, $advies);
//
//        break;
//
//      case 5: //Betalingsherinnering
//
//        $bh = new Betalingsherinnering($this);
//
//        $voldaan = ($gelukt == 1) ? 1 : 0;
//
//        return $bh->setProcessOne($gelukt, $extra, $voldaan, $bank, $betalingsherinnering);
//
//        break;
//
//      case 6: // Terugbetaalverzoek
//
//        $tb = new Terugbetaalverzoek($this);
//
//        return $tb->setProcessOne($gelukt, $extra, $voldaan, $bank, $betalingsherinnering);
//
//        break;
//
//      default:
//
//        # code...
//
//        break;
//    }
//  }
//
//  // sent within old appointment
//
//  public function setProcessTwo($input, $new_appointment)
//  {
//
//    //if new appointment is null it means there is no new appointment or there has yet to be a new appointment.
//
//    // echo 'new '.($new_appointment == null) ? 'null' : 'iets anders';
//
//    $afspraaktype = ($new_appointment == null) ? $this->afspraaktype_id : ((is_numeric($new_appointment)) ? null : $new_appointment->afspraaktype_id);
//
//    // echo '<br>'.'afsprtype '.$afspraaktype.'<br>';
//
//    // dd($input);
//
//    switch ($afspraaktype) {
//
//      case 1: // Uitvoeren Werkzaamheden
//
//        $uw = new UitvoerenWerkzaamheden($this);
//
//        if ($input['extra'] == 1) {
//
//          return $uw->setProcessTwo($input, true);
//        } else {
//
//          return $uw->setProcessTwo($input, false);
//        }
//
//        break;
//
//      case 2: // Offerte Opstellen
//
//        $oo = new OfferteOpstellen($this);
//
//        return $oo->setProcessTwo($input);
//
//        break;
//
//      case 3: //Uitgestelde Betaling
//
//        $ub = new UitgesteldeBetaling($this); // $this can be the old appointment (when a 'normal' appointment hasnt been paid.)
//
//        $bool = true;
//
//        // if($new_appointment != null){
//
//        //   if($new_appointment->afspraaktype_id == $this->afspraaktype_id){
//
//        //     $bool = false; // if bool = false, the original appointment is also a deferred payment and its referring to adding extra work first
//
//        //   }
//
//        // }
//
//        return $ub->setProcessTwo($input, $new_appointment, $bool);
//
//        break;
//
//      case 4: //Garantie
//
//        $ga = new Garantie($this);
//
//        if ($input['extra'] == 1) {
//
//          return $ga->setProcessTwo($input, true);
//        } else {
//
//          return $ga->setProcessTwo($input, false);
//        }
//
//        break;
//
//      case 5: //Betalingsherinnering
//
//        $bh = new Betalingsherinnering($this);
//
//        $bank = $new_appointment;
//
//        return $bh->setProcessTwo($input, $bank, true);
//
//        break;
//
//      case 6: // Terugbetaalverzoek
//
//        $tb = new Terugbetaalverzoek($this);
//
//        return $tb->setProcessOne($gelukt, $extra, $voldaan, $bank, $betalingsherinnering, $step);
//
//        break;
//
//      default:
//
//        # code...
//
//        break;
//    }
//  }
//
//  public function setProcessThree($input)
//  {
//
//    switch ($this->afspraaktype_id) {
//
//      case 1:
//
//        # code...
//
//        break;
//
//      case 2:
//
//        # code...
//
//        break;
//
//      case 3:
//
//        break;
//
//      case 4:
//
//        $ga = new Garantie($this);
//
//        return $ga->setProcessThree();
//
//        break;
//
//      case 5:
//
//        # code...
//
//        break;
//
//      case 6:
//
//        # code...
//
//        break;
//
//      default:
//
//        # code...
//
//        break;
//    }
//  }
//
//  public function setProcessFour($input)
//  {
//
//    switch ($this->afspraaktype_id) {
//
//      case 1:
//
//        # code...
//
//        break;
//
//      case 2:
//
//        # code...
//
//        break;
//
//      case 3:
//
//        $ga = new Garantie($this);
//
//        return $ga->setProcessFour($input);
//
//        break;
//
//      case 4:
//
//        # code...
//
//        break;
//
//      case 5:
//
//        # code...
//
//        break;
//
//      case 6:
//
//        # code...
//
//        break;
//
//      default:
//
//        # code...
//
//        break;
//    }
//  }
//  // method for returning if amount has been paid
//  public function getAmountOpen($id)
//  {
//    $amount = 0.00;
//
//    $nieuweafspraken = Appointment::where('afspraak_id', $id)->where('afspraaktype_id', 3)->get();
//
//    if ($nieuweafspraken->isEmpty()) {
//
//      return -1;
//    }
//
//    foreach ($nieuweafspraken as $nas) {
//
//      $amount += $nas->totaalbedrag;
//    }
//
//    if ($amount == Appointment::find($id)->niet_betaald) {
//      return 1;
//    } else {
//      return 0;
//    }
//  }
//
//  public function hasNewAppointment($id)
//  {
//
//    if (Appointment::where('afspraak_id', $id)->exists()) {
//      return true;
//    } else {
//      return false;
//    }
//  }
//
//  public function hasNewDefPayments()
//  {
//
//    $afspraken = Appointment::where('afspraak_id', $this->id)->get();
//
//    foreach ($afspraken as $a) {
//
//      if ($a->afspraaktype_id == 3 || $a->afspraaktype_id == 5) {
//
//        return true;
//      }
//    }
//
//    return false;
//  }
//
//  public function deleteNewDefPayments()
//  {
//
//    $afspraken = Appointment::where('afspraak_id', $this->id)->get();
//
//    foreach ($afspraken as $a) {
//
//      if ($a->afspraaktype_id == 3 || $a->afspraaktype_id == 5) {
//
//        $a->deleteSelf();
//      }
//    }
//  }
//
//  public function deleteSelf()
//  {
//
//    Geschiedenis::setHistory($this->klant_id, $this->id, Auth::user()->id, Auth::user()->name . " heeft de afspraak van " . $this->returnDate($this->startdatum) . " --- " . $this->returnDate($this->einddatum) . " verwijderd.", null);
//
//    Log::createLog(Auth::user()->id, $this->id, "Afspraak", Auth::user()->name . " heeft een afspraak op: " . $this->startdatum . " -- " . $this->einddatum . ", verwijderd.");
//
//    Activiteit_has_Afspraak::deleteSelf($this->id);
//
//    $this->setReferenceToNull();
//
//    Werklijst_has_afspraak::deleteAppFromWorklist($this->id);
//
//    $this->delete();
//  }
//
//  public function getTotalFromActivity()
//  {
//
//    return Activiteit_has_Afspraak::getTotal($this->id);
//  }
//
//  public function getAmountOfDefPayments()
//  {
//
//    return Status_has_appointment::with('afspraak', 'status')->where('status_id', 1)->whereHas('afspraak', function ($afs) {
//
//      $afs->where('afspraaktype_id', 3)->orWhere('afspraaktype_id', 5);
//    })->count();
//  }
//
//  public function getReasonFromReference()
//  {
//
//    if (Appointment::where('afspraak_id', $this->afspraak_id)->exists()) {
//
//      if (Appointment::where('afspraak_id', $this->afspraak_id)->first()->reden != '') {
//
//        $ta = Appointment::where('afspraak_id', $this->afspraak_id)->first()->reden;
//
//        return date('d-m-Y', strtotime($ta->startdatum)) . ': ' . $ta->reden;
//      }
//    }
//  }
//
//  public function getReden($id)
//  {
//
//    $redenen = '';
//
//    $bool = true;
//
//    $afspraak = Appointment::find($id);
//
//    if ($afspraak->afspraak_id != null) { // we know this has a reference.
//
//      if (Appointment::where('id', $afspraak->afspraak_id)->exists()) { // if appointment still exists and is not deleted
//
//        $oude_afspraak = Appointment::find($afspraak->afspraak_id); // get old appointment
//
//        $redenen .= date('d-m-Y', strtotime($oude_afspraak->startdatum)) . ': ' . $oude_afspraak->reden . '<br>'; // add old reason to appointment
//
//        if ($oude_afspraak->afspraak_id != null) { // there is another previous appointment.
//
//          while ($bool == true) {
//
//            if ($oude_afspraak->afspraak_id == null) {
//
//              $bool = false;
//            } else {
//
//              if (Appointment::where('id', $oude_afspraak->afspraak_id)->exists()) {
//
//                $oude_afspraak = Appointment::find($oude_afspraak->afspraak_id); // get previous reference of previous appointment
//
//                $redenen .= date('d-m-Y', strtotime($oude_afspraak->startdatum)) . ': ' . $oude_afspraak->reden . '<br>'; // add old reason to appointment
//
//              } else {
//
//                $bool = false;
//              }
//            }
//          }
//        }
//      }
//    }
//
//    return $redenen;
//  }
//
//
//  public static function getStaticReden($id)
//  {
//
//    $redenen = '';
//
//    $bool = true;
//
//    $afspraak = Appointment::find($id);
//
//    if ($afspraak->afspraak_id != null) { // we know this has a reference.
//
//      if (Appointment::where('id', $afspraak->afspraak_id)->exists()) { // if appointment still exists and is not deleted
//
//        $oude_afspraak = Appointment::find($afspraak->afspraak_id); // get old appointment
//
//        $redenen .= date('d-m-Y', strtotime($oude_afspraak->startdatum)) . ': ' . $oude_afspraak->reden . '<br>'; // add old reason to appointment
//
//        if ($oude_afspraak->afspraak_id != null) { // there is another previous appointment.
//
//          while ($bool == true) {
//
//            if ($oude_afspraak->afspraak_id == null) {
//
//              $bool = false;
//            } else {
//
//              if (Appointment::where('id', $oude_afspraak->afspraak_id)->exists()) {
//
//                $oude_afspraak = Appointment::find($oude_afspraak->afspraak_id); // get previous reference of previous appointment
//
//                $redenen .= date('d-m-Y', strtotime($oude_afspraak->startdatum)) . ': ' . $oude_afspraak->reden . '<br>'; // add old reason to appointment
//
//              } else {
//
//                $bool = false;
//              }
//            }
//          }
//        }
//      }
//    }
//
//    return $redenen;
//  }
//
//  public function sameActivities($old_appointment)
//  {
//
//    $oldApp = Appointment::find($old_appointment);
//
//    Activiteit_has_Afspraak::copyActivities($oldApp->id, $this->id);
//
//    $this->totaalbedrag = $oldApp->totaalbedrag;
//
//    $this->klusprijs = $oldApp->klusprijs;
//
//    $this->save();
//
//    return true;
//  }
//
//  //below is copy of what was.
//  // method for returning value of current set of appointments
//  public function getAmount($id)
//  {
//    $amount = 0.00;
//
//    $afspraken = Appointment::where('afspraak_id', $id)->get();
//
//    foreach ($afspraken as $a) {
//
//      if (Status_has_appointment::where('afspraak_id', $a->id)->exists()) {
//
//        if (Status_has_appointment::where('afspraak_id', $a->id)->first()->status_id == 4) {
//
//          $amount += $a->totaalbedrag;
//        }
//      }
//    }
//
//    if ($amount == Appointment::find($id)->niet_betaald) {
//      return 0;
//    } else {
//      return $amount;
//    }
//  }
//
//  public function getStatus()
//  {
//
//    $status = Status::find(Status_has_appointment::where('afspraak_id', $this->id)->first()->status_id);
//
//    return $status;
//  }
//
//  public function getType()
//  {
//
//    $type = AppointmentType::find($this->afspraaktype_id);
//
//    return $type;
//  }
//
//  public function getHistory()
//  {
//
//    return Geschiedenis::where('afspraak_id', $this->id)->get();
//  }
//
//  public function getReminders($klant_id)
//  {
//
//    $alleafspraken = Status_has_appointment::with('afspraak', 'status')->whereHas('afspraak', function ($afd) use ($klant_id) {
//
//      $afd->where('klant_id', $klant_id)->where('afspraaktype_id', 5);
//    })->get();
//
//    // dd($alleafspraken);
//
//    foreach ($alleafspraken as $t) {
//
//      $t->afspraak->afspraaktype = AppointmentType::find($t->afspraak->afspraaktype_id)->omschrijving;
//    }
//
//    $alleafspraken = $alleafspraken->sortByDesc(function ($afspraak) {
//
//      return $afspraak->afspraak->startdatum;
//    });
//
//    return $alleafspraken;
//  }
//
//  public static function getStaticReminders($klant_id)
//  {
//
//    $alleafspraken = Status_has_appointment::with('afspraak', 'status')->whereHas('afspraak', function ($afd) use ($klant_id) {
//
//      $afd->where('klant_id', $klant_id)->where('afspraaktype_id', 5);
//    })->get();
//
//    // dd($alleafspraken);
//
//    foreach ($alleafspraken as $t) {
//
//      $t->afspraak->afspraaktype = AppointmentType::find($t->afspraak->afspraaktype_id)->omschrijving;
//    }
//
//    $alleafspraken = $alleafspraken->sortByDesc(function ($afspraak) {
//
//      return $afspraak->afspraak->startdatum;
//    });
//
//    return $alleafspraken;
//  }
//
//  public function getRealAppointmentPrice()
//  {
//    // if 'klusprijs' has been set, get the discount
//
//    if ($this->offerte_id != null) {
//
//      return Offerte::find($this->offerte_id)->totaalbedrag;
//    } else {
//
//      $ahas = $this->aha;
//
//      $total = 0.00;
//      foreach ($ahas as $aha) {
//        $total += ($aha->aantal * $aha->activiteit->prijs);
//      }
//      return $total;
//    }
//  }
//
//  public function getPreviousApp()
//  {
//
//    return Appointment::find($this->afspraak_id);
//  }
//
//  public function changeBevestiging($id)
//  {
//
//    $a = Appointment::find($id);
//
//    $a->bevestiging = 2;
//
//    $a->save();
//  }
//
//  public function getHistoryFromThisAppointment()
//  {
//
//    return Afspraak_has_geschiedenis::getAllFromAppointment($this->id);
//  }
}
