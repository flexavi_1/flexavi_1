<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentType extends Model
{
    //
    const TYPE_EXECUTE = 1;
    const TYPE_QUOTE = 2;
    const TYPE_DEFERRED = 3;
    const TYPE_WARRANTY = 4;
    const TYPE_REMINDER = 5;
    const TYPE_CREDIT = 6;
    protected $guarded = ['id'];
    public $table = "afspraaktype";
    public $timestamps = false;

    public function afspraak()
    {
    	return $this->hasMany('App\Models\Appointment');
    }

    public static function getDescription($id){
        if(AppointmentType::where('id', $id)->exists())
           	return AppointmentType::find($id)->omschrijving;
        return "ERROR:ATYPENULL";
    }

}
