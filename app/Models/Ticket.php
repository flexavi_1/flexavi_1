<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;
use App\Models\Status;

class Ticket extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "ticket";

    public function toegevoegd_door()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function bijgewerkt_door()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function ticketregels()
    {
        return $this->hasMany('App\Models\Ticketregels');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    public function created_at()
    {
        return date('dd-mm-YYYY H:i', strtotime($this->created_at));
    }

    public function toegevoegd()
    {
        return User::find($this->toegevoegd_door)->name;
    }

    public function bijgewerkt(){
        if($this->bijgewerkt_door != null && $this->bijgewerkt_door != '' && $this->bijgewerkt_door > 0){
            return User::find($this->bijgewerkt_door)->name;
        } else {
            return '';
        }
    }

    public function getStatus(){
        return Status::find($this->status_id)->omschrijving;
    }

    public function updated_at()
    {
        return date('dd-mm-YYYY H:i', strtotime($this->updated_at));
    }

}
