<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use App\Models\Appointment;
use App\Models\Worklist_has_appointments;
use App\Models\Worklist;

/**
 * @property int $klant_id
 * @property string $type
 * @property int $afspraak_id
 * @property int $gebruiker_id
 * @property string $omschrijving
 * @property datetime $datum
 */
class History extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "geschiedenis";

    // public function klanttype()
    // {
    // 	return $this->hasMany('App\Models\Klanttype');
    // }

     public function createdBy()
     {
     	return $this->belongsTo(User::class, 'gebruiker_id');
     }

    // public function geschrevenDoor()
    // {
    //     return $this->belongsTo('App\Models\Werknemer');
    // }

    public function client(){
        return $this->belongsTo(Client::class, 'klant_id', );
    }

    public function appointment(){
        return $this->belongsTo(Appointment::class, 'afspraak_id', 'id');
    }

    public function returnDate($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public static function getSalesman($afspraak_id){
        if($afspraak_id != null){
            $afspraak = Appointment::find($afspraak_id);
            // dd($afspraak);
            if($afspraak){
                if(Worklist_has_appointments::where('afspraak_id', $afspraak->id)->exists()){
                    $wha = Worklist_has_appointments::where('afspraak_id', $afspraak->id)->first();
                    $werklijst = Worklist::find($wha->werklijst_id);
                    $verkoper = "";
                    $veger = "";
                    if($werklijst->veger != null && $werklijst->veger != 0){
                        $veger = Employee::find($werklijst->veger)->voornaam;
                    }
                    if($werklijst->verkoper != null && $werklijst->verkoper != 0){
                        $verkoper = Employee::find($werklijst->verkoper)->voornaam;
                    }
                    return "Klant van <b><i>".$verkoper." & ".$veger."</i></b>. ";
                } else {
                    return "Afspraak is niet ingedeeld";
                }
            }
            return "Afspraak is verwijderd.";
        }
    }

    public static function setHistory($klant_id, $afspraak_id, $gebruiker_id, $omschrijving, $type){
        // if(Geschiedenis::where('afspraak_id', $afspraak_id)->where('klant_id', $klant_id)->exists()){
        //     $geschiedenis = Geschiedenis::where('afspraak_id', $afspraak_id)->where('klant_id', $klant_id)->first();
        //     $geschiedenis->omschrijving = $geschiedenis->omschrijving.'<br>'.$omschrijving;
        // } else {
        $geschiedenis = new History;
        $geschiedenis->omschrijving = $omschrijving;
        // }
        $geschiedenis->datum = date('Y-m-d');
        $geschiedenis->klant_id = $klant_id;
        $geschiedenis->afspraak_id = $afspraak_id;
        $geschiedenis->gebruiker_id = $gebruiker_id;
        $geschiedenis->type = $type;
        $geschiedenis->save();

        return $geschiedenis;
    }

    //get history for attaching to appointment
    public static function getHistoryWithoutAppointment($klant_id, $afspraak_id){
        return History::where('klant_id', $klant_id)->where('afspraak_id', '!=', $afspraak_id)->get();
    }
}
