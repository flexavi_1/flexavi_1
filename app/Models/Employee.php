<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $voornaam
 * @property mixed $achternaam
 */
class Employee extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "werknemer";
    public $timestamps = false;

    public function klant()
    {
    	return $this->hasMany('App\Models\Client');
    }

    public function user()
    {
    	return $this->hasOne('App\Models\User');
    }

    public function uur()
    {
        return $this->hasMany('App\Models\Uur');
    }

    public function wha()
    {
        return $this->hasMany('App\Models\Werknemer_has_auto');
    }

    public function getFullName(){
        return $this->voornaam." ".$this->achternaam;
    }

    public static function findMe($id){
        $wn = Employee::find($id);
        if($wn == null)
            return null;
        return $wn;
    }
}
