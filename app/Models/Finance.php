<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Finance extends Model
{
    //function for retrieving revenue with set startdate
    public function getRevenue($startdate)
    {
        //retrieve all appointments within the startdate
        $afspraken = Appointment::with('klant')->where('startdatum', '>=', $startdate)->where('startdatum', '<', date('Y-m-d', strtotime("+1 day", strtotime($startdate))))->where('afspraaktype_id', 1)->get();
        $ghs = Appointment::with('klant')->where('startdatum', '>=', $startdate)->where('startdatum', '<', date('Y-m-d', strtotime("+1 day", strtotime($startdate))))->where('afspraaktype_id', 3)->get();
        //declaring vars
        $omzet = 0.00;
        $nb = 0.00;
        $gh = 0.00;
        //loop for getting the amount
        foreach ($afspraken as $a)
        {
            if(Status_has_appointment::where('afspraak_id', $a->id)->first()->status_id == 3 || Status_has_appointment::where('afspraak_id', $a->id)->first()->status_id == 4 ){
                $omzet += $a->totaalbedrag;
                $nb += $a->niet_betaald;
            }
        }
        // setting array
        $array = array();
        array_push($array, $omzet);
        array_push($array, $nb);
        return $array;
    }

    //get costs on the startdate and return them per division
    public function getCosts($startdate)
    {
        $crediteuren = Crediteur::where('updated_at','>=', $startdate)->where('updated_at', '<', date('Y-m-d', strtotime("+1 day", strtotime($startdate))))->where('terugbetaald', 1)->get();
        $creditering = 0.00;
        foreach ($crediteuren as $crediteur)
        {
            $creditering += $crediteur->bedrag;
        }

        $inkopen = Inkoop::where('datum', $startdate)->where('voldaan', 1)->get();
        $inkoop = 0.00;
        foreach ($inkopen as $i)
        {
            $inkoop += $i->totaalbedrag;
        }

        $kantoorkosten = Kantoorkosten::where('datum', $startdate)->get();
        $kantoor = 0.00;
        foreach ($kantoorkosten as $k)
        {
            $kantoor += $k->betaald;
        }

        $overigekosten = Overigekosten::where('datum', $startdate)->get();
        $overig = 0.00;
        foreach ($overigekosten as $o)
        {
            $overig += $o->kosten;
        }

        $wervingskosten = Wervingskosten::where('datum', $startdate)->get();
        $werving = 0.00;
        foreach ($wervingskosten as $w)
        {
            $werving += $w->ubloon;
            $werving += $w->benzine;
        }

        $werkdag = Werkdag::where('datum', $startdate)->first();
        if($werkdag == null)
        {
            $arr = array();
            $arr = (object) $arr;
            $arr->error = true;
            return $arr;
        }
        $werklijsten = Worklist::where('werkdag_id', $werkdag->id)->get();
        $werkdagkosten = 0.00;
        foreach ($werklijsten as $wl)
        {
            $werkdagkosten += ($wl->totaalloonVerk - $wl->nbloonVerk);
            $werkdagkosten += ($wl->totaalloonVeg - $wl->nbloonVeg);
            $werkdagkosten += $wl->benzine;
            $werkdagkosten += $wl->eten;
            $werkdagkosten += $wl->telefoonkosten;
            $werkdagkosten += $wl->extra;
            $werkdagkosten += $wl->inkoop;
        }

        $totalekosten = $creditering + $inkoop + $kantoor + $overig + $werving;
        $arr = array();
        $arr = (object) $arr;
        // $arr->kosten = $totalekosten;
        $arr->creditering = $creditering;
        $arr->inkoop = $inkoop;
        $arr->kantoor = $kantoor;
        $arr->overig = $overig;
        $arr->werving = $werving;
        $arr->verkoop = $werkdagkosten;
        $arr->totaal = $arr->creditering + $arr->inkoop + $arr->kantoor + $arr->overig + $arr->werving + $arr->verkoop;

        return $arr;
    }

    //get profit on startdate and returns profit
    public function getProfit($startdate)
    {
        $crediteuren = Crediteur::where('updated_at','>=', $startdate)->where('updated_at', '<', date('Y-m-d', strtotime("+1 day", strtotime($startdate))))->where('terugbetaald', 1)->get();
        $creditering = 0.00;
        foreach ($crediteuren as $crediteur)
        {
            $creditering += $crediteur->bedrag;
        }

        $inkopen = Inkoop::where('datum', $startdate)->where('voldaan', 1)->get();
        $inkoop = 0.00;
        foreach ($inkopen as $i)
        {
            $inkoop += $i->totaalbedrag;
        }

        $kantoorkosten = Kantoorkosten::where('datum', $startdate)->get();
        $kantoor = 0.00;
        foreach ($kantoorkosten as $k)
        {
            $kantoor += $k->betaald;
        }

        $overigekosten = Overigekosten::where('datum', $startdate)->get();
        $overig = 0.00;
        foreach ($overigekosten as $o)
        {
            $overig += $o->kosten;
        }

        $wervingskosten = Wervingskosten::where('datum', $startdate)->get();
        $werving = 0.00;
        foreach ($wervingskosten as $w)
        {
            $werving += $w->ubloon;
            $werving += $w->benzine;
        }

        $werkdag = Werkdag::where('datum', $startdate)->first();
        $werklijsten = Worklist::where('werkdag_id', $werkdag->id)->get();
        $werkdagkosten = 0.00;
        foreach ($werklijsten as $wl)
        {
            $werkdagkosten += ($wl->totaalloonVerk - $wl->nbloonVerk);
            $werkdagkosten += ($wl->totaalloonVeg - $wl->nbloonVeg);
            $werkdagkosten += $wl->benzine;
            $werkdagkosten += $wl->eten;
            $werkdagkosten += $wl->telefoonkosten;
            $werkdagkosten += $wl->extra;
            $werkdagkosten += $wl->inkoop;
        }

        $totalekosten = $creditering + $inkoop + $kantoor + $overig + $werving + $werkdagkosten;

        //retrieve all appointments within the startdate
        $afspraken = Appointment::with('klant')->where('startdatum', '>=', $startdate)->where('startdatum', '<', date('Y-m-d', strtotime("+1 day", strtotime($startdate))))->where('afspraaktype_id', 1)->get();
        //declaring vars
        $omzet = 0.00;
        $nb = 0.00;
        //loop for getting the amount
        foreach ($afspraken as $a)
        {
            if(Status_has_appointment::where('afspraak_id', $a->id)->first()->status_id == 3 || Status_has_appointment::where('afspraak_id', $a->id)->first()->status_id == 4 ){
                $omzet += $a->totaalbedrag;
                $nb += $a->niet_betaald;
            }
        }

        return $omzet - $nb - $totalekosten;
    }

    //get Salescosts. Input startdate and aggregate (boolean)
    public function getSales($startdate, $aggregate)
    {
        if($aggregate)
        {
            $werkdag = Werkdag::where('datum', $startdate)->first();
            $werklijsten = Worklist::where('werkdag_id', $werkdag->id)->get();
            $werkdagkosten = 0.00;
            foreach ($werklijsten as $wl)
            {
                $werkdagkosten += ($wl->totaalloonVerk - $wl->nbloonVerk);
                $werkdagkosten += ($wl->totaalloonVeg - $wl->nbloonVeg);
                $werkdagkosten += $wl->benzine;
                $werkdagkosten += $wl->eten;
                $werkdagkosten += $wl->telefoonkosten;
                $werkdagkosten += $wl->extra;
                $werkdagkosten += $wl->inkoop;
            }

            return $werkdagkosten;
        }
        else
        {
            $werkdag = Werkdag::where('datum', $startdate)->first();
            $werklijsten = Worklist::with('verkoper', 'veger')->where('werkdag_id', $werkdag->id)->get();
            foreach ($werklijsten as $wl)
            {
                $wl->wha = Worklist_has_appointments::where('werklijst_id', $wl->id)->get();
                foreach ($wl->wha as $wha)
                {
                    $wha->afspraak_id = Appointment::find($wha->afspraak_id);
                    $wha->afspraak_id->klant_id = Client::find($wha->afspraak_id->klant_id);
                    $wha->afspraak_id->status = Status_has_appointment::where('afspraak_id', $wha->afspraak_id->id)->first();
                    $wha->afspraak_id->status->status_id = Status::find( $wha->afspraak_id->status->status_id );
                }
            }
            $werkdagkosten = $werklijsten;

            return $werkdagkosten;
        }
    }

    // get acquisition costs. Input startdate and aggregate(boolean)
    public function getAcq($startdate, $aggregate)
    {
        if($aggregate)
        {
            $werving = Wervingskosten::with('werknemer_id')->where('datum', date('Y-m-d', strtotime($startdate)))->get();
            $costs = 0.00;
            $delta = 0.00;
            $expected = 0.00;
            foreach($werving as $w){
                $costs += ($w->totaalloon + $w->benzine + $w->benzinenb);
                $delta += $w->verschil;
                $expected += ($costs - $delta);
            }

            $arr = array();
            $arr = (object) $arr;
            $arr->wervingskosten = $costs;
            $arr->wervingsdelta = $delta;
            $arr->wervingsverwachting = $expected;
            return $arr;
        }
        else
        {
            $werving = Wervingskosten::where('datum', date('Y-m-d', strtotime($startdate)))->get();
            foreach($werving as $w){
                $w->werknemer_id = Employee::find($w->werknemer_id);
            }

            $klanten = DB::select( DB::raw( "select w.id as 'werknemer', count(k.id) as 'aantal'
                                         from klant as k
                                         join werknemer as w
                                         on w.id = k.geschreven_door
                                         where k.geschreven_op = '".date('Y-m-d', strtotime($startdate))."'
                                         group by w.id" ));
            // dd($klanten);
            foreach($klanten as $k){
                foreach($werving as $w){
                    if($k->werknemer == $w->werknemer_id->id){
                        $w->aantal = $k->aantal;
                        // $fhw = Functie_has_Werknemer::where('werknemer_id', $w->)
                        $w->dagloon = Functie_has_Werknemer::where('werknemer_id', $w->werknemer_id->id)->first()->dagloon;
                        $w->uurloon = Functie_has_Werknemer::where('werknemer_id', $w->werknemer_id->id)->first()->uurloon;
                        $w->provisiegrens = Functie_has_Werknemer::where('werknemer_id', $w->werknemer_id->id)->first()->provisiegrens;
                        $w->provisiebedrag = Functie_has_Werknemer::where('werknemer_id', $w->werknemer_id->id)->first()->provisiebedrag;
                        $w->provisieperc = Functie_has_Werknemer::where('werknemer_id', $w->werknemer_id->id)->first()->provisieperc;
                    }
                }
            }

            return $werving;
        }
    }

    public function getAdmin($startdate, $aggregate)
    {
        $kantoorkosten = Kantoorkosten::with('werknemer_id')->where('datum', date('Y-m-d', strtotime($startdate)))->get();
        if($aggregate)
        {
            $costs = 0.00;
            $delta = 0.00;
            $expected = 0.00;
            foreach($kantoorkosten as $k){
                $expected += ($k->aantal_uur * $k->uurtarief);
                $delta += $expected - $k->betaald;
                $costs += $k->betaald;
            }

            $arr = array();
            $arr = (object) $arr;
            $arr->kantoorkosten = $costs;
            $arr->kantoordelta = $delta;
            $arr->kantoorverwachting = $expected;
            return $arr;
        }
        else
        {
            foreach($kantoorkosten as $k){
                $k->werknemer_id = Employee::find($k->werknemer_id);
            }
            return $kantoorkosten;
        }
    }

    public function getPurchase($startdate, $aggregate)
    {
        $inkoop = Inkoop::with('klant_id')->where('datum', date('Y-m-d', strtotime($startdate)))->get();
        if($aggregate)
        {
            $inkoopkosten = 0.00;
            foreach ($inkoop as $i) {
                $inkoopkosten += $i->totaalbedrag;
            }
            return $inkoopkosten;
        }
        else
        {
            foreach ($inkoop as $i) {
                $i->klant_id = Client::find($i->klant_id);
            }
            return $inkoop;
        }
    }

    public function getRemainder($startdate, $aggregate)
    {
        $overig = Overigekosten::where('datum', date('Y-m-d', strtotime($startdate)))->get();
        if($aggregate)
        {
            $overigekosten = 0.00;
            foreach ($overig as $o) {
                $overigekosten += $o->kosten;
            }
            return $overigekosten;
        }
        else
        {
            return $overig;
        }
    }

    //function for retrieving revenue with set start- and enddate, and aggregate(boolean)
    public function getRevenueTwo($startdate, $enddate)
    {
        //retrieve all appointments within the startdate
        $afspraken = Appointment::with('klant')->where('startdatum', '>=', $startdate)->where('einddatum', '<', date('Y-m-d', strtotime("+1 day", strtotime($enddate))))->where('afspraaktype_id', 1)->get();
        //declaring vars
        $omzet = 0.00;
        $nb = 0.00;
        //loop for getting the amount
        foreach ($afspraken as $a)
        {
            $status = Status_has_appointment::where('afspraak_id', $a->id)->first();
            if($status != null){
                if($status->status_id == 3 || $status->status_id == 4 ){
                    $omzet += $a->totaalbedrag;
                    $nb += $a->niet_betaald;
                }
            }
        }
        // setting array
        $array = array();
        array_push($array, $omzet);
        array_push($array, $nb);
        return $array;
    }

    public function getCostsTwo($startdate, $enddate)
    {
        $crediteuren = Crediteur::where('updated_at','>=', $startdate)->where('updated_at', '<', date('Y-m-d', strtotime("+1 day", strtotime($enddate))))->where('terugbetaald', 1)->get();
        $creditering = 0.00;
        foreach ($crediteuren as $crediteur)
        {
            $creditering += $crediteur->bedrag;
        }

        $inkopen = Inkoop::where('datum', '>=', $startdate)->where('datum', '<=', $enddate)->where('voldaan', 1)->get();
        $inkoop = 0.00;
        foreach ($inkopen as $i)
        {
            $inkoop += $i->totaalbedrag;
        }

        $kantoorkosten = Kantoorkosten::where('datum','>=', $startdate)->where('datum', '<=', $enddate)->get();
        $kantoor = 0.00;
        foreach ($kantoorkosten as $k)
        {
            $kantoor += $k->betaald;
        }

        $overigekosten = Overigekosten::where('datum','>=', $startdate)->where('datum', '<=', $enddate)->get();
        $overig = 0.00;
        foreach ($overigekosten as $o)
        {
            $overig += $o->kosten;
        }

        $wervingskosten = Wervingskosten::where('datum', '>=', $startdate)->where('datum', '<=', $enddate)->get();
        $werving = 0.00;
        foreach ($wervingskosten as $w)
        {
            $werving += $w->totaalloon;
            $werving += $w->benzine;
            $werving += $w->benzinenb;
        }

        $werkdagen = Werkdag::where('datum', '>=', $startdate)->where('datum', '<=', $enddate)->get();
        if($werkdagen == null)
        {
            $arr = array();
            $arr = (object) $arr;
            $arr->error = true;
            return $arr;
        }
        // dd($werkdagen);
        $werkdagkosten = 0.00;
        foreach($werkdagen as $wd)
        {
            $werklijsten = Worklist::where('werkdag_id', $wd->id)->get();
            if($werklijsten != null){
                foreach ($werklijsten as $wl)
                {
                    $werkdagkosten += ($wl->totaalloonVerk - $wl->nbloonVerk);
                    $werkdagkosten += ($wl->totaalloonVeg - $wl->nbloonVeg);
                    $werkdagkosten += $wl->benzine;
                    $werkdagkosten += $wl->eten;
                    $werkdagkosten += $wl->telefoonkosten;
                    $werkdagkosten += $wl->extra;
                    $werkdagkosten += $wl->inkoop;
                }
            }
        }


        $totalekosten = $creditering + $inkoop + $kantoor + $overig + $werving + $werkdagkosten;
        $arr = array();
        $arr = (object) $arr;
        // $arr->kosten = $totalekosten;
        $arr->creditering = $creditering;
        $arr->inkoop = $inkoop;
        // dd($arr->inkoop);
        $arr->kantoor = $kantoor;
        $arr->overig = $overig;
        $arr->werving = $werving;
        $arr->verkoop = $werkdagkosten;
        $arr->totaal = $arr->creditering + $arr->inkoop + $arr->kantoor + $arr->overig + $arr->werving + $arr->verkoop;
        // dd($arr);
        return $arr;
    }

    public function getProfitTwo($startdate, $enddate)
    {
        $crediteuren = Crediteur::where('updated_at','>=', $startdate)->where('updated_at', '<', date('Y-m-d', strtotime("+1 day", strtotime($enddate))))->where('terugbetaald', 1)->get();
        $creditering = 0.00;
        foreach ($crediteuren as $crediteur)
        {
            $creditering += $crediteur->bedrag;
        }

        $inkopen = Inkoop::where('datum', '>=', $startdate)->where('datum', '<=', $enddate)->where('voldaan', 1)->get();
        $inkoop = 0.00;
        foreach ($inkopen as $i)
        {
            $inkoop += $i->totaalbedrag;
        }

        $kantoorkosten = Kantoorkosten::where('datum','>=', $startdate)->where('datum', '<=', $enddate)->get();
        $kantoor = 0.00;
        foreach ($kantoorkosten as $k)
        {
            $kantoor += $k->betaald;
        }

        $overigekosten = Overigekosten::where('datum','>=', $startdate)->where('datum', '<=', $enddate)->get();
        $overig = 0.00;
        foreach ($overigekosten as $o)
        {
            $overig += $o->kosten;
        }

        $wervingskosten = Wervingskosten::where('datum', '>=', $startdate)->where('datum', '<=', $enddate)->get();
        $werving = 0.00;
        foreach ($wervingskosten as $w)
        {
            $werving += $w->ubloon;
            $werving += $w->benzine;
        }

        $werkdagen = Werkdag::where('datum', '>=', $startdate)->where('datum', '<=', $enddate)->get();
        if($werkdagen == null)
        {
            $arr = array();
            $arr = (object) $arr;
            $arr->error = true;
            return $arr;
        }
        // dd($werkdagen);
        $werkdagkosten = 0.00;
        foreach($werkdagen as $wd)
        {
            $werklijsten = Worklist::where('werkdag_id', $wd->id)->get();
            if($werklijsten != null){
                foreach ($werklijsten as $wl)
                {
                    $werkdagkosten += ($wl->totaalloonVerk - $wl->nbloonVerk);
                    $werkdagkosten += ($wl->totaalloonVeg - $wl->nbloonVeg);
                    $werkdagkosten += $wl->benzine;
                    $werkdagkosten += $wl->eten;
                    $werkdagkosten += $wl->telefoonkosten;
                    $werkdagkosten += $wl->extra;
                    $werkdagkosten += $wl->inkoop;
                }
            }
        }

        //retrieve all appointments within the startdate
        $afspraken = Appointment::with('klant')->where('startdatum', '>=', $startdate)
                                            ->where('einddatum', '<', date('Y-m-d', strtotime("+1 day", strtotime($enddate))))
                                            ->where('afspraaktype_id', 1)->get();
        //declaring vars
        $omzet = 0.00;
        $nb = 0.00;
        //loop for getting the amount
        foreach ($afspraken as $a)
        {
            $status = Status_has_appointment::where('afspraak_id', $a->id)->first();
            if($status != null){
                if($status->status_id == 3 || $status->status_id == 4 ){
                    $omzet += $a->totaalbedrag;
                    $nb += $a->niet_betaald;
                }
            }
        }

        $totalekosten = $creditering + $inkoop + $kantoor + $overig + $werving + $werkdagkosten;
        return $omzet - $nb - $totalekosten;
    }

    public function getSalesTwo($startdate, $enddate, $aggregate)
    {
        //
        if($aggregate)
        {
            $werkdagen = Werkdag::where('datum', '>=', $startdate)->where('datum', '<=', $enddate)->orderBy('datum', 'ASC')->get();
            if($werkdagen == null)
            {
                $arr = array();
                $arr = (object) $arr;
                $arr->error = true;
                return $arr;
            }
            // dd($werkdagen);
            $werkdagkosten = 0.00;
            foreach($werkdagen as $wd)
            {
                $werklijsten = Worklist::where('werkdag_id', $wd->id)->get();
                if($werklijsten != null){
                    foreach ($werklijsten as $wl)
                    {
                        $werkdagkosten += ($wl->totaalloonVerk - $wl->nbloonVerk);
                        $werkdagkosten += ($wl->totaalloonVeg - $wl->nbloonVeg);
                        $werkdagkosten += $wl->benzine;
                        $werkdagkosten += $wl->eten;
                        $werkdagkosten += $wl->telefoonkosten;
                        $werkdagkosten += $wl->extra;
                        $werkdagkosten += $wl->inkoop;
                    }
                }
            }
            return $werkdagkosten;
        }

        $werkdagen = Werkdag::where('datum', '>=', $startdate)->where('datum', '<=', $enddate)->orderBy('datum', 'ASC')->get();
        $werkdagkosten = 0.00;
        foreach($werkdagen as $werkdag)
        {
            $werklijsten = Worklist::with('verkoper', 'veger')->where('werkdag_id', $werkdag->id)->get();
            foreach ($werklijsten as $wl)
            {
                $wl->wha = Worklist_has_appointments::where('werklijst_id', $wl->id)->get();
                foreach ($wl->wha as $wha)
                {
                    $wha->afspraak_id = Appointment::find($wha->afspraak_id);
                    if($wha->afspraak_id != null){
                        $wha->afspraak_id->klant_id = Client::find($wha->afspraak_id->klant_id);
                        $wha->afspraak_id->status = Status_has_appointment::where('afspraak_id', $wha->afspraak_id->id)->first();
                        $wha->afspraak_id->status->status_id = Status::find( $wha->afspraak_id->status->status_id );
                    }
                }
            }
            $werkdagkosten = $werklijsten;
        }

        return $werkdagkosten;
    }

    public function getAcqTwo($startdate, $enddate, $aggregate)
    {
        //
        if($aggregate)
        {
            $werving = Wervingskosten::with('werknemer_id')->where('datum', '>=', date('Y-m-d', strtotime($startdate)))->where('datum', '<=', date('Y-m-d', strtotime($enddate)))->get();
            $costs = 0.00;
            $delta = 0.00;
            $expected = 0.00;
            foreach($werving as $w){
                $costs += ($w->totaalloon + $w->benzine + $w->benzinenb);
                $delta += $w->verschil;
                $expected += ($costs - $delta);
            }

            $arr = array();
            $arr = (object) $arr;
            $arr->wervingskosten = $costs;
            $arr->wervingsdelta = $delta;
            $arr->wervingsverwachting = $expected;
            return $arr;
        }
        $werving = Wervingskosten::where('datum', '>=', date('Y-m-d', strtotime($startdate)))->where('datum', '<=', date('Y-m-d', strtotime($enddate)))->get();
        foreach($werving as $w){
            $w->werknemer_id = Employee::find($w->werknemer_id);
        }

        $klanten = DB::select( DB::raw( "select w.id as 'werknemer', count(k.id) as 'aantal'
                                     from klant as k
                                     join werknemer as w
                                     on w.id = k.geschreven_door
                                     where k.geschreven_op >= '".date('Y-m-d', strtotime($startdate))."' AND k.geschreven_op <= '".date('Y-m-d', strtotime($enddate))."'
                                     group by w.id" ));
        // dd($klanten);
        foreach($klanten as $k){
            foreach($werving as $w){
                if($k->werknemer == $w->werknemer_id->id){
                    $w->aantal = $k->aantal;
                    // $fhw = Functie_has_Werknemer::where('werknemer_id', $w->)
                    $w->dagloon = Functie_has_Werknemer::where('werknemer_id', $w->werknemer_id->id)->first()->dagloon;
                    $w->uurloon = Functie_has_Werknemer::where('werknemer_id', $w->werknemer_id->id)->first()->uurloon;
                    $w->provisiegrens = Functie_has_Werknemer::where('werknemer_id', $w->werknemer_id->id)->first()->provisiegrens;
                    $w->provisiebedrag = Functie_has_Werknemer::where('werknemer_id', $w->werknemer_id->id)->first()->provisiebedrag;
                    $w->provisieperc = Functie_has_Werknemer::where('werknemer_id', $w->werknemer_id->id)->first()->provisieperc;
                }
            }
        }

        return $werving;
    }

    public function getAdminTwo($startdate, $enddate, $aggregate)
    {
        $kantoorkosten = Kantoorkosten::with('werknemer_id')->where('datum', '>=', date('Y-m-d', strtotime($startdate)))->where('datum', '<=', date('Y-m-d', strtotime($enddate)))->get();
        if($aggregate)
        {
            $costs = 0.00;
            $delta = 0.00;
            $expected = 0.00;
            foreach($kantoorkosten as $k){
                $expected += ($k->aantal_uur * $k->uurtarief);
                $delta += $expected - $k->betaald;
                $costs += $k->betaald;
            }

            $arr = array();
            $arr = (object) $arr;
            $arr->kantoorkosten = $costs;
            $arr->kantoordelta = $delta;
            $arr->kantoorverwachting = $expected;
            return $arr;
        }
        foreach($kantoorkosten as $k){
            $k->werknemer_id = Employee::find($k->werknemer_id);
        }
        return $kantoorkosten;
    }

    public function getPurchaseTwo($startdate, $enddate, $aggregate)
    {
        $inkoop = Inkoop::where('datum', '>=', date('Y-m-d', strtotime($startdate)))->where('datum', '<=', date('Y-m-d', strtotime($enddate)))->where('voldaan', 1)->get();
        if($aggregate)
        {
            $inkoopkosten = 0.00;
            foreach ($inkoop as $i) {
                $inkoopkosten += $i->totaalbedrag;
            }
            return $inkoopkosten;
        }
        foreach ($inkoop as $i) {
            $i->klant_id = Client::find($i->klant_id);
        }
        return $inkoop;
    }

    public function getRemainderTwo($startdate, $enddate, $aggregate)
    {
        $overig = Overigekosten::where('datum', '>=', date('Y-m-d', strtotime($startdate)))->where('datum', '<=', date('Y-m-d', strtotime($enddate)))->get();
        if($aggregate)
        {
            $overigekosten = 0.00;
            foreach ($overig as $o) {
                $overigekosten += $o->kosten;
            }
            return $overigekosten;
        }
        return $overig;
    }

    public function getLists($startdate, $enddate){
        $werkdagen = Werkdag::where('datum', '>=', $startdate)->where('datum', '<=', $enddate)->get();
        return $werkdagen;
    }
}
