<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Model;

use App\Models\Appointment;

use App\Models\Work;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class Appointment_has_activities extends Model
{
    //
    protected $fillable = ['activiteit_id', 'afspraak_id', 'aantal', 'kortingsperc', 'kortingsbedrag'];
    public $table = "activiteit_has_afspraak";
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function appointment(): BelongsTo
    {
    	return $this->belongsTo(Appointment::class, 'afspraak_id');
    }

    public function work()
    {
    	return $this->belongsTo(Work::class, 'activiteit_id');
    }





    //only when price doesnt change and when it's only ONE activity

    public static function addStandardAha($afspraak_id, $act_id, $aantal){

        $aha = new Appointment_has_activities;

        $aha->activiteit_id = $act_id;

        $aha->afspraak_id = $afspraak_id;

        $aha->aantal = $aantal;

        $aha->prijs = Work::getPrice($act_id);

        $aha->kortingsbedrag = 0;

        $aha->kortingsperc = 0;

        $aha->save();

    }



    public static function getAllAppointmentsWithActivity($klant_id, $bool){

        if(!$bool){

            $actperafs = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function($afs) use ($klant_id)

            {

                $afs->where('klant_id', $klant_id)->where('niet_betaald', '!=', 0);

            })->get();

        } else {

            $actperafs = Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function($afs) use ($klant_id)

            {

                $afs->where('klant_id', $klant_id);

            })->get();

        }

        return $actperafs;

    }



    public static function addActivityToAppointment($input, $afspraak_id, $klusprijs){

        $afspraak = Appointment::find($afspraak_id);

        $totaalbedrag = ($klusprijs == true) ? $afspraak->totaalbedrag : 0.00;



        for($i = 0; $i < count($input['activiteiten']); $i++){

            $act = Work::find($input['activiteiten'][$i]);

            $aha = new Appointment_has_activities;

            $aha->activiteit_id = $act->id;

            $aha->afspraak_id = $afspraak_id;

            $aha->aantal = $input['aantal'][$i];

            $aha->opmerking = $input['opmerking'][$i];



            if($klusprijs){

                $aha->prijs = ($input['prijs'][$i] > 0 ? $input['prijs'][$i] : $act->prijs);

                $bedrag = $aha->prijs * $aha->aantal - $aha->input['korting'][$i];

                if($totaalbedrag - $bedrag > 0){

                    $aha->kortingsbedrag = $input['korting'][$i];

                    $aha->kortingsperc = $aha->kortingsbedrag / $bedrag * 100;

                    $totaalbedrag -= $bedrag;

                } else {

                    if($totaalbedrag > 0){

                        $aha->kortingsbedrag = $input['korting'][$i] + ($bedrag - $totaalbedrag);

                        $aha->kortingsperc = $aha->kortingsbedrag / $bedrag * 100;

                        $totaalbedrag -= $bedrag;

                    } else {

                        $aha->prijs = 0.00;

                        $aha->kortingsbedrag = $act->prijs;

                        $aha->kortingsperc = 100;

                    }

                }

            } else {

                $aha->prijs = ($input['prijs'][$i] != 0 ? $input['prijs'][$i] : $act->prijs);

                $aha->kortingsbedrag = $input['korting'][$i];

                $aha->kortingsperc = $aha->kortingsbedrag / ($aha->prijs * $aha->aantal) * 100;

                $totaalbedrag += $aha->prijs * $aha->aantal - $aha->kortingsbedrag;

            }

            $aha->save();

        }

        if(!$klusprijs){

            if(isset($input['extra'])){

                $totaalbedrag = 0.00;

                $ahas = Appointment_has_activities::where('afspraak_id', $afspraak_id)->get();

                foreach ($ahas as $aha) {

                    $totaalbedrag += $aha->aantal * $aha->prijs - $aha->kortingsbedrag;

                }

                if(isset($input['voldaan'])){

                    if($input['voldaan'] == 0){

                        $afspraak->niet_betaald = $totaalbedrag;

                    }

                }

            }

            $afspraak->setTotal($totaalbedrag);

        }

        return true;

    }



    public function getActivityDescription(){

        return $this->activiteit->omschrijving;

    }



    public static function deleteSelf($afspraak){

        $werkzaamheden = Appointment_has_activities::where('afspraak_id', $afspraak)->get();

        foreach ($werkzaamheden as $w) {

            $w->delete();

        }

    }



    //return all activities related to this appointment

    public static function aha($afspraak_id, $relatie_id){

        return Appointment_has_activities::with('afspraak', 'activiteit')->whereHas('afspraak', function($afs) use ($relatie_id, $afspraak_id)

        {

            $afs->where('klant_id', $relatie_id)->where('id', $afspraak_id);

        })->get();

    }



    public static function setService($afspraak_id, $service_array){

        $afspraak = Appointment::find($afspraak_id);

        for($i = 0; $i < count($service_array); $i++){

            $aha = Appointment_has_activities::where('activiteit_id', $service_array[$i])->where('afspraak_id', $afspraak_id)->first();

            $aha->prijs = 0;

            $aha->save();

            // Log::info('Aha aangepast: '.$aha->prijs.' - id: '.$aha->activiteit_id);

        }

        //define new total if there is no klusprijs.

        if($afspraak->klusprijs != 1){

            $totaalbedrag = 0.00;

            $ahas = Appointment_has_activities::where('afspraak_id', $afspraak_id)->get();

            foreach ($ahas as $aha) {

                $totaalbedrag += $aha->aantal * $aha->prijs - $aha->kortingsbedrag;

            }

            $afspraak->setTotal($totaalbedrag);

        }



    }



    public static function setWarrantyActivities($old_appointment, $new_appointment, $input){

        $tempafspraak = Appointment_has_activities::where('afspraak_id', $old_appointment)->get();

        foreach ($tempafspraak as $tempact)

        {

            $aha = new Appointment_has_activities;

            $aha->activiteit_id = $tempact->activiteit_id;

            $aha->afspraak_id = $new_appointment;

            $aha->aantal = $tempact->aantal;

            $aha->prijs = 0;



            $aha->kortingsbedrag = 0;

            $aha->kortingsperc = 0;

            $aha->opmerking = $tempact->opmerking;

            $aha->save();

        }

    }



    public static function getTotal($id){

        $total = 0.00;

        $ahas = Appointment_has_activities::where('afspraak_id', $id)->get();

        foreach($ahas as $aha){

            $total += ($aha->aantal * $aha->prijs - $aha->kortingsbedrag);

        }

        return $total;

    }



    public static function copyActivities($old, $new_id){

        $ahas = Appointment_has_activities::where('afspraak_id', $old)->get();

        foreach($ahas as $aha){

            $new = new Appointment_has_activities;

            $new->activiteit_id = $aha->activiteit_id;

            $new->afspraak_id = $new_id;

            $new->aantal = $aha->aantal;

            $new->prijs = $aha->prijs;

            $new->kortingsbedrag = $aha->kortingsbedrag;

            $new->kortingsperc = $aha->kortingsperc;

            $new->opmerking = $aha->opmerking;

            $new->save();

        }

    }



}

