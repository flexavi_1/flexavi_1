<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aanvulling extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "aanvulling";

    public function gebruiker()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function verzonden_aan()
    {
    	return $this->hasMany('App\Models\Verzonden_aan');
    }

    public function getKlant()
    {
        if($this->klant_id != null)
        {
            return Client::find($this->klant_id)->voornaam." ".Client::find($this->klant_id)->achternaam;
        }
        else return 0;
    }


}
