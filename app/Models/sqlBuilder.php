<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class sqlBuilder extends Model
{

	public static function returnSelect($selectOne, $selectTwo, $selectThree, $selectFour, $selectFive, $selectSix){
		$select = '';
		$isRevenue = false;

        if(!is_array($selectOne) && $selectOne != 'omzet'){
            if($selectOne == 'gemiddelde'){
                $select .= 'select AVG(#field1#) #field1Name ';
            } elseif($selectOne == 'Totaal'){
                $select .= 'select SUM(#field1#) #field1Name ';
            } else {
                $select .= 'select count(#field1#) #field1Name ';
            }
            if($selectTwo != 'van'){
                return false; // Groep 1 kan alleen met 'van'
            }
        } elseif(!is_array($selectOne)) {
        	$isRevenue = true;
        } else {// selectOne is array

        }

        if(is_array($selectOne)){
            //selectOne zit in de derde groep
            if($selectTwo == 'van'){
                return false; // de derde groep kan niet met van
            }
        } else {
        	if(!$isRevenue && is_array($selectThree)){
        		$select = str_replace('#field1#', $selectThree['name'], $select);
        		$select = str_replace('#field1Name', $selectThree['as'], $select);
        		if($selectFive == null){
	        		if(count($selectThree) > 3){
		        		$select .= ', '.$selectThree['name'].' '.$selectThree['as'];
	    			} else {
		        		$select .= ', '.$selectThree['f'].' '.$selectThree['as'];
	    			}
    			}
        	}
        }

        if($selectFive != null){
        	$select .= ', #field2# #field2Name';
        	$select = str_replace('#field2#', $selectFive['name'], $select);
        	$select = str_replace('#field2Name', $selectFive['as'], $select); 
        }

        return $select;
	}

	public static function returnJoins($selectOne, $selectTwo, $selectThree, $selectFour, $selectFive, $selectSix){
		$joins = '';

		if(!is_array($selectOne) && $selectOne != 'omzet'){
			//avg, count, sum
			if(is_array($selectThree)){
				if(count($selectThree) != 3){
					//external
					$joins .= 'join '.$selectThree['join'].' on '.$selectThree['f'].'='.$selectThree['id'].' ';
				} 
			}
			if($selectFive != null){
				if(count($selectFive) > 3){ // bigger than 5 is other table
					//external
					$joins .= 'join '.$selectFive['join'].' on '.$selectFive['f'].'='.$selectFive['id'].' ';
				}
			}
		} elseif(!is_array($selectOne) && $selectOne == 'omzet'){

		} else {

		}


		return $joins;
	}

	public static function returnGroupby($selectOne, $selectTwo, $selectThree, $selectFour, $selectFive, $selectSix){
		$groupby = ' Group by ';

		if($selectFive == null){
			$groupby .= $selectThree['f'].' ';
		} elseif(is_array($selectFive)){
			$groupby .= $selectFive['f'].' ';
		}
		return $groupby;
	}

	public static function returnWhere($selectOne, $selectTwo, $selectThree, $selectFour, $selectFive, $selectSix, $startdate, $enddate){
		$where = ' where ';
		$joinDate = sqlBuilder::returnDateJoin($selectOne, $selectThree, $selectFive);
		$where = $joinDate . $where;
		if($startdate == null && $enddate == null){
			//sinds vooraltijd.
			return '';

		} elseif($startdate != null && $enddate == null){
			//startdatum bekend
			$where .= ' dt.date >='.$startdate.' ';

		} elseif($startdate == null && $enddate != null){
			$where .= $enddate.' <= dt.date ';

		} else {
			//beide ingevuld.
			$where .= ' dt.date >='.$startdate.' and '.$enddate.' =< dt.date';
		}
		return $where;
	}

	public static function returnOrderby($selectOne, $selectTwo, $selectThree, $selectFour, $selectFive, $selectSix){
		if($selectSix == null){
			return '';
		}
		$asc = ($selectSix % 2 == 0) ? ' asc ' : ' desc ';
		$orderby = ' order by ';
		if($selectSix < 3){
			if(!is_array($selectOne)){
				if($selectOne == 'gemiddelde'){
					$orderby .= 'AVG('.$selectThree['name'].') '.$asc; 
				} elseif($selectOne == 'count'){
					$orderby .= 'COUNT('.$selectThree['name'].') '.$asc; 
				} elseif($selectOne == 'totaal'){
					$orderby .= 'SUM('.$selectThree['name'].') '.$asc; 
				} else {

				}
			} else {	
				$orderby = $orderby.$selectOne['name'].$asc;
			}
		} elseif($selectSix < 5){
			if(!is_array($selectOne)){
			} else {
				$orderby .= $selectThree['name'].$asc;
			}
		} else {
			$orderby .= $selectFive['name'].$asc;

		}
		return $orderby;
	}

	public static function returnDateJoin($selectOne, $selectThree, $selectFive){
		$bool = false;
		if(is_array($selectOne)){
			if($selectOne['f'] == 'fs.dim_time_id'){
				$bool = true;
			}
		}
		if(is_array($selectThree)){
			if($selectThree['f'] == 'fs.dim_time_id'){
				$bool = true;
			}
		}
		if($selectFive['f'] == 'fs.dim_time_id'){
			$bool = true;
		}
		if($bool){
			return '';
		} else {
			return ' join dim_time as dt on fs.dim_time_id = dt.id ';
		}
	}

}