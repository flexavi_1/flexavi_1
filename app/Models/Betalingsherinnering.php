<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Datetime;
use App\Models\Worklist_has_appointments;
use App\Models\Status_has_appointment;
use App\Models\AppointmentType;
use App\Models\Offerte;
use App\Models\Appointment;
use Auth;

class Betalingsherinnering extends Model
{
	public $afspraak;

	public function __construct($app_object){
		$this->afspraak = $app_object;
	}

	public function setProcessOne($gelukt, $extra, $voldaan, $bank, $betalingsherinnering){
		if($voldaan == 1){
			$this->afspraak->bank = $bank;
			$this->afspraak->betaald = 1;
			$check = $this->setProcessThree($bank);
			if($check){

			}
		} else { //betalingsherinnering niet nagekomen, datum verlengen
			$this->afspraak->betaald = 0;
			$this->afspraak->niet_betaald = $this->afspraak->totaalbedrag;
			$aantaldagen = $betalingsherinnering;
	        if($aantaldagen > 0 && $voldaan == 0){
		        $this->afspraak->startdatum = date('Y-m-d H:i', strtotime($this->afspraak->startdatum.' + '.$aantaldagen.' days'));
		        $this->afspraak->einddatum = date('Y-m-d H:i', strtotime($this->afspraak->einddatum.' + '.$aantaldagen.' days'));
	    	}
		}

		$this->afspraak->save();
		$status = ($voldaan == 1) ? 4 : 3;
		$message = ($voldaan == 1) ? ": Afspraak is voldaan en verwerkt" : ": Afspraak is niet voldaan, maar wel verwerkt.";
		Status_has_appointment::setSha($this->afspraak->id, $status, $message, $this->afspraak->startdatum);
		$message = ($voldaan == 0) ? Auth::user()->name." heeft de afspraak verwerkt, afspraak is niet voldaan" : Auth::user()->name." heeft de afspraak verwerkt, afspraak is voldaan";
		History::setHistory($this->afspraak->klant_id, $this->afspraak->id, Auth::user()->id, $message, 'Afspraak');
		Log::createlog(Auth::user()->id, $this->afspraak->klant_id, 'Afspraak', Auth::user()->name." heeft deze klant, ".$this->afspraak->klant->postcode." - ".$this->afspraak->klant->huisnummer." afspraak verwerkt.");

		if($extra == 1){
			return array($extra);
		}
		return array($voldaan, false);
	}

	public function setProcessTwo($input, $new_appointment, $bool){
		if($new_appointment != null){ // if new appointment is filled, it means this process of creating a new appointment is a result of a nonpaid appointment
			if(isset($input['betaald'])){ // if 'betaald' isset, its the first appointment to be made.
				if($input['betaald'] >= $this->afspraak->totaalbedrag || $input['betaald'] + $input['tebetalen'] > $this->afspraak->totaalbedrag){
					return false;
				}

				$new_appointment->saveAppoinmentPartOne($input);
				$new_appointment->setTotal($input['tebetalen']);
				$this->afspraak->niet_betaald -= $input['betaald']; // reduce non paid total from old appointment
				$this->afspraak->save();
				Log::createLog(Auth::user()->id, $new_appointment->klant_id, 'Afspraak', Auth::user()->name.' heeft een <b>betaalherinnering</b> gemaakt.');
				History::setHistory($new_appointment->klant_id, $this->afspraak->id, Auth::user()->id,
											Auth::user()->name.' heeft hiervoor een betaalafspraak gemaakt op '.$new_appointment->returnDate($new_appointment->startdatum), 'Afspraak');
		    	// set status for appointment
		    	Status_has_appointment::setSha($new_appointment->id, 1, 'heeft deze afspraak toegevoegd', $new_appointment->startdatum);
		    	Afspraak_has_geschiedenis::copyHistory($this->afspraak->id, $new_appointment->id);

				if($input['betaald'] + $input['tebetalen'] == $this->afspraak->totaalbedrag){
					return true;
				}

				return array('eerste');
			}

			$afspraken = $this->afspraak->getAllReferences(); // retrieve all new appointments with this appointment as reference
			$toBePaid = 0.00;
			foreach($afspraken as $a){
				$toBePaid += $a->totaalbedrag; // retrieve amount to be paid
			}
			if($input['tebetalen'] > $this->afspraak->niet_betaald - $toBePaid){ // if new value is bigger than the amount to be paid -> error
				return false;
			}
			// else -> save appointment
			$new_appointment->saveAppoinmentPartOne($input);
			$new_appointment->setTotal($input['tebetalen']);
			Log::createLog(Auth::user()->id, $new_appointment->klant_id, 'Afspraak', Auth::user()->name.' heeft een betaalafspraak gemaakt.');
			History::setHistory($new_appointment->klant_id, $this->afspraak->id, Auth::user()->id,
										Auth::user()->name.' heeft hiervoor een betaalafspraak gemaakt op '.$new_appointment->returnDate($new_appointment->startdatum), 'Afspraak');
	    	// set status for appointment
	    	Status_has_appointment::setSha($new_appointment->id, 1, 'heeft deze afspraak toegevoegd', $new_appointment->startdatum);
	    	Afspraak_has_geschiedenis::copyHistory($this->afspraak->id, $new_appointment->id);


			if($new_appointment->totaalbedrag == $this->afspraak->niet_betaald - $toBePaid){ // there is no need to create other appointments
				return true;
			}

			return array('tweede');
		} else{
			// else it means we need to process the extra work first
			if(isset($input['klusprijsAan'])){
				$klusprijs = true;
				$this->afspraak->setKlusprijs();
				$this->afspraak->setTotal($input['klusprijs']);
			} else {
				$klusprijs = false;
			}
			Appointment_has_activities::addActivityToAppointment($input, $input['afspraak_id'], $klusprijs);
			$this->afspraak->setActivityHistory();
			Voorraad::mutatingStock($this->afspraak->id);
			return ($input['voldaan'] == 1) ? true : false ;
		}
	}

	public function setProcessThree($bank){ // edit previous appointment
		$oud = Appointment::find($this->afspraak->afspraak_id);
        $oud->betaald = 1;
        $oud->niet_betaald = 0;
        $oud->bank = $bank;
        $oud->save();
        Status_has_appointment::setSha($oud->id, 4, "Betalingsherinnering nagekomen, afspraak betaald.", $oud->startdatum);
        $status_oud = Status_has_appointment::where('afspraak_id', $oud->id)->first();
        $this->afspraak->betaald = 1;
        $this->afspraak->save();

        return true;
	}


}
