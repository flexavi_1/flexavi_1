<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Uur extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "uur";

    public function returnDate($date)
    {
    	$format = date('d-M-Y H:i', strtotime($date));

    	return $format;
    }

    public function klant()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function werknemer()
    {
        return $this->belongsTo('App\Models\Employee');
    }

    public function minutes($starttijd, $eindtijd)
    {
        $start = DateTime::createFromFormat("d-m-Y H:i", $starttijd);
        $eind = DateTime::createFromFormat("d-m-Y H:i", $eindtijd);
        $interval = $start->diff($eind);
        $minutes = $interval->days * 24 * 60;
        $minutes += $interval->h * 60;
        $minutes += $interval->i;

        return $minutes;
    }
}
