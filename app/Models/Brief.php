<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brief extends Model
{
    //
    protected $guarded = ['id'];
    public $table = "brief"; 

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }
}