// import {Quill} from "../libs/quill/quill.min.js";

document.addEventListener('DOMContentLoaded', init);

let tags = {};
let toolbarOptions = [
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'font': [] }],
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],

    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
    [{ 'direction': 'rtl' }],                         // text direction

    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown

    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'align': [] }],

    ['image', 'video'],
    ['clean']                                         // remove formatting button
];
let user_id = -1;
async function init() {
    tags = {
        history: {
            modalId: 'add-history-modal-',
            modalTitle: 'Opmerking toevoegen',
            type: 'add-history',
            buttonId: 'save-history-',
            datetimeId: 'add-history-date-',
            datetimePlaceholder: 'Kies Datum & Tijd',
            textAreaId: 'add-history-text-',
            textareaPlaceholder: 'Opmerking',
            buttonNoNewId: 'save-history-no-new-',
            postNewHistoryUrl: '/terugbellijst/setComment',
            fieldsNotFilled: "Niet alle velden zijn ingevuld, vul deze eerst in."
        },
        general: {
            url: '/terugbellijst/js',
            tableBodyId: 'terugbellijst-table-body',
            cell4: 'cell-history-',
        },
        customer: {
            profileUrl: '/relatie/',
        },
        modalGeneral: {
            close: "Sluiten",
            add: "Toevoegen",
            edit: "Wijzigen",
            delete: "Verwijderen",
            cancel: "Annuleren",
            save: "Opslaan",
            confirm: "Bevestigen",
            saveNoNew: "Opslaan en uit TB lijst halen"
        },
        appointment: {
            modalId: 'add-appointment-modal-',
            modalTitle: 'Afspraak toevoegen',
            type: 'add-appointment',
            buttonId: 'save-appointment-',
            datetimeId: 'add-appointment-startdate-',
            datetimePlaceholder: 'Kies Datum & Tijd',
            textAreaId: 'add-appointment-text-',
            textareaPlaceholder: 'Beschrijving',
            postNewAppointment: '/terugbellijst/setAppointment',
            fieldsNotFilled: "Niet alle velden zijn ingevuld, vul deze eerst in.",
            lblStartDate: "Start datum",
            quillEditors: [],
            oldAppointmentId: 'oldAppointmentId-',
            editorId: 'editor-',
            beforeTwelveThirty: 'Voor de middag?',
            beforeTwelveThirtyId: 'beforeTwelveThirty-',
            copyOldAppointment: 'Oude afspraak kopieren?',
            copyOldAppointmentId: 'copyOldAppointment-',
            validateFields: "Geen geldige gegevens ingevuld"
        },
        GGH: {
            title: 'Geen Gehoor',
            id: 'no-answer-',
            url: '/terugbellijst/setGGH'
        }
    }
    user_id = document.getElementById('user_id').value
    const url = tags.general.url
    const data = await getJsData(url)
    if (data.status === 200) {
        const rows = data.data
        await fillTable(rows)
    } else {
        console.error(data.error)
    }
    initiateQuill()
    initiateDatePicker()
}

function fillTable(data) {
    const tableBody = document.getElementById(tags.general.tableBodyId)
    data.forEach(row => {
        tableBody.appendChild(createRow(row))
    })
}

function createRow(data) {
    const row = document.createElement('tr')
    row.id = data.klant_id
    //cell 1 -> name (link and text)
    const cell1 = document.createElement('td')
    const link = createLink({
        url: `${tags.customer.profileUrl}${data.klant_id}`,
        title: `${data.voornaam} ${data.achternaam}`
    })
    cell1.appendChild(link)
    row.appendChild(cell1)

    //cell 2 -> address (text)
    const cell2 = document.createElement('td')
    cell2.innerHTML = `${data.straat} ${data.huisnummer} ${data.hn_prefix}, <br> ${data.postcode} te ${data.woonplaats}`
    row.appendChild(cell2)

    //cell 3 -> contact (text)
    const cell3 = document.createElement('td')
    let businessNumberString = (data.telefoonnummer_zakelijk !== "") ? ` | ${data.telefoonnummer_zakelijk}` : ""
    cell3.innerHTML = `${data.telefoonnummer_prive}${businessNumberString}<br> ${data.email}`
    row.appendChild(cell3)

    //cell 4 -> description (html text)
    const cell4 = document.createElement('td')
    cell4.id = tags.general.cell4 + data.klant_id + '-' + data.afspraak_id
    // Split the input date-time string into an array of date and time components
    const [datePart, timePart] = data.startdatum.split(' ');

    // Split the date component into an array of year, month, and day
    const dateComponents = datePart.split('-');

    // Rearrange the date components in the "dd-mm-yyyy" format
    const formattedDate = `${dateComponents[2]}-${dateComponents[1]}-${dateComponents[0]}`;

    cell4.innerHTML = `<span class="text-danger"> ${formattedDate}: ${data.redenen[0]}</span><br>`
    for (let i = 1; i < data.redenen.length; i++) {
        if (i !== 1) cell4.innerHTML += `<br>`
        cell4.innerHTML += `<span class="text-success">${data.redenen[i].omschrijving}</span>`
    }
    row.appendChild(cell4)

    //cell 5 -> button (btn group)
    const cell5 = document.createElement('td')
    const btnGroup = createButton(data)
    cell5.appendChild(btnGroup.btnGroup)
    row.appendChild(cell5)
    btnGroup.rowModals.forEach(modal => {
        row.appendChild(modal)
    })

    return row
}

//create the button dropdown in the form
function createButton(data) {
    const btnGroup = document.createElement('div')
    btnGroup.classList.add('btn-group')
    //create trigger button
    const triggerBtn = document.createElement('button')
    triggerBtn.type = 'button'
    triggerBtn.classList.add('btn', 'btn-primary', 'dropdown-toggle', 'btn-xs')
    triggerBtn.setAttribute('data-bs-toggle', 'dropdown')
    triggerBtn.setAttribute('aria-expanded', 'false')
    triggerBtn.innerText = 'Opties'
    btnGroup.appendChild(triggerBtn)

    //create dropdown list
    const dropdownList = document.createElement('ul')
    dropdownList.classList.add('dropdown-menu')

    //create list elements
    //first item
    const liAddHistory = document.createElement('li')
    const aAddHistory = createLink({
        // url: `/geschiedenis/${data.klant_id}/toevoegen/${data.afspraak_id}/tb`,
        url: '#',
        title: tags.history.modalTitle
    })
    aAddHistory.classList.add('dropdown-item')
    aAddHistory.setAttribute('data-bs-toggle', 'modal')
    aAddHistory.setAttribute('data-bs-target', `#${tags.history.modalId}${data.klant_id}-${data.afspraak_id}`)
    liAddHistory.appendChild(aAddHistory)
    // console.log(data)
    const rowModal = createModal({
        id: `${tags.history.modalId}${data.klant_id}-${data.afspraak_id}`,
        title: tags.history.modalTitle + " - " + data.voornaam + " " + data.achternaam,
        type: tags.history.type,
        afspraak_id: data.afspraak_id,
        klant_id: data.klant_id
    })
    let rowModals = [rowModal]
    dropdownList.appendChild(liAddHistory)

    //second item
    const liCreateAppointment = document.createElement('li')
    const aCreateAppointment = createLink({
        url: '#',
        title: tags.appointment.modalTitle
    })
    aCreateAppointment.classList.add('dropdown-item')
    aCreateAppointment.setAttribute('data-bs-toggle','modal')
    aCreateAppointment.setAttribute('data-bs-target', `#${tags.appointment.modalId}${data.klant_id}-${data.afspraak_id}`)
    liCreateAppointment.appendChild(aCreateAppointment)
    const appointmentModal = createModal({
        id: `${tags.appointment.modalId}${data.klant_id}-${data.afspraak_id}`,
        title: tags.appointment.modalTitle + " - " + data.voornaam + " " + data.achternaam,
        type: tags.appointment.type,
        afspraak_id: data.afspraak_id,
        klant_id: data.klant_id
    }, true)
    rowModals.push(appointmentModal)
    dropdownList.appendChild(liCreateAppointment)

    const liGGH = document.createElement('li')
    const aGGH = createLink({
        url: '#',
        title: tags.GGH.title
    });
    aGGH.id = tags.GGH.id + data.klant_id + '-' + data.afspraak_id
    aGGH.classList.add('dropdown-item')
    liGGH.addEventListener('click', (e) => {
        e.preventDefault()
        saveGGH(e.target.id)
    })
    liGGH.appendChild(aGGH)
    dropdownList.appendChild(liGGH)

    btnGroup.appendChild(dropdownList)
    return {
        btnGroup,
        rowModals
    }
}

function createLink(data) {
    const link = document.createElement('a')
    link.href = data.url
    link.innerText = data.title
    return link
}

function createModal(data, isXL = false) {
    const modal = createModalMeta(data)
    //modal dialog
    const modalDialog = document.createElement('div')
    modalDialog.classList.add('modal-dialog', 'modal-dialog-centered')
    if(isXL){
        modalDialog.classList.add('modal-xl')
        modalDialog.style.maxWidth = "1500px"
    }
    //modal content
    const modalContent = document.createElement('div')
    modalContent.classList.add('modal-content')
    //modal header
    const modalHeader = createModalHeader(data)
    //modal body
    const modalBody = createModalBody(data)
    //modal footer
    const modalFooter = createModalFooter(data)

    modalContent.appendChild(modalHeader)
    modalContent.appendChild(modalBody)
    modalContent.appendChild(modalFooter)
    modalDialog.appendChild(modalContent)
    modal.appendChild(modalDialog)

    return modal;
}

function createModalMeta(data) {
    const modal = document.createElement('div')
    modal.classList.add('modal', 'fade')
    modal.id = data.id
    modal.setAttribute('tabindex', '-1')
    modal.setAttribute('aria-labelledby', 'exampleModalScrollable2Label')
    modal.setAttribute('aria-hidden', 'true')
    modal.setAttribute('data-bs-keyboard', 'false')

    return modal
}

function createModalFooter(data) {
    const modalFooter = document.createElement('div')
    modalFooter.classList.add('modal-footer')
    //add close button
    const btnClose = document.createElement('button')
    btnClose.type = 'button'
    btnClose.classList.add('btn', 'btn-secondary')
    btnClose.setAttribute('data-bs-dismiss', 'modal')
    btnClose.setAttribute('aria-label', tags.modalGeneral.close)
    btnClose.innerText = tags.modalGeneral.close
    modalFooter.appendChild(btnClose)
    //add save button
    if(data.type === tags.history.type){
        const historyButtons = returnHistoryButtons(data)
        modalFooter.appendChild(historyButtons.btnSave)
        modalFooter.appendChild(historyButtons.btnNoNewAppointment)
    }
    else if(data.type === tags.appointment.type){
        const appointmentButtons = returnAppointmentButtons(data)
        modalFooter.appendChild(appointmentButtons)
    }
    return modalFooter
}

function createModalBody(data) {
    const modalBody = document.createElement('div')
    modalBody.classList.add('modal-body')

    const frmBody = document.createElement('form')
    if (data.type === tags.history.type) frmBody.appendChild(createAddHistoryForm(data))
    if (data.type === tags.appointment.type) frmBody.appendChild(createAppointmentForm(data))

    modalBody.appendChild(frmBody)
    return modalBody
}

function createModalHeader(data) {
    const modalHeader = document.createElement('div')
    modalHeader.classList.add('modal-header')
    const headerTitle = document.createElement('h6')
    headerTitle.classList.add('modal-title')
    headerTitle.id = 'staticBackdropLabel2'
    headerTitle.innerText = data.title
    modalHeader.appendChild(headerTitle)
    const btnClose = document.createElement('button')
    btnClose.type = 'button'
    btnClose.classList.add('btn-close')
    btnClose.setAttribute('data-bs-dismiss', 'modal')
    btnClose.setAttribute('aria-label', tags.modalGeneral.close)
    modalHeader.appendChild(btnClose)
    return modalHeader
}

function createAddHistoryForm(data) {
    const divFgDateInput = document.createElement('div')
    divFgDateInput.classList.add('form-group')
    //create input group
    const divIgDateInput = document.createElement('div')
    divIgDateInput.classList.add('input-group', 'mb-3')
    //create icon group
    const divIconDateInput = document.createElement('div')
    divIconDateInput.classList.add('input-group-text', 'text-muted')
    //create icon
    const iconInputDate = document.createElement('i')
    iconInputDate.classList.add('ri-calendar-line')
    // add icon to the icon group
    divIconDateInput.appendChild(iconInputDate)
    //add icon section to the input group
    divIgDateInput.appendChild(divIconDateInput)
    //create input field
    const inputDate = document.createElement('input')
    inputDate.type = 'text'
    inputDate.classList.add('form-control', 'datetimepicker')
    inputDate.id = `${tags.history.datetimeId}${data.klant_id}-${data.afspraak_id}`
    inputDate.setAttribute('placeholder', tags.history.datetimePlaceholder)
    inputDate.value = getCurrentDateTime()
    //add input to the input group
    divIgDateInput.appendChild(inputDate)
    //create input group
    const divIgHistoryInput = document.createElement('div')
    divIgHistoryInput.classList.add('input-group')
    //create span
    const spanHistoryInput = document.createElement('span')
    spanHistoryInput.classList.add('input-group-text')
    spanHistoryInput.innerText = tags.history.textareaPlaceholder
    //add span to input group
    divIgHistoryInput.appendChild(spanHistoryInput)
    //create input field
    const textAreaHistoryInput = document.createElement('textarea')
    textAreaHistoryInput.classList.add('form-control')
    textAreaHistoryInput.id = `${tags.history.textAreaId}${data.klant_id}-${data.afspraak_id}`
    textAreaHistoryInput.setAttribute('aria-label', tags.history.textareaPlaceholder)
    //add textarea to the input group
    divIgHistoryInput.appendChild(textAreaHistoryInput)

    divFgDateInput.appendChild(divIgDateInput)
    divFgDateInput.appendChild(divIgHistoryInput)

    return divFgDateInput
}

function getCurrentDateTime() {
    const now = new Date();

    // Get year, month, day, hours, and minutes
    const year = now.getFullYear();
    const month = (now.getMonth() + 1).toString().padStart(2, '0'); // Note: Months are zero-based
    const day = now.getDate().toString().padStart(2, '0');
    const hours = now.getHours().toString().padStart(2, '0');
    const minutes = now.getMinutes().toString().padStart(2, '0');

    // Format the date and time
    return `${year}-${month}-${day} ${hours}:${minutes}`;
}

async function saveHistory(id, noNewAppointment) {
    let klant_id = id.split('-')[id.split('-').length - 2]
    let afspraak_id = id.split('-')[id.split('-').length - 1]
    let datetime = document.getElementById(`${tags.history.datetimeId}${klant_id}-${afspraak_id}`).value
    let history = document.getElementById(`${tags.history.textAreaId}${klant_id}-${afspraak_id}`).value
    if(!validateSaveHistoryFields(datetime, history)){
        window.confirm(tags.history.fieldsNotFilled)
    } else {
        if (user_id === -1) setUserId()

        console.log(noNewAppointment)
        let data = {
            user_id,
            klant_id,
            afspraak_id,
            datetime,
            history,
            noNewAppointment
        }
        let result = await postJsData(tags.history.postNewHistoryUrl, data)
        if(result.status === 200){
            let usedModal = document.getElementById(`${tags.history.modalId}${klant_id}-${afspraak_id}`)
            //reset modal fields
            let usedForm = usedModal.querySelector('form')
            usedForm.reset();

            //close modal
            let closeButton = usedModal.querySelector('[data-bs-dismiss="modal"]');
            closeButton.click();
            //append new data to the table
            let correctCell = document.getElementById(`${tags.general.cell4}${klant_id}-${afspraak_id}`)
            // document.getElementById(tags.general.tableBodyId).removeChild(correctCell.parentNode)
            if(noNewAppointment){
                //remove row from table
                document.getElementById(tags.general.tableBodyId).removeChild(correctCell.parentNode)
                displayAlert("Opmerking toegevoegd aan klant, klant is uit de tb lijst gehaald.");
            } else {
                //add data to table
                correctCell.innerHTML += '<br><span class="text-success">' + result.text + '</span>'
                displayAlert("Opmerking toegevoegd aan klant.");
            }
            //display alert
        } else {
            window.confirm(result.error)
        }

    }
}

function validateSaveHistoryFields(datetime, history) {
    return !(datetime === '' || history === '');
}

function returnHistoryButtons(data){
    const btnSave = document.createElement('button')
    btnSave.type = 'button'
    btnSave.classList.add('btn', 'btn-primary')
    btnSave.setAttribute('aria-label', tags.modalGeneral.save)
    btnSave.id = `${tags.history.buttonId}${data.klant_id}-${data.afspraak_id}`
    btnSave.innerText = tags.modalGeneral.save
    btnSave.addEventListener('click', (e) => {
        e.preventDefault()
        saveHistory(e.target.id, false)
    })

    const btnNoNewAppointment = document.createElement('button')
    btnNoNewAppointment.type = 'button'
    btnNoNewAppointment.classList.add('btn', 'btn-danger')
    btnNoNewAppointment.setAttribute('aria-label', tags.modalGeneral.saveNoNew)
    btnNoNewAppointment.id = `${tags.history.buttonNoNewId}${data.klant_id}-${data.afspraak_id}`
    btnNoNewAppointment.innerText = tags.modalGeneral.saveNoNew
    btnNoNewAppointment.addEventListener('click', (e) => {
        e.preventDefault()
        saveHistory(e.target.id, true)
    })

    return {
        btnSave,
        btnNoNewAppointment
    }
}
function setUserId(){
    user_id = document.getElementById('user_id').value
}

function createAppointmentForm(data){
    const appointmentForm = document.createElement('form')

    //first row
    const row1 = document.createElement('div')
    row1.classList.add('row')
    const col1 = document.createElement('div')
    col1.classList.add('col-xl-6', 'col-lg-6', 'col-md-6', 'col-sm-12')
    const lblStartDate = document.createElement('label')
    lblStartDate.setAttribute('for','startdatum')
    lblStartDate.innerText = tags.appointment.lblStartDate
    lblStartDate.classList.add('form-label')
    col1.appendChild(lblStartDate)
    const inputStartDate = document.createElement('input')
    inputStartDate.classList.add('form-control', 'datetimepicker', 'mb-3')
    inputStartDate.id = `${tags.appointment.datetimeId}${data.klant_id}-${data.afspraak_id}`
    inputStartDate.setAttribute('placeholder', tags.appointment.lblStartDate)
    inputStartDate.setAttribute('name', tags.appointment.datetimeId)
    col1.appendChild(inputStartDate)
    const hiddenOldAppointmentId = document.createElement('input')
    hiddenOldAppointmentId.setAttribute('type','hidden')
    hiddenOldAppointmentId.id = `${tags.appointment.oldAppointmentId}${data.klant_id}-${data.afspraak_id}`
    hiddenOldAppointmentId.value = data.afspraak_id
    col1.appendChild(hiddenOldAppointmentId)
    row1.appendChild(col1)

    //second row
    const row2 = document.createElement('div')
    row2.classList.add('row', 'mb-5')
    const col2 = document.createElement('div')
    col2.classList.add('col-xl-12', 'col-lg-12', 'col-md-12', 'col-sm-12')
    const divEditor = document.createElement('div')
    divEditor.id = `${tags.appointment.editorId}${data.klant_id}-${data.afspraak_id}`
    col2.appendChild(divEditor)
    row2.appendChild(col2)

    //third row
    const row3 = document.createElement('div')
    row3.classList.add('row')
    row3.setAttribute('style', 'margin-top: 65px !important;')
    // row3.style.marginTop = '65px !important;'
    const col3 = document.createElement('div')
    col3.classList.add('col-xl-6', 'col-lg-6', 'col-md-6', 'col-sm-12')
    // 2 check boxes
    const check1 = document.createElement('div')
    check1.classList.add('form-check', 'form-check-inline')
    const checkboxBeforeTwelveThirty = document.createElement('input')
    checkboxBeforeTwelveThirty.setAttribute('type', 'checkbox')
    checkboxBeforeTwelveThirty.classList.add('form-check-input')
    checkboxBeforeTwelveThirty.id = `${tags.appointment.beforeTwelveThirtyId}${data.klant_id}-${data.afspraak_id}`
    checkboxBeforeTwelveThirty.value = '1'
    check1.appendChild(checkboxBeforeTwelveThirty)
    const labelBeforeTwelveThirty = document.createElement('label')
    labelBeforeTwelveThirty.setAttribute('for', `${tags.appointment.beforeTwelveThirtyId}${data.klant_id}-${data.afspraak_id}`)
    labelBeforeTwelveThirty.innerText = tags.appointment.beforeTwelveThirty
    labelBeforeTwelveThirty.classList.add('form-check-label')
    check1.appendChild(labelBeforeTwelveThirty)
    col3.appendChild(check1)

    // const check2 = document.createElement('div')
    // check2.classList.add('form-check', 'form-check-inline')
    // const checkboxCopyOldAppointment = document.createElement('input')
    // checkboxCopyOldAppointment.setAttribute('type', 'checkbox')
    // checkboxCopyOldAppointment.classList.add('form-check-input')
    // checkboxCopyOldAppointment.id = `${tags.appointment.copyOldAppointmentId}${data.klant_id}-${data.afspraak_id}`
    // checkboxCopyOldAppointment.value = '1'
    // checkboxCopyOldAppointment.checked = true;
    // check2.appendChild(checkboxCopyOldAppointment)
    // const labelCopyOldAppointment = document.createElement('label')
    // labelCopyOldAppointment.setAttribute('for', `${tags.appointment.copyOldAppointmentId}${data.klant_id}-${data.afspraak_id}`)
    // labelCopyOldAppointment.innerText = tags.appointment.copyOldAppointment
    // labelCopyOldAppointment.classList.add('form-check-label')
    // check2.appendChild(labelCopyOldAppointment)
    // col3.appendChild(check2)
    row3.appendChild(col3)

    //append all to form
    appointmentForm.appendChild(row1)
    appointmentForm.appendChild(row2)
    appointmentForm.appendChild(row3)
    return appointmentForm
}

function returnAppointmentButtons(data){
    const btnSave = document.createElement('button')
    btnSave.type = 'button'
    btnSave.classList.add('btn', 'btn-primary')
    btnSave.setAttribute('aria-label', tags.modalGeneral.save)
    btnSave.id = `${tags.appointment.buttonId}${data.klant_id}-${data.afspraak_id}`
    btnSave.innerText = tags.modalGeneral.save
    btnSave.addEventListener('click', (e) => {
        e.preventDefault()
        saveAppointment(e.target.id)
    })

    return btnSave
}

function validateSaveAppointmentFields(datetime, text){
    return !(datetime === '');
}

async function saveAppointment(id) {
    console.log(id);
    let idArray = id.split('-')
    let klant_id = idArray[idArray.length - 2]
    let afspraak_id = idArray[idArray.length - 1]

    let datetime = document.getElementById(`${tags.appointment.datetimeId}${klant_id}-${afspraak_id}`).value;
    let text = document.getElementById(`${tags.appointment.editorId}${klant_id}-${afspraak_id}`)
    let description = text.querySelector('.ql-editor').innerHTML
    let copyOld = true //!!(document.getElementById(`${tags.appointment.copyOldAppointmentId}${klant_id}-${afspraak_id}`).checked);
    let beforeTwelveThirty = !!(document.getElementById(`${tags.appointment.beforeTwelveThirtyId}${klant_id}-${afspraak_id}`).checked);
    if(!validateSaveAppointmentFields(datetime, text)){
        window.confirm(tags.appointment.validateFields)
    } else {
        let data = {
            klant_id,
            afspraak_id,
            datetime,
            description,
            user_id,
            copyOld,
            beforeTwelveThirty
        }
        console.log(data)
        //apply validations
        let result = await postJsData(tags.appointment.postNewAppointment, data)

        if (result.status === 200) {
            let usedModal = document.getElementById(`${tags.appointment.modalId}${klant_id}-${afspraak_id}`)
            console.log(usedModal, `${tags.appointment.modalId}${klant_id}-${afspraak_id}`);
            //reset modal fields
            let usedForm = usedModal.querySelector('form')
            usedForm.reset();

            //close modal
            let closeButton = usedModal.querySelector('[data-bs-dismiss="modal"]');
            closeButton.click();
            //append new data to the table
            let correctCell = document.getElementById(`${tags.general.cell4}${klant_id}-${afspraak_id}`)
            document.getElementById(tags.general.tableBodyId).removeChild(correctCell.parentNode)

            //display alert
            displayAlert("Afspraak succesvol aangemaakt, klant is uit de tb lijst gehaald.");
        } else {
            window.confirm(result.error)
        }
    }
}

function initiateQuill(){
    //get all editors
    const editorDivs = document.querySelectorAll('div[id^="editor-"]');

    // Iterate over the selected elements
    editorDivs.forEach(divEditor => {
        let idArray = divEditor.id.split('-')
        let data = {
            klant_id: idArray[1],
            afspraak_id: idArray[2],
        }
        let QuillEditor = new Quill('#' + divEditor.id, {
            modules: {
                toolbar: toolbarOptions
            },
            placeholder: "Bij leeg wordt de oude beschrijving opgehaald.",
            theme: 'snow'
        })
        tags.appointment.quillEditors.push({
            id: `#${tags.appointment.editorId}${data.klant_id}-${data.afspraak_id}`,
            editor: QuillEditor,
            oldAppointment_id: data.afspraak_id,
            klant_id: data.klant_id,
        })
    });
}

function initiateDatePicker(){
    //get all datepickers
    const datepickers = document.querySelectorAll('input.datetimepicker');
    // Iterate over the selected elements
    datepickers.forEach(datepicker => {
        let idArray = datepicker.id.split('-')
        let data = {
            klant_id: idArray[1],
            afspraak_id: idArray[2],
        }
        flatpickr('#' + datepicker.id, {
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            time_24hr: true
        })
    });
}

function displayAlert(message){
    let divAlert = document.getElementById('alertDiv')
    let contentAlert = document.getElementById('alertContent')
    contentAlert.innerText = message
    divAlert.style.display = 'block'

    setTimeout(() => {
        divAlert.style.display = 'none'
    }, 5000)
}

async function saveGGH(id) {
    let idArray = id.split('-')
    let klant_id = idArray[idArray.length - 2]
    let afspraak_id = idArray[idArray.length - 1]
    let data = {
        user_id,
        klant_id,
        afspraak_id
    }
    let result = await postJsData(tags.GGH.url, data)
    if (result.status === 200) {
        let correctCell = document.getElementById(`${tags.general.cell4}${klant_id}-${afspraak_id}`)
        console.log(result.data)
        correctCell.innerHTML += '<br><span class="text-success">' + result.data + '</span>'
        displayAlert("Klant verwerkt als GGH.");
    }
}
