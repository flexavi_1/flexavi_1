// Function to make a GET request
const isLive = window.location.hostname !== 'localhost';
const csrfToken = document.head.querySelector('meta[name="csrf-token"]').content;
function getJsData(apiUrl) {
    return new Promise((resolve, reject) => {
        fetch(apiUrl)
            .then(response => {
                if (response.status !== 200) {
                    if (!isLive) throw new Error(`HTTP error! Status: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                // Handle the data from the response
                if (!isLive) console.log('GET request successful:', data);
                resolve(data)
            })
            .catch(error => {
                // Handle errors
                if (!isLive) console.error('Error during GET request:', error);
                reject(error)
            });
    });
}

// Function to make a POST request
function postJsData(apiUrl, dataObject) {
    return new Promise((resolve, reject) => {
        fetch(apiUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': csrfToken,
                // Add any other headers if needed
            },
            body: JSON.stringify(dataObject),
        })
            .then(response => {
                if (response.status !== 200) {
                    if (!isLive) throw new Error(`HTTP error! Status: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                // Handle the data from the response
                if (!isLive) console.log('POST request successful:', data);
                resolve(data)
            })
            .catch(error => {
                // Handle errors
                if (!isLive) console.error('Error during POST request:', error);
                reject(error)
            });
    });
}
