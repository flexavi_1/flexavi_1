
const inpZipcode = document.getElementById('input-postcode');
const inpHousenumber = document.getElementById('input-housenumber');
const inpSuffix = document.getElementById('input-suffix');

inpZipcode.addEventListener( 'keyup', () => {
    retrieveZipcode(inpZipcode.value, inpHousenumber.value, inpSuffix.value)
})

inpHousenumber.addEventListener( 'keyup', () => {
    retrieveZipcode(inpZipcode.value, inpHousenumber.value, inpSuffix.value)
})

inpSuffix.addEventListener( 'keyup', () => {
    retrieveZipcode(inpZipcode.value, inpHousenumber.value, inpSuffix.value)
})

async function retrieveZipcode(zipcode, housenumber, suffix = null) {
    if (zipcode === null || housenumber === null) return false;
    if (zipcode.length < 6 || zipcode.length > 6) return false;

    let url = `https://api.pro6pp.nl/v1/autocomplete?auth_key=rZE6bOnZOszdzAh4&nl_sixpp=${zipcode}&streetnumber=${housenumber}`
    if (suffix !== null && suffix !== "") url += `&extension=${suffix}/`

    console.log(url)

    const response = await fetch(url, {
        method: 'GET'
    }).then(response => response.json())

    if(response.status === "ok"){
        appendData({
            street: response.results[0].street,
            city: response.results[0].city
        })
    } else {
        appendData({
            street: 'Ongeldig postcode en huisnummer combinatie',
            city: null
        })
    }

}

function appendData(data){
    const city = document.getElementById('input-city');
    const street = document.getElementById('input-street');

    city.value = data.city;
    street.value = data.street;
}

window.addEventListener("load", (event) => {
    retrieveZipcode(inpZipcode.value, inpHousenumber.value, inpSuffix.value)
});
