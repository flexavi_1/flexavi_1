function changePost(id, mail_id) { // id = mail id. Als de brief ook per post is verstuurd verandert het van 0 -> 1 en andersom.
    
    var id = id;
    var mail_id = mail_id;

    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200) {
            document.getElementById("post") = this.responseText;
        }
    };

    xmlhttp.open("GET", "/relatie/"+id+"/post/"+mail_id+"/change", true);
    xmlhttp.send();
};

$('.delete-btn').click(function(){// id = afspraak_id. Methode verandert attribuut eigenschuld naar 1.


    // Confirm
    if ( ! confirm('Deze actie verwijdert de mogelijke brief, weet u het zeker?')){
        return false;
    }
    //ophalen data id                                                                                                       , mogelijk moet hier nog iets gedaan worden met parent.parent
    var id = $(this).attr('data-id');   
    // Current button 
    var obj = this;

    var url = '/veranderEigenschuld';
    var csrf = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        type: "POST",
            url: url,
            data: {
                '_token': csrf,
                id: id
                
            }
        success : function(result){
            result = $.trim(result);
            if (result == 'OK'){
                // Remove HTML row
                $(obj).parent().parent().remove();
            }
            else{
                alert('request fails');
            }
        }
    
    });
});