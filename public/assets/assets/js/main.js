// Nieuwe code

$(function() {
    // set width for scrum board container
    altair_scrum_board.init();
    // draggable tasks
    altair_scrum_board.draggable_tasks();
});

altair_scrum_board = {
    init: function() {
        var $scrumBoard = $('#scrum_board'),
            childWidth = $scrumBoard.children('div').width(),
            childsCount = $scrumBoard.children('div').length;

            $scrumBoard.width(childWidth * childsCount);
    },
    draggable_tasks: function() {

        var drake = dragula($('.scrum_column > div').toArray());

        var containers = drake.containers,
            length = containers.length;

        for (var i = 0; i < length; i++) {
            $(containers[i]).addClass('dragula dragula-vertical');
        }

        drake.on('drop', function(el, target, source, sibling) {
            var url = '/back-module/ajax/has_afspraak';
            var csrf = $('meta[name="csrf-token"]').attr('content');
            
            var drop_id = $(target).attr('drop-id');
            var drag_ids = [];
            $(target).children('div').each(function () {
                drag_ids.push($(this).attr('drag-id'));
            });

            // send
            altair_scrum_board.updateList(drop_id,drag_ids,url,csrf);
        });
    },
    updateList : function(parent, order, url, csrf) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                '_token': csrf,
                parent: parent,
                order: order,
                
            }
        }).success(function(data) {
            //console.log(data);
            
        }).fail(function (data) {
            console.log(data);
            //alert("Something went wrong.");
        });
    }
};