 (function($) {
        $.fn.serial = function() {
            var array = [];
            var $elem = $(this);
            $elem.each(function(i) {
                var list_id = $(this).data('list-identifier');
                $('tr[data-afspraak-id]', this).each(function(e) {
                    array.push(list_id + '[' + e + ']=' + $(this).data('afspraak-id'));
                });
            });
            return array.join('&');
        }
    })(jQuery);

    $('.sortable > tbody').sortable({
        items: 'tr:has(td)',
        connectWith: 'tbody',
        update: function(event, ui) {
            if (this === ui.item.parent()[0]) {
                $.post('{{ route('planning.today.update', $datum) }}', $('.sortable > tbody').serial(), function() {
                    location.reload();
                });
            }
        },
        helper: function(e, tr)
          {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index)
            {
              // Set helper cell sizes to match the original sizes
              $(this).width($originals.eq(index).width());
            });
            return $helper;
          }
    });