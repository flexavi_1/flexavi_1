/**
 * Created by René on 3-1-2015.
 */
$(function () {

    $(document).on("click", ".check input", function(e) {

        $.ajax({
            type: "POST",
            url: "../../../../back-module/ajax/update_color.php?reddish=true&ajax=true",
            data: {
                id: $(this).parent().parent().attr("id"),
                red: $(this).prop("checked") ? 1 : 0
            }
        }).fail(function (data) {
            alert("Kleuren fout");
        });

    });

    $(".toggle-color").each(function () {

        var span = $("span", this);
        span.eq(1).hide();

        $(this).click(function () {

            span.toggle();
            $(this).parent().toggleClass("color-off");

            $(".color-pick").remove();

        });

    });

    var colors = ["none","#ffffff", "#ff0000", "#ff6600", "#ff9400", "#fec500", "#ffff00", "#8cc700", "#0fad00", "#00a3c7", "#0064b5", "#c5007c"];

    function updateColor(parent, child, color, url) {

        $.ajax({
            type: "POST",
            url: url,
            data: {
                parent: parent,
                child: child,
                color: color
            }
        }).fail(function (data) {
            alert("Update kleuren");
        });

    }

    $(document).on("click", ".color-enable tr:not(.tmp_tr) td:not(.check)", function (e) {

        $(".color-pick").remove();

        if ($(".color-off").size() == 0) {

            var el = $('<div class="color-pick" />');

            $.each(colors, function (key, value) {
                el.append('<span data-color="' + value + '" style="background: ' + value + '; border: 1px solid black; margin: 1px;"></span>')
            });

            $(this).append(el);

        }

    }).on("click", ".color-pick span", function (e) {
        var url = $('base').attr('href') + '/back-module/ajax/update_color';
        var current = $(this).parent().parent().css({
            backgroundColor: $(this).attr("data-color")
        });

        updateColor(current.parent().attr("id"), current.attr("class"), $(this).attr("data-color"), url);

        $(".color-pick").remove();

        return false;

    });

    // Return a helper with preserved width of cells
    var fixHelper = function (e, ui) {
        ui.children().each(function () {
            $(this).width($(this).width());
        });
        return ui;
    };

    function updateData(parent, order, url, csrf) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                '_token': csrf,
                parent: parent,
                order: order,
                
            }
        }).fail(function (data) {
            alert("Something went wrong.");
        });
    }

    /* function updateTime() {

        $(".sortable.time").each(function () {

            $("tr", this).each(function () {

                var timeEl = $("td", this).eq(0);
                var time = timeEl.html();
                var good = true;

                var current = $(this);
                while (current.length > 0 && good) {
                    if (current.find("td").eq(0).html() < time)
                        good = false;
                    current = current.next();
                }
                timeEl.toggleClass("error", !good);

            });

        });

    }

    updateTime(); */

    $(".sortable").each(function () {

        var par = $(this);

        par.sortable({
            distance: 3,
            items: "> tr:not(.tmp_tr)",
            dropOnEmpty: true,
            connectWith: '.sortable',
            helper: fixHelper,
            cursor: 'move',
            remove: function (event, ui) {
                if ($(this).html().replace(/^\s+/, "") == '') {
                    $(this).html('<tr class="tmp_tr"><td colspan="6">Geen afspraken.</td></tr>');
                }
            },
            update: function (event, ui) {
                var url = $('base').attr('href') + '/back-module/ajax/update_afspraken';
                var csrf = $('meta[name="csrf-token"]').attr('content');
                
                $(this).find('.tmp_tr').remove();
                updateData(par.attr("id"), par.sortable("toArray"), url, csrf);
                //updateTime();
            }
        }).disableSelection();

    });

});

$(function() {
    // set width for scrum board container
    altair_scrum_board.init();
    // draggable tasks
    altair_scrum_board.draggable_tasks();
});

altair_scrum_board = {
    init: function() {
        var $scrumBoard = $('#scrum_board'),
            childWidth = $scrumBoard.children('div').width(),
            childsCount = $scrumBoard.children('div').length;

            $scrumBoard.width(childWidth * childsCount);
    },
    draggable_tasks: function() {

        var drake = dragula($('.scrum_column > div').toArray());

        var containers = drake.containers,
            length = containers.length;

        for (var i = 0; i < length; i++) {
            $(containers[i]).addClass('dragula dragula-vertical');
        }

        drake.on('drop', function(el, target, source, sibling) {
            var url = '/back-module/ajax/has_afspraak';
            var csrf = $('meta[name="csrf-token"]').attr('content');
            
            var drop_id = $(target).attr('drop-id');
            var drag_ids = [];
            $(target).children('div').each(function () {
                drag_ids.push($(this).attr('drag-id'));
            });

            // send
            altair_scrum_board.updateList(drop_id,drag_ids,url,csrf);
        });
    },
    updateList : function(parent, order, url, csrf) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                '_token': csrf,
                parent: parent,
                order: order,
                
            }
        }).success(function(data) {
            //console.log(data);
            
        }).fail(function (data) {
            console.log(data);
            //alert("Something went wrong.");
        });
    }
};