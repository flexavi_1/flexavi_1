<?php

use App\Http\Controllers\AfspraakController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\ReminderController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\TerugbellijstController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('/login');

});

Route::get('/index', function () {

    if(Auth::check())

    {

    	return redirect('/home');

    }

    else

    {

    	return redirect('/login');

    }

});

// Route::get('/', function () {

//     //return redirect('/login');
// });



    Auth::routes();
// Route::group(['middleware' => 'auth'], function () {
    // Your authenticated routes go here

    Route::get('/dashboard_test', 'DashboardController@test');

    Route::get('/planning_test', 'PlanningController@test'); //query problem

    Route::get('/KlantEmailRefreshNow', 'KlantController@refreshEmails'); //not showing



    // Route::group(['middleware' => ['admin']], function(){



    Route::get('/set-up', 'BedrijfController@setup');

    Route::post('/set-up', 'BedrijfController@savesetup'); //one field is not saving



    Route::get('/home', 'HomeController@home');

    Route::post('/zoeken', 'KlantController@zoeken');






    #eind relatie routes



    #mail routes

    Route::get('/relatie/{id}/post/{mail_id}/change/{var}', 'KlantController@changePost');

    Route::get('relatie/{id}/mail/create', 'MailController@create');

    Route::post('relatie/{id}/mail/create', 'MailController@store');

    #end mail routes



    #activiteit routes

    Route::get('/activiteit', 'ActiviteitController@index');

    Route::get('/activiteit/nieuw', 'ActiviteitController@create');

    Route::post('/activiteit/nieuw', 'ActiviteitController@store');

    Route::get('/activiteit/{id}/wijzigen', 'ActiviteitController@edit');

    Route::post('/activiteit/{id}/wijzigen', 'ActiviteitController@update');

    Route::get('/activiteit/{id}/verwijderen', 'ActiviteitController@destroy');

    Route::get('/activiteit/{id}/geenmateriaal', 'Activiteit_has_materiaalController@noMats');

    Route::get('/activiteit/{id}/materiaal', 'Activiteit_has_materiaalController@create');

    Route::post('/activiteit/{id}/materiaal', 'Activiteit_has_materiaalController@store');

    #eind activiteit routes



    #wagenpark routes

    Route::get('/wagenpark', 'AutoController@index');

    Route::get('/wagenpark/nieuw', 'AutoController@create');

    Route::post('/wagenpark/nieuw', 'AutoController@store');

    Route::get('/wagenpark/{id}/wijzigen', 'AutoController@edit');

    Route::post('/wagenpark/{id}/wijzigen', 'AutoController@update');

    Route::get('/wagenpark/{id}/verwijderen', 'AutoController@destroy');

    Route::get('/wagenpark/voeg-geschiedenis-toe', 'AutoController@addHistory');

    Route::post('/wagenpark/voeg-geschiedenis-toe', 'AutoController@saveHistory');

    Route::get('/wagenpark/autolijst', 'AutoController@showhistory');

    #eind wagenpark routes



    #Afspraak Routes

    Route::get('/afspraak/maken/{id}', 'AfspraakController@create');

    Route::post('/afspraak/maken/{id}', 'AfspraakController@store');

    Route::get('/afspraak/maken/{id}/{afspraak_id}/werkzaamheden', 'AfspraakController@voegwerkzaamhedentoe');

    Route::post('/afspraak/maken/{id}/{afspraak_id}/werkzaamheden', 'AfspraakController@afspraakOpslaan');

    Route::get('/afspraak/maken/{id}/nietnodig', 'AfspraakController@notNeeded');

    Route::get('/afspraak/{id}/verwijderen', 'AfspraakController@delete');

    Route::get('/afspraak/{id}/verwerken', 'AfspraakController@process');

    Route::post('/returntoprocess/{id}', 'AfspraakController@returntoprocess');

    Route::post('/afspraak/{id}/verwerken', 'AfspraakController@setProcess');

    Route::post('afspraak/maken/{id}/geschiedenis', 'AfspraakController@saveHistoryToAppointment');

    Route::post('/afspraak/{id}/verwerken/gelukt/nieuwe-betaal-afspraak', 'AfspraakController@setNewDefPayment');

    Route::post('/afspraak/{id}/verwerken/gelukt/extrawerk', 'AfspraakController@setExtraWork');

    Route::post('/afspraak/{id}/garantie/afspraken/nietopgelost', 'AfspraakController@endWarranty');

    Route::post('/afspraak/{id}/garantie/afspraken/nietopgelost/terugbetaalverzoek', 'AfspraakController@setCashRepay');

    Route::post('/afspraak/{id}/verwerken/uitgestelde-betaling/nietgelukt', 'AfspraakController@defPaymentFailed');

    Route::post('/afspraak/{id}/uitgestelde-betaling/nieuwe-afspraak', 'AfspraakController@setNewDefPayment');

    // Route::get('afspraak/{id}/nazorg', 'AfspraakController@verstuurNazorg'); // Nog te bouwen.

    // Route::get('afspraak/{id}/boom', 'AfspraakController@toonBoom'); // Nog te bouwen.

    Route::get('afspraak/{id}/annuleren', 'AfspraakController@cancelApp');

    Route::get('/afspraak/{id}/annuleren/{sha_id}', 'AfspraakController@cancelAppFromStatus');

    Route::get('afspraak/{id}/wijzigen', 'AfspraakController@editAppointment');

    Route::post('afspraak/{id}/wijzigen', 'AfspraakController@storeAppointment');

    Route::post('afspraak/{id}/wijzigen/werkzaamheden', 'AfspraakController@storeWork');

    Route::get('afspraak/{id}/bewerken/activiteit/nieuw', 'AfspraakController@addNewWorkToExisting');

    Route::get('afspraak/{id}/activiteit/nieuw', 'AfspraakController@addNewWork');

    Route::post('afspraak/{id}/activiteit/nieuw', 'AfspraakController@saveWork');

    Route::post('afspraak/{id}/bewerken/activiteit/nieuw', 'AfspraakController@saveWorkToExisting');

    Route::get('afspraak/{id}/verwerken/activiteit/nieuw', 'ActiviteitController@addNewWork');

    Route::get('/afspraak/maken/{id}/nietnodig/profiel', 'AfspraakController@notNeededProfile');

    Route::post('/afspraak/{id}/betalingsherinnering', 'AfspraakController@procesPaymentReminder');

    Route::get('/planning/verkoop/vandaag/{werklijst_id}/{afspraak_id}/start', 'AfspraakController@start');

    Route::get('/veranderEigenschuld/{afspraak_id}', 'AfspraakController@veranderEigenschuld');

    Route::get('/afspraak/{id}/bevestiging', 'MailController@sendConfirmation');

    Route::post('/afspraak/{id}/garantie/afspraken', 'AfspraakController@setWarranty'); // Tot hier AFSPRAAK herzien



    Route::get('/afspraak/{id}/verwerken/nietgelukt', 'AfspraakController@nietGelukt');

    Route::post('/afspraak/{id}/verwerken/nietgelukt', 'AfspraakController@nietgelukteind');

    Route::get('/afspraak/{id}/verwerken/nietgelukt/nieuweafspraak/{proces_id}', 'AfspraakController@setAfspraak');

    // Route::get('/afspraak/{id}/verwerken/gelukt/nieuweafspraak', 'AfspraakController@setAfspraak');

    // Route::post('/afspraak/{id}/verwerken/gelukt/nieuweafspraak', 'AfspraakController@setAfspraakeind'); // hetzelfde als een regel hieronder?

    // Route::post('/afspraak/{id}/verwerken/nietgelukt/nieuweafspraak', 'AfspraakController@setAfspraakeind');

    // Route::post('/afspraak/{id}/verwerken/gelukt/extrawerk', 'AfspraakController@setWerk');



    // Route::post('/afspraak/{id}/garantie/afspraken/nietopgelost', 'AfspraakController@endWarranty');

    Route::get('/afspraak/{id}/garantie/afspraak/maken', 'AfspraakController@idWarrenty');

    Route::post('/afspraak/{id}/garantie/afspraak/maken', 'AfspraakController@setidWarrenty');



    Route::get('/afspraak/openstaande-betalingen', 'AfspraakController@getOpenDefPayments');

    Route::get('/afspraak/openstaande-betalingen/{status}', 'AfspraakController@getOpenDefPaymentsPerStatus');



    Route::get('/refreshAllAppointmentsForUpdatingStatusHasAfspraak', 'AfspraakController@refresh');




    Route::get('/afspraak/{id}/voeg-geschiedenis-toe', 'AfspraakController@addHistory');

    Route::get('/afspraak/{id}/reset/verwerking', 'AfspraakController@resetProcess');

    Route::get('/afspraak/{id}/vorige/{proces_id}', 'AfspraakController@Previous');







    #eind afspraak routes



    #vijftien euro routes

    Route::get('/vijftieneuro', 'VijftienController@index');

    Route::get('/vijftien/{afspraak_id}/nieuw', 'VijftienController@create');

    Route::get('/veranderEigenschuld/{afspraak_id}/vijftieneurolijst', 'AfspraakController@veranderEigenschuldvijftien');

    Route::post('/vijftien/multiselect/verstuurd', 'VijftienController@multiSend');

    Route::get('/vijftieneuro/{id}/single/send', 'VijftienController@singleSend');

    Route::get('/vijftieneuro/{id}/show', 'VijftienController@show');

    Route::get('/vijftieneuro/toon/alle-brieven', 'VijftienController@showAllToSend');

    Route::get('/vijftieneuro/wijzig/brieven', 'VijftienController@editAllLetters');

    Route::post('/vijftieneuro/wijzig/brieven', 'VijftienController@saveAllLetters');

    Route::post('/vijftieneuro/{id}/edit', 'VijftienController@editLetter');

    #end vijftien euro routes





    #Werknemer Routes

    Route::get('/werknemer', 'WerknemerController@index'); //Overzicht

    Route::get('/werknemer/zoeken/{type}', 'WerknemerController@getType');

    Route::get('/werknemer/nieuw', 'WerknemerController@create'); //toevoegen, laat pagina met formulier zien

    Route::post('/werknemer/nieuw', 'WerknemerController@store'); // Opslaan, slaat formulier op

    Route::get('/werknemer/{id}', 'WerknemerController@show'); //relatie profiel

    Route::get('/werknemer/{id}/wijzigen', 'WerknemerController@edit'); // laat formulier zien om te bewerken

    Route::post('/werknemer/{id}/wijzigen', 'WerknemerController@update'); // updaten

    Route::get('/werknemer/{id}/verwijderen', 'WerknemerController@destroy'); // verwijderen

    Route::get('/werknemer/gebruiker/koppelen', 'WerknemerController@setUser');

    Route::post('/werknemer/gebruiker/koppelen', 'WerknemerController@storeUser');

    #eind werknemer routes



    #Geschiedenis Routes

    Route::get('/geschiedenis/{id}/toevoegen', 'GeschiedenisController@create');

    Route::post('/geschiedenis/{id}/toevoegen', 'GeschiedenisController@store');

    Route::get('/geschiedenis/{id}/wijzigen', 'GeschiedenisController@edit');

    Route::post('/geschiedenis/{id}/wijzigen', 'GeschiedenisController@update');

    Route::get('/geschiedenis/{id}/verwijderen', 'GeschiedenisController@delete');

    Route::get('/geschiedenis/{klant_id}/toevoegen/{afspraak_id}', 'GeschiedenisController@setGeschiedenis');

    Route::get('/geschiedenis/{klant_id}/toevoegen/{afspraak_id}/ggh', 'GeschiedenisController@setGeschiedenisggh');

    Route::get('/relatie/{klant_id}/geschiedenis/{afspraak_id}/set', 'GeschiedenisController@deleteSet');



    #eind geschiedenis



    #crediteurroutes

    Route::post('/crediteur/{id}/toevoegen/garantie', 'CrediteurController@warrantyStore');

    Route::get('/crediteur/{id}/{bedrag}/betaald', 'CrediteurController@setPayment');

    Route::get('/crediteur', 'CrediteurController@index');

    Route::get('/relatie/{id}/crediteur', 'CrediteurController@create');

    Route::post('/relatie/{id}/crediteur', 'CrediteurController@store');

    Route::get('/crediteur/niet-betaald', 'CrediteurController@indexNiet');

    Route::get('/crediteur/betaald', 'CrediteurController@indexBetaald');

    Route::get('/crediteur/{id}/verwijderen', 'CrediteurController@destroy');

    #eind crediteurroutes



    #Factuurroutes

    Route::get('/afspraak/{id}/factuur', 'FactuurController@genereerFactuur'); //nog bouwen.

    Route::get('/debiteur', 'FactuurController@index');

    Route::get('/debiteur/open', 'FactuurController@open');

    // Route::get('/debiteur/factuur/nieuw', 'FactuurController@nieuw');

    Route::get('/factuur/{id}', 'FactuurController@show');

    Route::get('/factuur/{id}/betaald', 'FactuurController@betaald');

    Route::get('/factuur/{id}/verwijderen', 'FactuurController@destroy');

    Route::get('/factuur/{id}/eerste', 'FactuurController@eerste');

    Route::get('/factuur/{id}/tweede', 'FactuurController@tweede');

    Route::get('/factuur/{id}/derde', 'FactuurController@derde');

    Route::get('/factuur/{id}/vierde', 'FactuurController@vierde');

    Route::get('/factuur/nieuw/{id}/handmatig', 'FactuurController@manualNew'); // nog bouwen

    Route::post('/factuur/nieuw/{id}/handmatig', 'FactuurController@saveManual'); // nog bouwen

    #end factuur







    #offerte

    Route::get('/offerte', 'OfferteController@index');

    Route::get('/offerte/nieuw/{klant_id}', 'OfferteController@create');

    Route::post('offerte/nieuw/{klant_id}', 'OfferteController@store');

    Route::get('/afspraak/{id}/offerte', 'OfferteController@openAfspraak');

    Route::post('/afspraak/{id}/offerte', 'OfferteController@setAfspraak');

    // Route::get('/offerte/nieuw/{klant_id}/{id}', 'OfferteController@addWork');

    Route::post('/offerte/nieuw/{klant_id}/activiteitentoevoegen', 'OfferteController@storeWork');

    Route::get('/offerte/{id}', 'OfferteController@show');

    Route::get('/offerte/{id}/verwijderen', 'OfferteController@destroy');

    Route::get('/offerte/{id}/activiteit/nieuw', 'OfferteController@addNewWork');

    Route::post('/offerte/{id}/activiteit/nieuw', 'OfferteController@saveNewWork');

    Route::get('/offerte/{id}/honoreren', 'OfferteController@honoreren');

    Route::get('/offerte/{id}/honoreren/{type}', 'OfferteController@honorerenWithType');

    Route::post('/offerte/{id}/honoreren', 'OfferteController@opslaan');

    Route::get('/offerte/{id}/vervallen', 'OfferteController@vervallen');

    Route::get('/offerte/{id}/vervallen/{type}', 'OfferteController@vervallenWithType');

    Route::get('/relatie/{relatie_id}/offerte/{id}/wijzigen', 'OfferteController@edit');

    Route::post('/relatie/{relatie_id}/offerte/{id}/wijzigen', 'OfferteController@update');

    Route::post('/relatie/{relatie_id}/offerte/{id}/activiteit/wijzigen', 'OfferteController@editWork');

    Route::post('/relatie/{relatie_id}/offerte/{id}/activiteit/toevoegen', 'OfferteController@addWorktoEdit');

    Route::post('/offerte/afspraak/{id}/nieuw', 'AfspraakController@setOfferApp');

    Route::get('/offerte/zoeken/{id}', 'OfferteController@indexPerType');

    #endofferte



    #uren

    Route::get('/uren', 'UurController@index');

    Route::get('/uren/nieuw', 'UurController@create');

    Route::post('/uren/nieuw', 'UurController@store');

    Route::get('/uren/{id}/verwijderen', 'UurController@delete');



    Route::get('/verlof', 'VerlofController@index');

    Route::get('/verlof/status/{id}', 'VerlofController@showStatus');

    Route::get('/verlof/{id}/setstatus/{sid}', 'VerlofController@setStatus');

    Route::get('/verlof/{id}/verwijder', 'VerlofController@destroy');

    Route::get('/verlof/nieuw', 'VerlofController@create');

    Route::post('/verlof/nieuw', 'VerlofController@store');

    #enduren



    #aanvulling

    Route::get('/aanvulling', 'AanvullingController@index');

    Route::get('/aanvulling/toevoegen', 'AanvullingController@create');

    Route::post('/aanvulling/toevoegen', 'AanvullingController@store');

    Route::get('/aanvulling/{id}', 'Verzonden_aanController@show');

    Route::get('/aanvulling/{id}/markeergelezen', 'Verzonden_aanController@setRead');

    Route::get('/aanvulling/{id}/markeeropgelost', 'Verzonden_aanController@setSolved');

    Route::get('/aanvulling/{id}/goedkeuren', 'Verzonden_aanController@setRight');

    Route::get('/aanvulling/{id}/afkeuren', 'Verzonden_aanController@setWrong');

    Route::post('/aanvulling/{id}/afkeuren', 'ReactieController@setReaction');

    Route::post('/aanvulling/{id}/reactieplaatsen', 'ReactieController@setReaction');

    Route::get('/aanvulling/{id}/{ontvanger}/goedgekeurd', 'AanvullingController@approved');

    Route::get('/aanvulling/{id}/verwijderen', 'Verzonden_aanController@delete');

    Route::get('/aanvulling/totaal/verwijderen/{id}', 'AanvullingController@delete');

    Route::get('/aanvulling/werknemer/overzicht', 'AanvullingController@showEmployees');

    Route::get('/aanvulling/werknemer/{id}', 'AanvullingController@getEmployee');

    Route::get('/aanvulling/{id}/return', 'ReactieController@returnToAanvulling');

    #endaanvulling



    #planning

    Route::get('/planning/verkoop/{date}', 'PlanningController@todaySale');

    Route::post('/planning/verkoop/{date}/update', 'PlanningController@updateSchedule')->name('planning.today.update');

    Route::get('/planning/kantoor/{date}', 'PlanningController@todayAdmin');

    Route::get('/planning/verkoop', 'PlanningController@Salesplanning');

    Route::get('/planning/refreshCalender', 'PlanningController@refreshCalender');

    Route::post('/back-module/ajax/update_afspraken', 'PlanningController@updateAfspraken');

    Route::post('/back-module/ajax/has_afspraak', 'PlanningController@updateHasAfspraken');



    Route::get('/planning/verkoop/{date}/{werkdag_id}/aanpassen', 'PlanningController@editWorkday');

    Route::post('/planning/verkoop/{date}/{werkdag_id}/aanpassen', 'PlanningController@saveWorkday');

    Route::get('/planning/verkoop/{date}/{werklijst_id}/instellingen', 'PlanningController@getSettings');

    Route::post('/planning/verkoop/{date}/{werklijst_id}/instellingen', 'PlanningController@setSettings');

    Route::get('/planning/verkoop/{date}/{werkdag_id}/behandeld', 'PlanningController@setCheckWorkday');

    Route::get('/planning/verkoop/{date}/{werklijst_id}/print', 'PlanningController@returnWerklijst');

    Route::get('/planning/verkoop/{date}/{werklijst_id}/verwijderen', 'PlanningController@deleteWorklist');

    Route::get('/planning/werving/telling', 'PlanningController@showTelling');

    Route::get('/planning/werving/dag/{date}', 'PlanningController@todayAcquisition');

    Route::post('/planning/werving/dag', 'PlanningController@getAcquisition');

    Route::get('/planning/werving/koppel', 'PlanningController@getKoppel');

    Route::post('/planning/werving/koppel', 'PlanningController@setKoppel');

    Route::get('/planning/verkoop/{date}/{werkdag_id}/controle', 'PlanningController@checkWorkday');

    Route::get('/planning/verkoop/{date}/{id}', 'PlanningController@todaySale');

    Route::get('/planning/werving/ub', 'PlanningController@acquisitionUB');

    Route::get('/planning/verkoop/{date}/{werklijst_id}/controle/afgerond', 'PlanningController@finishWorkday');



    #financieel

    Route::get('/planning/verkoop/{date}/{werkdag_id}/dagstaat/controle', 'FinancieelController@checkDagstaat');

    Route::post('/planning/verkoop/{date}/{werkdag_id}/dagstaat/controle', 'FinancieelController@setDagstaat');

    Route::get('/planning/verkoop/{date}/{werkdag_id}/dagstaat', 'FinancieelController@maakDagstaat');

    Route::post('/planning/verkoop/{date}/{werkdag_id}/dagstaat', 'FinancieelController@updateDagstaat');

    // Route::get('/financiele-rapportage', 'FinancieelController@getDagstaat');

    Route::post('/financiele-rapportage', 'FinancieelController@showDagstaat');

    Route::get('/ublijst', 'FinancieelController@getDagstaat');

    Route::get('/opvragen-rapportage', 'FinancieelController@getRapportage');

    Route::post('/opvragen-rapportage', 'FinancieelController@postRapportage');



    #Functies

    Route::get('/functie', 'FunctieController@index');

    Route::get('/functie/{id}/verwijderen', 'FunctieController@destroy');

    Route::get('/functie/{id}/wijzigen', 'FunctieController@edit');

    Route::post('/functie/{id}/wijzigen', 'FunctieController@update');

    Route::get('/functie/nieuw', 'FunctieController@create');

    Route::post('/functie/nieuw', 'FunctieController@store');



    #controle

    Route::get('/controle/verwijderde-relaties', 'KlantController@showDeleted');

    Route::get('/controle/aanvulling', 'AanvullingController@showDelComp');

    Route::get('/controle/gebruikers', 'WerknemerController@showUsers'); // nog niet af

    Route::get('/controle/werklijst', 'PlanningController@conceptWorklist');

    Route::get('/controle/werklijst/{date}', 'PlanningController@worklistCheck');

    Route::post('/controle/werklijst/{date}', 'PlanningController@showStats');

    // Route::post('/controle/werklijst/{date}/{var1}/{var2}', 'PlanningController@showStats');

    Route::get('/user/{id}/non-active', 'WerknemerController@nonactiveUser');



    //Dashboard

    Route::post('/slaPostItOp', 'HomeController@slaPostItOp');

    Route::get('/dashboard', 'DashboardController@index');

    Route::get('/verwijderpostit/{id}', 'HomeController@removePI');





    Route::get('/leveranciers', 'KlantController@getLeveranciers');

    Route::get('/leverancier/{id}/prijslijst/nieuw', 'LeverancierController@getNewPricelist');

    Route::post('/leverancier/{id}/prijslijst/nieuw', 'LeverancierprijslijstController@setNewPricelist');

    Route::get('/relatie/{id}/setRequest', 'LeverancierprijslijstController@setRequest');





    Route::get('/materiaal', 'MateriaalController@index');

    Route::get('/materiaal/nieuw', 'MateriaalController@create');

    Route::post('/materiaal/nieuw', 'MateriaalController@store');

    Route::get('/materiaal/{id}/wijzigen', 'MateriaalController@edit');

    Route::post('materiaal/{id}/wijzigen', 'MateriaalController@update');

    Route::get('/materiaal/{id}/verwijderen', 'MateriaalController@delete');

    Route::get('/voorraad', 'VoorraadController@index');

    Route::get('/voorraad/plaats-verzoek', 'InkoopverzoekController@setRequest');

    Route::post('/voorraad/plaats-verzoek', 'InkoopverzoekController@saveRequest');

    Route::get('/voorraad/verzoek', 'InkoopverzoekController@index');

    Route::get('/voorraad/verzoek/status/{id}', 'InkoopverzoekController@statusIndex');

    Route::get('/voorraad/mutatie', 'VoorraadController@getAllMutations');

    Route::get('/voorraad/materiaal/{date}', 'VoorraadController@stockPerMat');

    Route::get('/verzoek/{id}', 'InkoopverzoekController@show');

    Route::get('/verzoek/{id}/{status_id}', 'InkoopverzoekController@setStatus');

    Route::get('/inkoop/verwerken/{id}', 'InkoopController@processInvoice');

    Route::post('/inkoop/verwerken/{id}', 'InkoopController@storeInvoice');



    #ticketing

    Route::get('/ticket', 'TicketController@index');

    Route::get('/ticket/nieuw', 'TicketController@create');

    Route::post('/ticket/nieuw', 'TicketController@store');

    Route::get('/ticket/{$id}/wijzigen', 'TicketController@update');

    Route::post('/ticket/{$id}/wijzigen', 'TicketController@save');

    Route::get('/ticket/{id}/verwijderen', 'TicketController@delete');

    // });





    //Klant gedeelte

    Route::get('/relatie/{id}/factuur/{factuur_id}/share', 'OpenController@sendInvoice');

    Route::get('/relatie/{id}/offerte/{offerte_id}/share', 'OpenController@sendOffer');

    Route::get('/share/{relatie_id}_{huisnummer}_v_{factuur_id}/factuur', 'OpenController@shareInvoice');

    Route::get('/share/{relatie_id}_{huisnummer}_v_{offerte_id}/offerte', 'OpenController@shareOffer');

    Route::get('/klantinlog', 'OpenController@inlog'); // to do

    Route::get('/share/auto', function () {

        echo "";
    });







    // Api gedeelte voor Datamart

    Route::get('/loadActivity', 'DatamartController@loadActivity');

    Route::get('/loadEmployee', 'DatamartController@loadEmployee');

    Route::get('/loadPlace', 'DatamartController@loadPlace');

    Route::get('/loadTime', 'DatamartController@loadTime');

    Route::get('/loadTypes', 'DatamartController@loadTypes');

    Route::get('/loadFacts', 'DatamartController@loadFacts');

    Route::get('/getProCode', 'DatamartController@getProCode');



    //for testing only

    Route::get('/testEmail', 'MailController@sendConfirmation');
// });

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
#relatie routes

 //not showing

Route::get('/relatie/zoeken/{type}', 'KlantController@indexType'); // not showing

Route::get('/relatie/{id}/delete', 'KlantController@destroy'); // verwijderen

Route::get('/relatie/{id}/geschiedenis/tabel', 'KlantController@profielTable');

Route::get('/relatie/{id}/setActive', 'KlantController@setActive');

Route::get('/relatie/{klant_id}/factuur/nieuw', 'FactuurController@create');

Route::get('/relatie/{id}/hardDelete', 'KlantController@hardDelete');

Route::get('/relatie/{id}/voorbeeldbrief/{afspraak_id}', 'MailController@voorbeeld');

Route::get('/relatie/{id}/verstuur/{afspraak_id}/brief/{letter_id}', 'MailController@sendFirst');

Route::post('/relatie/{id}/verstuur/{afspraak_id}/brief/{letter_id}/edit', 'MailController@sendEditedFirst');

Route::get('/relatie/{id}/voorbeeldbrief/{afspraak_id}', 'MailController@voorbeeld');

Route::get('/relatie/{id}/zetAanvulling/{date}/{werkdag_id}', 'AanvullingController@setCustomer');

Route::post('/relatie/{id}/zetAanvulling/{date}/{werkdag_id}', 'AanvullingController@saveToCustomer');

Route::get('/relatie/{id}/add/email', 'KlantController@addEmail');

Route::post('/relatie/{id}/add/email', 'KlantController@storeEmail');

Route::get('/refreshAddresses', 'KlantController@refreshAddresses');

// NEW ROUTES - 10-01-2024

Route::get('/relatie', [ClientController::class, 'index']); //Overzicht
Route::get('/relatie/nieuw', [ClientController::class, 'create']); //toevoegen, laat pagina met formulier zien
Route::post('/relatie/nieuw', [ClientController::class, 'store']); // Opslaan, slaat formulier op
Route::get('/relatie/{client}', [ClientController::class, 'show']); //relatie profiel
Route::get('/relatie/{client}/aanpassen', [ClientController::class, 'edit']); // laat formulier zien om te bewerken
Route::post('/relatie/{client}/aanpassen', [ClientController::class, 'update']); // updaten
Route::get('/relatie/overzicht/getDatatable', [ClientController::class, 'getDatatable']);

Route::post('/zoekresultaten', [SearchController::class, 'search']);
Route::get('/zoekresultaten', [SearchController::class, 'index']);

#reminder
Route::get('/reminder', [ReminderController::class, 'index']);
Route::get('/reminder/{id}/behandeld', [ReminderController::class, 'processed']);

#terugbellijst
Route::get('/terugbellijst', [TerugbellijstController::class, 'index']);
Route::get('/terugbellijst/js', [TerugbellijstController::class, 'getIndexData']);
Route::post('/terugbellijst/setComment', [TerugbellijstController::class,'setComment']);
Route::post('/terugbellijst/setAppointment', [TerugbellijstController::class,'setAppointment']);
Route::post('/terugbellijst/setGGH', [TerugbellijstController::class,'setGGH']);
Route::get('/afspraak/maken/{id}/oude_afspraak/{afspraak_id}', [AfspraakController::class,'create2']); // -> create appointment from terug bel lijst

#controle
Route::get('/controle/logbeheer', [LogController::class, 'showControle']);
Route::get('/controle/logbeheer/{user_id}', 'LogController@showUser');
